package com.igeek.ch02;

import com.igeek.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class UserMapperTest {

    //会话工厂
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        //1.加载类路径下 mybatis的核心配置文件  产生输入流
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.通过输入流 构建会话工厂
        factory = new SqlSessionFactoryBuilder().build(is);
    }

    @Test
    public void selectOneById() {
        //3.通过会话工厂 创建会话对象
        SqlSession sqlSession = factory.openSession();
        //4.获取接口的代理对象  class com.sun.proxy.$Proxy5 --》jdk动态代理
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        System.out.println("userMapper = " + userMapper.getClass());
        //5.执行
        User user = userMapper.selectOneById(1);
        System.out.println("user = " + user);
        //6.关闭资源
        sqlSession.close();
    }
    @Test
    public void selectAllByName(){
        //3.通过会话工厂 创建会话对象
        SqlSession sqlSession = factory.openSession();
        //4.获取接口的代理对象
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        //5.执行
//        List<User> userList = mapper.selectAllByName("张");
        //引发SQL注入问题
        List<User> userList = mapper.selectAllByName(" '' or 1=1 -- ");
        userList.forEach(System.out::println);
        //6.关闭资源
        sqlSession.close();
    }
}