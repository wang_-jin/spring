package com.igeek.ch01.jdbc;

import com.igeek.pojo.User;

import java.sql.*;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/18 11:26
 */
public class UserDao {
    public User selectOne(int id) throws ClassNotFoundException, SQLException {
        //1.注册驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接对象
        Connection connection =
                DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/testmybatis?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai",
                        "root",
                        "root");
        //3.创建预编译语句对象
        PreparedStatement ps = connection.prepareStatement("select * from user where username=?");
        //4.给问号处赋值
        ps.setInt(1, id);
        //5.执行sql 后获取的结果集ResultSet
        ResultSet rs = ps.executeQuery();
        User user = null;
        while (rs.next()) {
            user = new User(
                    rs.getInt("id"),
                    rs.getString("username"),
                    rs.getString("sex"));

        }
        //6.释放连接资源
        if(rs!=null){
            rs.close();
        }
        if(ps!=null){
            ps.close();
        }
        if(connection!=null){
            connection.close();
        }
        return user;
    }

}
