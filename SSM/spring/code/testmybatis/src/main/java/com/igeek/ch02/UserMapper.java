package com.igeek.ch02;

import com.igeek.pojo.User;

import java.util.List;

/**
 * 接口中只定义抽象方法
 *
 * BindingException异常出现的原因：
 * UserMapper接口和UserMapper.xml映射
 * 1.UserMapper接口和UserMapper.xml 同名 且 同包下
 * 2.UserMapper.xml 中 namespace命名空间的值 必须是UserMapper接口的全类名
 * 3.UserMapper.xml 中 mapped statement中的id值，必须和UserMapper接口的方法名 相同
 * 4.UserMapper.xml 中 mapped statement中的paramterType值,必须和UserMapper接口的方法的形参类型 相同
 * 5.UserMapper.xml 中 mapped statement中的resultType值,必须和UserMapper接口的方法的返回值类型 相同
 * 6.在SqlMapConfig.xml中 必须添加扫描接口的路径 为接口创建代理对象
 */
public interface UserMapper {
    //根据编号进行查询用户信息
    public User selectOneById(int id);

    //根据名称查询用户信息
    public List<User> selectAllByName(String name);
}
