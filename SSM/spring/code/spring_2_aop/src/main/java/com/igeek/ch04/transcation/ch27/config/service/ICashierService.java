package com.igeek.ch04.transcation.ch27.config.service;


import com.igeek.ch04.transcation.ch27.config.exception.AccountException;

import java.io.IOException;
import java.util.List;

public interface ICashierService {

	//客户的结账
	public void cash(String username, List<Integer> bookIds) throws IOException, AccountException;
}
