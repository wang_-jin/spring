package com.igeek.ch03.circle.prototype;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 11:30
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        //出现循环依赖问题 BeanCurrentlyInCreationException
        //这种循环依赖是无法解决的 因为他没有缓存概念，每次创建的是一个新对象
        A a = ac.getBean("a", A.class);
        System.out.println("a = " + a);

       B b = ac.getBean("b", B.class);
        System.out.println("b = " + b);
    }
}
