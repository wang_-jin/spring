package com.igeek.ch03.circle.singleton;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:49
 *
 * Spring的解决思路：
 * A创建过程 生命周期
 *   sc:singletonsCurrentlyInCreation    add(A)存储正在创建的单例A ==> sc中存在A 说明有发生循环依赖
 * 1.推断构造方法，实例化 new A() -->若需要提前暴露  存入三级缓存singletonFactories  Map<a,ObjectFactory>(A的beanName&beanDefinition...)
 * 2.属性注入 B,C
 *      B创建过程 生命周期
 *      1.推断构造方法，实例化 new B()
 *      2.属性注入 A --> 一级缓存中查找-->没有 -->二级缓存中查找-->没有-->三级缓存中查找到-->先存入到二级缓存，再移除三级缓存中的bean -->二级缓存中获取到A代理对象
 *      3.初始化后，有可能发生AOP动作
 *      4.存入一级缓存中 单例池
 *
 *      C创建过程 生命周期
 *      1.推断构造方法，实例化 new C()
 *      2.属性注入 A --> 一级缓存中查找-->没有 -->二级缓存中获取到A代理对象
 *      3.初始化后，有可能发生AOP动作
 *      4.存入一级缓存中 单例池
 *
 * 3.初始化后，有可能发生AOP动作（判断实例A是否已经提前进行AOP 已经提前直接从二级缓存中获取A代理对象）
 * 4.存入一级缓存中 单例池
 *   sc:singletonsCurrentlyInCreation    remove(A)存储正在创建的单例A
 *
 *
 *  出现循环依赖如何解决：
 *  1.单例的setter注入  作用域是singleton spring默认解决了  解决思想：三级缓存
 *  2.多例的setter注入  作用域是prototype spring无法解决   解决思想：不要使用原型，就使用单例
 *  3.构造器注入        作用域是singleton spring无法解决   解决思想：@Lazy 延迟加载
 *  4.DependsOn循环依赖
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        B b = ac.getBean("b", B.class);//b = com.igeek.ch03.circle.singleton.B@7a36aefa
        System.out.println("b = " + b);
        A a = ac.getBean("a", A.class);//a = com.igeek.ch03.circle.singleton.A@17211155
        System.out.println("a = " + a);
        System.out.println("a.getClass().getName() = " + a.getClass().getName());
        //com.igeek.ch03.circle.singleton.A$$EnhancerBySpringCGLIB$$8a381a2 代理对象
        System.out.println("-----------------------------");
        a.testA();

        System.out.println("===============================");
        //在单例setter注入的情况，此时有循环依赖的问题存在，但是没有报错，spring解决了
        b.demo();
    }
}
