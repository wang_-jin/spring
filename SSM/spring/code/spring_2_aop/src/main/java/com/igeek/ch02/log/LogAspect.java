package com.igeek.ch02.log;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Description 日志切面
 * @Author fqq
 * @Date 2023/11/15 14:47
 *
 * 声明 切面：
 * 1.@Component  注册到IOC容器中
 * 2.@Aspect     声明当前类是一个切面
 *
 * spring-AOP的使用步骤：
 * 1.在pom.xml中添加 spring-aop  org.aspectj 两个依赖
 * 2.在配置类 扫描包注解@ComponentScan + 开启AOP动态代理@EnableAspectJAutoProxy
 * 3.接口的实现类 添加 @Component 注入到IOC容器中
 * 4.编写一个切面类 @Component+@Aspect
 * 5.在切面中编写方法（通知）@Before前置通知 @After 后置通知...
 * 6.在通知方法中，通过切点表达式execution 确定作用在哪一个类的哪一个方法的位置上
 * 7.在通知方法中，可以通过JoinPoint连接点 获取目标方法的方法名称和形参信息
 */
@Component
@Aspect
public class LogAspect {


    /**
     * @Before注解 表示 前置通知
     * @param joinPoint 连接点 获取到目标方法的方法名 方法的形参列表 （无法手动调用目标方法）
     *
     * 切点表达式
     *  execution(public int com.igeek.ch02.log.ICount.add(int,int))
     *  execution(public int com.igeek.ch02.log.ICount.*(int,int)) *代表的是任意方法
     *  execution(public * com.igeek.ch02.log.ICount.*(..))  第一个*代表的是 任意返回值 第二个*代表的是任意方法  ..代表的是任意参数
     *  execution( * com.igeek.ch02.log.ICount.*(..))  任意的访问权限
     *
     * execution( * *(..)) 任意类中的任意方法
     */
    @Before("execution( * com.igeek.ch02.log.ICount.*(..))")
    public void beforeAdvice(JoinPoint joinPoint){
        System.out.println("beforeAdvice method="+joinPoint.getSignature().getName()
                +",args="+ Arrays.toString(joinPoint.getArgs()));
    }

    //后置通知
    @After("execution( * com.igeek.ch02.log.ICount.*(..))")
    public void afterAdvice(JoinPoint joinPoint){
        System.out.println("afterAdvice method="+joinPoint.getSignature().getName()
                +",args="+ Arrays.toString(joinPoint.getArgs()));
    }


    /**
     * //返回通知
     * @param joinPoint
     * @param result 接收方法的执行结果，名称必须和@AfterReturning注解中returning的属性值相同
     *
     * @AfterReturning注解：
     * 1.value属性：声明切点表达式
     * 2.returning属性：值必须和方法的形参列表名称要相同 接收方法的执行结果
     */
    @AfterReturning(
            value = "execution( * com.igeek.ch02.log.ICount.*(..))",
            returning = "result"
    )
    public void returningAdvice(JoinPoint joinPoint,Object result){
        System.out.println("returningAdvice method="+joinPoint.getSignature().getName()
                +",result="+result);
    }
    //异常通知
    @AfterThrowing(
            value = "execution( * com.igeek.ch02.log.ICount.*(..))",
            throwing = "ex"
    )
    public void throwableAdvice(JoinPoint joinPoint,Throwable ex){
        System.out.println("throwableAdvice method="+joinPoint.getSignature().getName()
                +",发生了异常ex="+ex.getMessage());
    }
}
