package com.igeek.ch02.log;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 14:49
 */
//@EnableAspectJAutoProxy  开启AOP动态代理
//1.bean实例 发现有实现接口 则默认使用jdk动态代理
@ComponentScan("com.igeek.ch02.log")
@EnableAspectJAutoProxy
public class MyConfig {
}
