package com.igeek.test;


import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/15 19:51
 * @description TODO
 */
@Component("count")
public class CountImpl implements ICount {

    @Override
    public int add(int i, int j) {
        System.out.println("核心业务...add(int i, int j),i="+i+",j="+j);
        return i+j;
    }

    @Override
    public int sub(int i, int j) {
        System.out.println("核心业务...sub(int i, int j),i="+i+",j="+j);
        return i-j;
    }

    @Override
    public int mul(int i, int j) {
        System.out.println("核心业务...mul(int i, int j),i="+i+",j="+j);
        return i*j;
    }

    @Override
    public int div(int i, int j) {
        System.out.println("核心业务...div(int i, int j),i="+i+",j="+j);
        return i/j;
    }


}