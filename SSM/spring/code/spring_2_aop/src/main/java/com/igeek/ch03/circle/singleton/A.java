package com.igeek.ch03.circle.singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:47
 */
@Component
public class A {
    //单例的setter注入
    @Autowired
    private B b;
    @Autowired
    private C c;

    public void testA(){
        System.out.println("testA()...");
    }
}
