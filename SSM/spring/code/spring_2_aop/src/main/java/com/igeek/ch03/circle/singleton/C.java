package com.igeek.ch03.circle.singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 10:40
 */
@Component
public class C {
    @Autowired
    private A a;
}
