package com.igeek.ch03.circle.prototype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 11:29
 */
@Scope("prototype")
@Component
public class B {
    @Autowired
    private A a;
}
