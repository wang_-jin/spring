package com.igeek.ch03.circle.constructor;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 11:35
 */
@Component
public class B {
    private A a;
    //@Lazy 延迟加载 加载IOC容器时不需要注入属性a 只有使用属性a的时候才会注入
    @Lazy
    public B(A a) {
        this.a = a;
    }
}
