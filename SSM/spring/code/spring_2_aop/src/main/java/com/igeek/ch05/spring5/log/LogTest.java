package com.igeek.ch05.spring5.log;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/18 9:40
 */
public class LogTest {
    public static void main(String[] args) {
        //创建一个日志生成器 ==> 后续都会使用Lombok插件+注解直接创建日志生成器
        Logger logger = LoggerFactory.getLogger(LogTest.class);
        //输出日志(包含日志信息和级别)
        String name = "LogTest";
        int id = 1;
        logger.info("这是来自于{}一个日志重要信息,{}",name,id);
        logger.warn("这是一个日志警告信息");
        logger.debug("这是一个调试信息!!!");
    }
}
