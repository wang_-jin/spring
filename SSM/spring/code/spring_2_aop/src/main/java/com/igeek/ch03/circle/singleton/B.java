package com.igeek.ch03.circle.singleton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:47
 */
@Component
public class B {
    @Autowired
    private A a;//代理对象(AOP起作用)

    public void demo(){
        //原始对象？？ 代理对象？？
        a.testA();
    }
}
