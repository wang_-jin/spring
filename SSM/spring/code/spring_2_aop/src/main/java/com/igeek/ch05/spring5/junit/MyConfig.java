package com.igeek.ch05.spring5.junit;

import org.springframework.context.annotation.Bean;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/18 10:23
 */
public class MyConfig {

    @Bean
    public User user(){
        return new User("张三","123");
    }
}
