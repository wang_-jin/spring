package com.igeek.ch01;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 11:40
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        ICount count = ac.getBean("count", ICount.class);
        System.out.println("count = " + count.toString());//System.out.println(对象)底层实现的是对象的toString()方法
        System.out.println("count.getClass().getName() = " + count.getClass().getName());//com.sun.proxy.$Proxy8
//
        System.out.println("----------------------------");
        /*
        *  开始日志追踪 method=add,args=[1, 2]
            核心业务...add(int i, int j),i=1,j=2
            结束日志追踪 method=add,result=3
        * */
        int add = count.add(1, 2);
        System.out.println("add(1, 2)=result = " + add);

        System.out.println("----------------------------");
        /*
        *   开始日志追踪 method=mul,args=[2, 3]
            核心业务...mul(int i, int j),i=2,j=3
            结束日志追踪 method=mul,result=6*/
        int mul = count.mul(2, 3);
    }
}
