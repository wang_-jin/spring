package com.igeek.test;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/15 20:20
 * @description TODO
 */
@Component
@Aspect
@Order(1)
public class ValidateAspect {
    @Pointcut("execution( * com.igeek.test.ICount.*(..))")
    public void myPointCut(){}

    @Around("myPointCut()")
    public Object validate(ProceedingJoinPoint jp) {

        Object result = null;
        String name = jp.getSignature().getName();
        System.out.println("开始进行参数校验... method="+name);
        Object[] args = jp.getArgs();
        for (int i = 0; i < args.length; i++) {
            int num = (int)args[i];
            if (num<0){
                System.out.println("参数校验 method=" + name + ",参数不合法");
                return -1;
            }
        }
        try {
            //参数合法 则执行目标方法
            System.out.println("参数校验 method="+name+",参数合法，正常计算");
            result = jp.proceed(args);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return result;
    }
}
