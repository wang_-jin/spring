package com.igeek.ch03.circle.constructor;

import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 11:35
 */
@Component
public class A {
    private B b;
    //构造器的注入
    public A(B b) {
        this.b = b;
    }
}
