package com.igeek.ch03.circle.constructor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:48
 *
 * 如何开启AOP:
 */
@ComponentScan("com.igeek.ch03.circle.constructor")
@EnableAspectJAutoProxy
public class MyConfig {
}
