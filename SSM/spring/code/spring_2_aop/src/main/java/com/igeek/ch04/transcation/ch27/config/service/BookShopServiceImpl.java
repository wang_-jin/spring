package com.igeek.ch04.transcation.ch27.config.service;


import com.igeek.ch04.transcation.ch27.config.dao.IBookShopDao;
import com.igeek.ch04.transcation.ch27.config.exception.AccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * 编程式事务：
 * 1.获取连接对象 conn
 * 2.取消事务的自动提交 conn.setAutoCommit(false)
 * 3.执行 购买业务 ：1.更新书的库存 2.更新账户余额 ==> 原子性
 * 4.若出现异常catch 则回滚事务 conn.rollBack()
 * 5.finally中，提交事务恢复事务自动提交 conn.commit();conn.setAutoCommit(true);
 *
 * 声明式事务：
 * 注解：@Transactional 标注的是业务方法，表示该方法可以使用事务管理
 * 原理：动态代理
 *
 */

@Service
public class BookShopServiceImpl implements IBookShopService {

	@Autowired(required = false)
	private IBookShopDao bookShopDao;


	/**
	 *@Transactional注解：表示事务的管理
	 * 1.propagation属性:事务的传播行为  默认值：REQUIRED
	 * 		REQUIRED：
	 * 		REQUIRES_NEW：该方法必须启动一个新事务, 并在自己的事务内运行. 如果有事务在运行, 就应该先挂起它.
	 * 2.isolation属性： 设置隔离事务属性
	 * 		READ_COMMITTED：读提交 oracle默认支持
	 * 		REPEATABLE_READ:可重复读 mysql默认支持
	 * 3.timeout属性，事务在强制回滚之前可以保持多久. 这样可以防止长期运行的事务占用资源. 单位是秒
	 * 4.readOnly只读属性为true时，表示这个事务只读取数据但不更新数据, 这样可以帮助数据库引擎优化事务
	 * 		readOnly = true
	 * 5.rollbackFor:指定回滚异常的类类型
	 *   noRollbackFor：不会发生回滚异常的类类型
	 *
	 *   默认情况下只有(RuntimeException和Error类型的异常)会导致事务回滚,
	 *   Exception不会导致事务回滚
	 *
	 */
	@Transactional(
			propagation = Propagation.REQUIRES_NEW,
			isolation = Isolation.REPEATABLE_READ,
			timeout = 2
//			rollbackFor = ArithmeticException.class,
//			noRollbackFor = IOException.class
	)
	@Override
	public void purchase(String username, int bookId) throws AccountException, IOException {
		//测试超时属性
		/*try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		//根据书号 查询书的价格
		int price = bookShopDao.selectPrice(bookId);
		//根据书号 更新书的库存
		bookShopDao.updateStock(bookId);
		//根据账户和购买的金额 更新账户余额
		bookShopDao.updateBalance(username, price);

		//测试发生数学异常 -- 会事务回滚
		//int i = 10/0;

		//测试发生IO异常 -- 不会事务回滚
		//throw new IOException();
	}
}
