package com.igeek.ch03.circle.singleton;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 10:28
 */
@Component
@Aspect
public class MyAspect {
    //前置通知
    @Before("execution(* com.igeek.ch03.circle.singleton.A.testA(..))")
    public void beforeAdvice(){
        System.out.println("MyAspect切面的前置通知....");
    }
}
