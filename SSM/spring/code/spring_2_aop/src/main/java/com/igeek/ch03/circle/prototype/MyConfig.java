package com.igeek.ch03.circle.prototype;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:48
 *
 */
@ComponentScan("com.igeek.ch03.circle.prototype")
@EnableAspectJAutoProxy
public class MyConfig {
}
