package com.igeek.ch04.transcation.ch27.config.service;

import com.igeek.ch04.transcation.ch27.config.exception.AccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

//@Transactional 标注在类之上  类中所有方法都添加事务管理
//@Transactional 标注在方法之上 只对当前方法添加事务管理
@Transactional
@Service
public class CashierServiceImpl implements ICashierService {

	@Autowired(required = false)
	private IBookShopService bookShopService;


	//@Transactional
	@Override
	public void cash(String username, List<Integer> bookIds) throws IOException, AccountException {
		for (Integer bookId : bookIds) {
			bookShopService.purchase(username, bookId);
		}
	}

}
