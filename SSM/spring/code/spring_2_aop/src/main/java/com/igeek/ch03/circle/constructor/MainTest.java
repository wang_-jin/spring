package com.igeek.ch03.circle.constructor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 9:49
 *
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        B b = ac.getBean("b", B.class);
        System.out.println("b = " + b);
        //spring无法解决循环依赖的问题BeanCurrentlyInCreationException

        A a = ac.getBean("a", A.class);
        System.out.println("a = " + a);

    }
}
