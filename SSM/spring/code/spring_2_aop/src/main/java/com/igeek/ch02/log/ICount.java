package com.igeek.ch02.log;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 11:37
 */
public interface ICount {
    public int add(int i, int j);

    public int sub(int i, int j);

    public int mul(int i, int j);

    public int div(int i, int j);

    public double add(double i, double j);
}
