package com.igeek.ch04.transcation.ch27.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 14:36
 */
//扫描组件包 确保可以注入dao实例 service实例
@ComponentScan("com.igeek.ch04.transcation.ch27.config")
//加载属性文件 确保可以通过${key}进行获取文件中value
@PropertySource("db.properties")
//开启事务管理
@EnableTransactionManagement
public class MyConfig {

    //配置数据源
    @Bean
    public DataSource dataSource(
            @Value("${db.user}") String user,
            @Value("${db.password}") String password,
            @Value("${db.url}") String url,
            @Value("${db.driverClass}") String driverClass) throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(driverClass);
        dataSource.setJdbcUrl(url);
        dataSource.setUser(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    //配置JdbcTemplate 需要注入dataSource
    //把需要的资源放在形参列表中，在创建实例时会自动在IOC容器中找到匹配的DataSource完成注入
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    //配置事务管理器的实例
    @Bean
    public TransactionManager transactionManager(DataSource dataSource){
        DataSourceTransactionManager manager =
                new DataSourceTransactionManager(dataSource);
        return manager;
    }


}
