package com.igeek.ch01;

import org.springframework.stereotype.Component;

/**
 * @Description 核心业务
 * @Author fqq
 * @Date 2023/11/15 11:37
 *
 * 核心业务:计算器
 * 需求升级：计算器+日志追踪
 * 解决方法：动态代理模式  jdk动态代理(接口代理) cglib动态代理
 */
@Component("count")
public class CountImpl implements ICount{

    @Override
    public int add(int i, int j) {
        System.out.println("核心业务...add(int i, int j),i="+i+",j="+j);
        return i+j;
    }

    @Override
    public int sub(int i, int j) {
        System.out.println("核心业务...sub(int i, int j),i="+i+",j="+j);
        return i-j;
    }

    @Override
    public int mul(int i, int j) {
        System.out.println("核心业务...mul(int i, int j),i="+i+",j="+j);
        return i*j;
    }

    @Override
    public int div(int i, int j) {
        System.out.println("核心业务...div(int i, int j),i="+i+",j="+j);
        return i/j;
    }
}
