package com.igeek.ch04.transcation.ch27.config.dao;

import com.igeek.ch04.transcation.ch27.config.exception.AccountException;
import com.igeek.ch04.transcation.ch27.config.exception.BookStockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookShopDaoImpl implements IBookShopDao {

	@Autowired(required = false)
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int selectPrice(int bookId) {
		String sql = "select price from t_book where bookId = ?";
		Integer price = jdbcTemplate.queryForObject(sql, Integer.class, bookId);
		return price;
	}

	@Override
	public void updateStock(int bookId) {
		String s = "select stock from t_stock where bookId=?";
		int stock = jdbcTemplate.queryForObject(s, Integer.class, bookId);
		if(stock<=0){
			throw new BookStockException("库存不足!!!");
		}
		
		String sql = "update t_stock set stock=stock-1 where bookId=?";
		jdbcTemplate.update(sql, bookId);
	}

	@Override
	public void updateBalance(String username, int price) throws AccountException {
		String s = "select balance from t_account where username=?";
		Integer balance = jdbcTemplate.queryForObject(s, Integer.class, username);
		if(balance<price){
			throw new AccountException("账户余额不足!!!");
		}
		
		String sql = "update t_account set balance=balance-? where username=?";
		jdbcTemplate.update(sql, price,username);
	}

}
