package com.igeek.ch04.transcation.ch27.config;

import com.igeek.ch04.transcation.ch27.config.exception.AccountException;
import com.igeek.ch04.transcation.ch27.config.service.IBookShopService;
import com.igeek.ch04.transcation.ch27.config.service.ICashierService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.Arrays;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/16 14:49
 */
public class MainTest {
    public static void main(String[] args) throws IOException, AccountException {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        IBookShopService bookShopService =
                ac.getBean("bookShopServiceImpl", IBookShopService.class);

        //未添加事务：com.igeek.ch04.transcation.ch27.config.service.BookShopServiceImpl
        //添加事务：com.sun.proxy.$Proxy22 ==> jdk动态代理

        System.out.println(bookShopService.getClass().getName());
        //购买 张三 余额不足
       // bookShopService.purchase("张三",1);

        System.out.println("-----------------------------");
        //结算 张三120 购买1 2号 （100+80）  ==>余额不足
        ICashierService cashierService =
                ac.getBean("cashierServiceImpl", ICashierService.class);
        //com.sun.proxy.$Proxy22
        System.out.println(cashierService.getClass().getName());
        cashierService.cash("张三", Arrays.asList(1,2));

    }
}
