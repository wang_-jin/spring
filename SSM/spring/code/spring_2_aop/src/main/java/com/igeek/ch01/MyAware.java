package com.igeek.ch01;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 11:43
 */
@Component
public class MyAware implements BeanPostProcessor {
    /**
     * 初始化后执行的 ：业务逻辑+AOP
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization: bean="+bean+",beanName="+beanName);
        if(bean instanceof ICount){
            //创建代理对象(基于ICount接口的实现类)

            /**
             * ClassLoader loader, 类加载器
             * Class<?>[] interfaces, bean实例的实现的所有接口
             * InvocationHandler h  执行器
             */
            ICount proxy = (ICount)Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    new InvocationHandler() {
                        /**
                         * bean中的每个方法都需要经过的执行器
                         * @param proxy 代理对象
                         * @param method 目标方法 add sub...
                         * @param args 目标方法的形参 i=1,j=2
                         * @return 目标方法的执行结果  3
                         * @throws Throwable
                         */
                        @Override
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            //前置通知：开始日志追踪
                            System.out.println("开始日志追踪 method=" + method.getName() + ",args=" + Arrays.toString(args));
                            Object result = null;
                            try {
                                //执行目标方法--》  将方法调用转到原始对象上.
                                result = method.invoke(bean, args);
                                //返回通知：输出方法的执行结果
                                System.out.println("返回通知 method的执行结果=" + result);
                            } catch (Exception e) {
                                //异常通知
                                System.out.println("异常通知 method的异常信息=" + e);
                            }
                            //后置通知：结束日志追踪
                            System.out.println("结束日志追踪 method=" + method.getName() + ",result=" + result);
                            return result;
                        }
                    }
            );
            return proxy;
        }else {
            //原始对象
            return bean;
        }
    }
}
