package com.igeek.ch02.log;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 14:57
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        ICount count = ac.getBean("count", ICount.class);
        String name = count.getClass().getName();
        // com.sun.proxy.$Proxy16 ==》 jdk动态代理
        System.out.println("count的实际类型 = " + name);

        System.out.println("-------------------------");
        count.add(1,2);

        System.out.println("-------------------------");
        count.mul(2,3);

        System.out.println("-------------------------");
        count.add(1.0,2.0);

        System.out.println("-------------------------");
        count.div(1,0);
    }
}
