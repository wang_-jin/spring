package com.igeek.ch01;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 11:39
 */
@ComponentScan("com.igeek.ch01")
public class MyConfig {
}
