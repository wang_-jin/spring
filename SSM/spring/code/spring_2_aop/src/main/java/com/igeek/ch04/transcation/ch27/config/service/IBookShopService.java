package com.igeek.ch04.transcation.ch27.config.service;




import com.igeek.ch04.transcation.ch27.config.exception.AccountException;

import java.io.IOException;

public interface IBookShopService {

	//通过账户名及书号购买书
	public void purchase(String username, int bookId) throws IOException, AccountException;
}
