package com.igeek.ch04.transcation.ch27.config.exception;

//自定义异常 必须 extends  RuntimeException 一旦发生运行时异常 则事务正常回滚
//自定义异常 必须 extends  Exception   事务无法回滚
public class AccountException extends RuntimeException {

	public AccountException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AccountException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AccountException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AccountException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}

