package com.igeek.ch05.spring5.junit;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/18 10:24
 */
//复合注解:1.支持单元测试  2.加载指定的配置类创建IOC容器
//要使用复合注解 必须要添加spring-test.jar包
//@SpringJUnitConfig(locations = "springbean.xml")根据指定的类路径的配置文件 创建IOC容器
//@SpringJUnitConfig(MyConfig.class) 指定的配置类创建IOC容器

@SpringJUnitConfig(MyConfig.class)
public class Junit5Test {
    //3.通过@Autowired 代替 ac.getBean() 获取匹配的实例
    @Autowired
    private User user;

    @Test
    public void printUser(){
        System.out.println("user = " + user);
    }

}
