package com.igeek.test;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/11/15 19:51
 * @description TODO
 */
@Component
@Aspect
@Order(2)
public class LogAspect {
    @Pointcut("execution( * com.igeek.test.ICount.*(..))")
    public void myPointCut(){}

    @Around("myPointCut()")
    public Object logAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();
        Object result = null;
        try {
            result = joinPoint.proceed(args);
            System.out.println("@Around 获取执行结果 method=" + methodName + ",result=" + result);
        } catch (Throwable e) {
            System.out.println("@Around 执行方法出现异常 method=" + methodName + ",e=" + e.getMessage());
            e.printStackTrace();
            throw e;
        }
        System.out.println("@Around 日志结束追踪 method=" + methodName + ",args=" + Arrays.toString(args));
        return result;
    }
}
