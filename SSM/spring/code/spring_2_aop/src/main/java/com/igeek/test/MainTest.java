package com.igeek.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/15 19:52
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        ICount count = ac.getBean("count", ICount.class);
        int add = count.add(1, 2);
        System.out.println("add = " + add);
        int sub = count.sub(1, 2);
        System.out.println("sub = " + sub);
        int div = count.div(6, -12);
        System.out.println("div = " + div);
    }
}
