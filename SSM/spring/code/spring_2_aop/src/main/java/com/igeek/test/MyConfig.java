package com.igeek.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author wangjin
 * 2023/11/15 19:51
 * @description TODO
 */
@ComponentScan("com.igeek.test")
@EnableAspectJAutoProxy
public class MyConfig {
}