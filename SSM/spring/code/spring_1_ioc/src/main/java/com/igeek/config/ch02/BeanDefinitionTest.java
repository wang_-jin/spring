package com.igeek.config.ch02;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/13 15:51
 * @description TODO
 */
public class BeanDefinitionTest {
    public static void main(String[] args) {
        //1.获取IOC容器
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();

        //2.通过BeanDefinition 直接注册bean的定义 到IOC容器中
        //2.1构建beanDefinition的bean定义对象
        AbstractBeanDefinition beanDefinition =
                BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        //2.2 设置类的类型
        beanDefinition.setBeanClass(Employee.class);
        //2.3 设置类的属性信息 作用域 singleton单例   prototype原型(非单例)
        beanDefinition.setScope("singleton");
        //2.4 将beanDefinition放入到IOC容器中ac.registerBeanDefinition(唯一标识,bean定义对象)
        ac.registerBeanDefinition("emp",beanDefinition);
        //2.5 手动刷新
        ac.refresh();

        //3.获取实例bean
        Employee emp = ac.getBean("emp", Employee.class);
        emp.setName("小红");
        emp.setSalary(10000.0);
        //4.使用
        System.out.println("emp = " + emp);
    }

}
