package com.igeek.config.ch03.dl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @author wangjin
 * 2023/11/14 11:37
 * @description 控制层（view 前端和  后端服务的交换）
 *
 * @Controller 作用等同于Component
 * @Controller 没有指定beanName 默认以类名首字母小写作为beanName userController
 * @Controller("controller") 以指定的值作为beanName   controller
 */
@Controller("controller")
public class UserController {
    //自动装配：默认通过数据类型 在 ioc容器中查找匹配的实例bean 并且注入
    // 若找不到匹配的实例 则 抛出异常NoSuchBeanDefinitionException
    @Autowired
    private UserService userService;

    //get请求 ：http://locahost:8899/login?name=xxx&password=xxx
    public void login(){
        //执行业务
        boolean result = userService.login("111", "222");
        System.out.println("UserController.login():用户登录");
        //将业务的处理结果以json数据反馈给客户端 {"code":"ok","name":"xxx","message":"xxx"}
    }
}

