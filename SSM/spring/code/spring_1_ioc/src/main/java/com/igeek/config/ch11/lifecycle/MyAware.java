package com.igeek.config.ch11.lifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 16:49
 *
 * InstantiationAwareBeanPostProcessor extends BeanPostProcessor
 * 子接口：提供的方法可以 干涉 实例化前后 以及  初始化前后
 */
@Component
public class MyAware implements InstantiationAwareBeanPostProcessor {
    /**
     *  实例化之前执行
     * @param beanClass  类类型
     * @param beanName   唯一标识
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println("实例化之前执行 beanClass="+beanClass+",beanName="+beanName);
        return null; //继续进行默认实例化

    }

    /**
     * 实例化之后执行
     * @param bean 实例化完成后的bean
     * @param beanName  唯一标识
     * @return  true正常返回实例 false不正常返回实例
     * @throws BeansException
     */
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        System.out.println("实例化之后执行 bean="+bean+",beanName="+beanName);
        //return false 不会执行后续的其他的InstantiationAwareBeanPostProcessor（包含@Value注解的处理器）
        return true;
    }

    /**
     * 初始化之前执行
     * @param bean  实例化完成后的bean
     * @param beanName  唯一标识
     * @return   处理后的bean   返回null不做任何处理
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("初始化之前执行 bean="+bean+",beanName="+beanName);
        //return null
        // 1.不会执行后续的其他的BeanPostProcessors
        // 2.不执行@PostCustruct 注解的方法
        return bean;
    }

    /**
     * 初始化之后执行
     * @param bean  实例化完成后的bean
     * @param beanName  唯一标识
     * @return  处理后的bean   返回null不做任何处理(不会执行后续的其他的BeanPostProcessors)
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("初始化之后执行 bean="+bean+",beanName="+beanName);
        //return null 1.不会执行后续的其他的BeanPostProcessors
        return bean;
    }
}
