package com.igeek.config.ch02.test;

/**
 * @author wangjin
 * 2023/11/13 19:50
 * @description TODO
 */
public class Employee {
    private String username;
    private int age;

    public Employee() {
    }

    public Employee(String username, int age) {
        this.username = username;
        this.age = age;
    }

    /**
     * 获取
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Employee{username = " + username + ", age = " + age + "}";
    }
}
