package com.igeek.config.ch10.constructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:21
 */
@Component
public class Dog {
    @Value("金毛")
    private String name;

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
