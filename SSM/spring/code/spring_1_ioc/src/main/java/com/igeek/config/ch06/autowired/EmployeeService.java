package com.igeek.config.ch06.autowired;

import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 14:50
 */
@Service
public class EmployeeService implements IService<EmployeeService>{
    @Override
    public void register() {
        System.out.println("EmployeeService.register");
    }
}
