package com.igeek.config.ch09.InstantiationAwareBeanPostProcessor;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 16:49
 *
 * InstantiationAwareBeanPostProcessor extends BeanPostProcessor
 * 子接口：提供的方法可以 干涉 实例化前后 以及  初始化前后
 */
@Component
public class MyAware implements InstantiationAwareBeanPostProcessor {
    /**
     *  实例化之前执行
     * @param beanClass  类类型
     * @param beanName   唯一标识
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if (beanClass==User.class){
            System.out.println("postProcessBeforeInstantiation User实例的实例化之前~~");
        }
        return null;
    }

    /**
     * 实例化之后执行
     * @param bean 实例化完成后的bean
     * @param beanName  唯一标识
     * @return  true正常返回实例 false不返回实例
     * @throws BeansException
     */
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        //作业1：判断实例是否是user类 解析@MyValue注解 来完成属性的赋值
        if (bean instanceof User){
            //2.获取该类型上所有的属性
            Class<?> klass = bean.getClass();
            Field[] fields = klass.getDeclaredFields();

            //3.获取属性上是否有@MyValue注解 获取此注解的beanName
            Stream.of(fields).
                    filter(field -> field.isAnnotationPresent(MyValue.class))
                    .forEach(field->{
                        String psw = field.getDeclaredAnnotation(MyValue.class).value();
                        //4.通过beanName 从IOC容器中 获取 相关的实例bean
                        //Object obj = BeanUtils.getBean(name);
                        //5.通过反射 给当前属性赋值(破坏封装)
                        try {
                            field.setAccessible(true);
                            field.set(bean,psw);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    });
            //6.最后返回的是 修改后的bean
            return true;
        }
        //相当于不做任何处理 返回的还是 之前的实例bean
        return false;
    }

    /**
     * 初始化之前执行
     * @param bean  实例化完成后的bean
     * @param beanName  唯一标识
     * @return   处理后的bean   返回null不做任何处理
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }

    /**
     * 初始化之后执行
     * @param bean  实例化完成后的bean
     * @param beanName  唯一标识
     * @return  处理后的bean   返回null不做任何处理
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        //作业2：对所有的bean都实现 功能增强：添加日志追踪 ==> AOP
        //JDK动态代理

        return null;
    }
}
