package com.igeek.config.ch02.test;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/13 20:03
 * @description TODO
 */
@Component("emp2")
public class MyFactoryBean implements FactoryBean<Employee> {
    @Override
    public Employee getObject() throws Exception {
        return new Employee();
    }

    @Override
    public Class<?> getObjectType() {
        return Employee.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
