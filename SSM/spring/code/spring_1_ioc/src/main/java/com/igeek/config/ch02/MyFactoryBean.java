package com.igeek.config.ch02;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/13 16:31
 * @description FactoryBean接口
 *
 * FactoryBean接口：
 * 使用方式：实现FactoryBean接口 再添加@Component注解
 * 1.Object getObject() 返回注册在IOC容器中的实例
 * 2.Class<?> getObjectType()返 回注册在IOC容器中的实例的类类型
 * 3.boolean isSingleton() 是否是单例 true代表单例
 *
 * 简单工厂设计模式：
 * BeanFactory接口：通过beanName获取实例bean 根据beanName返回某个类的实例
 * @Bean定义的Bean是会经过完整的Bean生命周期的。
 *
 *
 * 工厂方法设计模式： DaoFactory ServiceFactory
 * FactoryBean接口 ：只负责某个对象的实例化  通过getObject()方法返回该对象
 * 通过这种方式创造出来的Bean，只会经过初始化后，其他Spring的生命周期步骤是不会经过的，比如依赖注入。
 */
//@Component("指定bean-id")
@Component("emp2")
public class MyFactoryBean implements FactoryBean<Employee> {
    @Override
    public Employee getObject() throws Exception {
        return new Employee();
    }

    @Override
    public Class<?> getObjectType() {
        return MyFactoryBean.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
