package com.igeek.config.ch04.datasource;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author wangjin
 * 2023/11/14 11:36
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) throws SQLException {
        //创建IOC容器
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(DataSourceConfig.class);
        //获取数据源的实例
        DataSource dataSource = ac.getBean("dataSource", DataSource.class);
        //获取连接对象
        Connection connection = dataSource.getConnection();

        //执行sql
        String sql = "select name from girl where id=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1,1);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            String name = rs.getString(1);
            System.out.println("name = " + name);
        }
        rs.close();
        ps.close();
        connection.close();

    }
}
