package com.igeek.config.ch05.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description 业务逻辑类
 * @Author fqq
 * @Date 2023/11/14 10:18
 *
 * @Service 作用等同于Component
 */
@Service
public class UserService {
    @Autowired(required = false)
    private UserDao userDao;

    //登录
    public boolean login(String name,String pwd){
        userDao.selectOne(name, pwd);
        System.out.println("UserService.login():用户登录业务逻辑的实现");
        return true;
    }
}
