package com.igeek.config.ch08.initAndDestory;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author wangjin
 * 2023/11/14 16:28
 * @description TODO
 *
 * 指定初始化方法和销毁方法：
 * 第一种方式：搭配@Component
 *  1.implements InitializingBean, DisposableBean
 *  2.重写afterPropertiesSet()  destroy()方法
 *
 *  第二种方式： 搭配@Component  （最常用）
 *  1.添加注解@PostConstruct 标记 初始化方法
 *  2.添加注解@PreDestroy 标记 销毁方法
 *
 *  第三种：搭配@Bean
 *  @Bean(name = "cc",initMethod = "initMeth",destroyMethod = "destroyMeth")
 *
 *  注意：允许同时出现三种情况
 *  执行顺序：注解方式>接口方式>@Bean方式  一般只会选择一种来使用
 */
//@Component
public class Cat /*implements  InitializingBean, DisposableBean*/ {
    @Value("加菲猫")
    private String  name;

   /* //第一种方式:
    //实现的是InitializingBean接口 初始化方法
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("afterPropertiesSet() 初始化");
    }

    //实现的是DisposableBean接口 销毁方法 (IOC容器销毁close时会销毁bean实例)
    @Override
    public void destroy() throws Exception {
        System.out.println("destroy() 销毁");
    }*/

    /*//第二种方式:
    //初始化方法
    @PostConstruct
    public void init1(){
        System.out.println(" @PostConstruct init1() 初始化");
    }

    //销毁方法
    @PreDestroy
    public void destroy2(){
        System.out.println("@PreDestroy destroy2() 销毁");
    }*/

    public void initMeth(){
        System.out.println("initMeth() 初始化");
    }
    public void destroyMeth(){
        System.out.println("destroyMeth() 销毁");
    }

    public Cat() {
        System.out.println("Cat.Cat()");
    }

    public Cat(String name) {
        System.out.println("Cat.Cat(String name)");
        this.name = name;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Cat{name = " + name + "}";
    }



}

