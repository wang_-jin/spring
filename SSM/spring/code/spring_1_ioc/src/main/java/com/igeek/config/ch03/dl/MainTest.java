package com.igeek.config.ch03.dl;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/14 11:36
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        //1.创建IOC容器
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        //获取实例userController
        UserController userController = ac.getBean("controller", UserController.class);
        userController.login();

        System.out.println("---------测试User实例的作用域-----------");
        User u1 = ac.getBean("u", User.class);
        System.out.println("u1 = " + u1);
        System.out.println("u1.hashCode() = " + u1.hashCode());
        User u2 = ac.getBean("u", User.class);
        System.out.println("u2 = " + u2);
        System.out.println("u2.hashCode() = " + u2.hashCode());
    }
}
