package com.igeek.config.ch11.lifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:35
 */
@Component
public class User implements InitializingBean, DisposableBean {
    @Value("张三")
    private String name;

    @PostConstruct //早于 初始化步骤 之前执行
    public  void initUser(){
        System.out.println("@PostConstruct  initUser()");
    }
    @PreDestroy
    public void destroyUser(){
        System.out.println("@PreDestroy destroyUser()");
    }
    //初始化步骤
    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean接口的方法 destroy()");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean接口的方法 afterPropertiesSet()");
    }


    public User() {
        System.out.println("实例化 执行构造方法User()");
    }

    public User(String name) {
        System.out.println("实例化 执行构造方法User(String name)");
        this.name = name;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        System.out.println("给属性赋值 setName(String name)");
        this.name = name;
    }

    public String toString() {
        return "User{name = " + name + "}";
    }


}
