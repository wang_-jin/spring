package com.igeek.config.ch05.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Description 数据访问
 * @Author fqq
 * @Date 2023/11/14 10:19
 *
 * @Repository 作用等同于Component
 */
@Repository
public class UserDao {
    @Autowired(required = false)
    private User user;

    public User selectOne(String name, String pwd){
        System.out.println("UserDao.selectOne():查询用户信息,user="+user);
        return user;
    }
}
