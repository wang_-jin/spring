package com.igeek.config.ch07.BeanPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 16:00
 */
@Component
public class BeanUtils implements ApplicationContextAware {
    //保存 当前IOC容器
    private static  ApplicationContext ac;
    /**
     *  可以创建IOC容器之后 自动回调当前方法
     * @param applicationContext   创建的IOC容器
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("容器已经创建了！！！");
        this.ac = applicationContext;
    }

    //可以从ac容器中 根据beanName获取实例
    public static Object getBean(String beanName) {
        Object bean = null;
        if (beanName != null && !beanName.equals("")) {
            bean = ac.getBean(beanName);
        }
        return bean;
    }
}
