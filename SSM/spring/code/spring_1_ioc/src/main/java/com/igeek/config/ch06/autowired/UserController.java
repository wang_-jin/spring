package com.igeek.config.ch06.autowired;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 14:49
 */
@Controller
public class UserController {
    //第一种方式:
    //自动装配：先根据属性类型去找Bean，如果找到多个再根据beanName(默认以属性名作为beanName)确定一个
    //如果找到多个并且无法选择一个使用 抛出异常NoUniqueBeanDefinitionException
    //@Autowired
    //private IService userService;
    //private IService employeeService;

    //第二种方式：通过泛型来确定具体的匹配类型
    //@Autowired
    //private IService<UserService> service;

    //第三种方式：Autowired+@Qualifier：根据名称进行注入   @Qualifier("beanName")根据beanName进行注入
    //@Autowired
    //@Qualifier("employeeService")
    //private IService service;

    //第四种方式：@Resource：可以根据类型注入，可以根据名称注入
    //@Resource//默认根据类型来注入
    //@Resource(type = UserService.class)  //根据指定类型来注入
    @Resource(name="employeeService") //根据名称进行注入
    private IService service;

    public void register(){
        service.register();
        System.out.println("UserController.register");
    }
}
