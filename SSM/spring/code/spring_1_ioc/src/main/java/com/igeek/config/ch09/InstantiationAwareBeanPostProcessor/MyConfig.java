package com.igeek.config.ch09.InstantiationAwareBeanPostProcessor;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 15:20
 */
@ComponentScan("com.igeek.config.ch09.InstantiationAwareBeanPostProcessor")
public class MyConfig {
}
