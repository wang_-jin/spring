package com.igeek.config.ch10.constructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description 推断构造方法
 * @Author fqq
 * @Date 2023/11/15 10:12
 *
 * 推断构造方法(在IOC容器中 如何选择适合的构造方法来进行实例化)
 * 1.有提供无参构造方法，则一定优先选择无参构造方法  Person()
 * 2.没有提供无参构造方法，则必须有构造方法标记为@Autowired 否则直接抛出异常
 * 3.没有提供无参构造方法，并且所有构造方法标记为@Autowired则会直接抛出异常， 建议搭配(required = false)一起使用
 * 4.没有提供无参构造方法，并且所有构造方法标记为@Autowired(required = false)，则优先选择参数最多的构造方法
 * 5.没有提供无参构造方法，并且所有构造方法标记为@Autowired(required = false)，并且参数个数一样的情况下，则会选择最后一个
 */
@Component
public class Person {

   /* public Person() {
        System.out.println("person() 无参构造方法");
    }*/

    @Autowired(required = false)
    public Person(Car car) {
        System.out.println("person(Car car) 1个参数的构造方法,car="+car);
    }
    @Autowired(required = false)
    public Person(Car car1,Car car2) {
        System.out.println("person(Car car1,Car car2) 2个参数的构造方法,car1="+car1+",car2="+car2);
    }
    @Autowired(required = false)
    public Person(Dog dog1,Dog dog2,Dog dog3) {
        System.out.println("person(Dog dog1,Dog dog2,Dog dog3) 3个参数的构造方法" +
                ",dog1="+dog1+",dog2="+dog2+",dog3="+dog3);
    }
    @Autowired(required = false)
    public Person(Car car1,Car car2,Car car3) {
        System.out.println("person(Car car1,Car car2,Car car3) 3个参数的构造方法" +
                ",car1="+car1+",car2="+car2+",car3="+car3);
    }

}
