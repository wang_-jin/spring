package com.igeek.config.ch09.InstantiationAwareBeanPostProcessor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//元注解
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyValue {
    String value() default "";//value属性表示要 注入的bean的名字
}
