package com.igeek.config.ch07.BeanPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * @Description BeanPostProcessor  bean的后置处理器，允许配置一个或者多个
 * @Author fqq
 * @Date 2023/11/14 15:14
 *
 * BeanPostProcessor接口：提供在bean的初始化之前 初始化之后执行一些自定义逻辑
 *
 * 前提条件：如果想要自定义后置处理器能够起作用 记得添加@Component
 */
@Component
public class MyAware implements BeanPostProcessor {
    /**
     *  bean的初始化之前
     * @param bean  实例化bean
     * @param beanName 实例bean在IOC容器中的唯一标识
     * @return 实际上返回的bean
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof Dog) {
            System.out.println("postProcessBeforeInitialization Dog实例的初始化之前~~");
            ((Dog) bean).setName("柯基");
            return bean;
        }
        //相当于不做任何处理 返回的还是 之前的实例bean
        return null;
    }

    /**
     * bean的初始化之后
     * @param bean 实例化bean
     * @param beanName 实例bean在IOC容器中的唯一标识
     * @return  实际上返回的bean
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        //1.判断实例bean的类型 Tiger
        if(bean instanceof Tiger) {
            //2.获取该类型上所有的属性
            Class<?> klass = bean.getClass();
            Field[] fields = klass.getDeclaredFields();

            //3.获取属性上是否有@MyAuto注解 获取此注解的beanName
            Stream.of(fields).
                    filter(field -> field.isAnnotationPresent(MyAuto.class))
                    .forEach(field->{
                        String name = field.getDeclaredAnnotation(MyAuto.class).value();
                        //4.通过beanName 从IOC容器中 获取 相关的实例bean
                        Object obj = BeanUtils.getBean(name);
                        //5.通过反射 给当前属性赋值(破坏封装)
                        try {
                            field.setAccessible(true);
                            field.set(bean,obj);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    });
            //6.最后返回的是 修改后的bean
            System.out.println("初始化之后：bean="+bean+",beanName="+beanName);
            return bean;
        }
        //相当于不做任何处理 返回的还是 之前的实例bean
        return null;
    }
}
