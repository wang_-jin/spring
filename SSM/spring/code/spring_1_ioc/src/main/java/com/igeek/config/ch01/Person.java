package com.igeek.config.ch01;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/13 15:09
 * @description TODO
 */
//@Component 组件注解 直接将当前实例加入到IOC容器中 默认使用类名首字母小写作为唯一标识 例如：person
//@Component
public class Person {
    //对该属性注入值
    @Value("李四")
    private String username;
    @Value("22")
    private String password;


    public Person() {
    }

    public Person(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * 获取
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "Person{username = " + username + ", password = " + password + "}";
    }
}
