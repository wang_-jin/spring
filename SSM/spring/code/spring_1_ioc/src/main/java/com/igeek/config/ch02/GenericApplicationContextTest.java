package com.igeek.config.ch02;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/14 11:37
 * @description GenericApplicationContext函数式风格
 *
 *GenericApplicationContext函数式风格：
 * GenericApplicationContext+Supplier<T> 供给型接口
 * 通过Supplier<T> 供给型接口的返回值 将返回值注册到IOC容器中
 *
 */
public class GenericApplicationContextTest {
    public static void main(String[] args) {
//1.创建IOC容器
        //AnnotationConfigApplicationContext extends  GenericApplicationContext
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext();
        //2.注册实例bean
        //ac.registerBean(实例的beanid,实例的类类型，存入到ioc中的实例)
        ac.registerBean("emp3",Employee.class,()->new Employee("哈哈哈",10000.0));
        //手动刷新
        ac.refresh();

        //3.获取实例
        Employee emp3 = ac.getBean("emp3", Employee.class);
        System.out.println("emp3 = " + emp3);

    }

}
