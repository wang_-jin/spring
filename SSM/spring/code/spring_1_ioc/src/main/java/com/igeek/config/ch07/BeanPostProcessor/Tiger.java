package com.igeek.config.ch07.BeanPostProcessor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 15:27
 */
@Component
public class Tiger {
    @Value("东北虎")
    private String name;
    @Value("20")
    private int age;

   // @Autowired
    //自定义注解 @MyAuto("要注入的实例的bean名字") 实现的作用等同于@Autowired
    @MyAuto("dog")
    private Dog dog;


    public Tiger() {
    }

    public Tiger(String name, int age, Dog dog) {
        this.name = name;
        this.age = age;
        this.dog = dog;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return dog
     */
    public Dog getDog() {
        return dog;
    }

    /**
     * 设置
     * @param dog
     */
    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public String toString() {
        return "Tiger{name = " + name + ", age = " + age + ", dog = " + dog + "}";
    }
}
