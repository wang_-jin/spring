package com.igeek.config.ch01;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangjin
 * 2023/11/13 15:11
 * @description TODO
 */
//@ComponentScan 配置组件扫描包的路径
//@ComponentScan("com.igeek.config.ch01")
public class MyConfig {

    //config配置类+@Bean  方法名称就是实例的标识id 将当前方法的返回值存放到IOC容器中
    @Bean
    public Person pp(){
        return new Person("丽丽","22");
    }
}
