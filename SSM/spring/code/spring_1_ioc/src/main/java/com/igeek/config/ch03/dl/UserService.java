package com.igeek.config.ch03.dl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangjin
 * 2023/11/14 11:37
 * @description 业务逻辑类
 *
 * @Service 作用等同于Component
 */
@Service
public class UserService {
    @Autowired(required = false)
    private UserDao userDao;

    //登录
    public boolean login(String name,String pwd){
        userDao.selectOne(name, pwd);
        System.out.println("UserService.login():用户登录业务逻辑的实现");
        return true;
    }
}

