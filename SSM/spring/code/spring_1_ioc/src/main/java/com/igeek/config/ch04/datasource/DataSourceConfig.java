package com.igeek.config.ch04.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @author wangjin
 * 2023/11/14 11:36
 * @description 数据源的配置类
 */
//默认类路径下 加载属性文件db.properties
@PropertySource("db.properties")
public class DataSourceConfig {

    //无法使用@Component注解 无法修改源代码
    //@Value可以用在形参列表上的 寻找匹配的值给参数注入值 可以支持spring表达式${db.user} 读取属性文件中的key的值
    @Bean
    public DataSource dataSource(
            @Value("${db.user}") String user,
            @Value("${db.password}") String password,
            @Value("${db.driverClass}") String driverClass,
            @Value("${db.url}") String url) throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setDriverClass(driverClass);
        dataSource.setJdbcUrl(url);
        return dataSource;
    }
}

