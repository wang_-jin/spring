package com.igeek.config.ch11.lifecycle;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:48
 *
 * bean的生命周期：
 * 1.构造BeanDefinition信息：类信息，作用域，是否是抽象类
 * 2.实例化前 -->  InstantiationAwareBeanPostProcessor接口的 postProcessBeforeInstantiation（）方法
 * 3.推断构造方法 --> User() 无参构造方法
 * 4.实例化
 * 5.实例化后 -->  InstantiationAwareBeanPostProcessor接口的 postProcessAfterInstantiation（）方法
 *
 * 6.填充属性
 * 7.初始化之前 --> BeanPostProcessor接口的 postProcessBeforeInitialization（）方法
 * 8.1 @PostConstruct
 * 8.2 初始化：（1） implements InitializingBean接口 搭配@Component
 *            (2)  @Bean指定init-method 属性
 * 9.初始化之后 -->  BeanPostProcessor接口的 postProcessAfterInitialization（）方法
 *
 * 10.获取bean实例并且使用
 *
 * 11.销毁 ac.close()
 * 11.1 @PreDestroy
 * 11.2 (1)implements   DisposableBean接口
 *      （2）@Bean指定destroy-method 属性
 */
public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);

        System.out.println("---------创建IOC容器成功----------");
        User user = ac.getBean("user", User.class);
        System.out.println("user.getName() = " + user.getName());

        System.out.println("--------------开始销毁IOC---------------");
        ac.close();
    }
}
