package com.igeek.config.ch03.dl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author wangjin
 * 2023/11/14 11:37
 * @description 数据访问
 *
 * @Repository 作用等同于Component
 */
@Repository
public class UserDao {
    @Autowired
    private User user;

    public User selectOne(String name,String pwd){
        System.out.println("UserDao.selectOne():查询用户信息,user="+user);
        return user;
    }
}

