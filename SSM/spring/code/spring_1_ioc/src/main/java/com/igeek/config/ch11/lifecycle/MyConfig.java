package com.igeek.config.ch11.lifecycle;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:48
 */
@ComponentScan("com.igeek.config.ch11.lifecycle")
public class MyConfig {
}
