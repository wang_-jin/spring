package com.igeek.config.ch08.initAndDestory;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/14 16:29
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        System.out.println("----------------IOC容器创建成功------------------");
        Cat cat = ac.getBean("cc", Cat.class);
        System.out.println("cat = " + cat);

        System.out.println("----------------IOC容器销毁------------------");
        ac.close();

    }
}

