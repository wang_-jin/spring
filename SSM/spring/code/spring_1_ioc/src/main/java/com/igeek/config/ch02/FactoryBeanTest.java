package com.igeek.config.ch02;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/11/13 16:34
 * @description TODO
 */
@ComponentScan("com.igeek.config.ch02")
public class FactoryBeanTest {
    public static void main(String[] args) {
        //1.创建IOC容器
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(FactoryBeanTest.class);
        //2.获取bean
        String[] beanDefinitionNames = ac.getBeanDefinitionNames();
        System.out.println("beanDefinitionNames = " + Arrays.toString(beanDefinitionNames));
        //找到FactoryBean的信息 myFactoryBean(默认使用 类名首字母小写 作为bean-id)
        //Object bean = ac.getBean("myFactoryBean");
        Object bean = ac.getBean("emp2");
        System.out.println("bean.getClass() = " + bean.getClass());
    }
}
