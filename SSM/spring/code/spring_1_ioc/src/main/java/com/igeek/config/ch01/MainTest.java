package com.igeek.config.ch01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/13 15:13
 * @description config配置类中@@ComponentScan+@Componment标识实例bean
 */
public class MainTest {
    public static void main(String[] args) {
        //1.创建一个IOC容器 指定配置类的类型
        ApplicationContext ac = new AnnotationConfigApplicationContext(MyConfig.class);
        //2.获取person实例--@Component +@ComponentScan
        // Person person = ac.getBean("person", Person.class);
        //3. 使用
//        person.setUsername("张三");
//        person.setPassword("123");
        // System.out.println("person = " + person);


        System.out.println("-------------------------");
        //2.获取person实例--@Bean
        Person pp = ac.getBean("pp", Person.class);
        System.out.println("pp = " + pp);
    }

}

