package com.igeek.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.Date;


/**
 * @author wangjin
 * 2023/11/13 11:50
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        //传统创建对象  主动拥有对象的控制权力
       /* User user = new User();
        user.setUsername("张三");
        user.setAge(20);
        System.out.println("user = " + user);*/

        //通过spring的IOC进行管理 bean --> 对象的控制权力反转
        //1.创建IOC容器（通过加载类路径resources目录下 applicationContext.xml）
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        //2.从IOC容器中获取实例 ac.getBean("bean-id属性值",bean实例类类型)
        User user = ac.getBean("user",User.class);
        //3.使用
//        user.setUsername("李四");
//        user.setAge(22);
        System.out.println("user = " + user);

        Date date1 = ac.getBean("date", Date.class);
        System.out.println("date1 = " +date1);

        Date date2 = ac.getBean("date", Date.class);
        System.out.println("date2 = " + date2);

        //默认IOC容器中都是单例的 true
        System.out.println(date1 == date2);

        System.out.println("-----------获取BeanDefinition-----------");
        //查看当前IOC容器中bean定义 BeanDefinition的情况
        String[] beanDefinitionNames = ac.getBeanDefinitionNames();
        System.out.println("Array.toString(beanDefinitionNames) = " + Arrays.toString(beanDefinitionNames));
        int beanDefinitionCount = ac.getBeanDefinitionCount();
        System.out.println("beanDefinitionCount = " + beanDefinitionCount);
    }

}
