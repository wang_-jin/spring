package com.igeek.config.ch01.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangjin
 * 2023/11/13 19:33
 * @description TODO
 */
@ComponentScan("com.igeek.config.test")
public class MyConfig {
/*    @Bean
    public Person pp(){
        return new Person("李四","333");
    }*/
}
