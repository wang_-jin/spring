package com.igeek.config.ch07.BeanPostProcessor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 15:21
 */
public class MainTest {
    public static void main(String[] args) {
        //创建IOC容器时 已经完成所有bean的定义
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);

        //利用BeanUtils 获取实例
       // Object bean = BeanUtils.getBean("dog");
       // System.out.println("bean = " + bean);

//        String[] beanDefinitionNames = ac.getBeanDefinitionNames();
//        System.out.println("Arrays.toString(beanDefinitionNames) = " +
//                Arrays.toString(beanDefinitionNames));

        System.out.println("-----------------------------------");
        Dog dog = ac.getBean("dog", Dog.class);
        System.out.println("dog = " + dog);

        Tiger tiger = ac.getBean("tiger", Tiger.class);
        System.out.println("tiger = " + tiger);
        System.out.println("tiger.getDog() = " + tiger.getDog());

    }
}
