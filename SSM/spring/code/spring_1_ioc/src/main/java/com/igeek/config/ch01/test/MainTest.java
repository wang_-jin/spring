package com.igeek.config.ch01.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/13 19:36
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        // 创建一个IOC容器 指定配置类的类型
        ApplicationContext ac =  new AnnotationConfigApplicationContext(MyConfig.class);
        Person person = ac.getBean("person", Person.class);
/*        person.setUsername("张三");
        person.setPsw("123");*/
        System.out.println("person = " + person);


        System.out.println("------------------------");
/*        Person pp = ac.getBean("pp", Person.class);
        System.out.println("pp = " + pp);*/
    }
}
