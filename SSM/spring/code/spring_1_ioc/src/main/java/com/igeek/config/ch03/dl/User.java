package com.igeek.config.ch03.dl;

/**
 * @author wangjin
 * 2023/11/14 11:37
 * @description TODO
 */
//@Component 没有添加该注解 不会被注册到IOC容器
//@Scope 定义作用域 默认是单例的  单例 singleton  原型prototype
public class User {
    //@Value("值") 将注解中的值 注入到该属性上（依赖注入）
    //bean的生命周期：1.执行构造方法 2.加入到IOC 3.执行@Value注解
    //@Value("张三")
    private String name;
    //@Value("123")
    private String password;

    public User() {
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "User{name = " + name + ", password = " + password + "}";
    }
}

