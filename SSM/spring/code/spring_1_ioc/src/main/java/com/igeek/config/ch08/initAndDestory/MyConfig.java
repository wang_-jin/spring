package com.igeek.config.ch08.initAndDestory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangjin
 * 2023/11/14 16:29
 * @description TODO
 */
@ComponentScan("com.igeek.config.ch08.initAndDestroy")
public class MyConfig {

    /**
     * @Bean注解：将方法的返回值 注入到IOC容器中
     *      1.name属性：指定实例bean在IOC容器中的beanName 例如 cc  getBean("cc")
     *       若没有指定该属性，则会默认用当前方法名称作为beanName 例如 cat   getBean("cat")
     *      2.initMethod属性：指定初始化方法的名称
     *      3.destroyMethod属性：指定销毁方法的名称
     * @return
     */
    @Bean(name = "cc",initMethod = "initMeth",destroyMethod = "destroyMeth")
    public Cat cat(){
        return new Cat("布偶");
    }
}
