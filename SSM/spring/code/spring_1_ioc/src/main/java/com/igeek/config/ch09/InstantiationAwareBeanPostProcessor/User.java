package com.igeek.config.ch09.InstantiationAwareBeanPostProcessor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 16:55
 */
@Component
public class User {
    @Value("张三")
    private String name;
    //实现自定义注解@MyValue("123") 功能相当于@Value
    @MyValue("123")
    private String password;

    public User() {
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    //在调用eat()之后 能对eat()添加 日志追踪
    ///日志开始追踪：eat()
    ///System.out.println("a = " + a+",b="+b);
    ///日志结束追踪：耗时
    public void eat(int a,int b){
        System.out.println("a = " + a+",b="+b);
    }


    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "User{name = " + name + ", password = " + password + "}";
    }
}
