package com.igeek.config.ch10.constructor;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:08
 */
@ComponentScan("com.igeek.config.ch10.constructor")
public class MyConfig {
}
