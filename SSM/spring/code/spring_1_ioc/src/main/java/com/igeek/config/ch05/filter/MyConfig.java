package com.igeek.config.ch05.filter;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 11:42
 *
 * @ComponentScan注解：
 *  1.basePackages属性：基本扫描包的路径
 *  2.excludeFilters属性：排除过滤器
 *      属性值：Filter[]   @ComponentScan中的内部注解@Filter(@ComponentScan.Filter)
 *      @Filter的属性：
 *          type 排除扫描的类型  （基于注解排除，基于类型排除）
 *          value 排除扫描的内容  (Repository)
 *
 *          不扫描Repository注解：excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = Repository.class)}
 *          不扫描UserService类型：excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = Repository.class)}
 *  3.IncludeFilters属性:包含过滤器
 *          必须搭配useDefaultFilters=false   默认为true表示会一定会扫描到组件注解@Component
 *          只扫描userDao(不包含userService):
 *          includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,value = UserDao.class)}
 */
@ComponentScan(
        basePackages = "com.igeek.config.ch05.filter",
//        excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,value = UserService.class)},
        includeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,value = UserDao.class)},
        useDefaultFilters=false
)
public class MyConfig {
}
