package com.igeek.config.ch06.autowired;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 14:53
 */
@ComponentScan("com.igeek.config.ch06.autowired")
public class MyConfig {
}
