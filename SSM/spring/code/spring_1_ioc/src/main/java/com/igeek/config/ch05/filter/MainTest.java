package com.igeek.config.ch05.filter;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/14 11:43
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
//        UserService userService = ac.getBean("userService", UserService.class);
//        userService.login("1","2");

        UserDao userDao = ac.getBean("userDao", UserDao.class);
        userDao.selectOne("1","2");

    }
}
