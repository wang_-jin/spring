package com.igeek.config.ch10.constructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/15 10:07
 */
@Component
public class Car {
    //@Value() 依赖注入直接 破坏私有属性
    @Value("奥迪")
    private String label;

    @Override
    public String toString() {
        return "Car{" +
                "label='" + label + '\'' +
                '}';
    }
}
