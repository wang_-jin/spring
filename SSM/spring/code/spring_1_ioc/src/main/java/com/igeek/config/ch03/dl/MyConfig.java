package com.igeek.config.ch03.dl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

/**
 * @author wangjin
 * 2023/11/14 11:36
 * @description TODO
 *
 * 基于注解配置Bean:
 *      @Component: 基本注解, 标识了一个受 Spring 管理的组件
 @Repository: 标识持久层dao层组件
 @Service: 标识服务层(业务层)组件
 @Controller: 标识表现层组件
 基于注解配置Bean的属性:
 @Value: 配置的基本数据类型，String等的值 例如 年龄 姓名等
 @Autowired 配置的是实例，例如 userService  userDao
 1.自动装配：默认通过数据类型 在 ioc容器中查找匹配的实例bean 并且注入
 2.若找不到匹配的实例 则 抛出异常NoSuchBeanDefinitionException
 3.@Autowired(required = false)则代表不一定要提供匹配的实例 但是不存在会直接给属性赋值为null 可能会出现NullPointerException


 @ComponentScan注解：
 basePackages属性：就是默认属性value
 1.指定基本扫描包，而且可以指定多个，使用数组来表示
 2.会扫描指定包下的的注解，让其可以注册到IOC 容器中
 可以扫描的注解：@Component  @Repository @Service @Controller
 */
@ComponentScan("com.igeek.config.ch03.dl")
//@ComponentScan(basePackages = {"com.igeek.config.ch03.dl","com.igeek.config.ch02"})
public class MyConfig {
    @Bean
    // @Scope 定义作用域 默认是单例的  单例 singleton  原型prototype
    @Scope("prototype")
    public User u(){
        return new User("李思思","666");
    }
}

