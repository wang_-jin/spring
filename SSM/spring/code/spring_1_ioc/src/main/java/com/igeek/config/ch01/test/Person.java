package com.igeek.config.ch01.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author wangjin
 * 2023/11/13 19:32
 * @description TODO
 */
@Component
public class Person {
    @Value("王五")
    private String username;
    @Value("111")
    private String psw;

    public Person() {
    }

    public Person(String username, String psw) {
        this.username = username;
        this.psw = psw;
    }

    /**
     * 获取
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取
     * @return psw
     */
    public String getPsw() {
        return psw;
    }

    /**
     * 设置
     * @param psw
     */
    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String toString() {
        return "Person{username = " + username + ", psw = " + psw + "}";
    }
}
