package com.igeek.config.ch09.InstantiationAwareBeanPostProcessor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/14 21:23
 * @description TODO
 */
public class MainTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac =
                new AnnotationConfigApplicationContext(MyConfig.class);
        User user = ac.getBean( User.class);
        System.out.println("user = " + user);

    }
}
