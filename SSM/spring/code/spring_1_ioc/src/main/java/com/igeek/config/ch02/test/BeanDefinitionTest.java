package com.igeek.config.ch02.test;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author wangjin
 * 2023/11/13 19:50
 * @description TODO
 */
public class BeanDefinitionTest {
    public static void main(String[] args) {
        //获取IOC容器
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
        //2.通过BeanDefinition 直接注册bean的定义 到IOC容器中
        //2.1 构建BeanDefinition的bean定义对象
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        //2.2设置类的类型
        beanDefinition.setBeanClass(Employee.class);
        //2.3设置类的属性信息 作用域 singleton单例 prototype原型(非单例)
        beanDefinition.setScope("singleton");
        //2.4将beanDefinition放到IOC容器中ac.registerBeanDefinition(唯一标识,bean定义对象)
        ac.registerBeanDefinition("emp",beanDefinition);
        //2.5手动刷新
        ac.refresh();

        //3.获取实例bean
        Employee emp = ac.getBean("emp", Employee.class);
        emp.setUsername("小鹿");
        emp.setAge(20);

        //4.使用
        System.out.println("emp = " + emp);
    }
}
