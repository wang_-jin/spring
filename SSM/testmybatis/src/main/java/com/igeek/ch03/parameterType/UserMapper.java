package com.igeek.ch03.parameterType;

import com.igeek.pojo.User;
import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface UserMapper {
    //根据id查询用户信息
    public User selectUserById(int id);
    //根据姓名和性别查询用户信息
    public List<User> selectAllByQuery(UserCustom userCustom);
    //根据姓名和性别查询用户信息
    public List<UserCustom> selectAllByQueryVO(UserQueryVO vo);
    //根据姓名和性别查询用户信息
    public List<UserCustom> selectAllByHashMap(HashMap<String,Object> map);

    //class文件中没有写入形参名称：public List<UserCustom> selectAllByParam(String arg0,String arg1);
    //class文件中没有写入形参名称：public List<UserCustom> selectAllByParam(String param1,String param2);
    //@Param("key") 括号里的key就是#{key}的内容，作用：可以将占位符#{key}和参数进行映射 实现原理：HashMap
    public List<UserCustom> selectAllByParam(@Param("username") String username, @Param("sex") String sex);

}
