package com.igeek.test.oo;

import com.igeek.pojo.OrderdetailCustom;

public interface OrdersAndOrderdetailsMapper {
    // 作业2：根据订单编号查询 订单明细以及关联查询订单信息
    public OrderdetailCustom selectOrderdetailAndOrderById(int id);
}
