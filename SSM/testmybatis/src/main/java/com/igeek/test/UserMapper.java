package com.igeek.test;

import com.igeek.pojo.User;
import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface UserMapper {
    //根据id查询用户信息
    public User selectUserById(int id);
    //插入用户信息
    public boolean insertUser(UserCustom userCustom);
    //根据id更新用户信息
    public boolean updateUserById(User user);
    //根据id删除用户信息
    public boolean deleteUserById(int id);
    //根据姓名模糊查询用户信息
    public List<User> selectUserByName(String username);
    //根据姓名和性别查询用户信息
    public List<UserCustom> selectUserByQuery(UserCustom userCustom);
    //根据性别查询用户信息
    public List<User> selectUserByQueryVo(UserQueryVO userQueryVO);
    //根据姓名和性别查询用户信息--hashmap
    public List<UserCustom> selectAllByHashMap(HashMap<String,Object> map);
    //根据姓名和性别查询用户信息--params
    public List<UserCustom> selectAllByParams(@Param("username") String username,@Param("sex") String sex);

}
