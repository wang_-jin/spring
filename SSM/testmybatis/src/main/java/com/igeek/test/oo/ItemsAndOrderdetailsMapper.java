package com.igeek.test.oo;

import com.igeek.pojo.OrderdetailCustom;

public interface ItemsAndOrderdetailsMapper {
    //作业1：根据订单明细，关联查询商品信息
    public OrderdetailCustom selectOrderdetailById(int id);
}
