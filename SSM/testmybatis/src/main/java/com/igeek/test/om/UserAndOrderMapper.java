package com.igeek.test.om;

import com.igeek.pojo.UserCustom;

public interface UserAndOrderMapper {
    //作业3：根据用户编号，关联查询用户信息及所下订单信息
    public UserCustom selectUserAndOrderById(int id);
}
