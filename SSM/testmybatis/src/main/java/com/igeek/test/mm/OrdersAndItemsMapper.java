package com.igeek.test.mm;

import com.igeek.pojo.OrdersCustom;

public interface OrdersAndItemsMapper {
    public OrdersCustom selectOrderAndItemById(int id);
}
