package com.igeek.ch05.sqlpart;

import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;

import java.util.List;

public interface UserMapper {

    //1.需求：通过用户性别和姓名查询用户列表 PS：不允许性别或者姓名为null或者空串
    public List<UserCustom> selectAll(UserQueryVO userQueryVO);

    //2.需求：通过用户性别和姓名查询用户列表条数 PS：不允许性别或者姓名为null或者空串
    public int selectCount(UserQueryVO userQueryVO);

    //3.需求：通过用户性别和姓名查询用户列表，且id值是15或20或25  PS：不允许性别或者姓名为null或者空串
    public List<UserCustom> selectAllByIds(UserQueryVO vo);

    //4.需求：根据ids批量删除用户信息
//    public Integer deleteByIds(@Param("ids") int[] ids);
//    public Integer deleteByIds(int... ids);
    public Integer deleteByIds(List<Integer> ids);

    //5.需求：动态修改用户信息
    public boolean updateUserById(UserCustom userCustom);
}
