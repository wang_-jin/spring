package com.igeek.pojo;

import java.util.List;

/**
 * @Description User实体类的扩展类
 * @Author fqq
 * @Date 2023/11/21 15:33
 */
public class UserCustom extends User {

    //一对多关联 订单信息
    private List<OrdersCustom> ordersCustomList;

    public List<OrdersCustom> getOrdersCustomList() {
        return ordersCustomList;
    }

    public void setOrdersCustomList(List<OrdersCustom> ordersCustomList) {
        this.ordersCustomList = ordersCustomList;
    }

    @Override
    public String toString() {
        return "UserCustom{" +
                "ordersCustomList=" + ordersCustomList +
                "} " + super.toString();
    }
}
