package com.igeek.pojo;

/**
 * @Description 思想：封装 综合查询条件
 * @Author fqq
 * @Date 2023/11/21 15:41
 */
public class UserQueryVO {
    //根据姓名和性别模糊查询 --> 来自于用户信息
    private UserCustom userCustom;

    //根据一组id相关的信息
    private int[] ids;
    //订单作为条件
    //商品作为条件

    public UserCustom getUserCustom() {
        return userCustom;
    }

    public void setUserCustom(UserCustom userCustom) {
        this.userCustom = userCustom;
    }

    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }
}
