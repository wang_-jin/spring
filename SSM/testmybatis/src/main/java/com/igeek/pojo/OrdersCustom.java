package com.igeek.pojo;

import java.util.Date;
import java.util.List;

/**
 * @Description Orders的扩展类
 * @Author fqq
 * @Date 2023/11/21 15:39
 */
public class OrdersCustom extends Orders{

    //一对一 用户信息
    private UserCustom userCustom;
    //一对多 订单明细
    private List<OrderdetailCustom> orderdetails;

    public UserCustom getUserCustom() {
        return userCustom;
    }

    public void setUserCustom(UserCustom userCustom) {
        this.userCustom = userCustom;
    }

    public List<OrderdetailCustom> getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(List<OrderdetailCustom> orderdetails) {
        this.orderdetails = orderdetails;
    }
}
