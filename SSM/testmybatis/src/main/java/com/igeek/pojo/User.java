package com.igeek.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * po类 实体类  User.java
 * 将来User 由逆向工程直接成功
 * 原则：一般逆向工程生成的pojo类 mapper接口 mapper.xml 不要随意改动
 *      自定义逻辑建议声明在扩展类中，即使以后逆向工程重构的话，也不会影响自定义逻辑
 */
public class User implements Serializable {
	//属性名称和数据库表的字段对应
	private int id;
	private String username;// 用户姓名
	private String sex;// 性别
	private Date birthday;// 生日
	private String address;// 地址

	//一对多关联 订单

	public User(int id, String username, String sex) {
		this.id = id;
		this.username = username;
		this.sex = sex;
	}

	public User() {
	}

	public User(String username, String sex, Date birthday, String address) {
		this.username = username;
		this.sex = sex;
		this.birthday = birthday;
		this.address = address;
	}

	public User(int id, String username, String sex, Date birthday, String address) {
		this.id = id;
		this.username = username;
		this.sex = sex;
		this.birthday = birthday;
		this.address = address;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", sex=" + sex
				+ ", birthday=" + birthday + ", address=" + address + "]";
	}
}
