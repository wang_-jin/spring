package com.igeek.ch06.mm;

import com.igeek.pojo.UserCustom;

public interface UserAndItemsMapper {

    //根据用户编号 查询用户购买的商品信息
    public UserCustom selectUserAndItemsByUid(int uid);
}
