package com.igeek.ch06.oo;

import com.igeek.pojo.OrdersCustom;

public interface OrdersAndUserMapper {

    //根据订单编号查询订单信息以及下单者的用户信息
    public OrdersCustom selectOrdersAndUserByOid(int oid);

}
