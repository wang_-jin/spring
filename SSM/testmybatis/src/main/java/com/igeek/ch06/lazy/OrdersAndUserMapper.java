package com.igeek.ch06.lazy;

import com.igeek.pojo.OrdersCustom;
import com.igeek.pojo.UserCustom;

import java.util.List;

public interface OrdersAndUserMapper {
    //查询所有订单信息
    public List<OrdersCustom> findOrders();

    //按需查询：根据用户id 查询用户信息
    public UserCustom selectUserById(int id);
}
