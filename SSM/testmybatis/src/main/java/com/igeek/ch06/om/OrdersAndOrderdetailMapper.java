package com.igeek.ch06.om;

import com.igeek.pojo.OrdersCustom;

public interface OrdersAndOrderdetailMapper {

    //根据订单编号 查询订单及订单明细的信息
    public OrdersCustom selectOrdersAndOrderdetailByOid(int oid);
}
