package com.igeek.ch04.resultType;

import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;

public interface UserMapper {

    //通过性别和姓名模糊查询用户列表的总记录数
    public int selectUserByQueryCounts(UserQueryVO userQueryVO);
    //通过id查询用户列表
    public UserCustom selectUserCustomById(int id);

}
