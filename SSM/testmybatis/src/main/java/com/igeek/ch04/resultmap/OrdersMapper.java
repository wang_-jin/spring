package com.igeek.ch04.resultmap;

import com.igeek.pojo.OrdersCustom;

/**
 * @author wangjin
 * 2023/11/22 10:22
 * @description TODO
 */
public interface OrdersMapper {
    //通过通过订单编号 查询订单信息
    public OrdersCustom selectOneById(int id);
}
