package com.igeek.ch04.resultmap;

import com.igeek.pojo.UserCustom;

public interface UserMapper {

    //通过id查询用户列表
    public UserCustom selectUserCustomById(int id);
}
