package com.igeek.ch05.sqlpart;

import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectAll() {
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        UserQueryVO userQueryVO = new UserQueryVO();
        UserCustom userCustom = new UserCustom();
        //封装条件1：select * from user where username like concat('%',?,'%') and sex=?
        //userCustom.setUsername("张");
        //userCustom.setSex("1");

        //封装条件2：select * from user where username like concat('%',?,'%')
        //userCustom.setUsername("张");

        //封装条件3：select * from user where sex=?
        //userCustom.setSex("1");

        //封装条件4：select * from user

        userQueryVO.setUserCustom(userCustom);
        List<UserCustom> userCustoms = mapper.selectAll(userQueryVO);
        System.out.println("userCustoms = " + userCustoms);
        session.close();
    }

    @Test
    public void selectCount() {
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        UserQueryVO userQueryVO = new UserQueryVO();
        UserCustom userCustom = new UserCustom();
        //封装条件1：select count(*) from user where username like concat('%',?,'%') and sex=?
        //userCustom.setUsername("张");
        //userCustom.setSex("1");

        //封装条件2：select count(*) from user where username like concat('%',?,'%')
        //封装条件3：select count(*) from user where sex=?
        userCustom.setSex("1");

        //封装条件4：select count(*) from user

        userQueryVO.setUserCustom(userCustom);
        int count = mapper.selectCount(userQueryVO);
        System.out.println("count = " + count);
        session.close();
    }
    @Test
    public void selectAllByIds(){
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        UserQueryVO userQueryVO = new UserQueryVO();
        UserCustom userCustom = new UserCustom();
        //封装条件1：select * from user where username like concat('%',?,'%') and sex=?
        //userCustom.setUsername("张");
        //userCustom.setSex("1");

        //封装条件2：select * from user where username like concat('%',?,'%') and sex=? and id in(?,?,?)
        //userCustom.setUsername("张");
        //userCustom.setSex("1");
        //userQueryVO.setIds(new int[]{10,16,24});//and id in(?,?,?)
        // userQueryVO.setIds(new int[]{10,16}); //and id in(?,?)

        //封装条件3：select * from user where username like concat('%',?,'%') and id in(?,?,?)
        userCustom.setUsername("张");
        userQueryVO.setIds(new int[]{10,16});

        userQueryVO.setUserCustom(userCustom);
        List<UserCustom> userCustomList = mapper.selectAllByIds(userQueryVO);
        System.out.println("userCustomList.size() = " + userCustomList.size());
        session.close();
    }

    @Test
    public void deleteByIds(){
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

//        Integer count = mapper.deleteByIds(new int[]{26, 27, 30});

        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(29);
        Integer count = mapper.deleteByIds(ids);
        System.out.println("删除成功的行数 count = " + count);
        session.commit();
        session.close();
    }
    @Test
    public void updateUserById(){
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);

        UserCustom userCustom = new UserCustom();
        //update user SET username=?, sex=? where id=?
        userCustom.setUsername("思思33");
        //userCustom.setSex("2");
        userCustom.setId(28);

        boolean flag = mapper.updateUserById(userCustom);
        System.out.println(flag?"更新成功":"更新失败");
        session.commit();
        session.close();
    }

















}