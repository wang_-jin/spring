package com.igeek.ch03.parameterType;

import com.igeek.pojo.User;
import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectUserById() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.selectUserById(1);
        System.out.println("user = " + user);
        sqlSession.close();
    }

    @Test
    public void selectAllByQuery() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        //封装查询条件
        UserCustom userCustom = new UserCustom();
        userCustom.setUsername("张");
        userCustom.setSex("1");
        List<User> userList = mapper.selectAllByQuery(userCustom);
        System.out.println("userList = " + userList);
        sqlSession.close();
    }
    @Test
    public void selectAllByQueryVO(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        //封装查询条件
        UserCustom userCustom = new UserCustom();
        userCustom.setUsername("张");
        userCustom.setSex("1");
        UserQueryVO userQueryVO = new UserQueryVO();
        userQueryVO.setUserCustom(userCustom);
        List<UserCustom> userCustomList = mapper.selectAllByQueryVO(userQueryVO);
        System.out.println("userCustomList = " + userCustomList);
        System.out.println("userCustomList.size() = " + userCustomList.size());
        sqlSession.close();
    }
    @Test
    public void selectAllByHashMap(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        //封装查询条件
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("name","张");
        hashMap.put("gender","1");
        List<UserCustom> userCustomList = mapper.selectAllByHashMap(hashMap);
        System.out.println("userCustomList = " + userCustomList);
        System.out.println("userCustomList.size() = " + userCustomList.size());
        sqlSession.close();
    }
    @Test
    public void selectAllByParam(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<UserCustom> userCustomList = mapper.selectAllByParam("张", "1");
        System.out.println("userCustomList = " + userCustomList);
        System.out.println("userCustomList.size() = " + userCustomList.size());
        sqlSession.close();
    }
}