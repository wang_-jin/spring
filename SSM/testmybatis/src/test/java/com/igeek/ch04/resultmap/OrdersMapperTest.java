package com.igeek.ch04.resultmap;

import com.igeek.pojo.OrdersCustom;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrdersMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectOneById() {
        SqlSession sqlSession = factory.openSession();
        OrdersMapper mapper = sqlSession.getMapper(OrdersMapper.class);
        OrdersCustom ordersCustom = mapper.selectOneById(3);
        System.out.println("ordersCustom = " + ordersCustom);
        sqlSession.close();
    }
}