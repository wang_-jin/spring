package com.igeek.ch04.resultType;

import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectUserByQueryCounts() {
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        UserQueryVO userQueryVO = new UserQueryVO();
        UserCustom userCustom = new UserCustom();
        userCustom.setUsername("张");
        userCustom.setSex("1");
        userQueryVO.setUserCustom(userCustom);
        int count = mapper.selectUserByQueryCounts(userQueryVO);
        System.out.println("count = " + count);
        session.close();
    }

    @Test
    public void selectUserCustomById() {
        SqlSession session = factory.openSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        UserCustom userCustom = mapper.selectUserCustomById(1);
        System.out.println("userCustom = " + userCustom);
        session.close();
    }
}