package com.igeek.ch06.oo;

import com.igeek.pojo.OrdersCustom;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;


public class OrdersAndUserMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectOrdersAndUserByOid() {
        SqlSession session = factory.openSession();
        OrdersAndUserMapper mapper = session.getMapper(OrdersAndUserMapper.class);

        OrdersCustom ordersCustom = mapper.selectOrdersAndUserByOid(3);
        System.out.println("订单信息：= " + ordersCustom);
        System.out.println("用户信息 = " + ordersCustom.getUserCustom());
        session.close();
    }
}