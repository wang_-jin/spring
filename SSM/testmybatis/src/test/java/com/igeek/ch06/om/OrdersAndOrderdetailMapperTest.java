package com.igeek.ch06.om;

import com.igeek.pojo.OrdersCustom;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrdersAndOrderdetailMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectOrdersAndOrderdetailByOid() {
        SqlSession sqlSession = factory.openSession();
        OrdersAndOrderdetailMapper mapper = sqlSession.getMapper(OrdersAndOrderdetailMapper.class);
        OrdersCustom ordersCustom = mapper.selectOrdersAndOrderdetailByOid(3);
        System.out.println("订单信息："+ordersCustom);
        System.out.println("订单明细信息："+ordersCustom.getOrderdetails());
    }
}