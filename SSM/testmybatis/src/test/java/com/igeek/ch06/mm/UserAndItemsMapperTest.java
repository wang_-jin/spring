package com.igeek.ch06.mm;

import com.igeek.pojo.OrderdetailCustom;
import com.igeek.pojo.UserCustom;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class UserAndItemsMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectUserAndItemsByUid() {
        SqlSession session = factory.openSession();
        UserAndItemsMapper mapper = session.getMapper(UserAndItemsMapper.class);
        UserCustom userCustom = mapper.selectUserAndItemsByUid(1);
        System.out.println("用户信息："+userCustom);
        userCustom.getOrdersCustomList().forEach(ordersCustom -> {
            System.out.println("--------订单信息：--------");
            System.out.println(ordersCustom);
            List<OrderdetailCustom> orderdetails = ordersCustom.getOrderdetails();
            orderdetails.forEach(orderdetailCustom -> {
                System.out.println("--------订单明细信息：--------");
                System.out.println(orderdetailCustom);
                System.out.println("商品信息："+orderdetailCustom.getItems());
            });
        });
    }
}