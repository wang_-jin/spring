package com.igeek.ch01.jdbc;

import com.igeek.pojo.User;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class UserDaoTest {
    private UserDao userDao;

    @Before
    public void setUp() throws Exception {
        userDao = new UserDao();
    }

    @Test
    public void selectOne() throws SQLException, ClassNotFoundException {
        User user = userDao.selectOne(1);
        System.out.println("user = " + user);
    }
}