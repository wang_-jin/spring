package com.igeek.test.om;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

public class UserAndOrderMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        factory = new SqlSessionFactoryBuilder().build(is);
    }
    @Test
    public void selectUserAndOrderById() {
        SqlSession sqlSession = factory.openSession();
        UserAndOrderMapper mapper = sqlSession.getMapper(UserAndOrderMapper.class);
        mapper.selectUserAndOrderById(1);
        sqlSession.close();
    }
}