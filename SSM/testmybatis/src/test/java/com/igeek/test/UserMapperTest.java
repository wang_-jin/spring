package com.igeek.test;

import com.igeek.pojo.User;
import com.igeek.pojo.UserCustom;
import com.igeek.pojo.UserQueryVO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class UserMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        factory = new SqlSessionFactoryBuilder().build(is);
    }

    @Test
    public void selectUserById() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.selectUserById(25);
        System.out.println("user = " + user);
    }
    @Test
    public void insertUser(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        UserCustom userCustom = new UserCustom();
        userCustom.setUsername("小红");
        userCustom.setSex("1");
        userCustom.setBirthday(new Date());
        userCustom.setAddress("安徽");
        boolean flag = mapper.insertUser(userCustom);
        System.out.println(flag?"插入成功":"插入失败");
        System.out.println("userCustom = " + userCustom);
        sqlSession.commit();
        sqlSession.close();


    }
    @Test
    public void updateUser(){
        SqlSession sqlSession = factory.openSession();
        com.igeek.ch02.UserMapper mapper = sqlSession.getMapper(com.igeek.ch02.UserMapper.class);
        User user = mapper.selectOneById(25);
        user.setSex("2");
        user.setAddress("北京");
        boolean b = mapper.updateUserById(user);
        System.out.println(b?"更新成功":"更新失败");
        System.out.println("user = " + user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteUser(){
        SqlSession sqlSession = factory.openSession();
        com.igeek.ch02.UserMapper mapper = sqlSession.getMapper(com.igeek.ch02.UserMapper.class);
        boolean flag = mapper.deleteUserById(27);
        System.out.println(flag?"删除成功":"删除失败");
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void selectUserByName() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.selectUserByName("张");
        System.out.println("list = " + list);
        sqlSession.close();
    }

    @Test
    public void selectUserByQuery() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        UserCustom userCustom = new UserCustom();
        userCustom.setUsername("五");
        userCustom.setSex("1");
        List<UserCustom> userCustoms = mapper.selectUserByQuery(userCustom);
        System.out.println("userCustoms = " + userCustoms);
        sqlSession.close();
    }
    @Test
    public void selectUserByQueryVo(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        UserQueryVO userQueryVO = new UserQueryVO();
        UserCustom userCustom = new UserCustom();
        userCustom.setSex("2");
        userQueryVO.setUserCustom(userCustom);
        List<User> users = mapper.selectUserByQueryVo(userQueryVO);
        System.out.println("users = " + users);
        sqlSession.close();
    }

    @Test
    public void selectAllByHashMap(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("name","张");
        hashMap.put("gender","1");
        List<UserCustom> userCustoms = mapper.selectAllByHashMap(hashMap);
        System.out.println("userCustoms = " + userCustoms);
        sqlSession.close();
    }

    @Test
    public void selectAllByParams(){
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<UserCustom> userCustoms = mapper.selectAllByParams("明", "1");
        System.out.println("userCustoms = " + userCustoms);
        sqlSession.close();
    }
}