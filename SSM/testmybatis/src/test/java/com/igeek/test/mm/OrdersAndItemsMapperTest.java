package com.igeek.test.mm;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OrdersAndItemsMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new SqlSessionFactoryBuilder()
                .build(Resources.getResourceAsStream("SqlMapConfig.xml"));
    }

    @Test
    public void selectOrderAndItemById() {
        SqlSession sqlSession = factory.openSession();
        OrdersAndItemsMapper mapper = sqlSession.getMapper(OrdersAndItemsMapper.class);
        mapper.selectOrderAndItemById(3);
        sqlSession.close();
    }
}