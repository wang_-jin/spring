package com.igeek.test.oo;

import com.igeek.pojo.OrderdetailCustom;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.*;

public class ItemsAndOrderdetailsMapperTest {
    private SqlSessionFactory factory;

    @Before
    public void setUp() throws Exception {
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        factory = new SqlSessionFactoryBuilder().build(is);
    }

    @Test
    public void selectOrderdetailById() {
        SqlSession sqlSession = factory.openSession();
        OrdersAndOrderdetailsMapper mapper = sqlSession.getMapper(OrdersAndOrderdetailsMapper.class);
        OrderdetailCustom orderdetailCustom = mapper.selectOrderdetailAndOrderById(2);
        sqlSession.close();

    }
}