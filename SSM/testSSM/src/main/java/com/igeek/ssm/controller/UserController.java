package com.igeek.ssm.controller;

import com.igeek.ssm.pojo.User;
import com.igeek.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/28 16:26
 */
@Controller
public class UserController {
    @Autowired(required = false)
    private UserService userService;

    /**
     * 显示用户列表页面
     * @return
     */
    @GetMapping("/basic_table.html")
    public String getBasicTable(Model model){
        //执行业务
        List<User> userList = userService.findAll();
        model.addAttribute("users",userList);//封装的数据是集合
        //跳转 /templates/basic_table.html
        return "basic_table";
    }

    /**
     * 显示注册表单页面
     * @return
     */
    @GetMapping("/form_layouts.html")
    public String getForm(){
        //跳转到 templates/form_layouts.html
        return "form_layouts";
    }

    /**
     * 处理注册请求的接口
     * @param user
     * @param photo
     * @return
     */
    //localhost:8899/form_layouts.html
    @PostMapping("/register")
    public String register(User user, MultipartFile photo,MultipartFile[] photos,Model model) throws IOException {
        //处理文件上传
        //判断原始文件名是否为空  则不需要处理文件上传
        if(StringUtils.hasLength(photo.getOriginalFilename())){
            //1.上传文件到本地服务器 （复制文件）
            //获取原始文件名 .png
            String fileName = photo.getOriginalFilename();
            System.out.println("fileName="+fileName); //user.png
            //生成上传后的新的文件名（唯一的） 拼接加扩展名  .png
            String newName = UUID.randomUUID()+ fileName.substring(fileName.lastIndexOf("."));
            photo.transferTo(new File("D:\\img\\"+newName));

            //2.设置用户头像路径pic属性的值
            user.setPic("/pic/"+newName);
        }
        for (MultipartFile file : photos) {
            if(StringUtils.hasLength(file.getOriginalFilename())){
                //1.上传文件到本地服务器 （复制文件）
                //获取原始文件名 .png
                String fileName = file.getOriginalFilename();
                System.out.println("fileName="+fileName); //user.png
                //生成上传后的新的文件名（唯一的） 拼接加扩展名  .png
                String newName = UUID.randomUUID()+ fileName.substring(fileName.lastIndexOf("."));
                file.transferTo(new File("D:\\img\\"+newName));
            }
        }
        //注册用户
        boolean flag = userService.register(user);
        if(flag){
            //注册成功 显示登陆页面
            return "redirect:/login.html";
        }
        //注册失败 显示表单页面 提示：当前用户名已被使用  springMVC支持自动回显(${key} key:类名首字母小写)
        model.addAttribute("errorMsg","当前用户名已被使用");
        return "form_layouts";
    }
}
