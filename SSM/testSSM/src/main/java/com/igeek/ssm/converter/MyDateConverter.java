package com.igeek.ssm.converter;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description 自定义的日期转换器 String-->Date
 * @Author fqq
 * @Date 2023/11/30 9:54
 *
 * 默认获取到的请求数据都是字符串，springMVC已经提供内置转换  String-->Integer String-->int...
 * 如果一般在pojo类中 出现时间类型 无法完成自动转换来实现参数绑定 需要自定义参数转换器
 */
public class MyDateConverter  implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(source);  //直接转成返回
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;  //如果参数绑定失败返回null
    }
}
