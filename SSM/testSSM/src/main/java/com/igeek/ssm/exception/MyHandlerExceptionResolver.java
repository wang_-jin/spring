package com.igeek.ssm.exception;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description 全局异常处理器(一个系统只有一个异常处理器)
 * @Author fqq
 * @Date 2023/11/30 14:14
 */
public class MyHandlerExceptionResolver implements HandlerExceptionResolver {
    /**
     * 解析异常
     * @param request  请求对象
     * @param response  响应对象
     * @param handler   正在执行的controller的方法
     * @param ex    执行的controller的方法 出现的异常
     * @return    ModelAndView   异常数据&逻辑视图名
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        //1.如果是自定义异常 则直接处理显示 message
        //2.如果不是自定义异常 包装成  未知错误
        //com.igeek.ssm.controller.ItemsController#viewOne(int, Model)
        System.out.println("handler = " + handler);

        CustomException customException = null;
        if(ex instanceof CustomException){
            //是自定义异常 则直接处理显示 message
            customException = (CustomException) ex;
        }else{
            customException = new CustomException("未知错误");
        }
        //统一返回数据
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("errorMsg",customException.getMessage());
        //设置逻辑视图名 /templates/error.html
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
