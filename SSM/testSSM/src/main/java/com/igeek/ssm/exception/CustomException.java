package com.igeek.ssm.exception;

/**
 * @Description 自定义异常基类
 * @Author fqq
 * @Date 2023/11/30 14:13
 */
public class CustomException extends Exception{
    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }
}
