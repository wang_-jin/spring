package com.igeek.ssm.pojo;

import java.util.List;

/**
 * @Description User类的扩展类
 * @Author fqq
 * @Date 2023/11/29 11:36
 */
public class UserCustom extends User{

    //一对多 订单信息
    private List<OrdersCustom> ordersCustomList;

    public List<OrdersCustom> getOrdersCustomList() {
        return ordersCustomList;
    }

    public void setOrdersCustomList(List<OrdersCustom> ordersCustomList) {
        this.ordersCustomList = ordersCustomList;
    }
}
