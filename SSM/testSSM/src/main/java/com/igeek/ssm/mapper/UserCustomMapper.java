package com.igeek.ssm.mapper;

import com.igeek.ssm.pojo.ItemsCustom;
import com.igeek.ssm.pojo.UserCustom;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

/**
 * Mybatis注解：
 * 1.插入商品信息
 * 2.根据订单的编号 订单信息以及 下单者的信息
 * 3.根据用户编号 ：查询用户信息 以及购买的商品信息
 * @Many注解  一对多关联
 * @One注解   一对一关联
 *
 * UserMapper扩展接口
 */
public interface UserCustomMapper {

    //根据订单编号 查询用户信息
    @Select("select * from user where id=#{uid}")
    public UserCustom selectUserById(int uid);

    //基于注解开发mybatis 常用于单表操作
    //根据用户编号查询用户信息 包含 订单信息 一对多
    @Select("select * from user where id=#{uid}")
    @Results({
        @Result(id = true,property = "id",column = "id"),     //主键字段的映射
        @Result(
                property = "ordersCustomList",              //property属性：一对多需要关联的属性名
                javaType = java.util.List.class,
                column = "id",                              //id属性：查询中的某一字段作为 关联查询的条件
                many = @Many(select = "com.igeek.ssm.mapper.OrdersCustomMapper.selectOrdersByUid",fetchType = FetchType.LAZY)
                //many属性 映射一对多关联  FetchType.LAZY延迟加载
        )
    }
    )
    public UserCustom findUserAndOrdersByUid(int uid);


}
