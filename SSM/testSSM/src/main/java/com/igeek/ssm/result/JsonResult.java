package com.igeek.ssm.result;

/**
 * @Description 封装json结果所需的数据
 * @Author fqq
 * @Date 2023/11/30 15:26
 */
public class JsonResult<T> {
    private String msg;
    private int code;
    private T data;


    public JsonResult() {
    }

    public JsonResult(String msg, int code, T data) {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public JsonResult(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    /**
     * 获取
     * @return msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * 设置
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 获取
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * 设置
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 获取
     * @return data
     */
    public T getData() {
        return data;
    }

    /**
     * 设置
     * @param data
     */
    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return "JsonResult{msg = " + msg + ", code = " + code + ", data = " + data + "}";
    }
}
