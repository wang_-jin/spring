package com.igeek.ssm.pojo;

/**
 * @Description Orderdetail的扩展类
 * @Author fqq
 * @Date 2023/11/22 15:29
 */
public class OrderdetailCustom extends Orderdetail{

    //一对一 商品信息
    private ItemsCustom items;

    //一对一 订单信息

    private OrdersCustom orders;

    public ItemsCustom getItems() {
        return items;
    }

    public void setItems(ItemsCustom items) {
        this.items = items;
    }

    public OrdersCustom getOrders() {
        return orders;
    }

    public void setOrders(OrdersCustom orders) {
        this.orders = orders;
    }
}
