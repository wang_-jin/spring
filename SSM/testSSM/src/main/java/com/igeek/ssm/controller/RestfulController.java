package com.igeek.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.igeek.ssm.dto.LoginDTO;
import com.igeek.ssm.pojo.User;
import com.igeek.ssm.result.JsonResult;
import com.igeek.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/30 15:23
 *
 * @RequestBody:作用在形参上 将json格式请求数据 转换为 java对象
 * @ResponseBody：作用在方法上 将java对象 转换为 json数据
 * @RestController:作用在类上，该类的所有方法都默认添加@ResponseBody注解  作用：@Controller+@ResponseBody
 *
 */

@RequestMapping("/rest")
@RestController
public class RestfulController {
    @Autowired(required = false)
    private UserService userService;

    //用户登陆 //localhost:8899/rest/user/userLogin.html
    //请求数据是字符串形式 xxx?name=admin&pwd=123 直接使用pojo类型 实现参数绑定
    //请求数据是json格式 {'name':'admin','pwd':'123'} 使用@RequestBody注解将json数据 转换为 pojo对象
    @PostMapping("/login")
    public JsonResult<User> login(@RequestBody LoginDTO loginDTO, HttpSession session){
        System.out.println("loginDTO.getName() = " + loginDTO.getName());
        System.out.println("loginDTO.getPwd() = " + loginDTO.getPwd());
        User user = userService.login(loginDTO.getName(), loginDTO.getPwd());
        JsonResult<User> result = null;
        if(user!=null){
            //200 登陆成功 user信息
            session.setAttribute("user",user);
            result = new JsonResult<>("登陆成功", 200, user);
        }else{
            //455 登陆失败
            result = new JsonResult<>("登陆失败", 455);
        }
        return  result;
    }
    @GetMapping("/logout")
    public JsonResult<Void> logout(HttpSession session){
        JsonResult<Void> result = null;
        session.invalidate();
        result = new JsonResult<>("登出成功", 200);
        return result;
    }

    //用户注册 //用户登陆 //localhost:8899/rest/user/userRegister.html
    //请求数据就是表单数据 可以直接User类型的形参来实现参数绑定
    @PostMapping("/register")
    public JsonResult<User> register(User user, MultipartFile file) throws IOException {
        System.out.println("user.getName() = " + user.getName());
        System.out.println("user.getGender() = " + user.getGender());
        System.out.println("user.getBirthday() = " + user.getBirthday());
        System.out.println("file.getOriginalFilename() = " + file.getOriginalFilename());
        //1.如果有头像 上传图片到本地服务器
        JsonResult<User> result = null;
        if(StringUtils.hasLength(file.getOriginalFilename())){
            //1.上传文件到本地服务器 （复制文件）
            //获取原始文件名 .png
            String fileName = file.getOriginalFilename();
            System.out.println("fileName="+fileName); //user.png
            //生成上传后的新的文件名（唯一的） 拼接加扩展名  .png
            String newName = UUID.randomUUID()+ fileName.substring(fileName.lastIndexOf("."));
            file.transferTo(new File("D:\\img\\"+newName));

            //2.设置用户头像路径pic属性的值
            user.setPic("/pic/"+newName);
        }
        //2.完成注册业务:如果用户名已使用则注册失败
        //如果正常执行注册计算年龄，用户状态默认为0
        boolean flag = userService.register(user);
        if(flag){
            result = new JsonResult<>("注册成功",200);
        }else {
            result = new JsonResult<>("注册失败", 455);
        }
        return result;
    }

    @GetMapping("/viewAll")
    public JsonResult<PageInfo<User>> viewAll(
            @RequestParam(value = "current",required = false,defaultValue = "1") int current,
            @RequestParam(value = "query",required = false,defaultValue = "") String query){
        //查询用户列表 封装pageInfo对象
        List<User> users = userService.viewAll(current,query);
        JsonResult result = null;
        if(users!=null && users.size()>0){
            //封装PageInfo对象
            PageInfo<User> pageInfo = new PageInfo<User>(users);
            //最后返回map
            HashMap<String, Object> map = new HashMap<>();
            map.put("pageInfo",pageInfo);//list数据+ 页码信息pageNum total
            map.put("query",query);//查询条件 页面回显查询条件
            result = new JsonResult("查询成功",200,map);
        }else{
            result = new JsonResult<>("查询失败",406);
        }
        return result;
    }

    @DeleteMapping("/delete/{id}")
    public JsonResult<Void> delete(@PathVariable("id") int uid) {
        boolean flag = userService.deleteOne(uid);
        JsonResult<Void> result;
        if (flag) {
            result = new JsonResult<>("删除成功", 200);
        } else {
            result = new JsonResult<>("删除失败", 499);
        }
        return result;
    }

    @GetMapping("/viewOne/{id}")
    public JsonResult<User> viewOne(@PathVariable("id") int id) {
        User user = userService.viewOne(id);
        JsonResult<User> result;
        if (user != null) {
            result = new JsonResult<>("查询成功", 200, user);
        } else {
            result = new JsonResult<>("查询失败", 488, null);
        }
        return result;
    }

    //修改用户信息 put请求能否支持文件上传
    @PutMapping("/update")
    public JsonResult<Void> update(User user,MultipartFile file) throws IOException {
        //如果有获取到file 则需要处理图片上传
        if(file!=null){
            String oldName = file.getOriginalFilename();
            if(StringUtils.hasLength(oldName)){
                String newName = UUID.randomUUID()+oldName.substring(oldName.lastIndexOf('.'));
                //一键上传
                file.transferTo(new File("D:\\img\\"+newName));
                //用户设置pic路径
                user.setPic("/pic/"+newName);
            }
        }
        //如果未上传图片 则请求数据中带回 原始pic路径  此处无需处理

        //更新动作
        boolean flag = userService.update(user);
        JsonResult<Void> result;
        if (flag) {
            result = new JsonResult<>("更新成功", 200);
        } else {
            result = new JsonResult<>("更新失败", 466);
        }
        return result;
    }
}
