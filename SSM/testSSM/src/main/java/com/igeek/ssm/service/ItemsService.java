package com.igeek.ssm.service;

import com.igeek.ssm.mapper.ItemsMapper;
import com.igeek.ssm.pojo.Items;
import com.igeek.ssm.pojo.ItemsCustom;
import com.igeek.ssm.pojo.ItemsExample;
import com.igeek.ssm.vo.ItemsQueryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/29 14:29
 */
@Service
@Transactional
public class ItemsService {
    @Autowired(required = false)
    private ItemsMapper itemsMapper;

    //根据商品名称 查询商品列表
    // query可以处理为空串  where name like '%%' ==>全表查询
    // query有查询条件     where name like '%query%' ==>模糊查询
    public List<Items> viewAll(String query){
        ItemsExample itemsExample = new ItemsExample();
        ItemsExample.Criteria criteria = itemsExample.createCriteria();
        criteria.andNameLike("%"+query+"%");
        List<Items> items = itemsMapper.selectByExampleWithBLOBs(itemsExample);
        return items;
    }

    //根据商品编号 查询商品信息
    public Items viewOne(int id) {
        Items items = itemsMapper.selectByPrimaryKey(id);
        return items;
    }

    //根据商品编号 更新商品信息
    public boolean update(Items items) {
        int count = itemsMapper.updateByPrimaryKeySelective(items);
        return count>0;
    }
    //根据商品编号 删除商品信息
    public boolean delete(int id) {
        int count = itemsMapper.deleteByPrimaryKey(id);
        return count>0;
    }
    //根据一组商品编号 批量删除
    public boolean deleteAll(Integer[] id) {
        ItemsExample itemsExample = new ItemsExample();
        ItemsExample.Criteria criteria = itemsExample.createCriteria();
        criteria.andIdIn(Arrays.asList(id));
        int count = itemsMapper.deleteByExample(itemsExample);
        return count>0;
    }
    //根据一组商品编号 批量更新
    public void updateAll(List<Items> itemsList) {
        for (Items item : itemsList) {
            itemsMapper.updateByPrimaryKeyWithBLOBs(item);
        }
    }
}
