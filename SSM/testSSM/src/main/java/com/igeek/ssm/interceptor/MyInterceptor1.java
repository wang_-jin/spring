package com.igeek.ssm.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description 拦截器1
 * @Author fqq
 * @Date 2023/12/1 14:10
 *
 *
 * 1.MyInterceptor1  和  MyInterceptor2 都放行：interceptorIndex=1
 * MyInterceptor1 preHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 * MyInterceptor2 preHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 * 执行handler.... viewAll（）.......
 * MyInterceptor2 postHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model),mv=ModelAndView [view="items/itemsList"; model={items=[com.igeek.ssm.pojo.Items@1f4489e8, com.igeek.ssm.pojo.Items@1c4c30b, com.igeek.ssm.pojo.Items@2edfa001], queryname=张}]
 * MyInterceptor1 postHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model),mv=ModelAndView [view="items/itemsList"; model={items=[com.igeek.ssm.pojo.Items@1f4489e8, com.igeek.ssm.pojo.Items@1c4c30b, com.igeek.ssm.pojo.Items@2edfa001], queryname=四}]
 * MyInterceptor2 afterCompletion.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 * MyInterceptor1 afterCompletion.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 *
 * 2.MyInterceptor1 放行  和  MyInterceptor2 不放行：interceptorIndex=0
 * MyInterceptor1 preHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 * MyInterceptor2 preHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 * MyInterceptor1 afterCompletion.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 *
 *
 * 3.MyInterceptor1  和  MyInterceptor2 都不放行： interceptorIndex=-1
 * MyInterceptor1 preHandle.... handler = com.igeek.ssm.controller.ItemsController#viewAll(String, Model)
 */
public class MyInterceptor1 implements HandlerInterceptor {
    /**
     * 在真正执行handler(controller#xxx方法)之前
     * @param request 请求
     * @param response 响应
     * @param handler
     * @return 返回true表示放行  返回false表示不放行
     * @throws Exception
     * 使用场景:登陆访问控制 权限验证等
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("MyInterceptor1 preHandle.... handler = " + handler);
        return true;//放行
//        return false;
    }

    /**
     * 在执行handler之后 && 渲染视图之前
     * @param request 请求
     * @param response 响应
     * @param handler
     * @param modelAndView 数据和逻辑视图的对象
     * @throws Exception
     * 使用场景：修改当前modelAndView的信息：修改数据，逻辑视图名
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //查询条件上面 出现 四
        modelAndView.addObject("queryname","四");
        System.out.println("MyInterceptor1 postHandle.... handler = " + handler+",mv="+modelAndView);
    }

    /**
     * 在渲染视图之后
     * @param request 请求
     * @param response 响应
     * @param handler
     * @param ex 异常(controller service dao等抛出的异常)
     * @throws Exception
     * 使用场景：统一异常处理，统一日志处理
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("MyInterceptor1 afterCompletion.... handler = " + handler);
    }
}
