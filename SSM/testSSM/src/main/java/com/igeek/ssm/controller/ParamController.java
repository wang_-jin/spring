package com.igeek.ssm.controller;

import com.igeek.ssm.pojo.Items;
import com.igeek.ssm.pojo.ItemsCustom;
import com.igeek.ssm.pojo.User;
import com.igeek.ssm.service.ItemsService;
import com.igeek.ssm.vo.ItemsQueryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author wangjin
 * 2023/11/30 9:44
 * @description TODO
 */
@Controller
public class ParamController {
    @Autowired(required = false)
    private ItemsService itemsService;

    /**
     * 1.默认支持的类型
     * @param request  request将数据封装到请求域中 request.setAttribute()
     * @param response 通过response将数据响应到客户端中  json数据 cookie(将数据保存到浏览器中)
     * @param session  session将数据封装到会话域中  session.setAttribute() session数据在服务器中
     * @param model   model将数据封装到请求域中  model.addAttribute()
     * @return
     */
    public String m1(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
        return "";
    }

    /**
     * 2.简单类型
     * 前端界面：input输入框 name="username"  请求路径 xxxx?username=admin
     * springMVC的方法的形参中，形参名称必须和name属性值 相同 则可以进行自动参数绑定
     * 如果不一致，可以搭配@RequestParam(value = "username",required = true)注解
     * 表示当前参数必须传入若未传入会出现400错误 Required request parameter 'username' for method parameter type String is not present
     *
     * @RequestParam(value = "username",required = false,defaultValue = "")注解 表示参数可以不必传入，没有参数则分配默认值defaultValue
     *
     * @param username
     * @return
     */
    @GetMapping("/m2")
    public String m2(
            @RequestParam(value = "username",required = false,defaultValue = "") String username,
            @RequestParam(value = "age",required = true)  int age){
        return "";
    }

    /**
     * 3.pojo类型
     * 前端界面：input输入框较多时可以使用
     * springMVC的方法的形参中，形参的类型是pojo类，要求pojo类中的属性名称 必须 和 输入框name属性的值一致，则可以参数绑定
     * @param user
     * @return
     */
    public String m3(User user){
        return null;
    }

    /**
     * 4.文件上传的类型
     * 前端界面：input输入框 file类型 name="photo"属性
     * springMVC的方法的形参中 形参的类型是MultipartFile，形参名称必须和name属性一致，则可以参数绑定。
     * 若不一致，则可以搭配 @RequestParam("photo")注解
     * @param  photo
     * @return
     */
    public String m4(@RequestParam("photo") MultipartFile photo){
        return null;
    }

    /**
     * 5.自定义类型参数绑定
     * 方式一：可以在pojo类的 Date createtime 属性上
     *          添加注解 @DateTimeFormat(pattern = "yyyy-MM-dd")
     * 方式二： 可以自定义参数绑定器
     *         1.自定义参数绑定器 implements Converter<String, Date>
     *         2.springMVC配置类中，将自定义参数绑定器注入到容器中 @Bean方式
     *         3.springMVC（implements WebMvcConfigurer）配置类中，重写addFormatters方法注入 参数绑定器
     *
     * 前端界面：传入的日期字符串 xxx?createtime=2023-11-30
     * @param items
     * @return
     */
    @GetMapping("/m5")
    public String m5(Items items){
        System.out.println("items.getName() = " + items.getName());
        System.out.println("items.getCreatetime() = " + items.getCreatetime());
        return "";
    }

    /**
     * 6.包装类型pojo参数绑定
     * 前端界面：input输入框name="items.name"
     * springMVC的方法形参中 形参类型是ItemsQueryVO类型 必须和vo类型的items属性名.name属性名 一致，才可以参数绑定
     * @param vo
     * @return
     */
    @GetMapping("/m6")
    public String m6(ItemsQueryVO vo, Model model){
        System.out.println("vo.getItems().getName() = " + vo.getItems().getName());
        List<Items> items = itemsService.viewAll(vo.getItems().getName());
        model.addAttribute("items",items);
        return "items/itemsList";
    }

    /**
     * 7.数组绑定
     * 批量删除
     *
     * 前端界面：多个 input标签 type="checkbox" 的name="id"  收集到多个同名的请求参数
     * springMVC的方法形参中 形参的类型是数组 形参名字和name属性值 一致 才可以数组绑定
     * @param id
     * @return
     */
    @GetMapping("/del")
    public String del(Integer[] id){
        boolean flag = itemsService.deleteAll(id);
        return "redirect:/items/itemsList.html";
    }
    /**
     * 8.集合绑定
     * 批量更新
     * @param
     * @return
     */
    //localhost:8899/items/updateItems?itemsList[0].id=1

    @PostMapping("/update")
    public String updateItems(ItemsQueryVO vo){
        System.out.println("itemsList = " + vo.getItemsList());
        itemsService.updateAll(vo.getItemsList());
        vo.getItemsList().forEach(items -> {
            System.out.println(items.getId()+","+items.getName()+","+ items.getPrice());
        });
        return "redirect:/items/itemsList.html";
    }
}

