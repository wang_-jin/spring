package com.igeek.ssm.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * @Description WebInitializer  代替web.xml
 * @Author fqq
 * @Date 2023/11/28 10:19
 */
public class WebInitializer implements WebApplicationInitializer {
    //一旦tomcat容器启动 加载onStartup方法
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //创建具有web环境的IOC容器 加载相关的配置类
        AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
        ac.register(new Class[]{MybatisConfig.class,SpringMVCConfig.class,ThymeleafTemplateConfig.class});

        //编码集过滤器  UTF-8
        FilterRegistration.Dynamic filterRegistration =
                servletContext.addFilter("CharacterEncodingFilter", new CharacterEncodingFilter("UTF-8"));
        //设置过滤器的过滤路径
        filterRegistration.addMappingForUrlPatterns(null,true,"/*");
        //将上下文装配至IOC容器中
        ac.setServletContext(servletContext);

        //配置前端控制器
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("dispatcher", new DispatcherServlet(ac));
        //前端控制器处理的请求url
        //servletRegistration.addMapping("*.action");

        //设置前端控制器处理的请求  RESTFul风格 HTTP动词 POST、GET、PUT、DELETE
        servletRegistration.addMapping("/");
        //随着Tomcat容器启动而加载
        servletRegistration.setLoadOnStartup(1);
    }

}
