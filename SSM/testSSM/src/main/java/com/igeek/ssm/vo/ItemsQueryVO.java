package com.igeek.ssm.vo;

import com.igeek.ssm.pojo.Items;
import com.igeek.ssm.pojo.ItemsCustom;
import com.igeek.ssm.pojo.User;

import java.util.List;

/**
 * @Description 封装综合查询条件
 * @Author fqq
 * @Date 2023/11/30 10:19
 */
public class ItemsQueryVO {
    //items name price
    private Items items;
    private ItemsCustom itemsCustom;

    public ItemsCustom getItemsCustom() {
        return itemsCustom;
    }

    public void setItemsCustom(ItemsCustom itemsCustom) {
        this.itemsCustom = itemsCustom;
    }

    //user name
    private User user;
    //评价 距离
    //批量商品信息
    private List<Items> itemsList;

    public List<Items> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<Items> itemsList) {
        this.itemsList = itemsList;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
