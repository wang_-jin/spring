package com.igeek.ssm.controller;

import com.igeek.ssm.pojo.User;
import com.igeek.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/28 11:50
 */
@Controller
public class IndexController {
    @Autowired(required = false)
    private UserService userService;

    /**
     * 显示成功页面
     * 窄化路径： localhost:8899/success.html
     * @param model  model域中封装数据
     * @return  视图名称
     */
    @RequestMapping("/success.html")
    public String success(Model model){
        //model域中封装数据 ==》请求域中存放键值对
        model.addAttribute("msg","明天是周三");
        model.addAttribute("link","http://www.taobao.com");
        //返回逻辑视图名称 跳转到 /templates/success.html
        return "success";
    }

    /**
     * 显示登陆页面
     * 窄化路径： localhost:8899/login.html 地址栏直接发出get请求
     * @return  视图名称
     */
    @GetMapping("/login.html")
    public String getLogin(){
        //跳转 /templates/login.html页面
        return "login";
    }

    /**
     * 处理登陆请求的接口
     * 窄化路径： localhost:8899/login
     *          login(String username,String pwd):HTML中表单input输入框 name属性必须和方法的形参名称一致
     *          login(User user):HTML中表单input输入框 name属性必须和方法的形参pojo类型的属性名一致
     * @return
     */
    @PostMapping("/login")
    public String login(String username, String pwd, HttpSession session, Model model){
        if(StringUtils.hasLength(username) && StringUtils.hasLength(pwd)){
            //非空判断通过后 执行业务
            User user = userService.login(username, pwd);
            if(user!=null){
                //可以登陆成功，跳转到main.html 并且 显示当前登陆用户名
                //session会话域存储 登陆成功后的用户信息
                session.setAttribute("user",user);
                /*
                跳转 /templates/main.html页面:
                return "forward:main"
                        默认行为是请求转发 地址栏显示一开始请求 /login ，
                        一旦手动刷新地址栏则会405 - Request method 'GET' not supported

                return "redirect:/main.html"
                        响应重定向  发起新的请求:地址栏显示目标路径 /main.html
                        此时不会再使用thymeleaf的视图解析器
                 */
               return "redirect:/main.html";
            }
        }
        //登陆失败，跳转到login.html并且 显示错误信息errorMsg
        model.addAttribute("errorMsg","登陆失败用户名或密码错误");
        return "login";
    }

    /**
     * 显示main.html主页
     * @return
     */
    @GetMapping("/main.html")
    public String getMain(HttpSession session,Model model){
        User user = (User) session.getAttribute("user");

        if(user == null){
            //否则 还是跳转到登陆页面 提示：请先登陆！！！
            model.addAttribute("errorMsg","请先登陆！！！");
            return "login";
        }
        //如果session域有user用户信息则可以跳转到main.html页面
        //跳转 /templates/main.html页面
        return "main";
    }

    /**
     * 处理登出请求的接口
     * @return
     */
    @GetMapping("/logout")
    public String logOut(HttpSession session,Model model){
        //清空session域中的用户信息
        session.invalidate();
        //登出后 跳转到登陆login.html 并显示 "已登出"  ==》使用thymeleaf的视图解析器
        model.addAttribute("errorMsg","已登出");
        return "login";
    }
}
