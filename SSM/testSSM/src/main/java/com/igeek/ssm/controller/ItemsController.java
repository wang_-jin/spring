package com.igeek.ssm.controller;

import com.igeek.ssm.exception.CustomException;
import com.igeek.ssm.pojo.Items;
import com.igeek.ssm.service.ItemsService;
import com.igeek.ssm.validator.ValidatorGroup1;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description 商品模块的控制层
 * @Author fqq
 * @Date 2023/11/29 14:23
 */
@Controller
@RequestMapping("/items")
public class ItemsController {
    @Autowired(required = false)
    private ItemsService itemsService;

    //根据商品名称 模糊 查看商品列表页面
    //访问路径： localhost:8899/items/itemsList.html
    //@RequestParam("queryname") 相当于 request.getParameter("queryname")
    //@RequestParam(name = "queryname",required = false,defaultValue = "") String query 处理query的空串的问题
    @RequestMapping (path = "/itemsList.html",method = RequestMethod.GET)
    public String viewAll(@RequestParam(name = "queryname",required = false,defaultValue = "") String query, Model model){

        //查询商品列表
        List<Items> items = itemsService.viewAll(query);
        //将数据封装到model 请求域中
        model.addAttribute("items",items);
        model.addAttribute("queryname",query);
        //跳转到 templates/items/itemsList.html
        return "items/itemsList";
    }

    //查看商品详情 localhost:8899/items/viewone?id=1
    @GetMapping("/viewone")
    public String viewOne(@RequestParam("id") int id, Model model) throws CustomException {
        Items items = itemsService.viewOne(id);

        //如果根据id未查询到相关商品 则引发自定义异常
        if(items==null){
            throw new CustomException("未找到id="+id+"的商品信息");
        }

        model.addAttribute("items",items);
        //跳转到 /templates/items/edititems.html
        return "items/edititems";
    }
    //查看商品详情 localhost:8899/items/1
    //获取路径中变量 @PathVariable("key") 一般搭配使用 请求路径 localhost:8899/items/{key}
    @GetMapping("/{id}")
    public String viewOneByPath(@PathVariable("id")int id, Model model){
        Items items = itemsService.viewOne(id);
        model.addAttribute("id",id);
        //跳转到 /templates/items/edititems.html
        return "items/edititems";
    }
    //根据商品编号 删除商品信息
    @GetMapping ("/delete")
    public String delete(@RequestParam("id") int id){
        boolean flag = itemsService.delete(id);
        return "redirect:/items/itemsList.html";
    }

    //根据商品编号 更新商品信息
    //使用校验:
    //1.需要校验Items属性内容是否合格 形参之前添加注解@Validated
    //2.需要在校验形参后面，必须紧跟BindingResult result参数 可以通过该参数获取校验结果
    //3.@Validated(value = {ValidatorGroup1.class}) 指定只让校验分组1生效 其他校验分组不使用
    @PostMapping("/update")
    public String update(@Validated(value = {ValidatorGroup1.class}) Items items, BindingResult result, MultipartFile photo, Model model) throws IOException {
        //如果有上传图片 处理
        String filename = photo.getOriginalFilename();
        if(StringUtils.hasLength(filename)){
            String newName = UUID.randomUUID()+filename.substring(filename.lastIndexOf("."));
            photo.transferTo(new File("D:\\img\\"+newName));
            items.setPic("/pic/"+newName);
        }else {
            //如果不上传图片则还是使用原来的图片 不更新图片pic
            String pic = itemsService.viewOne(items.getId()).getPic();
            items.setPic(pic);
        }

        //获取参数校验的结果
        if(result.hasErrors()){//是否有错误信息
            List<ObjectError> allErrors = result.getAllErrors();
            allErrors.forEach(objectError -> System.out.println(objectError.getDefaultMessage()));
            //回显错误信息到页面
            model.addAttribute("allErrors",allErrors);
            //跳转到/templates/items/edititems.html
            //如果强制添加前缀forward或者redirect 则不会使用thymeleaf视图解析器找到模板页面进行渲染 直接跳转到返回值路径
            return "items/edititems";
        }

        //必须生产日期不能比当前日期晚
        boolean timeFlag = items.getCreatetime().after(new Date());
        if(!timeFlag){
            //更新商品成功 则跳转到商品列表页面  请求转发？重定向？
            boolean flag = itemsService.update(items);
            if(flag){
                return "redirect:/items/itemsList.html";
            }
        }
        //更新失败 跳转到修改页面 回显商品信息 请求转发？重定向？
        //数据回显：1.model中封装数据 2.springMVC默认回显机制，要求input的value属性的key和 类名首字母小写一致
        //model.addAttribute("items",items);
        return "forward:items/edititems";
    }
}
