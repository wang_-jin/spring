package com.igeek.ssm.service;

import com.github.pagehelper.PageHelper;
import com.igeek.ssm.mapper.UserMapper;
import com.igeek.ssm.pojo.User;
import com.igeek.ssm.pojo.UserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

/**
 * @Description 用户模块的业务层
 * @Author fqq
 * @Date 2023/11/28 14:30
 */
@Service
public class UserService {
    //代替的是 sqlsession.getMapper()
    @Autowired(required = false)
    private UserMapper userMapper;

    //用户登陆
    //添加@Transactional注解：使用声明式事务 1.开启事务 2.执行业务 3.有异常回滚事务 4.提交事务
    @Transactional(readOnly = true)
    public User login(String name, String pwd){
        //封装查询条件 select * from user where name='' and pwd=''
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andNameEqualTo(name);
        criteria.andPwdEqualTo(pwd);
        //执行查询
        List<User> users = userMapper.selectByExample(userExample);
        return users.size()>0?users.get(0):null;
    }

    //查看所有用户列表
    @Transactional(readOnly = true)
    public List<User> findAll(){
        List<User> users = userMapper.selectByExample(null);
        return users;
    }

    //注册用户
    @Transactional
    public boolean register(User user) {
        //计算年龄
        Calendar c = Calendar.getInstance();
        c.setTime(user.getBirthday());//生日
        int age = LocalDate.now().getYear()-c.get(Calendar.YEAR);
        user.setAge(age);
        //根据用户名查询用户信息
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andNameEqualTo(user.getName());
        List<User> users = userMapper.selectByExample(userExample);
        if(users.size()>0){
            return false;
        }
        int count = userMapper.insertSelective(user);
        return count>0;
    }

    //携带查询条件 分页查询用户信息
    public List<User> viewAll(int current, String query) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andNameLike("%"+query+"%");
        //在真正查询之前 添加上分页的插件
        //第一个参数:当前页码 第二个参数:每页展示的记录数
        PageHelper.startPage(current,3);
        //分页+条件查询 将查询结果自动封装到pageInfo
        List<User> users = userMapper.selectByExample(userExample);
        return users;
    }
    //根据用户编号 删除用户信息
    public boolean deleteOne(int uid) {
        int count = userMapper.deleteByPrimaryKey(uid);
        return count>0;
    }

    //根据用户编号 查询用户信息
    public User viewOne(int id) {
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }

    //根据用户编号 更新用户信息
    public boolean update(User user) {
        //计算年龄
        Calendar c = Calendar.getInstance();
        c.setTime(user.getBirthday());//生日
        int age = LocalDate.now().getYear()-c.get(Calendar.YEAR);
        user.setAge(age);
        //执行非空更新
        int count = userMapper.updateByPrimaryKeySelective(user);
        return count>0;
    }
}
