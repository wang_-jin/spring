package com.igeek.ssm.mapper;

import com.igeek.ssm.pojo.OrdersCustom;
import com.igeek.ssm.pojo.UserCustom;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface OrdersCustomMapper {

/*    //根据用户编号 查询订单信息
    @Select("select * from orders where user_id=#{uid}")
    public List<OrdersCustom> selectOrdersByUid(int uid);*/

    //根据订单的编号 订单信息以及 下单者的信息 一对一
    @Select("select * from orders where id=#{oid}")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(
                    property = "userCustom",
                    column = "user_id",
                    one = @One(select = "com.igeek.ssm.mapper.UserCustomMapper.selectUserById")
            )
    })
    public OrdersCustom selectOrderAndUserByOid(int oid);

    //根据订单的编号 查询订单详情 一对多
    @Select("select * from orders where user_id=#{uid}")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(
                    property = "orderdetails",
                    javaType = java.util.List.class,
                    column = "id",
                    many = @Many(select = "com.igeek.ssm.mapper.OrderdetailCustomMapper.selectOrderdetailByOid")
            )
    })
    public List<OrdersCustom> selectOrdersByUid(int uid);
}
