package com.igeek.ssm.config;

import com.github.pagehelper.PageInterceptor;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.ibatis.logging.slf4j.Slf4jImpl;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;

/**
 * @Description MybatisConfig  代替 sqlMapConfig.xml
 * @Author fqq
 * @Date 2023/11/28 10:18
 */
//加载属性文件
@PropertySource("classpath:db.properties")
//开启注解式事务 使用cglib动态代理
@EnableTransactionManagement(proxyTargetClass = true)
//扫描mapper接口的包名 为其创建代理对象
@MapperScan("com.igeek.ssm.mapper")
public class MybatisConfig {

    //配置数据源
    @Bean
    public DataSource dataSource(
            @Value("${db.user}") String username,
            @Value("${db.password}") String password,
            @Value("${db.url}") String jdbcUrl,
            @Value("${db.driverClass}") String driverClass
    ) throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(username);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl(jdbcUrl);
        dataSource.setDriverClass(driverClass);
        return dataSource;
    }

    //配置事务管理器
    @Bean
    public PlatformTransactionManager platformTransactionManager(DataSource dataSource){
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource);
        return transactionManager;
    }

    //配置会话工厂
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) throws IOException {
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        //设置数据源
        factory.setDataSource(dataSource);
        //设置实体类中 别称
        factory.setTypeAliasesPackage("com.igeek.ssm.pojo");

        //设置全局参数 import org.apache.ibatis.session.Configuration;
        Configuration configuration = new Configuration();
        //开启日志的输出
        configuration.setLogImpl(StdOutImpl.class);
        //开启驼峰式命名
        configuration.setMapUnderscoreToCamelCase(true);
        //开启延迟加载
        configuration.setLazyLoadingEnabled(true);
        //关闭积极加载
        configuration.setAggressiveLazyLoading(false);
        //配置默认触发延迟加载的方法 设置为无
        configuration.setLazyLoadTriggerMethods(new HashSet<>());
        //开启二级缓存
        configuration.setCacheEnabled(true);
        factory.setConfiguration(configuration);

        //配置分页插件 就是拦截器
        PageInterceptor pageInterceptor = new PageInterceptor();
        //设置属性对象
        Properties properties = new Properties();
        //指定数据库的方言
        properties.setProperty("helperDialect","mysql");
        //指定参数合理化
        properties.setProperty("reasonable","true");
        pageInterceptor.setProperties(properties);
        //在会话工厂添加插件
        factory.setPlugins(new Interceptor[]{pageInterceptor});

        //配置XxxMapper.xml的路径
        factory.setMapperLocations(
                new PathMatchingResourcePatternResolver().
                        getResources("classpath:/mapper/*.xml"));

        //分页插件

        return factory;
    }

}
