package com.igeek.ssm.interceptor;

import com.igeek.ssm.pojo.User;
import com.igeek.ssm.result.JsonResult;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/12/1 15:14
 */
public class LoginInterceptor implements HandlerInterceptor {
    /**
     *
     * @param request 请求
     * @param response  响应
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取访问的url
        String uri = request.getRequestURI();
        System.out.println("uri = " + uri);

        //若没有登录用户信息 不放行 并且跳转到登陆页面 提示：请先登陆！！！
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user == null){
            //重定向到登录页面
            System.out.println("--------");
            response.sendRedirect("/rest/user/userLogin.html");
            return false;
        }
        //若有登录后的用户信息 则放行 到目标handler
        return true;
    }
}
