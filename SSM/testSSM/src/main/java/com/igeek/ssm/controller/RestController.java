package com.igeek.ssm.controller;

import com.igeek.ssm.dto.LoginDTO;
import com.igeek.ssm.pojo.User;
import com.igeek.ssm.result.JsonResult;
import com.igeek.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/30 15:23
 *
 * @RequestBody:作用在形参上 将json格式请求数据 转换为 java对象
 * @ResponseBody：作用在方法上 将java对象 转换为 json数据
 */
@RequestMapping("/rest")
@Controller
public class RestController {
    @Autowired(required = false)
    private UserService userService;

    //用户登陆
    //请求数据是字符串形式 xxx?name=admin&pwd=123 直接使用pojo类型 实现参数绑定
    //请求数据是json格式 {'name':'admin','pwd':'123'} 使用@RequestBody注解将json数据 转换为 pojo对象
    @PostMapping("/login")
    @ResponseBody
    public JsonResult<User> login(@RequestBody LoginDTO loginDTO){
        System.out.println("loginDTO.getName() = " + loginDTO.getName());
        System.out.println("loginDTO.getPwd() = " + loginDTO.getPwd());
        User user = userService.login(loginDTO.getName(), loginDTO.getPwd());
        JsonResult<User> result = null;
        if(user!=null){
            //200 登陆成功 user信息
            result = new JsonResult<>("登陆成功", 200, user);
        }else{
            //455 登陆失败
            result = new JsonResult<>("登陆失败", 455);
        }
        return  result;
    }


}
