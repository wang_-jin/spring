package com.igeek.ssm.mapper;

import com.igeek.ssm.pojo.OrderdetailCustom;
import com.igeek.ssm.pojo.OrdersCustom;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderdetailCustomMapper {

    //根据订单明细查询商品信息 一对一
    @Select("select * from orderdetail where orders_id=#{oid}")
    @Results({
            @Result(id = true,property = "id",column = "id"),
            @Result(
                    property = "items",
                    column = "items_id",
                    one = @One(select = "com.igeek.ssm.mapper.ItemsCustomMapper.selectItemByIid")
            )
    })
    public List<OrderdetailCustom> selectOrderdetailByOid(int oid);
}
