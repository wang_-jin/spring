package com.igeek.ssm.pojo;

import com.igeek.ssm.validator.ValidatorGroup1;
import com.igeek.ssm.validator.ValidatorGroup2;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class Items {
    private Integer id;

    //指定在分组1中
    @Size(min = 3,max = 6,message = "{items.name.length.msg}",groups = {ValidatorGroup1.class})
    private String name;

    private Float price;

    private String pic;


    //日期转换 ：按照指定的格式 将请求参数转换为日期
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    //指定在分组2中
    @NotNull(message = "{items.createtime.msg}",groups = {ValidatorGroup2.class})
    private Date createtime;

    private String detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }
}