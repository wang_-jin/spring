package com.igeek.ssm.mapper;

import com.igeek.ssm.pojo.ItemsCustom;

import com.igeek.ssm.pojo.UserCustom;
import org.apache.ibatis.annotations.*;


public interface ItemsCustomMapper {
    //插入商品信息
    @Insert("insert into items values(#{id},#{name},#{price},#{detail},#{pic},#{createtime})")
    public int insertItem(ItemsCustom itemsCustom);

    //根据商品编号 查询商品信息
    @Select("select * from items where id=#{iid}")
    public ItemsCustom selectItemByIid(int iid);


}
