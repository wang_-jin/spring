package com.igeek.ssm.controller;

import com.igeek.ssm.pojo.User;
import com.igeek.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

/**
 * @author wangjin
 * 2023/11/28 21:12
 * @description TODO
 */
@Controller
public class RegisterController {
    @Autowired
    private UserService userService;
    //localhost:8899/login.html
    @GetMapping("/form_layouts.html")
    public String getRegister(){
        //跳转 /templates/login.html页面
        return "form_layouts";
    }
    @PostMapping("/form_layouts")
    public String register(String username, String pwd, HttpSession session, Model model){
        if(StringUtils.hasLength(username) && StringUtils.hasLength(pwd)){
            User user = new User();
            user.setName(username);
            user.setPwd(pwd);
            boolean flag = userService.findOne(username);
            System.out.println(flag);
            if (!flag){
                User u = userService.register(user);
                if (u==null){
                    return "form_layouts";
                }
                session.setAttribute("user",u);
                model.addAttribute("errorMsg","注册成功");
                return "redirect:/login.html";
            }
        }
        model.addAttribute("errorMsg","用户名已存在");
        return "form_layouts";
    }
}
