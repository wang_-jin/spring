package com.igeek.ssm.pojo;

import java.util.List;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/11/29 11:37
 */
public class OrdersCustom extends Orders{
    //一对一 用户信息
    private UserCustom userCustom;
    //一对多 订单详情
    private List<OrderdetailCustom> orderdetails;

    public List<OrderdetailCustom> getOrderdetails() {
        return orderdetails;
    }

    public void setOrderdetails(List<OrderdetailCustom> orderdetails) {
        this.orderdetails = orderdetails;
    }

    public UserCustom getUserCustom() {
        return userCustom;
    }

    public void setUserCustom(UserCustom userCustom) {
        this.userCustom = userCustom;
    }
}
