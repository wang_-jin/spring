package com.igeek.ssm.config;

import com.igeek.ssm.converter.MyDateConverter;
import com.igeek.ssm.exception.MyHandlerExceptionResolver;
import com.igeek.ssm.interceptor.LoginInterceptor;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Description SpringMVCConfig 代替springmvc.xml
 * @Author fqq
 * @Date 2023/11/28 10:19
 */
//开启注解式开发
@EnableWebMvc
//配置组件扫描包 service controller
@ComponentScan(basePackages = {"com.igeek.ssm.controller","com.igeek.ssm.service"})
public class SpringMVCConfig implements WebMvcConfigurer {
    //文件上传  多部件解析器
    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        //当前多部件解析器，不仅可以支持POST请求、也可以支持PUT请求
        multipartResolver.setSupportedMethods("POST","PUT");
        multipartResolver.setMaxUploadSize(5242880);
        return multipartResolver;
    }

    //RestFul风格 对静态资源放行
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    //配置本地图片服务器路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // *  一个星号 只能匹配的是单层路径
        //  ** 两个星号 匹配的是多层级路径  包含 /pic/2023-11-28/user.png
        //项目部署后 地址栏 localhost:8899/pic/user.png  真正访问的本地图片服务器 D:\img
        registry.addResourceHandler("/pic/**").addResourceLocations("file:/D:\\img\\");
    }

    //转换器  String -->  Date
    //在IOC容器中 加入自定义日期转换器
   /* @Bean
    public Converter<String, Date> stringToDateConverter(){
        return new MyDateConverter();
    }
    //重写方法：在springMVC配置中添加自定义转换器
    @Override
    public void addFormatters(FormatterRegistry registry) {
        //会寻找 ioc容器中 stringToDateConverter()方法的实例
        registry.addConverter(stringToDateConverter());
    }*/


    //配置校验信息的配置文件
    @Bean
    public ReloadableResourceBundleMessageSource messageSource(){
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        //资源文件名
        messageSource.setBasenames("classpath:CustomValidationMessages");
        //资源文件编码格式
        messageSource.setDefaultEncoding("UTF-8");
        //对资源文件内容缓存时间，单位秒
        messageSource.setCacheSeconds(120);
        return messageSource;
    }

    //配置校验器
    @Bean
    public LocalValidatorFactoryBean validator(){
        LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
        //校验器的提供方为hibernate的HibernateValidator
        factoryBean.setProviderClass(HibernateValidator.class);
        //指定校验使用的资源文件，在文件中配置校验时的错误信息，如果不指定则默认使用classpath下的ValidationMessages.properties
        factoryBean.setValidationMessageSource(messageSource());
        return factoryBean;
    }

    //注入校验器到springMVC中
    @Override
    public Validator getValidator() {
        return validator();
    }

    //在IOC容器中注入 自定义全局异常处理器  @Bean
   /*
   @Bean
    public HandlerExceptionResolver handlerExceptionResolver(){
        return new MyHandlerExceptionResolver();
    }

    //springMVC中注入全局异常处理器 @Override
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {
        resolvers.add(handlerExceptionResolver());
    }
    */

    //配置拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       /*
       registry.addInterceptor(new MyInterceptor1())   //在springMVC中注入 自定义拦截器
                .addPathPatterns("/**")                 //对当前拦截器配置拦截路径
                .excludePathPatterns("");               //对当前拦截器配置除外路径
        registry.addInterceptor(new MyInterceptor2())
                .addPathPatterns("/**");
        */
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/rest/user/userLogin.html","/rest/login"); //常见的登录校验拦截 的除外路径：和登录注册有关的请求
    }
}
