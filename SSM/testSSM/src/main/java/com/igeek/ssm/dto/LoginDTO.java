package com.igeek.ssm.dto;

/**
 * @Description 封装登陆所需的请求数据
 * @Author fqq
 * @Date 2023/11/30 15:24
 */
public class LoginDTO {
    private String name;//登陆时填写的用户名
    private String pwd;//登陆时填写的密码

    public LoginDTO() {
    }

    public LoginDTO(String name, String pwd) {
        this.name = name;
        this.pwd = pwd;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return pwd
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * 设置
     * @param pwd
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String toString() {
        return "LoginDTO{name = " + name + ", pwd = " + pwd + "}";
    }
}
