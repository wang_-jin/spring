package com.igeek.ssm.mapper;

import com.igeek.ssm.config.MybatisConfig;
import com.igeek.ssm.pojo.OrderdetailCustom;
import com.igeek.ssm.pojo.OrdersCustom;
import com.igeek.ssm.pojo.UserCustom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;

import static org.junit.Assert.*;

//复合注解 ：加载spring环境 && 支持单元测试
@SpringJUnitConfig(MybatisConfig.class)
public class UserCustomMapperTest {
    @Autowired(required = false)
    private UserCustomMapper userCustomMapper;

    @Test
    public void findUserAndOrdersByUid() {
        UserCustom userCustom = userCustomMapper.findUserAndOrdersByUid(1);
        System.out.println("用户名："+userCustom.getName());

        List<OrdersCustom> ordersCustomList = userCustom.getOrdersCustomList();
        ordersCustomList.forEach(ordersCustom -> {
            System.out.println("---订单号：--"+ordersCustom.getNumber());
            List<OrderdetailCustom> orderdetails = ordersCustom.getOrderdetails();
            orderdetails.forEach(orderdetail->{
                System.out.println("订单数量 = " + orderdetail.getItemsNum());
                System.out.println("商品名称 = " + orderdetail.getItems().getName());
            });
        });
    }

}