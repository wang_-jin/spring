package com.igeek.ssm.mapper;

import com.igeek.ssm.config.MybatisConfig;
import com.igeek.ssm.pojo.OrdersCustom;
import com.igeek.ssm.pojo.UserCustom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.Assert.*;
@SpringJUnitConfig(MybatisConfig.class)
public class OrdersCustomMapperTest {
    @Autowired(required = false)
    private OrdersCustomMapper ordersCustomMapper;

    @Test
    public void selectOrderAndUserByOid() {
        OrdersCustom ordersCustom = ordersCustomMapper.selectOrderAndUserByOid(3);
        System.out.println("ordersCustom = " + ordersCustom);
        UserCustom userCustom = ordersCustom.getUserCustom();
        System.out.println("userCustom = " + userCustom);
    }
}