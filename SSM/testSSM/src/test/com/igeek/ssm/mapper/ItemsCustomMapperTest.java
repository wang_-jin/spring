package com.igeek.ssm.mapper;

import com.igeek.ssm.config.MybatisConfig;
import com.igeek.ssm.pojo.ItemsCustom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Date;

import static org.junit.Assert.*;
@SpringJUnitConfig(MybatisConfig.class)
public class ItemsCustomMapperTest {
    @Autowired(required = false)
    private ItemsCustomMapper itemsCustomMapper;
    @Test
    public void insertItem() {
        ItemsCustom itemsCustom = new ItemsCustom();
        itemsCustom.setName("耐克");
        itemsCustom.setPrice(100.0F);
        itemsCustom.setDetail("大牌联名!!!厂家直销!!!");
        itemsCustom.setPic("/pic/user.jpg");
        itemsCustom.setCreatetime(new Date());
        int i = itemsCustomMapper.insertItem(itemsCustom);
        System.out.println(i>0?"插入成功":"插入失败");
    }
}