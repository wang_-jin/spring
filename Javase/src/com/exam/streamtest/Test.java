package com.exam.streamtest;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/16 16:50
 * @description TODO
 */
public class Test {


    public static void main(String[] args) {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
        //(1) 找出2011年发生的所有交易，并按交易额排序（从低到高）。
        transactions.stream().filter(transaction -> transaction.getYear()==2011).sorted((o1, o2) -> o1.getValue()-o2.getValue()).forEach(System.out::println);
        //(2) 交易员都在哪些不同的城市工作过？
        System.out.println("---------------");
        transactions.stream().map(transaction -> transaction.getTrader().getCity()).distinct().forEach(System.out::println);
        //(3) 查找所有来自于剑桥的交易员，并按姓名排序。
        System.out.println("---------------");
        transactions.stream().filter(transaction -> transaction.getTrader().getCity().equals("Cambridge")).sorted(Comparator.comparing(transaction -> transaction.getTrader().getName())).forEach(System.out::println);
        //(4) 返回所有交易员的姓名字符串，按字母顺序排序。
        System.out.println("---------------");
        transactions.stream().map(transaction -> transaction.getTrader().getName()).sorted().forEach(System.out::println);
        //(5) 有没有交易员是在米兰工作的？
        System.out.println("---------------");
        boolean flag = transactions.stream().anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
        System.out.println(flag?"有在米兰工作":"没有");
        //(6) 打印生活在剑桥的交易员的所有交易额。
        System.out.println("---------------");
        transactions.stream().filter(transaction -> transaction.getTrader().getCity().equals("Cambridge")).map(Transaction::getValue).forEach(System.out::println);
        //(7) 所有交易中，最高的交易额是多少？
        System.out.println("---------------");
        Integer max = transactions.stream().map(Transaction::getValue).max(Integer::compareTo).orElse(0);
        System.out.println("最高的交易额是:"+max);
        //(8) 找到交易额最小的交易。
        System.out.println("---------------");
        Integer min = transactions.stream().map(Transaction::getValue).min(Integer::compareTo).orElse(0);
        System.out.println("最高的交易额是:"+min);

    }

}