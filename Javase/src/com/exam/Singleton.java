package com.exam;

/**
 * @author wangjin
 * 2023/9/16 11:11
 * @description TODO
 */
public class Singleton {
    //懒汉式单例 类加载时不会创建,再第一次调用对象时会被创建
/*    private Singleton (){}
    private static Singleton instance ;
    public static Singleton getInstance(){
        if (instance=null){
            instance = new Singleton();
        }
        return instance;
    }*/
    //饿汉式单例 类加载时就已创建
/*    private Singleton (){}
    private static Singleton intance = new Singleton();
    public static Singleton getInstance(){
        return intance;
    }*/
    //线程安全的静态内部类单例模式
    private Singleton(){}
    static class InnerClass{
        private static Singleton instance = new Singleton();
    }
    public static Singleton getInstance(){
        return InnerClass.instance;
    }
}
class MainDemo{
    public static void main(String[] args) {
        /*Singleton singleton = Singleton.getInstance();
        Singleton singleton1 = Singleton.getInstance();*/
        Singleton singleton = Singleton.getInstance();
        Singleton singleton1 = Singleton.getInstance();
        System.out.println(singleton==singleton1?true:false);
    }
}

