package com.exam;

/**
 * @author wangjin
 * 2023/9/16 16:14
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {

        int x = 2, y = 2, z = 2;

        if (x-- == 2 && y++ == 2 || z++ == 2) {
            System.out.println("x=" + x + ",y=" + y + ",z=" + z);
        }
        //输出结果为:
        //x=1,y=3,z=2

        System.out.println("=====================================");

    Integer var1 = new Integer(1);

    Integer var2 = var1;

    doSomething(var2);

    System.out.println(var1);
    System.out.println(var2);

    System.out.println(var1==var2);
    //输出结果为:
    //1 ,true


        System.out.println("=====================================");
        int x1 = fun();
        System.out.println(x1);
        //输出结果为:
        //Exception
        //i am in finally
        //-1
}


    public static void doSomething(Integer integer){
        integer=new Integer(2);
    }
    public static int fun(){

        int result = 5;

        try{

            result = result/0;

            return result;

        }catch(Exception e){

            System.out.println("Exception");

            result =  -1;

            return result;

        }finally{

            System.out.println("i am in finally");

        }

    }
}