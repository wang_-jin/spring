package com.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/9/16 15:58
 * @description TODO
 */
public class RegexTest {
    public static void main(String[] args) {
        /*5.提取字符串中的“-3.14good87nice19bye”的数值(数值包括正负数、整数和小数在内)并求和*/
        String s = "-3.14good87nice19bye";
        Pattern pattern = Pattern.compile("[-]*\\d+[.]*\\d+");
        Matcher matcher = pattern.matcher(s);
        List<Double>list = new ArrayList<>();
        Double sum = 0.0;
        while (matcher.find()){
            System.out.println(matcher.group());
            list.add(Double.parseDouble(matcher.group()));
        }
        for (Double i : list) {
            sum+=i;
        }
        System.out.println("sum="+sum);
    }
}
