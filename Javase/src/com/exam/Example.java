package com.exam;

/**
 * @author wangjin
 * 2023/9/16 11:03
 * @description TODO
 */
class Person {
    static void sayHello() {
        System.out.println("HelloWorld!");
    }
}

public class Example {
    public static void main(String[] args) {
        ((Person) null).sayHello();
    }
}
