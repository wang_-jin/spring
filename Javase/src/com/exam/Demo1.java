package com.exam;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/16 10:40
 * @description TODO
 */
public class Demo1 {

    public static void main(String[] args) {
        ArrayList al = new ArrayList();
        al.add("a");
        al.add("b");
        al.add("c");

        Iterator it = al.iterator();
        while(it.hasNext()){
            String s = (String)it.next();

            if(s.equals("c")){
                al.add("c1");
            }
        }

        System.out.println(al);
    }
}
