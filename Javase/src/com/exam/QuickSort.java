package com.exam;

/**
 * @author wangjin
 * 2023/9/16 11:48
 * @description TODO
 */
/*.控制台输入一组数据，请使用快速排序方式进行排序。*/
public class QuickSort {
    public static void quickSort(int[] arr,int low,int high) {
        if (low>high){
            return;
        }
        int i = low;
        int j = high;
        int temp = arr[low];
        while(i<j){
            while (arr[j]>=temp&&i<j){
                j--;
            }
            while (arr[i]<=temp&&i<j){
                i++;
            }
            if (i!=j) {
                int tem = arr[i];
                arr[i] = arr[j];
                arr[j] = tem;
            }
        }
        arr[low] = arr[i];
        arr[i] = temp;
        quickSort(arr,low,j-1);
        quickSort(arr,j+1,high);
    }


    public static void main(String[] args){
        int[] arr = {7,1,2,6,9,10,3,22};
        quickSort(arr, 0, arr.length-1);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
