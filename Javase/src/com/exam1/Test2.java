package com.exam1;

/**
 * @author wangjin
 * 2023/9/25 9:07
 * @description TODO
 */
/*2.如何在线程B之后执行线程A，线程C在线程B之前执行？
 * 需求：按照 C -> B -> A 的顺序输出
 */
public class Test2 {
    public static void main(String[] args) {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"开始执行了");
        },"线程C").start();

        new Thread(()->{
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"开始执行了");
        },"线程B").start();

        new Thread(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"开始执行了");
        },"线程A").start();
    }
}
