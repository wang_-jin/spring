package com.exam1;

/**
 * @author wangjin
 * 2023/9/25 11:04
 * @description TODO
 */
public class Singleton {
    private Singleton(){}
    static class InnerClass{
        private static Singleton instance = new Singleton();
    }
    public static Singleton getInstance(){
        return InnerClass.instance;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            new Thread(()->{
                    Singleton instance = Singleton.getInstance();
                    System.out.println(Thread.currentThread().getName()+"执行了"+instance);
            },"线程"+i).start();
        }
    }

}
