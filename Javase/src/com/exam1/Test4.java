package com.exam1;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author wangjin
 * 2023/9/25 11:19
 * @description TODO
 */
/*public class Test2 {
    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier(7,()-> System.out.println("召唤神龙!!!"));
        for (int i = 1; i <= 7 ; i++) {
            new Thread(()->{
                System.out.println("收集"+Thread.currentThread().getName());
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },"龙珠"+i).start();
        }
    }
}*/
public class Test4 {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(5,()-> System.out.println("可以开饭了!"));
        for (int i = 1; i <= 5; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"到了!");
                try {
                    cyclicBarrier.await();
                } catch (BrokenBarrierException | InterruptedException e) {
                    e.printStackTrace();
                }
            },"学生"+i).start();
        }
    }
}
