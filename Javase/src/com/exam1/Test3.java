package com.exam1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/9/25 9:13
 * @description TODO
 */
/*
  创建一个长度是200的字符串数组，使用长度是2的随机字符填充该字符串数组，统计这个字符串数组里重复的字符串有多少种？
*/
public class Test3 {
    public static void main(String[] args) {
        String[] arr = new String[10];
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        Random random = new Random();
        int count = 0;
        //添加100个长度为2的随机字符串
        while(count<10) {
            String s = null;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 2; i++) {
                char c = chars[random.nextInt(chars.length)];
                sb.append(c);
            }
            s = sb.toString();
            arr[count] = s;
            count++;
        }
        System.out.println(Arrays.toString(arr));
        AtomicInteger charCount = new AtomicInteger();
        Map<String, Integer> map = new HashMap<>();
        for (String s : arr) {
            int i = map.getOrDefault(s, 0) + 1;
            map.put(s,i);
        }
        System.out.println(map);
        map.forEach((key,values)->{
            if (values>1){
                charCount.getAndIncrement();
            }
        });
        System.out.println("重复字符串种数:"+charCount);
    }
}
