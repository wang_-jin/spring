package com.exam1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/25 9:04
 * @description TODO
 */
/* * 以当前时间为起始日期格式为yyyMMdd，控制台提示 “当前是yyyMMdd，请输入创建的文件夹天数”，
 * 通过键盘输入天数，在D盘date文件夹下生成连续的日期文件夹，
 * 并在每个日期文件夹中生成对应的文件，文件名为当前日期.md,文件中的内容为:这是 [对应日期] 的文件夹。
 *
 * 举例:当前为2022/11/30,控制台输入2，则最终文件夹及文件结构为:
 * D:\\date\\20221201\\20221201.md
 *         \\20221202\\20221202.md
 * 20221201.md文件中内容为这是20221201的文件夹。*/
public class Test1 {
    public static void main(String[] args) {
        //日期格式
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        //获取当前日期
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar);
        Date currentDate = calendar.getTime();
        String time = dateFormat.format(currentDate);
        Scanner sc = new Scanner(System.in);
        System.out.println("当前是"+time+", 请输入创建的文件夹天数:");
        int num = sc.nextInt();
        //新建date目录
        File parentFolder = new File("D://date");
        if (!parentFolder.exists()) {
            parentFolder.mkdirs();
        }

        for (int i = 0; i < num; i++) {
            calendar.add(Calendar.DATE, -1);
            Date previousDate = calendar.getTime();
            String previousDateString = dateFormat.format(previousDate);
            //新建日期目录
            File folder = new File(parentFolder, previousDateString);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            //新建md文件
            File file = new File(folder, previousDateString + ".md");
            try {
                FileWriter writer = new FileWriter(file);
                writer.write("这是 " + previousDateString + " 的文件夹。");
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("文件夹和文件创建完成.");
    }
}
