package com.exam1;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/25 11:40
 * @description TODO
 */
class Product implements Runnable{
    private Clerk clerk;

    public Product(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 15; i++) {
            clerk.add();
        }
    }
}
class Customer implements Runnable{
    private Clerk clerk;

    public Customer(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 15; i++) {
            clerk.sale();
        }
    }
}
public class Clerk {
    //创建锁
    private static Lock lock = new ReentrantLock();
    //创建Condition实例
    private static Condition condition = lock.newCondition();
    private int num = 0;
    public void add(){
        lock.lock();
        try {
            while (num >= 10) {
                System.out.println("仓库已满!");
                condition.await();
            }
            System.out.println(Thread.currentThread().getName() + "生产了" + (++num));
            condition.signalAll();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void sale(){
        lock.lock();
        try{
            while(num<=0){
                System.out.println("仓库空了");
                condition.await();
            }
            System.out.println(Thread.currentThread().getName() + "消费了" + (num--));
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        Clerk clerk = new Clerk();
        new Thread(new Product(clerk),"生产者1").start();
        new Thread(new Product(clerk),"生产者2").start();
        new Thread(new Customer(clerk),"消费者1").start();
        new Thread(new Customer(clerk),"消费者2").start();
    }
}
