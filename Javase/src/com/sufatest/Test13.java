package com.sufatest;

import net.sf.cglib.core.Local;

import java.time.LocalDate;

/**
 * @author wangjin
 * 2023/10/18 20:36
 * @description TODO
 */
/*13 输入某年某月某日，判断这一天是这一年的第几
天？
14 输入三个整数x,y,z，请把这三个数由小到大输出。
15 输出9*9口诀。*/
public class Test13 {
    public static void main(String[] args) {
        String str = "2020-02-18";
        LocalDate localDate = LocalDate.parse(str);
        int dayOfYear = localDate.getDayOfYear();
        System.out.println("dayOfYear = " + dayOfYear);
    }
}
