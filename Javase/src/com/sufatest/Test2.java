package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 11:32
 * @description TODO
 */
/*2 题目：判断1-1000之间有多少个素数，并输出所
有素数。*/
public class Test2 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 2; i <= 1000; i++) {
            boolean flag = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i%j==0) {
                    flag = false;
                    break;
                }
            }
            if (flag){
                sum++;
                System.out.println(i + "为素数");
            }
        }
        System.out.println("sum = " + sum);
    }
}
