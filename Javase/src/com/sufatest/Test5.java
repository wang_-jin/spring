package com.sufatest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*5 利用条件运算符的嵌套来完成此题：学习成绩> =90
分的同学用A表示，60-89分之间的用B表示，60分以
下的用C表示。*/
public class Test5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入成绩:");
        int grade = sc.nextInt();
        System.out.println(grade>=90?"A":(grade>=60)?"B":"C");
    }
}
