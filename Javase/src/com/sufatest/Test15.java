package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 20:38
 * @description TODO
 */
/*15 输出9*9口诀*/
public class Test15 {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            System.out.println();
            for (int j = 1; j <= i; j++) {
                System.out.print(j+"*"+i+"="+i*j+" ");
            }
        }
    }
}
