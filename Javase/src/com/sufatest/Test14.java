package com.sufatest;

import javax.script.ScriptContext;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/10/18 20:38
 * @description TODO
 */
/*14 输入三个整数x,y,z，请把这三个数由小到大输出。
*/
public class Test14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入三个数:");
        int x = sc.nextInt();
        int y = sc.nextInt();
        int z = sc.nextInt();

        System.out.println(z>y?(y>x?x+""+y+""+z:y+""+x+""+z):(y<x?z+""+y+""+x:(z>x?x+""+z+""+y:z+""+x+""+y)));
    }
}
