package com.sufatest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*9 一个数如果恰好等于它的因子之和，这个数就称为 "完
数 "。例如6=1＋2＋3.编程找出1000以内的所有完数。
6=1+2+3
28=1+2+4+7+14
496=1+2+4+8+16+31+62+124+248*/
public class Test9 {
    public static void main(String[] args) {
        for (int i = 1; i <= 1000; i++) {
            int sum = 0;
            for (int j = 1; j <= i/2; j++) {
                if (i%j==0){
                    sum+=j;
                }
            }
            if (sum==i){
                System.out.println("完数"+i);
            }
        }
    }
}
