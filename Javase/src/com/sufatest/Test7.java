package com.sufatest;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*7 scanf输入一行字符(不含空格)，分别统计出其中英文字
母、数字和其它字符的个数。*/
public class Test7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String str = sc.next();
        int charNum = 0;
        int num = 0;
        Pattern pt = Pattern.compile("[a-zA-Z]");
        Pattern pt1 = Pattern.compile("[0-9]");
        Matcher matcher = pt.matcher(str);
        Matcher matcher1 = pt1.matcher(str);
        while(matcher.find()){
            charNum++;
        }
        while (matcher1.find()){
            num++;
        }
        System.out.println("字母个数 = " + charNum);
        System.out.println("数字个数 = " + num);
        System.out.println("其它字符个数 = " + (str.length()-charNum-num));
    }
}
