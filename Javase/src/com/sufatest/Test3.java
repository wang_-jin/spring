package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 11:32
 * @description TODO
 */
/*3 题目：打印出所有的 "水仙花数 "，所谓 "水仙花数
"是指一个三位数，其各位数字立方和等于该数本身。
例如：153是一个 "水仙花数 "，因为153=1的三次
方＋5的三次方＋3的三次方。 */
public class Test3 {
    public static void main(String[] args) {
        int sum = 0;
        int num = 127;//定义一个数
        sum = (num%10)*(num%10)*(num%10);//模10 取个位数的三次方
        int count = 10;//此为记录给定数(num)的位数
        while(num/count>0){// 取尽每一位的数
            sum+=((num/count)%10)*((num/count)%10)*((num/count)%10);//从十位数开始模10取余
            count=count*10;
        }
        System.out.println(sum==num?"是水仙花数":"不是水仙花数");
    }
}
