package com.sufatest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*6 输入两个正整数m和n，求其最大公约数和最小公倍数。
程序分析：利用辗除法。*/
public class Test6 {
    public static void main(String[] args) {
        int m,n;
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入m和n:");
        m = sc.nextInt();
        n = sc.nextInt();
        int num1,num2;
        //临时数据用于交换和赋值
        num1 = m;
        num2 = n;
        while(num1%num2!=0){//若两数相除不等于0则执行 大数除以小数操作
            if (num1/num2<0){//被除数要比除数大
                int temp = num2;
                num2 = num1;
                num1 = temp;
            }
            int b = num1%num2;//用余数来代替下一个除数
            num1 = num2;
            num2 = b;
        }
        System.out.println("最大公因数"+num2);
        System.out.println("最小公倍数"+(m*n/num2));
    }
}
