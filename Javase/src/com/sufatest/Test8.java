package com.sufatest;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*8 求s=a+aa+aaa+aaaa+aa...a的值，其中a是数字.
用户输入a和n。例如输入a=2
n=5 ,S=2+22+222+2222+22222*/
public class Test8 {
    public static void main(String[] args) {
        int a,n;
        int sum = 0;
        int s = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入a和n:");
        a = sc.nextInt();
        n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            sum+= a * Math.pow(10,i-1);
            System.out.println(sum);
            s+= sum;
        }
        System.out.println("s = " + s);
    }
}
