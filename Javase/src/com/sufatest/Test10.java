package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*10 一球从100米高度自由落下，每次落地后反跳回原高
度的一半；再落下，求它在 第10次落地时，共经过多
少米？第10次反弹多高？*/
public class Test10 {
    public static void main(String[] args) {
        int sum = 100;
        int height = 100;
        for (int i = 2; i <= 10; i++) {
            if (height>0){
                System.out.println("height = " + height);
            }
            height = height/2;
            sum = sum+height*2;
        }
        System.out.println("共经过多"+sum+"米");
        System.out.println("第10次反弹"+height+"米");
    }
}
