package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 11:33
 * @description TODO
 */
/*12 一个整数，它加上100后是一个完全平方数，加上
168又是一个完全平方数，请问该数是多少？*/
public class Test12 {
    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            int x = (int)Math.sqrt(i+100);
            int y = (int)Math.sqrt(i+100+168);
            if (x*x==(i+100)&&y*y==(i+100+168)){
                System.out.println(i);
            }
        }
    }
}
