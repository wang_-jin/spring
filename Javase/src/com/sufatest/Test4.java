package com.sufatest;

/**
 * @author wangjin
 * 2023/10/18 11:32
 * @description TODO
 */
/*4 将一个正整数分解质因数。例如：输入90,打印出
90=2*3*3*5。
程序分析：对n进行分解质因数，应先找到一个最小的
质数k，然后按下述步骤完成：
(1)如果这个质数恰等于n，则说明分解质因数的过程已
经结束，打印出即可。
(2)如果n > k，但n能被k整除，则应打印出k的值，并
用n除以k的商,作为新的正整数,重复执行第一步。
(3)如果n不能被k整除，则用k+1作为k的值,重复执行
第一步。*/
public class Test4 {
    public static void main(String[] args) {

    }
}
