package com.sufatest;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author wangjin
 * 2023/10/21 19:46
 * @description TODO
 */
public class TestJD1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int N = 100004;
        Set<Integer> set = new TreeSet<>();
        int[] arrA = new int[N];
        int[] arrB = new int[N];
        for (int i = 1; i <= n; i++) {
            arrA[i] = in.nextInt();
        }
        //3,4,7,8,10
        //1,6,2,4,5
        for (int i = 1; i <= n; i++) {
            int x = (arrA[i]%i+i)%i;
            if (x==0){
                x = i;
            }
            while (set.contains(x)) x+= i;
            arrB[i] = x;
            System.out.println(arrB[i]);
            if (i == n){
                System.out.println();
            }else {
                System.out.println(" ");
            }
            set.add(x);
        }
    }
}
