package com.leetcode;

import java.util.*;

/**
 * @author wangjin
 * 2023/10/11 21:03
 * @description TODO
 */
/*给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
字母异位词 是由重新排列源单词的所有字母得到的一个新单词。*/
public class Solution_49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        //map 来存每个 strs数组的字符串 中的内部字符重排 后的值
        Map<String,List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            //获取重排后的字符串
            String key = new String(chars);
            //让 list 存不重复的字符串,不重复则新建ArraysList,重复则在原list集合中操作
            List<String> list = map.getOrDefault(key,new ArrayList<>());
            list.add(str);
            //将每个list添加到map中,key值目的是防止存入重复
            map.put(key,list);
        }
        return new ArrayList<>(map.values());
    }
}
