package com.leetcode;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author wangjin
 * 2023/11/13 20:24
 * @description TODO
 */
public class Solution_739 {
    public int[] dailyTemperatures(int[] temperatures) {
        int[] result = new int[temperatures.length];
        Arrays.fill(result,0);
        LinkedList<Integer> st = new LinkedList<>();
        st.add(0);
        for (int i = 1; i < temperatures.length; i++) {
            //若遍历到的元素比单调栈的元素小时,进行添加栈操作
            if (temperatures[i]<=temperatures[st.getFirst()]){
                st.push(i);
            }else {
                //若遍历到的元素比单调栈的元素大时,进行弹出栈操作,并给计算result数组的值,直到不大于停止,
                // 并将遍历的元素下标放到单调栈中
                while(!st.isEmpty() && temperatures[i]>temperatures[st.getFirst()]){
                    result[st.getFirst()] = i-st.getFirst();
                    st.pop();
                }
                st.push(i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solution_739 solution_739 = new Solution_739();
        int[] arr = {73,74,75,71,69,72,76,73};
        int[] array = solution_739.dailyTemperatures(arr);
        System.out.println("array = " + Arrays.toString(array));
    }
}
