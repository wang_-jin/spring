package com.leetcode;

/**
 * @author wangjin
 * 2023/10/6 2:09
 * @description TODO
 */
/*给定一个含有 n 个正整数的数组和一个正整数 target 。
找出该数组中满足其总和大于等于 target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，
并返回其长度。如果不存在符合条件的子数组，返回 0 。*/
public class Solution_209 {
    public int minSubArrayLen(int target, int[] nums) {
        int left = 0;
        int right = 0;
        int len = Integer.MAX_VALUE;//初始化数据
        int sum = 0;
        while(right < nums.length){
            sum+=nums[right++];
            while (sum>=target){//若sum大于目标值则计算窗口长度
                len = Math.min(len,right-left);
                sum-=nums[left++];//sum值减去做窗口数值并移动左窗口
            }
        }
        return len==Integer.MAX_VALUE?0:len;
    }
    public static void main(String[] args) {
        int[] nums = {1,1,1,1,1,1,1,1};
        Solution_209 solution209 = new Solution_209();
        System.out.println("solution19 = " + solution209.minSubArrayLen(11,nums));
    }
}
