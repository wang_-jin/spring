package com.leetcode;

/**
 * @author wangjin
 * 2023/9/27 20:47
 * @description TODO
 */
/*给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使得出现次数超过两次的元素只出现两次 ，返回删除后数组的新长度。
不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。*/
public class Solution_80 {
    public int removeDuplicates(int[] nums) {
        int n = 0;
        for (int num : nums) {
            if (n < 2  || num>nums[n-2]){//从第三个元素开始,与往左数三个的数的差大于0则重赋值
                nums[n++] = num;
            }
        }
        return n;
    }
}
