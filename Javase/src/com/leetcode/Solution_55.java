package com.leetcode;

/**
 * @author wangjin
 * 2023/10/25 19:17
 * @description TODO
 */
/*给你一个非负整数数组 nums ，你最初位于数组的 第一个下标 。数组中的每个元素代表你在该位置可以跳跃的最大长度。

判断你是否能够到达最后一个下标，如果可以，返回 true ；否则，返回 false 。*/
public class Solution_55 {
    public boolean canJump(int[] nums) {
        //贪心算法
        if (nums.length==1) return true;
        int cover = 0;//覆盖范围
        for (int i = 0; i <= cover; i++) {
            cover = Math.max(cover,nums[i]+i);//更新覆盖范围,
            if (cover >= nums.length-1) return true;//直到范围覆盖到最后一个值就返回true
        }
        return false;
    }

    public static void main(String[] args) {
        Solution_55 solution_55 = new Solution_55();
        int[] nums = {3,2,1,0,4};
        boolean b = solution_55.canJump(nums);
        System.out.println(b);
    }
}
