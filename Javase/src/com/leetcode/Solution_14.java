package com.leetcode;

/**
 * @author wangjin
 * 2023/11/15 22:30
 * @description TODO
 */
public class Solution_14 {
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        int length = strs[0].length();
        int count = strs.length;
        //以第一个字符串为模板进行逐个字符遍历
        for (int i = 0; i < length; i++) {
            char c = strs[0].charAt(i);
            for (int j = 1; j < count; j++) {
                // i == strs[j].length() 若数组中有长度比遍历的下标小的字符串,则直接截取到改下标
                if (i == strs[j].length() || strs[j].charAt(i) != c) {
                    return strs[0].substring(0, i);
                }
            }
        }
        return strs[0];
    }
    public static void main(String[] args) {
        String[] strs = {"ab", "a"};
        Solution_14 solution_14 = new Solution_14();
        String s = solution_14.longestCommonPrefix(strs);
        System.out.println("s = " + s);
    }
}
