package com.leetcode;

/**
 * @author wangjin
 * 2023/9/6 23:10
 * @description TODO
 */
public class Solution_5 {
            /*给你一个字符串 s，找到 s 中最长的回文子串。
            如果字符串的反序与原始字符串相同，则该字符串称为回文字符串。*/
    public String longestPalindrome(String s) {
        //暴力解法
        if (s.length()<2){
            return s;
        }
        int maxlen = 1;//最大回文字串长度
        int begin = 0;//最大回文子串开始索引
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length-1; i++) {
            for (int j = i+1; j < chars.length; j++) {
                if (j-i+1>maxlen&&isPalindrome(chars,i,j)){
                    maxlen = j-i+1;
                    begin = i;
                }
            }
        }
        return s.substring(begin,begin+maxlen);
    }
    //判断回文字符串
    public boolean isPalindrome(char[] chars, int left ,int right){
        while (left<right){
            if (chars[left]!=chars[right]){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
    public static void main(String[] args) {
        Solution_5 solution_5 = new Solution_5();
        String s = solution_5.longestPalindrome("abababab");
        System.out.println(s);
    }
}

