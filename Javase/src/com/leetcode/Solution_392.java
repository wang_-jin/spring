package com.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/27 0:02
 * @description TODO
 */
/*给定字符串 s 和 t ，判断 s 是否为 t 的子序列。
字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。
（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。*/
public class Solution_392 {
    public boolean isSubsequence(String s, String t) {
        if (s.length()==0){
            return true;
        }
        char[] chars =  s.toCharArray();
        List<Character> list = new ArrayList<>();
        for (char aChar : chars) {
            list.add(aChar);
        }
        System.out.println(list);
        int count = 0;
        for (int i = 0; i < t.length(); i++) {
            if (count <= list.size()-1 && list.get(count).equals(t.charAt(i))){
                count++;
            }
        }
        return count==s.length();
    }

    public static void main(String[] args) {
        Solution_392 solution392 = new Solution_392();
        System.out.println(solution392.isSubsequence("b", "ahbgde"));
    }
}
