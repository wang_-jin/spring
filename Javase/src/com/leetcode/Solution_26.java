package com.leetcode;

/**
 * @author wangjin
 * 2023/9/22 20:48
 * @description TODO
 */
/*给你一个 非严格递增排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，
返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。然后返回 nums 中唯一元素的个数。*/
public class Solution_26 {
    public int removeDuplicates(int[] nums) {
        if (nums.length<2){
            return 1;
        }
        int key = 0;//记录新存不重复数字的索引
        int max = 0;//新序列最大值
        for (int i = 1; i <= nums.length-1; i++) {
                if (nums[i]!=nums[i-1]){//若左边数字不等于右边数字则进行重赋值操作
                    nums[key] = nums[i-1];
                    nums[key+1] = nums[i];
                    key++;
                }
                max = Math.max(max, key+1);
            }
        return max;
    }
    public static void main(String[] args) {
        System.out.println("new Solution9().removeDuplicates(new int[]{1,2}) = " + new Solution_26().removeDuplicates(new int[]{1,2}));
    }
}
