package com.leetcode;

/**
 * @author wangjin
 * 2023/10/26 18:25
 * @description TODO
 */
/*给你一个字符串 s ，请你反转字符串中 单词 的顺序。

单词 是由非空格字符组成的字符串。s 中使用至少一个空格将字符串中的 单词 分隔开。

返回 单词 顺序颠倒且 单词 之间用单个空格连接的结果字符串。

注意：输入字符串 s中可能会存在前导空格、尾随空格或者单词间的多个空格。返回的结果字符串中，
单词间应当仅用单个空格分隔，且不包含任何额外的空格。*/
public class Solution_151 {
    public String reverseWords(String s) {
        //(1) 设置两个指向字符串末位的指针 i 和 j。
        //(2) 令 j 从后往前扫描字符串里的每个字符，遇到非空格的时候停一下，用 i 记录这个位置。
        //(3) j 继续往前走，当 j 再次遇到空格的时候就停下来。
        //(4) 此时的 i 和 j 就停在了最后一个单词的两端，用StringBuilder 的 append 方法连接即可。
        //时间复杂度O(N)：其中N为字符串s的长度 空间复杂度O(N)：新建的StringBuilder会占用O(N)的额外空间

        int n = s.length();
        int i = n-1;//标识单词结束
        int j = n-1;//标识单词开始
        StringBuilder res = new StringBuilder();
        while (j >= 0) {
            while (j >= 0 && s.charAt(j) == ' ') j--;
            i = j;
            while (j >= 0 && s.charAt(j) != ' ') j--;
            res.append(s.substring(j + 1, i + 1) + " ");
        }
        return res.toString().trim();//最后去掉两端多余的空格
    }

    public static void main(String[] args) {
        String s = "  hello world  ";
        Solution_151 solution_151 = new Solution_151();
        String s1 = solution_151.reverseWords(s);
        System.out.println("s1 = " + s1);
    }
}
