package com.leetcode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/9/10 16:37
 * @description TODO
 */
public class Solution_125 {
    public boolean isPalindrome(String s) {
        String s1 = s.toLowerCase();
        StringBuilder sb = new StringBuilder();
        Pattern ptn = Pattern.compile("[a-zA-Z0-9]");
        Matcher matcher = ptn.matcher(s1);
        while(matcher.find()){
            sb.append(matcher.group());
        }
        String stringBefor = sb.toString();
        String stringAfter = sb.reverse().toString();
        System.out.println(stringBefor);
        System.out.println(stringAfter);
        if (stringBefor.equals(stringAfter)){
            return true;
        }else {
            return false;
        }
    }
    /*如果在将所有大写字符转换为小写字符、并移除所有非字母数字字符之后，短语正着读和反着读都一样。
    则可以认为该短语是一个 回文串 。
    字母和数字都属于字母数字字符。
    给你一个字符串 s，如果它是 回文串 ，返回 true ；否则，返回 false 。*/
    public static void main(String[] args) {
        String s = "ab_a";
        Solution_125 solution125 = new Solution_125();
        System.out.println(solution125.isPalindrome(s));
    }
}
