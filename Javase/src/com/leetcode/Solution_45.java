package com.leetcode;

/**
 * @author wangjin
 * 2023/10/25 21:25
 * @description TODO
 */
/*给定一个长度为 n 的 0 索引整数数组 nums。初始位置为 nums[0]。

每个元素 nums[i] 表示从索引 i 向前跳转的最大长度。换句话说，如果你在 nums[i] 处，你可以跳转到任意 nums[i + j] 处:

0 <= j <= nums[i]
i + j < n
返回到达 nums[n - 1] 的最小跳跃次数。生成的测试用例可以到达 nums[n - 1]。*/
public class Solution_45 {
    public int jump(int[] nums) {
        int cur = 0;
        int count = 0;
        int next = 0;
        for (int i = 0; i < nums.length; i++) {
            next = Math.max(next,nums[i]+i);
            if (i==cur){
                if (cur!= nums.length-1){
                    cur=next;
                    count++;
                }else if (cur>= nums.length-1){
                    break;
                }else {
                    break;
                }
            }
        }
        return count;
    }
    public static void main(String[] args) {
        Solution_45 solution_45 = new Solution_45();
        int[] nums = {2,3,1,1,4};
        int i = solution_45.jump(nums);
        System.out.println(i);
    }
}
