package com.leetcode;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/14 9:01
 * @description TODO
 * 给定一个整数数组 nums 和一个整数目标值 target，
 * 请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
 *
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 *
 * 你可以按任意顺序返回答案。
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]  0 3  1 2  2 1
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 */
public class Solution_1 {
    public  int[] twoSum(int[] nums,int target){
        int[] arr = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j <= nums.length-1; j++) {
                if (nums[i]+nums[j]==target){
                    arr[0] = i;
                    arr[1] = j;
                }
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] nums = {2,7,11,15};
        Solution_1 s = new Solution_1();
        int[] index = s.twoSum(nums, 13);
        System.out.println(Arrays.toString(index));
    }
}
