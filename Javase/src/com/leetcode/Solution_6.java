package com.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/11/6 20:49
 * @description TODO
 */
public class Solution_6 {
    public String convert(String s, int numRows) {
        if(numRows < 2) return s;
        List<StringBuilder> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            list.add(new StringBuilder());
        }
        int i = 0;
        int flag = -1;
        for (char c : s.toCharArray()) {
            list.get(i).append(c);
            if (i==0 || i==numRows-1){
                flag = -flag;
            }
            i+=flag;
        }
        StringBuilder str = new StringBuilder();
        for (StringBuilder stringBuilder : list) {
            str.append(stringBuilder);
        }
        return str.toString();
    }

    public static void main(String[] args) {
        Solution_6 solution_6 = new Solution_6();
        String s = solution_6.convert("PAYPALISHIRING",3);
        System.out.println("s = " + s);
    }
}
