package com.leetcode;

import java.util.*;

/**
 * @author wangjin
 * 2023/8/18 19:19
 * @description TODO
 * 给你一个 无重叠的 ，按照区间起始端点排序的区间列表。
 *
 * 在列表中插入一个新的区间，你需要确保列表中的区间仍然有序且不重叠（如果有必要的话，可以合并区间）。
 */
public class Test {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        int left = newInterval[0];
        int right = newInterval[1];
        List<int[]> list = new ArrayList<>();
        boolean flag = false;
        for (int[] interval : intervals) {
            if (interval[0]>right){
                if (!flag){
                    list.add(new int[]{left,right});
                    flag = true;
                }
                list.add(interval);
            }else if (interval[1]<left){
                list.add(interval);
            }else {
                left = Math.min(interval[0],left);
                right = Math.max(interval[1],right);
            }
        }
        if (!flag){
            list.add(new int[]{left,right});
        }
        int[][] arr = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }
    public static void add(Integer a,int b){
        System.out.println("a");
    }
    public static void add(int a,Integer b){
        System.out.println("a");
    }
    public static void main(String[] args) {
        List list = new ArrayList(20);
        int[][] intervals = {{1,3},{6,9}};
        int[] newInterval = {2,5};
        Test test = new Test();
        int[][] insert = test.insert(intervals, newInterval);
        System.out.println("insert = " + Arrays.deepToString(insert));
    }
}
