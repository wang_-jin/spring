package com.leetcode;

import java.util.*;

/**
 * @author wangjin
 * 2023/11/2 22:20
 * @description TODO
 */
/*给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。

如果可以，返回 true ；否则返回 false 。

magazine 中的每个字符只能在 ransomNote 中使用一次。*/
public class Solution_383 {
    public boolean canConstruct(String ransomNote, String magazine) {
        //定义一个长度为26的数组用于存放magazine里每个字母出现的次数
        int[] arr = new int[26];
        int temp = 0;
        for (int i = 0; i < magazine.length(); i++) {
            temp = magazine.charAt(i) - 'a';
            arr[temp]++;
        }
        for (int i = 0; i < ransomNote.length(); i++) {
            temp = ransomNote.charAt(i) - 'a';
            arr[temp]--;//在ransomNote里找到已存在的字母就将数组元素进行减一操作
            if (arr[temp]<0){//若数组里的元素出现负数 也即ransomNote不是magazine的子集就返回false
                return false;
            }
        }
       return true;
    }

    public static void main(String[] args) {
        String s1 = "aa";
        String s2 = "aab";
        Solution_383 solution_383 = new Solution_383();
        boolean b = solution_383.canConstruct(s1, s2);
        System.out.println("b = " + b);
    }
}
