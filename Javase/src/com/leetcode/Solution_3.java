package com.leetcode;

import java.util.*;


/**
 * @author wangjin
 * 2023/8/18 18:20
 * @description TODO
 * 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 */
public class Solution_3 {
    public int lengthOfLongestSubstring(String s) {

        //使用 ArrayList 中外部 remove 元素，会造成其内部结构和游标的改变
        /*使用 List 进行元素的添加或者删除操作，一定要使用迭代器进行删除*/

//        在使用 asList 时不要将基本数据类型当做参数。
//        不要试图改变 asList 返回的列表，否则你会自食苦果(add,remove不能用)
        //存放不重复字符子串
        Set<Character> set = new HashSet<>();
        int left = 0;//出现重复字符时,需要删除重复字符之前字符的索引
        int right = 0;//添加字符串的索引
        int max = 0;
        while(right<s.length()){
            if(!set.contains(s.charAt(right))){
                //未查到重复字符就一直加，right右移
                set.add(s.charAt(right));
                right++;
            }else{
                //right查到重复字符先不动，left右移，set删left经过的字符，直到重复的这个字符删掉为止
                set.remove(s.charAt(left));
                left++;
            }
            //每一次计算当前set子串的最大长度
            System.out.println(set);
            max = Math.max(max, set.size());
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Solution_3 solution1 = new Solution_3();
        //System.out.println("请输入字符串:");
        //String s = sc.next();
        int max = solution1.lengthOfLongestSubstring("abcabcbb");
        System.out.println("最长子串的长度:"+max);
    }
}
