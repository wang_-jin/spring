package com.leetcode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author wangjin
 * 2023/9/24 23:13
 * @description TODO
 */
/*给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
有效字符串需满足：
左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
每个右括号都有一个对应的相同类型的左括号*/
public class Solution_20 {
public boolean isValid(String s) {
    Map<Character,Character> map = new HashMap<>();
    map.put('(',')');
    map.put('[',']');
    map.put('{','}');
    //若第一个括号是右括号则返回false,下面代码不用执行
    if (s.length()>0 && !map.containsKey(s.charAt(0))) return false;
    LinkedList<Character> stack = new LinkedList<>();
    stack.add('?');//是防止括号匹配到最后一个时,有值与之判断
    for(Character c:s.toCharArray()){
        if (map.containsKey(c)){//map的key值中包含左括号c则对stack进行添加
            stack.addLast(c);
        }else if (map.get(stack.removeLast()) != c){//若匹配到了右括号则让stack最后一次添加的括号来取得map中对应的值,也就是右括号,判断是否与c右括号相等
            return false;
        }
    }
    return stack.size() == 1;//若匹配完后只剩下最开始的?,则说明所有括号匹配完成
}

    public static void main(String[] args) {
        String str = "";
        Solution_20 solution20 = new Solution_20();
        System.out.println("solution11.isValid(str) = " + solution20.isValid(str));
    }
}
