package com.leetcode;

import java.util.*;

/**
 * @author wangjin
 * 2023/10/4 0:28
 * @description TODO
 */
/*给定一种规律 pattern 和一个字符串 s ，判断 s 是否遵循相同的规律。
这里的 遵循 指完全匹配，例如， pattern 里的每个字母和字符串 s 中的每个非空单词之间存在着双向连接的对应规律*/
public class Solution_290 {
    public boolean wordPattern(String pattern, String str) {
        String[] words = str.split(" ");
        if (words.length != pattern.length()) {
            return false;
        }
        Map<Object, Integer> map = new HashMap<>();
        //调用put方法时，如果已经存在一个相同的key， 则返回的是前一个key对应的value，
        // 同时该key的新value覆盖旧value，如果是新的一个key，则返回的是null。
        for (Integer i = 0; i < words.length; i++) {
            if (map.put(pattern.charAt(i), i) != map.put(words[i], i)) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {

    }
}
