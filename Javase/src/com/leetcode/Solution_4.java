package com.leetcode;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/24 21:25
 * @description TODO
 */
/*给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
算法的时间复杂度应该为 O(log (m+n)) 。*/
public class Solution_4 {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] nums = new int[nums1.length+ nums2.length];
        double midNum = 0;
        for (int i = 0; i < nums1.length; i++) {
            nums[i] = nums1[i];
        }
        for (int i = nums1.length; i < nums.length; i++) {
            nums[i] = nums2[i-nums1.length];
        }
        Arrays.sort(nums);
/*        int left = 0;
        int right = nums.length;*/
        int midIndex = (nums.length-1)/2;
        System.out.println(Arrays.toString(nums));
        if (nums.length%2==0){
            midNum = (nums[nums.length/2]+nums[nums.length/2-1])/2.0;
            return midNum;
        }else {
            return nums[midIndex];
        }
    }
    public static void main(String[] args) {
        Solution_4 solution4 = new Solution_4();
        System.out.println("solution10.findMedianSortedArrays(new int[]{1,2},new int[]{3,4}) = " + solution4.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}));
    }
}
