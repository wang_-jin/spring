package com.leetcode;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/10/30 20:54
 * @description TODO
 */
/*给你一个整数数组 citations ，其中 citations[i] 表示研究者的第 i 篇论文被引用的次数。
计算并返回该研究者的 h 指数。

根据维基百科上 h 指数的定义：h 代表“高引用次数” ，一名科研人员的 h 指数 是指他（她）至少发表了 h 篇论文，
并且每篇论文 至少 被引用 h 次。如果 h 有多种可能的值，h 指数 是其中最大的那个。
(寻找一个数 h，h 表示 citations 数组中至少有 h 个数，并且这 h 个数都大于等于 h。)
*/

public class Solution_274 {
    public int hIndex(int[] citations) {
        Arrays.sort(citations);//先将数组按从小到大排序
        int h = 0, i = citations.length - 1;
        while (i >= 0 && citations[i] > h) {//从后到前遍历
            //citations[i] > h -->每篇论文 至少 被引用 h 次
            h++;
            i--;
        }
        return h;
    }
    public static void main(String[] args) {
        int[] citations = {3,0,6,1,5};
        Solution_274 solution_274 = new Solution_274();
        int i = solution_274.hIndex(citations);
        System.out.println("i = " + i);
    }
}
