package com.leetcode;

/**
 * @author wangjin
 * 2023/9/28 9:20
 * @description TODO
 */
/*给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。*/
public class Solution_189 {
/*    public void rotate(int[] nums, int k) {
        List<Integer> list = new ArrayList<>();
        int len = k% nums.length;
            for (int num : nums) {
                list.add(num);
            }
            for (int i = 0; i < len; i++) {
                nums[i] = nums[nums.length - len + i];
            }
            int count = len;
            for (int i = 0; i < nums.length - len; i++) {
                nums[count] = list.get(i);
                count++;
            }
    }*/
/*      //使用额外的数组
        public void rotate(int[] nums, int k) {
        int n = nums.length;
        int[] newArr = new int[n];
        for (int i = 0; i < n; ++i) {
            newArr[(i + k) % n] = nums[i];
        }
        System.arraycopy(newArr, 0, nums, 0, n);
        }*/
    //数组翻转
    public void rotate(int[] nums, int k) {
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);
}

    public void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start += 1;
            end -= 1;
        }
    }
    public static void main(String[] args) {
        Solution_189 solution189 = new Solution_189();
        solution189.rotate(new int[]{1,2,3,4,5},1);
    }
}
