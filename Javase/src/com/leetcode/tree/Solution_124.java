package com.leetcode.tree;

import com.leetcode.Solution_172;

/**
 * @author wangjin
 * 2023/10/17 15:15
 * @description TODO
 */
/*二叉树中的 路径 被定义为一条节点序列，序列中每对相邻节点之间都存在一条边。同一个节点在一条路径序列中 至多出现一次 。
该路径 至少包含一个 节点，且不一定经过根节点。
路径和 是路径中各节点值的总和。
给你一个二叉树的根节点 root ，返回其 最大路径和 。*/
public class Solution_124 {
    //定义全局变量
    int maxValues = Integer.MIN_VALUE;
    public int maxPathSum(TreeNode root) {
        maxGain(root);
        return maxValues;

    }
    //递归dfs
    public int maxGain(TreeNode root){
        if (root==null) return 0;
        // 递归计算左右子节点的最大贡献值
        // 节点的最大路径和取决于该节点的值与该节点的左右子节点的最大贡献值
        int left = Math.max(0,maxGain(root.left));
        int right = Math.max(0,maxGain(root.right));
        // 更新答案
        maxValues = Math.max(maxValues,left+right+root.val);
        // 返回节点的最大贡献值 //因为路径不能分叉,所以返回当前节点的值加上左或右子树中的最大值即可
        return root.val+Math.max(left,right);
    }

    public static void main(String[] args) {
        Solution_124 solution_124 = new Solution_124();
        Solution_105 solution_105 = new Solution_105();
        TreeNode node = solution_105.buildTree(new int[]{-10, 9, 20, 15, 7}, new int[]{9,-10, 15, 20, 7});
        int maxPathSum = solution_124.maxPathSum(node);
        System.out.println("maxPathSum = " + maxPathSum);

    }
}
