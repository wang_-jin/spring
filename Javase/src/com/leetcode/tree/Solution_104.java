package com.leetcode.tree;

/**
 * @author wangjin
 * 2023/10/14 14:23
 * @description TODO
 */
/*给定一个二叉树 root ，返回其最大深度。

二叉树的 最大深度 是指从根节点到最远叶子节点的最长路径上的节点数。*/
public class Solution_104 {

    public int maxDepth(TreeNode root) {
        if (root==null){
            return 0;
        }
        //后序遍历求最大深度
        //递归左子树
        int leftLen = maxDepth(root.left);
        //递归右子树
        int rightLen = maxDepth(root.right);
        //递归父节点
        int gapLength = 1+Math.max(leftLen,rightLen);
        return gapLength;
    }
}

