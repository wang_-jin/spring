package com.leetcode.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/10/22 23:58
 * @description TODO
 */
/*给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。*/
public class Solution_94 {

    public List<Integer> inorderTraversal(TreeNode root) {
        //中序 左中右
        List<Integer> list = new ArrayList<>();
        dfs(root,list);
        return list;
    }
    public void dfs(TreeNode node, List<Integer> list){
        if (node==null) return;
        dfs(node.left,list);
        list.add(node.val);
        dfs(node.right,list);

    }
}
