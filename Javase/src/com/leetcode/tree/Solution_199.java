package com.leetcode.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author wangjin
 * 2023/11/16 16:39
 * @description TODO
 */
public class Solution_199 {
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        if (root==null){//判空
            return list;
        }
        queue.offer(root);
        while(!queue.isEmpty()){
            int size = queue.size();//记录队列长度
            while (size>0){
                TreeNode node = queue.poll();//移出每个父节点的并将其子节点加入到队列中
                if (node.left!=null)queue.offer(node.left);
                if (node.right!=null)queue.offer(node.right);
                if (size==1)list.add(node.val);//将每层最右边的节点进行添加
                size--;
            }
        }
        return list;
    }
}
