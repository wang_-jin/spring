package com.leetcode.tree;

import java.util.*;

/**
 * @author wangjin
 * 2023/10/15 22:51
 * @description TODO
 */
/*给定两个整数数组 preorder 和 inorder ，其中 preorder 是二叉树的先序遍历，
inorder 是同一棵树的中序遍历，请构造二叉树并返回其根节点。*/
public class Solution_105 {
/*    public TreeNode buildTree(int[] preorder, int[] inorder) {
        TreeNode root = new TreeNode();
        if (preorder.length==0&&inorder.length==0){
            return null;
        }
        int rootVal = preorder[0];
        root = new TreeNode(rootVal);
        int index = 0;//记录父节点
        for (index = 0;index< inorder.length;index++){
            if (inorder[index]==rootVal) break;
        }
        int leftLen = index;
        int rightLen = inorder.length-index-1;
        //preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
        //切割中序数组
        int[] leftInorder = new int[leftLen];//中左序
        int[] rightInorder = new int[rightLen];//中右序
        for (int i = 0; i < leftLen; i++) {
            leftInorder[i] = inorder[i];
        }
        for (int i = 0; i < rightLen; i++) {
            rightInorder[i] = inorder[i+index+1];
        }
        //切割先序数组
        int[] leftPreorder = new int[leftLen];
        int[] rightPreorder = new int[rightLen];
        for (int i = 0; i < leftLen; i++) {
            leftPreorder[i] = preorder[i+1];
        }
        for (int i = 0; i < rightLen; i++) {
            rightPreorder[i] = preorder[leftLen+i+1];
        }
        //递归处理左右区间
        root.left = buildTree(leftPreorder,leftInorder);
        root.right = buildTree(rightPreorder,rightInorder);
        return root;
    }*/
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder == null || preorder.length == 0) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[0]);
        Deque<TreeNode> stack = new LinkedList<TreeNode>();
        stack.push(root);
        int inorderIndex = 0;
        for (int i = 1; i < preorder.length; i++) {
            int preorderVal = preorder[i];
            TreeNode node = stack.peek();
            if (node.val != inorder[inorderIndex]) {
                node.left = new TreeNode(preorderVal);
                stack.push(node.left);
            } else {
                while (!stack.isEmpty() && stack.peek().val == inorder[inorderIndex]) {
                    node = stack.pop();
                    inorderIndex++;
                }
                node.right = new TreeNode(preorderVal);
                stack.push(node.right);
            }
        }
        return root;
    }


}
