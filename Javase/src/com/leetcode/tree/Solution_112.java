package com.leetcode.tree;



/**
 * @author wangjin
 * 2023/10/11 10:08
 * @description TODO
 */
/*给你二叉树的根节点 root 和一个表示目标和的整数 targetSum 。判断该树中是否存在 根节点到叶子节点 的路径，
这条路径上所有节点值相加等于目标和 targetSum 。如果存在，返回 true ；否则，返回 false 。
叶子节点 是指没有子节点的节点。*/


public class Solution_112 {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root==null){
            return false;
        }
        if (root.left==null && root.right==null && targetSum==root.val) return true;
        if (root.left==null && root.right==null && targetSum!=root.val) return false;
        if (root.left!=null){
            //目标值从头节点开始递减
            targetSum-=root.val;
            //递归若减到最后一个叶子节点的sum值与叶子节点相同则返回true
            if (hasPathSum(root.left,targetSum)) return true;
            targetSum+=root.val;//回溯
        }
        if (root.right!=null){
            targetSum-=root.val;
            if (hasPathSum(root.right,targetSum)) return true;
            targetSum+=root.val;//回溯
        }
        return false;
    }
}
