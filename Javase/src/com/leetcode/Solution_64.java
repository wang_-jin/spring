package com.leetcode;

/**
 * @author wangjin
 * 2023/10/10 18:55
 * @description TODO
 */
/*给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
说明：每次只能向下或者向右移动一步。*/
public class Solution_64 {
    public int minPathSum(int[][] grid) {
        int m = grid.length;//行数
        int n = grid[0].length;//行数
        int[][] dp = new int[m+1][n+1];
        dp[0][0] = grid[0][0];
        for (int i = 1; i < m; i++) {//左边界的dp数组值
            dp[i][0] = dp[i-1][0]+grid[i][0];
        }
        for (int i = 1; i < n; i++) {//上边界的dp数组值
            dp[0][i] = dp[0][i-1]+grid[0][i];
        }
        for (int i = 1; i < m; i++) {//其他行其他列的dp
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.min(dp[i-1][j],dp[i][j-1])+grid[i][j];
            }
        }
        return dp[m-1][n-1];
    }
    public static void main(String[] args) {
        int[][] grid = {{1,2,3},{4,5,6}};
        Solution_64 solution_64 = new Solution_64();
        int i = solution_64.minPathSum(grid);
        System.out.println("i = " + i);
    }
}
