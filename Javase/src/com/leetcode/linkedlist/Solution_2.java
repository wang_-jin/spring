package com.leetcode.linkedlist;

/**
 * @author wangjin
 * 2023/10/9 15:14
 * @description TODO
 */
/*给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
请你将两个数相加，并以相同形式返回一个表示和的链表。
你可以假设除了数字 0 之外，这两个数都不会以 0 开头。*/



public class Solution_2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return add(l1,l2,0);//返回两链表相加的头部
    }
    public ListNode add(ListNode l1,ListNode l2,int bit){
        //bit 是两链表同位置数相加的进位数
        if (l1 == null && l2 == null && bit == 0){
            return null;
        }
        int val = bit;
        if (l1 != null){
            val+= l1.val;
            l1 = l1.next;
        }
        if (l2 != null){
            val+= l2.val;
            l2 = l2.next;
        }
        ListNode node = new ListNode(val%10);//新链表的值
        node.next = add(l1,l2,val/10);//指向链表的下一个节点
        return node;
    }

}
