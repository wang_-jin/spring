package com.leetcode.linkedlist;

/**
 * @author wangjin
 * 2023/10/14 15:05
 * @description TODO
 */
//Definition for singly-linked list.
public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
