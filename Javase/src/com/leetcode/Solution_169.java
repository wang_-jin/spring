package com.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/10/4 0:08
 * @description TODO
 */
/*给定一个大小为 n 的数组 nums ，返回其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
你可以假设数组是非空的，并且给定的数组总是存在多数元素。*/
public class Solution_169 {
    public int majorityElement(int[] nums) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int num : nums) {
            int i = map.getOrDefault(num,0)+1;
            map.put(num,i);
        }
        int len = nums.length/2;
        AtomicInteger max = new AtomicInteger();
        map.forEach((key, values)->{
            if (values>len){
                max.set(key);
            }
        });
        return max.intValue();
    }

    public static void main(String[] args) {
        Solution_169 solution169 = new Solution_169();
        int i = solution169.majorityElement(new int[]{2, 2, 1, 1, 1, 2, 2});
        System.out.println("i = " + i);
    }
}
