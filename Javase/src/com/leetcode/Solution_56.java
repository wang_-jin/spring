package com.leetcode;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author wangjin
 * 2023/10/21 15:21
 * @description TODO
 */
/*以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
请你合并所有重叠的区间，并返回 一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间 。*/
public class Solution_56 {
    public int[][] merge(int[][] intervals) {
        if (intervals.length==0) return null;
        int n = intervals.length;
        int[][] result = new int[n][2];
        //对intervals数组进行排序
        Arrays.sort(intervals, new Comparator<int[]>() {
            public int compare(int[] interval1, int[] interval2) {
                return interval1[0] - interval2[0];
            }
        });
        //intervals = [[1,3],[2,6],[8,10],[15,18]]
        //添加首元素
        result[0] = intervals[0];
        int count = 1;//记录result数组的存放的区间个数
        for (int i = 1; i < n; i++) {
            if (intervals[i][0]<=result[count-1][1]){//若后一个的左区间小于前一个的右区间则进行合并操作
                //只用修改result数组最后一个区间数组的右区间就可
                result[count-1][1] = Math.max(result[count-1][1],intervals[i][1]);
            }else {//否则进行添加
                result[count] = intervals[i];
                count++;
            }
        }
        //对result数组进行裁剪多余{0,0}剔除
        int[][] newResult = new int[count][2];
        for (int i = 0; i < count; i++) {
            newResult[i] = result[i];
        }
        return newResult;
    }

    public static void main(String[] args) {
        int[][] intervals = {{1,3},{2,6},{8,10},{15,18}};
        int[][] intervalss = {{1,4},{0,2},{3,5}};
        Solution_56 solution_56 = new Solution_56();
        int[][] merge = solution_56.merge(intervalss);
        System.out.println("merge = " + Arrays.deepToString(merge));
    }
}
