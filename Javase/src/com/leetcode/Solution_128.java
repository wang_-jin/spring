package com.leetcode;

import java.util.*;

/**
 * @author wangjin
 * 2023/9/10 17:07
 * @description TODO
 */
public class Solution_128 {
    /*给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
    请你设计并实现时间复杂度为 O(n) 的算法解决此问题。*/
    public int longestConsecutive(int[] nums) {
/*        if (nums.length<1){
            return 0;
        }
        Set<Integer>set1 = new TreeSet<Integer>();//用来去除数组中的重复元素
        for (int num : nums) {
            set1.add(num);
        }//nums放TreeSet中去重并排序
        Integer[] num = set1.toArray(new Integer[]{});//集合转数组
        System.out.println(Arrays.toString(num));
        int max = 1;//最小长度为1
        Set<Integer>set = new TreeSet<>();//定义一个set集合存放最大长度
        for (int i = 1; i < num.length; i++) {
            if ((num[i]-num[i-1])==1||(num[i]-num[i-1])==0){
                max++;
                set.add(max);//连续就存放max++后的值
            }else {
                max = 1;//没连续就把1放set中
                set.add(max);
            }
        }
        Integer integer = set.stream().max(Integer::compare).orElse(1);//找到set中最后一个值
        return integer;*/
        Set<Integer> set = new HashSet<>();//用集合来去除重复元素
        for (int num : nums) {
            set.add(num);
        }
        int max = 0;//最长序列长度
        for (Integer i : set) {
            if (!set.contains(i-1)){//若集合中没有比当前的数小一的数则进入判断
                int nowNum = i; //当前正在遍历的数
                int maxLength = 1;//设置临时变量记录连续的序列长度
                while(set.contains(nowNum+1)){//若集合中存在比当前的数大一的数则进入循环,并执行maxLength++
                    nowNum++;
                    maxLength++;
                }
                max = Math.max(max, maxLength);//取最大序列长度
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] num = {0};
        Solution_128 solution128 = new Solution_128();
        System.out.println(solution128.longestConsecutive(num));
    }

}
