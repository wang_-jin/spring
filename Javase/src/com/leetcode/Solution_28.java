package com.leetcode;

/**
 * @author wangjin
 * 2023/10/31 20:20
 * @description TODO
 */
/*给你两个字符串 haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串的第一个匹配项的下标
（下标从 0 开始）。如果 needle 不是 haystack 的一部分，则返回  -1 。*/
public class Solution_28 {
    public int strStr(String haystack, String needle) {
        //KMP算法?
        //haystack = "sadbutsad", needle = "sad"
        int left = 0;
        int right = 0;
        boolean flag = false;
        String newHaystack = haystack+"?";
        while(right<haystack.length()&&left<haystack.length()){
            if (!haystack.substring(left,right+1).contains(needle)) {
                right++;
            } else {
                if (!haystack.substring(left,right+1).equals(needle)){
                    left++;
                }else {
                    flag = true;
                    break;
                }
            }
        }
        return flag?left:-1;
    }

    public static void main(String[] args) {
        String haystack = "leetcode";
        String needle = "leeto";
        Solution_28 solution_28 = new Solution_28();
        int i = solution_28.strStr(haystack, needle);
        System.out.println("i = " + i);
    }
}
