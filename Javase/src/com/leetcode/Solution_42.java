package com.leetcode;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author wangjin
 * 2023/11/13 21:44
 * @description TODO
 */
public class Solution_42 {
    public int trap(int[] height) {
        int sum = 0;
        LinkedList<Integer> st = new LinkedList<>();
        st.add(0);
        for (int i = 1; i < height.length; i++) {
            //若遍历到的元素比单调栈的元素小时,进行添加栈操作
            if (height[i]<height[st.getFirst()]){
                st.push(i);
            }else if (height[i]==height[st.getFirst()]){
                st.pop();
                st.push(i);
            }else {
                //
                //
                while(!st.isEmpty() && height[i]>height[st.getFirst()]){
                    int mid = st.getFirst();
                    st.pop();
                    if (!st.isEmpty()){
                        int h = Math.min(height[st.getFirst()],height[i])-height[mid];
                        int w = i-st.getFirst()-1;
                        sum+=h*w;
                    }
                }
                st.push(i);
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        Solution_42 solution_42 = new Solution_42();
        int[] arr = {0,1,0,2,1,0,1,3,2,1,2,1};
        int trap = solution_42.trap(arr);
        System.out.println("trap = " + trap);
    }
}
