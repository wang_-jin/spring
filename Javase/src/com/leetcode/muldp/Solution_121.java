package com.leetcode.muldp;

/**
 * @author wangjin
 * 2023/10/24 13:45
 * @description TODO
 */
/*
给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。

你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。

返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。*/
public class Solution_121 {
    public int maxProfit(int[] prices) {
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;//卖出
        dp[0][1] = -prices[0];//买入
        for (int i = 1; i < n; i++) {
            dp[i][0] = Math.max(dp[i-1][0],dp[i-1][1]+prices[i]);
            dp[i][1] = Math.max(-prices[i],dp[i-1][1]);
        }
        return dp[n-1][0];
    }

    public static void main(String[] args) {
        Solution_121 solution_121 = new Solution_121();
        int[] prices = {7,1,5,3,6,4};
        int i = solution_121.maxProfit(prices);
        System.out.println(i);
    }
}
