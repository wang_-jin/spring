package com.leetcode.muldp;

/**
 * @author wangjin
 * 2023/10/20 17:56
 * @description TODO
 */
/*给你一个整数数组 prices ，其中 prices[i] 表示某支股票第 i 天的价格。
在每一天，你可以决定是否购买和/或出售股票。你在任何时候 最多 只能持有 一股 股票。你也可以先购买，然后在 同一天 出售。
返回 你能获得的 最大 利润 。*/
public class Solution_122 {
    public int maxProfit(int[] prices) {
/*      //贪心算法
        //贪心算法只能用于计算最大利润，计算的过程并不是实际的交易过程。
        int maxSum = 0;
        for (int i = 1; i < prices.length; i++) {
            maxSum+= Math.max(0,prices[i]-prices[i-1]);
        }
        return maxSum;*/
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0]  = 0;
        dp[0][1]  = -prices[0];
        for (int i = 1; i < n; i++) {
            dp[i][0] = Math.max(dp[i-1][1]+prices[i],dp[i-1][0]);
            dp[i][1] = Math.max(dp[i-1][0]-prices[i],dp[i-1][1]);
        }
        return dp[n-1][0];
    }


    public static void main(String[] args) {
        int[] prices = {3,2,6,5,0,3};
        Solution_122 solution_122 = new Solution_122();
        int i = solution_122.maxProfit(prices);
        System.out.println(i);
    }
}
