package com.leetcode;

/**
 * @author wangjin
 * 2023/11/8 21:17
 * @description TODO
 */
/*一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。

        机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish”）。

        现在考虑网格中有障碍物。那么从左上角到右下角将会有多少条不同的路径？

        网格中的障碍物和空位置分别用 1 和 0 来表示。*/
public class Solution_63 {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;//行数
        int n = obstacleGrid[0].length;//列数
        int[] dp = new int[n];
        for (int i = 0; i < n && obstacleGrid[i][0]==0; i++) {//左边界的dp数组值
            dp[i] = 1;
        }

        for (int i = 1; i < m; i++) {//其他行其他列的dp
            for (int j = 0; j < n; j++) {
                if (j!=0) {
                    dp[j] = dp[j] + dp[j - 1];
                }else if (obstacleGrid[i][j]==1){
                    dp[j] = 0;
                }
            }
        }
        return dp[n-1];
    }

    public static void main(String[] args) {
        int[][] grid = {{0,1},{0,0}};
        Solution_63 solution_63 = new Solution_63();
        int i = solution_63.uniquePathsWithObstacles(grid);
        System.out.println("i = " + i);
    }
}
