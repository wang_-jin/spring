package com.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjin
 * 2023/10/6 4:14
 * @description TODO
 */
/*给定两个字符串 s 和 t ，判断它们是否是同构的。
如果 s 中的字符可以按某种映射关系替换得到 t ，那么这两个字符串是同构的。
每个出现的字符都应当映射到另一个字符，同时不改变字符的顺序。不同字符不能映射到同一个字符上，
相同字符只能映射到同一个字符上，字符可以映射到自己本身。*/
public class Solution_205 {
    public boolean isIsomorphic(String s, String t) {
        if (s.length()!=t.length()){
            return false;
        }
        Map<Character,Character> map1 = new HashMap<>();
        Map<Character,Character> map2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char x = s.charAt(i), y = t.charAt(i);
            //若map中含key值,且取的相应的values值是不能映射到另一个字符串的字符,则判断失败
            if ((map1.containsKey(x) && map1.get(x) != y) || (map2.containsKey(y) && map2.get(y) != x)) {
                return false;
            }
            map1.put(x, y);//存入相应的key值x和对应的values值y
            map2.put(y, x);
        }
        return true;
    }
    public static void main(String[] args) {
        Solution_205 solution205 = new Solution_205();
        boolean b = solution205.isIsomorphic("PaperABC.?.", "TitlezAB212");
        System.out.println("b = " + b);
    }
}
