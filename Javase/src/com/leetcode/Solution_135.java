package com.leetcode;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/11/6 20:14
 * @description TODO
 */
/*
n 个孩子站成一排。给你一个整数数组 ratings 表示每个孩子的评分。

你需要按照以下要求，给这些孩子分发糖果：

每个孩子至少分配到 1 个糖果。
相邻两个孩子评分更高的孩子会获得更多的糖果。
请你给每个孩子分发糖果，计算并返回需要准备的 最少糖果数目 。
*/
public class Solution_135 {
    public int candy(int[] ratings) {
        int[] candy = new int[ratings.length];
        Arrays.fill(candy, 1);
        //向右遍历--右孩子大于左孩子
        for (int i = 1; i < ratings.length; i++) {
            if (ratings[i]>ratings[i-1]){
                candy[i] = candy[i-1]+1;
            }
        }
        //向左遍历--左孩子大于右孩子
        for (int i = ratings.length-2; i >=0; i--) {
            if (ratings[i]>ratings[i+1]){
                //取两次遍历过程中的最大值即可满足两种条件
                candy[i] = Math.max((candy[i+1]+1),candy[i]);
            }
        }
        int sum = 0;
        for (int i : candy) {
            sum+=i;
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] arr = {1,2,2};
        Solution_135 solution_135 = new Solution_135();
        int candy = solution_135.candy(arr);
        System.out.println("candy = " + candy);
    }
}
