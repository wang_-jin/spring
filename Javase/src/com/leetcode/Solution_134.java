package com.leetcode;

/**
 * @author wangjin
 * 2023/10/31 20:09
 * @description TODO
 */
/*在一条环路上有 n 个加油站，其中第 i 个加油站有汽油 gas[i] 升。

你有一辆油箱容量无限的的汽车，从第 i 个加油站开往第 i+1 个加油站需要消耗汽油 cost[i] 升。
你从其中的一个加油站出发，开始时油箱为空。

给定两个整数数组 gas 和 cost ，如果你可以按顺序绕环路行驶一周，则返回出发时加油站的编号，否则返回 -1 。
如果存在解，则 保证 它是 唯一 的。*/
public class Solution_134 {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        //贪心算法
        int curSum = 0;//gas-cost的剩余油量
        int totalSum = 0;//跑完一圈的剩余油量
        int start = 0;//记录满足题意的出发索引
        for (int i = 0; i < gas.length; i++) {
            curSum+=gas[i]-cost[i];
            totalSum+=gas[i]-cost[i];
            if (curSum<0){//剩余油量相加小于零则剩余油量归零,记录出发索引
                curSum = 0;
                start = i+1;
            }
        }
        if (totalSum<0)return -1;
        return start;
    }

    public static void main(String[] args) {

    }
}
