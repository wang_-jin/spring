package com.leetcode;

/**
 * @author wangjin
 * 2023/10/6 3:26
 * @description TODO
 */
/*实现 pow(x, n) ，即计算 x 的整数 n 次幂函数（即，xn ）*/
public class Solution_50 {
    public double myPow(double x, int n) {
        long N = n;
        return N >= 0 ? quickMul(x, N) : 1.0 / quickMul(x, -N);
    }
    public double quickMul(double x, long N) {
        if (N == 0) {
            return 1.0;
        }
        double y = quickMul(x, N / 2);
        return N % 2 == 0 ? y * y : y * y * x;
    }
    public static void main(String[] args) {
        Solution_50 solution50 = new Solution_50();
        System.out.println("solution20.myPow(2.10000,3) = " + solution50.myPow(2.00000, -2));
    }
}
