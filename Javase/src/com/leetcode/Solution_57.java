package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author wangjin
 * 2023/10/20 19:32
 * @description TODO
 */
/*给你一个 无重叠的 ，按照区间起始端点排序的区间列表。

在列表中插入一个新的区间，你需要确保列表中的区间仍然有序且不重叠（如果有必要的话，可以合并区间）。*/

//    输入：intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
//    输出：[[1,2],[3,10],[12,16]]
//    解释：这是因为新的区间 [4,8] 与 [3,5],[6,7],[8,10] 重叠。
public class Solution_57 {
    public int[][] insert(int[][] intervals, int[] newInterval) {
        int left = newInterval[0];
        int right = newInterval[1];
        List<int[]> list = new ArrayList<>();
        boolean flag = false;
        //遍历每个给定区间
        for (int[] interval : intervals) {

            if (interval[0]>right){//新区间在被遍历的区间左边且不相交
                //当出现interval[0]>right list放入新区间 这样后面遍历的区间可按连续存入在这之后
                if (!flag){
                    list.add(new int[]{left,right});
                    flag = true;
                }
                list.add(interval);
            }else if (interval[1]<left){//新区间在被遍历的区间右边且不相交
                list.add(interval);
            }else {
                //有交集合并区间
                left = Math.min(interval[0],left);
                right = Math.max(interval[1],right);
            }
        }
        //特殊情况 若遍历完最后一个区间才合并或新区间在整个区间的右边
        if (!flag) {
            list.add(new int[]{left, right});
        }
        //把list里的区间放到二维数组里
        int[][] arr = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    public static void main(String[] args) {
        int[][] intervals = {{1,5}};
        int[] newInterval = {2,3};
        Solution_57 solution_57 = new Solution_57();
        int[][] insert = solution_57.insert(intervals, newInterval);
        System.out.println("insert = " + Arrays.deepToString(insert));
    }
}
