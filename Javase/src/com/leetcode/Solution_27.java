package com.leetcode;

/**
 * @author wangjin
 * 2023/9/21 22:52
 * @description TODO
 */
public class Solution_27 {
    /*给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。*/
    public int removeElement(int[] nums, int val) {
        int key = 0;//定义一个初始索引,用来记录除去val元素后数组重赋值的次数
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]!=val){
                nums[key] = nums[i];//把不符合条件的从索引0开值赋给数组
                key++;
            }
        }
        return key;
    }

    public static void main(String[] args) {
        Solution_27 solution27 = new Solution_27();
        int i = solution27.removeElement(new int[]{1, 2, 3, 4}, 2);
        System.out.println(i);
    }
}
