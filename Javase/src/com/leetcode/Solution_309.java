package com.leetcode;

/**
 * @author wangjin
 * 2023/10/5 18:24
 * @description TODO
 */
/*给定一个整数数组prices，其中第  prices[i] 表示第 i 天的股票价格 。​
设计一个算法计算出最大利润。在满足以下约束条件下，你可以尽可能地完成更多的交易（多次买卖一支股票）:
卖出股票后，你无法在第二天买入股票 (即冷冻期为 1 天)。*/
public class Solution_309 {
    public int maxProfit(int[] prices) {
        int n = prices.length;
        if (prices.length==1){
            return 0;
        }
        int[][] f = new int[n][3];
        f[0][0] = -prices[0];
        // f[i][0]: 手上持有股票的最大收益
        // f[i][1]: 手上不持有股票，并且处于冷冻期中的累计最大收益
        // f[i][2]: 手上不持有股票，并且不在冷冻期中的累计最大收益
        for (int i = 1; i < n; ++i) {
            f[i][0] = Math.max(f[i-1][0],f[i-1][2]-prices[i]);
            f[i][1] = f[i-1][0]+prices[i];
            f[i][2] = Math.max(f[i-1][1],f[i-1][2]);
        }
        return Math.max(f[n-1][1],f[n-1][2]);
    }

    public static void main(String[] args) {
        Solution_309 solution309 = new Solution_309();
        int i = solution309.maxProfit(new int[]{2,1});
        System.out.println("i = " + i);
    }
}
