package com.leetcode;

/**
 * @author wangjin
 * 2023/9/10 21:59
 * @description TODO
 */
public class Solution_58 {
    /*给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
    单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。*/
    public int lengthOfLastWord(String s) {
        String[] s1 = s.split(" ");
        int last = 0;
        for (String s2 : s1) {
            if (s2.equals(s1[s1.length-1])){
                last = s2.length();
            }
        }
        return last;
    }

    public static void main(String[] args) {
        String s = "dsasd gsdgre UHIUfdf21254 131aeeswa asdb";
        Solution_58 solution58 = new Solution_58();
        System.out.println(solution58.lengthOfLastWord(s));
    }
}
