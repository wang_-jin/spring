package com.leetcode.onedp;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/11/17 18:34
 * @description TODO
 */
/*给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。
计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。
你可以认为每种硬币的数量是无限的。*/
public class Solution_322 {
    public int coinChange(int[] coins, int amount) {
            int max = amount + 1;
            int[] dp = new int[amount + 1];
            Arrays.fill(dp, max);
            dp[0] = 0;
            for (int i = 1; i <= amount; i++) {//遍历背包
                for (int j = 0; j < coins.length; j++) {//遍历物品
                    if (coins[j] <= i) {
                        dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                    }
                }
            }
            return dp[amount] > amount ? -1 : dp[amount];
        }

    public static void main(String[] args) {
        Solution_322 solution_322 = new Solution_322();
        int i = solution_322.coinChange(new int[]{1, 2, 5}, 11);
        System.out.println("i = " + i);
    }
}
