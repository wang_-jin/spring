package com.leetcode.onedp;

import java.util.*;

/**
 * @author wangjin
 * 2023/10/8 9:35
 * @description TODO
 */
/*给你一个字符串 s 和一个字符串列表 wordDict 作为字典。请你判断是否可以利用字典中出现的单词拼接出 s 。
注意：不要求字典中出现的单词全部都使用，并且字典中的单词可以重复使用。*/
public class Solution_139 {
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<>(wordDict);
        boolean[] dp = new boolean[s.length()+1];
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (int j = 0; j < i; j++) {
                if (dp[j] && set.contains(s.substring(j,i))){
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }

    public static void main(String[] args) {
        Solution_139 solution139 = new Solution_139();
        String s = "aaaaaaa";
        List<String> list = new LinkedList<>(Arrays.asList("aaaa","aaa"));
        boolean b = solution139.wordBreak(s, list);
        System.out.println("b = " + b);
    }
}
