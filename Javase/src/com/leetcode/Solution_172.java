package com.leetcode;

import java.util.LinkedList;
import java.util.List;

/**
 * @author wangjin
 * 2023/10/21 15:05
 * @description TODO
 */
/*给定一个整数 n ，返回 n! 结果中尾随零的数量。

提示 n! = n * (n - 1) * (n - 2) * ... * 3 * 2 * 1

 */
public class Solution_172 {
/*    public int trailingZeroes(int n) {
        int num = 0;
        for (int i = 5; i <= n; i+=5) {
            for (int j = i; j%5==0; j/=5) {
                num++;
            }
        }
        return num;
    }*/
public int trailingZeroes(int n) {
    int num = 0;
    while (n!=0){
        n/=5;
        num+=n;
    }
    return num;
}

    public static void main(String[] args) {
        Solution_172 solution_172 = new Solution_172();
        int i = solution_172.trailingZeroes(30);
        System.out.println("i = " + i);
    }
}
