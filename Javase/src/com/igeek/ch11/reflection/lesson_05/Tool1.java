package com.igeek.ch11.reflection.lesson_05;

import java.lang.reflect.Field;

public class Tool1 {
    public static void main(String[] args) throws NoSuchFieldException, InstantiationException, IllegalAccessException {
        //获取Class对象
        Class<?> clazz = Person.class;
        //获取Field对象
        Field fname = clazz.getDeclaredField("name");
        Field fage = clazz.getDeclaredField("age");
        //创建实例对象
        Person p = (Person)clazz.newInstance();
        //设置属性
        fname.set(p,"刘备");
        //取消属性的访问权限
        fage.setAccessible(true);
        fage.setInt(p,18);
        //获取属性
        System.out.println(fname.get(p));
        System.out.println(fage.getInt(p));


    }
}
