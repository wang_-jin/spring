package com.igeek.ch11.reflection.lesson_05;
import java.lang.reflect.Field;
import java.util.Arrays;

public class Tool {
    public static void main(String[] args) throws NoSuchFieldException {
        //获取class
        Class<Person> clazz = Person.class;
        //获取公开的public属性
        Field[] fs = clazz.getFields();
        Arrays.stream(fs).forEach(item->{
            //System.out.println(item);
        });
        //获取所有属性(包含私有属性)
        Field[] fs1 = clazz.getDeclaredFields();
        Arrays.stream(fs1).forEach(item->{
            //System.out.println(item);
        });

        //获取某个具体属性
        Field name = clazz.getDeclaredField("name");
        System.out.println("name="+name);
    }
}
