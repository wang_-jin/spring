package com.igeek.ch11.reflection.lesson_02;

public class Tool {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> tool = Class.forName("ch11.reflection.lesson_02.Tool");
        //获取类名
        System.out.println(tool.getSimpleName());
        //获取全类名
        System.out.println(tool.getName());
        //获取包名
        System.out.println(tool.getPackage().getName());
        //获取父类信息
        System.out.println(tool.getSuperclass());
    }
}
