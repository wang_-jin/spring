package com.igeek.ch11.reflection.lesson_04;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Tool1 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        //获取class对象
        Class<?> clazz = Class.forName("ch11.reflection.lesson_04.Person");
        //获取方法
        Method m = clazz.getDeclaredMethod("eat",String.class);
        //取消访问控制权限
        m.setAccessible(true);
        //创建实例对象
        Person p = (Person)clazz.newInstance();
        //执行方法
        m.invoke(p,"西瓜");
    }
}
