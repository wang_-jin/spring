package com.igeek.ch11.reflection.lesson_04;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Tool {
    public static void main(String[] args) throws NoSuchMethodException {
        Class<Person> clazz = Person.class;
        //获取本类及父类所有公开的方法
        Method[] m = clazz.getMethods();
        Arrays.stream(m).forEach(item->{
            //System.out.println("方法名:"+item.getName()+";"+"方法形参个数:"+item.getParameterCount());
        });
        //获取本类的所有方法(包括私有的)
        Method[] m1 = clazz.getDeclaredMethods();
        Arrays.stream(m1).forEach(item->{
            //System.out.println("方法名:"+item.getName()+";"+"方法形参个数:"+item.getParameterCount());
        });
        //获取公开的具体方法
        Method m2 = clazz.getMethod("drink",String.class);
        //System.out.println(m2);

        //获取具体方法(包含私有方法)
        Method m3 = clazz.getDeclaredMethod("eat",String.class);
        System.out.println(m3);
    }
}
