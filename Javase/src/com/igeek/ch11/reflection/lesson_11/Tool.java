package com.igeek.ch11.reflection.lesson_11;

interface Person{
    public void eat();
}
//目标类
class Man implements Person {
    @Override
    public void eat(){
        System.out.println("男人在吃饭");
    }

}
//代理类
class ManProxy implements Person{
    private Man man;
    public ManProxy(Man man){
        this.man = man;
    }
    @Override
    public void eat(){
      System.out.println("饭前喝点冷饮");
      man.eat();
      System.out.println("饭后吃点甜点");
    }
}
public class Tool {
    public static void main(String[] args) {
        Man man = new Man();
        ManProxy manProxy = new ManProxy(man);
        manProxy.eat();
    }
}
