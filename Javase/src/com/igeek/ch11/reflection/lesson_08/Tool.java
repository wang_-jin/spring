package com.igeek.ch11.reflection.lesson_08;
/*
自定义注解+属性:
  @interface 注解名{
    数据类型 属性名();
    数据类型 属性名() default 默认值;
    数据类型 value();(当只有一个属性时可以使用value代替属性名)
    }
如何使用:
   @注解名(属性名1=属性值,属性名2=属性值...)

*/

//只有一个属性时可以使用value,好处是在使用注解时可以省略属性名
@interface MyAnnotation{
    String value();
}

@interface MyAnnotation1{
    String name();
    int age() default 18;
    int height();
}

//只有一个value属性,使用时可以省略 value=
@MyAnnotation("hello")
@MyAnnotation1(name="刘备",height=178)
public class Tool {

}


