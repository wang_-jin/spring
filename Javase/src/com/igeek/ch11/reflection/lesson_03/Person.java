package com.igeek.ch11.reflection.lesson_03;

public class Person {
    String name;
    int age;
    public Person(){

    }
    private Person(String name){
        this.name = name;
    }
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
}
