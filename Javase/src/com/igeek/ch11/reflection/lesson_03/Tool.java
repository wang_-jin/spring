package com.igeek.ch11.reflection.lesson_03;

import java.lang.reflect.Constructor;
import java.util.Arrays;

public class Tool {
    public static void main(String[] args) throws NoSuchMethodException {
        Class<?> p = Person.class;
        //获取所有公开的构造方法
        Constructor<?>[] cs1 = p.getConstructors();
        Arrays.stream(cs1).forEach(item -> {
            //System.out.println(item);
        });

        //获取所有的构造方法(包含私有的)
        Constructor<?>[] cs2 = p.getDeclaredConstructors();
        Arrays.stream(cs2).forEach(item -> {
            //System.out.println(item);
        });

        //获取指定的公开的构造方法
        Constructor<?> cs3 = p.getConstructor();
        System.out.println(cs3);

        //获取指定的构造方法
        Constructor<?> cs4 = p.getDeclaredConstructor();
        Constructor<?> cs5 = p.getDeclaredConstructor(String.class);
        System.out.println(cs4);
        System.out.println(cs5);
    }
}


