package com.igeek.ch11.reflection.lesson_03;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Tool1 {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<Person> clazz = Person.class;
        //创建无参实例对象
        Person a = clazz.newInstance();
        System.out.println("a:"+a);
        Constructor<Person> cs = clazz.getDeclaredConstructor();
        Constructor<Person> cs1 = clazz.getDeclaredConstructor(String.class);
        //取消成员变量访问权限的控制
        cs1.setAccessible(true);
        //创建实例对象
        Person p =  cs.newInstance();
        Person p1 = cs1.newInstance("刘备");
        System.out.println("p:"+p);
        System.out.println("p1:"+p1);
    }
}
