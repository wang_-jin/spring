package com.igeek.ch11.reflection.lesson_07;
/*
定义注解:
  @interface 注解名{
  }
使用注解:
  @注解名
  注解可以使用的位置:
    TYPE类,FIELD成员变量,METHOD方法,PARAMETER方法的形参,
    CONSTRUCTOTR构造器等
*/

@MyAnnotation
public class Tool {
    @MyAnnotation
    public String eat(@MyAnnotation String name){
      return "";
    }
}

@interface MyAnnotation{

}
