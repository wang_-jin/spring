package com.igeek.ch11.reflection.lesson_06;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class Tool {
    public static void main(String[] args) {
        /*
        获取对象的所有属性和值存入到文件中
        */
        Person p = new Person("刘备",18,170);
        try (
                PrintStream ps = new PrintStream(new FileOutputStream("obj.txt"));
        ) {
            //改变打印输出方向
            System.setOut(ps);
            //获取Class类对象
            Class<Person> clazz = Person.class;
            //获取所有属性
            Field[] fs = clazz.getDeclaredFields();
            Arrays.stream(fs).forEach(field->{
                try {
                    System.out.println(field.getName()+"="+field.get(p));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            });

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
}
