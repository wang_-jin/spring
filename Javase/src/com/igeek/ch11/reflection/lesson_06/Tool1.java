package com.igeek.ch11.reflection.lesson_06;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Tool1 {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        /*
        破坏泛型:
          在Integer集合中使用add添加一个字符串
        */
        List<Integer> list = new ArrayList();
        list.add(1);
        list.add(3);
        list.add(5);
        Class<?> clazz = List.class;
        //获取方法对象
        Method addMethod = clazz.getDeclaredMethod("add",Object.class);
        addMethod.invoke(list,"hello");
        System.out.println(list);
    }
}
