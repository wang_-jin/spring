package com.igeek.ch11.reflection.lesson_06;

public class Person {
    public String name;
    public int age;
    public int height;
    public Person(){};
    public Person(String name,int age,int height){
        this.name = name;
        this.age = age;
        this.height = height;
    }
}
