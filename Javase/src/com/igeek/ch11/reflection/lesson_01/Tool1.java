package com.igeek.ch11.reflection.lesson_01;

public class Tool1 {
    public static void main(String[] args) throws ClassNotFoundException {
        //1.类名class
        Class<Tool1> t1 = Tool1.class;
        System.out.println("t1:"+t1);

        //2.对象.getClass()
        Tool1 t = new Tool1();
        Class<?> clazz = t.getClass();
        System.out.println(clazz);
        
        //3.Class.forName("包名.类名")
        Class<?> cl = Class.forName("ch11.reflection.lesson_01.Tool1");
        System.out.println(cl);
    }
}
