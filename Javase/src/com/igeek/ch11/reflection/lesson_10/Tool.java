package com.igeek.ch11.reflection.lesson_10;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation{
    String name();
    int age();
}

@MyAnnotation(name="刘备",age=18)
public class Tool {
    public static void main(String[] args) {
        //获取类对象
        Class<Tool> toolClass = Tool.class;
        //获取注解对象
        MyAnnotation myAnnotation = toolClass.getDeclaredAnnotation(MyAnnotation.class);
        //获取注解属性信息
        System.out.println(myAnnotation.name());
        System.out.println(myAnnotation.age());

    }
}
