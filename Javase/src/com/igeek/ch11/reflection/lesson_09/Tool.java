package com.igeek.ch11.reflection.lesson_09;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
//设置注解使用的位置
@Target({ElementType.TYPE})
//设置注解的使用阶段
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation{

}

@MyAnnotation
public class Tool {
    public void eat(){

    }
}
