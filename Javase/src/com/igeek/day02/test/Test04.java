package com.igeek.day02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 15:51
 * @description TODO
 * 定义一个int类型的变量,初始化值为123,
 * 求这个数的个位,十位,百位分别是多少,输出结果:123的个位是3,十位是2,百位是1
 * 1.	定义变量初始化值为123
 * 2.	模10获取个位
 * 3.	先除以10,再模10获取到十位
 * 4.	除以100获取到百位
 * 5.	输出结果
 */
public class Test04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入：");
        int a = sc.nextInt();
        int b = a%10;
        int c = (a/10)%10;
        int d = a/100;
        System.out.println("a="+a);
        System.out.println("个位"+b);
        System.out.println("十位"+c);
        System.out.println("百位"+d);
    }
}
