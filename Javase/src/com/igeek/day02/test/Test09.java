package com.igeek.day02;

/**
 * @author wangjin
 * 2023/8/10 15:59
 * @description TODO
 * A在B前面5km，A速度为1km/h，B速度为2km/h，求b多久能赶上a。
 * 1.	计算A与B的速度差
 * 2.	时间 = 路程 / 速度
 * 3.	输出结果
 */
public class Test09 {
    public static void main(String[] args) {
        int Va = 1;
        int Vb = 2;
        int v = Vb-Va;
        int t=5/v;
        System.out.println("b追赶a的时间为："+t);
    }
}
