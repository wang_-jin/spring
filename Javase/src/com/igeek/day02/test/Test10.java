package com.igeek.day02;

/**
 * @author wangjin
 * 2023/8/10 15:59
 * @description TODO
 */
public class Test10 {
        public static void main(String[] args) {
            int x = 4;
            int y = (--x)+(x--)+(x*10);

/*            int a = 1;
            int b = 2;
            boolean flag8 = (a<b)|(b++>1);
            System.out.println("b:"+b);*/

            //     3   3    2
            System.out.println("x = " + x + ",y = " + y);//x=2  y=26
        }
    }


/*public class Test04 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int x = a + b++;
        System.out.println("b=" + b);  //21
        System.out.println("x=" + x);  //30
    }
}*/

/*public class Test05 {
    public static void main(String[] args) {
        short s = 30;
        int i = 50;
        s += i;  //s = s+i
        System.out.println("s="+s);  //80

        int x = 0;
        int y = 0;
        int z = 0;
        boolean a,b;
        a = x>0 & y++>1;
        System.out.println("a="+a);  //false
        System.out.println("y="+y);  //1

        b = x>0 && z++>1;
        System.out.println("b="+b);  //false
        System.out.println("z="+z);   //0

        a = x>0 | y++>1;
        System.out.println("a="+a);  //false
        System.out.println("y="+y);  //2

        b = x>0 || z++>1;
        System.out.println("b="+b);  //false
        System.out.println("z="+z);	//0
    }
}*/


