package com.igeek.day02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 15:44
 * @description TODO
 * 键盘录入一个int类型的数据,使用三元运算符判断这个数是奇数还是偶数
 * 1.	创建键盘录入对象
 * 2.	调用方法获取输入的数据
 * 3.	将变量%2如果 == 0 是偶数,否则是奇数
 * 4.	输出结果
 */
public class Test01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入：");
        int a = sc.nextInt();

        boolean b = a%2==0?true:false;
        if (b){
            System.out.println("偶数");
        }else{
            System.out.println("奇数");
        }
    }
}
