package com.igeek.day02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 15:50
 * @description TODO
 * 键盘录入一个学生成绩(int类型),如果成绩大于等于60输出”及格”,如果成绩小于60输出”不及格”
 * 1.	创建键盘录入对象
 * 2.	调用方法获取输入的成绩
 * 3.	使用三元运算符如果成绩大于等于60返回"及格",否则返回不"及格"
 * 4.	输出结果
 */
public class Test02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入成绩：");
        int score  = sc.nextInt();
        String s = score<60?"不及格":"及格";
        System.out.println(s);
/*        if (score<60){
            System.out.println("不及格");
        }else{
            System.out.println("及格");
        }*/

    }
}
