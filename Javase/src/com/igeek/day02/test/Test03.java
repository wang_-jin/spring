package com.igeek.day02;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 15:50
 * @description TODO
 * 键盘录入自己的姓名(String),年龄(int),身高(int)保存到对应的变量中,
 * 输出结果如:我的姓名是张三,年龄25岁,身高180CM(提示姓名是String类型,需要使用Scanner的next()方法.)
 * 1.	创建Scanner对象
 * 2.	获取输入的姓名
 * 3.	获取输入的年龄
 * 4.	获取输入的身高
 * 5.	按照格式输出,注意字符串和变量的拼接
 */
public class Test03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("姓名：");
        String name = sc.next();
        System.out.println("年龄：");
        int age = sc.nextInt();
        System.out.println("身高：");
        int height = sc.nextInt();
        System.out.println("我的姓名是"+name+","+"年龄"+age+"岁,"+"身高"+height+"CM");
    }

}
