package com.igeek.ch10.multi_thread.lesson_09;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Tool2 {
    public static void main(String[] args) {
        /* 创建具有固定线程数量的线程池 */
        ExecutorService pool = Executors.newFixedThreadPool(3);
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());

        //关闭线程池
        pool.shutdown();
    }
}
