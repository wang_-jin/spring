package com.igeek.ch10.multi_thread.lesson_09;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Tool3 {
    public static void main(String[] args) {
        /*
        创建一个能够根据需要创建新线程的线程池,
        该线程池中的线程数可以根据任务量的大小自动调整,
        如果当前线程池中有空闲线程,就会重复利用这些线程
        来执行新的任务,如果没有空闲线程,就会创建新的线程来执行任务
        */
        ExecutorService pool = Executors.newCachedThreadPool();
        for (int i = 0; i < 50; i++) {
            pool.execute(new MyRunnable());
        }
        //关闭线程池
        pool.shutdown();
    }
}
