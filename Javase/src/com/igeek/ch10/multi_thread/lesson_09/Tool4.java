package com.igeek.ch10.multi_thread.lesson_09;

import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Tool4 {
    public static void main(String[] args) {
        /* 创建一个可以周期性执行任务的线程池 */
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
        pool.scheduleAtFixedRate(()->{
            LocalTime time = LocalTime.now();
            System.out.println(time);
            /*
            TimeUnit是一个枚举类型,用来提供时间单位
            */
        },1000,1000, TimeUnit.MILLISECONDS);
    }
}
