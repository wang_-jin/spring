package com.igeek.ch10.multi_thread.lesson_09;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Tool1 {
    public static void main(String[] args) {

        /* 等号左边创建线程池 等号右边创建只有一个线程的执行器 */
        ExecutorService pool = Executors.newSingleThreadExecutor();
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        pool.shutdown();
    }
}
