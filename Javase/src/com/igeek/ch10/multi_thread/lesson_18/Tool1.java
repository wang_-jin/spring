package com.igeek.ch10.multi_thread.lesson_18;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/*
高铁检票案例,假设有3个检票口同时检票,每个乘客就相当于一个线程,
最多同时允许有3个乘客一起检票
*/
public class Tool1 {
    public static void main(String[] args) {
        //创建一个semaphore对象,最多同时允许3个线程访问资源
        Semaphore semaphore = new Semaphore(3);
        for (int i = 1;i<=6;i++){
            new Thread(()->{
                try {
                    /*
                    acquire方法获取访问许可,使用这个方法后允许线程数量
                    会减少1,如从3变为2
                    */
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"开始检票...");
                    TimeUnit.SECONDS.sleep(3);
                    System.out.println(Thread.currentThread().getName()+"检票结束");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }finally {
                    //释放许可证,即乘客检完票,将检票口让出来给其他乘客检票用
                    semaphore.release();
                }
            },"乘客"+i).start();
        }
    }
}
