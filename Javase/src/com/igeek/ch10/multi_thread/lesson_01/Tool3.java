package com.igeek.ch10.multi_thread.lesson_01;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Tool3 {
    public static void main(String[] args) {
        /*创建带有返回值的任务对象*/
        MyCallable3 callable3 = new MyCallable3();
        /*创建FutureTask对象,将来获取任务对象的返回值*/
        FutureTask futureTask = new FutureTask(callable3);
        /*创建线程对象*/
        Thread thread = new Thread(futureTask,"t1");
        /*启动线程*/
        thread.start();
        try {
            String s = futureTask.get().toString();
            System.out.println("s="+s);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
