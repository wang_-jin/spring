package com.igeek.ch10.multi_thread.lesson_01;

public class MyThread1 extends Thread{
    public MyThread1(String name){
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i <100; i++) {
            //获取执行当前任务的线程
            Thread thread = Thread.currentThread();
            System.out.println(thread.getName()+":"+i);
        }
    }
}
