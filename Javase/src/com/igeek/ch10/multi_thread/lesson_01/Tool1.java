package com.igeek.ch10.multi_thread.lesson_01;

public class Tool1 {
    public static void main(String[] args) {
        MyThread1 myThread1 = new MyThread1("t");
        /*
        注意:
          启动线程需要使用start方法,不能使用run方法,
          调用run方法仍然是主线程在执行任务
        */
        //myThread1.run();
        //开启子线程
        myThread1.start();


    }
}
