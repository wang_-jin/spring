package com.igeek.ch10.multi_thread.lesson_01;

public class Tool4 {
    public static void main(String[] args) {
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+":"+i);
                }
            }
        },"t1").start();*/

        /*
        使用Lambda表达式简化写法2
        */
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"t1").start();
    }
}
