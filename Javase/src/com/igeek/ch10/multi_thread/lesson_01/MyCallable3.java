package com.igeek.ch10.multi_thread.lesson_01;

import java.util.concurrent.Callable;

public class MyCallable3 implements Callable<String> {
    @Override
    public String call() throws Exception {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum+=i;
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
        return sum+"";
    }
}
