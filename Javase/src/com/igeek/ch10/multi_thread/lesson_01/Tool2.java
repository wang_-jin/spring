package com.igeek.ch10.multi_thread.lesson_01;

public class Tool2 {
  public static void main(String[] args) {
        /*
        注意:
            多个线程之间执行任务是并行的,java中的线程调度采用抢占式调度机制.
            java多个线程之间没有固定的执行顺序,线程执行顺序由线程的优先级和
            cpu调度算法决定

        抢占式调度机制:
            在多线程编程中,线程抢占时间片是指多个线程轮流使用CPU的时间,
            以实现并发执行.每个线程被分配一小段时间,
            然后操作系统会切换到下一个线程执行.这样做可以避免某个线程独占
            CPU导致其他线程无法执行,提高程序的响应速度和稳定性.
            你可以把它看作是多个人轮流使用一个游戏机,每个人玩一段时间后,
            让下一个人玩,这样就不会有人占用游戏机时间过长,
            其他人无法玩的情况发生.
        */
    /*创建任务对象*/
    MyRunnable2 runnable = new MyRunnable2();
    /*创建子线程*/
    Thread thread1 = new Thread(runnable, "t1");
    /*开启子线程*/
    thread1.start();
    Thread thread2 = new Thread(runnable, "t2");
    thread2.start();
  }
}
