package com.igeek.ch10.multi_thread.lesson_01;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Tool5 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*FutureTask task = new FutureTask(new Callable() {
            @Override
            public Object call() throws Exception {
                int sum = 0;
                for (int i = 0; i < 10; i++) {
                    sum+=i;
                    System.out.println(Thread.currentThread().getName()+":"+i);
                }
                return sum+"";
            }
        });
        new Thread(task,"t1").start();
        String s = task.get().toString();
        System.out.println(s);*/

        /*使用Lambda简化写法3*/
        FutureTask task = new FutureTask(()->{
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                sum+=i;
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
            return sum+"";
        });
        new Thread(task,"t1").start();
        String s = task.get().toString();
        System.out.println(s);
    }
}
