package com.igeek.ch10.multi_thread.lesson_20;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class Tool1 {
    public static void main(String[] args) {
        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();
        for (int i = 1;i<=30;i++){
            int finalI = i;
            new Thread(()->{
                list.add(finalI);
                System.out.println(list);
            },"线程"+i).start();
        }
    }
}
