package com.igeek.ch10.multi_thread.lesson_23;

public class Test2 {
    public static void main(String[] args) throws InterruptedException {
        Student2 stu = new Student2();
        new Thread(()->{
            stu.study();
        }).start();
        Thread.sleep(1000);
        new Thread(()->{
            stu.sleep();
        }).start();
    }
}

class Student2{
    public synchronized void study() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("学习");
    }
    public synchronized void sleep(){
        System.out.println("睡觉");
    }
}
