package com.igeek.ch10.multi_thread.lesson_23;

public class Test3 {
    public static void main(String[] args) throws InterruptedException {
        Student3 stu1 = new Student3();
        Student3 stu2 = new Student3();
        new Thread(()->{
            stu1.study();
        }).start();
        Thread.sleep(1000);
        new Thread(()->{
            stu2.sleep();
        }).start();
    }
}

class Student3{
    public synchronized void study() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("学习");
    }
    public synchronized void sleep(){
        System.out.println("睡觉");
    }
}
