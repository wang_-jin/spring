package com.igeek.ch10.multi_thread.lesson_23;

public class Test5 {
    public static void main(String[] args) throws InterruptedException {
        Student5 stu1 = new Student5();
        Student5 stu2 = new Student5();
        new Thread(()->{
            stu1.study();
        }).start();
        Thread.sleep(1000);
        new Thread(()->{
            stu2.sleep();
        }).start();
    }
}

class Student5{
    //同步静态方法相当于类锁:Student5.class
    public static synchronized void study() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("学习");
    }
    public static synchronized void sleep(){
        System.out.println("睡觉");
    }
}
