package com.igeek.ch10.multi_thread.lesson_23;

public class Test1 {
    public static void main(String[] args) throws InterruptedException {
      Student1 stu = new Student1();
      new Thread(()->{
          stu.study();
      }).start();
      Thread.sleep(1000);
      new Thread(()->{
          stu.sleep();
      }).start();
    }
}
class Student1{
    //同步方法的锁相当于this锁
    public synchronized void study(){
        System.out.println("学习");
    }
    public synchronized void sleep(){
        System.out.println("睡觉");
    }
}
