package com.igeek.ch10.multi_thread.lesson_23;

public class Test6 {
    public static void main(String[] args) throws InterruptedException {
        Student6 stu = new Student6();
        new Thread(()->{
            stu.study();
        }).start();
        Thread.sleep(1000);
        new Thread(()->{
            stu.sleep();
        }).start();
    }
}

class Student6{
    public static synchronized void study() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("学习");
    }
    public synchronized void sleep(){
        System.out.println("睡觉");
    }
}
