package com.igeek.ch10.multi_thread.lesson_12;

import java.util.concurrent.atomic.AtomicInteger;

public class Tool1 {
  public static void main(String[] args) {
    MyRunable task = new MyRunable();
    for (int i = 0; i < 10000; i++) {
      new Thread(task).start();
    }
  }
}

class MyRunable1 implements Runnable {
  AtomicInteger count = new AtomicInteger(0);

  @Override
  public void run() {
    try {
      Thread.sleep(1);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    count.getAndIncrement();//类似count++
    //count.incrementAndGet();//类似++count
    System.out.println(count);

  }
}