package com.igeek.ch10.multi_thread.lesson_12;

public class Tool {
  public static void main(String[] args) {
    MyRunable task = new MyRunable();
    for (int i = 0; i < 10000; i++) {
      new Thread(task).start();
    }
  }
}

class MyRunable implements Runnable {
  private int count = 0;

  @Override
  public void run() {
    try {
      Thread.sleep(1);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    //使用类锁保证原子性
    synchronized (MyRunable.class) {
      count++;
      System.out.println(count);
    }
  }
}