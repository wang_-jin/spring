package com.igeek.ch10.multi_thread.lesson_12.Tool0;

public class Tool {
  public static void main(String[] args) {
    MyRunable task = new MyRunable();
      /*
      count小于10000的原因:
        假设有两个线程同时执行task任务,它们都将主内存中count=0读取
        到工作内存,都将count加上1,然后分别将count的新值刷新到主内存,
        此时主内存中count为1,而不是2,导致这个问题的原因就是线程的非
        原子性,因为线程在修改count的过程中被其他线程打断了,执行了其他
        线程中的操作!!!
        上面问题出现的可能执行步骤:
          线程1获取count=0,将count变为1还没来得及将新值刷新到主内存,
          线程2获取到时间片打断了线程1的操作,线程2获取count=0,将count
          变为1,将count=1刷新到主内存,轮到线程1继续将count1刷新到主内存,
          此时主内存中count=1!
      */
    for (int i = 0; i < 10000; i++) {
      new Thread(task).start();
    }
  }
}

class MyRunable implements Runnable {
  private int count = 0;

  @Override
  public void run() {
    try {
      Thread.sleep(1);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    count++;
    System.out.println(count);
  }
}