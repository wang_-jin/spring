package com.igeek.ch10.multi_thread.lesson_15;

public class Tool1 {
    public static void main(String[] args) {
        String lock1 = "lock1";
        String lock2 = "lock2";
        /*
        常见的死锁:
          线程1和线程2分别获取到了锁lock1和锁lock2,
          线程1需要等待线程2执行完释放锁lock2,才能执行
          完内部代码,释放锁lock1;线程2需要等待线程1执行完
          释放锁lock2,才能执行完内部代码,释放锁lock2.
        */
        new Thread(()->{
            synchronized (lock1){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (lock2){
                    System.out.println("去看电影");
                }
            }
        }).start();
        new Thread(()->{
            synchronized (lock2){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (lock1){
                    System.out.println("去看电影");
                }
            }
        }).start();
    }
}
