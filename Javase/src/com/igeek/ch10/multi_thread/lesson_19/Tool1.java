package com.igeek.ch10.multi_thread.lesson_19;

import java.util.concurrent.Exchanger;

public class Tool1 {
    public static void main(String[] args) {
      Exchanger<String> exchanger = new Exchanger<>();
      new BoyThread("唐三",exchanger).start();
      new GirlThread("小舞",exchanger).start();
    }
}

class BoyThread extends Thread{
    Exchanger<String> exchanger;
    public BoyThread(String name,Exchanger<String>exchanger){
        super(name);
        this.exchanger = exchanger;
    };

    @Override
    public void run() {
        try {
           //送出礼物鲜花,并收到礼物gift
           String gift =  exchanger.exchange("鲜花");
           System.out.println(Thread.currentThread().getName()+"收到了礼物:"+gift);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class GirlThread extends Thread{
    Exchanger<String> exchanger;
    public GirlThread(String name,Exchanger<String>exchanger){
        super(name);
        this.exchanger = exchanger;
    };

    @Override
    public void run() {
        try {
            //送出礼物围巾,并收到礼物gift
            String gift =  exchanger.exchange("围巾");
            System.out.println(Thread.currentThread().getName()+"收到了礼物:"+gift);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
