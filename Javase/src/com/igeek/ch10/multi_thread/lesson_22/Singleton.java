package com.igeek.ch10.multi_thread.lesson_22;

public class Singleton {
    private static volatile Singleton singleton;
    private Singleton(){

    }
    public static Singleton getSingleton(){
        /*
        这里判断了两次是否为 null 是因为在并发环境中当线程一执行了
        第一个判断的时候是为null,可此刻另外一个线程正好执行完初始化操作,
        在释放锁以后该线程并不知道已经初始化,如果此刻进入代码块不进行
        再次判断会再初始化一次,这就违背了单例模式的初衷了
        */
        if(singleton == null){
            synchronized (Singleton.class){
                if(singleton == null){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
