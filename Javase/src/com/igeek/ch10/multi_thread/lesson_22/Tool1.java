package com.igeek.ch10.multi_thread.lesson_22;

public class Tool1 {
    public static void main(String[] args) {
        for (int i = 0;i<50;i++){
            new Thread(()->{
                Singleton s = Singleton.getSingleton();
                System.out.println(Thread.currentThread().getName()+s);
            },"thread"+i).start();

        }

    }
}
