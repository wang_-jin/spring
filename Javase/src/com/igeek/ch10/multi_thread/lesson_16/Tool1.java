package com.igeek.ch10.multi_thread.lesson_16;

import java.util.concurrent.CountDownLatch;

public class Tool1 {
    public static void main(String[] args) throws InterruptedException {
        /* 模拟电影结束,观众离场,清洁工打扫电影院的操作 */
        CountDownLatch countDownLatch = new CountDownLatch(5);
        for(int i= 0;i<5;i++){
            new Thread(()->{
                //计数器减少1
                countDownLatch.countDown();
                System.out.println(Thread.currentThread().getName()+"已经离场");
            },"观众"+i).start();
        }
        //等待,直到计数器为0,才会继续往下执行
        countDownLatch.await();
        System.out.println("清洁工打扫电影院");
    }
}
