package com.igeek.ch10.multi_thread.lesson_16.test;

import java.util.concurrent.CountDownLatch;

public class Tool1 {
    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new Thread(()->{
            System.out.println("a");
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("c");
        },"线程1").start();

        new Thread(()->{
            try {
                //暂时让出时间片,让线程1先执行打印a
                Thread.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("b");
            countDownLatch.countDown();
        },"线程2").start();
    }
}
