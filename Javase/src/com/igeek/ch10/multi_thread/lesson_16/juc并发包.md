## juc并发包
juc是java util concurrent的简写,它提供了一组并发编程的工具类,
用于简化多线程编程的开发,提高多线程程序的性能和可靠性.
juc包提供了许多实用的工具类和接口,包括线程池,锁,原子变量,并发容器等

CountDownLatch:
  等待多个线程执行完某个操作再继续往下执行任务,
  CountDownLatch的主要方法是countDown()和await(),
  countDown()方法用于使计数器减1,而await()方法则是让
  等待线程进入等待状态,直到计数器的值为0,才会继续往下执行任务