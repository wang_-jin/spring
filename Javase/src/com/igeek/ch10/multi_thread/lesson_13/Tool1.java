package com.igeek.ch10.multi_thread.lesson_13;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

public class Tool1 {
    public static void main(String[] args) {
        /*
        AtomicStampedReference构造有两个参数:
          第一个主内存初始值;
          第二个初始版本号
        */
        AtomicStampedReference asr = new AtomicStampedReference(10,1);
        new Thread(()->{
            /*
            使用AtomicStampReference解决aba问题,原理是增加一个版本号,
            每次主内存的数据被修改版本号都加1,在创建AtomicStampedReference对象时,
            需要传入初始值和初始版本号,当AtomicStampedReference设置对象值时,
            对象值以及版本号都必须满足期望值,写入才会成功
            */

            /*
            compareAndSet(期望值,新值,期望版本号,新版本号)
            getStamp():获取当前版本号
            */
            boolean flag =  asr.compareAndSet(10,30,
                    asr.getStamp(),asr.getStamp()+1);
            asr.compareAndSet(30,10,
                    asr.getStamp(), asr.getStamp()+1);
        }).start();

        new Thread(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            boolean flag = asr.compareAndSet(10,30,
                    1,asr.getStamp()+1);
            System.out.println(asr.getReference());
            System.out.println("flag:"+flag);
        }).start();

    }
}

