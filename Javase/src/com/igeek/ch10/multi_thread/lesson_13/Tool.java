package com.igeek.ch10.multi_thread.lesson_13;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Tool {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(10);
        new Thread(()->{
            /*
            aba问题演示:
            compareAndSet方法会比较共享变量的当前值和期望的值是否相等,
            如果相等,就使用更新的值来更新共享变量,并返回true;
            否则,不做任何操作,并返回false.
            */
            boolean flag =  atomicInteger.compareAndSet(10,30);
            System.out.println("flag:"+flag);
            atomicInteger.compareAndSet(30,10);
        }).start();

        new Thread(()->{
            atomicInteger.compareAndSet(10,30);
            System.out.println(atomicInteger.intValue());
        }).start();

    }
}

