package com.igeek.ch10.multi_thread.lesson_17;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Tool1 {
    /* 集齐七颗龙珠召唤神龙 */
    public static void main(String[] args) {
        /*
        CyclicBarrier构造有两个参数:
          第一个参数:
            等待的线程数
          第二个参数:
            当等待的线程数达到屏障点时触发的回调函数
        */
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("七颗龙珠已经集齐,即将召唤神龙!");
        });

        for (int i = 1;i<=7;i++){
            int finalI = i;
            new Thread(()->{
                System.out.println("收集到"+ finalI +"号龙珠");
                try {
                    //await作用是让等待的线程数+1
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } catch (BrokenBarrierException e){
                    throw new RuntimeException(e);
                }
                
            }).start();
        }
    }
}
