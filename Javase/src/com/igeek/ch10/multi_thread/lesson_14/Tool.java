package com.igeek.ch10.multi_thread.lesson_14;

public class Tool {
  static int x = 0,y=0;
  static int a = 0,b=0;
  static void resort() throws InterruptedException {
    int i = 1;
    while (true){
      Thread t1 = new Thread(() -> {
          a=1;
          x=b;
      });
      Thread t2 = new Thread(() -> {
          b=1;
          y=a;
      });
      t1.start();
      t2.start();
      t1.join();
      t2.join();
      /*
      这个案例中x,y的打印结果有3种:
      x=0,y=1:
        线程1先执行,线程2后执行
      x=1,y=0:
        线程2先执行,线程1后执行
      x=1,y=1:
        线程1执行a=1,接着切换到线程2执行b=1,y=a(1),
        再切换到线程1执行x=b(1)
      x=0并且y=0是永远不会出现的,除非发生指令重排,
      a=1和x=b换位置,b=1和y=a换位置,换完位置后,线程1先
      执行x=b(0),接着切换到线程2,y=a(0),b=1,再切回线程1,
      a=1,这种情况下得到的才是x=0,y=0!!!
      */
      System.out.println("第"+i+"次"+":"+"x="+x+";"+"y="+y);
      i++;
      if(x==0&&y==0){
        break;
      }
      x=0;
      y=0;
      a=0;
      b=0;
    }

  }
  public static void main(String[] args) throws InterruptedException {
    resort();
  }
}

