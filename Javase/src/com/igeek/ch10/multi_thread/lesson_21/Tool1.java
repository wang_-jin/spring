package com.igeek.ch10.multi_thread.lesson_21;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Tool1 {
    public static void main(String[] args) {
        ConcurrentHashMap hashMap = new ConcurrentHashMap();
        for (int i = 0;i<30;i++){
            int finalI = i;
            new Thread(()->{
              hashMap.put(Thread.currentThread().getName(), finalI);
                System.out.println(hashMap);
            },"thread"+i).start();
        }

    }
}
