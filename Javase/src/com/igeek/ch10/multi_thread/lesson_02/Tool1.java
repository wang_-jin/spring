package com.igeek.ch10.multi_thread.lesson_02;
/*
setPriority(int):设置优先级
优先级范围1-10,一般来说高优先级的线程会优先执行,
但是优先级并不可靠,不要以此为判断线程执行顺序的依据
*/
public class Tool1 {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"t1");
        t1.setPriority(1);
        t1.start();
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"t2");
        t2.setPriority(10);
        t2.start();
    }
}
