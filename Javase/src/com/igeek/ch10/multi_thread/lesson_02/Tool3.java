package com.igeek.ch10.multi_thread.lesson_02;
/*
join()方法:等待一个线程执行完再继续往下执行
  在一个A线程中调用另外一个B线程的join方法,
  A线程会等待B执行完再继续执行!
*/
public class Tool3 {
    public static void main(String[] args) {
        /*
        要求:
          t1线程任务结束了t2线程任务才开始执行,
          t2线程结束了,主线程任务才结束
        */
        System.out.println("main开始");
        Thread t1 = new Thread(()->{
            System.out.println("t1开始");
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
            System.out.println("t1结束");
        },"t1");
        Thread t2 = new Thread(()->{
            try {
                t1.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("t2开始");
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
            System.out.println("t2结束");
        },"t2");
        t1.start();
        t2.start();
        try {
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("main结束");
    }
}
