package com.igeek.ch10.multi_thread.lesson_02;
/*
Thread.sleep和Thread.yield区别:
 1.sleep()方法会使当前线程阻塞,在睡眠时间内不会被执行,
 让出时间片给其他线程使用;
 yield方法只是让当前线程暂时让出时间片,随后立马又会
 进入到抢占时间片中!

 2.sleep()方法声明抛出InterruptedException异常;
 而yield()方法声明没有抛出任何异常;
*/
public class Tool2 {
    public static void main(String[] args) {
        new Thread(()->{
            for (int i = 0; i < 10; i++) {

                /*try {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName()+":"+i);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }*/

                /*
                尽管使用了Thread.yield()方法,
                但在实际执行中,仍有可能先执行t1线程的任务,
                然后再切换到其他线程执行.这可能是因为t1线程在切换之前
                已经获取了CPU时间片
                */
                Thread.yield();
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"t1").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        },"t2").start();
    }
}
