package com.igeek.ch10.multi_thread.lesson_06;

import java.util.concurrent.locks.ReentrantLock;

public class Tool1 {
  private static ReentrantLock lock = new ReentrantLock();
  void test(){
    lock.lock();
    for (int i = 0; i < 5; i++) {
      try {
        Thread.sleep(1000);
        System.out.println(Thread.currentThread().getName()+":"+i);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
    lock.unlock();
  }
  public static void main(String[] args) {
    Tool1 tool1 = new Tool1();
    Tool1 tool2 = new Tool1();
    Thread t1 = new Thread(()->{
      tool1.test();
    },"t1");
    Thread t2 = new Thread(()->{
      tool2.test();
    },"t2");
    t1.start();
    t2.start();
  }
}
