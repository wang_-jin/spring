package com.igeek.ch10.multi_thread.lesson_06;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
Lock锁:
  Lock接口:
    ReentrantLock可重用锁是Lock的实现类
  lock():
    获取锁,获取锁之后的代码会被锁定只允许一个线程访问,
    直到锁被释放
  unlock():
    释放锁,获取锁之后需要在任务完成后释放锁,如果不释放
    的话其他线程就访问不到被锁住的代码了
*/
public class Tool {
  /*
  注意:
    Lock锁属性不用static修饰相当于对象锁:this;
    Lock锁属性使用static修饰相当于类锁:类名.class;
  */
  private Lock lock = new ReentrantLock();

  void test() {
        /*
            获取锁:
              获取锁之后的代码会被锁定只允许一个线程访问,
              直到锁被释放.
        */
    System.out.println(lock);
    lock.lock();

    for (int i = 0; i < 10; i++) {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      System.out.println(Thread.currentThread().getName() + ":" + i);
    }
    //释放锁
    lock.unlock();
  }

  public static void main(String[] args) {
    /*
      实例锁只能锁相同对象,类似this锁
    */
    Tool tool = new Tool();
    Tool tool1 = new Tool();
    new Thread(() -> {
      tool.test();
    }, "t1").start();
    new Thread(() -> {
      tool1.test();
    }, "t2").start();
  }
}
