package com.igeek.ch10.multi_thread.lesson_03;

import java.util.concurrent.Callable;

public class MyRunnable implements Runnable {
  int count = 0;
  @Override
  public void run() {
    count = count + 1;
        /*
        两个线程同时修改同一个变量打印结果有下面几种:
        t1先执行,t2再执行:
          t1:1
          t2:2
        t2先执行,t1再执行:
          t2:1
          t1:2
        t1,t2同时执行:
          t1:1,t2:1或t2:1,t1:1
        t1(t2)先执行,执行完不打印,再执行t2(t1),
        最后打印t1,t2:
          t1:2,t2:2或t2:2,t1:2
        */
    //System.out.println(Thread.currentThread().getName() + ":" + count);

  }
}
