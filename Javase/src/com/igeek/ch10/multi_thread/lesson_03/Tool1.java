package com.igeek.ch10.multi_thread.lesson_03;



import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Tool1 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        for (int i = 0; i < 1000; i++) {
            MyRunnable r = new MyRunnable();
            Thread t1 = new Thread(r,"t1");
            Thread t2 = new Thread(r,"t2");
            t1.start();
            t2.start();
            t1.join();
            t2.join();
            /*
            多个线程修改同一个变量会造成安全问题,即修改结果
            的不确定性,造成这个的原因就是多线程抢占式调度!
            count打印结果有下面几种情况:
            1或者2
            如果为1说明线程1和线程2同时进行;
            如果为2说明线程1和线程2先后进行;

            */
            if(r.count == 1){
                System.out.println("count:"+r.count);
            }

        }
    }
}
