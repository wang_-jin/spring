package com.igeek.ch10.multi_thread.lesson_07;
/*
创建两个线程,轮流打印数字1-10
*/
class MyRunnable implements Runnable{
    private int num = 1;
    @Override
    public void run() {
        while (true){
            synchronized (this){
                if(num>10)break;
                //唤醒线程,解除阻塞
                notify();
                System.out.println(Thread.currentThread().getName()+":"+num);
                num++;
                try {
                    //阻塞当前线程,同时释放锁
                    wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }
}
public class Tool {
    public static void main(String[] args) {
        Runnable runnable = new MyRunnable();
        Thread thread1 = new Thread(runnable,"t1");
        Thread thread2 = new Thread(runnable,"t2");
        thread1.start();
        thread2.start();
    }
}
