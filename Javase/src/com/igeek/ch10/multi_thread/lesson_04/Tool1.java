package com.igeek.ch10.multi_thread.lesson_04;

/*
字符串锁:
  使用相同的字符串做锁,可以保证同一时刻只有一个
  线程访问该代码块,当代码执行完锁释放其他线程
  才会继续执行

byte数组锁:
  开销最小,作用类似于对象锁是this的情况
  定义一个byte数组属性,长度设置为0,
  byte[] bytes = new byte[0];
*/
public class Tool1 {
  byte[] bytes = new byte[0];

  void test1() {
    synchronized ("A") {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }

  }

  void test2() {
    synchronized ("A") {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  void test3() {
    synchronized (bytes) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  void test4() {
    synchronized (bytes) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  public static void main(String[] args) {
    //字符串锁
      /*Tool1 tool1 = new Tool1();
      Tool1 tool2 = new Tool1();
      new Thread(()->{
          tool1.test1();
      },"t1").start();
      new Thread(()->{
          tool2.test2();
      },"t2").start();*/

    Tool1 tool1 = new Tool1();
    new Thread(() -> {
      tool1.test3();
    }, "t1").start();
    new Thread(() -> {
      tool1.test4();
    }, "t2").start();
  }
}
