package com.igeek.ch10.multi_thread.lesson_04;

/*
同步代码块:
  synchronized(锁){
    //代码块
  }
  锁的类型:
  -对象锁:
    this:谁调用方法,this指向谁,this一般指实例对象
    确保同一时刻只有一个线程可以访问该对象,
    其他线程需要等待锁的释放才能继续执行.

  -类锁:
    类名.class
    锁定同一个类的所有对象,确保同一时刻只有一个线程
    可以访问该类下的对象

  注意:
    锁要保证唯一才能生效,如果不同的线程使用不同的锁,
    那么同步代码块就不能起到同步的作用,因此需要确保所有线程
    都使用同一个锁!
*/
public class Tool {
  void test1() {
    synchronized (this) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }

  }

  void test2() {
    synchronized (this) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  void test3() {
    synchronized (Tool.class) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  void test4() {
    synchronized (Tool.class) {
      for (int i = 0; i < 10; i++) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        System.out.println(Thread.currentThread().getName() + ":" + i);
      }
    }
  }

  public static void main(String[] args) {
    //对象锁
    Tool tool = new Tool();
    new Thread(() -> {
      tool.test1();
    }, "t1").start();
    new Thread(() -> {
      tool.test2();
    }, "t2").start();

    //类锁
    /*Tool tool = new Tool();
    Tool tool2 = new Tool();
    new Thread(() -> {
      tool.test3();
    }, "t1").start();
    new Thread(() -> {
      tool2.test4();
    }, "t2").start();*/
  }
}
