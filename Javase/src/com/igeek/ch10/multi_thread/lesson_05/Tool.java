package com.igeek.ch10.multi_thread.lesson_05;
/*
  同步方法:
   使用synchronized修饰实例方法,作用类似对象锁:this;
   使用synchronized修饰静态方法,作用类似类锁:类.class;
*/
public class Tool {
    synchronized void test1(){
        for (int i = 0; i <10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
    }

    synchronized void test2(){
        for (int i = 0; i <10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
    }

    synchronized static void test3(){
        for (int i = 0; i <10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
    }
    synchronized static void test4(){
        for (int i = 0; i <10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
    }

    public static void main(String[] args) {

        /*Tool tool = new Tool();
        new Thread(()->{
            tool.test1();
        },"t1").start();
        new Thread(()->{
            tool.test2();
        },"t2").start();*/

        Tool tool1 = new Tool();
        Tool tool2 = new Tool();
        new Thread(()->{
            tool1.test3();
        },"t1").start();
        new Thread(()->{
            tool2.test3();
        },"t2").start();
    }
}
