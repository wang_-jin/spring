package com.igeek.ch10.multi_thread.lesson_11.Tool0;

public class Tool {
  public static void main(String[] args) {

    MyRunable task = new MyRunable();
    /*
    创建了一个新线程,在新线程中将flag改为true
    */
    new Thread(task).start();
    /*
    在主线中使用循环获取flag的值,一旦flag为true
    就终止循环打印一段文字,但是我们发现这里获取的flag
    始终为false!这是线程的不可见性导致的问题,虽然子线程
    已经把flag改为true了,但是主线程循环一直在忙于获取缓存
    中的flag没有机会获取主存中的修改后的flag,如果我们在while
    里面加个几毫米的延迟让主线程有时间去获取主存中的flag,就能
    获取到修改后的为true的flag了!
    */
    while (true) {
      //加个延迟让主线程有时间去获取最新的flag
      /*try {
        Thread.sleep(1);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }*/
      if (task.flag) {
        System.out.println("执行了");
        break;
      }
    }
  }
}

class MyRunable implements Runnable {
  boolean flag = false;

  @Override
  public void run() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    flag = true;
    System.out.println("flag=" + flag);
  }
}
