package com.igeek.ch10.multi_thread.lesson_11;

public class Tool {
  public static void main(String[] args) {
    MyRunable task = new MyRunable();
    new Thread(task).start();
    while (true) {
            /*
            当一个线程进入synchronized块时,
            它会获取锁,并清空本地内存中的缓存数据,
            然后从主内存中读取最新的值,当线程退出synchronized块时,
            它会释放锁,并将修改后的值刷新到主内存中
            */
      synchronized (Tool.class) {
        if (task.flag) {
          System.out.println("执行了");
          break;
        }
      }

    }
  }
}

class MyRunable implements Runnable {
  boolean flag = false;

  @Override
  public void run() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    flag = true;
    System.out.println("flag=" + flag);
  }
}
