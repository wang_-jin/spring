package com.igeek.ch10.multi_thread.lesson_11;

public class Tool1 {
  public static void main(String[] args) {
    MyRunable1 task = new MyRunable1();
    new Thread(task).start();
    while (true) {
      if (task.flag) {
        System.out.println("执行了");
      }

    }
  }
}

class MyRunable1 implements Runnable {
  //volatile关键字可以让变量具备可见性
  volatile boolean flag = false;
  @Override
  public void run() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    flag = true;
    System.out.println("flag=" + flag);
  }
}
