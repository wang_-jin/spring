package com.igeek.ch10.multi_thread.lesson_10;



import com.igeek.ch10.multi_thread.lesson_03.MyRunnable;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Tool {
    private ThreadPoolExecutor pool;
    public Tool(int maxPoolSize,int queueSize){
      pool = new ThreadPoolExecutor(
              /*核心线程数:线程池中至少拥有的线程数*/
              1,
              /* 最大线程数:线程池最大能拥有几个线程 */
              maxPoolSize,
              /*
              keepAliveTime用于设置空闲线程的存活时间.
              当线程池中的线程数量超过核心线程数时,
              如果有一些线程处于空闲状态(即没有任务可执行),
              那么这些空闲线程就会被回收,以释放系统资源.
              但是如果线程池中的任务数量较少,而线程回收的速度较快,
              这会导致线程创建和销毁的开销较大,从而影响系统性能.

              为了避免这种情况,可以使用keepAliveTime参数来设置空闲线程的存活时间.
              当线程处于空闲状态的时间超过keepAliveTime时,该线程就会被回收,
              以释放系统资源.这样可以避免频繁地创建和销毁线程,提高系统的性能.
              需要注意的是,keepAliveTime参数只有在线程池中的线程数量超过核心
              线程数时才会起作用.如果线程池中的线程数量不超过核心线程数,
              直到线程池被关闭.
              */
              3000l,
              /* 时间单位 */
              TimeUnit.MILLISECONDS,
              /*
              阻塞队列:
                将待执行的任务放置到阻塞队列中,阻塞队列中有最大等待的任务数
              */
              new LinkedBlockingQueue(queueSize),
              /*
              拒绝策略:
                Java中的线程池在处理任务时,如果线程池中的线程数量已经达到上限,
                而又有新的任务需要执行,那么就需要采取一些策略来处理这种情况.
                这些策略被称为线程的拒绝策略(RejectedExecutionHandler),
                它们决定了当线程池无法处理任务时应该采取的行动

              默认拒绝策略AbortPolicy:
                当任务数大于maxPoolSize+queueSize直接拒绝抛出异常RejectedExecutionException
              */
              new ThreadPoolExecutor.AbortPolicy());
    }

    public void execute(Runnable task){
        pool.execute(task);
    }

    public static void main(String[] args) {
        Tool mypool = new Tool(2,3);
        for (int i = 1;i<=6;i++){
            mypool.execute(new MyRunnable());
        }
    }
}
