package com.igeek.ch02.work1.test3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/29 19:18
 * @description TODO
 */
public class Test {
    /*请用代码实现如下需求:
把你的生日字符串表示转换为对应的日期对象.
1. 在com.igeek.level1包下创建Test10类
2. 在Test10类中,提供main方法,在main方法中
a)	创建SimpleDateFormat对象 df,并制定日期格式
b)	调用df的parse(String str)方法,传入你的生日字符串;结束返回的日期对象
c)	打印这个日期对象
d)	使用try...catch处理解析异常

*/
    public static void main(String[] args) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDay = new Date();
        try {
            birthDay = df.parse("2000-02-18");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(birthDay.toString());
    }
}
