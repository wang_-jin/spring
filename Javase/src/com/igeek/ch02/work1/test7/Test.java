package com.igeek.ch02.work1.test7;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/29 19:21
 * @description TODO
 */
public class Test {
    /*写代码验证使用SimpleDateFormat把日期转换为字符串的时候;
创建SimpleDateFormat对象,比较消耗程序的执行性能
1.	在com.igeek.level2包下创建Test14类
2.	在Test14类中提供main方法,在main方法中
a)	使用System的currentTimeMillis()方法获取当前操作系统的毫秒值,作用程序执行的开始时间,使用start变量接收
b)	需要测试执行性能的代码
c)	使用System的currentTimeMillis()方法获取当前操作系统的毫秒值,作用程序执行的结束时间,使用end变量接收
d)	计算代码执行花费的时间 end - start,输出代码执行消耗的时间
3.	在Test14类中提供一个静态方法testSimpleDataFormatInLoop(),在方法中
a)	创建日期(Date)对象 now
b)	写一个循环100000次的for循环
c)	在循环中
i.	创建SimpleDateFormat对象 df,并制定日期格式为yyyy-MM-dd
ii.	调用df的format(Date date),传入now
d)	在main方法中,需要测试执行的性能的代码处调用testSimpleDataFormatInLoop()方法
e)	记录程序执行的效率
4.	在Test14类,完整复制testSimpleDataFormatInLoop()方法
a)	改名为testSimpleDataFormatOutLoop
b)	把创建SimpleDateFormat的代码移动到循环外部
c)	在main方法中,注释调用testSimpleDataFormatInLoop();
d)	在语句下一行调用testSimpleDataFormatOutLoop方法
e)	运行程序,查看输出结果,并记录
5.	得出结论: 创建日期格式对象,确实比较消耗程序性能,所以在实际开发一般会写一个工具类专门处理,日期的格式化与解析.

*/
    public static void testSimpleDataFormatInLoop(){

        Date now = new Date();
        for (int i = 0;i < 100000;i++){
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String s = df.format(now);
        }
    }
    public static void testSimpleDataFormatOutLoop(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        for (int i = 0;i < 100000;i++){
            String s = df.format(now);
        }
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        //testSimpleDataFormatInLoop();
        testSimpleDataFormatOutLoop();
        long end = System.currentTimeMillis();
        long time = end-start;
        System.out.println("time:"+time);
    }
}
