package com.igeek.ch02.work1.test1;

/**
 * @author wangjin
 * 2023/8/29 19:16
 * @description TODO
 */
public class Test {
    /*1． 训练案例1
1.1． 训练考核知识点
  Date类
1.2． 训练描述
请说出你对Date类的理解
1.3． 操作步骤描述
1.	请问Date类用于表示什么 ?  java.util.Date
2.	请问如何使用Date 类获取当前的日期?  Date date = new Date()
3.	请问如何把当前的日期转换为毫秒值?   long time = date.getTime();
4.	请问如何把毫秒值转换为日期? new Date(time);
2． 训练案例2
2.1． 训练考核知识点
    日期格式化  DateFormat  SimpleDateFormat
2.2． 训练描述
 请阐述如何实现日期对象与指定格式的字符串之间的转换
2.3． 操作步骤描述
1. 请说出日期格式化的核心类是什么?
2. 请说出日期指定格式的常见的规则是什么 ?  yyyy-MM-dd HH:mm:ss
3. 请说出如何把日期转换为指定格式的字符串 ? format()
4. 请说出如何把指定格式的字符串转换为日期 ? parse(“2020-12-12”)
3． 训练案例3
3.1． 训练考核知识点
Calendar类
3.2． 训练描述
请阐述Calendar的基本使用
3.3． 操作步骤描述
1.	请说出如何获取Calendar对象  Calendar c = Calendar.getInstance();
2.	请说出Calendar的常见方法有哪些,作用是什么?  c.get(Calendar.year)  set
3.	请说出Calendar注意事项有哪些? 抽象类，不能进行实例化

4． 训练案例4
4.1． 训练考核知识点
  System类
4.2． 训练描述
 请阐述System类的理解  java.lang.Systen
4.3． 操作步骤描述
1.	请说System类的特点是什么? err、in、out
2.	请说System类的常见方法有哪些,作用是什么? arrayCopy()  exit(0)  load()
5． 训练案例5
5.1． 训练考核知识点
    Math类
5.2． 训练描述
 请阐述你对Math类的理解  java.lang.Math  static静态的方法
5.3． 操作步骤描述
1.	请阐述Math类的概念?
2.	请说出Math类的常见方法有哪些,作用是什么?  abs  min max  random  pow
6． 训练案例6
6.1． 训练考核知识点
    包装类型  int-->Integer   char-->Character
byte-->Byte short-->Short long-->Long float-->Float  double-->Double boolean-->Boolean
6.2． 训练描述
 请阐述你对基本数据类型包装类的理解
6.3． 操作步骤描述
1.	请说出基本数据类型包装类的主要作用是什么?  使用逻辑的方法操作
2.	请说出基本数据类型对应的八个包装类分别是什么?
3.	请说出如何把字符串转换为对应的基本数据类型,有什么主意点?
基本数据类型 parseXxx(String)  包装器类型 valueOf(String)

7． 训练案例7
7.1． 训练考核知识点
自动装箱
自动拆箱
7.2． 训练描述
 请阐述你对自动装箱和自动拆箱的理解
7.3． 操作步骤描述
1.	请说出你对自动装箱的理解 将基本数据类型 --> 包装器类型
2.	请说出你对自动拆箱的理解 将包装器类型  --> 基本数据类型
Integer i = 10 ; i+=2; sout(i);
8． 训练案例8
8.1． 训练考核知识点
正则表达式
8.2． 训练描述
请阐述你对正则表达式的理解
8.3． 操作步骤描述
1.	请说出什么是正则表达式  String类型数据、定义规则
2.	请说出正则表达式的作用是什么?  检测出是否合规
3.	请说出字符串中关于正则表达式的常见方法有哪些?  matches(String regex)  split(String regex)  replaceAll(String regex, String replacement)
4.	请说出正则表达式的匹配规则是什么? [^abc] {n,m} .* \ $
*/
}
