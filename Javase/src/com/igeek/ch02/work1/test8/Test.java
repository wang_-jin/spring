package com.igeek.ch02.work1.test8;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/29 19:21
 * @description TODO
 */
public class Test {
    /*使用正则表达式切割字符串
* 192.168.105.27 按照  . 号切割字符串
* 18 22 40 65 按照空格切割字符串
    提示: 由于 . 号 在正则表达式中用于匹配除换行符以外的任意字符,会把字符串切的连渣都不剩,此处使用 . 号需要转义; 使用\\.作为切割使用正则表达式
3.3． 操作步骤描述
1. 在com.igeek.level2包下创建Test15
2. 在Test15类中提供main方法,在main方法中
a)	定义字符串变量str1,赋值为192.168.105.27
b)	调用str1的split的方法,传入 “\\.”,使用字符春数组strs1接收
c)	遍历数组strs1,输出每一个元素
d)	定义字符串变量str2,赋值为 18  22  40  65
e)	调用str1的split的方法,传入“\\s+”,使用字符春数组strs2接收
f)	遍历数组strs2,输出每一个元素
*/
    public static void main(String[] args) {
        String str1 = "192.168.105.27";
        String[] str2 = str1.split("\\.");
        for (String s : str2) {
            System.out.println(s);
        }
        System.out.println("------------------");
        String str3 = "18 22 40 65";
        String[] str4 = str3.split("\\s");
        for (String s : str4) {
            System.out.println(s);
        }
    }
}
