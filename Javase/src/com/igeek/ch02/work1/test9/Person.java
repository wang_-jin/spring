package com.igeek.ch02.work1.test9;

/**
 * @author wangjin
 * 2023/8/31 19:03
 * @description TODO
 */
public class Person {
    /*性别,性别,年龄和QQ号码属性*/
    String name;
    String sex;
    int age;
    String qqNum;

    public Person() {
    }

    public Person(String name, String sex, int age, String qqNum) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.qqNum = qqNum;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return qqNum
     */
    public String getQqNum() {
        return qqNum;
    }

    /**
     * 设置
     * @param qqNum
     */
    public void setQqNum(String qqNum) {
        this.qqNum = qqNum;
    }

    public String toString() {
        return "Person{name = " + name + ", sex = " + sex + ", age = " + age + ", qqNum = " + qqNum + "}";
    }
}
