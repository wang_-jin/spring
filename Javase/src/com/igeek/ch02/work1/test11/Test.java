package com.igeek.ch02.work1.test11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/29 19:23
 * @description TODO
 */
public class Test {
    /*使用SimpleDateFormat类,把2016-12-18转换为2016年12月18日.
提示: SimpleDateFormat对象的日期格式字符串可以通过applyPattern(String pattern)进行修改
*/
    public static void main(String[] args) throws ParseException {
        /*使用SimpleDateFormat类,把2016-12-18转换为2016年12月18日*/
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = df.parse("2016-12-18");
        df.applyPattern("yyyy年MM月dd日");
        String date = df.format(date1);
        System.out.println(date);


    }
}
