package com.igeek.ch02.work1.test;

/**
 * @author wangjin
 * 2023/8/22 16:04
 * @description TODO
 */
public class Test {
    /*1． 训练案例1
1.1． 训练考核知识点
  Object类
1.2． 训练描述
请谈谈你对Object类的理解
1.3． 操作步骤描述
1.	请说出Object类在继承层次结构中的位置
2.	请说出 Object 类的特点
2． 训练案例2
2.1． 训练考核知识点
    equals方法
2.2． 训练描述
 请阐述你对equals方法的理解
2.3． 操作步骤描述
1. 请说出equals方法在Object类中的默认实现什么?.
2. 请说出我们自定义类什么时候需要对equals方法进行重写.
3. 请说出重写equals方法注意事项
3． 训练案例3
3.1． 训练考核知识点
  toString方法
3.2． 训练描述
请阐述你对toString()的理解
3.3． 操作步骤描述
1.	请说出toString()方法在Object类默认返回的是什么?
2.	请说出什么时候需要重写toString()方法?
3.	在什么时候,会自动调用这个对象的toString()方法
*/
}
