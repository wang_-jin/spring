package com.igeek.ch02.work1.test5;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/29 19:20
 * @description TODO
 */
public class Test {
    /*编写一个校验用户名的程序,检测键盘录入的用户名是否合法
	 要求:必须以英文开头,只能包含英文,数字和_;最少6位,做多12位
	 1. 在com.igeek.level1中创建Test12类
2. 在Test12类中提供main方法,在main方法中
a)	创建键盘录入对象
b)	接收用户录入的用户名字符串
c)	调用字符串的matches方法,判断用户名是否合法
d)	如果合法就打印: 用户名合法
e)	如果不合法就打印: 用户名不合法

*/
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String str = sc.next();
        //必须以英文开头,只能包含英文,数字和_;最少6位,做多12位
        boolean flag = str.matches("[a-zA-Z][a-zA-Z0-9_]{5,11}");
        System.out.println(flag?"用户名合法":"用户名不合法");
    }
}
