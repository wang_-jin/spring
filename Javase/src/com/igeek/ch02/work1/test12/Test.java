package com.igeek.ch02.work1.test12;

/**
 * @author wangjin
 * 2023/8/29 19:23
 * @description TODO
 */
public class Test {
    /*请用代码实现
分别使用String的+= 和StringBuilder的append方法对字符串做100000次拼接,
计算String拼接100000次花费时间与StringBuilder拼接100000次所花费时间并打印
*/
    public static void main(String[] args) {
        long start1 = System.currentTimeMillis();
        String s = "123";
        for (int i = 0; i < 100000; i++) {
            s+=1;
        }
        long start2 = System.currentTimeMillis();
        System.out.println(start2-start1);


        long start3 = System.currentTimeMillis();
        StringBuilder s1 = new StringBuilder("123");
        for (int i = 0; i < 100000; i++) {

            s1.append("1");
        }
        long start4 = System.currentTimeMillis();
        System.out.println(start4-start3);
    }
}
