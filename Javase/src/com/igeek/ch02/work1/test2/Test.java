package com.igeek.ch02.work1.test2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/29 19:18
 * @description TODO
 */
public class Test {
    /*请用代码实现:
  获取当前的日期,并把这个日期转换为指定格式的字符串,如2088-08-08 08:08:08
  1. 创建包com.igeek.level1
2. 定义类Test09
3. 提供main方法,在main方法中
a)	获取当前日期对象 now;
b)	创建SimpleDateFormat对象 df,并制定日期格式
c)	调用df的format(Date  date) 方法,传入now; 接收返回的字符串
d)	打印这个字符串

*/
    public static void main(String[] args) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        String time = df.format(date.getTime());
        System.out.println("当前时间:"+time);
    }
}

