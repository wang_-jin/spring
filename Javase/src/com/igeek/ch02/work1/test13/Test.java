package com.igeek.ch02.work1.test13;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/29 19:24
 * @description TODO
 */
public class Test {
    /*模拟验证手机号的过程，按照以下要求实现相关代码（友情提示：不一定要用正则）
	a.	提示用户在控制台输入手机号，用一个字符串对其进行保存
	b.	判断该手机号位数是否是11位
	c.	判断该手机号，是否都是数字
*/
    /*不一定要用正则*/
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String phone = sc.next();
        System.out.println(phone.length()==11?"是11位":"不是11位");
        boolean flag = phone.matches("\\d{11}");
        System.out.println(flag?"都是数字":"不都是数字");
    }
}
