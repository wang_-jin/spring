package com.igeek.ch02.work1.test4;

import java.util.Calendar;

/**
 * @description TODO
 * @author wangjin
 * 2023/8/29 19:19
 */
public class Test {
    /*使用代码实现
500天后是几几年几月几日.
1.	创建包com.igeek.level1中创建Test11
2.	在Test11类中,提供main方法,在main方法中
a)	获取当前日历对象
b)	调用日期对象的add()方法,让当前日历向后移动500天
c)	获取日历中的年,月,日
d)	输出几几年几月几日.

*/
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH,500);
        System.out.println(c.get(Calendar.YEAR)+"年");
        System.out.println((c.get(Calendar.MONTH)+1)+"月");
        System.out.println(c.get(Calendar.DAY_OF_MONTH)+"日");
    }
}
