package com.igeek.ch02.work1.test10;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/29 19:22
 * @description TODO
 */
public class Test {
    /*通过代码实现:
把字符串 "Hello12345World6789012" 中所有数字替换为 "#"
提示: 字符串类的replaceAll(String regex,string str)方法,使用str替换掉当前字符串中所有与正则表达式匹配的字符
5.3． 操作步骤描述
1. 在com.igeek.level2包中
2. 创建Test17类,在Test17类中:
a)	提供main方法,在main方法中
b)	定义字符串变量str,赋值为:"Hello12345World6789012"
c)	调用str的replaceAll(String regex,String str),第一个参数传入”\\d+”,第二个参数传入”#”,定义字符串变量newStr接收替换后的字符串
d)	输出: newStr
*/
    public static void main(String[] args) {
        /*把字符串 "Hello12345World6789012" 中所有数字替换为 "#"
提示: 字符串类的replaceAll(String regex,string str)方法,使用str替换掉当前字符串中所有与正则表达式匹配的字符*/
        String str = "Hello12345World6789012";
        String s = str.replaceAll("\\d", "#");
        System.out.println(s);

    }
}
