package com.igeek.ch02.work1.test6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/29 19:20
 * @description TODO
 */
public class Test {
    /*请使用代码实现
计算你活了多少天
1. 创建包com.igeek.level2
2. 在包中创建测试类Test13
3. 在Test13类中,提供main方法,在main方法中
a)	把你生日的字符串,转换为日期对象
b)	把日期对象转换为对应的毫秒值
c)	获取当前系统的毫秒值
d)	当前系统毫秒值-出生时间对于的毫秒值计算你活的毫秒值
e)	把你活毫秒值转换为天
f)	输出天数
g)	使用try...catch处理异常

*/
    public static void main(String[] args) {
        String date = "2000-02-18";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            long birthDay = df.parse(date).getTime();
            long start = new Date().getTime();
            long day = (start-birthDay)/1000/60/60/24;
            System.out.println("天数:"+day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
