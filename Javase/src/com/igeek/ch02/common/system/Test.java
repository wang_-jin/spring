package com.igeek.ch02.common.system;

/**
 * @author wangjin
 * 2023/8/23 10:20
 * @description TODO
 */
public class Test {
    /*在控制台输出1-10000,计算这段代码执行了多少毫秒?*/
    public static void main(String[] args) {
        long a = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            System.out.println(i);
        }
        long b = System.currentTimeMillis();
        long c = b - a;
        System.out.println(c);
        System.exit(5);

    }
}
