package com.igeek.ch02.common.clone;

import java.util.Objects;

/**
 * @author wangjin
 * 2023/8/23 9:11
 * @description TODO
 */
public class Person implements Cloneable{
    String name;
    int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        return (Person)super.clone();
    }
}
