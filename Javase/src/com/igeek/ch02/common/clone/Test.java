package com.igeek.ch02.common.clone;

/**
 * @author wangjin
 * 2023/8/23 9:18
 * @description TODO
 */
public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person p = new Person("刘备",18);
        Person p1 = p.clone();
        Person p2 = p;
        System.out.println(p);
        System.out.println(p1);
        System.out.println("p==p1?:"+(p.equals(p1)));//Person类中重写了equals方法比较的是两个对象的内容而非地址
        System.out.println("p==p2?:"+(p==p2));
    }
}
