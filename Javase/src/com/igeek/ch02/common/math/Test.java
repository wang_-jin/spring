package com.igeek.ch02.common.math;

/**
 * @author wangjin
 * 2023/8/23 19:35
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        int a = -32;
        double b = 3.14;
        System.out.println("a的绝对值:"+Math.abs(a));
        System.out.println("b的向下取整:"+Math.floor(b));
        System.out.println("b的向上取整:"+Math.ceil(b));
        System.out.println("幂:"+Math.pow(2,3));
        System.out.println("获得随机数"+(int)(Math.random()*10));
        System.out.println("最小值"+Math.min(1,6));
        System.out.println("最大值"+Math.max(5,9));

    }
}
