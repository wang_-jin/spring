package com.igeek.ch02.common.time;

import java.util.Calendar;

/**
 * @author wangjin
 * 2023/8/22 16:17
 * @description TODO
 */
public class CalendarTest {
    /*距离当前时间365天后的日期和时间,星期*/
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH,365);
        System.out.println(c.get(Calendar.YEAR)+"年");
        System.out.println((c.get(Calendar.MONTH)+1)+"月");
        System.out.println(c.get(Calendar.DAY_OF_MONTH)+"日");
        int a = c.get(Calendar.DAY_OF_WEEK);
        System.out.println(getWeek(a));
    }
    static String getWeek(int index){
    /*
        获取星期几:
          注意:
            1.星期的第一天是从星期日开始,不是星期一;
            2.DAY_OF_WEEK范围是1-7,即星期日是1不是0;
    */
        //一个星期的第一天是星期日,Calendar中获取星期的数字是1
        String[] arr = {"星期日","星期一","星期二","星期三","星期四","星期五","星期六"};
        return arr[index-1];
    }
}
