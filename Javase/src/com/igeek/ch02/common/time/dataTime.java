package com.igeek.ch02.common.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/22 15:49
 * @description TODO
 */
public class dataTime {
    /*2012-12-28到2014-5-25总共多少天*/
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String data1 = "2012-12-28 00:00:00";
        String data2 = "2014-05-25 00:00:00";
        Date d1 = fmt.parse(data1);
        Date d2 = fmt.parse(data2);
        long time1 = d1.getTime();
        long time2 = d2.getTime();
        long time = time2-time1;
        int day = (int)(time/1000/60/60/24);
        System.out.println("总共天数:"+day);
    }
}
