package com.igeek.ch02.common.random;

import java.util.Random;

/**
 * @author wangjin
 * 2023/8/23 19:43
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Random r1 = new Random(2);
        Random r2 = new Random(2);

        System.out.println(r1.nextInt(10));
        System.out.println(r2.nextInt(10));
        System.out.println(r1.nextInt(5));
        System.out.println(r2.nextInt(5));
    }
}
