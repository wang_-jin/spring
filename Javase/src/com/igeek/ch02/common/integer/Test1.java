package com.igeek.ch02.common.integer;

/**
 * @author wangjin
 * 2023/8/23 20:33
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        //Integer-->int 拆箱
        Integer a = 12;
        int b = a.intValue();
        System.out.println(b);

        //int-->Integer 装箱
        int c = 14;
        Integer d = new Integer(c);
        System.out.println(d);
        //自动拆箱
        Integer e = 15;
        int f = e;
        //自动装箱
        int e1 = 16;
        Integer f1 = 17;
        //自动拆装箱
        int a1 = 18;
        Integer b1 = a1+19;
        System.out.println(b1);
        System.out.println("------------");
        //Integer-->String
        Integer m = 15;
        String s = m.toString();
        System.out.println(s);
        System.out.println(m+"");
        //String-->Integer
        String s1 = "1234";
        Integer m1 = new Integer(s1);
        System.out.println(m1);
        //String-->int
        String s2 = "12345";
        int a2 = Integer.parseInt(s2);
        System.out.println(a2);
        //int-->String
        int a3 = 20;
        System.out.println(20+"");
        /*字符串拼接:
        当一个操作数是字符串类型时,另一个操作数会自动转换成字符串,并将字符串连接起来
        */

    }
}
