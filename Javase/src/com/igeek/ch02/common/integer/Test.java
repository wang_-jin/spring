package com.igeek.ch02.common.integer;

/**
 * @author wangjin
 * 2023/8/23 19:51
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        String s = "hello";
        String s1 = "hello";
        Integer a = 10;
        Integer b = 10;

        Integer a1 = -129;
        Integer b1 = -129;


        System.out.println("s==s1?"+(s==s1));//字符串常量池 同一对象 true
        System.out.println("a==b?"+(a==b));//整型常量池 同一对象 true
        System.out.println("a==b?"+(a.equals(b)));//比较内容 true
        System.out.println("a1==b1?"+(a1==b1));//比较地址  false
        System.out.println("a1==b1?"+(a1.equals(b1)));//比较内容 true
        Integer a2 = new Integer(10);
        Integer b2 = new Integer(10);
        System.out.println("a2==b2?"+(a2==b2));  //false
        /*因为用了new关键词创建对象,所以会在堆中开辟内存空间用来存放新的对象,而不会存放在常量池中*/
        System.out.println("a2==b2?"+(a2.equals(b2))); //true
    }
}
