package com.igeek.ch02.common.regex.advance.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test1 {
  /* 获取alt=后面的书名打印出来 */
  static String str = "<dt><a href=\"/146_146557/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/146/146557.jpg\" alt=\"《离婚后，我神医身份曝光了》\"></a></dt>\n" +
    "<dd><h3><a href=\"/146_146557/\">《离婚后，我神医身份曝光了》</a></h3><span><a href=\"/writter/%E9%9C%B8%E7%8E%8B%E6%98%9F.html\" target=\"_blank\" title=\"霸王星\">霸王星</a></span><p>七年之痒，李默被离婚了，一腔情谊错付。黯然伤神后，李默只要女儿抚养权，一心做个称职奶爸。然而，风雨诡...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/155_155493/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/155/155493.jpg\" alt=\"《这个影帝不讲武德》\"></a></dt>\n" +
    "<dd><h3><a href=\"/155_155493/\">《这个影帝不讲武德》</a></h3><span><a href=\"/writter/%E6%B2%B3%E9%A9%AC%E4%B8%8D%E4%BC%9A%E5%8F%AB.html\" target=\"_blank\" title=\"河马不会叫\">河马不会叫</a></span><p>假如，我是说假如！假如我拍戏时带着五十个猛男，阁下又当如何应对？（华娱文）...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/155_155477/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/155/155477.jpg\" alt=\"《娱乐圈黑红的我，靠实力成为国宠》\"></a></dt>\n" +
    "<dd><h3><a href=\"/155_155477/\">《娱乐圈黑红的我，靠实力成为国宠》</a></h3><span><a href=\"/writter/%E6%8A%B9%E9%97%B2.html\" target=\"_blank\" title=\"抹闲\">抹闲</a></span><p>沈青黎从修真界回来，头一天就以“报假警”的方式将自己送上热搜，之后更是常年霸榜，黑红黑红不说，还硬生...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/155_155838/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/155/155838.jpg\" alt=\"《避孕药失效，我怀上了亿万总裁的继承人》\"></a></dt>\n" +
    "<dd><h3><a href=\"/155_155838/\">《避孕药失效，我怀上了亿万总裁的继承人》</a></h3><span><a href=\"/writter/%E5%8D%95%E6%99%9A%E5%98%89.html\" target=\"_blank\" title=\"单晚嘉\">单晚嘉</a></span><p>顾繁星前脚跟苏暮沉坦白怀孕的事。后脚俩人就去了民政局，当晚她就入住了苏总那一千多平米的大豪宅。身为苏...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/156_156072/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/img/17.jpg\" alt=\"《末世纹身：我为守护神，力挽天倾》\"></a></dt>\n" +
    "<dd><h3><a href=\"/156_156072/\">《末世纹身：我为守护神，力挽天倾》</a></h3><span><a href=\"/writter/%E6%A2%A6%E9%87%8C%E7%94%BB%E5%8D%BF%E9%A2%9C.html\" target=\"_blank\" title=\"梦里画卿颜\">梦里画卿颜</a></span><p>《末世纹身：我为守护神，力挽天倾》...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/156_156073/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/uploads/156/156073.jpg\" alt=\"《灵戒猎手》\"></a></dt>\n" +
    "<dd><h3><a href=\"/156_156073/\">《灵戒猎手》</a></h3><span><a href=\"/writter/%E5%8F%91%E5%93%A5%E6%80%AA%E8%B0%88.html\" target=\"_blank\" title=\"发哥怪谈\">发哥怪谈</a></span><p>据计算，平均500克的反物质能量在现实世界发生‘湮灭’，相当于一颗氢弹的威力！有人通过神秘仪式造成现...</p></dd>\n" +
    "</dl><dl>\n" +
    "<dt><a href=\"/156_156522/\"><img rel-class=\"lazyload\" src=\"/Public/images/nocover.jpg\" data-original=\"/Public/images/nocover.jpg\" alt=\"《西游：都成玉帝了，佛门还想大兴》\"></a></dt>\n" +
    "<dd><h3><a href=\"/156_156522/\">《西游：都成玉帝了，佛门还想大兴》</a></h3><span><a href=\"/writter/%E7%88%B1%E5%90%83%E8%A5%BF%E7%93%9C%E9%85%B1.html\" target=\"_blank\" title=\"爱吃西瓜酱\">爱吃西瓜酱</a></span><p>《西游：都成玉帝了，佛门还想大兴》...</p></dd>\n" +
    "</dl><dl>";


  public static void main(String[] args) {
    Pattern ptn = Pattern.compile("alt=\"(.*?)\">");
    Matcher matcher = ptn.matcher(str);
    while (matcher.find()){
      String s = matcher.group(1);
      System.out.println("s:"+s);
    }
  }
}
