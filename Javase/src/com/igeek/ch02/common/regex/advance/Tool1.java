package com.igeek.ch02.common.regex.advance;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class
Tool1 {
  public static void main(String[] args) {
    String str1 = "123456";
    /* 使用Matcher匹配是包含匹配 */
    //设置匹配模式
    Pattern ptn = Pattern.compile("\\d{3}");
    //加上^和$表示以这个字符串开头和结尾,就是一个完整匹配了
    //Pattern ptn = Pattern.compile("^\\d{3}$");
    //设置匹配器
    Matcher matcher = ptn.matcher(str1);
    //查找符合正则条件的子字符串
    while (matcher.find()){
      //获取符合条件的子字符串
      String s = matcher.group();
      System.out.println("s:"+s);
    }

    /* matches是完整匹配 */
    boolean flag =  str1.matches("\\d{3}");
    System.out.println(flag);
  }
}
