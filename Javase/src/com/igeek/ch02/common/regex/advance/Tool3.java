package com.igeek.ch02.common.regex.advance;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tool3 {
  public static void main(String[] args) {
    /*获取等于号左边的属性和右边的属性值*/
    String str = "name=刘备;age=18;height=170;like=swim;";
    //下面的正在定义了两个分组
    Pattern ptn = Pattern.compile("(\\w+)=(.*?);");
    Matcher matcher = ptn.matcher(str);
    while (matcher.find()){
      //获取匹配到的完整字符串
      String s = matcher.group(0);
      System.out.println("s:"+s);
      //获取第一个分组
      String s1 = matcher.group(1);
      System.out.println("s1:"+s1);
      //获取第二个分组
      String s2 = matcher.group(2);
      System.out.println("s2:"+s2);
    }
  }
}
