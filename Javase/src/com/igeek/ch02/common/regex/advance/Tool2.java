package com.igeek.ch02.common.regex.advance;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tool2 {
  public static void main(String[] args) {
/*    String str = "a1234";
    //默认是贪婪匹配,尽可能多的匹配匹配结果:a1
    Pattern ptn = Pattern.compile("a\\d?");
    //在数量限定符后面加?变为非贪婪匹配尽量少的匹配,匹配结果:a
    //Pattern ptn = Pattern.compile("a\\d??");
    Matcher matcher = ptn.matcher(str);
    while (matcher.find()){
      String s = matcher.group();
      System.out.println("s:"+s);
    }*/

    String str = "123456";
    //贪婪匹配,匹配结果:12345
    Pattern ptn = Pattern.compile("\\d{2,5}");
    //非贪婪匹配,匹配结果:12 34 56
    //Pattern ptn = Pattern.compile("\\d{2,5}?");
    Matcher matcher = ptn.matcher(str);
    while (matcher.find()){
      String s = matcher.group();
      System.out.println("s:"+s);
    }
  }
}
