package com.igeek.ch02.common.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/24 19:49
 * @description TODO
 */
public class Test5 {
    public static void main(String[] args) {

        /*正向先行断言:*/
        String str = "<p class=box>点赞数:345</p>";
        //获取</p>前面的数字
/*        Pattern ptn = Pattern.compile("\\d+(?=</p>)");
        Matcher matcher = ptn.matcher(str);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println(s);
        }*/

        /* 正向后行断言*/
        //获取class的属性值box
/*        Pattern ptn = Pattern.compile("(?<=class=)\\w+");
        Matcher matcher = ptn.matcher(str);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println(s);
        }*/
        /* 负向先行断言*/
/*        String str1 = "11筷子,12婴儿,13医生;";
        //匹配数字,数字后面不能是医生
        Pattern ptn = Pattern.compile("\\d{2}(?!医生)");
        Matcher matcher = ptn.matcher(str1);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println(s);
        }*/
        /* 负向后行断言*/
        String str1 = "我爱吃#apple,你爱吃#apple,他爱吃#apple";
        //查找"#apple",前面不能是"我爱吃"
        Pattern ptn = Pattern.compile("(?<!我爱吃)#apple");
        Matcher matcher = ptn.matcher(str1);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println(s);
        }
    }
}
