package com.igeek.ch02.common.regex;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/23 16:22
 * @description TODO
 */
public class Test3 {
    /*1.验证用户密码,长度在6~18 之间,只能包含英文和数字

2.验证电子邮件是否合法:
  1.只能有一个@
  2.@左边是用户名,可以是a-z,A-Z,0-9,_字符
  3.@右边是域名,域名由字母和点组成,如baidu.com,jd.cn.com

3.验证一个数是不是整数或小数,需要考虑正数和负数
如:123 -123 23.35 -34.67 0.34 -0.45

4.将字符串"ab@qq.com;df#123.com;uh@yahu.cn;dfg@jd.com"转为
     数组{ab@qq.com,df@123.com,uh@yahu.com,dfg@jd.com}*/
    public static void main(String[] args) {
/*        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String a = sc.next();*/
/*        String s = "asD123456";
        boolean flag = s.matches("[a-zA-Z0-9]{6,18}");
        System.out.println(flag);*/
/*        String s = "aZ56_@baidu.com";
        boolean flag = s.matches("\\w+@(\\w+[.])+com");
        System.out.println(flag);*/

/*        String a = "-34.67";
        boolean flag = a.matches("-?\\d+([.]\\d+)?");
        System.out.println(flag);*/
        String s = "ab@qq.com;df#123.com;uh@yahu.cn;dfg@jd.com";
        s = s.replaceAll("cn","com").replaceAll("#","@");
        String[] arr = s.split(";");
        System.out.println(Arrays.toString(arr));
    }
}
