package com.igeek.ch02.common.regex;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/24 18:47
 * @description TODO
 */
public class Test4 {
    public static void main(String[] args) {
/*        String str = "123456";
        *//*包含匹配*//*
        Pattern p = Pattern.compile("\\d{3}");
        *//*完整匹配*//*
        //Pattern p = Pattern.compile("^\\d{6}$");
        Matcher matcher = p.matcher(str);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println("s:"+s);
        }
        *//*matches是完整匹配*//*
        boolean flag = str.matches("^\\d{3}$");
        System.out.println(flag);*/

        /*默认贪婪匹配*/
/*        Pattern ptn = Pattern.compile("\\d{2,5}");
        Matcher matcher1 = ptn.matcher(str);
        while(matcher1.find()){
            String s1 = matcher1.group();
            System.out.println(s1);
        }*/
        /*非贪婪匹配*/
/*        Pattern p = Pattern.compile("\\d{2,5}?");
        Matcher matcher = p.matcher(str);
        while (matcher.find()){
            String s = matcher.group();
            System.out.println(s);
        }*/
        /*获取等于号左边的属性和右边的属性值*/
        String str = "name=刘备;age=18;height=170;like=swim;";
        Pattern p = Pattern.compile("(\\w+)=(.+?);");
        Matcher matcher = p.matcher(str);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println("s:"+s);
            String s1 = matcher.group(1);
            System.out.println("s:"+s1);
            String s2 = matcher.group(2);
            System.out.println("s:"+s2);
        }
    }
}
