package com.igeek.ch02.common.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/25 9:21
 * @description TODO
 */
public class Test6 {
    public static void main(String[] args) {
        String str = "123abc456asf12";
        Pattern p = Pattern.compile("a(?=s)");
        Matcher matcher = p.matcher(str);
        while (matcher.find()){
            String s = matcher.group();
            System.out.println("s:"+s);
        }
    }
}
