package com.igeek.ch02.common.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/24 11:35
 * @description TODO
 */
public class Test2 {
    public static void main(String[] args) {
            /*
    获取url中a,b,c的值打印出来
    */
        String str = "http://www.baidu.com?a=17&b=24&c=31";
        Pattern ptn = Pattern.compile("(?<==)\\d+");
        Matcher matcher = ptn.matcher(str);
        while(matcher.find()){
            String s = matcher.group();
            System.out.println("s:"+s);
        }
    }
}
