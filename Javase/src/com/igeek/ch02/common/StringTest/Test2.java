package com.igeek.ch02.common.StringTest;



/**
 * @author wangjin
 * 2023/8/23 14:22
 * @description TODO
 */
public class Test2 {
    /*练习3：判断一个字符串是否是对称字符串。
    例如"abc"不是对称字符串，"aba"、"abba"、"aaa"、"mnanm"是对称字符串*/
    public static void main(String[] args) {
        StringBuilder s = new StringBuilder("asdfdsa");
        StringBuilder s1 = new StringBuilder();
        for (int i = s.length(); i>0; i--) {
            char c = s.charAt(i-1);
            s1.append(c);
        }
        String s2 = s.toString();
        String s3 = s1.toString();
        //System.out.println("s1==s?" + s.toString().equals(s1.toString()));
        /*StringBuilder中没有重写equals方法,所以用的是
        Object类中的equals方法,而Object中的equals方法比较的是地址而不是内容,所以返回的是false*/
        //System.out.println("s1==s?"+s.equals(s1));//false
        System.out.println("s2==s3?"+s2.equals(s3));//true

    }
}
