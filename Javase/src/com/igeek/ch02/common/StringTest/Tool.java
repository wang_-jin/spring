package com.igeek.ch02.common.StringTest;

public class Tool {
    /*
    Integer类是int的包装器类型
    整型常量池:
      和字符串类似,java整型也有一个整型常量池,范围是:-128~127,
      当我们使用-128到127之间的整数是直接从整型常量池中引用!
    */
    public static void main(String[] args) {
        Integer a1 = 10;
        Integer a2 = 10;
        //128不在整型常量池中,需要使用new开辟一块新的内存空间存储
        Integer a3 = 128;
        Integer a4 = 128;
        Integer a5 = new Integer(10);
        Integer a6 = new Integer(10);
        System.out.println("a1==a2"+(a1==a2));
        System.out.println("a3==a4"+(a3==a4));
        System.out.println("a5==a6"+(a5==a6));

        System.out.println("a1==a2"+(a1.equals(a2)));
        System.out.println("a3==a4"+(a3.equals(a4)));
        System.out.println("a5==a6"+(a5.equals(a6)));
    }
}
