package com.igeek.ch02.common.StringTest;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/23 11:36
 * @description TODO
 */
public class Test {
    /*练习1：用StringBuilder把数组拼接成一个字符串
       举例：int[] arr = {1,2,3}; 结果：[1, 2, 3]*/
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <arr.length; i++) {
            if (i==0){
                sb.append("["+arr[i]+",");
            }else if (i==arr.length-1){
                sb.append(arr[i]+"]");
            }else {
                sb.append(arr[i]+",");
            }
        }
        System.out.println(sb);
    }

}
