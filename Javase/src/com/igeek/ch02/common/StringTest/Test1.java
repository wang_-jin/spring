package com.igeek.ch02.common.StringTest;

/**
 * @author wangjin
 * 2023/8/23 13:57
 * @description TODO
 */
public class Test1 {
    /*练习2：利用StringBuilder完成字符串反转*/
    public static void main(String[] args) {
/*        StringBuilder s = new StringBuilder("asdfg");
        StringBuilder s1 = new StringBuilder();
        for (int i = s.length(); i > 0; i--) {
            s1.append(s.charAt(i-1));
        }
        System.out.println(s1);
        s1.reverse();
        System.out.println(s1);*/
        StringBuilder s = new StringBuilder("hello");
        int left_index = 0;
        int right_index = s.length()-1;
        for (int i = 0; i < s.length()/2; i++) {
            if (left_index<right_index){
                char temp = s.charAt(i);
                s.setCharAt(i,s.charAt(s.length()-1-i));
                s.setCharAt(s.length()-1-i,temp);
                right_index--;
                left_index++;
            }
        }
        System.out.println(s);
    }
}
