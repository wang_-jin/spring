package com.igeek.ch02.common_class.lesson_04.test;

import java.util.Calendar;

// 距离当前时间365天后的日期和时间,星期
public class Test {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH,365);
        System.out.println(c.get(Calendar.YEAR));
        System.out.println(c.get(Calendar.MONTH)+1);
        System.out.println(c.get(Calendar.DAY_OF_MONTH));
        System.out.println(c.get(Calendar.HOUR));
        System.out.println(c.get(Calendar.MINUTE));
        System.out.println(c.get(Calendar.SECOND));
        System.out.println(getWeek(c.get(Calendar.DAY_OF_WEEK)));
    }
    public static String getWeek(int num){
        String[] weeks = {"星期日","星期一","星期二","星期三","星期四","星期五","星期六",};
        return weeks[num-1];
    }
}
