package com.igeek.ch02.common_class.lesson_04;

import java.util.Calendar;

public class Tool {
    public static void main(String[] args) {
        /*
        获取Calendar实例对象,这里不能new实例对象
        因为Calendar是抽象类,不能实例化
        */
        Calendar c = Calendar.getInstance();
        //获取年
        System.out.println(c.get(Calendar.YEAR));
        /*
        获取月:
          注意:
            月是从0开始的,因此我们获取月份时需要将月份+1
        */
        System.out.println(c.get(Calendar.MONTH)+1);
        //获取日
        System.out.println(c.get(Calendar.DAY_OF_MONTH));
        //获取小时
        System.out.println(c.get(Calendar.HOUR));
        //获取分钟
        System.out.println(c.get(Calendar.MINUTE));
        //获取秒数
        System.out.println(c.get(Calendar.SECOND));
        /*
        获取星期几:
          注意:
            1.星期的第一天是从星期日开始,不是星期一;
            2.DAY_OF_WEEK范围是1-7,即星期日是1不是0;
        */
        System.out.println(getWeek(c.get(Calendar.DAY_OF_WEEK)));

        /*
        set方法：
          设置年月日
        */
        setDate(c,2023,0,18);

        /*
        add方法：
          计算从当前日期开始往后的日期
        */
        Calendar c1 = Calendar.getInstance();
        //计算90天之后的日期
        c1.add(Calendar.DAY_OF_MONTH,90);
        System.out.println(c1.get(Calendar.MONTH)+1);
        System.out.println(c1.get(Calendar.DAY_OF_MONTH));



    }
    public static void setDate(Calendar c,int year,int month,int day){
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,day);
        System.out.println(c);
    }
    public static String getWeek(int num){
        String[] weeks = {"星期日","星期一","星期二","星期三","星期四","星期五","星期六",};
        return weeks[num-1];
    }
}
