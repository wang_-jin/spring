package com.igeek.ch02.common_class.lesson_02;

import java.util.Objects;

public class Test {
    public static void main(String[] args) {
        Object a = null;
        Object b = new Object();
        //打印false
        System.out.println(Objects.equals(a,b));
        //因为a为空直接报错
        //System.out.println(a.equals(b));
        System.out.println(a.toString());//a.toString();//报错
        //System.out.println(Objects.toString(a));//Objects.toString(a);//正常打印
        /*
        a.toString();//报错
        Objects.toString(a);//正常打印
        */
    }
}
