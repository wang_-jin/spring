## Objects类
Object和Objects的区别:
  1.Object来自java.lang.object;
  Objects来自java.util.objects;
  2.Object是所有类的根类,所有类都直接或间接继承自Object,Object上
  的所有方法,其他类都能使用;
  Objects是一个非空判断处理的工具类,为一些
  方法加入了检测空数据的处理逻辑,使得方法更加安全,如equals,toString等方法.