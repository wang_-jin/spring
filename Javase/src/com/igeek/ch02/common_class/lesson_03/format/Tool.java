package com.igeek.ch02.common_class.lesson_03.format;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
public class Tool {
    public static void main(String[] args) throws ParseException {
        /*
        yyyy:year年 2023
        MM:month月 09
        dd:day日 06
        HH:hour时24小时制 hh:hour12小时制
        mm:minute分 09
        ss:seconds秒 08
        */
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        //Date-->String
        String date = fmt.format(now);
        System.out.println(date);
        //String-->Date
        String datestr = "2022-03-05 11:23:34";
        Date d = fmt.parse(datestr);
        System.out.println(d);
    }
}
