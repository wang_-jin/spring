package com.igeek.ch02.common_class.lesson_03.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Test {
    /*
    2012-12-28到2014-5-25总共多少天
    */
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String start = "2012-12-28";
        String end = "2014-05-25";
        //获取long类型时间戳
        long startCount = fmt.parse(start).getTime();
        long endCount = fmt.parse(end).getTime();
        //计算两个时间相差的毫秒数
        long time = endCount-startCount;
        //将毫秒转为天数
        long day = time/1000/60/60/24;
        System.out.println(day);
    }
}
