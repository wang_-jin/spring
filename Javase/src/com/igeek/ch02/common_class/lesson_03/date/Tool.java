package com.igeek.ch02.common_class.lesson_03.date;
import java.util.Date;

public class Tool {
    public static void main(String[] args) {
        /*
        周一：Monday 周二：Tuesday 周三：Wednesday 周四：Thursday
        周五：Friday 周六：Saturday 周日：Sunday

        一月：January 二月：February 三月：March 四月：April
        五月：May 六月：June 七月：July 八月：August 九月：September
        十月：October 十一月：November 十二月：December
        */
        //获取当前时间
        Date nowtime = new Date();
        System.out.println(nowtime);
        //Date-->long 获取当前时间到1970年1月1日0点:0分:0秒的毫秒数
        //毫秒数我们也称为时间戳
        long longtime = nowtime.getTime();
        System.out.println(longtime);
        /*
        long-->Date
        currentTimeMillis中Millis是Milliseconds的简写,
        表示当前时间的毫秒数,返回值是long类型
        */
        long l = System.currentTimeMillis();
        Date d = new Date(l);
        System.out.println(d);
    }
}
