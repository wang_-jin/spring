package com.igeek.ch02.common_class.lesson_09;

public class Tool {
    public static void main(String[] args) {
        /*
        new StringBuilder会创建一个字符数组char[],
        默认容量是16
        */
        StringBuilder s1 = new StringBuilder("hello");
        //append在字符串末尾拼接一个元素
        s1.append("1");
        s1.append("2");
        System.out.println(s1);
        //charAt获取对应索引位置的字符元素
        System.out.println(s1.charAt(0));
        //setCharAt设置对应位置的字符元素
        s1.setCharAt(1,'9');
        System.out.println(s1);
        //length()获取字符串长度
        System.out.println(s1.length());
        //toString转为字符串
        System.out.println(s1.toString());
    }
}
