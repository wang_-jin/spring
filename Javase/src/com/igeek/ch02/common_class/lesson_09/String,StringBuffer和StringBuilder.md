## String,StringBuffer和StringBuilder

String是不可变字符串

StringBuffer和StringBuilder是可变字符序列:
  StringBuffer:是线程安全的,但是效率低,适用于多线程
  StringBuilder:是线程不安全的,但是效率高,适用于单线程
注意:
StringBuffer和StringBuilder的api几乎一样,区别仅仅是
一个是线程安全的,一个是线程不安全的
