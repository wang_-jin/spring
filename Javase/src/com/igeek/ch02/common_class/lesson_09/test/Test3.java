package com.igeek.ch02.common_class.lesson_09.test;
//练习3:判断一个字符串是否是对称字符串.
//例如"abc"不是对称字符串,"aba","abba","aaa","mnanm"是对称字符串
public class Test3 {
    public static void main(String[] args) {
      StringBuilder s = new StringBuilder("aba");
        System.out.println(isSym(s));
    };
    public static boolean isSym(StringBuilder s){
        int left = 0;
        int right = s.length()-1;
        while (left<=right){
            if(s.charAt(left) != s.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    };
}
