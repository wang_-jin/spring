package com.igeek.ch02.common_class.lesson_09.test;

public class Test1 {
    public static void main(String[] args) {
        //练习1：用StringBuilder把数组拼接成一个字符串
        int[] arr = {1,2,3};
        StringBuilder s = new StringBuilder();
        for (int i = 0; i <arr.length;i++) {
            int item = arr[i];
            if(i==0){
                s.append("["+item+",");
            } else if (i == arr.length-1) {
                s.append(item+"]");
            }else{
                s.append(item+",");
            }
        }
        System.out.println(s.toString());
    }
}
