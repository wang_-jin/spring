package com.igeek.ch02.common_class.lesson_09.test;

public class Test2 {
    public static void main(String[] args) {
        //练习2:利用StringBuilder完成字符串反转
        StringBuilder s = new StringBuilder("hello");
        int left = 0;
        int right = s.length()-1;
        while (left<=right){
            char temp = s.charAt(left);
            s.setCharAt(left,s.charAt(right));
            s.setCharAt(right,temp);
            left++;
            right--;
        }
        System.out.println(s.toString());
    }
}
