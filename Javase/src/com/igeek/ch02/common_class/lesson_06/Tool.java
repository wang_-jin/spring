package com.igeek.ch02.common_class.lesson_06;

import java.util.Random;

public class Tool {
    public static void main(String[] args) {
        /*
        Random r = new Random();
        //获取随机整数0到9范围[0,10)
        System.out.println(r.nextInt(10));
        //获取随机整数1-10范围[1,11)
        System.out.println(r.nextInt(10)+1);
        */

        /*
        Random有参构造函数可以传一个long类型的整数作为
        参数,这个参数我们称为随机种子,相同种子的Random对象,
        生成相同次数的随机数是完全相同的,也就是说如果两个
        Random对象随机种子相同,那么第一次生成的随机数字完全相同,
        第二次生成的随机数字也完全相同...

        注意:
          Random无参构造的随机种子默认是当前时间的时间戳,
          因此每次获取的随机数都不一样,因为每次随机种子
          都不一样!
        */
        Random r1 = new Random(1);
        Random r2 = new Random(1);
        //第一次随机
        System.out.println(r1.nextInt(10));
        System.out.println(r2.nextInt(10));
        //第二次随机
        System.out.println(r1.nextInt(10));
        System.out.println(r2.nextInt(10));
    }
}
