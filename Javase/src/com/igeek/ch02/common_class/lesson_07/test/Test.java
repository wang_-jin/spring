package com.igeek.ch02.common_class.lesson_07.test;
//在控制台输出1-10000,计算这段代码执行了多少毫秒?
public class Test {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            System.out.println(i+1);
        }
        long end = System.currentTimeMillis();
        long time = end-start;
        System.out.println("运行时间:"+time);
    }
}
