package com.igeek.ch02.common_class.lesson_07;
public class Tool {
    public static void main(String[] args) {
        //获取当前时间的毫秒数
        System.out.println(System.currentTimeMillis());
        //终止当前java虚拟机,参数非0表示异常终止
        System.exit(0);

    }
}
