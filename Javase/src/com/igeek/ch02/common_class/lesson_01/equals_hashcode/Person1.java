package com.igeek.ch02.common_class.lesson_01.equals_hashcode;

public class Person1 {
    private String name;
    private int age;
    public Person1(){};
    public Person1(String name, int age){
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {
        Person1 p1 = new Person1("刘备",18);
        Person1 p2 = new Person1("刘备",18);
        //不重写equals,比较的是两个对象的地址
        System.out.println(p1.equals(p2));//false
    }
}
