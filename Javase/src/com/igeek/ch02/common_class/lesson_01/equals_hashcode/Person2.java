package com.igeek.ch02.common_class.lesson_01.equals_hashcode;

import java.util.Objects;

public class Person2 {
    private String name;
    private int age;
    public Person2(){};
    public Person2(String name, int age){
        this.name = name;
        this.age = age;
    }
    /*
    自动重写快捷键:
      alt+ins,在弹出的框里面选择重写equals()和hashCode()
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person2 person = (Person2) o;
        return age == person.age && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    public static void main(String[] args) {
        Person2 p1 = new Person2("刘备",18);
        Person2 p2 = new Person2("刘备",18);
        //重写equals比较的是两个对象的属性
        System.out.println(p1.equals(p2));//true
    }

}
