package com.igeek.ch02.common_class.lesson_01.clone;
public class Person implements Cloneable {
    public String name;
    public int age;
    public Phone phone;
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
    public Person(String name,int age,Phone phone){
        this.name = name;
        this.age = age;
        this.phone = phone;
    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        return (Person)super.clone();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", phone=" + phone +
                '}';
    }
}