package com.igeek.ch02.common_class.lesson_01.clone;

public class Phone {
    String name;
    public Phone(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                '}';
    }
}
