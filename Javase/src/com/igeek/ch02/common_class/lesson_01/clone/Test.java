package com.igeek.ch02.common_class.lesson_01.clone;

public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {

        //浅拷贝 修改拷贝的新对象属性,原对象属性也会发生变化;如果拷贝的对象属性存在引用类型,那么就是浅拷贝
        Phone phone = new Phone("华为");
        Person p1 = new Person("刘备",18,phone);
        Person p2 = p1.clone();
        p2.phone.name= "苹果";
        System.out.println("p1="+p1);
        System.out.println("p2="+p2);

        //深拷贝 深克隆:修改拷贝的新对象属性,原对象不会发生变化; 如果拷贝的对象属性都是基本类型,那么就是深拷贝;
/*        Phone phone = new Phone("华为");
        Person p1 = new Person("刘备",18,phone);
        Person p2 = p1.clone();
        p2.age = 99;
        System.out.println("p1="+p1);
        System.out.println("p2="+p2);*/
    }
}
