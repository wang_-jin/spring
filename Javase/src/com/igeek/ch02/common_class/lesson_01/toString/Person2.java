package com.igeek.ch02.common_class.lesson_01.toString;

public class Person2 {
    String name;
    int age;
    public Person2(String name, int age){
        this.name = name;
        this.age = age;
    }
    /*
    alt+ins自动重写快键键
    */
    @Override
    public String toString() {
        return "Person4{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        Person2 p = new Person2("刘备",18);
        /*
        在Person4中重写了toString方法,
        打印Person4的对象时就会调用Person4的toString,
        而不会调用Object的toString,因此会打印出所有对象的属性
        */
        System.out.println(p);
    }
}
