package com.igeek.ch02.common_class.lesson_01.toString;

public class Person1 {
    String name;
    int age;
    public Person1(String name, int age){
        this.name = name;
        this.age = age;
    }
    public static void main(String[] args) {
        Person1 p = new Person1("刘备",18);
        /*
        Person3没有重写toString方法,那么打印Person3的对象,
        会默认调用根类Object的toString方法,打印出:全类名@哈希码
        */
        System.out.println(p);
    }
}
