package com.igeek.ch02.common_class.lesson_10;

public class Tool {
    public static void main(String[] args) {
        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个数字:");
        String s = sc.next();
        //匹配一位数字
        boolean flag =  s.matches("[0-9]");
        System.out.println(flag);*/

        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入3位数字:");
        String s = sc.next();
        //匹配3位数字
        boolean flag =  s.matches("[0-9]{3}");
        System.out.println(flag);*/

        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入至少一位数字:");
        String s = sc.next();
        //匹配至少一位数字
        boolean flag =  s.matches("[0-9]+");
        System.out.println(flag);*/

        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入3-5位小写字母:");
        String s = sc.next();
        //匹配3到5位小写字母
        boolean flag =  s.matches("[a-z]{3,5}");
        System.out.println(flag);*/

        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入字母:");
        String s = sc.next();
        //匹配字母ab忽略大小写
        boolean flag =  s.matches("(?i)ab");
        System.out.println(flag);*/

        /*
        案例:让用户输入一个QQ号码，我们要验证：
	    - QQ号码必须是5--15位长度
	    - 而且必须全部是数字
	    - 而且首位不能为0
        */
        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入qq号码:");
        String s = sc.next();
        boolean flag =  s.matches("[1-9][0-9]{4,14}");
        System.out.println(flag);*/

        /*
        案例:匹配url
        */
        String url = "http://www.baidu.com/info";

        boolean flag =  url.matches("http://(\\w+[.])+\\w+/\\w+");
        System.out.println(flag);

    }
}
