package com.igeek.ch02.common_class.lesson_10;

import java.util.Arrays;

public class Tool1 {
    /*
    字符串split和replace方法
    split:字符串分割
    splice:字符串替换
    */
    public static void main(String[] args) {
        String str = "刘备#语文#优秀";
        String[] split = str.split("#");
        System.out.println(Arrays.toString(split));

        /*String str = "刘备34rrr语文54rtt优秀";
        String[] s = str.split("\\w+");
        System.out.println(Arrays.toString(s));*/

        /*String str = "刘备34rrr语文54rtt优秀";
        String s = str.replaceAll("\\w+","#");
        System.out.println(s);*/
    }
}
