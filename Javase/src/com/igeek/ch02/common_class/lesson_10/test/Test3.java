package com.igeek.ch02.common_class.lesson_10.test;
/*
3.验证一个数是不是整数或小数,需要考虑正数和负数
如:123 -123 23.35 -34.67 0.34 -0.45
*/
public class Test3 {
    public static void main(String[] args) {
        String str = "-0.45";
        boolean flag = str.matches("-?\\d+(\\.\\d+)?");
        System.out.println(flag);
    }
}
