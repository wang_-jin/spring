package com.igeek.ch02.common_class.lesson_10.test;
/*
2.验证电子邮件是否合法:
  1.只能有一个@
  2.@左边是用户名,可以是a-z,A-Z,0-9,_字符
  3.@右边是域名,域名由字母和点组成,以com结尾,
  如xx.com,xx.yy.com,xx.yy.zz.com
*/
public class Test2 {
    public static void main(String[] args) {
        String str = "dbb@qq.com";
        boolean flag =  str.matches("\\w+@([a-zA-Z]+\\.)+com");
        System.out.println(flag);
    }
}
