package com.igeek.ch02.common_class.lesson_10.test;

import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) {
        /*
        1.验证用户密码,长度在6~18 之间,只能包含英文和数字
        */
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入密码:");
        String s = sc.next();
        boolean flag =  s.matches("([a-zA-Z0-9]{6,18})");
        System.out.println(flag);
    }
}
