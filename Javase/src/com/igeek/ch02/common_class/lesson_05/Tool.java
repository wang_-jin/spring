package com.igeek.ch02.common_class.lesson_05;

public class Tool {
    public static void main(String[] args) {
        int a = -12;
        //abs获取绝对值
        System.out.println(Math.abs(a));
        double b = 3.14;
        //floor向下取整:即将小数点右边直接省略掉
        System.out.println(Math.floor(b));
        //ceil向上取整:将小数点后面省略并将整数部分加1
        System.out.println(Math.ceil(b));
        //Math.pow(a,b)获取a的b次幂
        System.out.println(Math.pow(2,3));
        //random获取随机小数[0,1)能取到0取不到1
        System.out.println(Math.random());
        //Math.min(a,b)返回两个数中较小的数
        System.out.println(Math.min(11,22));
        //Math.max(a,b)返回两个数中较大的数
        System.out.println(Math.max(34,19));
    }
}
