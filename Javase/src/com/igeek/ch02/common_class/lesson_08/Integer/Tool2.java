package com.igeek.ch02.common_class.lesson_08.Integer;

public class Tool2 {
    /*
    拆箱和装箱:
      拆箱:包装器类型转基本类型
        将Integer转为int
      装箱:基本类型转包装器类型
        将int转为Integer
    */
    public static void main(String[] args) {
        //拆箱
        Integer a = 20;
        int a1 = a.intValue();
        System.out.println(a1);

        //装箱
        int b = 12;
        Integer b1 = new Integer(b);
        System.out.println(b1);

        //自动拆箱
        Integer d = 12;
        int d1 = d;

        //自动装箱
        int e = 34;
        Integer e1 = 2;

        //自动拆装箱
        Integer c = 1;
        c = c+10;//先拆箱再装箱
        System.out.println(c);
    }
}
