package com.igeek.ch02.common_class.lesson_08.Integer;

public class Tool1 {
    /*
    String和int(Integer)之间的相互转换
    */
    public static void main(String[] args) {
        //String-->int:parseInt方法
        String str = "9527";
        int i = Integer.parseInt(str);
        System.out.println(i);

        //int-->String:字符串拼接
        int a = 1234;
        String s = a+"";
        System.out.println(s);

        //String-->Integer:valueOf方法
        String s1 = "3456";
        Integer a1 = Integer.valueOf(s1);
        System.out.println(a1);

        //Integer-->String:toString方法
        Integer i1 = 77;
        Integer i2 = 88;
        System.out.println(i1.toString());
        System.out.println(i2+"");
    }
}
