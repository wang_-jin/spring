package com.igeek.ch05.map_test.test2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author wangjin
 * 2023/8/28 11:47
 * @description TODO
 */
public class Test {
    /*2.定义一个Map字典用来存储员工工资,key是员工姓名,value是员工工资
1)添加下面员工信息到集合中:
姓名:刘备 工资:5600
姓名:关羽 工资:4900
姓名:张飞 工资:4800
姓名:赵云 工资:3000
姓名:刘禅 工资:2000
2)列出所有的员工姓名
3列出所有员工姓名及其工资
4)删除名叫"赵云"的员工信息
5)输出"关羽"的工资，并将其工资加1000元(通过取值实现)
6)将所有工资低于5000元的员工的工资上涨20%(通过取值实现)*/
    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("刘备",5600);
        map.put("关羽",4900);
        map.put("张飞",4800);
        map.put("赵云",3000);
        map.put("刘禅",2000);
        //列出所有的员工姓名
        System.out.println(map.keySet());
        //列出所有的员工工资
        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        //列出所有员工姓名及其工资
        map.forEach((key,values)->{
            System.out.println(key+":"+values);
        });
        //删除名叫"赵云"的员工信息
        map.remove("赵云");
        //输出"关羽"的工资，并将其工资加1000元(通过取值实现)
        System.out.println(map.get("关羽"));
        int salary =(int) map.get("关羽");
        map.put("关羽",salary+1000);
        System.out.println(map);
        //将所有工资低于5000元的员工的工资上涨20%(通过取值实现)
        map.forEach((key,values)->{
            int d = (int) values;
            if (d<5000){
                map.put(key,d*1.2);
            }
        });
        System.out.println(map);
    }
}
