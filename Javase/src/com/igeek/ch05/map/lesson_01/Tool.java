package com.igeek.ch05.map.lesson_01;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Tool {
    public static void main(String[] args) {
        Map map = new HashMap();
        //使用put添加一对键值对到map中
        map.put("name","刘备");
        //键具有唯一性,同名的键会被后面的键替换
        map.put("name","关羽");
        //键值允许为null
        map.put(null,null);
        map.put("age",18);
        //System.out.println(map);
        /*
        注意:
          Map不能直接使用迭代器获取值,因为Map是单独的接口,不是Collection子类,
          没有实现Iterable接口,因此不能迭代,但是Map有个values方法,可以使用Map
          的值生成一个Collection集合,我们可以使用values()返回的集合进行迭代!
        */
        Iterator iterator = map.values().iterator();
        while (iterator.hasNext()){
            //HashMap键值对是无序的,不保证按照添加顺序进行遍历
            System.out.println(iterator.next());
        }

        Map map1 = new LinkedHashMap();
        map1.put("name","刘备");
        map1.put("age",18);
        map1.put("height",180);
        Iterator iterator1 = map1.values().iterator();
        while (iterator1.hasNext()){
            //LinkedHashMap键值对是有序的
            System.out.println(iterator1.next());
        }
    }
}
