## Map介绍
Map也是存储数据的容器,和Collection集合不同之处在于,
Collection存储的是一个个的值,而Map存储的是一对对的键值对key-value,
键值对的概念可以近似理解成属性名和属性值,键key是名字,值value是值的具体内容

## Map族谱
Map接口:
  HashMap实现类(无序键值对,线程不安全,性能高)
  HashTable实现类(线程安全,性能低)
  LinkedHashMap实现类(有序键值对)
  TreeMap实现类(有序键值对,可以用来实现排序)

## Map特点
1.键唯一性:Map中的键是唯一的,每个键只能对应一个值.
如果试图向Map中添加一个已经存在的键,则会覆盖该键对应的值
2.可以包含null值:Map中的键和值都可以为null,但需要注意的是,
当使用null作为键时,只能添加一个null值,否则会抛出异常
3.无序性:Map中的键值对是无序的,即不保证按照添加顺序进行遍历.
如果需要按照特定顺序进行遍历,则需要使用特定的数据结构,如TreeMap

## HashMap,LinkedHashMap,TreeMap的区别?
HashMap.LinkedHashMap和TreeMap都是Java中实现Map接口的类,
用于存储键值对.它们的区别在于它们如何存储和排序元素.
HashMap:
HashMap是一个无序的键值对集合,它使用哈希表来实现元素的存储和查找.
它允许使用null作为键或值,但是由于哈希表的特性,存储元素的顺序是不确定的.
HashMap的查找,插入和删除操作都是常数级别的时间复杂度O(1),因此它是一种高效的数据结构.
LinkedHashMap:
LinkedHashMap是一个有序的键值对集合,它使用哈希表和双向链表来实现元素的存储和查找.
它维护了一个双向链表,记录了元素插入的顺序或访问的顺序.LinkedHashMap允许使用
null作为键或值,它的性能与HashMap类似,
但是它可以保证元素的迭代顺序与插入或访问的顺序一致.
TreeMap：
TreeMap是一个有序的键值对集合,它使用红黑树来实现元素的存储和查找.
它根据键的自然顺序或自定义排序规则来对元素进行排序.TreeMap不允许使用null作为键,
但是允许使用null作为值.TreeMap的查找,插入和删除操作的时间复杂度都是o(log n),
因此它比HashMap和LinkedHashMap的性能略低,但是它提供了有序的键值对集合.