## Map常用方法
1.增加:
添加键值对
put(Object key,Object value)

2.删除:
删除对应key的值
remove(Object key)


3.修改:
修改对应key的值
put(Object key,Object value)

4.查找:
获取对应key的值
get(Object key)

获取所有key组成的数组
keySet

判断key是否存在
containskey(Object key)
