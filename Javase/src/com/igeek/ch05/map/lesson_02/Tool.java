package com.igeek.ch05.map.lesson_02;

import java.util.HashMap;

public class Tool {
    public static void main(String[] args) {
        HashMap hashMap = new HashMap();
        //添加键值对
        hashMap.put("name","刘备");
        hashMap.put("age",18);
        hashMap.put("score",88);
        hashMap.put("like","游泳");
        System.out.println(hashMap);
        //删除键值对
        hashMap.remove("like");
        System.out.println(hashMap);
        //修改某个键对应的值
        hashMap.put("name","关羽");
        System.out.println(hashMap);
        //查找name对应的值
        System.out.println(hashMap.get("name"));
        //获取所有key组成的数组
        System.out.println(hashMap.keySet());
        //判断key是否存在
        System.out.println(hashMap.containsKey("name"));
    }
}
