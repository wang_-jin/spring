package com.igeek.ch05.map.lesson_03;

public class Dog {
    String name;
    int age;
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    public Dog(){};
    public Dog(String name, int age){
        this.name = name;
        this.age = age;
    }
}
