package com.igeek.ch05.map.lesson_03.Test;

import java.util.HashMap;

/*
2.定义一个Map字典用来存储员工工资,key是员工姓名,value是员工工资
1)添加下面员工信息到集合中:
姓名:刘备 工资:5600
姓名:关羽 工资:4900
姓名:张飞 工资:4800
姓名:赵云 工资:3000
姓名:刘禅 工资:2000
2)列出所有的员工姓名
3)列出所有员工姓名及其工资
4)删除名叫"赵云"的员工信息
5)输出"关羽"的工资，并将其工资加1000元
6)将所有工资低于5000元的员工的工资上涨20%
*/
public class Test2 {
    public static void main(String[] args) {
        HashMap map = new HashMap();
        //添加员工信息
        map.put("刘备",5600);
        map.put("关羽",4900);
        map.put("张飞",4800);
        map.put("赵云",3000);
        map.put("刘禅",2000);
        //列出所有员工姓名
        System.out.println(map.keySet());
        //列出所有员工姓名及其工资
        map.forEach((key,value)->{
            System.out.println(key+":"+value);
        });
        //删除赵云
        map.remove("赵云");
        System.out.println(map);
        //输出"关羽"的工资，并将其工资加1000元
        Integer salary =  (Integer)map.get("关羽");
        System.out.println("关羽工资:"+salary);
        map.put("关羽",salary+1000);
        System.out.println(map);
        //将所有工资低于5000元的员工的工资上涨20%
        map.forEach((key,value)->{
            int v = (int)value;
            if(v<5000){
                map.put(key,v*1.2);
            }
        });
        System.out.println(map);

    }
}
