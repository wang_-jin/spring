package com.igeek.ch05.map.lesson_03;

import java.util.TreeMap;

public class Tool {
    public static void main(String[] args) {
        /*
        自然排序:
          根据key自动升序排序
        */
        TreeMap treeMap = new TreeMap();
        treeMap.put(2,"A");
        treeMap.put(3,"B");
        treeMap.put(1,"C");
        treeMap.put(55,"D");
        treeMap.put(23,"E");
        System.out.println(treeMap);
    }
}
