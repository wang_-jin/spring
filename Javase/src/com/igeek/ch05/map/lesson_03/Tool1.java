package com.igeek.ch05.map.lesson_03;
import java.util.Comparator;
import java.util.TreeMap;

public class Tool1 {
    public static void main(String[] args) {
        /*
        定制排序:
          给TreeMap的有参构造传一个Comparator接口的
          匿名内部类,可以定制排序
        */
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Dog &&o2 instanceof Dog){
                    return ((Dog)o1).age-((Dog)o2).age;
                }
                return 0;
            }
        };
        TreeMap treeMap = new TreeMap(comparator);
        treeMap.put(new Dog("小白",18),1);
        treeMap.put(new Dog("小红",88),2);
        treeMap.put(new Dog("小黑",12),3);
        treeMap.put(new Dog("小黄",56),4);
        treeMap.put(new Dog("小花",4),5);
        System.out.println(treeMap);
    }
}
