package com.igeek.ch08.file.lesson_02;

import java.io.File;
import java.util.Arrays;

public class Tool {
    public static void main(String[] args) {
        File file = new File("D:\\filetest");
        /*过滤出符合条件的文件名*/
        String[] list = file.list((dir,name)->{
            /*
            dir:指定的目录
            name:指定的目录下,所有文件名
            */
            if(name.endsWith(".txt")){
                return true;
            }
            return false;
        });
        //System.out.println(Arrays.toString(list));

        //过滤出符合条件的文件
        File[] files = file.listFiles(filename -> {
            if (filename.isDirectory()) {
                return true;
            }
            return false;
        });
        System.out.println(Arrays.toString(files));
    }
}
