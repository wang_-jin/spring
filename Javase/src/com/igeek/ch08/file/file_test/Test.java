package com.igeek.ch08.file.file_test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangjin
 * 2023/8/31 20:23
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        /*绝对路径:D:\\temp
          相对路径:当前项目下  举例 1.txt*/
        //建文件
        File f = new File("D:\\1.txt");
        if (!f.exists()){
            try {
                f.createNewFile();
                System.out.println("文件创建成功!");
            } catch (IOException e) {
                System.out.println(e);
            }
        }else {
            System.out.println("文件已存在!");
        }
        System.out.println("-----------------");
        //建目录文件
        File f1 = new File("D:\\temp");
        if (!f1.exists()){
            f1.mkdirs();
            System.out.println("文件夹创建成功!");
        }else {
            System.out.println("文件夹已存在!");
        }
        System.out.println("--------------");

        //判断当前对象是否是一个文件或目录
        System.out.println("f:"+f.isFile());
        System.out.println("f1:"+f.isDirectory());//false
        //获取绝对路径
        System.out.println("f1:"+f1.getAbsolutePath());//D:/temp
        //获取相对路径
        System.out.println("f:"+f.getPath());//1.txt
        //获取文件名
        System.out.println(f1.getName());

        //获取文件长度
        System.out.println(f1.length());


        //获取文件最后修改时间
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date now = new Date(f.lastModified());
        String s = df.format(now);
        System.out.println(s);
    }
}
