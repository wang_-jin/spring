package com.igeek.ch08.file.file_test;

import java.io.File;
import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/31 20:56
 * @description TODO
 */
public class Test2 {
    public static void main(String[] args) {
        File file = new File("D:\\");
        /*过滤出符合条件的文件名*/
        String[] list = file.list(((dir, name) -> {
            if (name.endsWith(".txt")){
                return true;
            }
            return false;
        }));
        for (String s : list) {
            System.out.println(s);
        }

        //过滤出符合条件的文件(目录)
        File[] files = file.listFiles(pathname ->{
            if (pathname.isDirectory()){return true;}
            return false;
        });
        System.out.println(Arrays.toString(files));
    }
}
