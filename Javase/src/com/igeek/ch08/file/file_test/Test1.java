package com.igeek.ch08.file.file_test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/8/31 20:47
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        File file = new File("D:\\");
        //获取目录下文件以及文件夹的名称
        String[] list = file.list();
        Arrays.stream(list).forEach(System.out::println);

        //获取目录下文件以及文件夹的对象
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(System.out::println);
    }
}
