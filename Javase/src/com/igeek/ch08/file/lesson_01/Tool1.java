package com.igeek.ch08.file.lesson_01;

import java.io.File;
import java.util.stream.Stream;

public class Tool1 {
    public static void main(String[] args) {

        File file = new File("D:\\");
        //获取目录下文件以及文件夹的名称
        /*String[] list = file.list();
        Stream.of(list).forEach(System.out::println)*/;

        //获取目录下文件以及文件夹的对象
        File[] files = file.listFiles();
        Stream.of(files).forEach(System.out::println);
    }
}
