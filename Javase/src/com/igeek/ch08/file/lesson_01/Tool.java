package com.igeek.ch08.file.lesson_01;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
绝对路径: 在电脑盘符下 D:\temp
相对路径: 在当前项目下
*/
public class Tool {
    public static void main(String[] args) {
        //定义文件对象
        File file1 = new File("a.txt");
        //如果文件不存在
        if(!file1.exists()){
            try {
                //创建文件
                file1.createNewFile();
                System.out.println("创建成功");
            }catch (IOException err){
                System.out.println(err);
            }
        }else{
            System.out.println("文件已经存在");
        }

        //定义目录对象
        File file2 = new File("D:\\temp");
        if(!file2.exists()){
            //创建目录
            file2.mkdirs();
            System.out.println("创建目录成功");
        }else{
            System.out.println("目录已经存在");
        }

        //判断当前对象是否是一个文件或是一个目录
        System.out.println("file1是否是一个文件:"+file1.isFile());
        System.out.println("file2是否是一个目录:"+file2.isDirectory());

        //获取绝对路径
        System.out.println("file1的绝对路径:"+file1.getAbsolutePath());
        //获取相对路径
        System.out.println("file1的相对路径:"+file1.getPath());

        //获取文件名
        System.out.println("获取file1的文件名:"+file1.getName());
        System.out.println("获取file2的文件名:"+file2.getName());

        //获取文件长度
        System.out.println("file1长度:"+file1.length());

        //获取文件最后修改时间
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = new Date(file1.lastModified());
        String str = fmt.format(time);
        System.out.println("file1最后修改时间:"+str);




    }
}
