package com.igeek.day01;

/**
 * @author wangjin
 * 2023/8/10 15:00
 * @description TODO
 * 1. 定义一个类,类名Test06
 * 2. 在类中定main方法
 * 3. 在main方法中,使用输出语句输出如下图形:
 *
 * *
 * **
 * ***
 * ****
 */
public class Test06 {
    public static void main(String[] args) {
        System.out.println(" ");
        System.out.println("*");
        System.out.println("**");
        System.out.println("***");
        System.out.println("****");
    }
}
