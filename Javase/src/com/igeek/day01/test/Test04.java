package com.igeek.day01;

/**
 * @author wangjin
 * 2023/8/10 9:09
 * @description TODO
 * 1. 定义一个类,类名Test04
 * 2. 在类中定main方法
 * 3. 在main方法中,使用输出语句输出如下图形:
 *      *
 * 	   * *
 * 	  *   *
 * 	 *     *
 * 	  *   *
 * 	   * *
 * 		*
 */
public class Test04 {
    public static void main(String[] args) {

        for (int i = 1; i<=7 ;i++){
            System.out.println(" ");
            for (int j = 1; j<=10; j++){
                if (i<5) {
                    if (j == 8 - i || j == i + 6 || j == 14 - i ) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                else{
                    if (j == i || j == 14 - i ){
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }

        }
    }
}

