package com.igeek.day01;

/**
 * @author wangjin
 * 2023/8/10 15:18
 * @description TODO
 */
public class Test07 {
    public static void main(String[] args) {
        int a=40;
        System.out.println(a);
        {
            int c = 20;
            System.out.println(c);
        }
        int c = 30;
        System.out.println(c);
    }
}



/*public class Test07_02 {
    public static void main(String[] args) {
        int x = 2;
        {
            int y = 6;
            System.out.println("x is " + x);
            System.out.println("y is " + y);
        }
        y = x;
        System.out.println("x is " + x);
    }
}*/
