package com.igeek.day01;

/**
 * @author wangjin
 * 2023/8/10 14:10
 * @description TODO
 * 1. 定义一个类,类名Test05
 * 2. 在类中定main方法
 * 3. 在main方法中,使用输出语句输出如下图形.
 * {@}
 * /|\
 *  |
 *
 * 4. 提示: “\”在java中是转移符,如果要在字符串中写一个”\”,你需要写为”\\”
 */
public class Test05 {
    public static void main(String[] args) {
        System.out.println("{@}");
        System.out.println("/|\\");
        System.out.println(" | ");
    }
}
