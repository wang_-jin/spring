package com.igeek.day01;

/**
 * @author wangjin
 * 2023/8/10 8:57
 * @description TODO
 * 1. 定义一个Test02类
 * 2. 在类中定义主方法
 * 3. 在主方法中,使用输出语句,输出如下变量
 * (1)整数变量i1:                 88,   i2:-88
 * (2)小数变量d:                  88.888
 * (3)字符变量ch1:                'A',   ch2:'8'
 * (4)布尔变量flag1:  true,        flag2:false
 */
public class Test02 {
    public static void main(String[] args) {
        int i1 = 88;
        int i2 = -88;
        float d = 88.888f;
        char ch1 = 'A';
        char ch2 = '8';
        boolean flag1 = true;
        boolean flag2 = false;
        System.out.println("i1="+i1);
        System.out.println("i2="+i2);
        System.out.println("d="+d);
        System.out.println("ch1="+ch1);
        System.out.println("ch2="+ch2);
        System.out.println("flag1="+flag1);
        System.out.println("flag2="+flag2);

    }

}
