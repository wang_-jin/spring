package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/5 20:27
 * @description TODO
 */
/*贷款额不能为负数,如果用户试图将贷款额置为负数,则会抛出这个异常*/
public class LoanException extends ATMException{
    public LoanException() {
        super();
    }

    public LoanException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "LoanException{} " + super.toString();
    }
}
