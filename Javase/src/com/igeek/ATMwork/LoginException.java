package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/5 20:27
 * @description TODO
 */
/*用户登录异常的情况,例如id错误,密码错误*/
public class LoginException extends ATMException{
    public LoginException() {
        super();
    }

    public LoginException(String message) {
        super(message);
    }
}
