package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/4 20:19
 * @description TODO
 */
/*该账户可以贷款,不可以透支*/
public class LoanSavingAccount extends SavingAccount implements Loanable{
    //贷款
    @Override
    public Account requestLoan(double money) {
        long loanAmount = (long)(this.getLoanAmount() + money);
        this.setLoanAmount(loanAmount);
        return this;
    }
    //还款
    @Override
    public Account payLoan(double money) {
        long loanAmount = (long)(this.getLoanAmount() - money);
        this.setLoanAmount(loanAmount);
        this.setBalance(this.getBalance()-money);
        return this;
    }
}
