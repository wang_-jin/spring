package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/4 10:09
 * @description TODO
 */
public abstract class Account {
    private long id;//账户号码
    private String password;//账户密码
    private String name;//真实姓名
    private String personId;//身份证号码
    private String email;//客户的电子邮箱
    private double balance;//账户余额
    private double totalBalance;
    private long loanAmount = 0;//记录贷款金额
        /*deposit:  存款方法,参数类型：double, 返回类型：Account
          withdraw:取款方法,参数类型：double, 返回类型：Account*/

    final Account deposit(double a){return null;}
    abstract Account withdraw(double b);

    public Account() {
    }

    public Account(long id, String password, String name, String personId, String email, double balance) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.personId = personId;
        this.email = email;
        this.balance = balance;
    }


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getPersonId() {
        return personId;
    }


    public void setPersonId(String personId) {
        this.personId = personId;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public double getBalance() {
        return balance;
    }


    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(long loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", personId='" + personId + '\'' +
                ", email='" + email + '\'' +
                ", balance=" + balance +
                ", loanAmount=" + loanAmount +
                '}';
    }
}
