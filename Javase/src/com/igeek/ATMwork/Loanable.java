package com.igeek.ATMwork;

public interface Loanable {
    //贷款
    Account requestLoan(double money);//贷款金额
    //还贷
    Account payLoan(double money);//还贷款金额
}
