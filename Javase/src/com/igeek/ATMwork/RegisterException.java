package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/5 20:26
 * @description TODO
 */
/*用于开户异常的情况,例如密码两次输入不一致等情况*/
public class RegisterException extends ATMException{
    public RegisterException() {
        super();
    }

    public RegisterException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "RegisterException{} " + super.toString();
    }
}
