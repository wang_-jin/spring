package com.igeek.ATMwork;
import java.util.Scanner;


/**
 * @author wangjin
 * 2023/9/4 10:09
 * @description TODO
 */
public class Test {
    public static void main(String[] args) throws LoginException, RegisterException {
        Bank bank = Bank.getInstance();
        Account account = new Account() {
            @Override
            Account withdraw(double b) {
                return null;
            }
        };
        account = null;
        //用户注册
        System.out.println("-----------------");
        boolean flag = true;
        while(flag) {
            Scanner sc = new Scanner(System.in);
            System.out.println("0.退出系统 1.账户开户 2.登录 3.存款 \n4.取款 5.设置透支额度 6.转账 \n" +
                    "7.查看账户 8.借贷 9.还贷");
            System.out.print("请输入你对应操作的数字0~9: ");
            int num = sc.nextInt();
            switch (num){
                case 0:
                    flag = false;
                    break;
                case 1:
                    System.out.println("--------1.用户开户---------");
                    System.out.print("请输入密码: ");
                    String password = sc.next();
                    System.out.print("确认密码: ");
                    String repassword = sc.next();
                    System.out.print("请输入姓名: ");
                    String name = sc.next();
                    System.out.print("请输入身份证号: ");
                    String personId = sc.next();
                    System.out.print("请输入邮箱: ");
                    String email = sc.next();
                    System.out.print("请输入你的户型(0 – 储蓄账户  1 – 信用账户 2 – 可贷款储蓄账户 3– 可贷款信用账户): ");
                    int type = sc.nextInt();
                    if (!password.equals(repassword)){
                        System.out.println("两次密码不一致!");
                        continue;
                    }
                    bank.register(password,repassword,name,personId,email,type);

                    continue;
                case 2:
                        System.out.println("--------2.用户登录---------");
                        System.out.print("请输入账户: ");
                        Long id = sc.nextLong();
                        System.out.print("请输入密码: ");
                        String password1 = sc.next();
                        account = bank.login(id, password1);

                    continue;
                case 3:
                    if (account!=null) {
                        System.out.println("--------3.用户存款---------");
                        System.out.println("欢迎"+account.getName());
                        System.out.println("请输入要存的数额: ");
                        long inMoney = sc.nextLong();
                        bank.deposit(account.getId(), inMoney);
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }
                case 4:
                    if (account!=null) {
                        System.out.println("--------4.用户取款---------");
                        System.out.println("欢迎"+account.getName());
                        System.out.println("请输入要取的数额: ");
                        long outMoney = sc.nextLong();
                        bank.withdraw(account.getId(), account.getPassword(), outMoney);
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }
                case 5:
                    if (account!=null){
                        System.out.println("--------5.用户透支额度---------");
                        System.out.println("欢迎"+account.getName());
                        System.out.println("请输入要透支的额度: ");
                        long overMoney = sc.nextLong();
                        account = bank.updateCeiling(account.getId(), account.getPassword(), overMoney);
                        if (account==null){
                            System.out.println("该用户为储蓄用户,不能透支额度!");
                        }
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }

                case 6:
                    if (account!=null){
                        System.out.println("--------6.转账---------");
                        System.out.println("欢迎"+account.getName());
                        System.out.print("请输入你要转账的用户账户: ");
                        long to = sc.nextLong();
                        System.out.print("请输入要转账的金额: ");
                        double transMoney = sc.nextDouble();
                        bank.transfer(account.getId(),account.getPassword(),to,transMoney);
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }

                case 7:
                    if (account!=null) {
                        System.out.println("--------7.查看账户余额---------");
                        System.out.println("欢迎"+account.getName());
                        System.out.println("余额" + account.getBalance()+"元");
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }
                case 8:
                    if (account!=null) {
                        System.out.println("--------8.贷款---------");
                        System.out.println("欢迎 "+account.getName());
                        System.out.print("请输入要贷款的金额: ");
                        double requestLoanMoney = sc.nextDouble();
                        account = bank.requestLoan(account.getId(), requestLoanMoney);
                        if (account==null){
                            System.out.println("该用户为非可贷款用户,不能贷款!");
                        }
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }
                case 9:
                    if (account!=null) {
                        System.out.println("--------8.还款---------");
                        System.out.println("欢迎 "+account.getName());
                        System.out.print("请输入要还的贷款金额: ");
                        double payLoanMoney = sc.nextDouble();
                        account = bank.payLoan(account.getId(), payLoanMoney);
                        if (account==null){
                            System.out.println("该用户为非可贷款用户,不能还款!");
                        }
                        continue;
                    }else {
                        System.out.println("error!!!请登录!");
                        continue;
                    }
            }
        }
        //银行所有账户余额
        System.out.println("-----------------------");
        int allAccountBalanceSum = bank.getAllAccountBalanceSum();
        System.out.println("银行所有账户余额:"+allAccountBalanceSum);
        //银行所有账户透支额度
        int allAccountCeiling = bank.getAllAccountCeiling();
        System.out.println("银行所有账户透支额度:"+allAccountCeiling);
        //银行所有账户贷款数额
        double totalLoan = bank.getTotalLoan();
        System.out.println("银行所有账户贷款数额:"+totalLoan);
        //总资产排名
        System.out.println("--------本银行总资产排名---------");
        bank.getRank();
    }
}
