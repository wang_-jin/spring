package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/4 10:21
 * @description TODO
 */
public class CreditAccount extends Account{
    private long ceiling;//透支数额

    public CreditAccount() {
    }

    public CreditAccount(long id, String password, String name, String personId, String email, double balance,long ceiling) {
        super(id, password, name, personId, email, balance);
        this.ceiling = ceiling;
    }


    @Override
    Account withdraw(double a) {
        return null;
    }

    /**
     * 获取
     * @return ceiling
     */
    public long getCeiling() {
        return ceiling;
    }

    /**
     * 设置
     * @param ceiling
     */
    public void setCeiling(long ceiling) {
        this.ceiling = ceiling;
    }


    @Override
    public String toString() {
        return "CreditAccount{" +
                "ceiling=" + ceiling +
                "} " + super.toString();
    }
}
