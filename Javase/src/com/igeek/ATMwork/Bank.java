package com.igeek.ATMwork;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author wangjin
 * 2023/9/4 10:23
 * @description TODO
 */
public class Bank {

    private static Bank instance = new Bank();
    public static Bank getInstance(){return instance;}

    //将Bank类作成单例。
    private static final String COUNTRY_CODE = "862";//设置前三位数字不变
    private static final String POSTAL_CODE = "150212";//设置中间六位数字不变
    private static final String DATE_FORMAT = "yyyyMM";//设置年月不变
    private static AtomicLong sequence = new AtomicLong(1);//设置开户序号,默认为1
    private static List<Account> accounts;//存用户
    private static int index = 0;//

    public Bank() {
        accounts = new ArrayList<>();
    }

    //创建账户
    public static void createAccount(Account account, String password, String repassword, String name, String personID, String email) throws RegisterException {

        StringBuilder sb = new StringBuilder();
        sb.append(COUNTRY_CODE);
        sb.append(POSTAL_CODE);
        sb.append(DateTimeFormatter.ofPattern(DATE_FORMAT).format(LocalDate.now()));
        sb.append(String.format("%04d", sequence.getAndIncrement()));
        long accountId = Long.parseLong(sb.toString());
        account.setId(accountId);//设置用户账号
        account.setPassword(password);
        account.setName(name);
        account.setPersonId(personID);
        account.setEmail(email);
        if (password.equals(repassword)){
            throw new RegisterException("两次密码不一致!");
        }
        accounts.add(account);// 完成用户信息存入数组
        System.out.println("开户成功!");
        System.out.println("开户账户为"+account.getId());

    }

    //1.用户开户  Long 账号, String密码, String确认密码,String 姓名,String身份证号码,String邮箱,int 账户类型
    /*项目需求规定账户类型：0 – 储蓄账户  1 – 信用账户 2 – 可贷款储蓄账户 3– 可贷款信用账户*/
    public Account register( String password, String repassword, String name, String personID, String email, int type) throws RegisterException {
        //设置开户类型
        switch (type){
            case 0:
                SavingAccount sa = new SavingAccount();
                //创建储蓄账户
                createAccount(sa,password,repassword,name,personID,email);
                return sa;
            case 1:
                CreditAccount ca = new CreditAccount();
                //创建信用账户
                createAccount(ca,password,repassword,name,personID,email);
                return ca;
            case 2:
                SavingAccount saa = new LoanSavingAccount();//多态创建实例
                //创建可贷款储蓄账户
                createAccount(saa,password,repassword,name,personID,email);
                return saa;
            case 3:
                CreditAccount caa = new LoanCreditAccount();//多态创建实例
                //创建信用账户
                createAccount(caa,password,repassword,name,personID,email);
                return caa;
            default:
                System.out.println("开户失败");
                System.exit(0);
        }

        return null;
    }
    //2.用户登录 Long 账号, String密码
    public Account login(Long id, String password) throws LoginException {
        if (accounts!=null) {
            for (Account account : accounts) {
                if (account.getId() == id && account.getPassword().equals(password)) {
                    System.out.println("登录成功!");
                    return account;
                } else if (account.getId() != id) {
                    throw new LoginException("id错误");
                } else if (!account.getPassword().equals(password)) {
                    throw new LoginException("密码错误");
                }
            }
        }
        return null;
    }
    //3.用户存款  Long 账号, double存款金额
    public Account deposit(Long id, double money){
        for (Account account : accounts) {
            if (account.getId()==id){
                account.setBalance(money);
                return account;
            }
        }
        return null;
    }
    //4.用户取款  Long 账号,String 密码，double取款金额
    public Account withdraw(Long id, String password, double money){
        for (Account account : accounts) {
            if (account.getId()==id&&account.getPassword().equals(password)&&account instanceof CreditAccount){
                account.setBalance(account.getBalance()-money);
                return account;
            }else if (account.getId()==id&&account.getPassword().equals(password)&&account instanceof SavingAccount){
                account.setBalance(account.getBalance()-money);
                if (account.getBalance()<0){
                    System.out.println("非信用用户,不能透支!");
                     account.setBalance(0);//储蓄用户透支后余额设置为0
                }
                return account;
            }
        }
        return null;
    }
    //5.设置透支额度 Long 账号, String 密码，double透支额度金额
    /*提示：这个方法需要验证账户是否是信用账户*/
    public Account updateCeiling(Long id, String password, double money){
        for (Account account : accounts) {
            if (account.getId()==id&&account.getPassword().equals(password)&&account instanceof CreditAccount){
                CreditAccount c = (CreditAccount)account;
                c.setCeiling((long) money);
                return account;
            }
        }
        return null;
    }
    //6.转账功能 from转出账户，passwordFrom 转出账号的密码，to转入账户，money转账的金额
     /*不能透支*/
    public boolean transfer(Long from, String passwordFrom, Long to, double money){
        for (Account account : accounts) {
            if (account.getId()==from&&account.getPassword().equals(passwordFrom)){
                account.setBalance(account.getBalance()-money);
                for (Account account1 : accounts) {
                    if (to.equals(account1.getId())){
                        account1.setBalance(account1.getBalance()+money);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    /*统计银行所有账户余额总数*/
    int getAllAccountBalanceSum(){
        int sumBalance = 0;
        for (Account account : accounts) {
                sumBalance+=account.getBalance();
        }
        return sumBalance;
    }
    /*统计所有信用账户透支额度总数*/
    int getAllAccountCeiling(){
        int sumCeiling = 0;
        for (Account account : accounts) {
            if (account instanceof CreditAccount){
                sumCeiling+=((CreditAccount)account).getCeiling();
            }
        }
        return sumCeiling;

    }

    /*贷  款  参数：id 账户，money贷款金额*/
    Account requestLoan(Long id , double money){
        for (Account account : accounts) {
            if ((account instanceof LoanCreditAccount)&&account.getId()==id){
                ((LoanCreditAccount) account).requestLoan(money);
                return account;
            }else if (account instanceof LoanSavingAccount&&account.getId()==id){
                ((LoanSavingAccount) account).requestLoan(money);
                return account;
            }
        }
        return null;
    }

    /*还贷款  id 账户，money还贷款金额*/
    Account payLoan(Long id , double money){
        for (Account account : accounts) {
            if ((account instanceof LoanCreditAccount)&&account.getId()==id){
                ((LoanCreditAccount) account).payLoan(money);
                return account;
            }
            if (account instanceof LoanSavingAccount&&account.getId()==id){
                if (money<account.getBalance()){
                    ((LoanSavingAccount) account).payLoan(money);
                    return account;
                }
                //若可贷款储蓄用户还款金额超过余额则清空余额进行还贷
                else if (money>=account.getBalance()){
                    ((LoanSavingAccount) account).payLoan(account.getBalance());
                    return account;
                }

            }
        }
        return null;
    }

    /*统计所有账户贷款的总额*/
    double getTotalLoan(){
        double allRequestLoan = 0;
        for (Account account : accounts) {
            if ((account instanceof LoanCreditAccount||account instanceof LoanSavingAccount)){
                allRequestLoan+=account.getLoanAmount();
            }
        }
        return allRequestLoan;
    }
    /*打印所有用户的总资产排名*/
    void getRank(){
        Map<String,Double>map = new HashMap<>();
        getAccountTotalBalance();
        for (Account account : accounts) {
            map.put(account.getName(),account.getTotalBalance());
        }
        List<Map.Entry<String,Double>>list = new ArrayList<>(map.entrySet());
        Collections.sort(list,Comparator.comparing(Map.Entry::getValue));
        map.forEach((key,values)->{
            System.out.println("用户名:"+key+"  "+"资产:"+values);
        });
    }
    //若personId相同则将两余额相加放totalBalance
    static void getAccountTotalBalance(){
        Map<String,Double>map = new HashMap<>();
        for (Account account : accounts) {
                double values = map.getOrDefault(account.getPersonId(), 0.0)+account.getBalance();
                map.put(account.getPersonId(), values);
                double totalBalance = map.get(account.getPersonId());
                account.setTotalBalance(totalBalance);
        }
    }

    public static AtomicLong getSequence() {
        return sequence;
    }

    public static void setSequence(AtomicLong sequence) {
        Bank.sequence = sequence;
    }

    public static List<Account> getAccounts() {
        return accounts;
    }

    public static void setAccounts(List<Account> accounts) {
        Bank.accounts = accounts;
    }

    public static int getIndex() {
        return index;
    }

    public static void setIndex(int index) {
        Bank.index = index;
    }


    public String toString() {
        return "Bank{COUNTRY_CODE = " + COUNTRY_CODE + ", POSTAL_CODE = " + POSTAL_CODE + ", DATE_FORMAT = " + DATE_FORMAT + ", sequence = " + sequence + ", accounts = " + accounts + ", index = " + index + "}";
    }
}
