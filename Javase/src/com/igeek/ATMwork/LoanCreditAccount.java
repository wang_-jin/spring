package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/4 20:19
 * @description TODO
 */
/*该账户可以贷款,可以透支*/
public class LoanCreditAccount extends CreditAccount implements Loanable{
    //贷款
    @Override
    public Account requestLoan(double money) {
        long loanAmount = (long)(this.getLoanAmount() + money);
        this.setLoanAmount(loanAmount);
        return this;
    }
    //还款
    @Override
    public Account payLoan(double money) {
        long loanAmount = (long)(this.getLoanAmount() - money);
        this.setLoanAmount(loanAmount);
        this.setBalance(this.getBalance()-money);
        return this;
    }
}
