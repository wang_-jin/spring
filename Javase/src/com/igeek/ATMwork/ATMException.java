package com.igeek.ATMwork;

/**
 * @author wangjin
 * 2023/9/5 20:25
 * @description TODO
 */
/*ATM业务异常基类*/
public class ATMException extends Exception{
    public ATMException() {
        super();
    }
    public ATMException(String message) {
        super(message);
    }
}
