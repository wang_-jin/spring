package com.igeek.day05;

/**
 * @author wangjin
 * 2023/8/15 10:04
 * @description TODO
 */
public class Method1 {
       /*
    练习1:比较数据是否相等.参数类型分别为两个byte类型,
    两个short类型,两个int类型,两个long类型,并在main方法中进行测试
    */
    boolean isEquals(byte a,byte b){
        return a==b?true:false;
    }
    boolean isEquals(short a,short b){
        return a==b?true:false;
    }
    boolean isEquals(int a,int b){
        return a==b?true:false;
    }
    boolean isEquals(long a,long b){
        return a==b?true:false;
    }

    public static void main(String[] args) {
        Method1 m = new Method1();
        boolean f = m.isEquals(3,4);
        System.out.println("是否相等:"+(f?"相等":"不相等"));
        System.out.println("");
    }
}
