package com.igeek.day05;

/**
 * @author wangjin
 * 2023/8/14 8:56
 * @description TODO
 * 主方法中给定数组int[] arr= {10,20,30,40,50,60};这个数组没有重复元素.
 * 定义一个方法可以接受这个给定的数组并返回这个数组中最大元素值的索引值
 */
public class Test06 {
    int getMaxIndex(int[] arr){
        int max = 0;
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]>max){
                max=arr[i];
                index = i;
            }
        }
        return index;
    }
    public static void main(String[] args) {
        int[] arr= {10,20,80,40,50,30};
        Test06 t =new Test06();
        int i = t.getMaxIndex(arr);
        System.out.println("最大值的索引值:"+i);
    }
}
