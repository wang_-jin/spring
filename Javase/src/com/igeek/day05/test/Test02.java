package com.igeek.day05;

/**
 * @author wangjin
 * 2023/8/14 8:54
 * @description TODO
 * 随机产生两个整数,随机数的范围均是[1,100],定义方法求这两个整数的和并打印和值
 * 1.创建一个测试类
 * 2.在主方法中使用Random产生两个范围是[1,100]的随机数
 * 3.定义一个求两个整数和的方法
 * 4.在主方法中调用这个求和方法得到结果值,并打印
 */
public class Test02 {
    static int getSum(int a,int b){
        return a+b;
    }

    public static void main(String[] args) {
        int a = (int)(Math.random()*100+1);
        int b = (int)(Math.random()*100+1);
        System.out.println("两个随机数分别为\n"+a+","+b);
        int sum = Test02.getSum(a, b);
        System.out.println("sum="+sum);
    }
}
