package com.igeek.day05;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/14 8:55
 * @description TODO
 * 有定义一个方法,可以接受一个整数,
 * 如果为[90,100] 这个范围方法返回’A’
 * 如果为[80,90)这个范围方法返回’B’
 * 如果为[70,80)这个范围方法返回’C’
 * 如果为[60,70)这个范围方法返回’D’
 * 如果为[0,60)这个范围方法返回’E’
 * 整数不在以上范围的返回’F’
 * .创建一个测试类
 * 2.定义个方法根据传递进来的数字返回对应的字母
 * 3.在主方法中使用键盘录入一个整数,调用这个方法得到这个整数对应的字母并打印
 */
public class Test05 {
    char level(int a){
        switch (a/10){
            case 10:
            case 9:
                return 'A';
            case 8:
                return 'B';
            case 7:
                return 'C';
            case 6:
                return 'D';
            case 5:
            case 3:
            case 2:
            case 1:
            case 0:
                return 'E';
            default:
                return 'F';
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        int a = sc.nextInt();
        Test05 t = new Test05();
        char l = t.level(a);
        System.out.println("等级为:"+l);
    }
}
