package com.igeek.day05;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/14 8:56
 * @description TODO
 * 分析以下需求，并用代码实现
 * 	1.键盘录入长方形的长和宽
 * 		定义方法计算该长方形的周长,并在main方法中打印周长
 * 	2.键盘录入长方形的长和宽
 * 		定义方法计算该长方形的面积,并在main方法中打印面积
 * 	3.键盘录入圆的半径
 * 		定义方法计算该圆的周长,并在main方法中打印周长
 * 	4.键盘录入圆的半径
 * 		定义方法计算该圆的面积,并在main方法中打印面积
 */
public class Test07 {
    int rectLength(int length, int width){
        return (length+width)*2;
    }
    int rectArea(int length, int width){
        return length*width;
    }
    double cirLength(int r){
        return 2*3.14*r;
    }
    double cirArea(int r){
        return 3.14*r*r;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入长方形的长:");
        int a = sc.nextInt();
        System.out.println("请输入长方形的宽:");
        int b = sc.nextInt();
        System.out.println("请输入圆的半径:");
        int r = sc.nextInt();
        Test07 t = new Test07();
        int l1 = t.rectLength(a,b);
        System.out.println("长方形的周长为:"+l1);
        int s1 = t.rectArea(a,b);
        System.out.println("长方形的面积为:"+s1);
        double l2 = t.cirLength(r);
        System.out.println("圆的周长为:"+l2);
        double s2 = t.cirArea(r);
        System.out.println("圆的面积为:"+s2);


    }
}
