package com.igeek.day05;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/14 8:53
 * @description TODO
 * 定义一个方法 能接受一个整数(这个整数大于3)打印0到这个整数(包含)之间的所有的偶数
 * 如 接受的数字是6则调用完方法打印出来的偶数是 0 2 4 6
 * 如 接受的数字是 5则调用完方法打印出来的偶数是 0 2 4
 * 1.创建一个测试类,在测试类中创建上述方法
 * 2.定义一个无返回值,有参数的方法
 * 3.在方法内部把符合条件的数字打印
 * 4.在主方法中调用这个方法,并传入数字15进行测试
 */
public class Test01 {
    static void printEven(int a){
        if (a>3){
            for (int i = 0; i <= a; i++) {
                if (i%2==0){
                    System.out.print(i+" ");
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        int a = sc.nextInt();
        Test01.printEven(a);
    }
}
