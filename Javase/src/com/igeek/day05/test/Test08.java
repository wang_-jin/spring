package com.igeek.day05;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/14 8:56
 * @description TODO
 * 分析以下需求，并用代码实现
 * 	1.定义一个方法equals(int[] arr1,int[] arr2),
 * 	功能:比较两个数组是否相等(长度和内容均相等则认为两个数组是相同的)
 * 	2.定义一个方法fill(int[] arr,int value),功能:将数组arr中的所有元素的值改为value
 * 	3.定义一个方法fill(int[] arr,int fromIndex,int toIndex,int value),
 * 	功能:将数组arr中的元素从索引fromIndex开始到toIndex(不包含toIndex)对应的值改为value
 * 	4.定义一个方法copyOf(int[] arr, int newLength),
 * 	功能:将数组arr中的newLength个元素拷贝到新数组中,并将新数组返回,从索引为0开始
 * 	5.定义一个方法copyOfRange(int[] arr,int from, int to),
 * 	功能:将数组arr中从索引from(包含from)开始到索引to结束(不包含to)的元素复制到新数组中,并将新数组返回
 */
public class Test08 {
    //比较两个数组是否相等(长度和内容均相等则认为两个数组是相同的)
    boolean arrayEquals(int[] arr1,int[] arr2){
        if (Arrays.equals(arr1,arr2) &&arr1.length==arr2.length){
            return true;
        }else {
            return false;
        }
    }
    //将数组arr中的所有元素的值改为value
    int[] fill1(int[] arr,int value){
        Arrays.fill(arr,value);
        return arr;
    }
    //将数组arr中的元素从索引fromIndex开始到toIndex(不包含toIndex)对应的值改为value
    int[] fill2(int[] arr,int fromIndex,int toIndex,int value){
        Arrays.fill(arr,fromIndex,toIndex,value);
        return arr;
    }
    //将数组arr中的newLength个元素拷贝到新数组中,并将新数组返回,从索引为0开始
    int[] copyOf(int[] arr, int newLength){
        return Arrays.copyOf(arr, newLength);
    }
    //将数组arr中从索引from(包含from)开始到索引to结束(不包含to)的元素复制到新数组中,并将新数组返回
    int[] copyOfRange(int[] arr,int from, int to){
        return Arrays.copyOfRange(arr,from,to);
    }

    public static void main(String[] args) {
        int[] arr1 = {2,3,4,5};
        int[] arr2 = {2,3,4,5};
        Test08 t = new Test08();
        boolean f = t.arrayEquals(arr1,arr2);
        System.out.println("两数组情况为:"+(f?"相同":"不相同"));
        System.out.println("arr1=");
        System.out.println(Arrays.toString(t.fill1(arr1,2)));
        System.out.println("arr2=");
        System.out.println(Arrays.toString(t.fill2(arr2,1,3,2)));
        int[] arr3 = t.copyOf(arr2,4);
        System.out.println("arr3=");
        System.out.println(Arrays.toString(arr3));
        int[] arr4 = t.copyOfRange(arr2,2,4);
        System.out.println("arr4=");
        System.out.println(Arrays.toString(arr4));
    }
}
