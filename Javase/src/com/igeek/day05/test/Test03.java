package com.igeek.day05;

/**
 * @author wangjin
 * 2023/8/14 8:55
 * @description TODO
 * 有如下数组int[] arr={10,20};
 * 定义一个方法求两个数字的和(方法参数为两个int类型),使用这个方法求出数组这两个元素相加的结果并打印
 * 1.创建一个测试类
 * 2.定义个方法可以求两个整数的和
 * 3.Arr数组写在主方法中,在主方法中调用求和方法求出两个元素的和并打印
 */
public class Test03 {
    void printSum(int a,int b){
        int sum = 0;
        sum = a+b;
        System.out.println("两数和为:"+sum);
    }

    public static void main(String[] args) {
        int[] arr = {10,20};
        Test03 t  =new Test03();
        t.printSum(arr[0],arr[1]);
        }
    }

