package com.igeek.day05;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/14 8:58
 * @description TODO
 * 1.键盘录入一个整数(正数或者负数都可以,但是符号位不算有效的数字位)
 * 	2.定义一个方法,该方法的功能是计算该数字是几位数字,并将位数返回
 * 	3.在main方法中打印该数字是几位数
 * 	4.演示格式如下:
 * 		(1)演示一:
 * 			请输入一个整数:1234
 * 			控制台输出:1234是4位数字
 * 		(2)演示二:
 * 			请输入一个整数:-34567
 * 			控制台输出:-34567是5位数字
 */
public class Test10 {
    int place(int a){
        int sum = 0;
        while(a!=0){
            a=a/10;
            sum++;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个整数:");
        int a = sc.nextInt();
        Test10 t = new Test10();
        int sum = t.place(a);
        System.out.println("控制台输出:"+a+"是"+sum+"位数字");
    }
}

