package com.igeek.day05;

/**
 * @author wangjin
 * 2023/8/14 8:57
 * @description TODO
 * 定义一个方法,去打印指定两个年份之间所有的闰年年份
 * 如:将2000和2005传入方法,方法执行完毕会打印出这段之间的所有闰年年份
 * 2000和2004
 * 提示:
 * 计算公历年闰年的算法: 四年一闰，百年不闰，四百年再闰
 * 翻译:
 * 满足如下任意一条即使如年
 * 1)年份是整百数的必须是400的倍数才是闰年
 * 2)其他年份能被4的是闰年
 * 实例: 2000 是整百数,并且是400的倍数所以是闰年; 2004年是4的倍数是闰年
 * 2100 是整百的倍数但不是400的倍数,所以不是闰年
 */
public class Test09 {
    boolean isRuinian(int a,int b){
        boolean flag = false;
        for (int i = a ; i <= b; i++) {
            if (i%100==0&&i%400==0||i%100!=0&&i%4==0){
                System.out.println(i);
                flag = true;
            }
        }
        return flag;
    }
    public static void main(String[] args) {
        Test09 t = new Test09();
        boolean f = t.isRuinian(2090,2100);
        System.out.println(f?"有闰年":"无闰年");
    }
}
