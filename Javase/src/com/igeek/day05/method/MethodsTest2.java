package com.igeek.day05.method;

public class MethodsTest2 {
    /*
    定义方法的几种形式:
    1.无返回值,无形式参数
    2.无返回值,有形式参数
    3.有返回值,无形式参数
    4.有返回值,有形式参数
    5.静态方法

    形参和实参的区别:
      形参就像是一个盒子,用来装方法调用时传递进来的参数类型
      和数量的信息,但是盒子里面并没有实际的值,
      实参则是真正的参数值,就像是盒子里面装的实际物品,
      在方法调用时,这些实参会被传递给形参,让方法能够使用这些参数进行操作.

      举个例子,假设有一个方法:public int add(int a,int b){return a+b},
      它接收两个整数作为参数,并返回它们的和.那么,在方法声明中,
      这两个整数就是形参,而在方法调用时,传递给add方法的两个具体的
      整数就是实参.例如,add(2,3)中的2和3就是实参,
      而方法声明中的int a和int b就是形参.

    关键字return的作用:
      1.将return右边的数据返回给方法外面使用
      2.return会终止函数的执行,return下面的
      代码永远不会执行了

    静态方法:
      定义方法时在public右边添加一个static关键字,
      定义的就是静态方法
    如何执行静态方法:
      类名.方法名()或方法名()
      执行静态方法使用类名.方法名()去调用,类名也可以省略
      直接用方法名()调用,java会自动在方法名前面加上当前
      所在的类名调用静态方法!
    注意:
      1.静态方法和实例方法一样也有参数和返回值,
      只不过定义和调用的方式不一样
      2.静态方法里面不能直接通过方法名调用实例方法,
      但实例方法中可以直接通过方法名调用静态方法
    */
    //1.无返回值,无形式参数
    public void foo1(){
        System.out.println("无返回值,无形式参数");
    }
    //2.无返回值,有形式参数
    public void foo2(int a,int b){
        System.out.println("a+b="+(a+b));
    }
    // 3.有返回值,无形式参数
    public int foo3(){
        return 520;
    }
    //4.有返回值,有形式参数
    public int foo4(int a,int b){
        return a+b;
    }
    //5.静态方法
    public static void foo5(){
        System.out.println("静态方法");
    }
    public static void main(String[] args) {
        MethodsTest2 meth = new MethodsTest2();
        meth.foo1();
        meth.foo2(2,3);
        //定义一个变量a接受foo3的返回值
        int a = meth.foo3();
        int sum = meth.foo4(11,22);
        //使用类名调用静态方法
        MethodsTest2.foo5();
    }
}
