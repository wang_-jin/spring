package com.igeek.day05.method;

public class MethodsTest1 {
    /*
    方法是什么:
      java中的方法是一段被命名的代码块,用于执行某个特定的功能.
      在多个地方使用相同的代码块时,可以将其封装到一个方法中并在
      需要时调用.这样可以减少重复的代码,提高代码的可读性和可维护性.

    方法书写的位置:
      方法要定义在类中,即class关键字所在的花括号中,
      不能定义在其他方法中!

    如何定义方法:
      修饰符 返回值类型 方法名(参数类型 参数1,参数类型 参数2...){
        return 返回值
      }

     如何执行方法体中的代码:
       调用实例方法: 实例对象.方法名(实参1,实参2...);
       方法其实分为实例方法和静态方法,我们说的方法一般默认
       是指实例方法,实例方法需要使用类的实例对象来调用执行,
       静态方法使用类名调用执行
    */
    public void cook(){
        System.out.println("买菜");
        System.out.println("洗菜");
        System.out.println("切菜");
        System.out.println("炒菜");
    }
    public static void main(String[] args) {
        /*
        下面做菜的例子我们发现,做菜的4行代码块被重复使用,
        代码显得很臃肿,如果我们可以给做菜的代码块起个名字,
        通过名字直接调用这个代码块,代码就会减少很多,也会
        显得很清晰,而给代码块起名字的这个操作我们就称为
        定义方法!
        */
        /*//做一次菜
        System.out.println("买菜");
        System.out.println("洗菜");
        System.out.println("切菜");
        System.out.println("炒菜");
        //做一天的菜
        //早上
        System.out.println("买菜");
        System.out.println("洗菜");
        System.out.println("切菜");
        System.out.println("炒菜");
        //中午
        System.out.println("买菜");
        System.out.println("洗菜");
        System.out.println("切菜");
        System.out.println("炒菜");
        //晚上
        System.out.println("买菜");
        System.out.println("洗菜");
        System.out.println("切菜");
        System.out.println("炒菜");
        */
        //定义类的实例对象
        MethodsTest1 meth = new MethodsTest1();
        //使用实例对象调用方法
        meth.cook();
    }
}
