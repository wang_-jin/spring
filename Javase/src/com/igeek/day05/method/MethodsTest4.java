package com.igeek.day05.method;

public class MethodsTest4 {
    /*
    方法的重载(overloading):
      在java中是允许出现同名的方法的,
      只要方法的 参数类型 或 参数个数 不同即可,
      这种现象我们称为方法的重载.
    方法重载的条件:
      1.方法名必须相同;
      2.参数类型或参数个数必须不同;
      3.方法重载与返回值类型无关

    方法重载的作用:
      方法重载允许将一些功能相似但参数不同的方法用相同的方法名来命名,
      这样可以减少代码量,提高代码的灵活性!
    */

    public int sum(int a,int b){
      return a+b;
    };

    public int sum(int a,int b,int c){
        return a+b+c;
    }

    public static void main(String[] args) {
        MethodsTest4 meth = new MethodsTest4();
        meth.sum(1,2);
        meth.sum(1,2,3);
    }

    /*
    练习1:比较数据是否相等.参数类型分别为两个byte类型,
    两个short类型,两个int类型,两个long类型,并在main方法中进行测试
    */












    //练习1答案
    /*
    public boolean isEqual(byte a,byte b){
        return a==b?true:false;
    }
    public boolean isEqual(short a,short b){
        return a==b?true:false;
    }
    public boolean isEqual(int a,int b){
        return a==b?true:false;
    }
    public boolean isEqual(long a,long b){
        return a==b?true:false;
    }
    */
}

