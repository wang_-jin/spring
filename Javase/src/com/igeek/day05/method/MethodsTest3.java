package com.igeek.day05.method;
import java.util.Arrays;

public class MethodsTest3 {
    int getMax(int a,int b){
        return a>b?a:b;
    }
    boolean isEqual(int a,int b){
        return a==b?true:false;
    }
    int getThreeMax(int a,int b,int c){
        return ((a>b?a:b)>c)?(a>b?a:b):c;
    }
    int getArrMax(int[] arr){
        Arrays.sort(arr);
        return arr[arr.length-1];
    }
    int getSum(int[] arr){
        int sum = 0;
        for (int item:arr) {
            sum+=item;
        }
        return sum;
    }
    int getArrSum(int[][] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sum+=arr[i][j];
            }
        }
        return sum;
    }
    public static void main(String[] args) {
        //练习1：键盘录入两个数据,定义方法返回两个数中的较大值
/*        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入第一个数：");
        int a = scanner.nextInt();
        System.out.println("请输入第二个数：");
        int b = scanner.nextInt();
        MethodsTest3 m = new MethodsTest3();
        System.out.println("max="+m.getMax(a,b));*/

        //练习2：键盘录入两个数据,定义方法比较两个数是否相等
/*        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入第一个数：");
        int a = scanner.nextInt();
        System.out.println("请输入第二个数：");
        int b = scanner.nextInt();
        MethodsTest3 m = new MethodsTest3();
        boolean equal = m.isEqual(a, b);
        if (equal){
            System.out.println("相同！");
        }else {
            System.out.println("不相同！");
        }*/


        //练习3：键盘录入三个数据,定义方法返回三个数中的最大值
/*        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入第一个数：");
        int a = scanner.nextInt();
        System.out.println("请输入第二个数：");
        int b = scanner.nextInt();
        System.out.println("请输入第三个数：");
        int c = scanner.nextInt();
        MethodsTest3 m = new MethodsTest3();
        int max = m.getThreeMax(a,b,c);
        System.out.println("max="+max);*/

        //练习4：写一个方法,用于获取数组的最大值,并调用方法
        //int[] arr = {1,6,8,6};
/*        MethodsTest3 m = new MethodsTest3();
        int max = m.getArrMax(new int[] {1,10,8,6});
        System.out.println("max="+max);*/


        //练习5：写一个方法,用于对数组进行求和,并调用方法
        /*int[] arr = {1,6,8,6};
        MethodsTest3 m = new MethodsTest3();
        int sum = m.getSum(arr);
        System.out.println("sum="+sum);*/

        //练习6：写一个方法,用于对二维数组进行求和,并调用方法
        int[][] arr = {{1,2,3},{4,5,6}};
        MethodsTest3 m = new MethodsTest3();
        int sum = m.getArrSum(arr);
        System.out.println("sum="+sum);
























        //练习1答案
        /*
        MethodsTest3 meth = new MethodsTest3();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数字a:");
        int a = sc.nextInt();
        System.out.println("请输入数字b:");
        int b = sc.nextInt();
        System.out.println("较大的数是:"+meth.getMax(a,b));
        */

        //练习2答案
        /*
        MethodsTest3 meth = new MethodsTest3();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数字a:");
        int a = sc.nextInt();
        System.out.println("请输入数字b:");
        int b = sc.nextInt();
        System.out.println("a,b是否相等:"+meth.isEqual(a,b));
        */

        //练习3答案
        /*
        MethodsTest3 meth = new MethodsTest3();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数字a:");
        int a = sc.nextInt();
        System.out.println("请输入数字b:");
        int b = sc.nextInt();
        System.out.println("请输入数字c:");
        int c = sc.nextInt();
        System.out.println("最大值是:"+meth.maxInThree(a,b,c));
        */

        //练习4答案
        /*int[] arr = {1,22,3,77,5,7};
        System.out.println(arrayMax(arr));*/

        //练习5答案
        /*int[] arr = {1,2,3,4,5};
        System.out.println(sum(arr));*/

    }
    //练习4答案
    /*public static int arrayMax(int[] arr){
        int max = 0;
        for (int item : arr) {
            if(item>max){
                max = item;
            }
        }
        return max;
    };*/

    //练习5答案
    public static int sum(int[] arr){
        int sum = 0;
        for (int item : arr) {
            sum += item;
        }
        return sum;
    };

   /* public int getMax(int a,int b) {
        return a > b ? a : b;
    };*/

   /* public boolean isEqual(int a,int b){
        return a==b?true:false;
    };*/

    /*public int maxInThree(int a,int b,int c){
        return a>b?(a>c?a:c):(b>c?b:c);
    }*/
}
