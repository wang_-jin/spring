package com.igeek.day05.method;

import java.util.Arrays;

public class MethodsTest5 {
    /*
      参数传递:
        基本数据类型,String类型,包装器类型传递的是值;
        其他引用类型传递的是地址;
    */
    public static void change(int a){
        a = a*2;
        System.out.println("change方法中a:"+a);
    }
    public static void change(int[] arr){
        arr[0] = 99;
        System.out.println("change方法中arr:"+Arrays.toString(arr));
    }
    public static void main(String[] args) {
        /*
        //1.基本类型传值
        int a = 1;
        change(a);
        System.out.println("main方法中a:"+a);
        */

        /*
        //2.引用类型传地址
        int[] arr = {1,2,3,4,5};
        change(arr);
        System.out.println("main方法中arr:"+Arrays.toString(arr));
        */

        /*
        //练习1:分析打印出什么内容?
        public static void fn(int[] y){
          y[0] = 100;
          y = new int[]{100,0};
          y[1] = 200;
          System.out.println("y:"+Arrays.toString(y));
        }
        int[] x = {12,23};
        fn(x);
        System.out.println("x:"+Arrays.toString(x));
        */
        /*
        int[] x = {12,23};
        fn(x);
        System.out.println("x:"+Arrays.toString(x));
        */
    }

    public static void fn(int[] y){
        y[0] = 100;
        y = new int[]{100,0};
        y[1] = 200;
        System.out.println("y:"+Arrays.toString(y));
    }
}
