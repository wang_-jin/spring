package com.igeek.day05.string;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/15 19:22
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
/*        String a = new String("helloworld");//在堆中开辟新的内存空间
        String b = new String("helloworld");
        String c = "helloworld";//字面量定义,放方法区的常量池
        String d = "helloworld";
        System.out.println("a==b?:"+(a==b));//==比较地址 false
        System.out.println("a==b?:"+a.equals(b));//equals比较内容 true
        System.out.println("c==d?:"+(c==d));//地址相同 true
        System.out.println("c==d?:"+c.equals(d));//true
        System.out.println("a==c?:"+(a==c));//false
        System.out.println("a==c?:"+a.equals(c));//true*/
/*        String str = new String("hello");
        int[] arr = {1,2,3};
        System.out.println("变换前的str="+str);
        System.out.println("变换前的arr="+ Arrays.toString(arr));
        Test1.changes(str);
        changes(arr);
        System.out.println("变换后的str="+str);
        System.out.println("变换后的arr="+ Arrays.toString(arr));

    }
    public static void changes(String s){
         s = "world";
    }
    public static void changes(int[] a){
        a[0] = 99;*/
        //练习3:定义一个方法,实现数组中元素添加时,数组能够扩容
        int[] arr = {1,2,3};
        int[] array = expendArray(arr, 4);
        int[] array1 = expendArray(array,5);
        System.out.println(Arrays.toString(array1));
    }
    public static int[] expendArray(int[] arr,int val){
        int[] newArray = Arrays.copyOf(arr,arr.length+1);
        newArray[newArray.length-1]=val;
        return newArray;
    }
}
