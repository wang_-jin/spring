package com.igeek.day05.string;

import java.util.Arrays;

public class StringTest4 {
    public static void main(String[] args) {
        //练习1
        //需求：键盘录入一个字符串,把该字符串的首字母转成大写,
        //其余为小写.(只考虑英文大小写字母字符)
/*        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String s = sc.next();
        String s1 = s.substring(0,1);
        String s2 = s.substring(1);
        String s3 = s1.toUpperCase()+s2;
        System.out.println("s3="+s3);*/


        //练习2:字符串反转
/*        Scanner sc1 = new Scanner(System.in);
        System.out.println("请输入:");
        String s4 = sc1.next();
        char[] c = s4.toCharArray();
        for (int i = 0; i < (c.length)/2; i++) {
            char temp = c[i];
            c[i] = c[c.length-1-i];
            c[c.length-1-i] = temp;
        }
        String c1 = new String(c);
        System.out.println(c1);*/
        //练习3:定义一个方法,实现数组中元素添加时,数组能够扩容



        //练习1
        /*Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个字符串:");
        String str = sc.next();
        //截取第一个字符串
        String first = str.substring(0, 1);
        //截取除第一个外剩下的字符串
        String end = str.substring(1);
        //将第一个字符转为大写并拼接
        String result = first.toUpperCase() + end;
        System.out.println(result);*/

        //练习2答案
        /*String str = "helloworld";
        char[] c = str.toCharArray();
        int left = 0;
        int right = c.length-1;
        for (int i = 0; i < c.length; i++) {
            if(left<right){
                char temp = c[i];
                c[i] = c[c.length-1-i];
                c[c.length-1-i] = temp;
                left++;
                right--;
            }
            str = new String(c);
        }
        System.out.println(str);*/

        //练习3答案
        int[] arr = {1,2};
        System.out.println(Arrays.toString(arrAdd(arr, 3)));

    }
    //练习3答案
/*    public static int[] arrayList(int[] originArray,int newElement){
        int[] newarr = Arrays.copyOf(originArray,originArray.length+1);
        newarr[newarr.length-1] = newElement;
        return newarr;
    }*/















    static int[] arrAdd(int[] arr,int a){
        int[] newArray = Arrays.copyOf(arr,arr.length+1);
        newArray[newArray.length-1]=a;
        return newArray;
    }
}
