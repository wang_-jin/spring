package com.igeek.day05.string;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/15 20:23
 * @description TODO
 */
public class StringDemo2 {
    public static void main(String[] args) {
        //创建字符串对象
        String s = "abcde";
        String s1 = "NiHao";

        //char[] toCharArray():把字符串转换为字符数组
        char[] chs = s.toCharArray();
        char[] str = s1.toCharArray();
        System.out.println("反转前:"+ Arrays.toString(str));
        for (int i = 0; i < (str.length)/2; i++) {
            char temp = str[i];
            str[i] = str[str.length-1-i];
            str[str.length-1-i] = temp;
        }
        for(int x=0; x<chs.length; x++) {
            System.out.println(chs[x]);
        }
        System.out.println("-----------");
        //反转字符NiHao
        String s2 = new String(str);
        System.out.println("反转后:"+s2);//oaHiN
        //String toLowerCase():把字符串转换为小写字符串
        System.out.println("HelloWorld".toLowerCase());
        System.out.println(s1.toLowerCase());//nihao
        //String toUpperCase():把字符串转换为大写字符串
        System.out.println("HelloWorld".toUpperCase());
        System.out.println(s1.toUpperCase());//NIHAO
    }
}
