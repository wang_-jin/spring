package com.igeek.day05.string;

public class StringTest1 {
    public static void main(String[] args) {
        /*
        字符串创建的两种方法:
          1.字面量形式
            String s = "hello";//注意字符串用双引号,字符char用单引号
          2.使用new创建
          String s = new String("hello");

        两种创建方式存储的区别:
          字面量形式:
              字面量形式创建的字符串存储在一个叫 字符串常量池 的特殊内存区域,
              字符串常量池只能存储唯一的字符串常量,因此可以避免重复创建相同的
              字符串对象,可以节省内存空间和提高程序的性能.
          new形式:
              使用new创建字符串,会在堆中开辟一块内存空间存储字符串
              注意:
                在java中只要是使用new创建变量,一般都会在堆中开辟一块
                内存空间存储数据.
        */


        String str1 = "hello";
        String str2 = new String("hello");
        //equals会比较两个字符串的内容是否相同
        System.out.println("equals:"+(str1.equals(str2)));
        /*equals是Object中的方法,比较的是对象的引用,即地址,但有些类中重写了equals方法,
        使其比较的是内容,比如String类型*/
        //==会比较两个字符串的地址是否相同
        /*==基本数据类型比较的是值,引用数据类型比较的是地址*/
        System.out.println("==:"+(str1==str2));

        String str3 = "hello";
        String str4 = new String("hello");
        System.out.println("str2==str4:"+(str2==str4));
        /*字面量形式定义的字符串会放在字符串常量池中,字符串常量池只能存储唯一的字符串常量,因此可以避免重复创建相同的
          字符串对象,可以节省内存空间和提高程序的性能.因此str1和str3会被优化成同一对象*/
        System.out.println("str1==str3:"+(str1==str3));//true


        /*
        注意:
          字符串参数传递,传的是值,不是地址.
          虽然字符串是引用类型,但是因为字符串是
          final不可变的类型,因此同一个地址下
          的字符串是不允许修改的,想要修改字符串
          需要重新开辟一块内存空间存储新字符串,
          并将字符串变量指向新开辟的内存地址!
        */
        String str = "hello";
        change(str);
        System.out.println(str);

    }
    public static void change(String str){
        str+="world";
        System.out.println(str);
    }
}
