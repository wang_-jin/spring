package com.igeek.day05.string;

import java.util.Arrays;

public class StringTest2 {
    public static void main(String[] args) {
        /*
        字符串方法:
        */
        /*
        如何判断一个对象调用了方法后是否将对象本身改变了?
          可以查看这个方法的返回值,如果有返回值类型和对象相同,
          那么源对象一般不会改变,
          返回的那个就是修改后的新对象,如果没有返回值,
          调用方法一般会改变对象本身!
        */
        //1.字符数组转字符串
        /*char[] c = {'a', 'b', 'c', 'd'};
        String str = new String(c);
        System.out.println(str);*/

        //2.toCharArray字符串转字符数组
        /*String str = "abcd";
        char[] c = str.toCharArray();
        //注意:字符数组打印出来是数组内容拼接得到的字符串,
        //而不是一个包含hash码的字符串,这和其他数组打印不一样,需要注意下
        System.out.println(c);
        System.out.println(Arrays.toString(c));*/

        //3.split字符串切割成数组
        //传递一个正则表达式作为参数
        String str = "刘备,18,关羽,19";
        String s = "张飞,20,武器,丈八蛇矛";
        String[] strs = str.split(",");
        String[] s1 = s.split(",");
        System.out.println(Arrays.toString(strs));
        System.out.println(Arrays.toString(s1));

        /*
        4.length()获取字符串长度
        注意要和获取数组长度区分:
          数组长度:arr.length
          字符串长度:str.length()
        */
        /*String str = "hello";
        System.out.println(str.length());*/

        //5.substring截取字符串
/*        String str = "helloworld";
        String str1 = str.substring(1);
        System.out.println(str1);
        //包含0,不包含3
        String str2 = str.substring(0,3);
        System.out.println(str2);*/

        //6.toLowerCase将字符串转为小写
        /*String str = "HelloWorld";
        String str1 = str.toLowerCase();
        System.out.println(str1);*/

        //7.toUpperCase将字符串转为大写
        /*String str = "HelloWorld";
        String str1 = str.toUpperCase();
        System.out.println(str1);*/

    }
}
