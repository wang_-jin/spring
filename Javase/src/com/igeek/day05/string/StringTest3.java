package com.igeek.day05.string;

public class StringTest3 {
    public static void main(String[] args) {
        /*
        字符串查找:
        */
        //1.charAt返回某个索引位置的具体字符
        /*String str = "hello@world";
        char c = str.charAt(5);
        System.out.println(c);*/

        //2.startsWith判断是否以某个字符串开头
        /*
        注意:
          这里使用的是startsWith而不是startWith,
          start后面要加个s,这是因为start是动词,
          这个方法的意思是一个字符串是否是以另外一个
          字符串开头,这里的字符串是第三人称,starts
          则表示第三人称单数形式,这是英语语法的一个
          规范,后面的endsWith也是这个道理!
        */
        /*String str = "liubei@qq.com";
        boolean flag = str.startsWith("liubei");
        System.out.println(flag);*/

        //3.endsWith是否以某个字符串结尾
        /*String str = "liubei@qq.com";
        boolean flag = str.endsWith("com");
        System.out.println(flag);*/

        //4.indexOf返回某个字符串第一次出现的索引位置(从左往右找)
        /*String str = "liubei@qq.com@163.com";
        int i1 = str.indexOf("q");
        System.out.println(i1);*/

        //5.lastIndexOf返回某个字符串第一次出现的索引位置(从右往左找)
        /*String str = "liubei@qq.com@163.com";
        int i1 = str.lastIndexOf("@");
        System.out.println(i1);*/

        //6.contains是否包含某个字符串
        /*String str = "liubei@qq.com@163.com";
        boolean flag = str.contains("qq");
        System.out.println(flag);*/


    }
}
