package com.igeek.ch01.object.calculate;

/**
 * @author wangjin
 * 2023/8/16 17:42
 * @description TODO
 * 5、程序入口main方法
 * 在main中分别通过有参和无参的构造方法实例化成绩类的对象并传入成绩参数,
 * 再通过对象调用计算平均值和总成绩的方法打印输出,
 * 在计算平均值和总成绩的方法中,分别获取到属性的值进行计算
 */
public class Test {
    public static void main(String[] args) {
        Calculate c = new Calculate();
        c.setJavaGrade(88.5F);
        c.setJsGrade(80.5F);
        c.setPythonGrade(92.5F);
        System.out.println(c);
        float aver = c.average();
        System.out.println("平均值为:"+ aver);
        float sum = c.sumGrade();
        System.out.println("总成绩为:"+sum);
    }
}
