package com.igeek.ch01.object.calculate;

/**
 * @author wangjin
 * 2023/8/16 17:16
 * @description TODO
 * 第二题
 * 定义一个成绩计算类
 * 类中包含的内容有:
 * 1、三个float类型的成员属性(java成绩、js成绩、python成绩);
 * 2、一个无参的构造方法和一个带参数的构造方法;
 * 3、计算三个科目的平均值的方法;
 * 4、计算三个科目的总成绩的方法;
 * 5、程序入口main方法
 * 在main中分别通过有参和无参的构造方法实例化成绩类的对象并传入成绩参数,
 * 再通过对象调用计算平均值和总成绩的方法打印输出,
 * 在计算平均值和总成绩的方法中,分别获取到属性的值进行计算
 */
public class Calculate {
    private float javaGrade;
    private float jsGrade;
    private float pythonGrade;

    public Calculate() {
    }

    public Calculate(float javaGrade, float jsGrade, float pythonGrade) {
        this.javaGrade = javaGrade;
        this.jsGrade = jsGrade;
        this.pythonGrade = pythonGrade;
    }
    public float average(){
        return (this.javaGrade+this.jsGrade+this.pythonGrade)/3;
    }
    public float sumGrade(){
        return this.javaGrade+this.jsGrade+this.pythonGrade;
    }

    /**
     * 获取
     * @return javaGrade
     */
    public float getJavaGrade() {
        return javaGrade;
    }

    /**
     * 设置
     * @param javaGrade
     */
    public void setJavaGrade(float javaGrade) {
        this.javaGrade = javaGrade;
    }

    /**
     * 获取
     * @return jsGrade
     */
    public float getJsGrade() {
        return jsGrade;
    }

    /**
     * 设置
     * @param jsGrade
     */
    public void setJsGrade(float jsGrade) {
        this.jsGrade = jsGrade;
    }

    /**
     * 获取
     * @return pythonGrade
     */
    public float getPythonGrade() {
        return pythonGrade;
    }

    /**
     * 设置
     * @param pythonGrade
     */
    public void setPythonGrade(float pythonGrade) {
        this.pythonGrade = pythonGrade;
    }

    public String toString() {
        return "Calculate{javaGrade = " + javaGrade + ", jsGrade = " + jsGrade + ", pythonGrade = " + pythonGrade + "}";
    }

    public static void main(String[] args) {
        Calculate c = new Calculate(88.5F,80.5F,92.5F);
        System.out.println(c);
        float aver = c.average();
        System.out.println("平均值为:"+ aver);
        float sum = c.sumGrade();
        System.out.println("总成绩为:"+sum);
    }
}
