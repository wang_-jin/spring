package com.igeek.ch01.object.person;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/16 16:26
 * @description TODO
 * 第一题
 * 定义一个Person类,类的内容有:
 * 1.有姓名,年龄,爱好三个属性,爱好是一个一维数组
 * 2.定义一个有参的构造方法
 * 3.定义一个添加爱好的方法,每次调用可以往爱好数组中添加一个爱好
 * 4.在main中创建实例对象测试
 */
public class Person {
    private String name;
    private int age;
    private String[] hobbies;

    public Person(){}
    public Person(String name, int age,String[] hobbies){
        this.name = name;
        this.age = age;
        this.hobbies = hobbies;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public int getAge(){
        return age;
    }
    public void setHobbies(String[] hobbies,String val){
        this.hobbies=Arrays.copyOf(hobbies,hobbies.length*3/2+1);
        this.hobbies[this.hobbies.length-1]=val;
    }
    public String[] getHobbies(){
        return hobbies;
    }
}
