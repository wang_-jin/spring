package com.igeek.ch01.object.person;

import com.igeek.ch01.object.person.Person;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/16 16:37
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Person p = new Person("张三",18,new String[]{"游戏"});
        System.out.println("name="+p.getName()+" "+"age="+p.getAge()+" "+"hobbies="+ Arrays.toString(p.getHobbies()));
        p.setHobbies(p.getHobbies(), "运动");
        System.out.println("name="+p.getName()+" "+"age="+p.getAge()+" "+"hobbies="+ Arrays.toString(p.getHobbies()));
    }
}
