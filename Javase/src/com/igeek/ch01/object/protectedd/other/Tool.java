package com.igeek.ch01.object.protectedd.other;

import com.igeek.ch01.object.protectedd.Person;

/**
 * @author wangjin
 * 2023/8/16 18:25
 * @description TODO
 */
public class Tool extends Person{
    public static void main(String[] args) {
        Person p = new Person();
        //proteted访问权限修饰符修饰的成员变量,在其他包中不能被基类的实例对象访问到
        //System.out.println(p.name);
        //proteted访问权限修饰符修饰的成员变量,可以在其他包的子类的实例对象访问到
        Tool t = new Tool();
        System.out.println(t.name);
    }
}
