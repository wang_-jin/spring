package com.igeek.ch01.object.teat1;

/**
 * @author wangjin
 * 2023/8/22 9:09
 * @description TODO
 */
public abstract class Animal {
    public abstract void eat();
}
