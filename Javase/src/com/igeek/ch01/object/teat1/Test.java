package com.igeek.ch01.object.teat1;

/**
 * @author wangjin
 * 2023/8/22 9:09
 * @description TODO
 */
public class Test {
     static void print(Type type){
        switch (type){
            case MAN:
                System.out.println("男生");
                break;
            case WOMAN:
                System.out.println("女生");
                break;
        }

    }
    public static void main(String[] args) {
        Animal animal = new Animal() {
            @Override
            public void eat() {
                System.out.println("吃饭");
            }
        };
        animal.eat();
        print(Type.MAN);

    }
}
