package com.igeek.ch01.object.game;

/**
 * @author wangjin
 * 2023/8/21 19:43
 * @description TODO
 */
public class Animal {
    String name;
    int HP;
    int ATK;
    int DEF;

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    void showInfo(){}
    void attack(Animal a){
        if(this.HP<=0)return;
        //造成的伤害
        int damage = this.ATK - a.DEF;
        //伤害如果小于等于0,则让伤害强制变为1
        damage = (damage<=0?1:damage);
        System.out.println(this.name+"攻击了"+a.name);
        System.out.println(this.name+"对"+a.name+"造成了"+damage+"点伤害");
        a.HP -= damage;
    }
}
