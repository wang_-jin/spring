package com.igeek.ch01.object.game;



/**
 * @author wangjin
 * 2023/8/21 19:43
 * @description TODO
 */
public class Player extends Animal{
    String type;
    public Player(String name, Type type){
        super(name);
        //战士
        if(type == Type.FIGHTER){
            this.type = "战士";
            this.HP = 10;
            this.ATK = 2;
            this.DEF = 4;
        }
        //法师
        else if (type== Type.MAGE) {
            this.type="法师";
            this.HP = 8;
            this.ATK = 4;
            this.DEF = 2;
        }
        else if (type== Type.SHOOTER) {
            this.type="射手";
            this.HP = 6;
            this.ATK = 5;
            this.DEF = 2;
        }
    }

    @Override
    void showInfo() {
        System.out.println("姓名:"+this.name);
        System.out.println("职业:"+this.type);
        System.out.println("生命值:"+this.HP);
        System.out.println("攻击力:"+this.ATK);
        System.out.println("防御力:"+this.DEF);
        System.out.print("---------------------");
    }
    void getEquipment(Equipment equipment){
        if (equipment.name.equals("狂徒铠甲")){
            System.out.println("获得装备:"+equipment.name);
            this.ATK+=equipment.ATK;
            this.DEF+=equipment.DEF;
        }
        if (equipment.name.equals("渴血战斧")){
            System.out.println("获得装备:"+equipment.name);
            this.ATK+=equipment.ATK;
            this.DEF+=equipment.DEF;
        }
        if (equipment.name.equals("卢登的回声")){
            System.out.println("获得装备:"+equipment.name);
            this.ATK+=equipment.ATK;
            this.DEF+=equipment.DEF;
        }
        if (equipment.name.equals("无尽之刃")){
            System.out.println("获得装备:"+equipment.name);
            this.ATK+=equipment.ATK;
            this.DEF+=equipment.DEF;
        }
    }
}
