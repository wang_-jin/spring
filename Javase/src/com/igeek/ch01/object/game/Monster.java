package com.igeek.ch01.object.game;

/**
 * @author wangjin
 * 2023/8/21 19:43
 * @description TODO
 */
public class Monster extends Animal{


    public Monster() {
        String[] m = {"史莱姆","骷髅","狼人"};
        int index = (int)(Math.random() * m.length);
        this.name = m[index];
        switch (name){
            case "史莱姆":
                this.HP = 2;
                this.ATK = 5;
                this.DEF = 1;
                break;
            case "骷髅":
                this.HP = 10;
                this.ATK = 4;
                this.DEF = 0;
                break;
            case "狼人":
                this.HP = 8;
                this.ATK = 5;
                this.DEF = 5;
                break;
        }
    }

    @Override
    void showInfo() {
        System.out.println("怪物名字:"+this.name);
        System.out.println("生命值:"+this.HP);
        System.out.println("攻击力:"+this.ATK);
        System.out.println("防御力:"+this.DEF);
    }
}
