package com.igeek.ch01.object.test;

/**
 * @author wangjin
 * 2023/8/16 14:26
 * @description TODO
 */
public class Dog {
    String name;
    int age;
    String color;

    void eat(String food){
        System.out.println(this.color+"色的小狗"+this.name+"正在吃"+food);
    }
    public Dog(){

    }
    public Dog(String name, int age, String color){
        this.name=name;
        this.age=age;
        this.color=color;
    }

    public static void main(String[] args) {
        Dog d = new Dog("来福",5,"白");
        d.eat("骨头");
    }
}
