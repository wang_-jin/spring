package com.igeek.ch01.object.test;

/**
 * @author wangjin
 * 2023/8/16 10:39
 * @description TODO
 */
public class Phone {
    String brand;
    int price;
    String color;

    void call(String brand){
        System.out.println(this.brand+"手机正在打电话!");
    }
    void sendMessage(String color){
        System.out.println(this.color+"色手机正在发短信!");
    }

    public static void main(String[] args) {
        Phone p = new Phone();
        p.brand="小米";
        p.price=2999;
        p.color="墨绿";
        p.call(p.brand);
        p.sendMessage(p.color);
    }
}
