package com.igeek.ch01.object.pet;

/**
 * @author wangjin
 * 2023/8/17 11:35
 * @description TODO
 */
public class Dog extends Pet{
    private String brand;
    public Dog(){}
    public Dog(String brand){

    }
    public Dog(String name,String color,String brand){
        super(color,name);
        this.brand=brand;
    }
    void info(){
        System.out.println(this.name+"是一只"+this.brand+"它的颜色是"+this.color);
    }

    public static void main(String[] args) {
        Dog d = new Dog("来福","金色","哈士奇");
        d.info();
    }
}
