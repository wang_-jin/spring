package com.igeek.ch01.object.pet;

/**
 * @author wangjin
 * 2023/8/17 11:35
 * @description TODO
 */
public class Pet extends Animal{
    String color;

    public Pet(){}
    public Pet(String color,String name){
        super(name);
        this.color=color;
    }

}
