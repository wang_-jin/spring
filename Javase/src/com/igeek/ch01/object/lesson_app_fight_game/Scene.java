package com.igeek.ch01.object.lesson_app_fight_game;
import java.util.Scanner;
public class Scene {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("一个古老的远古世界," +
                "有着无数的神秘和未知.你作为穿越者来到了这个世界," +
                "你在探索着这个神秘的世界,却被凶残的怪兽盯上了...");
        sc.nextLine();
        System.out.println("这时你的耳边突然响起一个声音");
        sc.nextLine();
        System.out.println("穿越者,请输入你的名字:");
        String name = sc.next();
        System.out.println("请选择你的职业,输入0表示战士,输入1表示法师.");
        int n = sc.nextInt();
        Type type = n==0?Type.FIGHTER:Type.MAGE;
        Player player = new Player(name,type);
        System.out.println("恭喜,注册成功,下面是你的信息:");
        player.showInfo();
        sc = new Scanner(System.in);
        sc.nextLine();
        while (player.HP>0){
            System.out.println("怪物出现了,下面是怪物信息:");
            Monster monster = new Monster();
            //显示怪物信息
            monster.showInfo();
            sc.nextLine();
            System.out.println("请选择应对方式,输入0攻击,输入1投降");
            int n1 = sc.nextInt();
            //攻击
            if(n1 == 0){
                //当主角和怪物的HP都大于0才能进行循环战斗
                while (player.HP>0&&monster.HP>0){
                    //主角攻击怪物
                    player.attack(monster);
                    //怪物攻击主角
                    monster.attack(player);
                    System.out.println("------------------------");
                }
                //当战斗结束,如果主角血量小于等于0,则主角死亡
                //游戏结束
                if(player.HP<=0){
                    System.out.println("你被怪物杀死了,游戏结束!");
                    return;
                }
                //当战斗结束,如果怪物血量小于等于0,则怪物被主角打死,继续游戏
                else if(monster.HP<=0){
                    System.out.println("你杀死了怪物!");
                    sc.nextLine();
                }

            }else{
                Scanner sc1 = new Scanner(System.in);
                System.out.println("你的耳边传来声音:游戏结束,懦弱的凡人,你将臣服于我!");
                sc1.nextLine();
                System.out.println("你:你到底是谁?");
                sc1.nextLine();
                System.out.println("神秘声音:弱者无权知晓神之名姓!毁灭吧蝼蚁!");
                sc1.nextLine();
                System.out.println("你被无名的力量撕成了碎片,消失在异界的寰宇中!");
                return;
            }
        }


    }
}
