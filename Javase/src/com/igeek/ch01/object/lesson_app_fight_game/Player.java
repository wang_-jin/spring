package com.igeek.ch01.object.lesson_app_fight_game;

public class Player extends Animal{

    //职业
    String type;
    public Player(String name,Type type){
      super(name);
      //战士
      if(type == Type.FIGHTER){
          this.type = "战士";
          this.HP = 10;
          this.ATK = 2;
          this.DEF = 4;
      }
      //法师
      else if (type==Type.MAGE) {
          this.type="法师";
          this.HP = 10;
          this.ATK = 4;
          this.DEF = 2;
      }
    }
    //显示信息
    public void showInfo(){
        System.out.println("姓名:"+this.name);
        System.out.println("职业:"+this.type);
        System.out.println("生命值:"+this.HP);
        System.out.println("攻击力:"+this.ATK);
        System.out.println("防御力:"+this.DEF);
        System.out.print("---------------------");
    }



}
