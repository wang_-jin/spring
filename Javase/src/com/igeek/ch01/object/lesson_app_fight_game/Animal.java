package com.igeek.ch01.object.lesson_app_fight_game;

public class Animal {
    //名字
    String name;
    //生命值
    public int HP;
    //攻击力
    int ATK;
    //防御力
    int DEF;
    public Animal(){};
    //定义构造方法
    public Animal(String name){
        this.name = name;
    }
    //显示信息
    public void showInfo(){}
    //发动攻击
    public void attack(Animal a){
        //如果HP小于0,不允许攻击
        if(this.HP<=0)return;
        //造成的伤害
        int damage = this.ATK - a.DEF;
        //伤害如果小于等于0,则让伤害强制变为1
        damage = (damage<=0?1:damage);
        System.out.println(this.name+"攻击了"+a.name);
        System.out.println(this.name+"对"+a.name+"造成了"+damage+"点伤害");
        a.HP -= damage;
        System.out.println(a.name+"剩余血量:"+a.HP);

    }

}
