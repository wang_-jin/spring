package com.igeek.ch01.object.lesson_app_fight_game;

public class Monster extends Animal {
    public Monster() {
        String[] names = {"史莱姆", "骷髅", "狼人"};
        //获取随机怪物索引
        int index = (int) (Math.random() * names.length);
        this.name = names[index];
        switch (this.name) {
            case "史莱姆":
                this.HP = 2;
                this.ATK = 5;
                this.DEF = 1;
                break;
            case "骷髅":
                this.HP = 10;
                this.ATK = 4;
                this.DEF = 0;
                break;
            case "狼人":
                this.HP = 8;
                this.ATK = 5;
                this.DEF = 5;
                break;
        }
    }
    public void showInfo(){
        System.out.println("怪物名字:"+this.name);
        System.out.println("生命值:"+this.HP);
        System.out.println("攻击力:"+this.ATK);
        System.out.println("防御力:"+this.DEF);
    }
}
