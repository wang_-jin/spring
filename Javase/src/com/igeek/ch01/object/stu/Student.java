package com.igeek.ch01.object.stu;

/**
 * @author wangjin
 * 2023/8/16 17:37
 * @description TODO
 * 第三题
 * 封装一个学生类,里面有姓名,年龄,性别3个属性,
 * 提供一个方法用来显示所有属性,一个方法用来学习(并显示谁正在学习),
 * 私有化这些属性并提供对应的get以及set方法并在测试类中测试
 */
public class Student {
    private String stuName;
    private int age;
    private char sex;

    public Student() {
    }

    public Student(String stuName, int age, char sex) {
        this.stuName = stuName;
        this.age = age;
        this.sex = sex;
    }
    public void study(){
        System.out.println(this.stuName+"正在学习~");
    }


    public String getStuName() {
        return stuName;
    }


    public void setStuName(String stuName) {
        this.stuName = stuName;
    }


    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }


    public void setSex(char sex) {
        this.sex = sex;
    }

}
