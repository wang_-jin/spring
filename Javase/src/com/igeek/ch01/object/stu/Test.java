package com.igeek.ch01.object.stu;

/**
 * @author wangjin
 * 2023/8/16 17:53
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Student s = new Student();
        s.setStuName("周杰伦");
        s.setAge(18);
        s.setSex('男');
        s.study();
    }
}
