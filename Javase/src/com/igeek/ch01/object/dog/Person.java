package com.igeek.ch01.object.dog;

/**
 * @author wangjin
 * 2023/8/17 9:47
 * @description TODO
 */
public class Person {
    private String name;
    private int age;
    private Dog dog;
    void play(){
        System.out.println(this.name+"带着他的"+dog.getColor()+"的宠物狗"+dog.getName()+"溜了一圈");
    }
    public Person(){

    }
    public Person(String name,int age,Dog dog){
        this.name=name;
        this.age=age;
        this.dog=dog;
    }

    public static void main(String[] args) {
        Dog d = new Dog();
        d.setColor("白色");
        d.setName("藏獒");
        Person p = new Person("小王",18,d);
        p.play();
    }
}
