package com.igeek.ch01.object.dog;

/**
 * @author wangjin
 * 2023/8/17 9:47
 * @description TODO
 */
public class Dog {
    private String name;
    private String color;

    public Dog(){
    }
    public Dog(String name,String color){
        this.name=name;
        this.color=color;
    }

    String getName(){
        return name;
    }
    void setName(String name){
        this.name=name;
    }
    String getColor(){
        return color;
    }
    void setColor(String color){
        this.color=color;
    }
}
