package com.igeek.ch01.work4.audi;

public interface Smart {
    void automaticParking();
    void automaticDrive();
}
