package com.igeek.ch01.work4.audi;

/**
 * @author wangjin
 * 2023/8/19 11:45
 * @description TODO
请使用代码描述:
奥迪车(Audi)都具有跑的功能，但是智能奥迪车(SmartAudi)除了具有跑的功能外，还具有自动泊车(automaticParking)和无人驾驶(automaticDrive)的功能！
要求:在测试类中创建Audi车对象和智能奥迪车对象,分别调用各自方法; 然后测试向上转型(转换为父类类型和实现的接口类型)和向下转型.
1.	定义奥迪车类(Audi)
a)	成员方法: 跑(run())
i.	输出格式: 奥迪车在跑
2.	定义智能接口(Smart)
a)	抽象方法: 自动泊车(automaticParking)和无人驾驶(automaticDrive)
3.	定义智能奥迪车类(SmartAudi) 继承Audi实现Smart接口
a)	成员方法
i.	实现automaticParking方法
1.	输出:智能奥迪车在自动泊车
ii.	实现automaticDrive方法
1.	输出:智能奥迪车在无人驾驶
4.	定义测试类Test
a)	提供main方法
b)	在main方法中
i.	创建Audi车对象 a,调用跑方法
ii.	创建SmartAudi车对象 sa,调用跑,自动泊车,自动驾驶方法
iii.	定义Audi类型的变量 aa 把sa赋值aa; 测试aa只能调用run方法,不能调用其他方法
iv.	判断如果aa是SmartAudi的实例对象, 把aa强制转换为saa;使用saa调用自动泊车和自动驾驶方法
v.	定义Smart类型的变量 s,把sa赋值给s,测试只能调用自动泊车和自动驾驶方法,不能调用run方法.

 */
public class Test {
    public static void main(String[] args) {
        Audi a = new Audi();
        a.run();
        SmartAudi sa = new SmartAudi();
        sa.run();
        sa.automaticParking();
        sa.automaticDrive();
        System.out.println("----------------");
        Audi aa;
        aa = sa;
        //相当于 Audi aa = new SmartAudi();
        System.out.println("sa:"+sa);
        System.out.println("aa:"+aa);
        System.out.println("----------------");
        aa.run();
        if(aa instanceof SmartAudi){
            SmartAudi saa = ((SmartAudi) aa);
            saa.automaticParking();
            saa.automaticDrive();
        }
        System.out.println("----------------");
        Smart s;
        s = sa;
        //上两行代码相当于 Smart s = new SmartAudi();
        s.automaticParking();
        s.automaticDrive();
    }
}
