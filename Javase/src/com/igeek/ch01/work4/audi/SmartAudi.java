package com.igeek.ch01.work4.audi;

/**
 * @author wangjin
 * 2023/8/21 14:38
 * @description TODO
 */
public class SmartAudi extends Audi implements Smart{
    @Override
    public void automaticParking() {
        System.out.println("智能奥迪车在自动泊车");
    }

    @Override
    public void automaticDrive() {
        System.out.println("智能奥迪车在无人驾驶");
    }
}
