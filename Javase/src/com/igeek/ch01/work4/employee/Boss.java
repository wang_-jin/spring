package com.igeek.ch01.work4.employee;

/**
 * @author wangjin
 * 2023/8/22 18:51
 * @description TODO
 */
public class Boss extends Employee{
    int age;

    public Boss() {
    }

    public Boss(String name, String id, int salary, int age) {
        super(name, id, salary);
        this.age = age;
    }
    public Boss(String name, int age) {
        super(name);
        this.age = age;
    }


    void command(Employee employee){
        System.out.print(this.age+"岁的"+this.getName()+"老板");
        employee.work();
        employee.play();
    }
}
