package com.igeek.ch01.work4.employee;

/**
 * @author wangjin
 * 2023/8/19 11:49
 * @description TODO
请用代码描述
在某个公司每一个员工都有工号,姓名和工资,都要工作. 按照做工作的内容不同分为行政部(AdminStaff),开发部(Developer)和财务部(Treasurer)
所有行政部人员都要工作(采购),但是有的行政部员工会表演(跳舞)
所有的开发部人员都要工作(编程),但有开发部员工会表演(唱歌)
所有的财务部人员都要工作(对账),但是有的财务部员工表演(玩魔术)
35岁的薛之谦老板会安排员工工作和组织员工表演.

 */
public class Test {
    public static void main(String[] args) {
        Developer d = new Developer("小张","s001",12000);
        AdminStaff a = new AdminStaff("小王","s005",10000);
        Treasurer t = new Treasurer("小李","s006",9000);
        Boss b = new Boss("薛之谦",35);
        b.command(a);
        b.command(d);
        b.command(t);

    }
}
