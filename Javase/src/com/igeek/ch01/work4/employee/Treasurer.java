package com.igeek.ch01.work4.employee;

/**
 * @author wangjin
 * 2023/8/22 18:40
 * @description TODO
 */
public class Treasurer extends Employee{

    public Treasurer() {
    }

    public Treasurer(String name, String id, int salary) {
        super(name, id, salary);
    }

    @Override
    public void work() {
        System.out.print("会安排员工对账");
    }

    @Override
    public void play() {
        System.out.println("和组织员工玩魔术");
    }
}
