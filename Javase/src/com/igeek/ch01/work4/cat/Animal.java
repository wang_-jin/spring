package com.igeek.ch01.work4.cat;

/**
 * @author wangjin
 * 2023/8/22 18:30
 * @description TODO
 */
public abstract class Animal {
    String color;
    int age;

    public Animal() {
    }

    public Animal(String color, int age) {
        this.color = color;
        this.age = age;
    }

    static void actingCute() {
        System.out.println("去卖萌");
    }
    static void eat(){
        System.out.println("去吃饭");
    }
}
