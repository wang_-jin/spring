package com.igeek.ch01.work4.cat;

/**
 * @author wangjin
 * 2023/8/19 11:49
 * @description TODO
请使用代码描述:
所有的猫都有颜色和年龄,都会吃(吃鱼)和抓老鼠,但是有部猫会卖萌(actingCute)
所有的狗都有颜色和年龄,都会吃(肯骨头)和看家,但有部分狗会卖萌(actingCute)
去卖萌,会卖萌的猫和狗可以. (提示通过在测试类中的静态方法实现)
如果传入的猫就调用猫的抓老鼠方法
如果传入的狗就调用狗的看家方法
去吃饭,所有动物都可以去(提示通过在测试类中的静态方法实现)
如果传入的猫就调用猫的抓老鼠方法
如果传入的狗就调用狗的看家方法

 */
public class Test {
    public static void main(String[] args) {
        Animal cat = new Cat();
        Animal dog = new Dog();
        Animal.actingCute();
        Animal.eat();
        if (cat instanceof Cat){
            ((Cat) cat).catchMouse();
        }
        if (dog instanceof Dog){
            ((Dog) dog).lookHome();
        }

    }
}
