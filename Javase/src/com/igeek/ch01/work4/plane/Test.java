package com.igeek.ch01.work4.plane;

/**
 * @author wangjin
 * 2023/8/19 11:48
 * @description TODO
请使用代码描述:
所有飞机(Plane)都具有飞(fly)的功能，但是战斗机(Battleplane)除了具有飞的功能外，还具有发射炮弹(fire)的功能！
要求:在测试类中创建战斗机对象,分别调用fly和fire方法; 然后测试向上转型(转换为父类类型和实现的接口类型)和向下转型.

 */
public class Test {
    public static void main(String[] args) {
        Plane bp = new Battleplane();
        bp.fly();
        Planes bp1 = new Battleplane();
        bp1.flys();

        if (bp instanceof Battleplane){
            ((Battleplane) bp).fire();
        }
        ((Battleplane) bp1).fire();
    }
}
