package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:36
 * @description TODO
 */
public class ActedGorilla extends Gorilla implements Actor{
    public ActedGorilla() {
    }

    public ActedGorilla(String color, int age) {
        super(color, age);
    }

    @Override
    public void play() {
        System.out.println(this.age+"岁的"+this.color+"大猩猩在表演骑自行车");
    }
}
