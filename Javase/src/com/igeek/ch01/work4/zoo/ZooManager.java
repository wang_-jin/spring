package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:40
 * @description TODO
 */
public class ZooManager {
    private String name;
    private int age;

    public ZooManager() {
    }

    public ZooManager(String name, int age) {
        this.name = name;
        this.age = age;
    }
    void feed(Animal a){
        a.eat();
    }
    void letItShow(Actor a){
        a.play();
        if(a instanceof Parrot){
            ((Parrot) a).say();
        }
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "ZooManager{name = " + name + ", age = " + age + "}";
    }
}
