package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:35
 * @description TODO
 */
public class Gorilla extends Animal{
    public Gorilla() {
    }

    public Gorilla(String color, int age) {
        super(color, age);
    }

    @Override
    void eat() {
        System.out.println(this.age+"岁的"+this.color+"大猩猩吃香蕉");
    }
}
