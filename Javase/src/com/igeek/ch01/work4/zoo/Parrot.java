package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:37
 * @description TODO
 */
public class Parrot extends Animal{
    public Parrot() {
    }

    public Parrot(String color, int age) {
        super(color, age);
    }
    void say(){
        System.out.println(this.age+"岁的"+this.color+"鹦鹉在说你好");
    }

    @Override
    void eat() {
        System.out.println(this.age+"岁的"+this.color+"鹦鹉在吃小米");
    }
}
