package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:39
 * @description TODO
 */
public class ActedParrot extends Parrot implements Actor{
    public ActedParrot() {
    }

    public ActedParrot(String color, int age) {
        super(color, age);
    }

    @Override
    public void play() {
        System.out.println(this.age+"岁的"+this.color+"鹦鹉在表演过跷跷板");
    }
}
