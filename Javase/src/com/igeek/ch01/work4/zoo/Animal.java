package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:30
 * @description TODO
 */
public abstract class Animal {
    String color;
    int age;
    abstract void eat();

    public Animal() {
    }

    public Animal(String color, int age) {
        this.color = color;
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
