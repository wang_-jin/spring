package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:33
 * @description TODO
 */
public class Tiger extends Animal{

    public Tiger() {
    }

    public Tiger(String color, int age) {
        super(color, age);
    }

    @Override
    void eat() {
        System.out.println(this.age+"岁的"+this.color+"老虎吃肉");
    }
}
