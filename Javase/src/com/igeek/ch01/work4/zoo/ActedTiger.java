package com.igeek.ch01.work4.zoo;

/**
 * @author wangjin
 * 2023/8/22 20:34
 * @description TODO
 */
public class ActedTiger extends Tiger implements Actor{
    @Override
    public void play() {
        System.out.println(this.age+"岁的"+this.color+"老虎在表演钻火圈");
    }
}
