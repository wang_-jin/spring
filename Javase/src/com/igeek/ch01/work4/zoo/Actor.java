package com.igeek.ch01.work4.zoo;

public interface Actor {
    void play();
}
