package com.igeek.ch01.work4.stu;

/**
 * @author wangjin
 * 2023/8/22 20:18
 * @description TODO
 */
public class SportStudent extends Student implements Sport{
    public SportStudent() {
    }

    public SportStudent(String name, int age) {
        super(name, age);
    }

    @Override
    public void playBasketball() {
        System.out.println("年龄为"+this.age+"岁"+this.name+" 在打篮球");
    }
}
