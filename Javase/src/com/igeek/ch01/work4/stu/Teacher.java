package com.igeek.ch01.work4.stu;

/**
 * @author wangjin
 * 2023/8/22 19:42
 * @description TODO
 */
public class Teacher extends Person{

    public Teacher() {
    }

    public Teacher(String name, int age) {
        super(name, age);
    }

    @Override
    void eat() {
        System.out.println("年龄为"+this.age+"岁"+this.name+"老师,正在吃工作餐");
    }
}
