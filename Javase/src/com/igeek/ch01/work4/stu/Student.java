package com.igeek.ch01.work4.stu;

/**
 * @author wangjin
 * 2023/8/22 19:46
 * @description TODO
 */
public class Student extends Person{
    public Student() {
    }

    public Student(String name, int age) {
        super(name, age);
    }

    @Override
    void eat() {
        System.out.println("年龄为"+this.age+"岁"+this.name+" 在吃学生餐");
    }

}
