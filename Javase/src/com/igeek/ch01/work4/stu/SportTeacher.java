package com.igeek.ch01.work4.stu;

/**
 * @author wangjin
 * 2023/8/22 19:44
 * @description TODO
 */
public class SportTeacher extends Teacher implements Sport{

    public SportTeacher() {
    }

    public SportTeacher(String name, int age) {
        super(name, age);
    }

    @Override
    public void playBasketball() {
        System.out.println("年龄为"+this.age+"岁"+this.name+"老师在打篮球");
    }
}
