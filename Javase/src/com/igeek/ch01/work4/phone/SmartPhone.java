package com.igeek.ch01.work4.phone;

/**
 * @author wangjin
 * 2023/8/22 19:22
 * @description TODO
 */
public class SmartPhone extends Phone implements PlayGame{
    public SmartPhone() {
    }

    public SmartPhone(String brand, int price) {
        super(brand, price);
    }

    @Override
    public void palyGame() {
        System.out.println("在使用"+this.price+"元的 "+this.brand+" 手机玩游戏");
    }

    public String toString() {
        return "SmartPhone{}";
    }
}
