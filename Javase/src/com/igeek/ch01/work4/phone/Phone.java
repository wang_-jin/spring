package com.igeek.ch01.work4.phone;

/**
 * @author wangjin
 * 2023/8/22 19:19
 * @description TODO
 */
public class Phone {
    String brand;
    int price;

    public Phone() {
    }

    public Phone(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }
    public void call(String name){
        System.out.println("在使用"+this.price+" 元 "+this.brand+" 手机 给 "+name+" 打电话");
    }
    public void sendMessage(String name){
        System.out.println("在使用"+this.price+" 元 "+this.brand+" 手机 给 "+name+" 发短信");
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * 获取
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String toString() {
        return "Phone{brand = " + brand + ", price = " + price + "}";
    }
}
