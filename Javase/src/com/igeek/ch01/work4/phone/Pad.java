package com.igeek.ch01.work4.phone;

/**
 * @author wangjin
 * 2023/8/22 19:24
 * @description TODO
 */
public class Pad extends Phone implements PlayGame{
    public Pad() {
    }

    public Pad(String brand, int price) {
        super(brand, price);
    }

    @Override
    public void palyGame() {
        System.out.println("在使用"+this.price+"元的 "+this.brand+"平板玩游戏");
    }
    public void listenMusic(){
        System.out.println("在使用"+this.price+"元的 "+this.brand+"板听音乐");
    }
}
