package com.igeek.ch01.work4.game;

/**
 * @author wangjin
 * 2023/8/22 19:08
 * @description TODO
 */
public class Phone implements PlayGame{
    String brand;
    int price;

    public Phone() {
    }

    public Phone(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    @Override
    public void playGame() {
        System.out.println("在使用"+this.price+" 元 "+this.brand+"手机发短信");
    }
    public void call(){
        System.out.println("在使用"+this.price+" 元 "+this.brand+"手机打电话");
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * 获取
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String toString() {
        return "Phone{brand = " + brand + ", price = " + price + "}";
    }
}
