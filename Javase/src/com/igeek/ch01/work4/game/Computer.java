package com.igeek.ch01.work4.game;

/**
 * @author wangjin
 * 2023/8/22 19:12
 * @description TODO
 */
public class Computer implements PlayGame{
    String brand;
    int price;

    public Computer() {
    }

    public Computer(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * 获取
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String toString() {
        return "Computer{brand = " + brand + ", price = " + price + "}";
    }

    @Override
    public void playGame() {
        System.out.println("使用"+this.price+" 元 "+this.brand+"电脑玩游戏");
    }
    public void coding(){
        System.out.println("使用"+this.price+" 元 "+this.brand+"电脑开发JavaEE应用");
    }
}
