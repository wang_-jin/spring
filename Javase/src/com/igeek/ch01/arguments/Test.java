package com.igeek.ch01.arguments;

/**
 * @author wangjin
 * 2023/8/21 15:25
 * @description TODO
 */
public class Test {
    public static void foo(int...  args){
        int acc;
        int max = 0;
        int sum = 0;

        if (args.length==2){
            acc = args[0]*args[1];
            System.out.println("acc:"+acc);
        }
        if (args.length==3) {
            for (int i = 0; i < args.length; i++) {
                if (args[i] > max) {
                    max = args[i];
                }
            }
            System.out.println("max:"+max);
        }
        if (args.length!=2&&args.length!=3){
            for (int i = 0; i < args.length; i++) {
                sum+=args[i];
            }
            System.out.println("sum:"+sum);
        }


    }
    public static void main(String[] args) {
        foo(1,2);
        foo(1,2,3);
        foo(1,2,3,4);
    }
}

