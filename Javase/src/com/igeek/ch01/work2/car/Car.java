package com.igeek.ch01.work2.car;

/**
 * @author wangjin
 * 2023/8/17 20:02
 * @description TODO
 * 2.	定义小汽车类(Car),继承Vehicle类
 * a)	成员方法: 加油 (addGasoline() ))
 * i.	在方法中的输出格式为: 4个轮子的白色小汽车在加油
 */
public class Car extends Vehicle{
    void addGasoline(){
        System.out.println(this.wheel+"个轮子"+this.color+"小汽车在加油");
    }
}
