package com.igeek.ch01.work2.car;

/**
 * @author wangjin
 * 2023/8/16 16:00
 * @description TODO
 *  请用代码描述
 *  四个轮子的白色的汽车可以跑和加油
 *  两个轮子的红色电动自行车可以跑和充电
 *  要求: 把汽车和电动自行车的共性抽取到交通工具类中
 *  1.	定义交通工具类(Vehicle)
 * a)	成员变量
 * i.	轮子个数 wheel
 * ii.	颜色 color
 * b)	成员方法
 * i.	跑  run()
 * 1.	打印格式:  4个轮子白色的车在跑
 * c)	要求:
 * i.	成员变量不带权限修饰符,方法是公共权限的.
 * 2.	定义小汽车类(Car),继承Vehicle类
 * a)	成员方法: 加油 (addGasoline() ))
 * i.	在方法中的输出格式为: 4个轮子的白色小汽车在加油
 * 3.	定义电动车类(ElectricBicycle),继承Vehicle类
 * a)	成员方法: 充电 (charge())
 * i.	在方法中的输出格式为: 2个轮子的黑色电动自行车在充电
 * 4.	定义测试类Test
 * c)	提供main方法
 * d)	在main方法中
 * i.	使用空参构造,创建Car 对象c
 * ii.	把c对象的成员wheel 赋值为 4
 * iii.	把c对象的成员color赋值为 白色
 * iv.	调用c对象成员方法run方法
 * v.	调用c对象的成员方法addGasoline()
 * vi.	使用空参构造,创建ElectricBicycle对象 eb
 * vii.	把eb对象的成员wheel 赋值为2
 * viii.	把ed对象的成员color赋值为 黑色
 * ix.	调用ed对象成员方法run方法
 * x.	调用ed对象成员方法 charge方法
 */
public class Test08 {
    public static void main(String[] args) {
        Car c = new Car();
        c.setWheel(4);
        c.setColor("白色");
        c.run();
        c.addGasoline();
        ElectricBicycle eb = new ElectricBicycle();
        eb.setWheel(2);
        eb.setColor("黑色");
        eb.run();
        eb.charge();// *  四个轮子的白色的汽车可以跑和加油 两个轮子的红色电动自行车可以跑和充电
    }
}
