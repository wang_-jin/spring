package com.igeek.ch01.work2.car;

/**
 * @author wangjin
 * 2023/8/17 19:59
 * @description TODO
 *  1.	定义交通工具类(Vehicle)
 * a)	成员变量
 * i.	轮子个数 wheel
 * ii.	颜色 color
 * b)	成员方法
 * i.	跑  run()
 * 1.	打印格式:  4个轮子白色的车在跑
 * c)	要求:
 * i.	成员变量不带权限修饰符,方法是公共权限的.
 */
public class Vehicle {
    int wheel;
    String color;

    public Vehicle() {
    }

    public Vehicle(int wheel, String color) {
        this.wheel = wheel;
        this.color = color;
    }
    void run(){
        System.out.println(this.wheel+"个轮子"+this.color+"的车在跑");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public int getWheel(){
        return wheel;
    }
    public void setWheel(int wheel){
        this.wheel = wheel;
    }
}
