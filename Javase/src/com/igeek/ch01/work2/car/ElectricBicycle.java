package com.igeek.ch01.work2.car;

/**
 * @author wangjin
 * 2023/8/17 20:02
 * @description TODO
 * 3.	定义电动车类(ElectricBicycle),继承Vehicle类
 * a)	成员方法: 充电 (charge())
 * i.	在方法中的输出格式为: 2个轮子的黑色电动自行车在充电
 */
public class ElectricBicycle extends Vehicle{
    void charge(){
        System.out.println(this.wheel+"个轮子"+this.color+"电动自行车在充电");
    }
}
