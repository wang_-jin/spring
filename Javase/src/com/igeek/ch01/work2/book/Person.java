package com.igeek.ch01.work2.book;

/**
 * @author wangjin
 * 2023/8/16 20:45
 * @description TODO
 * 2. 定义人类(Person)
 * a)	属性: 名称(name)
 * b)	行为: 看书: readBook(Book book)
 * c)	要求: 提供带参构造
 */
public class Person {
    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
    void readBook(Book book){
        System.out.println(this.name+"正在看"+book.getName()+"小说");
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Person{name = " + name + "}";
    }
}
