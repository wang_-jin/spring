package com.igeek.ch01.work2.book;

/**
 * @author wangjin
 * 2023/8/16 20:45
 * @description TODO
 * 1. 定义书类(Book)
 * a)	属性: 名称(name)
 * b)	要求:
 * i.	提供带参构造,setXxx与getXxx方法
 */
public class Book {
    private String name;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Book{name = " + name + "}";
    }
}
