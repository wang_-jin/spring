package com.igeek.ch01.work2.phone;

/**
 * @author wangjin
 * 2023/8/16 20:31
 * @description TODO
 * 2. 定义人类(Person)
 * i.	成员变量: 姓名(name),年龄(age)
 * ii.	成员方法: 使用手机玩游戏 (usePhone(Phone p))
 * iii.	提供空参和带参构造方法
 * iv.	提供setXxx和getXxx方法
 */
public class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    void usePhone(Phone p){
        System.out.println(this.age+"岁的"+this.name+"在使用"+p.getBrand()+"手机玩游戏");
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
}
