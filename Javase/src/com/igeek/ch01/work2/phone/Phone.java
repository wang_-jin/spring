package com.igeek.ch01.work2.phone;

/**
 * @author wangjin
 * 2023/8/16 20:31
 * @description TODO
 * 1. 定义手机类(Phone)
 * i.	成员变量:  品牌(brand)
 * ii.	成员方法:  玩游戏(void playGame())
 * iii.	提供空参和带参构造方法
 * iv.	提供setXxx和getXxx方法
 */
public class Phone {
    private String brand;

    public Phone() {
    }

    public Phone(String brand) {
        this.brand = brand;
    }
    void playGame(){
        System.out.println(this.brand+"手机玩游戏");
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String toString() {
        return "Phone{brand = " + brand + "}";
    }
}
