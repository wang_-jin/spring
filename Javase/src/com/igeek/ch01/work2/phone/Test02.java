package com.igeek.ch01.work2.phone;

/**
 * @author wangjin
 * 2023/8/16 15:56
 * @description TODO
 * 1. 定义手机类(Phone)
 * i.	成员变量:  品牌(brand)
 * ii.	成员方法:  玩游戏(void playGame())
 * iii.	提供空参和带参构造方法
 * iv.	提供setXxx和getXxx方法
 * 2. 定义人类(Person)
 * i.	成员变量: 姓名(name),年龄(age)
 * ii.	成员方法: 使用手机玩游戏 (usePhone(Phone p))
 * iii.	提供空参和带参构造方法
 * iv.	提供setXxx和getXxx方法
 * 3. 定义Test类
 * a)	提供main方法
 * b)	在main方法中
 * i.	创建手机对象,并把手机品牌赋值为iPhone
 * ii.	创建Person对象,并把名称赋值为景甜,年龄赋值为18
 * iii.	调用Person对象的,usePhone方法,传入手机对象
 * iv.	打印格式如下:
 *         18岁的景甜在使用iPhone手机玩游戏
 */
public class Test02 {
    public static void main(String[] args) {
        Phone p = new Phone("iPhone");
        Person p1 = new Person("景甜",18);
        p1.usePhone(p);
    }
}
