package com.igeek.ch01.work2.boy;

/**
 * @author wangjin
 * 2023/8/17 20:13
 * @description TODO
 */
public class Person {
    String name;
    int age;
    Clother clother;
    public Person(){}
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
    // * 请使用代码描述: 45岁的大男孩李晨有一个20岁的女友(范冰冰),李晨在逛街(和范冰冰)
    public void play(Person p){
        System.out.println(this.age+"岁的大男孩"+this.name+"有一个"+p.getAge()+
                "岁的女友("+p.getName()+"),"+this.name+"在逛街(和"+p.getName()+")");
    }
    //18岁的欧阳青青正在洗一件白色的LiNing牌的衣服
    public void wash(Clother clother){
        System.out.println(this.age+"岁的"+this.name+"正在洗一件"+clother.getColor()+"的"+
                clother.getBrand()+"牌的衣服");
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Clother getClother() {
        return clother;
    }

    public void setClother(Clother clother) {
        this.clother = clother;
    }
}
