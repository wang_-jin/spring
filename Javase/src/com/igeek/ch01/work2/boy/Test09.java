package com.igeek.ch01.work2.boy;

/**
 * @author wangjin
 * 2023/8/16 16:00
 * @description TODO
 * 请用代码描述: 18岁的欧阳青青正在洗一件白色的LiNing牌的衣服
 * 请使用代码描述: 45岁的大男孩李晨有一个20岁的女友(范冰冰),李晨在逛街(和范冰冰)
 */
public class Test09 {
    public static void main(String[] args) {
        Clother c = new Clother("LiNing","白色");
        Person p  = new Person("欧阳青青",18);
        p.wash(c);
        Person p1 = new Person("李晨",45);
        Person p2 = new Person("范冰冰",20);
        p1.play(p2);
    }
}
