package com.igeek.ch01.work2.boy;

/**
 * @author wangjin
 * 2023/8/17 20:13
 * @description TODO
 */
public class Clother {
    String brand;
    String color;
    public Clother(){}
    public Clother(String brand,String color){
        this.brand = brand;
        this.color = color;
    }
    public String getBrand(){
        return brand;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }
}
