package com.igeek.ch01.work2.work06;

/**
 * @author wangjin
 * 2023/8/17 9:31
 * @description TODO
 * 2. 定义Person类
 * a)	成员变量
 * i.	姓名(name)
 * ii.	年龄(age)
 * iii.	手机(Phone)
 * b)	成员方法: 打电话: public void callTo(Person p)  使用自己的手机给指定的人打电话
 * i.	在方法内部调用手机的call(String name) 方法,给传入的人打电话
 * c)	要求:
 * i.	提供无参和带参构造
 * ii.	提供setXxx和getXxx方法
 */
public class Person {
    private String name;
    private int age;
    private Phone phone;
    public Person(){}
    public Person(String name,int age,Phone phone){
        this.name=name;
        this.age=age;
        this.phone=phone;
    }
    public void callTo(Person p){
        System.out.print(this.getAge()+"岁的"+this.getName()+"使用"+phone.getPrice()+"元的"+phone.getBrand()+"的手机");
        phone.call(p.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }
}
