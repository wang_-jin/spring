package com.igeek.ch01.work2.sing;

/**
 * @author wangjin
 * 2023/8/16 20:41
 * @description TODO
 * 1. 定义Person类
 * a)	属性: 姓名(name)
 * b)	行为: 唱歌(void sing(String song))
 * c)	要求： 提供带参构造方法
 */
public class Person {
    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
    void sing(String song){
        System.out.println(this.name+"正在演唱"+song+"歌曲");
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Person{name = " + name + "}";
    }
}
