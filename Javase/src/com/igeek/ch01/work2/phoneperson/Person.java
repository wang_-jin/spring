package com.igeek.ch01.work2.phoneperson;

/**
 * @author wangjin
 * 2023/8/16 20:53
 * @description TODO
 * 2. 定义人类(Person)
 * a)	属性: 名称(name)
 * b)	行为:  卖手机: Phone sellPhone() ,在方法内部使用匿名对象作为返回值
 * c)	要求: 提供带参构造,setXxx和getXxx方法
 */
public class Person {
    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
    Phone sellPhone(Phone phone){
        Phone p2 = new Phone(phone.getBrand(),phone.getPrice());
        return p2;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Person{name = " + name + "}";
    }
}
