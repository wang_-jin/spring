package com.igeek.ch01.work2.phoneperson;

/**
 * @author wangjin
 * 2023/8/16 15:58
 * @description TODO
 * 请用代码描述： 小王卖出一部价值6000的iPhone手机
 * 1. 定义手机类
 * a)	属性: 品牌(brand),价格(price)
 * b)	要求:
 * i.	提供带参构造,setXxx与getXxx方法
 * 2. 定义人类(Person)
 * a)	属性: 名称(name)
 * b)	行为:  卖手机: Phone sellPhone() ,在方法内部使用匿名对象作为返回值
 * c)	要求: 提供带参构造,setXxx和getXxx方法
 * 3. 定义测试类Test
 * a)	提供main方法
 * b)	在main方法中
 * i.	使用带参构造创建Person对象 p,name初始化为小王
 * ii.	调用对象p的sellPhone()方法,赋值给Phone phone变量
 * iii.	在main方法输出: 小王卖出一部价值6000的iPhone手机
 */
public class Test05 {
    public static void main(String[] args) {
        Person p = new Person("小王");
        Phone p1 = p.sellPhone(new Phone("iPhone",6000));
        System.out.println(p.getName()+"卖出一部价值"+p1.getPrice()+"的"+p1.getBrand()+"手机");
    }
}
