package com.igeek.ch01.work2.phoneperson;

/**
 * @author wangjin
 * 2023/8/16 20:53
 * @description TODO
 * 1. 定义手机类
 * a)	属性: 品牌(brand),价格(price)
 * b)	要求:
 * i.	提供带参构造,setXxx与getXxx方法
 */
public class Phone {
    private String brand;
    private int price;

    public Phone() {
    }

    public Phone(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * 获取
     * @return price
     */
    public int getPrice() {
        return price;
    }

    /**
     * 设置
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String toString() {
        return "Phone{brand = " + brand + ", price = " + price + "}";
    }
}
