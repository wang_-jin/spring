package com.igeek.ch01.work2.cat;

/**
 * @author wangjin
 * 2023/8/16 20:26
 * @description TODO
 * 1. 定义Cat类
 * a)	成员变量 名称(name), 颜色(color),年龄(age)
 * b)	成员方法 抓老鼠(catchMouse())
 * c)	提供空参和带参的构造方法
 * d)	提供setter和getter方法
 */
public class Cat {
    private String name;
    private String color;
    private int age;


    public Cat() {
    }

    public Cat(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }
    void catchMouse(){
        System.out.println(this.age+"岁的"+this.color+this.name+",在抓老鼠");
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Cat{name = " + name + ", color = " + color + ", age = " + age + "}";
    }
}
