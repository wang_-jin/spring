package com.igeek.ch01.work2.cat;

/**
 * @author wangjin
 * 2023/8/16 15:55
 * @description TODO
 * 1. 定义Cat类
 * a)	成员变量 名称(name), 颜色(color),年龄(age)
 * b)	成员方法 抓老鼠(catchMouse())
 * c)	提供空参和带参的构造方法
 * d)	提供setter和getter方法
 *         2. 创建测试类Test01
 * a) 提供main方法
 * b) 在main方法中
 * a)	创建Cat对象,并把名称设置为加菲,颜色设置为棕色,年龄设置为2
 * b)	调用Cat对象的catchMouse()方法,打印格式如下:
 *  					 2岁的棕色加菲猫,在抓老鼠
 */
public class Test01 {
    public static void main(String[] args) {
        Cat c = new Cat("加菲猫","棕色",2);
        c.catchMouse();
    }
}
