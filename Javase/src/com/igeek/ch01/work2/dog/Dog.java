package com.igeek.ch01.work2.dog;

/**
 * @author wangjin
 * 2023/8/17 19:48
 * @description TODO
 */
public class Dog extends Animal{
    public Dog(){}
    public Dog(String name,String color){
        super(name,color);
    }
    public void lookHome(){
        System.out.println(this.color+"的"+this.name+"狗在看家");
    }
}
