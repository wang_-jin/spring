package com.igeek.ch01.work2.dog;

/**
 * @author wangjin
 * 2023/8/17 19:45
 * @description TODO
 *  1.	定义动物类(Animal)
 * a)	成员变量
 * i.	名称 name
 * ii.	颜色 color
 * b)	成员方法
 * i.	吃  eat()
 * 1.	打印格式: 白色的Tom动物在吃东西
 * c)	要求:
 * i.	成员变量不带权限修饰符,方法是公共权限的.
 */
public class Animal {
    String name;
    String color;
    public Animal(){}
    public Animal(String name,String color){
        this.name=name;
        this.color=color;
    }
    public void eat(){
        System.out.println(this.color+"的"+this.name+"动物在吃东西");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
