package com.igeek.ch01.work2.dog;

/**
 * @author wangjin
 * 2023/8/17 19:48
 * @description TODO
 */
public class Cat extends Animal{
    public Cat(){}
    public Cat(String name,String color){
        super(name,color);
    }
    public void catchMouse(){
        System.out.println(this.color+"的"+this.name+"猫在抓老鼠");
    }

}
