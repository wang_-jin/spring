package com.igeek.ch01.work2.cook;

/**
 * @author wangjin
 * 2023/8/17 20:33
 * @description TODO
 */
public class Waiter extends Employee{
    public Waiter() {
    }

    public Waiter(String name, int age) {
        super(name, age);
    }

    //18岁的服务员景甜,会说话,上菜
    @Override
    public void info(){
        System.out.println(this.age+"岁的服务员"+this.name+",会说话,上菜");
    }
}
