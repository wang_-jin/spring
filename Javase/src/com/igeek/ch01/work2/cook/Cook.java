package com.igeek.ch01.work2.cook;

/**
 * @author wangjin
 * 2023/8/17 20:33
 * @description TODO
 */
public class Cook extends Employee{
    public Cook() {
    }

    public Cook(String name, int age) {
        super(name, age);
    }

    // * 30岁的厨师秦俊杰,会说话,炒菜
    @Override
    public void info(){
        System.out.println(this.age+"岁的厨师"+this.name+",会说话,炒菜");
    }
}
