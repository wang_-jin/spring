package com.igeek.ch01.work2.cook;

/**
 * @author wangjin
 * 2023/8/16 16:02
 * @description TODO
 * 请使用代码描述
 * 18岁的服务员景甜,会说话,上菜
 * 30岁的厨师秦俊杰,会说话,炒菜
 * 要求: 把服务员类和厨师类的共性抽取到员工类中
 */
public class Test10 {
    public static void main(String[] args) {
        Employee w = new Waiter("景甜",18);
        w.info();
        Employee c = new Cook("秦俊杰",30);
        c.info();
    }
}
