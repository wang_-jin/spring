package com.igeek.ch01.recursion;

/**
 * @author wangjin
 * 2023/8/21 16:31
 * @description TODO
 */
/*
2.斐波那契数列是一个数列,每个数字是前两个数字之和,
数列的前几个数字是:1, 1, 2, 3, 5, 8, 13, 21...
使用递归来计算斐波那契数列中的第n个数字?
foo(1)=1;
foo(2)=1;
foo(3)=foo(2)+foo(1);
foo(4)=foo(3)+foo(2);
foo(5)=foo(4)+foo(3);
...
foo(n)=foo(n-1)+foo(n-2);
*/
public class Test2 {
    public static int foo(int n){
        if(n==1||n==2)return 1;
        return foo(n-1)+foo(n-2);
    }

    public static void main(String[] args) {
        System.out.println(foo(3));
    }
}
