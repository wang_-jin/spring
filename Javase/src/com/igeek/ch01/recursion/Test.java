package com.igeek.ch01.recursion;

/**
 * @author wangjin
 * 2023/8/21 15:50
 * @description TODO
 */
public class Test {
    /*1.求1~n的阶乘1*2*3*4*5*...*n
2.猴子吃桃子问题,第一天吃了一半多一个,第二天再吃一半多一个,第10天剩余1个,
请问第一天摘了多少个桃子?*/
     static int getMultiple(int num){
        if (num==1)return 1;
        return getMultiple(num-1)*num;
    }

    public static void main(String[] args) {
        System.out.println("阶乘:"+getMultiple(3));
    }
}
