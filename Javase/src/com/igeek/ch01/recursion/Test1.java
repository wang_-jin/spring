package com.igeek.ch01.recursion;

/**
 * @author wangjin
 * 2023/8/21 15:55
 * @description TODO
 */
public class Test1 {
    /*2.猴子吃桃子问题,第一天吃了一半多一个,第二天再吃一半多一个,第10天剩余1个,
请问第一天摘了多少个桃子?*/
    /*
      f(1)=(f(2)+1)*2
      f(2)=(f(3)+1)*2


      f(8)=(f(9)+1)*2
      f(10)=1
      f(n)=(f(n+1)+1)*2

    */
    static double m(int day) {
        if (day==10)return 1;
            return (m(day+1)+1)*2;
    }

    public static void main(String[] args) {
        System.out.println("第一天摘的桃子:" + m(1));
    }

}
