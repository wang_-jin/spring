package com.igeek.ch01.interfacee;
/*1.定义一个Fly接口,接口中有一个fly方法,
定义一个Run接口,接口中有一个run方法,
定义一个Exercise接口,该接口继承Fly和Run接口;
2.定义一个Bird类,实现Exercise接口,
定义一个Dog类,实现Run接口;
3.创建一个测试类Test,在main方法中创建Dog和Bird
实例对象,Dog对象执行run方法打印"狗在奔跑",Bird对象
执行fly和run方法,分别打印"鸟在飞行","鸟在奔跑".*/

public interface Fly {
    void fly();
}
