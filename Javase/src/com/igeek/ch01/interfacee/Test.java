package com.igeek.ch01.interfacee;

/**
 * @author wangjin
 * 2023/8/19 19:08
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Dog d = new Dog();
        d.run();
        Bird b = new Bird();
        b.fly();
        b.run();

    }
}
