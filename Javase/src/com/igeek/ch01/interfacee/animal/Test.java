package com.igeek.ch01.interfacee.animal;

/**
 * @author wangjin
 * 2023/8/19 20:49
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Animal a = new Animal() {
            @Override
            void eat() {
                System.out.println("吃饭");
            }

            @Override
            void drink() {
                System.out.println("喝水");
            }
        };
        a.eat();
        a.drink();
    }
}
