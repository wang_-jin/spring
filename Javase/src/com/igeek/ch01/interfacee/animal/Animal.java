package com.igeek.ch01.interfacee.animal;

/**
 * @author wangjin
 * 2023/8/19 20:47
 * @description TODO
定义一个抽象类Animal,有eat和drink两个抽象方法,
创建一个测试类Test,在Test的main方法中使用匿名内部类
创建一个Animal的实例并实现eat,drink方法,并执行这两个
方法分别打印"吃饭","喝水"
 */
public abstract class Animal {
    abstract void eat();
    abstract void drink();
}
