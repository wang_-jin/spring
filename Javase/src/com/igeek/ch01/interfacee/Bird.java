package com.igeek.ch01.interfacee;

/**
 * @author wangjin
 * 2023/8/19 19:07
 * @description TODO
 */
public class Bird implements Exercise{
    @Override
    public void fly() {
        System.out.println("鸟在飞行");
    }

    @Override
    public void run() {
        System.out.println("鸟在奔跑");
    }
}
