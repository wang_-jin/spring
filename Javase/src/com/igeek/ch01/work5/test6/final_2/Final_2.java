package com.igeek.ch01.work5.test6.final_2;

/**
 * @author wangjin
 * 2023/8/28 20:05
 * @description TODO
 */
//final修饰的方法不可以被覆盖,但父类中没有被final修饰方法，子类覆盖后可以加final
public class Final_2 {
    final void finalMethod(){}
    void normalMethod(){}
}
