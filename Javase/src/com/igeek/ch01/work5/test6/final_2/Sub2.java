package com.igeek.ch01.work5.test6.final_2;

/**
 * @author wangjin
 * 2023/8/28 20:21
 * @description TODO
 */
public class Sub2 extends Final_2{
    @Override
    final void normalMethod() {
        super.normalMethod();
    }
    /*父类中没有被final修饰方法，子类覆盖后可以加final*/


/*    @Override
    void finalMethod() {
        super.normalMethod();
    }*/
    /*final修饰方法不能被覆盖(重写)*/
}
