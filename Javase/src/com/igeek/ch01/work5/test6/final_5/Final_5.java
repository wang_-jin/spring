package com.igeek.ch01.work5.test6.final_5;

/**
 * @author wangjin
 * 2023/8/28 20:06
 * @description TODO
 */
//修饰成员变量，需要在创建对象前赋值，否则报错。(当没有显式赋值时，多个构造方法的均需要为其赋值。
public class Final_5 {
    final int a = 5;
    final int b;
    /*被final修饰的成员变量可能没有被赋值*/

    public Final_5() {
        b = 20;
    }



    public Final_5(int a, int b){
        //this.a = a;
        /*被final修饰成员变量只能赋值一次*/
        this.b = b;
        /*当没有显式赋值时，多个构造方法的均需要为其赋值*/
    }
}
