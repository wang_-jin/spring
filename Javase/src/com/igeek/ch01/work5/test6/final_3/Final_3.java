package com.igeek.ch01.work5.test6.final_3;

/**
 * @author wangjin
 * 2023/8/28 20:05
 * @description TODO
 */
//验证: final修饰的变量称为常量，这些变量只能赋值一次
public class Final_3 {
    public static void main(String[] args) {
        final int a = 10;
        final int b;
        b = 5;
        //a = 20;
        /*final修饰的变量称为常量，这些变量只能赋值一次*/

        //b = 15;
        /*final修饰的变量称为常量，这些变量只能赋值一次*/
    }
}
