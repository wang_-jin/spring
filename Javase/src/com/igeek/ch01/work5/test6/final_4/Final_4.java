package com.igeek.ch01.work5.test6.final_4;

/**
 * @author wangjin
 * 2023/8/28 20:06
 * @description TODO
 */
//验证:引用类型的变量值为对象地址值，地址值不能更改，但是地址内的对象属性值可以修改
public class Final_4 {
    public static void main(String[] args) {
        final Cat c = new Cat(2);
        //c = new Cat();
        /*引用类型的变量值为对象地址值，地址值不能更改*/
        c.age = 3;
        /*地址内的对象属性值可以修改*/
    }
}
