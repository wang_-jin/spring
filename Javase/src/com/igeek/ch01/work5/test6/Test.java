package com.igeek.ch01.work5.test6;

/**
 * @author wangjin
 * 2023/8/21 11:45
 * @description TODO
 */
public class Test {
    /*1.	验证:final修饰类不可以被继承，但是可以继承其他类
a)	定义类(Final_1),用于验证第一个特点
b)	在类上面使用注释写上: 验证:final修饰类不可以被继承，但是可以继承其他类
c)	在Final_1.java中,定义被修final修饰的类(FinalClass) 类
d)	在Final_1.java中,定义子类Sub继承FinalClass类,编译报错,把鼠标放在红色X上面,提示:The type Sub cannot subclass the final class FinalClass:
e)	把类Sub,使用注释掉,然后在下面写上: 被final修饰的类不能被继承
f)	让FinalClass继承Final_1,编译通过; 在FinalClass上面使用注释写上:  验证:被final的修饰类可以继承其他类.

2.	final修饰的方法不可以被覆盖,但父类中没有被final修饰方法，子类覆盖后可以加final
a)	定义类(Final_2),用于验证第二特点
i.	成员方法:
1.	被final修饰的成员方法 finalMethod
2.	普通方法 normalMethod
b)	在Final_2.java中,定义子类(Sub2) 继承 Final_2
i.	覆盖 finalMethod编译报错,把鼠标放到红色x上面,提示:Cannot override the final method from Final_2: 被final修饰方法不能被覆盖(重写);
ii.	注释调用这个方法,在上面使用注释写上: final修饰方法不能被覆盖(重写);
iii.	覆盖normalMethod 方法,在权限修饰符 和返回值直接加上final,编译通过,在这个方法上面写上: 父类中没有被final修饰方法，子类覆盖后可以加final
3.	final修饰的变量称为常量，这些变量只能赋值一次
a)	定义Final_3类,表示用于验证final的第三个特点
b)	在这个类上面使用注释写上: 验证: final修饰的变量称为常量，这些变量只能赋值一次
c)	提供main方法
d)	在main方法中
i.	定义被final修饰的整型变量a,初始化值为10
ii.	给a赋值为20,编译报错,
iii.	把鼠标放在红色x上面提示:The final local variable a cannot be assigned:被fianl修饰的局部变量不能被赋值;
iv.	注释掉这句话,在上面写上:final修饰的变量称为常量，这些变量只能赋值一次
v.	定义被final修饰的整型变量b,没有初始化值
vi.	下一行,给变量b赋值为5
vii.	然后再给变量b赋值为15,编译报错
viii.	把鼠标放到红色x上面提示:The final local variable b may already have been assigned: 被final修饰的局部变量已经赋值了
ix.	注释掉这个赋值语句,然后在上面使用注释写上:final修饰的变量称为常量，这些变量只能赋值一次
4.	引用类型的变量值为对象地址值，地址值不能更改，但是地址内的对象属性值可以修改
a)	定义类(Final_4),表示用于验证final的第四个特点,在这个类上面使用注释写上:验证:引用类型的变量值为对象地址值，地址值不能更改，但是地址内的对象属性值可以修改
b)	在Fianl4.java中,定义辅助类,随便写,我这里使用猫类(Cat)
i.	定义公共权限成员变量: 年龄 (age)
c)	在Fianl_4类中提供main方法
d)	在main方法中
i.	定义被final修饰的Cat类型的引用变量c,new Cat()赋值给它
ii.	然后,在new Cat() 赋值给变量c,编译报错
iii.	错误提示:The final local variable c cannot be assigned. 被fianl修饰的局部变量不能被赋值
iv.	注释掉这个赋值语句,然后在上面使用注释写上: 引用类型的变量值为对象地址值，地址值不能更改
e)	给对象c的age属性赋值为2,编译通过; 表示: 其指向的对象属性值可以修改
f)	在这话上面,使用注释写上:地址内的对象属性值可以修改
5.	修饰成员变量，需要在创建对象前赋值，否则报错。(当没有显式赋值时，多个构造方法的均需要为其赋值。
a)	定义类(Final_5),表示用于验证final的第五个特点
b)	在这个类上面使用注释写上:验证:修饰成员变量，需要在创建对象前赋值，否则报错。(当没有显式赋值时，多个构造方法的均需要为其赋值。
c)	这里需要验证两点:
i.	定义final成员变量的时候,显示赋值
ii.	否则每一个构造方法都需要给这个final修饰成员变量赋值
d)	定义被final修饰的整型成员变量a,并显式赋值为10
e)	定义被final修饰的整型成员变量b,不显式赋值,编译报错,
i.	提示The blank final field b may not have been initialized,被final修饰的成员变量可能没有被赋值
f)	提供空参构造方法,上面成员变量b不报错了,但是这里开始报错了
i.	提示:The blank final field b may not have been initialized
ii.	在空参构造方法内部写上: b = 20; 编译通过
g)	提供满参构造,但是不给a和b赋值,报错
i.	提示:The blank final field b may not have been initialized
ii.	在满参构造方法中写上this.b = b; 编译通过,说明: 当没有显式赋值时，多个构造方法的均需要为其赋值.
iii.	在写上 this.a = a; 编译报错,提示:The final field Final_5.a cannot be assigned 表示被final修饰成员变量只能赋值一次
iv.	注释掉这句代码,在后面写上,被final修饰成员变量只能赋值一次
*/
}
