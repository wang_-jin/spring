package com.igeek.ch01.work5.test3;

/**
 * @author wangjin
 * 2023/8/21 11:41
 * @description TODO
 */
//看下面的代码说出运行结果以及执行流程
public class Test {
    static {
        System.out.println("Test 静态代码块");
    }
    public Test(){
        System.out.println("Test 构造方法");
    }
    public static void main(String[] args) {
        System.out.println("Test main方法");
        Test t = new Test();
        /*代码执行顺序:"Test 静态代码块"
                     "Test main方法"
                     "Test 构造方法"*/
    }
}

