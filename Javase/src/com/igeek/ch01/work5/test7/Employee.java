package com.igeek.ch01.work5.test7;

/**
 * @author wangjin
 * 2023/8/28 20:50
 * @description TODO
 */
public abstract class Employee {
    int id;
    String name;
    void work(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
