package com.igeek.ch01.work5.test12;

/**
 * @author wangjin
 * 2023/8/29 19:03
 * @description TODO
 */
public class Test {
    /*请使用代码描述
某公司成立一个游泳俱乐部,只要是会游泳的都可以加入;
要求: 使用到接口,接口作为参数,匿名内部类.
提示: 把游泳功能抽取到接口中 ,在测试类中提供参加运动会的静态方法*/
    static void joinSport(Swim swim){
        swim.canSwim();
    }

    public static void main(String[] args) {
        Swim swim = new Swim() {
            @Override
            public void canSwim() {
                System.out.println("某公司成立一个游泳俱乐部,只要是会游泳的都可以加入");
            }
        };
        joinSport(swim);
    }
}
