package com.igeek.ch01.work5.test2;

/**
 * @author wangjin
 * 2023/8/21 9:45
 * @description TODO
 */
//请说出下面代码执行结果并绘制内存图
public class Student {
    //	学校
    public static String school  = "清华大学";
    //	姓名
    private String name;
    //	年龄
    private int age;

    //	构造方法
    public Student() {
        super();
    }
    public Student(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }
}




