package com.igeek.ch01.work5.test9;

public interface Player {
    void playBasketball();
    void playFootball();
    void playVolleyball();
}
