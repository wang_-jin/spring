package com.igeek.ch01.work5.test4;

/**
 * @author wangjin
 * 2023/8/28 19:36
 * @description TODO
 */
public class Car {
    boolean status;//true 表示运行 false 表示停止
    public class Engine{
        void work(){
            if (status) {
                System.out.println("发动机就飞速旋转");
            }else {
                System.out.println("发动机停止工作");
            }
        }
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
