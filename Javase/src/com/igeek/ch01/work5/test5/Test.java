package com.igeek.ch01.work5.test5;

/**
 * @author wangjin
 * 2023/8/21 11:44
 * @description TODO
 */
public class Test {

    static {
        System.out.println("Test 类的静态代码块");
    }

    public static void main(String[] args) {
        System.out.println("执行main方法");
//		创建学生对象
        Student s1 = new Student("main");
    }
}
            /*代码执行顺序:
                Test 类的静态代码块
                执行main方法
                Student 类的静态代码块
                在main方法中创建Student对象调用的
            */
//提示: 类只有被使用到的时候才会加载
