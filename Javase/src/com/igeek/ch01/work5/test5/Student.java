package com.igeek.ch01.work5.test5;

/**
 * @author wangjin
 * 2023/8/21 11:43
 * @description TODO
 */
//看下面的程序,说出的它的执行结果,以及执行流程
public class Student {
    static {
        System.out.println("Student 类的静态代码块");
    }
    public Student(){
        System.out.println("Student 类的构造方法");
    }
    public Student(String methodName){
        System.out.println("在"+methodName+"方法中创建Student对象调用的");
    }
}


