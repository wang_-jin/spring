package com.igeek.ch01.work5.test11;

/**
 * @author wangjin
 * 2023/8/21 11:49
 * @description TODO
请使用代码描述:
某人养了几只宠物,每个宠物都有名字,每一个宠物都会吃,但是每个宠物吃的东西都不一样;宠物1叫tom吃鱼,宠物2叫小黑,啃骨头.
要求: 用到抽象类和匿名内部类
 */
public class Test {
    public static void main(String[] args) {
        Animal animal1 = new Animal("Tom") {
            @Override
            void eat() {
                System.out.println(this.name+"吃鱼");
            }
        };
        Animal animal2 = new Animal("小黑") {
            @Override
            void eat() {
                System.out.println(this.name+"啃骨头");
            }
        };
        animal1.eat();
        animal2.eat();
    }
}
