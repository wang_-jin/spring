package com.igeek.ch01.work5.test10;

/**
 * @author wangjin
 * 2023/8/21 11:48
 * @description TODO
 */
//看下面的程序说出执行结果并阐述程序的执行流程
public class Student {
    static {
        System.out.println("Student 类的静态代码块");
    }
    public Student(){
        System.out.println("Student 类的构造方法");
    }
    public Student(String methodName){
        System.out.println("在"+methodName+"创建Student对象调用的");
    }
}

