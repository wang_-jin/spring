package com.igeek.ch01.work5.test10;

/**
 * @author wangjin
 * 2023/8/21 11:48
 * @description TODO
 */
public class Test {

    //	实例(对象)成员变量
    Student s = new Student("成员变量显式初始化的时候");
    //	静态代码块
    static {
        System.out.println("Test 类的静态代码块");
    }

    public static void main(String[] args) {
        System.out.println("main方法");
//		创建学生对象
        Student s1 = new Student("main方法中");
//		创建Test对象
        Test t = new Test();//只有创建对象才会为成员变量分配存储空间才能对成员变量赋值
    }
}
//提示: 1. 类只有被使用就会加载到内存中
//     2. 只有创建对象才会为成员变量分配存储空间才能对成员变量赋值
                    /* Test 类的静态代码块
                       main方法
                       Student 类的静态代码块
                       在main方法中创建Student对象调用的
                       在成员变量显式初始化的时候中创建Student对象调用的
                    */
