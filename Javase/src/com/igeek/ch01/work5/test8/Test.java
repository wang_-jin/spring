package com.igeek.ch01.work5.test8;

/**
 * @author wangjin
 * 2023/8/21 11:46
 * @description TODO
请用代码描述：
某市举办一场运动会,只要是会跑的都可以参加.
要求: 使用到接口,接口作为参数,匿名内部类.
提示: 把跑功能抽取到接口中 ,在测试类中提供参加运动会的静态方法
1. 定义接口(Sport)
a)	抽象方法: 跑(run)
2. 定义测试类Test
a)	定义静态方法:进入运行会(enter(Sport s)),在方法中调用s的run方法
b)	提供main方法
c)	在main方法中
i.	调用enter(Sport s),传入使用Sport接口的匿名内部类创建的对象;
ii.	在匿名内部类中重写run方法
1.	输出: 参加运动会,奔跑吧

 */
public class Test {
    static void enter(Sport s){
        s.run();
    }

    public static void main(String[] args) {
        Sport sport = new Sport() {
            @Override
            public void run() {
                System.out.println("参加运动会,奔跑吧");
            }
        };
        enter(sport);
    }
}
