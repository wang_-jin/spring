package com.igeek.ch01.enumeration;

/**
 * @author wangjin
 * 2023/8/20 15:28
 * @description TODO
定义一个Role类,有生命HP,攻击力ATK,防御了DEF三个属性,
定义一个有参构造方法,参数是一种职业,参数是枚举类型Career,
职业有战士,坦克,法师三种,三种职业的信息如下:
战士:
生命值:85
攻击力:83
防御力:82
坦克:
生命值:98,
攻击力:62,
防御力:94
法师:
生命值:65
攻击力:100
防御力:75
使用构造方法创建实例对象:new Role(职业) 会打印:
你选择了xx职业,生命值是xx,攻击力是xx,防御力是xx.
 */
public class Role {
    int HP;
    int ATK;
    int DEF;
    String role;
    public Role(Career career){
        switch (career){
            case ZHANSHI:
                this.role = "战士";
                this.HP = 85;
                this.ATK = 83;
                this.DEF = 82;
                break;
            case TANK:
                this.role = "坦克";
                this.HP = 98;
                this.ATK = 62;
                this.DEF = 94;
                break;
            case FASHI:
                this.role = "法师";
                this.HP = 65;
                this.ATK = 100;
                this.DEF = 75;
                break;
        }
    }

    public static void main(String[] args) {
        Role r1 = new Role(Career.TANK);
        System.out.println("你选择了"+r1.role+"职业,生命值是"+r1.HP+",攻击力是"+r1.ATK+",防御力是"+r1.DEF);
    }

}
