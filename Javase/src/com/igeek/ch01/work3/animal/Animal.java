package com.igeek.ch01.work3.animal;

/**
 * @author wangjin
 * 2023/8/19 9:08
 * @description TODO
 * i.	成员变量(私有):  颜色(color),腿的个数(numOfLegs)
 * ii.	抽象方法:  吃饭(void eat())
 * iii.	提供空参和带参构造方法
 * iv.	提供setXxx和getXxx方法
 */
public abstract class Animal {
    private String color;
    private int numOflegs;

    public Animal() {
    }

    public Animal(String color, int numOflegs) {
        this.color = color;
        this.numOflegs = numOflegs;
    }
    public abstract void eat();

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return numOflegs
     */
    public int getNumOflegs() {
        return numOflegs;
    }

    /**
     * 设置
     * @param numOflegs
     */
    public void setNumOflegs(int numOflegs) {
        this.numOflegs = numOflegs;
    }

    public String toString() {
        return "Animal{color = " + color + ", numOflegs = " + numOflegs + "}";
    }
}
