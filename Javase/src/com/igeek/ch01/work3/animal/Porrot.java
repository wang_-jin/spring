package com.igeek.ch01.work3.animal;

/**
 * @author wangjin
 * 2023/8/19 9:09
 * @description TODO
 * 1.	 重写父类的 eat()方法
 * a)	输出格式:: 2条腿的绿色鹦鹉在吃小米
 * 2.	 特有方法: say()  说明
 * a)	输出格式::  2条腿的绿色鹦鹉在说你好,丑八怪
 * ii.	提供空参和带参构造方法
 */
public class Porrot extends Animal{
    @Override
    public void eat() {
        System.out.println(this.getNumOflegs()+"条腿"+this.getColor()+"鹦鹉在吃小米");
    }
    public void say(){
        System.out.println(this.getNumOflegs()+"条腿"+this.getColor()+"鹦鹉在说你好,丑八怪");
    }

    public Porrot() {
    }

    public Porrot(String color, int numOflegs) {
        super(color, numOflegs);
    }
}
