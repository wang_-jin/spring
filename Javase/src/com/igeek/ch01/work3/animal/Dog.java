package com.igeek.ch01.work3.animal;

/**
 * @author wangjin
 * 2023/8/19 9:09
 * @description TODO
 * 1.	重写父类的 eat()方法
 * a)	输出格式::  4条腿黑色的狗在啃骨头
 * 2.	 特有方法:  看家(lookHome)
 * a)	输出格式::  4条腿黑色的狗在看家.
 * ii.	提供空参和带参构造方法
 */
public class Dog extends Animal{
    @Override
    public void eat() {
        System.out.println(this.getNumOflegs()+"条腿"+this.getColor()+"的狗在啃骨头");
    }
    public void lookHome(){
        System.out.println(this.getNumOflegs()+"条腿"+this.getColor()+"的狗在看家");
    }

    public Dog() {
    }

    public Dog(String color, int numOflegs) {
        super(color, numOflegs);
    }
}
