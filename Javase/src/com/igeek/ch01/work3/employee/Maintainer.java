package com.igeek.ch01.work3.employee;

/**
 * @author wangjin
 * 2023/8/19 10:25
 * @description TODO
 */
public class Maintainer extends Employee{
    @Override
    void work() {
        System.out.println("工号为 "+this.getId()+" 的维护专员 "+this.getName()+" 在解决不能共享屏幕问题");
    }

    public Maintainer() {
    }

    public Maintainer(int id, String name) {
        super(id, name);
    }
}
