package com.igeek.ch01.work3.employee;

/**
 * @author wangjin
 * 2023/8/19 10:25
 * @description TODO
 */
public class Tutor extends Employee{
    @Override
    void work() {
        System.out.println("工号为 "+this.getId()+" 的助教 "+this.getName()+" 在帮助学生解决问题");
    }

    public Tutor() {
    }

    public Tutor(int id, String name) {
        super(id, name);
    }
}
