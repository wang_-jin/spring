package com.igeek.ch01.work3.employee;

/**
 * @author wangjin
 * 2023/8/17 20:53
 * @description TODO
请使用代码描述
在极客营有很多员工(Employee),按照工作内容不同分教研部员工(Teacher)和行政部员工(AdminStaff)
教研部根据教学的方式不同又分为讲师(Lecturer)和助教(Tutor)
行政部根据负责事项不同,有分为维护专员(Maintainer),采购专员(Buyer)
公司的每一个员工都编号,姓名和其负责的工作
工作内容:
讲师: 工号为 666 的讲师 傅红雪 在讲课
助教: 工号为 668的助教 顾棋 在帮助学生解决问题
维护专员: 工号为 686 的维护专员 庖丁 在解决不能共享屏幕问题
采购专员: 工号为 888 的采购专员 景甜 在采购音响设备
提示: 参考今天的综合案例

 */
public class Test {
    public static void main(String[] args) {
        Lecturer l = new Lecturer(666,"傅红雪");
        l.work();
        Tutor t = new Tutor(668,"顾棋");
        t.work();
        Maintainer m = new Maintainer(686,"庖丁");
        m.work();
        Buyer b = new Buyer(888,"景甜");
        b.work();
    }
}
