package com.igeek.ch01.work3.employee;

/**
 * @author wangjin
 * 2023/8/19 10:25
 * @description TODO
 */
public class Lecturer extends Employee{
    @Override
    void work() {
        System.out.println("工号为 "+this.getId()+" 的讲师 "+this.getName()+" 在讲课");
    }

    public Lecturer() {
    }

    public Lecturer(int id, String name) {
        super(id, name);
    }
}
