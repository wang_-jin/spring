package com.igeek.ch01.work3.employee;

/**
 * @author wangjin
 * 2023/8/19 10:25
 * @description TODO
 */
public class Buyer extends Employee{
    @Override
    void work() {
        System.out.println("工号为 "+this.getId()+" 的采购专员 "+this.getName()+" 在采购音响设备");
    }

    public Buyer() {
    }

    public Buyer(int id, String name) {
        super(id, name);
    }
}
