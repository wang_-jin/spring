package com.igeek.ch01.work3.dog;

/**
 * @author wangjin
 * 2023/8/19 9:38
 * @description TODO
 */
public class Dog extends Animal{
    private String brand;
    public Dog() {
    }

    public Dog(String name, String color, String brand) {
        super(name, color);
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
