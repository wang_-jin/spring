package com.igeek.ch01.work3.girl;

/**
 * @author wangjin
 * 2023/8/19 10:37
 * @description TODO
 */
public class Clothes {
    private String color;
    private String brand;

    public Clothes() {
    }

    public Clothes(String color, String brand) {
        this.color = color;
        this.brand = brand;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * 设置
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String toString() {
        return "Clothes{color = " + color + ", brand = " + brand + "}";
    }
}
