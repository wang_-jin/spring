package com.igeek.ch01.work3.girl;

/**
 * @author wangjin
 * 2023/8/19 10:37
 * @description TODO
 */
public class Girl extends Person{
    private String face;
    private Boy boyFriend;

    public Girl(Boy boyFriend){
        this.boyFriend = boyFriend;
    }
    public Girl() {
    }

    public Girl(String name, int age, double height, String face) {
        super(name, age, height);
        this.face = face;
    }


    @Override
    public void say(String content) {
        System.out.println(this.getName()+"微笑着用于甜美的声音对"+boyFriend.getName()+"说:"+content);
    }
    public void wash(Clothes c){
        System.out.println(this.getName()+"在洗一件"+c.getColor()+"的"+c.getBrand()+"牌子的衣服");
    }

    /**
     * 获取
     * @return face
     */
    public String getFace() {
        return face;
    }

    /**
     * 设置
     * @param face
     */
    public void setFace(String face) {
        this.face = face;
    }

    public Boy getBoyFriend() {
        return boyFriend;
    }

    public void setBoyFriend(Boy boyFriend) {
        this.boyFriend = boyFriend;
    }

    public String toString() {
        return "Girl{face = " + face + "}";
    }
}
