package com.igeek.ch01.work3.actor;

/**
 * @author wangjin
 * 2023/8/19 8:54
 * @description TODO
 * 1.	重写父类的 eat()方法
 * a)	输出格式:  18岁的演员景甜在吃小龙虾
 * 2.	 特有方法:  跳舞(dance)
 * a)	输出格式:  18岁的演员景甜在跳白天鹅.
 */
public class Actor extends Person{
    @Override
    public void eat() {
        System.out.println(this.getAge()+"岁的演员"+this.getName()+"在吃小龙虾");
    }
    public void dance(){
        System.out.println(this.getAge()+"岁的演员"+this.getName()+"在跳白天鹅");
    }

    public Actor() {
    }

    public Actor(String name, int age) {
        super(name, age);
    }
}
