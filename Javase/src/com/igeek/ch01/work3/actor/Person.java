package com.igeek.ch01.work3.actor;

/**
 * @author wangjin
 * 2023/8/19 8:55
 * @description TODO
 */
public abstract class Person {
    private String name;
    private int age;

    public abstract void eat();

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
