package com.igeek.ch01.work3.actor;

/**
 * @author wangjin
 * 2023/8/19 8:55
 * @description TODO
 * 1.	 重写父类的 eat()方法
 * a)	输出格式: 30岁的歌手薛之谦在吃大闸蟹
 * 2.	 特有方法: sing()  唱歌
 * a)	输出格式:  30岁的歌手薛之谦在演唱丑八怪
 * ii.	提供空参和带参构造方法
 */
public class Singer extends Person{
    @Override
    public void eat() {
        System.out.println(this.getAge()+"岁的歌手"+this.getName()+"在吃大闸蟹");
    }
    public void sing(){
        System.out.println(this.getAge()+"岁的歌手"+this.getName()+"在演唱丑八怪");
    }

    public Singer() {
    }

    public Singer(String name, int age) {
        super(name, age);
    }
}
