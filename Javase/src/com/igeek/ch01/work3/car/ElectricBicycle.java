package com.igeek.ch01.work3.car;

/**
 * @author wangjin
 * 2023/8/19 9:32
 * @description TODO
 */
public class ElectricBicycle extends Car{
    @Override
    public void info() {
        System.out.println(this.getWheel()+"个轮子的"+this.getColor()+"电动自行车可以跑(骑着跑)和充电");
    }

    public ElectricBicycle() {
    }

    public ElectricBicycle(int wheel, String color) {
        super(wheel, color);
    }
}
