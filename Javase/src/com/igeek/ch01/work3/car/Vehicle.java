package com.igeek.ch01.work3.car;

/**
 * @author wangjin
 * 2023/8/19 9:32
 * @description TODO
 */
public class Vehicle extends Car{
    @Override
    public void info() {
        System.out.println(this.getWheel()+"个轮子的"+this.getColor()+"的汽车可以跑(开着跑)和加油");
    }

    public Vehicle() {
    }

    public Vehicle(int wheel, String color) {
        super(wheel, color);
    }
}
