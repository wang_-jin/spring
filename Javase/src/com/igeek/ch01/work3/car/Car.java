package com.igeek.ch01.work3.car;

/**
 * @author wangjin
 * 2023/8/19 9:30
 * @description TODO
 */
public abstract class Car {
    private int wheel;
    private String color;

    public abstract void info();

    public Car() {
    }

    public Car(int wheel, String color) {
        this.wheel = wheel;
        this.color = color;
    }

    /**
     * 获取
     * @return wheel
     */
    public int getWheel() {
        return wheel;
    }

    /**
     * 设置
     * @param wheel
     */
    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        return "Car{wheel = " + wheel + ", color = " + color + "}";
    }
}
