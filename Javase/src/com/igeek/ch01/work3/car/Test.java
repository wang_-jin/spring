package com.igeek.ch01.work3.car;

/**
 * @author wangjin
 * 2023/8/17 20:53
 * @description TODO
请使用代码描述:
4个轮子的白色的汽车可以跑(开着跑)和加油
2个轮子的红色电动自行车可以跑(骑着跑)和充电
要求: 把汽车和电动自行车的共性抽取到交通工具类中,使用抽象类和抽象方法


 */
public class Test {
    public static void main(String[] args) {
        Car c1 = new Vehicle(4,"白色");
        c1.info();
        Car c2 = new ElectricBicycle(2,"红色");
        c2.info();
    }
}
