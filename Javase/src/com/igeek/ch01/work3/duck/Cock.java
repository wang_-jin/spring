package com.igeek.ch01.work3.duck;

/**
 * @author wangjin
 * 2023/8/19 9:49
 * @description TODO
 */
public class Cock extends Poultry{
    public Cock() {
    }

    public Cock(String color, int age) {
        super(color, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getAge()+"岁的"+this.getColor()+"公鸡在啄米");
    }
    public void crow(){
        System.out.println(this.getAge()+"岁的"+this.getColor()+"公鸡在打鸣");
    }
}
