package com.igeek.ch01.work3.duck;

/**
 * @author wangjin
 * 2023/8/19 9:49
 * @description TODO
 */
public class Duck extends Poultry{
    public Duck() {
    }

    public Duck(String color, int age) {
        super(color, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getAge()+"岁的"+this.getColor()+"鸭子在吃鱼");
    }
    public void swim(){
        System.out.println(this.getAge()+"岁的"+this.getColor()+"鸭子在游泳");
    }
}
