package com.igeek.ch01.work3.duck;

/**
 * @author wangjin
 * 2023/8/19 9:48
 * @description TODO
 */
public class Poultry {
    private String color;
    private int age;

    public Poultry() {
    }

    public Poultry(String color, int age) {
        this.color = color;
        this.age = age;
    }
    public void eat(){
        System.out.println(this.age+"岁的"+this.color+"家禽在吃饭");
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Poultry{color = " + color + ", age = " + age + "}";
    }
}
