package com.igeek.ch01.work3.bear;

/**
 * @author wangjin
 * 2023/8/19 9:17
 * @description TODO
 */
public class Bear extends Animal{
    public Bear() {
    }

    public Bear(String color, int numOflegs) {
        super(color, numOflegs);
    }
    public void eat(String food,String object){
        System.out.println(this.getColor()+this.getNumOflegs()+"条腿的北极熊"+"会吃"+food+"和"+object);
    }

}
