package com.igeek.ch01.work3.bear;

/**
 * @author wangjin
 * 2023/8/19 9:17
 * @description TODO
 */
public class Animal {
    private String color;
    private int numOflegs;

    public Animal() {
    }

    public Animal(String color, int numOflegs) {
        this.color = color;
        this.numOflegs = numOflegs;
    }

    /**
     * 获取
     * @return color
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取
     * @return numOflegs
     */
    public int getNumOflegs() {
        return numOflegs;
    }

    /**
     * 设置
     * @param numOflegs
     */
    public void setNumOflegs(int numOflegs) {
        this.numOflegs = numOflegs;
    }

    public String toString() {
        return "Animal{color = " + color + ", numOflegs = " + numOflegs + "}";
    }
}
