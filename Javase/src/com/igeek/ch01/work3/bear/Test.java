package com.igeek.ch01.work3.bear;

/**
 * @author wangjin
 * 2023/8/17 20:53
 * @description TODO
请用代码描述:
白色四条腿的北极熊(Bear)会吃(吃蜂蜜)和抓鱼(catchFish)
黑色四条腿的大熊猫(Panda)会吃(吃竹子)和爬树(climbTree)
要求: 把北极熊和大熊猫的共性提取动物类(Animal)中,不使用抽象类

 */
public class Test {
    public static void main(String[] args) {
        Bear b = new Bear("白色",4);
        b.eat("蜂蜜","抓鱼");
        Panda p = new Panda("黑色",4);
        p.eat("竹子","爬树");
    }
}
