package com.igeek.ch01.work3.teacher;

/**
 * @author wangjin
 * 2023/8/19 11:16
 * @description TODO
 */
public class Teacher extends Person{
    private int salary;

    public Teacher() {
    }

    public Teacher(String name, int age, int salary) {
        super(name, age);
        this.salary = salary;
    }

    @Override
    public void eat() {
        System.out.println("工资为"+this.salary+"元的"+this.getAge()+"岁的"+this.getName()+"老师在吃工作餐");
    }
    //工资为8000元的30岁的王小平老师在讲课
    public void lecture(){
        System.out.println("工资为"+this.salary+"元的"+this.getAge()+"岁的"+this.getName()+"老师在讲课");
    }

    /**
     * 获取
     * @return salary
     */
    public int getSalary() {
        return salary;
    }

    /**
     * 设置
     * @param salary
     */
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String toString() {
        return "Teacher{salary = " + salary + "}";
    }
}
