package com.igeek.ch01.work3.teacher;

/**
 * @author wangjin
 * 2023/8/19 11:17
 * @description TODO
 */
public class Student extends Person{
    private int score;

    public Student() {
    }

    public Student(String name, int age, int score) {
        super(name, age);
        this.score = score;
    }
    //成绩为90分的15岁的李小乐学生在吃学生餐
    @Override
    public void eat() {
        System.out.println("成绩为"+this.score+"分的"+this.getAge()+"岁的"+this.getName()+"学生在吃学生餐");
    }
    //2.	 特有方法: study() 学习方法
    //a)	输出格式:: 成绩为90分的15岁的李小乐学生在学习
    public void study(){
        System.out.println("成绩为"+this.score+"分的"+this.getAge()+"岁的"+this.getName()+"学生在学习");
    }
    /**
     * 获取
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * 设置
     * @param score
     */
    public void setScore(int score) {
        this.score = score;
    }

    public String toString() {
        return "Student{score = " + score + "}";
    }
}
