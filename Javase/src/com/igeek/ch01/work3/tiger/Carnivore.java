package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:27
 * @description TODO
 */
public abstract class Carnivore extends Animal{
    public Carnivore() {
    }

    public Carnivore(int age, int numOfLegs) {
        super(age, numOfLegs);
    }


}
