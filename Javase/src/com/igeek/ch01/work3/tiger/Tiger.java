package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:28
 * @description TODO
 */
public class Tiger extends Carnivore{
    @Override
    void eat() {
        System.out.println("年龄为 "+this.getAge()+"的"+this.getNumOfLegs()+"条腿的老虎在吃羊");
    }

    public Tiger() {
    }

    public Tiger(int age, int numOfLegs) {
        super(age, numOfLegs);
    }
}
