package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:28
 * @description TODO
 */
public class Pecker extends Carnivore{
    @Override
    void eat() {
        System.out.println("年龄为 "+this.getAge()+"的"+this.getNumOfLegs()+"条腿的啄木鸟在吃昆虫");
    }

    public Pecker() {
    }

    public Pecker(int age, int numOfLegs) {
        super(age, numOfLegs);
    }
}
