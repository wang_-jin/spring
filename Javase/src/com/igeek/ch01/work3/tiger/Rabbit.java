package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:29
 * @description TODO
 */
public class Rabbit extends Herbivore{
    @Override
    void eat() {
        System.out.println("年龄为 "+this.getAge()+"的"+this.getNumOfLegs()+"条腿的兔子在吃狗尾草");
    }

    public Rabbit() {
    }

    public Rabbit(int age, int numOfLegs) {
        super(age, numOfLegs);
    }
}
