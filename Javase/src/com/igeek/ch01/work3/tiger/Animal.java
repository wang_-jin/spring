package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:27
 * @description TODO
 */
public abstract class Animal {
    private int age;
    private int numOfLegs;

    public Animal() {
    }

    public Animal(int age, int numOfLegs) {
        this.age = age;
        this.numOfLegs = numOfLegs;
    }

    abstract void eat();

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return numOfLegs
     */
    public int getNumOfLegs() {
        return numOfLegs;
    }

    /**
     * 设置
     * @param numOfLegs
     */
    public void setNumOfLegs(int numOfLegs) {
        this.numOfLegs = numOfLegs;
    }

    public String toString() {
        return "Animal{age = " + age + ", numOfLegs = " + numOfLegs + "}";
    }
}
