package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:29
 * @description TODO
 */
public class Giraffe extends Herbivore{
    @Override
    void eat() {
        System.out.println("年龄为 "+this.getAge()+"的"+this.getNumOfLegs()+"条腿的长颈鹿在吃树叶");
    }

    public Giraffe() {
    }

    public Giraffe(int age, int numOfLegs) {
        super(age, numOfLegs);
    }
}
