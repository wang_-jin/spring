package com.igeek.ch01.work3.tiger;

/**
 * @author wangjin
 * 2023/8/19 11:28
 * @description TODO
 */
public abstract class Herbivore extends Animal{
    public Herbivore() {
    }

    public Herbivore(int age, int numOfLegs) {
        super(age, numOfLegs);
    }
}
