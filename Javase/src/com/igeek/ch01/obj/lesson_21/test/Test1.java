package com.igeek.ch01.obj.lesson_21.test;
/*
1.求1~n的阶乘1*2*3*4*5*...*n
foo(1)=1;
foo(2)=foo(1)*2;
foo(3)=foo(2)*3;
...
递归公式:
  foo(n)=foo(n-1)*n
终结条件:
  foo(1)=1;
*/

public class Test1 {
  public static int foo(int n){
    if(n==1)return 1;
    return foo(n-1)*n;
  }

  public static void main(String[] args) {
    System.out.println(foo(4));
  }
}
