package com.igeek.ch01.obj.lesson_21;

public class Tool1 {
  /*
  求1-10的和
  sum(1)=1;
  sum(2)=sum(1)+2;
  sum(3)=sum(2)+3;
  ...
  sum(n)=sum(n-1)+n
  递归公式:
    sum(n)=sum(n-1)+n
  终止条件:
    sum(1)=1
  */
  static int sum(int n){
    //终止条件,n=1返回1
    if(n==1)return 1;
    //代入递归公式
    return sum(n-1)+n;
  }
  public static void main(String[] args) {
    System.out.println(sum(10));
  }
}
