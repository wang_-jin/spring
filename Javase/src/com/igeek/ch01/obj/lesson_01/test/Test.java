package com.igeek.ch01.obj.lesson_01.test;

public class Test {
  public static void main(String[] args) {
    Phone p = new Phone();
    p.brand = "华为";
    p.price = 4500;
    p.color = "紫色";
    p.call();
    p.sendMessage();
    System.out.println("brand:"+p.brand);
    System.out.println("price:"+p.price);
    System.out.println("color:"+p.color);
  }
}
