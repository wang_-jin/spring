package com.igeek.ch01.obj.lesson_01.test;
/*
练习:创建一个手机类：
成员变量：品牌,价格,颜色
成员方法：打电话,发短信
并创建实例对象给属性赋值,调用方法
*/
public class Phone {
  String brand;
  int price;
  String color;
  void call(){
    System.out.println("打电话");
  };
  void sendMessage(){
    System.out.println("发信息");
  }
}
