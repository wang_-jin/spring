package com.igeek.ch01.obj.lesson_01;
/*
创建了一个Person类
public:
  这个public是一个访问权限修饰符,表示其他所有的类
  都可以使用当前类

注意:
  java中类名首字母需要大写

类的组成:
  属性(也叫成员变量):
    属性直接定义在类的花括号中,
    不能定义在方法法中,定义在方法中的变量叫局部变量

    属性是可以不赋初始值的,java会自动给他添加一个默认值
  方法(成员方法):
    方法也是直接定义在类的花括号中,不能定义在其他方法的
    方法体中
*/
public class Person {
  /* 定义了三个属性 */
  String name;
  int age;
  int height;

  /*定义了两个方法*/
  void eat(){
    System.out.println("吃饭");
  }
  void drink(){
    System.out.println("喝水");
  }

  public static void main(String[] args) {
    //局部变量是必须赋初始值的
    int a=1;
    System.out.println("a:"+a);
    /*
    创建一个Person类的实例对象:
      Person()这个叫无参构造方法,在类中会自动生成,
      他的作用就是用来创建实例对象的!!!
      注意:
        构造方法的名字和类名是一样的!

      new Person()作用:
        创建一个内存空间,用来存储创建的实例对象!!!
    */
    Person p = new Person();
    //给属性赋值:对象.属性名 = 属性值;
    p.name = "刘备";
    p.age = 18;
    p.height = 170;
    //获取属性值:对象.属性名
    System.out.println("name:"+p.name);
    System.out.println("age:"+p.age);
    System.out.println("height:"+p.height);
    /*调用eat方法*/
    p.eat();
    /* 调用drink方法 */
    p.drink();
  }
}
