package com.igeek.ch01.obj.lesson_01;

public class ClassCompareMethod {
  /*
  类体里面主要能写的东西只有两个:
    属性和方法,像if语句这种代码是不能
    写在类体里面的,会报错
  */
  String name;
  void eat(){

  }
  public static void main(String[] args) {
    /*
    方法体里面可以写的东西比类体要多很多,除了属性和方法不能
    在方法体里面定义,其他的语法代码基本上都可以在里面写!
    */
  }
}
