package com.igeek.ch01.obj.lesson_17_2.test1;

/*
注意:
  当一个类同时有extends和implements关键字的时候,extends继承需要
  在左边
*/
public class Bird extends Animal implements Drink{
  @Override
  public void drink() {
    System.out.println("喝水");
  }
}
