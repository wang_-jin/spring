package com.igeek.ch01.obj.lesson_17_2.test;

public class Bird implements Exercise{
  @Override
  public void fly() {
    System.out.println("鸟在飞");
  }

  @Override
  public void run() {
    System.out.println("鸟在跑");
  }
}
