package com.igeek.ch01.obj.lesson_17_2.test;

public class Test {
  public static void main(String[] args) {
    Dog dog = new Dog();
    dog.run();
    Bird bird = new Bird();
    bird.fly();
    bird.run();
  }
}
