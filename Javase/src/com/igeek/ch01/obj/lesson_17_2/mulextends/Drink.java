package com.igeek.ch01.obj.lesson_17_2.mulextends;

public interface Drink {
  public abstract void drink();
}
