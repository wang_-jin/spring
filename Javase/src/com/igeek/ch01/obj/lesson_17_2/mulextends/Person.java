package com.igeek.ch01.obj.lesson_17_2.mulextends;

public class Person implements Basic{

  @Override
  public void drink() {
    System.out.println("喝水");
  }

  @Override
  public void eat() {
    System.out.println("吃饭");
  }
}
