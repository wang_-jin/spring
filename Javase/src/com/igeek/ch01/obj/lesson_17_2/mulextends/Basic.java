package com.igeek.ch01.obj.lesson_17_2.mulextends;

/*
接口可以多继承,使用extends关键字,多个接口之间使用逗号隔开
*/
public interface Basic extends Drink,Eat{

}
