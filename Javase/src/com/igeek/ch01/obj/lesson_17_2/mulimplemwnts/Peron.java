package com.igeek.ch01.obj.lesson_17_2.mulimplemwnts;
/*
类可以实现多个接口,类实现接口使用关键字implements,多个
接口之间用逗号隔开
*/
public class Peron implements Drink,Eat{
  @Override
  public void drink() {
    System.out.println("喝水");
  }

  @Override
  public void eat() {
    System.out.println("吃饭");
  }
}
