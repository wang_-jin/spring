package com.igeek.ch01.obj.lesson_10.test3;

public class Bird extends Animal{
  int flySpeed;
  public Bird(String name,int age,int flySpeed){
    super(name,age);
    this.flySpeed = flySpeed;
  }
  @Override
  public void eat() {
    System.out.println(this.name+"吃了谷子");
  }
}
