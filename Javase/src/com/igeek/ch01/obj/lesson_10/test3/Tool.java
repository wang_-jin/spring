package com.igeek.ch01.obj.lesson_10.test3;

public class Tool {
  public static void main(String[] args) {
    Animal dog = new Dog("旺财",3,"黑色");
    dog.eat();
    Animal bird = new Bird("凤凰",2,100);
    bird.eat();
  }
}
