package com.igeek.ch01.obj.lesson_10.test3;

public class Dog extends Animal{
  String color;
  public Dog(String name,int age,String color){
    super(name,age);
    this.color = color;
  }
  @Override
  public void eat() {
      System.out.println(this.name+"吃了骨头");
  }
}
