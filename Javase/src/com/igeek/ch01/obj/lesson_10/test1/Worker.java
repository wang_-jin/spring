package com.igeek.ch01.obj.lesson_10.test1;

public class Worker extends Person{
  public Worker(String name,int age){
    this.name = name;
    this.age = age;
  }
  @Override
  public void study() {
    System.out.println("工人"+this.name+"正在学习技能");
  }
}
