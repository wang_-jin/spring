package com.igeek.ch01.obj.lesson_10.test1;

public class Student extends Person{
  /*
  ctrl+o(override)重写的快捷键
  */
  public Student(String name,int age){
    this.name = name;
    this.age = age;
  }
  @Override
  public void study() {
    System.out.println("学生"+this.name+"正在学习知识");
  }

}
