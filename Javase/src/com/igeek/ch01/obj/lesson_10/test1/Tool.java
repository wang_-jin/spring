package com.igeek.ch01.obj.lesson_10.test1;

public class Tool {

  public static void main(String[] args) {
    /*
    重写的作用:
      让同一个方法在不同的子类上有不同的实现逻辑
    */
    Student s = new Student("小明",15);
    s.study();

    Worker w = new Worker("张三",23);
    w.study();
  }
}
