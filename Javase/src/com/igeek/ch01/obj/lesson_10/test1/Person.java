package com.igeek.ch01.obj.lesson_10.test1;
/*
1.创建一个"人"的类,有姓名和年龄,有一个学习方法,
有学生和工人继承这个人类,他们也有姓名和年龄.
学生有自己的学习方法,工人有自己的学习方法.
现在有个学生叫张三,22岁,正在学习,调用学习方法打印:学生张三正在学习知识
有个工人叫李四,25岁,正在学习,调用学习方法打印:工人李四正在学习技能
*/
public class Person {
  String name;
  int age;
  public void study(){

  }
}
