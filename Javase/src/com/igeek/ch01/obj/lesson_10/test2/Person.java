package com.igeek.ch01.obj.lesson_10.test2;
/*
2.写一个类Person,包含以下属性：
String  name;
int  age;
boolean  gender;  //性别  true男  false女
为Person类写一个marry(Person p)方法,代表当前对象和p结婚,
如若可以结婚,则打印恭贺新婚,
否则输出不能结婚原因.要求在main方法中测试以上程序.
下列情况不能结婚:
1.同性,打印同性不能结婚;
2.未达到结婚年龄,男<24,女<22,打印未达到结婚年龄;
*/
public class Person {
  String  name;
  int  age;
  boolean  gender;
  public Person(String name,int age,boolean gender){
    this.name = name;
    this.age = age;
    this.gender = gender;
  }
  public void marry(Person p){
    //同姓不能结婚
    if(this.gender == p.gender){
      System.out.println("同性不能结婚");
    }else{
      //异性
      //当前对象是男性
      if(this.gender){
        if(this.age>=24&&p.age>=22){
          System.out.println("恭贺新婚");
        }else{
          System.out.println("未达到结婚年龄");
        }
      }else{
        //当前对象是女性
        if(this.age>=22&&p.age>=24){
          System.out.println("恭贺新婚");
        }else{
          System.out.println("未达到结婚年龄");
        }
      }
    }
  }
}
