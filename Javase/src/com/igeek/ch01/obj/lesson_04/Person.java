package com.igeek.ch01.obj.lesson_04;
/*
构造方法的作用:
  构造方法可以用来创建实例对象;也可以创建实例对象并给实例对象的属性赋值;

构造方法的特点:
  1.构造方法的名字和类名是相同的;
  2.构造方法是没有void或其他类型返回值的;
  3.如果类里面没有定义任何构造方法,编译器会自动添加一个无参的构造方法;
  4.如果类里面定义了有参构造方法,但是没有定义无参构造方法,编译器是不会
  自动创建无参构造方法的!!!

无参构造和有参构造方法的区别:
  有参构造方法可以创建实例对象并给实例对象的属性赋值,而无参构造方法只能
  创建实例对象.
*/
public class Person {
  String name;
  int age;
  /* 无参构造方法 */
  public Person(){

  }

  /* 有参构造方法 */
  public Person(String name,int age){
    //给属性赋值
    this.name = name;
    //p1.name = "关羽";
    this.age = age;
    //p1.age=17;
  }

  public static void main(String[] args) {
    Person p = new Person();
    p.name = "刘备";
    p.age = 18;
    System.out.println("p.name:"+p.name);
    System.out.println("p.age:"+p.age);

    Person p1 = new Person("关羽",17);
  }
}
