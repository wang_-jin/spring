package com.igeek.ch01.obj.lesson_04.test;
/*
创建一个Dog类,有姓名,年龄,颜色三个属性,有一个eat方法,参数是食物String类型,
执行eat会打印"X色的小狗XX正在吃XX".创建2个构造方法,一个无参构造方法,一个
包含3个参数的有参构造方法,使用有参构造创建对象并执行测试!
*/
public class Dog {
  String name;
  int age;
  String color;
  void eat(String food){
    System.out.println(this.color+"色的小狗"+this.name+"正在吃"+food);
  }
  //无参构造方法
  public Dog(){};
  //有参构造方法
  public Dog(String name){
    this.name = name;
  }
  public Dog(String name,int age,String color){
    this.name = name;
    this.age = age;
    this.color = color;
  }

  public static void main(String[] args) {
    Dog dog = new Dog("旺财",3,"黑");
    dog.eat("骨头");
    System.out.println("name:"+dog.name);
    System.out.println("age:"+dog.age);
    System.out.println("color:"+dog.color);
  }
}
