package com.igeek.ch01.obj.lesson_17_1;

/*
定义了一个接口,接口的关键字interface
*/
public interface Master {
  /*
  接口中的变量默认被 public static final修饰,
  即使你不写这三个关键字,接口也会自动添加
  */
  //车厢宽度
  int width = 100;
  /*
  接口中所有方法默认都是抽象方法,不管你有没有写abstract关键字,
  接口中方法都是抽象的,因为接口会默认在方法左边添加 public abstract
  这两个关键字
  */
  //加水箱
  void addWaterBox();
  //扩容
  void expandCarBox();

}
