package com.igeek.ch01.obj.lesson_17_1;
/*
一个类实现另一个接口我们用关键字implements(实现),
这个类我们叫接口的实现类(也可以叫子实现类),接口
的实现类必须重写接口中所有的抽象方法!
*/
public class ModiCar implements Master {

  @Override
  public void addWaterBox() {
    System.out.println("加水箱");
  }

  @Override
  public void expandCarBox() {
    System.out.println("扩展车的容量");
  }

  public static void main(String[] args) {
    ModiCar mc = new ModiCar();
    mc.addWaterBox();
    mc.expandCarBox();
    //不能修改接口中的的常量,因为它被final修饰
    //mc.width = 120;
  }
}
