package com.igeek.ch01.obj.lesson_05.publicc.other;

import com.igeek.ch01.obj.lesson_05.publicc.Person;

public class Tool extends Person {
  public static void main(String[] args) {
    Person p = new Person("刘备");
    System.out.println(p.name);

    Tool t = new Tool();
    System.out.println(t.name);
  }
}
