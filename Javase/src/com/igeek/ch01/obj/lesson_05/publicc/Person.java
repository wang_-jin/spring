package com.igeek.ch01.obj.lesson_05.publicc;

public class Person {
  /*
  public修饰的成员属性或方法可以被任何位置的类访问到!!!
  */
  public String name;
  public Person(){};
  public Person(String name){
    this.name = name;
  }

}
