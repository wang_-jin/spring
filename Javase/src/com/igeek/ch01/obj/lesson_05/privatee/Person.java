package com.igeek.ch01.obj.lesson_05.privatee;

public class Person {
  /*
  private访问权限修饰符修饰的成员属性或方法只能在
  本类中被访问!
  */
  private String name;
  public Person(){};
  public Person(String name){
    this.name = name;
  }
}
