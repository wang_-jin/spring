package com.igeek.ch01.obj.lesson_05.protectedd.other;
import com.igeek.ch01.obj.lesson_05.protectedd.Person;

/*
extends左边的类是子类,右边的类是父类
*/
public class Tool extends Person {
  public static void main(String[] args) {
    Person p = new Person("刘备");
    //protected修饰的属性在其他包中的子类里面,使用属性的本类去访问属性,仍然是访问不到的
    //System.out.println(p.name);

    Tool t = new Tool();
    /*
    protected访问权限修饰符修饰的成员变量,可以被其他包中的
    子类的实例对象访问到
    */
    System.out.println(t.name);
  }
}
