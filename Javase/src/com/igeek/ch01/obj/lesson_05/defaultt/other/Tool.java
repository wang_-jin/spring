package com.igeek.ch01.obj.lesson_05.defaultt.other;

import com.igeek.ch01.obj.lesson_05.defaultt.Person;

public class Tool extends Person {
  public static void main(String[] args) {
    Person p = new Person("刘备");
    //name是默认访问权限,是不能被其他包中的任何类访问到的
    //System.out.println(p.name);

    Tool t = new Tool();
    //子类也访问不到默认权限的成员变量
    //System.out.println(t.name);
  }
}
