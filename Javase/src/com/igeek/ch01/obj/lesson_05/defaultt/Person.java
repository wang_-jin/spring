package com.igeek.ch01.obj.lesson_05.defaultt;

public class Person {
  /*
  成员变量或成员方法没有使用任何访问权限修饰,可以理解成使用了
  默认的访问权限符去修饰了,默认访问权限符修饰的变量可以在本类
  或本包被访问,不能被其他包中的类访问!
  */
  String name;
  public Person(){};

  public Person(String name){
    this.name = name;
  }

  public static void main(String[] args) {
    Person p = new Person("刘备");
    System.out.println(p.name);
  }
}
