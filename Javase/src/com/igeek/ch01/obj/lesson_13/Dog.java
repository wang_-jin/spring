package com.igeek.ch01.obj.lesson_13;

public class Dog extends Animal {
  @Override
  void eat() {
    System.out.println("狗吃骨头");
  }

  @Override
  void drink() {
    System.out.println("狗喝水");
  }

  public static void main(String[] args) {
    Dog dog = new Dog();
    dog.eat();
    dog.drink();
    dog.print();
    System.out.println("age:"+dog.age);
  }
}
