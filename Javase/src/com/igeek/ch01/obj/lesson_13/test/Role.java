package com.igeek.ch01.obj.lesson_13.test;
/*
定义一个角色类Role是抽象类,
其中的攻击方法attack是抽象方法,
英雄类Hero和怪兽类Monster继承Role类,这两个类都有一个属性name,
重写attack攻击方法,Hero类中方法attack打印:英雄xx攻击了,
Monster类中attack方法打印:怪兽xx攻击了
*/
public abstract class Role {
  String name;
  abstract void attack();
}
