package com.igeek.ch01.obj.lesson_13.test;

public class Tool {
  public static void main(String[] args) {
    Hero hero = new Hero("迪迦");
    hero.attack();
    Monster monster = new Monster("哥斯拉");
    monster.attack();
  }
}
