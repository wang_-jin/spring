package com.igeek.ch01.obj.lesson_13.test;

public class Monster extends Role{

  public Monster() {
  }
  public Monster(String name) {
    this.name = name;
  }

  @Override
  void attack() {
    System.out.println("怪物"+this.name+"攻击了");
  }
}
