package com.igeek.ch01.obj.lesson_13.test;

public class Hero extends Role {

  public Hero() {
  }
  public Hero(String name) {
    this.name = name;
  }

  @Override
  void attack() {
    System.out.println("英雄"+this.name+"攻击了");
  }
}
