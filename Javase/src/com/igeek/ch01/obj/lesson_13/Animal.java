package com.igeek.ch01.obj.lesson_13;
/*
定义一个抽象类:
  1.抽象类是不能被实例化的
  2.抽象类只能做父类,抽象类的子类必须实现所有
  父类中的抽象方法,否则子类也要变为抽象类
  3.抽象类中可以没有抽象方法,但是抽象方法一定要定义在
  抽象类中
  4.抽象类中也是可以定义成员变量的
*/
public abstract class Animal {
  int age = 18;
  /*
  定义了一个抽象方法:
    1.抽象方法是指只有方法声明,没有方法实现(没有方法体)的方法!
    2.抽象方法只能在抽象类中定义
  */
  abstract void eat();
  abstract void drink();
  void print(){
    System.out.println("我是抽象父类");
  }
  public static void main(String[] args) {
    //抽象类不能被实例化
    //Animal ani = new Animal();
  }
}
