package com.igeek.ch01.obj.lesson_16.exam1;

public class Tool {
  public static void main(String[] args) {
    Animal a = new Dog();
    a.eat();

    Animal b = new Cat();
    b.eat();
  }
}
