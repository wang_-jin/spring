package com.igeek.ch01.obj.lesson_16.exam2;

public class Tool1 {
  //多态参数
  static void print(Animal a){
    // a = dog;(伪代码)
    a.eat();
  }

  public static void main(String[] args) {
    Dog dog = new Dog();
    print(dog);

    Cat cat = new Cat();
    print(cat);

    Bird bird = new Bird();
    print(bird);
  }
}
