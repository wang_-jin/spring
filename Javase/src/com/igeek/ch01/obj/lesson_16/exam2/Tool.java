package com.igeek.ch01.obj.lesson_16.exam2;


public class Tool {
  static void print(Dog dog){
    dog.eat();
  }
  static void print(Cat cat){
    cat.eat();
  }

  static void print(Bird bird){
    bird.eat();
  }

  public static void main(String[] args) {
    Dog dog = new Dog();
    print(dog);
    Cat cat = new Cat();
    print(cat);

    Bird bird = new Bird();
    print(bird);
  }
}
