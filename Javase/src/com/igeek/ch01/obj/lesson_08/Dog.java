package com.igeek.ch01.obj.lesson_08;

public class Dog extends Animal{
  public Dog(){
    /*
    如果你没有在子类的构造里面写super(),
    子类的构造方法里面第一行会默认添加一个super()方法;

    注意点:
      1.super()方法必须写在子类构造方法的第一行
      2.如果父类里面定义了有参构造,但是没有定义无参构造,
      那么在子类的构造方法里面可能会报错,因为子类的构造方法的
      第一行默认会添加一个super(),super()方法的作用是执行
      父类的无参构造,因为父类显示定义了有参构造,不会自动添加
      无参构造,父类中是没有无参构造的,子类通过super()调用
      父类的无参构造会报错!!!

    子类如何寻找属性使用:
    先从本类中去寻找属性,如果找到了直接使用,如果没找到再去父类
    寻找属性,如果找到了直接使用,否则继续往上层寻找,一直找到Object,
    如果都没找到则会报错

    ctrl+h(hierarchy层次结构):可以查看一个类的继承关系
    */
    super();//执行父类的无参构造
    System.out.println("Dog()");
  }

  public Dog(String name,int age){
    super(name,age);//执行父类的有参构造,给子类的属性赋初始值
    //上面的代码就相当于下面的代码,作用是给子类的属性赋初始值
    //this.name = name
    //this.age = age;
  }

  public static void main(String[] args) {
    //Dog dog = new Dog();
    Dog dog1 = new Dog("旺财",3);
    System.out.println("dog1:"+dog1);
    System.out.println(dog1.name);
    System.out.println(dog1.age);
    color="白色";
    System.out.println(color);
    Dog.eat();
    dog1.play();
  }
}
