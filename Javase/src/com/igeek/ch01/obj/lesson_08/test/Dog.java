package com.igeek.ch01.obj.lesson_08.test;
/*
对象使用属性或方法时,当它本身没有这个属性或方法时,会顺着继承链往上面的类寻找,
如果找到了就直接用,如果找不到继续顺着继承链往更高层去找,一直找到Object

如何查看继承链:
  1.ctrl+鼠标左键 点击父类名去类里面查看是否有子类需要的属性
  2.ctrl+h(hierarchy层次)可以查看继承树的层次关系
*/
public class Dog extends Pet{
  String category;
  public Dog(String name,String category,String color){
    this.name = name;
    this.category = category;
    this.color = color;
  }
  public void info(){
    System.out.println(this.name+"是一只"+this.category+"它的颜色是"+this.color);
  }

  public static void main(String[] args) {
    Dog dog =new Dog("旺财","泰迪","黑色");
    dog.info();
  }
}
