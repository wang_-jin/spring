package com.igeek.ch01.obj.lesson_08.test;
/*
有一个Animal类,他有一个姓名属性;有一个Pet类继承自Animal类,
他有一个颜色属性;有一个Dog类继承自Pet,他有一个品种属性;Dog类下面
还有一个info方法,执行后打印"XX(写姓名)是一只XX(写品种)它的颜色是XX(写颜色)",
在Dog类中创建一个有3个参数的有参构造,使用这个有参构造创建Dog的实例对象,并执行
info方法!
*/
public class Animal {
  String name;
}
