package com.igeek.ch01.obj.lesson_08;

public class Animal {
  static String color;
  String name;
  int age;
  public Animal(){
    System.out.println("Animal()");
  }
  static void eat(){
    System.out.println("好吃");
  }
  void play(){
    System.out.println("好玩");
  }

  public Animal(String name,int age){
    this.name = name;
    this.age = age;
    System.out.println("父类的有参构造中this:"+this);
  }
}
