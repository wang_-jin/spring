package com.igeek.ch01.obj.lesson_07;
/*
继承关键字extends
继承语法:
  public class 子类名 extends 父类名{}
  extends关键字左边是子类,右边是父类

继承的作用:
  子类使用extends关键字继承父类后,就可以使用父类中的
  属性和方法了,即使子类中没有定义父类的这些属性和方法仍然
  是可以使用的;
  继承后子类仍然可以有自己的属性(继承后子类仍然可以扩展父类没有的属性);

总结:
  继承可以让子类使用父类的属性和方法,同时子类仍然可以扩展自己的
  属性和方法!!!
*/
public class Son extends Father {
  String play;
  public Son(){};
  public static void main(String[] args) {
    Son s = new Son();
    s.money = "100万";
    s.car = "奥迪";
    //子类扩展的play属性,父类是没有这个属性的
    s.play = "唱歌";
    s.eat();
    System.out.println("s.play:"+s.play);
    System.out.println("s.money:"+s.money);
    System.out.println("s.car:"+s.car);

    Father f = new Father();
    //注意:继承之后子类可以使用父类的属性和方法,但父类不可以使用子类的属性和方法
    //System.out.println(f.play);


  }
}
