package com.igeek.ch01.obj.lesson_18.mulextends;

public class Tool {
  public static void main(String[] args) {
    Dog dog = new Dog();
    Dog.Teddy teddy = dog.new Teddy();//成员内部类
    //Dog.Teddy teddy = new Dog.Teddy();//静态内部类
    teddy.eat();
    teddy.drink();
  }
}
