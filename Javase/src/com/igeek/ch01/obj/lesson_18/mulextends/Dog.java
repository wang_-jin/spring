package com.igeek.ch01.obj.lesson_18.mulextends;

public class Dog {
  /*
  使用内部类结合接口的实现可以模拟类的多继承
  */
  public class Teddy implements Animal,Pet{
    @Override
    public void eat() {
      System.out.println("狗吃骨头");
    }

    @Override
    public void drink() {
      System.out.println("狗喝水");
    }
  }
}
