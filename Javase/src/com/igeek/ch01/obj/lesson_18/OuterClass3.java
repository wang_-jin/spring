package com.igeek.ch01.obj.lesson_18;

public class OuterClass3 {
  public static void main(String[] args) {
    /*
    创建一个局部类:
      在方法里面定义的类我们叫局部类
    */
    class Inner{
      void eat(){
        System.out.println("吃饭");
      }
    }
    class Inner1{
      void drink(){
        System.out.println("喝水");
      }
    }
    Inner inner = new Inner();
    inner.eat();
    Inner1 inner1 = new Inner1();
    inner1.drink();
  }
}
