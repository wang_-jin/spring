package com.igeek.ch01.obj.lesson_18;

public class OuterClass2 {
  /*
  创建一个静态内部类
  */
  public static class Inner{
    void eat(){
      System.out.println("吃饭");
    }
  }
  public static void main(String[] args) {
    /*
    创建一个静态内部类的实例对象
    */
    Inner inner = new Inner();
    inner.eat();
  }
}
