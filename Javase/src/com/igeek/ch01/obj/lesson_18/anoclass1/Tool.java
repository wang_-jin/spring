package com.igeek.ch01.obj.lesson_18.anoclass1;


public class Tool {
  public static void main(String[] args) {
    /*
    创建一个匿名的内部类,他的作用相当于是:
      创建了一个匿名的类实现了Animal接口,并且用这个匿名的类创建了
      一个实例对象,下面几行代码的作用:
        就相当于创建了一个接口的实现类并创建实例对象!
    */
    Animal ani = new Animal() {
      @Override
      public void eat() {
        System.out.println("狗吃骨头");
      }
    };

  }

}
