package com.igeek.ch01.obj.lesson_18.test;

public class Test {
  public static void main(String[] args) {
    /*
    匿名内部类
    */
    Animal ani = new Animal() {
      @Override
      public void drink() {
        System.out.println("喝水");
      }

      @Override
      public void eat() {
        System.out.println("吃饭");
      }
    };
    ani.drink();
    ani.eat();
  }
}
