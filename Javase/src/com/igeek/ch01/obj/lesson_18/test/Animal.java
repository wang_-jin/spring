package com.igeek.ch01.obj.lesson_18.test;

public interface Animal {
  void drink();
  void eat();
}
