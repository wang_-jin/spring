package com.igeek.ch01.obj.lesson_18;

public class OuterClass1 {
  /*
  成员内部类
  */
  public class Inner{
    void eat(){
      System.out.println("吃饭");
    }
  }
  public class Inner1{
    void drink(){
      System.out.println("喝水");
    }
  }

  public static void main(String[] args) {
    //创建外部类的实例对象
    OuterClass1 outer = new OuterClass1();
    //创建内部类的实例对象
    Inner inner = outer.new Inner();
    inner.eat();
    Inner1 inner1 = outer.new Inner1();
    inner1.drink();
  }
}
