package com.igeek.ch01.obj.lesson_09;

public class Dog extends Animal{
  /*
  @Override这个是重写的一个注解,
  注解的作用是规范代码的书写,也就是说这里使用了@Override重写注解,
  那么你下面代方法就要严格按照重写的规则去定义,否则编译器会给你提示报错,
  注意:
    注解并非一定要写,他只是起到一个协助你检查代码的功能,你不写也不会
    对你的代码逻辑产生任何影响,他只是辅助检查代码的作用!

  子类是一种特殊的父类,因为父类拥有的属性方法,子类都能用
  */
  @Override
  public void eat(String food){
    System.out.println("狗在吃骨头");
  }

  public static void main(String[] args) {
    Dog dog = new Dog();
    dog.eat("");
  }
}
