package com.igeek.ch01.obj.lesson_06;

public class Person {
  /*
  属性封装:将属性设为private私有的,提供getter和setter方法供外部访问
  */
  private String name;
  private int age;
  public Person(){};
  public Person(String name,int age){
    this.name = name;
    this.age = age;
  }

  /*
  修改属性的方法我们一般统称为setter方法
  创建setter方法的注意点:
    1.使用公开修饰符public修饰setter方法;
    2.setter方法名命名规则:
      set+属性名,如setName,setAge
  */
  public void setName(String name){
    this.name = name;
  }
  /*
  获取属性的方法我们一般统称为getter方法
  创建getter方法的注意点:
    1.使用公开修饰符public修饰getter方法;
    2.getter方法名命名规则:
      get+属性名,如getName,getAge
  */
  public String getName(){
    return this.name;
  }

  public void setAge(int age){
    this.age = age;
  }
  public int getAge(){
    return this.age;
  }

  public static void main(String[] args) {

  }
}
