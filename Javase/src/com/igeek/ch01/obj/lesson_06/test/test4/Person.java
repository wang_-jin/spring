package com.igeek.ch01.obj.lesson_06.test.test4;
/*
第四题
定义一个Person类,有姓名(String类型),年龄(int类型),宠物狗(Dog类)三个属性,有一个遛狗方法,
执行遛狗方法会显示"xx(写人的姓名)带着他的xx(狗的颜色)的宠物狗xx(写狗的名字)遛了一圈";
定义一个Dog类,有姓名(String类型),颜色(String类型)两个属性;
创建一个Person的实例对象,执行遛狗方法!
*/
public class Person {
  String name;
  int age;
  //某个类作为属性,属性的值应该是类的实例对象
  Dog dog;
  public Person(){

  }
  public Person(String name,int age,Dog dog){
    this.name = name;
    this.age = age;
    this.dog = dog;
  }
  void walkTheDog(){
    System.out.println(this.name+"带着他的"+this.dog.color+"的宠物狗"+this.dog.name+"遛了一圈");
  }
  public static void main(String[] args) {
    Dog d = new Dog("旺财","黑色");
    Person p = new Person("张三",18,d);
    p.walkTheDog();
  }
}
