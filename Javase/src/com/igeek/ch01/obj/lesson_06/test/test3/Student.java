package com.igeek.ch01.obj.lesson_06.test.test3;
/*
第三题
封装一个学生类,里面有姓名,年龄,性别3个属性,
提供一个方法用来显示所有属性,一个方法用来学习(并显示谁正在学习),
私有化这些属性并提供对应的get以及set方法并在测试类中测试
*/
public class Student {
  String name;
  int age;
  String sex;
  public Student(){};
  public Student(String name,int age,String sex){
    this.name = name;
    this.age = age;
    this.sex = sex;
  }
  public void setName(String name){
    this.name = name;
  }
  public String getName(){
    return this.name;
  }

  public void setAge(int age){
    this.age= age;
  }
  public int getAge(){
    return this.age;
  }

  public void setSex(String sex){
    this.sex = sex;
  }
  public String getSex(){
    return this.sex;
  }

  public void info(){
    System.out.println("姓名:"+this.name);
    System.out.println("年龄:"+this.age);
    System.out.println("性别:"+this.sex);
  }

  public void study(){
    System.out.println(this.name+"正在学习");
  }

  public static void main(String[] args) {
    Student s = new Student("刘备",18,"男");
    s.info();
    s.study();
  }
}
