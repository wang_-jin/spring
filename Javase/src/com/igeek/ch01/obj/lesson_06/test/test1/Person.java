package com.igeek.ch01.obj.lesson_06.test.test1;

import java.util.Arrays;

/*
第一题
定义一个Person类,类的内容有:
1.有姓名,年龄,爱好三个属性,爱好是一个一维数组
2.定义一个有参的构造方法
3.定义一个添加爱好的方法,每次调用可以往爱好数组中添加一个爱好
4.在main中创建实例对象测试

*/
public class Person {
  String name;
  int age;
  String[] likes;
  public Person(){};
  public Person(String name,int age,String[] likes){
    this.name = name;
    this.age = age;
    this.likes = likes;
  }
  public void addLike(String like){
    //数组扩容,给数组容量加1
    String[] arr = Arrays.copyOf(this.likes,this.likes.length+1);
    //给数组最后一个索引位置添加元素
    arr[arr.length-1] = like;
    //将新的数组的地址赋值给likes属性
    this.likes = arr;
  }

  public static void main(String[] args) {
    //注意:数组的字面量形式只能在创建数组变量并给其赋初始值的时候使用,其他情况下请使用
    //另外两种创建数组的方法!!!
    String[] arr = {"篮球","足球"};
    Person p = new Person("刘备",18,arr);
    p.addLike("看电影");
    System.out.println("name:"+p.name);
    System.out.println("age:"+p.age);
    System.out.println(Arrays.toString(p.likes));
  }
}
