package com.igeek.ch01.obj.lesson_06.test.test2;
/*
第二题
定义一个成绩计算类
类中包含的内容有:
1、三个float类型的成员属性(java成绩、js成绩、python成绩);
2、一个无参的构造方法和一个带参数的构造方法;
3、计算三个科目的平均值的方法;
4、计算三个科目的总成绩的方法;
5、程序入口main方法
在main中分别通过有参和无参的构造方法实例化成绩类的对象并传入成绩参数,
再通过对象调用计算平均值和总成绩的方法打印输出,
在计算平均值和总成绩的方法中,分别获取到属性的值进行计算
*/
public class Score {
  float java;
  float js;
  float python;
  public Score(){};
  public Score(float java,float js,float python){
    this.java = java;
    this.js = js;
    this.python = python;
  }
  public float average(){
    return (this.java+this.js+this.python)/3;
  }

  public float sum(){
    return (this.java+this.js+this.python);
  }

  public static void main(String[] args) {
    Score s1 = new Score();
    s1.java = 67F;
    s1.js = 78F;
    s1.python = 88F;
    System.out.println("平均分:"+s1.average());
    System.out.println("总分:"+s1.sum());

    Score s2 = new Score(66,77,88);
    System.out.println("平均分:"+s2.average());
    System.out.println("总分:"+s2.sum());

  }
}
