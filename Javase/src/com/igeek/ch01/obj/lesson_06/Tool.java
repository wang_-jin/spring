package com.igeek.ch01.obj.lesson_06;


public class Tool {
  public static void main(String[] args) {
    Person p = new Person("刘备",18);
    //使用setter方法修饰私有属性
    p.setName("关羽");
    //使用getter方法获取私有属性值
    System.out.println(p.getName());
  }
}
