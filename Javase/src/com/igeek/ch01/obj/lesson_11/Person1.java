package com.igeek.ch01.obj.lesson_11;

public class Person1 {
  String name;
  int age;
  int height;
  /*
  代码块的作用:
    初始化属性
  */
  public Person1(){
    System.out.println("构造方法执行了");
  }
  /*
  构造代码块:
    不使用static修饰的代码块{},我们称为构造代码块,
    他的执行顺序是在构造方法的前面.
    多次使用构造方法创建实例对象,构造代码块也会多次执行
  */
  {
    this.age = 18;
    this.height = 170;
    System.out.println("构造代码块执行了");
  }
  /*
  静态代码块:
    使用static修饰的代码块{},我们称为静态代码块,
    他的执行顺序是在构造方法的前面.
    多次使用构造方法创建实例对象,静态代码块也执行一次,
    静态代码块会在类加载时执行,因此只会执行一次!

  */
  static{
    System.out.println("静态代码块执行了");
  }

  public static void main(String[] args) {
    Person1 p1 = new Person1();
    System.out.println("p1.age:"+p1.age);
    System.out.println("p1.height:"+p1.height);
    Person1 p2 = new Person1();

  }
}
