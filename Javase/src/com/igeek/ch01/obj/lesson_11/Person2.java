package com.igeek.ch01.obj.lesson_11;

public class Person2 {
  String name="刘备";
  static int age = 18;
  public void eat(){
    //this.name-->p.name
    System.out.println("name:"+name);
  }
  public static void main(String[] args) {
    Person2 p = new Person2();
    p.eat();

    System.out.println();
    /*
    在java中实例变量一般被对象访问:对象.实例变量名,
    也可以将 对象. 省略掉,直接使用 实例变量名 访问实例对象,
    如果 对象. 被省略掉了,java编辑器会自动在变量名
    左边添加 this. 来访问实例变量,实例方法中存在this直接使用
    实例变量名没有问题,但是,静态方法中不能使用this,因为静态方法
    属于类,静态方法被类调用,
    而this表示调用方法的实例对象,静态方法没有被实例对象调用,因此
    使用this会报错
    */
    //System.out.println("name:"+name);
    //System.out.println("age:"+age);

  }
}
