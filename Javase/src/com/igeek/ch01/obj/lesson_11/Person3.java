package com.igeek.ch01.obj.lesson_11;

public class Person3 {
  /*
  静态变量只会在内存中创建一次,在程序生命周期内,静态变量只会被分配一块内存空间,
  所有实例对象共享同一个静态变量
  */
  int a = 1;
  static int b = 1;

  public static void main(String[] args) {
    Person3 p1 = new Person3();
    Person3 p2 = new Person3();
    p1.a = 2;
    p1.b = 2;
    System.out.println("p2.a:"+p2.a);
    System.out.println("p2.b:"+p2.b);
  }
}
