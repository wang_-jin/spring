package com.igeek.ch01.obj.lesson_11;

public class Person {
  /*
  成员变量(属性):
    1.实例变量
    2.静态变量
  */
  //没有static修饰的成员变量也叫实例变量
  int a=1;
  //使用static修饰的成员变量也叫静态变量
  static int b=2;

  //没有使用static修饰的方法叫实例方法
  void foo(){
    System.out.println("foo方法");
  }
  //使用static修饰的方法叫静态方法
  static void foo1(){
    System.out.println("foo1方法");
  }

  public static void main(String[] args) {
    /*
    使用实例变量:
      实例对象.变量名
    */
    Person p = new Person();
    System.out.println("p.a:"+p.a);
    /*
    使用静态变量:
      实例对象.变量名 或 类名.变量名
    注意:
      静态变量尽量不要使用实例对象去调用,会产生歧义,
      别人会误以为你这个静态变量是实例变量!!!
    */
    System.out.println("p.b:"+p.b);
    System.out.println("Person.b:"+Person.b);

    /*
    调用实例方法:
      实例对象.方法名
    */
    p.foo();
    /*
    调用静态方法:
      类名.方法名
    */
    p.foo1();
    Person.foo1();

  }
}
