package com.igeek.ch01.obj.lesson_20;

public class Tool {
  /*
  可变参数:
    参数的个数不确定,由传入的实参决定,传入几个实参就有几个参数
  语法:
  方法名(形参类型... 形参名){}
  注意:
    可变参数的形参args本质是一个数组,我们可以使用数组的循环
    操作获取到所有传进来的实参,然后进行一些你需要的逻辑操作
  */
  static void sum(int... args){
    int s = 0;
    for(int i:args){
      s+=i;
    }
    System.out.println("多个数字的和为:"+s);
  }
  public static void main(String[] args) {
    //可变参数实参可以传单个的数据
    sum(1,2);
    sum(1,3,5);
    sum(1,5,8,9);
    //可变参数实参也可以传一个数组
    sum(new int[]{1,3,5,7,9});
  }
}
