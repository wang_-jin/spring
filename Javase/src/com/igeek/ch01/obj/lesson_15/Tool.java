package com.igeek.ch01.obj.lesson_15;

public class Tool {
  public static void main(String[] args) {
    /*
    编译期类型:
      编译器类型主要看=左边的类型
    运行期类型:
      运行期类型主要看=右边的类型
    */

    /*
    向上转型:
      将子类类型赋值给父类类型使用,一般不会不需要进行强制类型转换,
      因为子类就是一种特殊的父类,父类拥有的属性,方法,子类都有,
      因此子转父是不会出现任何问题的!
    */
    Animal ani = new Dog();
    ani.eat();

    /*
    向下转型:
      将父类类型赋值给子类类型使用,需要用到强制类型转换,
      这里存在一定的安全问题,如果父类引用指向的不是子类实例对象,
      那么父类转子类就会报错!!!
    */
    Animal ani1 = new Dog();
    Dog d = (Dog)ani1;
    d.eat();
  }
}
