package com.igeek.ch01.obj.lesson_19;
/*
定义了一个枚举类型,枚举类型的关键字enum
定义枚举的语法:
  public enum 枚举类型名字{
    常量1,
    常量2,
    ...
  }

注意:
  在java中如果你定义的是一个常量,一般建议
  将这个常量的所有字母都大写,这是常量的规范,
  而不是一种语法!
*/
public enum Season {
  SPRING,
  SUMMER,
  AUTUMN,
  WINTER
}
