package com.igeek.ch01.obj.lesson_19.test;

public class Role {
  int HP;
  int ATK;
  int DEF;
  //设置职业信息的方法
  void setup(String career,int HP,int ATK,int DEF){
    this.HP = HP;
    this.ATK = ATK;
    this.DEF = DEF;
    System.out.println("你选择了"+career+"职业,生命值是"+HP+"" +
      ",攻击力是"+ATK+",防御力是"+DEF);
  }
  public Role(Career career){
    switch (career){
      case FIGHTER:
        this.setup("战士",85,83,82);
        break;
      case TANK:
        this.setup("坦克",98,62,94);
        break;
      case Mage:
        this.setup("法师",65,100,75);
        break;
    }
  }
}
