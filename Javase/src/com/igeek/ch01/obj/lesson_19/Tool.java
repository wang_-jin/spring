package com.igeek.ch01.obj.lesson_19;

public class Tool {
  /*
  代码格式化快捷键:
    ctrl+alt+l
  */
  //1:春天 2:夏天 3:秋天 4:冬天
  static void chooseSeason(int season) {
    switch (season) {
      case 1:
        System.out.println("春天");
        break;
      case 2:
        System.out.println("夏天");
        break;
      case 3:
        System.out.println("秋天");
        break;
      case 4:
        System.out.println("冬天");
        break;
      default:
        System.out.println("参数不对");
    }
  }

  /*
  定义了一个chooseSeason方法,参数是枚举类型Season
  */
  static void chooseSeason(Season season){
    switch (season){
      case SPRING:
        System.out.println("春天");
        break;
      case SUMMER:
        System.out.println("夏天");
        break;
      case AUTUMN:
        System.out.println("秋天");
        break;
      case WINTER:
        System.out.println("冬天");
        break;
    }
  }

  public static void main(String[] args) {
    /*
    直接用常量做参数,常量具体的指代是通过代码完全看不出来的,
    代码的可读性比较差,那么,有什么方法可以让常量变得有意义,
    让常量的可读性提高呢,我们可以使用枚举类型给常量起个名字,
    这样的话代码的语义化就很明显了,我们就可以做到见名知意的效果!
    */
    /*chooseSeason(1);
    chooseSeason(2);
    chooseSeason(3);
    chooseSeason(4);*/

    /*
    当方法的参数是一个枚举类型时,如何传参:
      枚举名.常量名,如Season.SPRING
    */
    chooseSeason(Season.SPRING);
    chooseSeason(Season.SUMMER);
    chooseSeason(Season.AUTUMN);
    chooseSeason(Season.WINTER);
  }
}
