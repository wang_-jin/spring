package com.igeek.ch01.obj.lesson_03;

public class Person {
  /*
  java中的this本质就是一个实例对象.
  1.this所在的方法被哪一个实例对象
  调用,this就指向这个实例对象;
  2.this如果在一个构造方法中,那么this就
  直接指向这个构造方法创建的实例对象;
  */
  String name;
  public Person(){
    this.name = "刘备";
    //因为this在构造方法中,所以this指向这个构造方法创建的实例对象p
    System.out.println("this:"+this);
  }
  //创建了一个有参构造方法
  public Person(String name){
    /*
    这里this的作用:
      当有参构造方法中的形参名和属性名相同时,我们是无法
      直接通过属性名获取属性值的,我们必须通过 this.属性名 来获取
      属性并给其赋值;
    */
    this.name = name;
    //p1.name = "刘备"
  }




  void eat(){
    //this所在的eat方法被p对象调用了,因此this指向p对象
    System.out.println("eat中的this:"+this);
  }

  public static void main(String[] args) {
    Person p = new Person();
    p.eat();
    System.out.println("p:"+p);

    Person p1 = new Person("刘备");
    System.out.println("p1.name:"+p1.name);
  }
}
