package com.igeek.ch01.obj.lesson_12;

public class Person {
  String name;
  //final修饰的属性必须赋初始值
  final int age=18;
  final void eat(){
    System.out.println("eat");
  }
  public static void main(String[] args) {
    Person p = new Person();
    //final修饰的属性值不允许被修改
    //p.age = 20;
  }
}
