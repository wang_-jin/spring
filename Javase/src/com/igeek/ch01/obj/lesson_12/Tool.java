package com.igeek.ch01.obj.lesson_12;

public class Tool {
  final Person p = new Person();
  public static void main(String[] args) {
    /*
    注意:
      final如果修饰的是引用类型变量,变量的地址是不能变的,
      但是变量的属性是可以修改的!
    */
    Tool tool = new Tool();
    tool.p.name = "刘备";
    System.out.println("p.name:"+tool.p.name);
  }
}
