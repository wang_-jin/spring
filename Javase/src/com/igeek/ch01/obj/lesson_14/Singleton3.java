package com.igeek.ch01.obj.lesson_14;
/*    静态内部类单例
    也是一种特殊的懒汉式单例,只不过单例对象在
    静态内部类中创建*/
public class Singleton3 {
  private Singleton3(){};
  static class Inner{
    private static Singleton3 instance = new Singleton3();
  }
  public static Singleton3 getInstance(){
    return Inner.instance;
  }
}
