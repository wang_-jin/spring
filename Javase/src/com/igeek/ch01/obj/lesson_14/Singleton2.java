package com.igeek.ch01.obj.lesson_14;
/*
懒汉式单例模式:
  在使用单例对象时才去创建对象
*/
public class Singleton2 {
  //创建私有的,静态的实例变量
  private static Singleton2 instance;
  //私有化无参构造
  private Singleton2(){}
  //暴露一个公开的,静态的方法给其他类获取单例使用
  public static Singleton2 getInstance(){
    if(instance == null){
      instance = new Singleton2();
    }
    return instance;
  }
  public static void main(String[] args) {
    System.out.println(instance);
  }
}
