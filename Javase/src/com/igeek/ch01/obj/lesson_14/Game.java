package com.igeek.ch01.obj.lesson_14;

public class Game {
  //游戏场景宽度
  int width = 320;
  //游戏场景高度
  int height = 480;
  //游戏存档
  String save_path = "game.txt";
  int score = 0;
  private static Game gameConfig = new Game();
  private Game(){};
  public static Game getGameConfig(){
    return gameConfig;
  }
}
