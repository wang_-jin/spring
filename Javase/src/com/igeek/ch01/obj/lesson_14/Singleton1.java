package com.igeek.ch01.obj.lesson_14;
/*
饿汉式单例模式:
  在类加载时就创建一个实例对象,可以保证线程安全
  单例:
    一个类中唯一的一个实例对象

单例使用场景:
  可以用来作为一个全局共享数据配置对象存在
*/
public class Singleton1 {
  /*
  private保证单例对象不被修改
  static保证类加载后只有唯一的一个内存空间保存单例对象
  */

  private static Singleton1 instance = new Singleton1();
  /*
  私有化构造方法:
    保证外部类不能主动创建Singleton1的实例对象,就是保证了单例类
    的实例对象的唯一性
  */
  private Singleton1(){}
  /*
  将获取单例对象的方法定义为公开的,静态的方法,
  这样可以让所有其他类通过类名执行这个方法
  */
  public static Singleton1 getInstance(){
    return instance;
  }

}
