package com.igeek.ch01.obj.lesson_02;
/*
成员变量和局部变量的区别:
1.位置
成员变量:在方法体外,类中声明;
局部变量:在方法体内部声明.
2.作用域(变量可以使用的范围)
成员变量:本类中直接调用,其他类中是可以通过"对象.变量名"或"类名.变量名"调用;
局部变量:出了方法体就不能使用
3.修饰符(后面会讲)
成员变量:public,protected,private,final,volatile,transient等;
局部变量:final.
4.默认值
实例变量:有默认值;
局部变量:没有,必须手动初始化.
*/
public class Person {
    //属性=成员变量:不是必须设置默认值,如果没设置,java会自动帮你添加默认值
    int a;
  public static void main(String[] args) {
    //局部变量:必须设置默认值
    int b = 1;
    System.out.println("b:"+b);
    Person p = new Person();
    System.out.println("a:"+p.a);
  }

  void foo(){
    //局部变量b只能在当前方法体里面使用,不能在其他方法里面使用
    //System.out.println("b:"+b);
  }
}
