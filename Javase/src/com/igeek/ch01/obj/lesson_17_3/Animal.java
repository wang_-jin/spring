package com.igeek.ch01.obj.lesson_17_3;

public interface Animal {
  void eat();
  /*
  JDK1.8给接口添加了一个默认方法,默认方法的关键字是default,
  默认方法的出现可以极大的提高后期代码的一个维护效率,不需要
  每次修改接口,都去更新接口的实现类!
  默认方法的定义:
    default 修饰符 返回值类型 方法名(){
    }

  默认方法的调用:
    接口实现类的实力对象.方法名()

  */
  default void drink(){
    System.out.println("喝水");
  }
  /*
  静态实现方法,作用和默认实现方法类似,都是为了便于接口的后期维护!
  静态方法的定义:
  static 修饰符 返回值类型 方法名(){
  }
  静态实现方法的调用:
    接口名.方法名()
  */
  static void sing(){
    System.out.println("唱歌");
  }
}
