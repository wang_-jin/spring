package com.igeek.ch01.obj.lesson_17_3.JDK9;

public interface Animal {
  default void foo(){
    System.out.println("foo方法");
    doSomeThing();
  }

  default void foo1(){
    System.out.println("foo1方法");
    doSomeThing1();
  }
  /*
  接口中的私有方法,他主要是为默认实现服务的,当默认实现方法中有
  重复的代码时,可以将这些重复的代码拎出来使用私有方法封装一下!
  如何定义私有方法:
  private 返回值类型 方法名(){
  }
  如何调用私有方法:
    在默认实现方法中直接通过方法名调用即可
  */
  default void doSomeThing(){
    System.out.println("吃饭");
    System.out.println("睡觉");
    System.out.println("打豆豆");
  }
  /*
  静态私有方法,和私有方法作用一样,都是为默认实现方法服务的
  如何定义:
    static private 返回值类型 方法名(){
    }
   如何调用:
     在默认实现方法中直接通过方法名调用
  */
  static void doSomeThing1(){
    System.out.println("吃饭");
    System.out.println("睡觉");
    System.out.println("打豆豆");
  }
}
