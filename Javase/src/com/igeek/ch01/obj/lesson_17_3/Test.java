package com.igeek.ch01.obj.lesson_17_3;

public class Test {
  public static void main(String[] args) {
    Dog dog = new Dog();
    dog.eat();
    dog.drink();
    Animal.sing();

    Cat cat = new Cat();
    cat.eat();
    cat.drink();
    Animal.sing();
  }
}
