package com.igeek.ch01.work1.manager;

/**
 * @author wangjin
 * 2023/8/16 19:06
 * @description TODO
 * 	1.项目经理类Manager
 * 		属性：
 * 			姓名name
 * 			工号id
 * 			工资salary
 * 			奖金bonus
 * 		行为：
 * 			工作work()
 * 	要求:
 * 		1.按照以上要求定义Manager类和Coder类,属性要私有,生成空参、有参构造，setter和getter方法
 * 		2.定义测试类,在main方法中创建该类的对象并给属性赋值(演示两种方法:setter方法和构造方法)
 * 		3.调用成员方法,打印格式如下:
 * 			工号为123基本工资为15000奖金为6000的项目经理正在努力的做着管理工作,分配任务,检查员工提交上来的代码.....
 * 			工号为135基本工资为10000的程序员正在努力的写着代码......
 */
public class Manager {
    private String managerName;
    private String managerId;
    private int managerSalary;
    private int managerBonus;


    public Manager() {
    }

    public Manager(String managerName, String managerId, int managerSalary, int managerBonus) {
        this.managerName = managerName;
        this.managerId = managerId;
        this.managerSalary = managerSalary;
        this.managerBonus = managerBonus;
    }
    public void work(){
        System.out.println("工号为"+this.managerId+"基本工资为"+this.managerSalary+"奖金为"+this.managerBonus
        +"的项目经理正在努力的做着管理工作,分配任务,检查员工提交上来的代码.....");
    }

    /**
     * 获取
     * @return managerName
     */
    public String getManagerName() {
        return managerName;
    }

    /**
     * 设置
     * @param managerName
     */
    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    /**
     * 获取
     * @return managerId
     */
    public String getManagerId() {
        return managerId;
    }

    /**
     * 设置
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * 获取
     * @return managerSalary
     */
    public int getManagerSalary() {
        return managerSalary;
    }

    /**
     * 设置
     * @param managerSalary
     */
    public void setManagerSalary(int managerSalary) {
        this.managerSalary = managerSalary;
    }

    /**
     * 获取
     * @return managerBonus
     */
    public int getManagerBonus() {
        return managerBonus;
    }

    /**
     * 设置
     * @param managerBonus
     */
    public void setManagerBonus(int managerBonus) {
        this.managerBonus = managerBonus;
    }

    public String toString() {
        return "Manager{managerName = " + managerName + ", managerId = " + managerId + ", managerSalary = " + managerSalary + ", managerBonus = " + managerBonus + "}";
    }
}
