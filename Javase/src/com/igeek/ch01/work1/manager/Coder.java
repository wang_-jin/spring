package com.igeek.ch01.work1.manager;

/**
 * @author wangjin
 * 2023/8/16 19:06
 * @description TODO
 * 	2.程序员类Coder
 * 		属性：
 * 			姓名name
 * 			工号id
 * 			工资salary
 * 		行为：
 * 			工作work()
 * 	要求:
 * 		1.按照以上要求定义Manager类和Coder类,属性要私有,生成空参、有参构造，setter和getter方法
 * 		2.定义测试类,在main方法中创建该类的对象并给属性赋值(演示两种方法:setter方法和构造方法)
 * 		3.调用成员方法,打印格式如下:
 * 			工号为123基本工资为15000奖金为6000的项目经理正在努力的做着管理工作,分配任务,检查员工提交上来的代码.....
 * 			工号为135基本工资为10000的程序员正在努力的写着代码......
 */
public class Coder {
    private String coderName;
    private String coderId;
    private int coderSalary;


    public Coder() {
    }

    public Coder(String coderName, String coderId, int coderSalary) {
        this.coderName = coderName;
        this.coderId = coderId;
        this.coderSalary = coderSalary;
    }

    public void work(){
        System.out.println("工号为"+this.coderId+"基本工资为"+this.coderSalary+"的程序员正在努力的写着代码......");
    }

    /**
     * 获取
     * @return coderName
     */
    public String getCoderName() {
        return coderName;
    }

    /**
     * 设置
     * @param coderName
     */
    public void setCoderName(String coderName) {
        this.coderName = coderName;
    }

    /**
     * 获取
     * @return coderId
     */
    public String getCoderId() {
        return coderId;
    }

    /**
     * 设置
     * @param coderId
     */
    public void setCoderId(String coderId) {
        this.coderId = coderId;
    }

    /**
     * 获取
     * @return coderSalary
     */
    public int getCoderSalary() {
        return coderSalary;
    }

    /**
     * 设置
     * @param coderSalary
     */
    public void setCoderSalary(int coderSalary) {
        this.coderSalary = coderSalary;
    }

    public String toString() {
        return "Coder{coderName = " + coderName + ", coderId = " + coderId + ", coderSalary = " + coderSalary + "}";
    }
}
