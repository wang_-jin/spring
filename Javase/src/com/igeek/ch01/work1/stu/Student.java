package com.igeek.ch01.work1.stu;

/**
 * @author wangjin
 * 2023/8/16 18:56
 * @description TODO
 *  * 	设计，并定义一个学员类：Student，要求有以下属性：
 *  * 		学员编号（String）
 *  * 		姓名（String）
 *  * 		性别（String）
 *  * 		身高（double）
 *  * 		年龄（int）
 */
public class Student {
    private String stuId;
    private String stuName;
    private String sex;
    private String height;
    private int age;


    public Student() {
    }

    public Student(String stuId, String stuName, String sex, String height, int age) {
        this.stuId = stuId;
        this.stuName = stuName;
        this.sex = sex;
        this.height = height;
        this.age = age;
    }

    /**
     * 获取
     * @return stuId
     */
    public String getStuId() {
        return stuId;
    }

    /**
     * 设置
     * @param stuId
     */
    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    /**
     * 获取
     * @return stuName
     */
    public String getStuName() {
        return stuName;
    }

    /**
     * 设置
     * @param stuName
     */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    /**
     * 获取
     * @return sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 获取
     * @return height
     */
    public String getHeight() {
        return height;
    }

    /**
     * 设置
     * @param height
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Student{stuId = " + stuId + ", stuName = " + stuName + ", sex = " + sex + ", height = " + height + ", age = " + age + "}";
    }
}
