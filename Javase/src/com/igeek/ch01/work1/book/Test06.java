package com.igeek.ch01.work1.book;

/**
 * @author wangjin
 * 2023/8/16 14:53
 * @description TODO
 * 一、	需求说明：创建三个图书类对象，找出价格最高的图书并打印该图书的所有信息。
 * 二、	设计“图书类”Book，要求有以下属性：
 * 	图书编号：
 * 	书名：
 * 	价格：
 */
public class Test06 {
    public static void main(String[] args) {
        Book1 b1 = new Book1(1,"西游记",50);
        Book2 b2 = new Book2(2,"水浒传",60);
        Book3 b3 = new Book3(3,"三国演义",55);
        int a = b1.getPrice();
        int b = b2.getPrice();
        int c = b3.getPrice();
        int maxPrice = (a>b?(Math.max(a, c)):(Math.max(b, c)));
        System.out.println("最高价格为:"+maxPrice);
        if (maxPrice== b1.getPrice()){
            System.out.println("图书编号:"+b1.getId());
            System.out.println("图书名:"+b1.getBookName());
            System.out.println("图书价格:"+b1.getPrice());
        }else if (maxPrice== b2.getPrice()){
            System.out.println("图书编号:"+b2.getId());
            System.out.println("图书名:"+b2.getBookName());
            System.out.println("图书价格:"+b2.getPrice());
        }else if (maxPrice== b3.getPrice()){
            System.out.println("图书编号:"+b3.getId());
            System.out.println("图书名:"+b3.getBookName());
            System.out.println("图书价格:"+b3.getPrice());
        }
    }
}
