package com.igeek.ch01.work1.book;

/**
 * @author wangjin
 * 2023/8/16 19:56
 * @description TODO
 */
public class Book2 {
    private int id;
    private String bookName;
    private int price;

    public Book2(){}
    public Book2(int id,String bookName,int price){
        this.id=id;
        this.bookName=bookName;
        this.price=price;
    }
    void setId(int id){
        this.id=id;
    }
    int getId(){
        return id;
    }
    void setBookName(String bookName){
        this.bookName=bookName;
    }
    String getBookName(){
        return bookName;
    }
    void setPrice(int price){
        this.price=price;
    }
    int getPrice(){
        return price;
    }
}
