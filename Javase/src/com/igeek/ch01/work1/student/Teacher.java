package com.igeek.ch01.work1.student;

/**
 * @author wangjin
 * 2023/8/16 19:24
 * @description TODO
 * 	1.老师类Teacher
 * 		属性:
 * 			姓名name
 * 			年龄age
 * 			讲课内容content
 * 		行为:
 * 			吃饭
 * 			讲课
 */
public class Teacher {
    private String teaName;
    private int teaAge;
    private String teaContent;

    public Teacher() {
    }

    public Teacher(String teaName, int teaAge, String teaContent) {
        this.teaName = teaName;
        this.teaAge = teaAge;
        this.teaContent = teaContent;
    }

    /*年龄为30的周志鹏老师正在吃饭....
     年龄为30的周志鹏老师正在亢奋的讲着Java基础中面向对象的知识........("Java基础中面向对象"代表老师讲课的内容)*/
    void eat(){
        System.out.println("年龄为"+this.teaAge+"的"+this.teaName+"老师正在吃饭");
    }
    void teach(){
        System.out.println("年龄为"+this.teaAge+"的"+this.teaName+"老师正在亢奋的讲着"+this.teaContent+"的知识");
    }
    /**
     * 获取
     * @return teaName
     */
    public String getTeaName() {
        return teaName;
    }

    /**
     * 设置
     * @param teaName
     */
    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    /**
     * 获取
     * @return teaAge
     */
    public int getTeaAge() {
        return teaAge;
    }

    /**
     * 设置
     * @param teaAge
     */
    public void setTeaAge(int teaAge) {
        this.teaAge = teaAge;
    }

    /**
     * 获取
     * @return teaContent
     */
    public String getTeaContent() {
        return teaContent;
    }

    /**
     * 设置
     * @param teaContent
     */
    public void setTeaContent(String teaContent) {
        this.teaContent = teaContent;
    }

    public String toString() {
        return "Teacher{teaName = " + teaName + ", teaAge = " + teaAge + ", teaContent = " + teaContent + "}";
    }

}
