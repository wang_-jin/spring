package com.igeek.ch01.work1.student;

/**
 * @author wangjin
 * 2023/8/16 19:24
 * @description TODO
 * 	2.学生类Student
 * 		属性:
 * 			姓名name
 * 			年龄age
 * 			学习内容content
 * 		行为:
 * 			吃饭eat()
 * 			学习study()
 */
public class Student {
    private String stuName;
    private int stuAge;
    private String stuContent;

    public Student() {
    }

    public Student(String stuName, int stuAge, String stuContent) {
        this.stuName = stuName;
        this.stuAge = stuAge;
        this.stuContent = stuContent;
    }
    /*年龄为18的韩光同学正在吃饭....
     年龄为18的韩光同学正在专心致志的听着面向对象的知识....("面向对象"代表学生学习的内容)*/
    void eat(){
        System.out.println("年龄为"+this.stuAge+"的"+this.stuName+"同学正在吃饭");
    }
    void study(){
        System.out.println("年龄为"+this.stuAge+"的"+this.stuName+"同学正在专心致志的听着"+this.stuContent+"的知识");
    }

    /**
     * 获取
     * @return stuName
     */
    public String getStuName() {
        return stuName;
    }

    /**
     * 设置
     * @param stuName
     */
    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    /**
     * 获取
     * @return stuAge
     */
    public int getStuAge() {
        return stuAge;
    }

    /**
     * 设置
     * @param stuAge
     */
    public void setStuAge(int stuAge) {
        this.stuAge = stuAge;
    }

    /**
     * 获取
     * @return stuContent
     */
    public String getStuContent() {
        return stuContent;
    }

    /**
     * 设置
     * @param stuContent
     */
    public void setStuContent(String stuContent) {
        this.stuContent = stuContent;
    }

    public String toString() {
        return "Student{stuName = " + stuName + ", stuAge = " + stuAge + ", stuContent = " + stuContent + "}";
    }
}
