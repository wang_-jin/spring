package com.igeek.ch01.work1.animal;

/**
 * @author wangjin
 * 2023/8/16 19:37
 * @description TODO
 * 	2.狗类Dog
 * 		属性:
 * 			毛的颜色color
 * 			品种breed
 * 		行为:
 * 			吃饭()
 * 			看家lookHome()
 */
public class Dog {
    private String dogColor;
    private String dogBreed;

    void eat(){
        System.out.println(this.dogColor+"的"+this.dogBreed+"正在啃骨头");
    }
    void lookHome(){
        System.out.println(this.dogColor+"的"+this.dogBreed+"正在看家");
    }
     String getDogColor(){
        return dogColor;
     }
     void setDogColor(String dogColor){
        this.dogColor=dogColor;
     }
     String getDogBreed(){
        return dogBreed;
     }
     void setDogBreed(String dogBreed){
        this.dogBreed=dogBreed;
     }

}
