package com.igeek.ch01.work1.animal;

/**
 * @author wangjin
 * 2023/8/16 19:37
 * @description TODO
 * 	1.猫类Cat
 * 		属性:
 * 			毛的颜色color
 * 			品种breed
 * 		行为:
 * 			吃饭eat()
 * 			抓老鼠catchMouse()
 * 		狗特有行为:看家lookHome
 */
public class Cat {
    private String catColor;
    private String catBreed;


    public Cat() {
    }

    public Cat(String catColor, String catBreed) {
        this.catColor = catColor;
        this.catBreed = catBreed;
    }
    void eat(){
        System.out.println(this.catColor+"的"+this.catBreed+"正在吃鱼");
    }
    void catchMouse(){
        System.out.println(this.catColor+"的"+this.catBreed+"正在逮老鼠");
    }


    /**
     * 获取
     * @return catColor
     */
    public String getCatColor() {
        return catColor;
    }

    /**
     * 设置
     * @param catColor
     */
    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }

    /**
     * 获取
     * @return catBreed
     */
    public String getCatBreed() {
        return catBreed;
    }

    /**
     * 设置
     * @param catBreed
     */
    public void setCatBreed(String catBreed) {
        this.catBreed = catBreed;
    }

    public String toString() {
        return "Cat{catColor = " + catColor + ", catBreed = " + catBreed + "}";
    }
}
