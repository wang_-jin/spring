package com.igeek.ch01.work1.teacher;

import java.util.Date;

/**
 * @author wangjin
 * 2023/8/16 18:40
 * @description TODO
 * 2.	存储两个科目信息：
 * 课程编号	    名称	      创建时间	课程描述
 * s001			Java	2007-02-08	Java学科，包含JavaSE和JavaEE
 * s002			IOS		2007-02-09	IOS系统开发
 */
public class Course {
    private String coursrId;
    private String courseName;
    private String setTime;
    private String description;


    public Course() {
    }

    public Course(String coursrId, String courseName, String setTime, String description) {
        this.coursrId = coursrId;
        this.courseName = courseName;
        this.setTime = setTime;
        this.description = description;
    }

    /**
     * 获取
     * @return coursrId
     */
    public String getCoursrId() {
        return coursrId;
    }

    /**
     * 设置
     * @param coursrId
     */
    public void setCoursrId(String coursrId) {
        this.coursrId = coursrId;
    }

    /**
     * 获取
     * @return courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * 设置
     * @param courseName
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * 获取
     * @return setTime
     */
    public String getSetTime() {
        return setTime;
    }

    /**
     * 设置
     * @param setTime
     */
    public void setSetTime(String setTime) {
        this.setTime = setTime;
    }

    /**
     * 获取
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return "Course{coursrId = " + coursrId + ", courseName = " + courseName + ", setTime = " + setTime + ", description = " + description + "}";
    }
}
