package com.igeek.ch01.work1.teacher;

/**
 * @author wangjin
 * 2023/8/16 18:40
 * @description TODO
 * 1.	三个老师信息：
 * 教师编号	    姓名	   性别	   年龄	    科目
 * t001			薛之谦	男		26		Java
 * t002			张碧晨	女		24		IOS
 * t003			张杰	    男		28		Java
 */
public class Teacher {
    private String teacherId;
    private String teacherName;
    private char sex;
    private int age;
    private String course;

    public Teacher() {
    }

    public Teacher(String teacherId, String teacherName, char sex, int age, String course) {
        this.teacherId = teacherId;
        this.teacherName = teacherName;
        this.sex = sex;
        this.age = age;
        this.course = course;
    }

    /**
     * 获取
     * @return teacherId
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * 设置
     * @param teacherId
     */
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * 获取
     * @return teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * 设置
     * @param teacherName
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * 获取
     * @return sex
     */
    public char getSex() {
        return sex;
    }

    /**
     * 设置
     * @param sex
     */
    public void setSex(char sex) {
        this.sex = sex;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return course
     */
    public String getCourse() {
        return course;
    }

    /**
     * 设置
     * @param course
     */
    public void setCourse(String course) {
        this.course = course;
    }

    public String toString() {
        return "Teacher{teacherId = " + teacherId + ", teacherName = " + teacherName + ", sex = " + sex + ", age = " + age + ", course = " + course + "}";
    }
}
