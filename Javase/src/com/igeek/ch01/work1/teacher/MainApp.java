package com.igeek.ch01.work1.teacher;

/**
 * @author wangjin
 * 2023/8/16 14:00
 * @description TODO
 * 一、	有以下数据：
 * 1.	三个老师信息：
 * 教师编号	    姓名	   性别	   年龄	    科目
 * t001			薛之谦	男		26		Java
 * t002			张碧晨	女		24		IOS
 * t003			张杰	    男		28		Java
 * 2.	存储两个科目信息：
 * 课程编号	    名称	      创建时间	课程描述
 * s001			Java	2007-02-08	Java学科，包含JavaSE和JavaEE
 * s002			IOS		2007-02-09	IOS系统开发
 * 二、	请分别定义两个类；
 * 三、	创建MainApp类中，包含main()方法，创建相应对象，通过构造方法给成员变量赋值。
 * 四、	打印每个对象的所有属性。
 * 要求：每个类要按照封装的原则进行定义。并提供无参和全参的构造方法。
 * 	设计类：Teacher(教师类)和Course(课程类)
 * 	为每个类设计“成员属性”
 * 	定义两个类
 * 	定义MainApp类，包含main()方法，分别创建对象存储数据。
 */
public class MainApp {
    public static void main(String[] args) {
        Course c1 = new Course("s001","Java","2007-02-08","Java学科，包含JavaSE和JavaEE");
        Course c2 = new Course("s002","IOS","2007-02-09","IOS系统开发");
        Teacher t1 = new Teacher("t001","薛之谦",'男',26,"Java");
        Teacher t2 = new Teacher("t002","张碧晨",'女',24,"IOS");
        Teacher t3 = new Teacher("t003","张杰",'男',28,"Java");
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
    }
}
