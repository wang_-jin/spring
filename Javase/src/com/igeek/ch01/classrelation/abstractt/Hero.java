package com.igeek.ch01.classrelation.abstractt;

/**
 * @author wangjin
 * 2023/8/18 11:17
 * @description TODO
 */
public class Hero extends Role{
    @Override
    void attack() {
        System.out.println("英雄"+this.name+"被攻击了");
    }
}
