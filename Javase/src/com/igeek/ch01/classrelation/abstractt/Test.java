package com.igeek.ch01.classrelation.abstractt;

/**
 * @author wangjin
 * 2023/8/18 11:17
 * @description TODO
定义一个角色类Role是抽象类,
其中的攻击方法attack是抽象方法,
英雄类Hero和怪兽类Monster继承Role类,这两个类都有一个属性name,
重写attack攻击方法,Hero类中方法attack打印:英雄xx攻击了,
Monster类中attack方法打印:怪兽xx攻击了
 */
public class Test {
    public static void main(String[] args) {
        Hero h = new Hero();
        h.name = "张无忌";
        h.attack();
        Monster m = new Monster();
        m.name = "哥斯拉";
        m.attack();
    }
}
