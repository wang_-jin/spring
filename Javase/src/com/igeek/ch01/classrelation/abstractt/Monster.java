package com.igeek.ch01.classrelation.abstractt;

/**
 * @author wangjin
 * 2023/8/18 11:18
 * @description TODO
 */
public class Monster extends Role{
    @Override
    void attack() {
        System.out.println("怪兽"+this.name+"被攻击了");
    }
}
