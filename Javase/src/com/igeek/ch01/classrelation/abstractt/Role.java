package com.igeek.ch01.classrelation.abstractt;

/**
 * @author wangjin
 * 2023/8/18 11:17
 * @description TODO
 */
public abstract class Role {
    String name;
    abstract void attack();
}
