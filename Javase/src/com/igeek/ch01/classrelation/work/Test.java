package com.igeek.ch01.classrelation.work;

/**
 * @author wangjin
 * 2023/8/17 14:51
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Worker w = new Worker("李四",20);
        w.study();
        Student s = new Student("张三",18);
        s.study();
    }
}
