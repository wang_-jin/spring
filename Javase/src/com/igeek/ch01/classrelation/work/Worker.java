package com.igeek.ch01.classrelation.work;

/**
 * @author wangjin
 * 2023/8/17 14:41
 * @description TODO
 */
public class Worker extends Person{
/*    private String workerName;
    private int workerAge;*/

    public Worker() {
    }

    public Worker(String name, int age) {
        super(name,age);
    }
    @Override
    void study(){
        System.out.println("工人"+this.name+"正在学习技能");
    }
}
