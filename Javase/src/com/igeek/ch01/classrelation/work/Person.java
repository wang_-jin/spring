package com.igeek.ch01.classrelation.work;

/**
 * @author wangjin
 * 2023/8/17 14:41
 * @description TODO
1.创建一个"人"的类,有姓名和年龄,有一个学习方法
有学生和工人继承这个类,他们也有姓名和年龄.
学生有自己的学习方法,工人有自己的工作方法.
现在有个学生叫张三,22岁,正在学习,调用学习方法打印:学生张三正在学习知识
有个工人叫李四,25岁,正在学习,调用工作方法打印:工人李四正在学习技能
 */
public class Person {
    String name;
    int age;
    public Person(){}
    public Person (String name,int age){
        this.name = name;
        this.age = age;
    }
    void study(){
        System.out.println("正在学习");
    }
}
