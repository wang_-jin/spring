package com.igeek.ch01.classrelation.work;

/**
 * @author wangjin
 * 2023/8/17 14:41
 * @description TODO
 */
public class Student extends Person{
/*    private String stuName;
    private int stuAge;*/

    public Student() {
    }

    public Student(String name, int age) {
        super(name,age);
    }
    @Override
    void study(){
        System.out.println("学生"+this.name+"正在学习知识");
    }

}
