package com.igeek.ch01.classrelation.singleton;

/**
 * @author wangjin
 * 2023/8/18 14:59
 * @description TODO
 */
/*    静态内部类单例
    也是一种特殊的懒汉式单例,只不过单例对象在
    静态内部类中创建*/
public class Singleton3 {
    //私有化构造函数
    private Singleton3(){}
    static class InnerClass{
        //私有化静态单例对象
        private static Singleton3 instance = new Singleton3();
    }
    //公开的静态的获取单例对象的方法
    public static  Singleton3 getInstance(){
        return InnerClass.instance;
    }
}
