package com.igeek.ch01.classrelation.singleton;

/**
 * @author wangjin
 * 2023/8/18 14:43
 * @description TODO
 */
    //懒汉式单例
    //在需要使用单例对象时,才去创建单例对象
public class Singleton2 {
    //私有化静态单例对象
    private static Singleton2 instance;
    //私有化构造函数
    private Singleton2(){}
    public static Singleton2 getInstance(){
        //如果单例对象不存在,创建单例对象
        if (instance == null){
            instance = new Singleton2();
        }
        return instance;
    }
}
