package com.igeek.ch01.classrelation.singleton;

/**
 * @author wangjin
 * 2023/8/18 14:42
 * @description TODO
 */
/*
单例模式:
  单例的意思是单独的唯一的实例对象,单例模式是指某个类
  只有唯一的一个实例对象!

单例模式分类:
  饿汉式单例:
    在类加载时就创建好单例对象,保证了单例对象的唯一性和线程安全性
  懒汉式单例:
    在需要使用单例对象时,才去创建单例对象
  静态内部类单例:
    也是一种特殊的懒汉式单例,只不过单例对象在
    静态内部类中创建
*/
public class Singleton1 {
    /*
私有化静态单例对象:
将单例对象定义为private的作用是不允许其他类访问,
防止单例对象被修改;
将单例对象设置为static的作用是在类加载的时候就创建
一个单例对象,保证对象的唯一性;
*/
    String name;
    private static Singleton1 instance = new Singleton1();
    //私有化构造函数,不允许其他类创建实例对象
    private Singleton1(){}
    //提供一个公开的,静态的方法暴露单例对象给其他类使用
    public static Singleton1 getInstance(){
        return instance;
    }
}
