package com.igeek.ch01.classrelation.singleton;

/**
 * @author wangjin
 * 2023/8/18 14:49
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Singleton1 s1 = Singleton1.getInstance();
        Singleton1 s2 = Singleton1.getInstance();
        System.out.println("s1==s2?"+(s1==s2));

        Singleton2 a = Singleton2.getInstance();
        Singleton2 b = Singleton2.getInstance();
        System.out.println("a==b?"+(a==b));

        Singleton3 c = Singleton3.getInstance();
        Singleton3 d = Singleton3.getInstance();
        System.out.println("s1==s2?"+(c==d));
    }
}
