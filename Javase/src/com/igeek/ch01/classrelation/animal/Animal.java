package com.igeek.ch01.classrelation.animal;

/**
 * @author wangjin
 * 2023/8/17 15:40
 * @description TODO
3.现有Animal类,有姓名,年龄属性，
Dog类和Bird类除以上属性,Dog类有颜色属性,Bird类有飞行速度属性,
三个类中都有eat方法,但其实现方式不同,Animal类中eat打印某某吃了东西,
Dog类中eat打印某某吃了骨头,Bird类中eat打印某某吃了谷子,
请写出各类构造方法,并要用到this和super关键字.
 */
public class Animal {
    String name;
    int age;

    public Animal(){

    }
    public Animal(String name,int age){
        this.age=age;
        this.name=name;
    }
    public void eat(){
        System.out.println(this.name+"吃了东西");
    }
}
