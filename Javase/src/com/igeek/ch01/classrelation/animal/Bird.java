package com.igeek.ch01.classrelation.animal;

/**
 * @author wangjin
 * 2023/8/17 15:40
 * @description TODO
 */
public class Bird extends Animal{
    int speed;
    public Bird(){}
    public Bird(String name,int age){
        super(name,age);
    }

    public Bird(String name,int age,int speed){
        super(name,age);
        this.speed = speed;
    }
    @Override
    public void eat(){
        System.out.println(this.name+"吃了谷子");
    }
}
