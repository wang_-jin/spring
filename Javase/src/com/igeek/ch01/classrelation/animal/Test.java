package com.igeek.ch01.classrelation.animal;

/**
 * @author wangjin
 * 2023/8/17 15:46
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Animal a = new Animal("大象",10);
        a.eat();
        Bird b = new Bird("喜鹊",6);
        b.eat();
        Dog d = new Dog("来福",6);
        d.eat();
    }
}
