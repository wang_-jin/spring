package com.igeek.ch01.classrelation.animal;

/**
 * @author wangjin
 * 2023/8/17 15:40
 * @description TODO
 */
public class Dog extends Animal{
    String color;
    public Dog(){}
    public Dog(String name,int age){
        super(name,age);
    }
    public Dog(String name,int age,String color){
        super(name,age);
        this.color = color;
    }
    @Override
    public void eat(){
        System.out.println(this.name+"吃了骨头");
    }
}
