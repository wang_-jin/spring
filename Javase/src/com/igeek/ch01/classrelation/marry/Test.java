package com.igeek.ch01.classrelation.marry;

/**
 * @author wangjin
 * 2023/8/17 15:25
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Person p = new Person("许仙",25,true);
        Person p1 = new Person("白娘子",26,false);
        Person p2 = new Person("小青",25,false);
        Person p3 = new Person("张三",25, true);
        p.marry(p1);
        p.marry(p2);
        p1.marry(p3);
    }
}
