package com.igeek.ch01.classrelation.marry;

/**
 * @author wangjin
 * 2023/8/17 15:00
 * @description TODO
2.写一个类Person,包含以下属性：
String  name;
int  age;
boolean  gender;  //性别  true男  false女
为Person类写一个marry(Person p)方法,代表当前对象和p结婚,
如若可以结婚,则打印恭贺新婚,
否则输出不能结婚原因.要求在main方法中测试以上程序.
下列情况不能结婚:
1.同性,打印同性不能结婚;
2.未达到结婚年龄,男<24,女<22,打印未达到结婚年龄;
 */
public class Person {
    String  name;
    int  age;
    boolean  gender;  //性别  true男  false女
    boolean married=false;

    public Person() {
    }

    public Person(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    /*下列情况不能结婚:
1.同性,打印同性不能结婚;
2.未达到结婚年龄,男<24,女<22,打印未达到结婚年龄;*/
    public void marry(Person p){
        if ((p.gender&&p.age>24||!(p.gender)&&p.age>22)&&(this.gender&&this.age>24||!(this.gender)&&this.age>22)){
            if(this.gender== p.gender){
                System.out.println("同性,不能结婚!");
            }else if (this.married||p.married){
                System.out.println((this.married?this.name:p.name)+"已有对象,不可以劈腿!");
            } else{
                System.out.println("恭贺新婚!");
                this.married=true;
                p.married=true;
            }
        }else{
            System.out.println("未达到结婚年龄");
        }
    }



    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return gender
     */
    public boolean isGender() {
        return gender;
    }

    /**
     * 设置
     * @param gender
     */
    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + ", gender = " + gender + "}";
    }
}
