package com.igeek.ch01.classrelation.staticc;

/**
 * @author wangjin
 * 2023/8/17 19:05
 * @description TODO
 */
public class Person1 {
    String name;
    int age;
    int height;
    public Person1(){
        System.out.println("构造方法被调用");
    }
    {
        this.name = "张三";
        this.age = 15;
        this.height = 180;
        System.out.println("构造代码块被调用");
    }
    static {
        System.out.println("静态代码块被调用");
    }

    public static void main(String[] args) {
        Person1 p = new Person1();
        System.out.println("p.name"+p.name);
        System.out.println("p.age"+p.age);
        System.out.println("p.height"+p.height);

    }
}
