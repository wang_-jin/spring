package com.igeek.ch01.classrelation.employee;

/**
 * @author wangjin
 * 2023/8/18 9:57
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Employee e = new Employee("小明",12000);
        Employee e1 = new Employee("小红",9000);
        System.out.println(e.name+"原薪资为"+e.salary);
        EmployeeUtil.addBonus(e);
        System.out.println(e1.name+"原薪资为"+e1.salary);
        EmployeeUtil.addBonus(e1);
    }
}
