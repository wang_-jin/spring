package com.igeek.ch01.classrelation.employee;

/**
 * @author wangjin
 * 2023/8/18 9:42
 * @description TODO
 */
public class Employee {
    String name;
    double salary;

    public Employee() {
    }
    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
}
