package com.igeek.ch01.classrelation.employee;

/**
 * @author wangjin
 * 2023/8/18 9:42
 * @description TODO
1.Employee类有姓名，薪资属性
2.EmployeeUtil工具类中,有涨工资 addBonus(Employee e)的方法(要求方法静态);
若获得到的薪资>10000,则涨薪水20%,否则涨薪10%.
 */
public class EmployeeUtil {
    static void addBonus(Employee e){
        if (e.salary>10000){
            e.salary= e.salary*1.20;
            System.out.println("涨后薪资为"+e.salary);
        }else{
            e.salary= e.salary*1.1;
            System.out.println("涨后薪资为"+e.salary);
        }
    }

}
