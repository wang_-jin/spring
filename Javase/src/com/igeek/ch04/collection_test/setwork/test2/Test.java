package com.igeek.ch04.collection_test.setwork.test2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/26 9:15
 * @description TODO
 */
public class Test {
    /*2.求一个字符串"aiodjl;hriWFUADJSV123UEHiowfjnivowe"中一共有几个不重复的字母,
    区分大小写,如a,A算两个字符(HashSet)*/
    public static void main(String[] args) {
        Set set = new HashSet();
        StringBuilder str = new StringBuilder("aiodjl;hriWFUADJSV123UEHiowfjnivowe");
        for (int i = 0; i < str.length(); i++) {
            set.add(str.charAt(i));
        }
        int num = 0;
        Pattern ptn = Pattern.compile("[a-zA-Z]");
        Matcher matcher = ptn.matcher(str);
        while (matcher.find()){
            num++;
        }
        System.out.println("去重后的字符集合:"+set);
        //存取重复字符
        List list = new ArrayList();
        for (int i = 0; i < str.length(); i++) {
            char item = str.charAt(i);
            String s = item+"";
            if (str.indexOf(s)!=i){
                list.add(s);
            }
        }
        Set set1 = new HashSet();
        set1.addAll(list);

        int exCharNum = str.length()-num;//除字母外字符个数
        System.out.println("除字母外字符个数"+exCharNum);
        int reCharNum = set1.size();//重复字符个数
        System.out.println("重复字符个数"+reCharNum);
        int notreCharNum = set.size()-exCharNum-reCharNum;//不重复的字母个数
        System.out.println("不重复的字母个数:"+notreCharNum);


    }
}
