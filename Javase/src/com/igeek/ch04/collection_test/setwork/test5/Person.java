package com.igeek.ch04.collection_test.setwork.test5;

import java.util.Objects;

/**
 * @author wangjin
 * 2023/8/27 17:06
 * @description TODO
 */
public class Person {
    String name;
    int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        //若名字相同则认为是相同对象
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        /*
         根据名字生成哈希码,相同名字对应的哈希码相同
         在哈希表中对应的哈希桶索引也相同
        */
        return Objects.hash(name);
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
}
