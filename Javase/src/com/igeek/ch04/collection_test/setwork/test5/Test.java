package com.igeek.ch04.collection_test.setwork.test5;


import com.igeek.ch01.obj.lesson_17_2.mulimplemwnts.Peron;
import com.igeek.day04.Array;

import java.util.*;

/**
 * @author wangjin
 * 2023/8/26 9:17
 * @description TODO
 */
public class Test {
    public static ArrayList moveString(ArrayList list) {
        Set set = new HashSet(list);
        list = new ArrayList(set);
        return list;
    }

    public static ArrayList moveObject(ArrayList list) {
        Set set = new HashSet(list);
        list = new ArrayList(set);
        return list;
    }

    /*5.定义方法去除ArrayList集合中重复元素
  1>.假设存入的是字符串元素,进行去重
  2>.假设存入的是自定义对象元素(如Person对象,根据name去重),进行去重*/
    public static void main(String[] args) {
        ArrayList<Object> arrayList = new ArrayList<>();
        ArrayList<Object> arrayList1 = new ArrayList<>();
        arrayList.add("a");
        arrayList.add("b");
        arrayList.add("c");
        arrayList.add("d");
        arrayList.add("a");
        arrayList.add("d");
        arrayList.add("f");
        arrayList1.add(new Person("张三", 25));
        arrayList1.add(new Person("张三", 28));
        arrayList1.add(new Person("李四", 26));
        arrayList1.add(new Person("王五", 22));
        arrayList1.add(new Person("王五", 29));
        arrayList1.add(new Person("张三", 24));
        arrayList1.add(new Person("小明", 25));
        /*HashSet去重原理:
        向HashSet中添加元素时,会用HashCode方法来判断HashCode位置哈希桶是否为空,为空则
        允许添加,如果已存在对象则会调用equals方法,判断两对象的属性值是否相同,如果相同则说明
        有重复对象则不与添加,不相同则可添加.
        */
        System.out.println(arrayList);
        System.out.println(arrayList1);
        System.out.println(moveString(arrayList));
        System.out.println(moveObject(arrayList1));


    }
}
