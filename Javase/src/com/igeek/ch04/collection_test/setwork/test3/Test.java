package com.igeek.ch04.collection_test.setwork.test3;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author wangjin
 * 2023/8/26 9:16
 * @description TODO
 */
public class Test {
    /*3.TreeSet练习将字符串中的数值进行排序。例如String str="8 10 15 5 2 7";
      2,5,7,8,10,15  使用 TreeSet完成。*/
    public static void main(String[] args) {
        Set set = new TreeSet();

        String str="8 10 15 5 2 7";
        String[] s = str.split(" ");

        for (String item: s) {
            set.add(Integer.parseInt(item));
        }
        System.out.println(set);//TreeSet默认按元素升序排列
    }
}
