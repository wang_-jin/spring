package com.igeek.ch04.collection_test.setwork.test4;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/26 9:16
 * @description TODO
 */
public class Test {
    /*4.模拟登陆,编写用户类,用户类有用户名和密码两个属性
  1>.给集合中存储5个用户对象
  2>.从控制台输入用户名和密码,和集合中的对象信息进行比较,相同表示成功,不同表示失败
  3>.有三次输入机会*/
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(new User("张三",123456));
        list.add(new User("李四",123333));
        list.add(new User("刘备",123222));
        list.add(new User("王五",125556));
        list.add(new User("关羽",322216));
        Scanner sc = new Scanner(System.in);
        int count = 3;
        while (count>0){
            System.out.println("请输入用户名:");
            String name = sc.next();
            System.out.println("请输入密码:");
            int password = sc.nextInt();
            for (Object o:list) {
                User user = (User) o;
                if (user.name.equals(name)&&user.password==password) {
                    System.out.println("登录成功!");
                    System.exit(0);
                }
            }
            System.out.println("登录失败!");
            count--;
        }
        System.out.println("三次机会已用完!");
    }
}
