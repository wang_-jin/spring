package com.igeek.ch04.collection_test.setwork.test6;

import com.igeek.day04.Array;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/26 9:17
 * @description TODO
 */
public class Test {
    /*6.输入任意一个字符串(只包含字母和数字),要求将字符串中所包含的每一位数字取出并存储在ArrayList中
 1>.打印ArrayList中所有数字相加之和. 比如 "ab3y8mm9" 输出20
 2>.将数字进行升序排序*/
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String str = sc.next();
        Pattern ptn = Pattern.compile("\\d");
        Matcher matcher = ptn.matcher(str);
        while (matcher.find()){
            int i  = Integer.parseInt(matcher.group());
            list.add(i);
        }
        System.out.println(list);
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum+=(int)list.get(i);
        }
        Collections.sort(list);
        System.out.println("和为:"+sum);
        System.out.println(list);

    }
}
