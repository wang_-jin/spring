package com.igeek.ch04.collection_test.setwork.test1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/26 9:13
 * @description TODO
 */
public class Test {
    /*1.从控制台输入一个字符串，请统计其中小写字母、大写字母、数字的个数，并输出。*/
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        String str = sc.next();
        int mchar = 0;
        int bchar = 0;
        int num = 0;
        Pattern ptn1 = Pattern.compile("[a-z]");
        Pattern ptn2 = Pattern.compile("[A-Z]");
        Pattern ptn3 = Pattern.compile("\\d");
        Matcher matcher1 = ptn1.matcher(str);
        Matcher matcher2 = ptn2.matcher(str);
        Matcher matcher3 = ptn3.matcher(str);
        while (matcher1.find()){
            mchar++;
        }
        while (matcher2.find()){
            bchar++;
        }
        while (matcher3.find()){
            num++;
        }
        System.out.println("小写字母个数:"+mchar);
        System.out.println("大写字母个数:"+bchar);
        System.out.println("数字个数:"+num);
    }
}
