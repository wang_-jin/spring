package com.igeek.ch04.collection_test.setwork.game;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/27 19:00
 * @description TODO
 */
public class Computer {
    Game game;
    String name;
    int score;

    public Computer() {
    }

    public Computer(int rival){
        String[] arr= new String[]{"关羽", "曹操", "刘备"};
        name = arr[rival-1];
    }
    void showFist(int m,User user) {
        int i = (int)(Math.random()*3+1);
        switch (i){
            case 1:
                System.out.println(this.name+"出拳: 剪刀");
                break;
            case 2:
                System.out.println(this.name+"出拳: 石头");
                break;
            case 3:
                System.out.println(this.name+"出拳: 布");
                break;
            default:
                System.out.println("输入格式有误!");
                break;
        }
        if (m==i){
            System.out.println("和局,真衰!嘿嘿,等着瞧吧!");

        }else if (m==1&&i==3||m==2&&i==1||m==3&&i==2){
            user.score++;
            this.score--;
            System.out.println("恭喜!你赢了");
        }else {
            System.out.println("你输了,真笨!");
            user.score--;
            this.score++;
        }
    }


}
