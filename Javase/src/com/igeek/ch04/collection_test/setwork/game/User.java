package com.igeek.ch04.collection_test.setwork.game;

/**
 * @author wangjin
 * 2023/8/27 19:00
 * @description TODO
 */
public class User {
    String name;
    int score;

    public User(String name) {
        this.name = name;
    }

    String showFist(int i) {
        switch (i){
            case 1:
                return "剪刀";
            case 2:
                return "石头";
            case 3:
                return "布";
            default:
                System.out.println("输入格式有误!");
                break;
        }
        return null;
    }
}
