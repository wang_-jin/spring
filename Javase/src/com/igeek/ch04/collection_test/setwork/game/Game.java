package com.igeek.ch04.collection_test.setwork.game;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/27 19:00
 * @description TODO
 */
public class Game {
    void startGame(Computer computer,User user,int count){
        System.out.println(computer.name+"  VS  "+user.name);
        System.out.println("对战次数:"+count);
        System.out.println();
        System.out.println("姓名      得分");
        System.out.println(computer.name+"      "+computer.score);
        System.out.println(user.name+"      "+user.score);
        if (computer.score<user.score){
            System.out.println("恭喜恭喜!");
        }else if (computer.score==user.score){
            System.out.println("旗鼓相当!");
        }else {
            System.out.println("呵呵!真笨,下次加油啊");
        }
    }

}
