package com.igeek.ch04.collection_test.setwork.game;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/27 18:58
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("----------欢迎进入游戏世界----------");
        System.out.println(sc.nextLine());
        System.out.println("*****************");
        System.out.println("**  猜拳  开始  **");
        System.out.println("*****************");
        System.out.println(sc.nextLine());

        Game game = new Game();
        System.out.println("出拳规则: 1.剪刀 2.石头 3.布");
        //System.out.print("请选择对方角色"+"(1: 关羽 2: 曹操 3: 刘备) :");
        boolean f = true;
        int count = 0;
        while (f) {
            System.out.print("请选择对方角色"+"(1: 关羽 2: 曹操 3: 刘备) :");
            int rival = sc.nextInt();
            System.out.print("请输入你的姓名:");
            String name = sc.next();
            User user = new User(name);
            Computer computer = new Computer(rival);
            System.out.println("你选择了"+computer.name+"对战");
            System.out.print("要开始吗? (y/n)");
            String flag = sc.next();
            if (flag.equals("y")) {
                System.out.print("请出拳:1.剪刀 2.石头 3.布 (输入相应数字) :");
                int m = sc.nextInt();
                computer.showFist(m, user);
                count++;
            }else {
                System.out.println("这把不想玩了!");
                f = false;
            }
            boolean f1 = true;
            while (f1) {
                System.out.print("是否开始下一轮(y/n):");
                flag = sc.next();
                if (flag.equals("n")) {
                    f1 = false;
                    game.startGame(computer, user, count);
                    System.out.print("要开始下一局吗(y/n):");

                    String flag1 = sc.next();
                    if (flag1.equals("y")){
                        f = true;
                        break;

                    }
                    else if (flag1.equals("n")){
                        System.exit(0);
                    }
                }
                else if (flag.equals("y")) {
                    System.out.print("请出拳:1.剪刀 2.石头 3.布 (输入相应数字) :");
                    int m = sc.nextInt();
                    computer.showFist(m, user);
                    count++;
                }
            }
        }
    }
}
