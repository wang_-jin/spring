package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test2;

import java.util.HashSet;
import java.util.Set;

public class Test {
  public static void main(String[] args) {
    /*
    2.求一个字符串"aiodjl;hriWFUADJSV123UEHiowfjnivowe"中一共
    有几个不重复的字母,区分大小写,如a,A算两个字符(HashSet)
    */
    String str = "aiodjl;hriWFUADJSV123UEHiowfjnivowe";
    Set set = new HashSet();
    //将字符串转为字符数组
    char[] arr = str.toCharArray();
    //将字符数组中元素添加到HashSet中
    for (char c : arr) {
      set.add(c);
    }
    System.out.println(set.size());
  }
}
