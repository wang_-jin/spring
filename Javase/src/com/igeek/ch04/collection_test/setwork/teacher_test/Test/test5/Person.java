package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test5;

import java.util.Objects;

public class Person {
  String name;
  int age;

  public Person() {
  }

  public Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  @Override
  public String toString() {
    return "Person{" +
      "name='" + name + '\'' +
      ", age=" + age +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    System.out.println("equals");
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Person person = (Person) o;
    //如果名字相同我们就认为是相同的对象
    return Objects.equals(name, person.name);
  }

  @Override
  public int hashCode() {
    /*
    根据名字生成哈希码,相同的名字生成的哈希码相同,
    在哈希表中表示的哈希桶索引也相同
    */
    return Objects.hash(name);
  }
}
