package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Test4 {
  /*
  4.模拟登陆,编写用户类,用户类有用户名和密码两个属性
  1>.给集合中存储5个用户对象
  2>.从控制台输入用户名和密码,和集合中的对象信息进行比较,相同表示成功,不同表示失败
  3>.有三次输入机会
  */
  public static void main(String[] args) {
    List list = new ArrayList();
    list.add(new User("a","123"));
    list.add(new User("b","1234"));
    list.add(new User("c","111"));
    list.add(new User("d","222"));
    list.add(new User("e","333"));
    Scanner sc = new Scanner(System.in);
    int i =1;
    while(i<=3){
      System.out.println("请输入用户名和密码:");
      String str = sc.next();
      String[] arr = str.split(",");
      System.out.println(Arrays.toString(arr));
      for (Object o:list){
        User u = (User)o;
        if((u.name.equals(arr[0]))&&(u.psw.equals(arr[1]))){
          System.out.println("登录成功!");
          return;
        }
      }
      i++;

    }


  }
}
