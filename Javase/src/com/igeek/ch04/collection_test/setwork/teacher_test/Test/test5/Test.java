package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test5;

import java.util.*;

public class Test {
  /*
  5.定义方法去除ArrayList集合中重复元素
  1>.假设存入的是字符串元素,进行去重
  2>.假设存入的是自定义对象元素(如Person对象,根据name去重),进行去重
  */
  public static void main(String[] args) {
    List list = new ArrayList();
    list.add("a");
    list.add("b");
    list.add("b");
    list.add("c");
    System.out.println(distinctString(list));

    List list1 = new ArrayList();
    list1.add(new Person("刘备", 18));
    list1.add(new Person("刘备", 55));
    list1.add(new Person("张飞", 45));
    list1.add(new Person("关羽", 30));
    list1.add(new Person("诸葛", 18));
    System.out.println(distinctObject(list1));
  }

  //去重对象
  static List distinctObject(List list) {
    /*
    HashSet去重原理:
      添加元素时会调用HashCode方法判断哈希表中HashCode位置哈希桶是否为空,
      如果为空直接添加;如果已经存在对象,则会进一步调用Equals方法判断两个对象
      属性是否相同,如果相同说明是重复对象不会添加,如果不同则添加对象!
    */
    Set set = new HashSet(list);
    return new ArrayList<>(set);
  }

  //去重字符串
  static List distinctString(List list) {
    Set set = new HashSet(list);
    list = new ArrayList(set);
    return list;
  }
}
