package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test6;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
  static List getNumber(String str){
    Pattern ptn = Pattern.compile("\\d");
    Matcher matcher = ptn.matcher(str);
    List list = new ArrayList();
    while (matcher.find()){
      String s = matcher.group();
      list.add(Integer.parseInt(s));
    }
    return list;
  }
  public static void main(String[] args) {
    /*
    6.输入任意一个字符串(只包含字母和数字),要求将字符串中所包含的每一位
    数字取出并存储在ArrayList中
    1>.打印ArrayList中所有数字相加之和. 比如 "ab8y3mm9" 输出20
    2>.将数字进行升序排序
    */
    List list = getNumber("ab8y3mm9");
    //求和
    int sum = 0;
    for (Object i:list){
      sum+=(Integer)i;
    }
    System.out.println("求和:"+sum);
    //排序
    list = new ArrayList(new TreeSet(list));
    System.out.println("升序:"+list);
  }
}
