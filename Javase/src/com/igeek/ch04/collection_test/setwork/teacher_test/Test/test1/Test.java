package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
  /*
  1.从控制台输入一个字符串,请统计其中小写字母,大写字母,数字的个数,并输出
  */
  static int charCount(String input,String regex){
    Pattern ptn = Pattern.compile(regex);
    Matcher matcher = ptn.matcher(input);
    int count = 0;
    while (matcher.find()){
      count++;
    }
    return count;
  }
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("请输入一个字符串:");
    String s = sc.next();
    int a = charCount(s,"[A-Z]");
    System.out.println("大写字母个数:"+a);
    int b = charCount(s,"[a-z]");
    System.out.println("小写字母个数:"+b);
    int c = charCount(s,"[0-9]");
    System.out.println("数字个数:"+c);
  }
}
