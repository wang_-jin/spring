package com.igeek.ch04.collection_test.setwork.teacher_test.Test.test3;

import java.util.Set;
import java.util.TreeSet;

public class Test {
  static String sort(String str){
    Set set = new TreeSet();
    String[] arr = str.split(" ");
    for (int i = 0; i < arr.length; i++) {
      set.add(Integer.parseInt(arr[i]));
    }
    return set.toString();
  };
  public static void main(String[] args) {
    /*
    3.TreeSet练习将字符串中的数值进行排序,
    例如String str="8 10 15 5 2 7"; 2,5,7,8,10,15  使用 TreeSet完成
    */
    System.out.println(sort("8 10 15 5 2 7"));
  }
}
