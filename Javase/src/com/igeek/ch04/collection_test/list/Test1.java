package com.igeek.ch04.collection_test.list;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/8/25 11:18
 * @description TODO
 */
public class Test1 {
    /*2.运用List完成下面的要求:
创建一个List,在List中增加三个工人,基本信息如下:
   姓名  年龄  工资
   Tom   18  3000
   Peter 25  3500
   Mark  22  3200
   Bob   26  3800
1.在第二行插入一个工人,信息为:姓名:Robert,年龄:24,工资3300;
2.删除姓名为"Mark"的工人;
3.将Tom的工资改为4000;
4.打印出1号员工Tom的信息;*/
    public static void main(String[] args) {
        List list = new ArrayList<>();
        list.add(new Student("Tom", 18, 3000));
        list.add(new Student("Peter", 25, 3500));
        list.add(new Student("Mark", 22, 3200));
        list.add(new Student("Bob", 26, 3800));
        list.add(1, new Student("Robert", 24, 3300));

        for (Object o : list) {
            Student s = (Student) o;
            if (s.name == "Mark") {
                list.remove(s);
            }
        }
        System.out.println(list);
        for (Object o : list) {
            Student s = (Student) o;
            if (s.name == "Tom") {
                s.salary = 4000;
            }
        }
        System.out.println(list);
        //list.remove(list.get());

        System.out.println(list.get(0));
    }
}
