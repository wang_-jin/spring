package com.igeek.ch04.collection_test.list;

/**
 * @author wangjin
 * 2023/8/25 11:21
 * @description TODO
 */
public class Student {
    String name;
    int age;
    int salary;

    public Student() {
    }

    public Student(String name, int age, int salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
