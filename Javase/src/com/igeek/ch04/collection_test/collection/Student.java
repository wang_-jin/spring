package com.igeek.ch04.collection_test.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/8/24 16:36
 * @description TODO
 */
public class Student {
    /*定义一个Student类,有姓名,年龄,分数属性,
创建一个Collection集合,将下面的学生对象存储到集合中:
  1. 姓名:刘备 年龄:48 分数:88
  2. 姓名:关羽 年龄:46 分数:87
  3. 姓名:张飞 年龄:44 分数:55
  4. 姓名:刘禅 年龄:20 分数:28
  5. 姓名:诸葛 年龄:30 分数:100
并完成以下任务:
1.打印出集合中元素个数;
2.打印集合中所有学生对象的信息;
3.计算出集合中所有学生的平均分;
4.打印出分数最高的学生信息;
5.打印不及格学生的数量*/
    String name;
    int age;
    int score;

    public Student() {
    }

    public Student(String name, int age, int score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", score=" + score +
                '}';
    }

    public static void main(String[] args) {
        Student s1 = new Student("刘备",48,88);
        Student s2 = new Student("关羽",46,87);
        Student s3 = new Student("张飞",44,55);
        Student s4 = new Student("刘禅",20,28);
        Student s5 = new Student("诸葛",30,100);
        Collection c = new ArrayList<>();
        c.add(s1);
        c.add(s2);
        c.add(s3);
        c.add(s4);
        c.add(s5);
        //打印出集合中元素个数;
        System.out.println("元素个数:"+c.size());
        //打印集合中所有学生对象的信息;
/*        for (Object item:c) {
            System.out.println(item.toString());
        }*/
        Iterator iterator = c.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        //计算出集合中所有学生的平均分;
        int sum = 0;

        for (Object stu:c) {
            Student ss = (Student) stu;
                    sum+=ss.score;
        }
        System.out.println("平均分:"+sum/c.size());
        //打印出分数最高的学生信息;
        int max = 0;
        for (Object stu: c) {
            Student ss = (Student) stu;
            if (ss.score>max){
                max = ss.score;
            }
        }
        for (Object stu:c) {
            Student ss = (Student) stu;
            if (max==ss.score){
                System.out.println("高分学生信息:"+ss);
            }
        }
        //打印不及格学生的数量
        int num = 0;
        for (Object stu:c) {
            Student ss = (Student) stu;
            if (ss.score<60){
                num++;
            }
        }
        System.out.println("不及格学生数量:"+num);
    }
}
