package com.igeek.ch04.collection_test.set;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author wangjin
 * 2023/8/25 15:33
 * @description TODO
 */
public class Student{
    String name;
    int chineseGrade;
    int mathGrade;
    int englishGrade;
    int sum;
    public int getSum(){
        return this.sum=(chineseGrade+mathGrade+englishGrade);
    }

    public Student() {
    }

    public Student(String name, int chineseGrade, int mathGrade, int englishGrade) {
        this.name = name;
        this.chineseGrade = chineseGrade;
        this.mathGrade = mathGrade;
        this.englishGrade = englishGrade;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return chineseGrade
     */
    public int getChineseGrade() {
        return chineseGrade;
    }

    /**
     * 设置
     * @param chineseGrade
     */
    public void setChineseGrade(int chineseGrade) {
        this.chineseGrade = chineseGrade;
    }

    /**
     * 获取
     * @return mathGrade
     */
    public int getMathGrade() {
        return mathGrade;
    }

    /**
     * 设置
     * @param mathGrade
     */
    public void setMathGrade(int mathGrade) {
        this.mathGrade = mathGrade;
    }

    /**
     * 获取
     * @return englishGrade
     */
    public int getEnglishGrade() {
        return englishGrade;
    }

    /**
     * 设置
     * @param englishGrade
     */
    public void setEnglishGrade(int englishGrade) {
        this.englishGrade = englishGrade;
    }

    public String toString() {
        return "Student{name = " + name + ", chineseGrade = " + chineseGrade + ", mathGrade = " + mathGrade + ", englishGrade = " + englishGrade + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return chineseGrade == student.chineseGrade && mathGrade == student.mathGrade && englishGrade == student.englishGrade && sum == student.sum && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, chineseGrade, mathGrade, englishGrade, sum);
    }
    /*1）定义一个Student类,成员变量:姓名,语文成绩,数学成绩,英语成绩,总分
成员方法:无参构造,有参构造,有参构造的参数分别是姓名,语文成绩,数学成绩,
英语成绩,toString方法,需要重写,遍历学生类打印不是内存地址,而是属性的值。
2）创建键盘录入对象,需要导入Scanner类
3）创建TreeSet集合对象,传入参数是比较器,总分降序
4）录入5个学生信息,集合的size就是等于5,超过不继续存储,
录入的字符串形式为:刘备,89,78,67 你需要使用逗号切割,
返回一个字符串数组,还需要把成绩字符串转成Int类型,然后把键盘录入的信息
填入到学生对象中,然后把学生对象添加到TreeSet集合中,遍历TreeSet集合,打印学生信息*/
}
