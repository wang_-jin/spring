package com.igeek.ch04.collection_test.set;

import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;


/**
 * @author wangjin
 * 2023/8/25 15:40
 * @description TODO
 */
public class Test {
    /*1）定义一个Student类,成员变量:姓名,语文成绩,数学成绩,英语成绩,总分
成员方法:无参构造,有参构造,有参构造的参数分别是姓名,语文成绩,数学成绩,
英语成绩,toString方法,需要重写,遍历学生类打印不是内存地址,而是属性的值。
2）创建键盘录入对象,需要导入Scanner类
3）创建TreeSet集合对象,传入参数是比较器,总分降序
4）录入5个学生信息,集合的size就是等于5,超过不继续存储,
录入的字符串形式为:刘备,89,78,67 你需要使用逗号切割,
返回一个字符串数组,还需要把成绩字符串转成Int类型,然后把键盘录入的信息
填入到学生对象中,然后把学生对象添加到TreeSet集合中,遍历TreeSet集合,打印学生信息*/
    public static void main(String[] args) {
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
/*                if (((Student) o1).sum > ((Student) o2).sum) {
                    return ((Student) o1).sum-((Student) o2).sum;
                }else if(((Student) o1).sum < ((Student) o2).sum){
                    return ((Student) o2).sum-((Student) o1).sum;
                }else {
                    // 总分相等时按照姓名进行排序
                    return ((Student) o1).name.compareTo(((Student) o2).name);
                }*/
                if(o1 instanceof Student &&o2 instanceof Student){
                    /*降序排列  传入对象大于当前对象即>0 传入对象排前面,否则排后面*/
                    //return ((Student)o2).getSum()-((Student)o1).getSum();//降序排列
                    /*升序排列  当前对象小于传入对象即<0 传入对象排后面,否则排前面*/
                    return ((Student)o1).getSum()-((Student)o2).getSum(); //升序排列
                }
                return 0;
            }
        };
        TreeSet set = new TreeSet(comparator);
        Scanner sc = new Scanner(System.in);
        for (int i = 1;i<=5;i++) {
            System.out.println("请输入第"+i+"个学生信息:");
            String s = sc.next();
            boolean flag = s.matches("(.+),(\\d{1,3},){2}\\d{1,3}");
            if (flag){
                String[] ss = s.split(",");
                Integer i1 = new Integer(ss[1]);
                Integer i2 = new Integer(ss[2]);
                Integer i3 = new Integer(ss[3]);
                Student s1 = new Student(ss[0],i1,i2,i3);
                set.add(s1);
            }else {
                System.out.println("格式有误!");
                return;
            }
        }
        set.forEach(o -> {
            System.out.println(o);
        });
    }
}
