package com.igeek.ch04.collection_test.foreach;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * @author wangjin
 * 2023/8/24 20:31
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Collection c = new ArrayList<>();
        c.add(1);
        c.add(2);
        c.add(3);
        c.add(4);
        /*foreach 迭代*/
        for (Object item: c) {
            System.out.println(item);
        }
        System.out.println("----------");
        /*迭代器*/
        Iterator iterator = c.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("-----------");
        /*lambda*/
        c.forEach(new Consumer() {
            @Override
            public void accept(Object o) {
                System.out.println(o);
            }
        });
        /*简写形式*/
/*        c.forEach(o -> {
            System.out.println(o);
        });*/
    }
}
