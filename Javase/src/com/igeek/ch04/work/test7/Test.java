package com.igeek.ch04.work.test7;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/23 15:47
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Person person1 = new Person("张三",20,180);
        Person person2 = new Person("李四",20,170);
        ArrayList<Person> list = new ArrayList<>();
        list.add(person1);
        list.add(person2);
        Iterator<Person> iterator = list.iterator();
        while(iterator.hasNext()){
            Person person = iterator.next();
            person.setAge(person.getAge()+2);
        }
        for (Person person : list) {
            System.out.println("姓名:"+person.getName()+", 年龄:"+person.getAge()+", 身高:"+person.getHeight());
        }
    }
}
