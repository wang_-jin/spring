package com.igeek.ch04.work.test7;

/**
 * @author wangjin
 * 2023/9/23 15:46
 * @description TODO
 */
/*自定义人类:包含姓名,年龄,身高属性.私有成员变量,生成无参,有参构造方法,
生成get/set方法.创建5个人对象放到ArrayList中.使用迭代器获取每个人对象.
将每个人的年龄加2岁.再使用增强for打印每个人的信息*/
public class Person {
    private String name;
    private int age;
    private int height;

    public Person() {
    }

    public Person(String name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * 设置
     * @param height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + ", height = " + height + "}";
    }
}
