package com.igeek.ch04.work.test8;

import java.util.ArrayList;

/**
 * @author wangjin
 * 2023/9/23 15:52
 * @description TODO
 */
/*编写一个deleteElements方法，接收一个ArrayList,ArrayList中存放元素的类型不确定，
在deleteElements方法的删除ArrayList中的第一个和最后一个元素
1.	定义deleteElements(ArrayList<?> array)方法
2.	在deleteElements方法中删除第一个元素
3.	删除最后一个元素
4.	创建ArrayList
5.	往ArrayList添加元素
6.	调用deleteElements方法传入array
7.	打印array
*/
public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        deleteElements(list);
    }
    public static void deleteElements(ArrayList<?> array){
        array.remove(array.get(array.size()-1));
        array.remove(array.get(0));
        System.out.println(array);
    }
}
