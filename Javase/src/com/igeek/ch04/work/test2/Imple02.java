package com.igeek.ch04.work.test2;

/**
 * @author wangjin
 * 2023/9/23 11:07
 * @description TODO
 */
public class Imple02<E> implements Inter<E>{

    @Override
    public void show(E e) {
        System.out.println(e);
    }
}
