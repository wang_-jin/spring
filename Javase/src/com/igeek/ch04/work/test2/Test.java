package com.igeek.ch04.work.test2;

/**
 * @author wangjin
 * 2023/9/23 11:10
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Imple01 imple01 = new Imple01();
        imple01.show("nihao");
        Imple02<Integer> imple02 = new Imple02<>();
        imple02.show(456);
    }

}
