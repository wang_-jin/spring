package com.igeek.ch04.work.test6;

import java.util.ArrayList;

/**
 * @author wangjin
 * 2023/9/23 15:43
 * @description TODO
 */
/*有一个数组 String arr[]={"abc","bad","abc","aab","bad","cef","jhi"};
创建一个ArrayList，将arr数组里面的元素添加进ArrayList中去，但ArrayList中元素不能重复 (arr数组不变)*/
public class Test3 {
    public static void main(String[] args) {
        String arr[]={"abc","bad","abc","aab","bad","cef","jhi"};
        ArrayList<String> list = new ArrayList<>();
        for (String s : arr) {
            if (!list.contains(s)){
                list.add(s);
            }
        }
        System.out.println(list);
    }
}
