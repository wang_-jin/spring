package com.igeek.ch04.work.test6;

import com.igeek.ch04.work.test2.Inter;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/23 15:14
 * @description TODO
 */
/*编写一个泛形方法名称为swap，实现指定位置数组元素的交换.数组和要交换的索引作为方法参数*/
public class Test {
    public static void main(String[] args) {
        String[] arr1 = {"asd","ert","zxc"};
        Integer[] arr2 = {1,2,3,4};
        String[] swap1 = swap(arr1, 0, 2);
        System.out.println("swap1 = " + Arrays.toString(swap1));
        Integer[] swap2 = swap(arr2, 1, 3);
        System.out.println("swap2 = " + Arrays.toString(swap2));
    }
    public static <E> E[] swap(E[] arr,int index1,int index2){
        E temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
        return arr;
    }
}
