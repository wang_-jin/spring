package com.igeek.ch04.work.test6;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/23 15:24
 * @description TODO
 */
/*编写一个泛形方法，接收一个任意类型数组，并反转数组中的所有元素*/
public class Test1 {
    public static <E> E[] foo(E[] arr){
        for (int i = 0; i < arr.length / 2; i++) {
            E temp = arr[i];
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = temp;
        }
        return arr;
    }

    public static void main(String[] args) {
        Integer[] foo = foo(new Integer[]{1, 2, 3, 4});
        String[] foo1 = foo(new String[]{"a", "b", "c"});
        System.out.println("foo = " + Arrays.toString(foo));
        System.out.println("Arrays.toString(foo1) = " + Arrays.toString(foo1));
    }
}
