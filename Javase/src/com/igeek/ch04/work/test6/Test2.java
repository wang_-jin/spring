package com.igeek.ch04.work.test6;

/**
 * @author wangjin
 * 2023/9/23 15:36
 * @description TODO
 */
/*编写一个泛形方法，接收一个任意类型数组，并将数组中的元素按照一定的格式打印.” [3, 6, 7, 1, 2] ”*/
public class Test2 {
    public static void main(String[] args) {
        foo(new Integer[]{3,6,7,1,2});
    }
    public static <E> void foo(E[] arr){
        for (int i = 0; i < arr.length; i++) {
            if (i==0){
                System.out.print("["+arr[i]);
            }else if (i == arr.length-1){
                System.out.print(", "+arr[i]+"]");
            }else {
                System.out.print(", "+arr[i]);
            }
        }
    }
}
