package com.igeek.ch04.work.test1;

/**
 * @author wangjin
 * 2023/9/23 10:45
 * @description TODO
 */
/*自定义泛型类GenericClass<E>,包含E类型的成员变量,提供get/set方法,并使用这个泛型类*/
public class Test2 {
    public static void main(String[] args) {
        GenericClass<String> genericClass = new GenericClass();
        genericClass.setValue("abc");
        System.out.println("genericClass.getValue() = " + genericClass.getValue());
    }
}
class GenericClass<E>{
    private E value;

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }
}
