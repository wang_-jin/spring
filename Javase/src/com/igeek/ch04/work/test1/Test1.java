package com.igeek.ch04.work.test1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/23 10:16
 * @description TODO
 */
/*往ArrayList添加以下元素” abc”, ” igeek”,
5.练习ArrayList不加泛型和加泛型2种情况迭代出ArrayList中的元素.获取元素的长度.并说出泛型的好处*/
public class Test1 {
    public static void main(String[] args) {
        method1();
        System.out.println("-------------------------------");
        method2();
    }
    public static void method1(){
        ArrayList list = new ArrayList<>(Arrays.asList("abc","igeek",1,2));
        Iterator iterator = list.iterator();
        while(iterator.hasNext()){
            String next = (String) iterator.next();
            System.out.println("next = " + next);
        }
        System.out.println("list.size() = " + list.size());
    }
    public static void method2(){
        ArrayList<Object> list = new ArrayList<>();
        list.add("abc");
        list.add("igeek");
        list.add(1);
        list.add(2);
        Iterator iterator = list.iterator();
        while(iterator.hasNext()){
            System.out.println("iterator.next() = " + iterator.next());
        }
        System.out.println(list);
        System.out.println("list.size() = " + list.size());
    }
    /*泛型的优点
    1、类型安全
    泛型的主要目的是提高Java程序的类型安全。通过知道使用泛型定义的变量的类型限制，
    编译器可以在非常高的层次上验证类型假设。没有泛型，这些假设就只能存在于系统开发人员的头脑中。

    通过在变量声明中捕获这一附加的类型信息，泛型允许编译器实施这些附加的类型约束。
    类型错误就可以在编译时被捕获了，而不是在运行时当作ClassCastException展示出来。
    将类型检查从运行时挪到编译时有助于Java开发人员更早、更容易地找到错误，并可提高程序的可靠性。

    2、消除强制类型转换
    泛型的一个附带好处是，消除源代码中的许多强制类型转换。这使得代码更加可读，
    并且减少了出错机会。尽管减少强制类型转换可以提高使用泛型类的代码的累赞程度，
    但是声明泛型变量时却会带来相应的累赞程度。在简单的程序中使用一次泛型变量不会降低代码累赞程度。
    但是对于多次使用泛型变量的大型程序来说，则可以累积起来降低累赞程度。所以泛型消除了强制类型转换之后，
    会使得代码加清晰和筒洁。

    3、更高的效率
    在非泛型编程中，将筒单类型作为Object传递时会引起Boxing（装箱）和Unboxing（拆箱）操作，
    这两个过程都是具有很大开销的。引入泛型后，就不必进行Boxing和Unboxing操作了，
    所以运行效率相对较高，特别在对集合操作非常频繁的系统中，这个特点带来的性能提升更加明显。

    4、潜在的性能收益
    泛型为较大的优化带来可能。在泛型的初始实现中，
    编译器将强制类型转换（没有泛型的话，Java系统开发人员会指定这些强制类型转换）插入生成的字节码中。
    但是更多类型信息可用于编译器这一事实，为未来版本的JVM的优化带来可能。
*/
}
