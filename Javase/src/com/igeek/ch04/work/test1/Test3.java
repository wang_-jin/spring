package com.igeek.ch04.work.test1;

/**
 * @author wangjin
 * 2023/9/23 10:52
 * @description TODO
 */
/*自定义泛型方法function.可以传入任意类型数据t.在这个方法中打印传入的t.并使用这个泛型方法*/
public class Test3 {
    public static <T> void function(T t){
        if (t instanceof String){
            System.out.println("((String) t).length() = " + ((String) t).length());
        }else if (t instanceof Integer){
            System.out.println("t*t = " + ((Integer)t) * ((Integer)t));
        }
    }

    public static void main(String[] args) {
        //function("abcdef");
        function(12);
    }
}
