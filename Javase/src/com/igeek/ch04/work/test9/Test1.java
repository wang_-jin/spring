package com.igeek.ch04.work.test9;

/**
 * @author wangjin
 * 2023/9/23 17:04
 * @description TODO
 */
/*统计101-200之间有多少个素数，并输出所有素数。素数又叫质数，
就是除了1和它本身之外，再也没有整数能被它整除的数。*/
public class Test1 {
    public static void main(String[] args) {
        int count = foo(101, 105);
        System.out.println("count = " + count);
    }
    public static int foo(int a, int b){
        int count = 0;
        for (int i = a; i <= b; i++) {
            for (int j = 2; j <= a/2; j++) {
                if (i%j==0){
                    count++;
                    break;
                }
            }
        }
        return count;
    }
}
