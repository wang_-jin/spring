package com.igeek.ch04.work.test9;

/**
 * @author wangjin
 * 2023/9/23 16:49
 * @description TODO
 */
/*编写一个泛型方法求两个数之和.两个数可以传入int、long、float、double类型.
提示:泛型不能使用基本数据类型,需要使用引用数据类型.Integer,Long,Float,Double都是Number的子类.
Number有doubleValue()方法*/
public class Test {
    public static void main(String[] args) {
        foo(1.2323,3.1212);
        foo(2,6);
        foo(2L,6L);
        foo(5.6F,8.4F);
    }
    public static <E extends Number> void foo(E value,E key){
        if (value instanceof Integer && key instanceof Integer){
            System.out.println("和"+((Integer) value+(Integer) key));
        }else if (value instanceof Long && key instanceof Long){
            System.out.println("和"+((Long) value+(Long) key));
        }else if (value instanceof Float && key instanceof Float){
            System.out.println("和"+((Float) value+(Float) key));
        }else if (value instanceof Double && key instanceof Double){
            System.out.println("和"+((Double) value+(Double) key));
        }
    }
}
