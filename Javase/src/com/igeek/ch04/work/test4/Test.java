package com.igeek.ch04.work.test4;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/23 14:51
 * @description TODO
 */
/*自定义人类:包含姓名,年龄,身高属性.私有成员变量,生成无参,有参构造方法,生成get/set方法.
创建5个人放到ArrayList中.使用迭代器获取每个人的信息.找出最高的人,最矮的人并输出最高人和最矮人的信息.
打印格式如下:最高的人是张三,身高1.80. 最矮的人是李四,身高1.60*/
public class Test {
    public static void main(String[] args) {
        ArrayList<Person>list = new ArrayList<>();
        list.add(new Person("王五",18,1.70));
        list.add(new Person("张三",19,1.80));
        list.add(new Person("赵钱",20,1.73));
        list.add(new Person("李四",18,1.60));
        list.add(new Person("孙李",20,1.69));
        Iterator<Person> iterator = list.iterator();
        Person p1 = new Person();
        Person p2 = new Person("",0,2.0);
        while(iterator.hasNext()){
            Person person = iterator.next();
            if (person.getHeight()> p1.getHeight()){
                p1 = person;
            }
            if (person.getHeight()<p2.getHeight()){
                p2 = person;
            }
        }
        System.out.println("最高的人是"+p1.getName()+", 身高"+p1.getHeight());
        System.out.println("最矮的人是"+p2.getName()+", 身高"+p2.getHeight());
    }
}
