package com.igeek.ch04.work.test3;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/23 11:15
 * @description TODO
 */
/*自定义学生类:包含姓名,年龄,成绩属性.私有成员变量,生成无参,有参构造方法,生成get/set方法.
创建5个学生放到ArrayList中.使用迭代器获取每个学生信息.统计总分,平均分,最高分,最低分并输出*/
public class Test {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        list.add(new Student("张三",18,60));
        list.add(new Student("李四",20,55));
        list.add(new Student("王五",19,70));
        list.add(new Student("赵钱",21,88));
        list.add(new Student("孙李",18,99));
        int sum = 0;
        double max = 0;
        double min = 100;
        System.out.println("list = " + list);
        Iterator<Student> iterator = list.iterator();
        while (iterator.hasNext()){
            Student stu = (Student)(iterator.next());
            sum+=stu.getGrade();
            if (stu.getGrade()>max){
                max = stu.getGrade();
            }
            if (stu.getGrade()<min){
                min = stu.getGrade();
            }
        }
        //下面这块代码每执行一次iterator都会返回下一个迭代器中下一个元素,并且会将迭代器位置向后移动。
        // 每次循环都调用了五次`iterator.next()`方法，因此会导致每次循环时获取的元素都不同。
        // 而在前面的代码中，只调用了一次`iterator.next()`方法，并将其赋值给了变量`stu`，所以在循环体内使用的都是同一个元素。
//        Iterator<Student> iterator = list.iterator();
//        while (iterator.hasNext()){
//            //Student stu = (Student)(iterator.next());
//            System.out.println("---");
//            sum+=iterator.next().getGrade();
//            if (iterator.next().getGrade()>max){
//                max = iterator.next().getGrade();
//            }
//            if (iterator.next().getGrade()<min){
//                min = iterator.next().getGrade();
//            }
//        }
        System.out.println("总分 = " + sum);
        System.out.println("平均分 = " + sum/ list.size());
        System.out.println("最高分 = " + max);
        System.out.println("最低分 = " + min);
    }
}
