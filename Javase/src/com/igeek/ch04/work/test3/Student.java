package com.igeek.ch04.work.test3;

/**
 * @author wangjin
 * 2023/9/23 11:13
 * @description TODO
 */
public class Student {
    private String name;
    private int age;
    private double grade;


    public Student() {
    }

    public Student(String name, int age, double grade) {
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return grade
     */
    public double getGrade() {
        return grade;
    }

    /**
     * 设置
     * @param grade
     */
    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String toString() {
        return "Student{name = " + name + ", age = " + age + ", grade = " + grade + "}";
    }
}
