package com.igeek.ch04.work.test5;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/23 15:06
 * @description TODO
 */
/*ArrayList中有如下数据:”a”, ”b”, ”c”, ”c”, ”a”, ”b”, ”b”, ”b”, ”a”.
定义名为:frequency(ArrayList arr, String key)的方法.arr是ArrayList集合,
key是要查找的某个元素.使用增强for方式查找key在ArrayList中出现的次数.并将次数作为方法的返回值.
在mian方法中调用frequency方法*/
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("a", "b", "c", "c","a","b","b","b","a"));
        System.out.println( "b 出现的次数:" + frequency(list, "b"));
    }
    public static int frequency(ArrayList<String> arr, String key){
        int count = 0;
        for (String s : arr) {
            if (s.equals(key)){
                count++;
            }
        }
        return count;
    }
}
