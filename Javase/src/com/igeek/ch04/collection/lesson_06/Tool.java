package com.igeek.ch04.collection.lesson_06;

import java.util.TreeSet;

public class Tool {
    public static void main(String[] args) {
        /*
        自然排序:
          TreeSet中传入数字或字符串可以按从小到大的顺序自然排序
        */
        TreeSet treeSet = new TreeSet();
        treeSet.add(11);
        treeSet.add(2);
        treeSet.add(99);
        treeSet.add(3);
        treeSet.add(1);
        System.out.println(treeSet);
    }
}
