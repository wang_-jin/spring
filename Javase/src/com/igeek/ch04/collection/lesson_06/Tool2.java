package com.igeek.ch04.collection.lesson_06;

import java.util.Comparator;
import java.util.TreeSet;

public class Tool2 {
    public static void main(String[] args) {
        /*
        定制排序:
          TreeSet有参构造方法可以传一个Comparator接口,
          我们可以传一个匿名内部类作为参数,重写compare方法,
          来实现排序功能
        */
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Dog&&o2 instanceof Dog){
                   return ((Dog)o1).age-((Dog)o2).age;
                }
                return 0;
            }
        };
        TreeSet treeset = new TreeSet(comparator);
        treeset.add(new Dog("小白",18));
        treeset.add(new Dog("小红",88));
        treeset.add(new Dog("小黑",12));
        treeset.add(new Dog("小黄",56));
        treeset.add(new Dog("小花",4));
        System.out.println(treeset);
    }
}
