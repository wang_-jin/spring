## TreeSet介绍
TreeSet是基于红黑树实现的Set集合,
它可以保证元素的有序性.具体来说,TreeSet中的元素是
按照它们的自然顺序或指定的比较器顺序进行排序的.
因此,当需要保证元素的有序性时,可以使用TreeSet.

TreeSet常用的方法也和Collection一样,几乎没有新添加自己的方法!