package com.igeek.ch04.collection.lesson_06;

import java.util.TreeSet;

public class Tool1 {
    public static void main(String[] args) {
        TreeSet treeset = new TreeSet();
        /*
        在Person中实现Comparable接口,并重写compareTo方法,可以
        自定义按某一个属性排序
        */
        treeset.add(new Person("刘备",18));
        treeset.add(new Person("关羽",88));
        treeset.add(new Person("张飞",12));
        treeset.add(new Person("诸葛",56));
        treeset.add(new Person("赵云",4));
        System.out.println(treeset);
    }
}
