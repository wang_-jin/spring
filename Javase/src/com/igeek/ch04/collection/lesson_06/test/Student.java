package com.igeek.ch04.collection.lesson_06.test;

public class Student {
  String name;
  int language;
  int math;
  int english;
  //总分
  private int sum;
  public int getSum(){
    return language+math+english;
  }

  public Student(){};

  public Student(String name, int language, int math, int english) {
    this.name = name;
    this.language = language;
    this.math = math;
    this.english = english;
  }

  @Override
  public String toString() {
    return "Student{" +
      "name='" + name + '\'' +
      ", language=" + language +
      ", math=" + math +
      ", english=" + english +
      ", sum=" + getSum() +
      '}';
  }
}
