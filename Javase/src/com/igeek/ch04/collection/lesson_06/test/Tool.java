package com.igeek.ch04.collection.lesson_06.test;
import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeSet;

/*
1）定义一个Student类,成员变量:姓名,语文成绩,数学成绩,英语成绩,总分
成员方法:无参构造,有参构造,有参构造的参数分别是姓名,语文成绩,数学成绩,
英语成绩,toString方法,需要重写,遍历学生类打印不是内存地址,而是属性的值。
2）创建键盘录入对象,需要导入Scanner类
3）创建TreeSet集合对象,传入参数是比较器,总分降序
4）录入5个学生信息,集合的size就是等于5,超过不继续存储,
录入的字符串形式为:刘备,89,78,67 你需要使用逗号切割,
返回一个字符串数组,还需要把成绩字符串转成Int类型,然后把键盘录入的信息
填入到学生对象中,然后把学生对象添加到TreeSet集合中,遍历TreeSet集合,打印学生信息
*/
public class Tool {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    TreeSet treeSet = new TreeSet(new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        if(o1 instanceof Student&&o2 instanceof Student){
          return ((Student)o2).getSum()-((Student)o1).getSum();
        }
        return 0;
      }
    });
    int i = 1;
    while(i<=5){
      System.out.println("请输入第"+(treeSet.size()+1)+"个学生信息:");
      String stu = sc.next();
      String[] infoArr = stu.split(",");
      String name = infoArr[0];
      int language = Integer.parseInt(infoArr[1]);
      int math = Integer.parseInt(infoArr[2]);
      int english = Integer.parseInt(infoArr[3]);
      Student s = new Student(name,language,math,english);
      treeSet.add(s);
      i++;
    }
    System.out.println(treeSet);

  }
}
