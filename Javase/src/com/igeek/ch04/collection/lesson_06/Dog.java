package com.igeek.ch04.collection.lesson_06;

public class Dog {
    String name;
    int age;
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    public Dog(){};
    public Dog(String name,int age){
        this.name = name;
        this.age = age;
    }
}
