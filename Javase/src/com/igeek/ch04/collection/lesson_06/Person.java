package com.igeek.ch04.collection.lesson_06;
/*
自然排序:
  实现Comparable接口,重写compareTo方法进行排序
*/
public class Person implements Comparable{
    String name;
    int age;
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    @Override
    public int compareTo(Object o) {
        /*
        instanceof判断左边的实例是否是右边类的实例,
        或者判断左边实例是否实现了右边接口,返回值是bool类型:
        实例对象 instanceof 类;
        实例对象 instanceof 接口;
        */
        if(o instanceof Person){
            Person p = (Person)o;
            /*
            升序:this.age-p.age
            降序:p.age-this.age
            */
            return this.age-p.age;
        }
        return 0;
    }
    public Person(){};
    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }
}
