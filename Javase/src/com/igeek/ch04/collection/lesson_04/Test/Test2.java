package com.igeek.ch04.collection.lesson_04.Test;

import java.util.ArrayList;
/*
2.运用List完成下面的要求:
创建一个List,在List中增加三个工人,基本信息如下:
   姓名  年龄  工资
   Tom   18  3000
   Peter 25  3500
   Mark  22  3200
   Bob   26  3800
1.在第二行插入一个工人,信息为:姓名:Robert,年龄:24,工资3300;
2.删除姓名为"Mark"的工人;
3.将Tom的工资改为4000;
4.打印出1号员工Tom的信息;
*/
public class Test2 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(new Worker("Tom",18,3000));
        list.add(new Worker("Peter",25,3500));
        list.add(new Worker("Mark",22,3200));
        list.add(new Worker("Bob",26,3800));
        //在第二行插入一个工人,信息为:姓名:Robert,年龄:24,工资3300
        list.add(1,new Worker("Robert",24,3300));
        //删除姓名为"Mark"的工人;
        int index = 0;
        for (Object o : list) {
            Worker w = (Worker)o;
            if(w.name == "Mark"){
                index = list.indexOf(w);
            }
        }
        list.remove(index);
        System.out.println(list);
        //3.将Tom的工资改为4000;
        int index1 = 0;
        for (Object o : list) {
            Worker w = (Worker)o;
            if(w.name == "Tom"){
                index1 = list.indexOf(w);
            }
        }
        Worker w = (Worker)list.get(index1);
        w.salary = 4000;
        System.out.println(list);
        //4.打印出1号员工Tom的信息;
        System.out.println(list.get(0));

    }

}
