package com.igeek.ch04.collection.lesson_04.Test;

import java.util.ArrayList;

public class Test1 {
    public static void main(String[] args) {
        /*
        1.将字符串"a","b","a","c","d","c"存入到ArrayList,并去掉重复元素
        */
        ArrayList list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("a");
        list.add("c");
        list.add("d");
        list.add("c");
        for (int i = 0; i < list.size(); i++) {
            String item = (String)list.get(i);
            //如果元素重复了,将其删除
            if(list.indexOf(item) != i){
                list.remove(i);
            }
        }
        System.out.println(list);

    }
}
