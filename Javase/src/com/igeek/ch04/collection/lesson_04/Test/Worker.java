package com.igeek.ch04.collection.lesson_04.Test;

public class Worker {
  String name;
  int age;
  int salary;

  public Worker(){};
  public Worker(String name, int age, int salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Worker{" +
      "name='" + name + '\'' +
      ", age=" + age +
      ", salary=" + salary +
      '}';
  }
}
