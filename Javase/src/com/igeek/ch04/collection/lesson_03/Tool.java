package com.igeek.ch04.collection.lesson_03;

import java.util.ArrayList;
import java.util.List;

public class Tool {
    public static void main(String[] args) {
        List list =new ArrayList();
        list.add(11);
        list.add(22);
        list.add(33);
        list.add(44);
        list.add(11);
        System.out.println("获取索引为2的元素:"+list.get(2));
        System.out.println("获取11第一次出现的索引:"+list.indexOf(11));
        System.out.println("获取11最后一次出现的索引:"+list.lastIndexOf(11));
        list.remove(1);
        System.out.println("移除索引是1的元素:"+list);
        list.set(0,99);
        System.out.println("将索引是0的元素改为99:"+list);
        list.add(0,110);
        System.out.println("在索引0的位置插入一个110:"+list);
    }
}
