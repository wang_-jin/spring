## List接口特点
1.有序性:List中的元素是按照它们添加的顺序排列的,可以通过索引访问每个元素
2.允许重复元素:和Collection接口一样,List接口也允许存储重复的元素。
3.可变性:List接口提供了添加,删除,修改元素等操作,使得它可以动态地调整集合大小和内容
4.支持索引访问:List接口中定义了一些访问和修改元素的方法,
如get,set,add,remove等操作,可以通过索引来访问和修改元素

## List接口常用方法
因为List接口继承自Collection接口,所以Collection拥有的方法List都可以
使用,List增加了很多对于索引操作的方法,这是Collection没有的.
# 增加
(仍然用的是Collection接口提供的add方法)

# 删除
移除此列表中指定位置的元素
remove(int index)

# 修改和插入
用指定元素替换此列表中指定位置的元素
set(int index, E element)
在列表中指定的位置上插入指定的元素
add(int index, E element)

# 查找
返回此列表中指定位置的元素
get(int index)
返回此列表中指定元素的第一个出现的索引,如果此列表不包含该元素返回-1
indexOf(Object o)
返回此列表中指定元素的最后一个出现的索引,如果此列表不包含该元素返回-1
lastIndexOf(Object o)


