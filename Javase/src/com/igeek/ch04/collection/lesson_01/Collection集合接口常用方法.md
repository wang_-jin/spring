## 集合常用方法
1.add添加元素
2.addAll将指定集合中的所有元素添加到这个集合
3.toArray将集合转为数组
4.contains(Object o)如果集合包含指定元素则返回true
5.containsAll(Collection c)如果这个集合包含指定集合的所有元素则返回true
6.equals(Object o)判断两个集合中元素是否相同
7.isEmpty如果集合不包含任何元素则返回true
8.remove(Object o)从这个集合中移除指定元素的一个实例,如果它是存在的
9.removeAll(Collection c)从当前集合中删除另一个集合中拥有的所有元素
10.retainAll(Collection c)在当前集合中仅仅保留指定集合中的所有元素
11.size返回集合中元素的个数
12.clear从这个集合中移除所有的元素