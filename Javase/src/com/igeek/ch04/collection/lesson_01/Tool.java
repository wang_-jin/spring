package com.igeek.ch04.collection.lesson_01;

import java.util.ArrayList;
import java.util.Collection;

public class Tool {
  public static void main(String[] args) {
        /*
        因为Collection是一个接口,不能实例化,如果我们想要使用
        Collection中的方法,需要借助于子实现类,
        使用多态将Collection接口的引用指向子实现类的实例对象
        */
    Collection col = new ArrayList();

    /*1.add添加元素*/
    col.add("a");
    col.add("b");
    col.add("c");

    /*2.addAll将指定集合中的所有元素添加到这个集合*/
    Collection col1 = new ArrayList();
    col1.add(1);
    col1.add(2);
    col1.add(3);
    col.addAll(col1);
        /*
        注意:
          Collection打印直接打印的是集合内容而不是哈希码,
          这是因为ArrayList的父类AbstractCollection重写了
          toString方法,虽然ArrayList本身没有重写toString方法,
          但是程序运行时它调用父类的toString因此将集合内容打印了出来
        */
    System.out.println("col:" + col);

    /*3.toArray将集合转为数组*/
    Object[] arr = col.toArray();
        /*
        注意:
          直接打印数组时会显示一串包含哈希码的字符串,
          从这串字符串的开头字母组成我们可以看出数组元素的类型:
            [I 一维int数组
            [[I 二维int数组
            [J 一维long数组
            [L 一维对象数组
            [Z 一维boolean数组
          总结:
            数组类型一般为基本类型的首字母大写,只有L,J,Z需要特别记忆一下
          */
    System.out.println("arr:"+arr);

    /*4.contains(Object o)如果集合包含指定元素则返回true*/
    boolean flag = col.contains(1);
    System.out.println("是否包含1:" + flag);

        /*5.containsAll(Collection c)
        如果这个集合包含指定集合的所有元素则返回true*/
    System.out.println("是否包含col1中所有元素:" + col.containsAll(col1));

    /*6.equals(Object o)判断两个集合中元素是否相同*/
    Collection col2 = new ArrayList(col);
    System.out.println(col.equals(col2));

    /*7.isEmpty如果集合不包含任何元素则返回true*/
    System.out.println(col.isEmpty());

    /*8.remove(Object o)从这个集合中移除指定元素的一个实例,如果它是存在的*/
    col.remove(1);
    System.out.println(col);

    /*9.removeAll(Collection c)从当前集合中删除另一个集合中拥有的所有元素*/
    col.removeAll(col1);
    System.out.println(col);

    /*10.retainAll(Collection c)在当前集合中仅仅保留指定集合中的所有元素*/
    Collection col3 = new ArrayList();
    col3.add("a");
    col3.add("b");
    col.retainAll(col3);
    System.out.println(col);

    /*11.size返回集合中元素的个数*/
    System.out.println(col.size());

    /*12.clear从这个集合中移除所有的元素*/
    col.clear();
    System.out.println(col);

  }
}
