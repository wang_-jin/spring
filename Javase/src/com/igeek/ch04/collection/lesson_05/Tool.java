package com.igeek.ch04.collection.lesson_05;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Tool {
    public static void main(String[] args) {
        //HashSet例子
        /*Set set = new HashSet();
        set.add("a");
        set.add("a");
        set.add("a");
        set.add(22222);
        set.add("b");
        set.add(4);
        set.add("ccc");
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            //打印出来的数据是无序的,不可重复的
            System.out.println(iterator.next());
        }*/

        //LinkedHashSet例子
        Set set = new LinkedHashSet();
        set.add("a");
        set.add("a");
        set.add("a");
        set.add(22222);
        set.add("b");
        set.add(4);
        set.add("ccc");
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            //打印出来的数据是有序的,不可重复的
            System.out.println(iterator.next());
        }
    }
}
