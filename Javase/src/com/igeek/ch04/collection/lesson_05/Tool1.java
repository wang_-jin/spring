package com.igeek.ch04.collection.lesson_05;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Tool1 {
    /*使用Set将List去重*/
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        //使用Set去重,List转Set
        HashSet set = new HashSet(list);
        //Set再转为List
        list = new ArrayList(set);
        System.out.println(list);
    }
}
