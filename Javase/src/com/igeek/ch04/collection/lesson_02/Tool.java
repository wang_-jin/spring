package com.igeek.ch04.collection.lesson_02;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;

public class Tool {
    public static void main(String[] args) {
        Collection col = new ArrayList();
        col.add("a");
        col.add("b");
        col.add("c");
        col.add("d");

        //方法1.加强for循环
        /*for (Object o : col) {
            System.out.println(o);
        }*/

        /*
        方法2.Iterator迭代器
        所有的集合类都可以使用迭代器的原因:
          Collection接口继承自Iterable接口,而Iterable接口定义了一个方法：
          Iterator<T> iterator(),该方法返回一个Iterator对象,可用于遍历集合中的元素.
          因此,所有实现了Iterable接口的集合类都可以使用迭代器,
          这包括Java中所有的集合类,如ArrayList,LinkedList,HashSet,
          TreeSet,HashMap等
        */

        /*Iterator iterator = col.iterator();
        //判断集合中下一个元素是否存在
        while (iterator.hasNext()){
            //获取集合中下一个元素
            System.out.println(iterator.next());
        }*/

        //方法3.Lambda表达式
/*        col.forEach(o->{
            System.out.println(o);
        });
        //上面代码是下面匿名函数的简写形式
        col.forEach(new Consumer() {
            @Override
            public void accept(Object o) {
                System.out.println(o);
            }
        });*/

    }
}
