package com.igeek.ch04.collection.lesson_02.test;

import java.util.ArrayList;
import java.util.Collection;

public class Tool {

  /*
  定义一个Student类,有姓名,年龄,分数属性,
创建一个Collection集合,将下面的学生对象存储到集合中:
  1. 姓名:刘备 年龄:48 分数:88
  2. 姓名:关羽 年龄:46 分数:87
  3. 姓名:张飞 年龄:44 分数:55
  4. 姓名:刘禅 年龄:20 分数:28
  5. 姓名:诸葛 年龄:30 分数:100
并完成以下任务:
1.打印出集合中元素个数;
2.打印集合中所有学生对象的信息;
3.计算出集合中所有学生的平均分;
4.打印出分数最高的学生信息;
5.打印不及格学生的数量
  */
  public static void main(String[] args) {
    Collection col = new ArrayList();
    //添加对象到集合
    col.add(new Student("刘备", 48, 88));
    col.add(new Student("关羽", 46, 87));
    col.add(new Student("张飞", 44, 55));
    col.add(new Student("刘禅", 20, 28));
    col.add(new Student("诸葛", 30, 100));
    //打印集合元素个数
    System.out.println("集合中元素个数"+col.size());
    //打印疾患中所有对象信息
    col.forEach(s->{
      System.out.println(s);
    });
    //计算平均分
    int average = 0;
    int sum = 0;
    for (Object s: col) {
      Student s1 = (Student)s;
      sum+=s1.score;
    }
    average = sum/col.size();
    System.out.println("平均分是:"+average);
    //打印分数最高的学生信息
    int max = 0;
    Student maxStudent = null;
    for (Object s: col) {
      Student s1 = (Student)s;
      if(s1.score>max){
        max = s1.score;
        maxStudent = s1;
      }
    }
    System.out.println("分数最高的学生信息:"+maxStudent);
    //打印不及格学生的数量
    int count = 0;
    for (Object s: col) {
      Student s1 = (Student)s;
      if(s1.score<60){
        count++;
      }
    }
    System.out.println("不及格的学生数量:"+count);
  }
}
