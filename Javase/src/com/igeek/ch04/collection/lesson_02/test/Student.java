package com.igeek.ch04.collection.lesson_02.test;

public class Student {
  String name;
  int age;
  int score;
  public Student(){};

  public Student(String name, int age, int score) {
    this.name = name;
    this.age = age;
    this.score = score;
  }

  @Override
  public String toString() {
    return "Student{" +
      "name='" + name + '\'' +
      ", age=" + age +
      ", score=" + score +
      '}';
  }
}
