package com.igeek.ch04.work1;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author wangjin
 * 2023/9/24 16:48
 * @description TODO
 */
/*定义一个Student类,包含名称,年龄,性别(姓名,年龄,性别完全相同视为同一学生)
创建10个Student对象,至少有两个学生姓名,年龄,性别完全相同. 把这10个学生添加到Set集合中,不可以重复,
遍历Set集合打印学生信息,使用两种方式
*/
class Student{
    private String name;
    private int age;
    private Character gender;

    public Student() {
    }

    public Student(String name, int age, Character gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return gender
     */
    public Character getGender() {
        return gender;
    }

    /**
     * 设置
     * @param gender
     */
    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String toString() {
        return "Student{name = " + name + ", age = " + age + ", gender = " + gender + "}";
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name,this.age,this.gender);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){return true;}
        if (obj == null || this.getClass()!= obj.getClass()){return false;}
        Student student = (Student) obj;
        return Objects.equals(this.name,student.getName()) && this.age == student.getAge() && Objects.equals(this.gender,student.getGender());
    }
}
public class Test7 {
    public static void main(String[] args) {
//定义一个Student类,包含名称,年龄,性别(姓名,年龄,性别完全相同视为同一学生)
//创建10个Student对象,至少有两个学生姓名,年龄,性别完全相同. 把这10个学生添加到Set集合中,不可以重复,
//遍历Set集合打印学生信息,使用两种方式
        Student stu1 = new Student("1",1,'女');
        Student stu2 = new Student("2",2,'男');
        Student stu3 = new Student("1",1,'女');
        Student stu4 = new Student("4",4,'男');
        Student stu5 = new Student("2",2,'男');
        Set<Student> set = new HashSet<>();
        set.add(stu1);
        set.add(stu2);
        set.add(stu3);
        set.add(stu4);
        set.add(stu5);
        for (Student student : set) {
            System.out.println("student = " + student);
        }
    }
}
