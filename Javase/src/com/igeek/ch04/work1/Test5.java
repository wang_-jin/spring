package com.igeek.ch04.work1;

import java.util.HashSet;

/**
 * @author wangjin
 * 2023/9/24 16:42
 * @description TODO
 */
/*向HashSet集合添加姓名{张三,李四,王五,二丫,钱六,孙七},将二丫删除,添加王小丫*/
public class Test5 {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("张三");
        set.add("李四");
        set.add("王五");
        set.add("二丫");
        set.add("钱六");
        set.add("孙七");
        System.out.println("set = " + set);
        set.remove("二丫");
        System.out.println("set = " + set);
        set.add("王小丫");
        System.out.println("set = " + set);
    }
}
