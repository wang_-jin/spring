package com.igeek.ch04.work1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @author wangjin
 * 2023/9/24 15:57
 * @description TODO
 */
/*往HashSet中添加字符串"zhangsan", "lisi", "wangwu", "zhangsan".使用迭代器获取HashSet中的元素.*/
public class Test1 {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>(Arrays.asList("zhangsan", "lisi", "wangwu", "zhangsan"));
        Iterator<String> iterator = set.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
