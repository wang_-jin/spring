package com.igeek.ch04.work1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author wangjin
 * 2023/9/24 17:02
 * @description TODO
 */
/*有一个数组 String arr[]={"abc","bad","abc","aab","bad","cef","jhi"};
创建一个ArrayList，将数组里面的元素添加进ArrayList，但元素不能重复(使用HashSet去除重复元素)*/
public class Test8 {
    public static void main(String[] args) {
        String arr[]={"abc","bad","abc","aab","bad","cef","jhi"};
        ArrayList<String> list = new ArrayList<>(Arrays.asList(arr));
        System.out.println("list = " + list);
        HashSet<String> set = new HashSet<>(list);
        System.out.println("set = " + set);
    }
}
