package com.igeek.ch04.work1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author wangjin
 * 2023/9/24 16:39
 * @description TODO
 */
/*ArrayList有以下元素: "a","f","b","c","a","d"利用HashSet对ArrayList集合去重
(最终结果: ArrayList中没有重复元素)*/
public class Test4 {
    public static void main(String[] args) {
        String[] arr = {"a","f","b","c","a","d"};
        ArrayList<String> list = new ArrayList<>(Arrays.asList(arr));
        System.out.println(list);
        HashSet<String> set = new HashSet<>(list);
        System.out.println(set);
    }
}
