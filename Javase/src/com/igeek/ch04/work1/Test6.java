package com.igeek.ch04.work1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author wangjin
 * 2023/9/24 16:45
 * @description TODO
 */
/*已知数组存放一批QQ号码.QQ号码最长为11位,最短为5位
String[] strs = {"12345","67891",1"2347809933","98765432102","67891","12347809933"}
将该数组里面的所有qq号都存放在LinkedList中,将list中重复元素删除,将list中所有元素用两种方式打印出来
*/
public class Test6 {
    public static void main(String[] args) {
        String[] strs = {"12345","67891","12347809933","98765432102","67891","12347809933"};
        LinkedList<String> list = new LinkedList<>(Arrays.asList(strs));
        HashSet<String> set = new HashSet<>(list);
        Iterator<String> iterator = set.iterator();
        while(iterator.hasNext()){
            System.out.println("iterator.next() = " + iterator.next());
        }
        for (String s : set) {
            System.out.println("s = " + s);
        }
    }
}
