package com.igeek.ch04.work1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

/**
 * @author wangjin
 * 2023/9/24 16:33
 * @description TODO
 */
/*使用LinkedHashSet存储以下元素:"王昭君","王昭君","西施","杨玉环","貂蝉".使用迭代器和增强for循环遍历LinkedHashSet*/
public class Test3 {
    public static void main(String[] args) {
        String[] str = {"王昭君","王昭君","西施","杨玉环","貂蝉"};
        Integer[] arr = {1,2,3,4,5};
        LinkedHashSet<String> set = new LinkedHashSet<>(Arrays.asList(str));
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>(Arrays.asList(arr));
        for (String s : set) {
            System.out.println("s = " + s);
        }
        System.out.println(linkedHashSet);
        Iterator<Integer> iterator = linkedHashSet.iterator();
        while(iterator.hasNext()){
            System.out.println("iterator.next() = " + iterator.next());
        }
    }
}
