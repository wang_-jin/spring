package com.igeek.ch04.work1;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author wangjin
 * 2023/9/24 16:07
 * @description TODO
 */
class Person {
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
    //在hashCode()方法中，我们使用了一个常数31来计算哈希值，然后将年龄和姓名的hashCode()值进行混合计算，
    // 最后返回计算得到的结果。
    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.age);
    }
    //在equals()方法中，我们首先判断两个对象是否是同一个对象，如果是则直接返回true。
    // 然后再判断两个对象的类型是否相同，如果不同则直接返回false。
    // 接着比较两个对象的年龄和姓名是否相等，如果都相等则返回true，否则返回false。
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Person person = (Person) obj;
        return Objects.equals(this.name,person.getName()) && this.age == person.getAge();
    }
}
public class Test2 {
    public static void main(String[] args) {
        Set<Person> set = new HashSet<>();
        set.add(new Person("张三",18));
        set.add(new Person("王五",20));
        set.add(new Person("张三",18));
        System.out.println(set);
    }
}
