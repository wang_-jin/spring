package com.igeek.ch07.lambda.lesson_01;

import java.util.TreeSet;

public class Tool {
    public static void main(String[] args) {
        /*
        Comparator接口里面只有compare一个抽象方法所以可以使用
        lambda简写
        注意:
          Comparator接口里面的equals是从Object继承的抽象方法,
          在父类已经实现了该方法,因此lambda简写的仅仅是compare方法
        */
        /*TreeSet<Person> set = new TreeSet<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.age-o2.age;
            }
        });*/

        /*TreeSet<Person> set = new TreeSet<>((Person o1,Person o2)->{
                return o1.age-o2.age;
        });*/

        TreeSet<Person> set = new TreeSet<>((o1, o2) -> o1.age- o2.age);
        set.add(new Person("刘备",18));
        set.add(new Person("关羽",14));
        set.add(new Person("张飞",56));
        set.add(new Person("诸葛",23));
        set.add(new Person("赵云",12));
        System.out.println(set);
    }
}
