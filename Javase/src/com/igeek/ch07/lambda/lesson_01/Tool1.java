package com.igeek.ch07.lambda.lesson_01;

import java.util.Arrays;
import java.util.List;

public class Tool1 {
    /*
    forEach方法中的Consumer可以使用lambda简写,
    因为Consumer接口只有一个抽象方法accept
    */
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,3,5,7,9);
        /*list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer i) {
                System.out.println(i);
            }
        });*/
        list.forEach(i->{
            System.out.println(i);
        });
    }
}
