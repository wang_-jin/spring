package com.igeek.ch07.lambda.lesson_02;
@FunctionalInterface
public interface Animal {
    void eat();
}
