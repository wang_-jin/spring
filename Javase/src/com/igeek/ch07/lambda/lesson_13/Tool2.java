package com.igeek.ch07.lambda.lesson_13;

import java.util.ArrayList;
import java.util.List;

public class Tool2 {
    /*
    使用reduce和map将对象属性求和
    */
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        list.add(new Person("刘备",10));
        list.add(new Person("关羽",20));
        list.add(new Person("张飞",30));
        Integer a =  list.stream().map(Person::getAge).reduce(0,Integer::sum);
        System.out.println(a);

    }
}
