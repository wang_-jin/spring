package com.igeek.ch07.lambda.lesson_13.test;
import java.util.Arrays;
import java.util.List;

public class Test4 {
    /*
    4.计算Integer集合中大于6的元素的个数
    */
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(12,3,66,7,89,4);
        Long a = list.stream().filter(s->s>6).count();
        System.out.println(a);
    }
}
