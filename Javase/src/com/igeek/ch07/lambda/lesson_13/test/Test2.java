package com.igeek.ch07.lambda.lesson_13.test;


import java.util.Arrays;
import java.util.List;

public class Test2 {
    /*
    2.获取String集合中最长的元素,并打印,例如"a","att","da","tyua"
    */
    public static void main(String[] args) {
        List<String> list = Arrays.asList("a","att","da","tyua");
        String s = list.stream().reduce("",(result,item)->{
            if(item.length()>result.length()){
                result = item;
            }
            return result;
        });
        System.out.println(s);
    }

}
