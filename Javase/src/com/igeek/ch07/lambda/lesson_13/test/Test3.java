package com.igeek.ch07.lambda.lesson_13.test;

import java.util.Arrays;
import java.util.List;

public class Test3 {
    /*
    3.获取Integer集合中最大值,并打印,例如6,7,3,8,1,2,9
    */
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(6,7,3,8,1,2,9);
        Integer a = list.stream().max(Integer::compare).orElse(0);
        System.out.println(a);
    }
}
