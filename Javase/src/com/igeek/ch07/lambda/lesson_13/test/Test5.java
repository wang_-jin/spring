package com.igeek.ch07.lambda.lesson_13.test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test5 {
    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("刘备",6700));
        list.add(new Employee("关羽",9800));
        list.add(new Employee("张飞",9950));
        list.add(new Employee("诸葛",1200));
        list.add(new Employee("赵云",3400));
        list.add(new Employee("曹操",9400));
        //5.1.筛选员工中工资高于8000的人,并形成新的集合
        List<Employee> list1 = list.stream().filter(s->s.getSalary()>8000).collect(Collectors.toList());
        System.out.println(list1);
        System.out.println("---------------");
        //5.2获取员工工资最高的人
        //写法1
        Employee e = list.stream().max((a1,a2)->a1.getSalary()-a2.getSalary()).orElse(new Employee());
        //写法2
        Employee e1 = list.stream().max(Comparator.comparingInt(Employee::getSalary)).orElse(new Employee());
        System.out.println(e);
        System.out.println("------------------");
        //5.3.将员工按照工资由低到高排序
        list.stream().sorted(Comparator.comparingInt(Employee::getSalary))
                .forEach(System.out::println);
        System.out.println("-------------------");
        //5.4.将员工按照工资由高到低排序
        list.stream().sorted(Comparator.comparingInt(Employee::getSalary).
                reversed()).forEach(System.out::println);
        System.out.println("------------------------");

        //5.5求员工的平均薪资
        //使用mapToInt转为整数流,好处是可以使用sum,average等方法了
        double a1 = list.stream().mapToInt(Employee::getSalary).average().orElse(0);
        System.out.println(a1);
    }
}
