package com.igeek.ch07.lambda.lesson_13.test;

import java.util.Arrays;
import java.util.List;

public class Test1 {
    /*
    1.筛选出Integer集合中大于7的元素,并打印,例如6,7,3,8,1,2,9
    */
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(6,7,3,8,1,2,9);
        list.stream().filter(s->s>7).forEach(System.out::println);
    }
}
