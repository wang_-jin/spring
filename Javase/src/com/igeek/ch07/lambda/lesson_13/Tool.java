package com.igeek.ch07.lambda.lesson_13;

import java.util.stream.Stream;

public class Tool {
    public static void main(String[] args) {
        //reduce传一个参数
        Stream<Integer> stream = Stream.of(1,3,5,7,9);
        Integer s = stream.reduce((result, item) -> {
            /*
            reduce方法中的lambda表达式会进行for循环操作;
            result是每次循环return的结果,第一次的值默认是集合的第一个元素;
            item是集合中的每个元素;

            注意:
            reduce方法的返回值是Optional类型,如果想获取到最后的
            结果,可以使用orElse(默认值)方法,将Optional转为结果
            的类型,orElse的参数是一个默认值,当集合为空时,最后结果
            会取默认值
            */
            System.out.println("result:"+result);
            System.out.println("item:"+item);
            return result + item;
        }).orElse(0);
        System.out.println(s);
        System.out.println("------------------");
        Stream<Integer> stream1 = Stream.of(1,3,5,7,9);
        //reduce传2个参数
        Integer s1 = stream1.reduce(0, (result, item) -> {
            /*
            第一个参数是result的默认值
            第二个参数是lambda表达式
            注意:
              写两个参数时,reduce的返回值默认是第一个参数类型,
              不是Optional了,不用写orElse了
            */
            System.out.println("result:"+result);
            System.out.println("item:"+item);
            return result + item;
        });
        System.out.println(s1);
    }
}
