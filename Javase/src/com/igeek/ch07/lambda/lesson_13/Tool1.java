package com.igeek.ch07.lambda.lesson_13;

import java.util.stream.Stream;

public class Tool1 {
    public static void main(String[] args) {
        //方法引用
        Stream<Integer> stream = Stream.of(1,3,5,7,9);
        //求和
        Integer s = stream.reduce(0,Integer::sum);
        System.out.println(s);
        //求最大值
        Stream<Integer> stream1 = Stream.of(1,3,5,7,9);
        Integer s1 = stream1.reduce(0,Integer::max);
        System.out.println(s1);
    }
}
