## 收集Stream结果
1.将Stream中元素收集到List中有两种方法
1.1toList()返回一个不可变的List
1.2collect(Collections.toList())返回一个可变的List

2.将Stream中元素收集到Set中
2.1toSet()
2.2collect(Collections.toSet())

3.将Stream中元素收集到数组中
toArray