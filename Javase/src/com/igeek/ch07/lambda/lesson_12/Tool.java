package com.igeek.ch07.lambda.lesson_12;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Tool {
    public static void main(String[] args) {
        List<Integer> list= Arrays.asList(1, 3, 5, 7, 9);
        List list1 = list.stream().filter(s->s>3).collect(Collectors.toList());
        System.out.println(list1);
        //修改toList返回的list会报错,因为返回的是不可变list
        //list1.add(99);
        System.out.println("------------------");

        /*
        修改collect(Collectors.toList())返回的list不会报错,
        因为返回的是可变的list
        */
        List list2 = list.stream().limit(3).collect(Collectors.toList());
        System.out.println(list2);
        list2.add(99);
        System.out.println(list2);
        System.out.println("--------------");
        //收集Stream元素到数组
        Object[] arr = list.stream().skip(2).toArray();
        System.out.println(Arrays.toString(arr));

    }
}
