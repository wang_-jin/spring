package com.igeek.ch07.lambda.lesson_04.test;

import java.util.function.Supplier;

public class Test1 {
    public static Integer max(Supplier<Integer> supplier){
        return supplier.get();
    }

    public static void main(String[] args) {
        Integer[] arr = {1,22,4,5,66,21,7};
        Integer m = max(()->{
            int a= 0;
            for (Integer i : arr) {
                if(i>a){
                    a = i;
                }
            }
            return a;
        });
        System.out.println(m);
    }
}
