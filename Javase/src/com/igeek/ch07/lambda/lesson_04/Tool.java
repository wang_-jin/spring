package com.igeek.ch07.lambda.lesson_04;

import java.util.function.Supplier;

public class Tool {
    public static String upper(Supplier<String> supplier){
        return supplier.get();
    }

    public static void main(String[] args) {
        /*
        Supplier<返回值类型>:
        这个接口我们也叫供给型函数式接口,它的作用是通过返回值提供数据,
        他是没有参数的,也就是说不提供处理参数的逻辑,Supplier的抽象方法:
        T get();
    */
        /*将小写字符串转为大写*/
        String str = upper(()->{
            return "hello".toUpperCase();
        });
        System.out.println(str);
    }
}
