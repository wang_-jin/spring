## Function转换型函数式接口
在Java中,Function是一个函数式接口,
它定义了一个有参有返回值的方法R apply(T t),
用于将传入的T类型转换为R类型
抽象方法:R apply(T t)
默认方法:andThen(Function function)在第一次操作的基础上进行第二次操作