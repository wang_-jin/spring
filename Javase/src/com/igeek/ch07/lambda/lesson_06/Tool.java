package com.igeek.ch07.lambda.lesson_06;

import java.util.function.Function;

public class Tool {
    /*
    获取字符串长度方法,将String类型转为Integer类型
    */
    public static Integer getLength(Function<String,Integer>function,String str){
        return function.apply(str);
    }

    public static void main(String[] args) {
        getLength(s->Integer.parseInt(s),"hello");
    }
}
