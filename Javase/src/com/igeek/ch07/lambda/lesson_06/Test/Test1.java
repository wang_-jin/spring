package com.igeek.ch07.lambda.lesson_06.Test;

import java.util.function.Function;

/*
请使用Function进行函数模型的拼接,按照顺序需要执行的多个函数操作为:
String str = "张三,20";
将字符串截取数字部分,得到字符串;
将上一步的字符串转换成为int类型的数字;
将上一步的int数字加上100,得到结果int数字
*/
public class Test1 {
    public static Integer join(Function<String,String>one,
                               Function<String,Integer>two,
                               Function<Integer,Integer>three,
                               String str){
        return one.andThen(two).andThen(three).apply(str);
    }
    public static void main(String[] args) {
        Integer s =  join(str->{
            return str.split(",")[1];
        },str->{
            return Integer.parseInt(str);
        },i->{
            return i+100;
        },"张三,20");
        System.out.println(s);
    }
}
