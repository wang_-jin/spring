package com.igeek.ch07.lambda.lesson_06;

import java.util.function.Function;

public class Tool1 {
    /*
    使用andThen将String转Integer加1,再将Integer转String
    */
    public static String transform(Function<String,Integer>one,Function<Integer,String>two,String str){
      return one.andThen(two).apply(str);
    }

    public static void main(String[] args) {
        /*
        下面会先将"100"转为100,加1得到101,再将101转为"101"
        */
        String s = transform(str->{
            return Integer.parseInt(str)+1;
        },i->{
            return i+"";
        },"100");
        System.out.println(s);
    }
}
