package com.igeek.ch07.lambda.lesson_10;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Tool {
    public static void main(String[] args) {
        /*
        1.List集合
        使用stream()方法
        2.Set集合
        使用Stream方法
        3.Map
          3.1map.keySet().stream()获取键的stream对象
          3.2map.values().stream()获取值的stream对象
          3.3map.entrySet.stream()获取键值对的stream对象
        4.数组
          4.1Stream.of(数组对象)获取stream对象
          4.2Arrays.stream(数组对象)获取stream对象
        */

        //1.List-->Stream
        List<String> list = Arrays.asList("adfssf","bdsf","adfg","drrr");
        /*Stream<String> stream = list.stream();
        stream.filter(s->s.startsWith("a")).forEach(System.out::println);*/

        //Set-->Stream
        /*Set set = new HashSet(list);
        Stream stream1 = set.stream();
        stream1.forEach(System.out::println);*/


        Map map = new HashMap();
        map.put(1,"a");
        map.put(2,"b");
        map.put(3,"c");
        map.put(4,"d");
        //Map-->键的Stream对象
        /*Stream stream = map.keySet().stream();
        stream.forEach(System.out::println);*/

        //Map-->值的Stream对象
        /*Stream stream = map.values().stream();
        stream.forEach(System.out::println);*/

        //Map-->键值对的Stream对象
        /*Stream stream = map.entrySet().stream();
        stream.forEach(System.out::println);*/

        //数组-->Stream
        Integer[] arr = {1,3,5,7,9};
        //使用Stream.of(可变参数)
        Stream.of(arr).forEach(System.out::println);
        Stream.of(2,4,6,8).forEach(System.out::println);
        //使用Arrays.stream(数组)
        Arrays.stream(arr).forEach(System.out::println);
    }
}
