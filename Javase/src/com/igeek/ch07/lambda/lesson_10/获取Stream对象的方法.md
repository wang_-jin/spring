## 获取Stream对象的方法
1.List集合
使用stream()方法
2.Set集合
使用Stream方法
3.Map
  3.1map.keySet().stream()获取键的stream对象
  3.2map.values().stream()获取值的stream对象
  3.3map.entrySet.stream()获取键值对的stream对象
4.数组
  4.1Stream.of(数组对象)获取stream对象
  4.2Arrays.stream(数组对象)获取stream对象