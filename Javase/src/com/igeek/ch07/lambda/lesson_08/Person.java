package com.igeek.ch07.lambda.lesson_08;

public class Person {
    private String name;
    int age;
    public Person(){};
    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
