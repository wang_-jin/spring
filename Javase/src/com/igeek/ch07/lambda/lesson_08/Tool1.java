package com.igeek.ch07.lambda.lesson_08;

import java.util.Arrays;
import java.util.List;

/*实例方法引用*/
public class Tool1 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Bob","Cat","Alice","Henry");
        names.forEach(s-> System.out.println(s));
        //实例方法引用
        names.forEach(System.out::println);

    }


}
