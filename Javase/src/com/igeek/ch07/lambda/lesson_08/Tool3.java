package com.igeek.ch07.lambda.lesson_08;

import java.util.function.Supplier;

public class Tool3 {
    public static void main(String[] args) {
        Supplier<Person> supplier = ()->{
            return new Person();
        };
        System.out.println(supplier.get());
        //构造函数引用,下面代码相当于上面的lambda表达式
        Supplier<Person> supplier1 = Person::new;
        System.out.println(supplier1.get());
    }
}
