package com.igeek.ch07.lambda.lesson_08;

import java.util.Arrays;

/*静态方法引用*/
public class Tool {

    public static void main(String[] args) {
        Student[] arr =new Student[]{
                new Student("刘备", 18),
                new Student("关羽", 20),
                new Student("张飞", 17)
        };
        /*Arrays.sort(arr, (a, b) -> {
            return a.getAge() - b.getAge();
        });
        //Arrays.toString方法会自动调用数组中元素的toString方法
        System.out.println(Arrays.toString(arr));*/

        //静态方法引用
        Arrays.sort(arr,Student::compare);
        System.out.println(Arrays.toString(arr));


    }
}
