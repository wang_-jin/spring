package com.igeek.ch07.lambda.lesson_08;

import java.util.Arrays;
import java.util.List;

public class Tool2 {
    public static void main(String[] args) {
        //特定方法引用
        List<String> names = Arrays.asList("Bob","Cat","Alice","Henry");
        names.sort((s1,s2)->s1.compareToIgnoreCase(s2));
        System.out.println(names);
    }
}
