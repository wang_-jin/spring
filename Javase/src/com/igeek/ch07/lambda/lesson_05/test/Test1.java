package com.igeek.ch07.lambda.lesson_05.test;

import java.util.function.Consumer;

/*
格式化打印信息String[] array = { "张三,女", "李四,女", "王二麻,男" };
如上字符串数组当中存有多条信息,请按照格式姓名:XX,性别:XX 的格式将信息打印出来,
要求将打印姓名的动作作为第一个Consumer接口的Lambda实例,
将打印性别的动作作为第二个Consumer接口的Lambda实例
*/
public class Test1 {
    public static void info(Consumer<String>one,Consumer<String>two,String[] arr){
        for (String s : arr) {
            one.andThen(two).accept(s);
        }
    }
    public static void main(String[] args) {
        String[] array = { "张三,女", "李四,女", "王二麻,男" };
        info(str->{
            System.out.print("姓名:"+str.split(",")[0]+",");
        },str->{
            System.out.println("性别:"+str.split(",")[1]);
        },array);
    }
}
