package com.igeek.ch07.lambda.lesson_05;

import java.util.function.Consumer;

public class Tool1 {
    /*
    使用andThen实现先将数字加1,再将数字乘以2
    注意:
      第一次操作和第二次操作是独立的,也就是说第二次操作
      不会在第一次数据的基础上修改,第一次和第二次操作
      都是单独的操作数据,只不过有个先后顺序罢了
    */
    public static void calc(Consumer<Integer> one,Consumer<Integer> two,int i){
        one.andThen(two).accept(i);
        //上面代码相当于下面代码
        /*one.andThen(i)
          weo.andThen(i)    */
    }

    public static void main(String[] args) {
        calc((Integer i)->{
            System.out.println(i+1);
        },i->{
            System.out.println(i*2);
        },10);
    }
}
