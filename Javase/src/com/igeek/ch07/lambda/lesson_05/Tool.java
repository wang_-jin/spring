package com.igeek.ch07.lambda.lesson_05;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Tool{
    /*
Consumer<参数类型>消费型函数接口:
  它的作用是接受一个参数对这个参数进行一些逻辑处理,
  并没有任何返回值!
抽象方法:
  void accept(T t);
*/
    /*使用accept模拟实现forEach方法*/
    public static void foreach(Consumer consumer, List<Integer> list){
        for (Integer i : list) {
            consumer.accept(i);
        }
    }

    public static void main(String[] args) {
        foreach(i->{
            System.out.println(i);
        }, Arrays.asList(1,3,5,7,9));
    }

}
