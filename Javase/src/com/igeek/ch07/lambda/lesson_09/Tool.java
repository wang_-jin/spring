package com.igeek.ch07.lambda.lesson_09;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Tool {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("az","b","cz","d");
        //传统循环
        list.forEach(System.out::println);

        //传统过滤
        List<String> list1 = new ArrayList<>();
        list.forEach(s->{
            if(s.contains("z")){
                list1.add(s);
            }
        });
        System.out.println(list1);

        System.out.println("--------------------");
        //stream循环
        list.stream().forEach(System.out::println);
        //stream过滤
        System.out.println("--------------");
        list.stream().filter(s->s.contains("z")).collect(Collectors.toList()).forEach(System.out::println);
        //System.out.println(list2);

        /*
        //stream重复使用会报错:
        //stream has already been operated upon or closed
        Stream<String> stream = list.stream();
        stream.forEach(System.out::println);
        stream.filter(s->s.contains("z")).toList();
        */
    }
}
