package com.igeek.ch07.lambda.lesson_07;

import java.util.Arrays;
import java.util.function.Predicate;

public class Tool {
  public static Integer[] filter(Predicate<Integer> predicate,
                                 Integer[] arr) {
    Integer[] result = new Integer[arr.length];
    int j = 0;
    for (int i = 0; i < arr.length; i++) {
      if (predicate.test(arr[i])) {
        result[j] = arr[i];
        j++;
      }
    }
    return result;
  }

  public static void main(String[] args) {
    /*将数组中大于5的保留生成一个新数组*/
    Integer[] arr = filter(i -> {
      return i > 5;
    }, new Integer[]{1, 3, 5, 7, 9, 11});
    System.out.println(Arrays.asList(arr));
  }

  ;

}
