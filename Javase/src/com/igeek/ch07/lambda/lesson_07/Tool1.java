package com.igeek.ch07.lambda.lesson_07;

import java.util.function.Predicate;

public class Tool1 {

    public static Boolean and(Predicate<String> one,
                              Predicate<String> two,
                              String str){
      return one.and(two).test(str);
    }

    public static Boolean or(Predicate<String> one,
                              Predicate<String> two,
                              String str){
        return one.or(two).test(str);
    }

    public static Boolean negate(Predicate<String> predicate,
                             String str){
        return predicate.negate().test(str);
    }

    public static void main(String[] args) {
        //判断提供的字符串是否既包含h又包含e
        Boolean flag = and(str-> str.contains("h")
        ,str-> str.contains("e")
        ,"hello");
        System.out.println(flag);

        //判断提供的字符串是否包含h或者包含u
        Boolean flag1 = or(str-> str.contains("h")
                ,str-> str.contains("u")
                ,"hello");
        System.out.println(flag1);

        //判断提供的字符串长度是否大于3
        Boolean flag2 = negate(str-> str.length()>3
                ,"hello");
        System.out.println(flag2);
    }
}
