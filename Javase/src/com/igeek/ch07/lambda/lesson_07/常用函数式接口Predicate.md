## Prediate判断型函数接口
在Java中,Predicate接口是一个函数式接口,它定义了一个名为test()的方法,
该方法接受一个参数并返回一个boolean值.Predicate接口通常用于过滤和筛选
数据集中的元素,可以作为一种通用的函数式编程方式
抽象方法:Boolean test(T t)
默认方法and: 与
默认方法or: 或
默认方法negate: 非