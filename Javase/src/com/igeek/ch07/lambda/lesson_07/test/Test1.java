package com.igeek.ch07.lambda.lesson_07.test;

import java.util.ArrayList;
import java.util.function.Predicate;

/*
集合信息筛选String[] array = {"张三:男","李四:女","王二麻子:女","李小小红:女"};
数组当中有多条“姓名+性别”的信息如下,请通过Predicate接口的拼装将符合
要求的字符串筛选到集合ArrayList中,需要同时满足两个条件:
1.必须为女生
2.姓名为4个字
*/
public class Test1 {
    public static ArrayList<String> filter(Predicate<String> one,
                                           Predicate<String> two,
                                           String[] arr){
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if(one.and(two).test(arr[i])){
                list.add(arr[i]);
            }
        }
        return list;
    }

    public static void main(String[] args) {
        String[] array = {"张三:男","李四:女","王二麻子:女","李小小红:女"};
        ArrayList list =  filter(str->{
            return str.contains("女");
        },str->{
            return str.split(":")[0].length()==4;
        },array);
        System.out.println(list);
    }
}
