package com.igeek.ch07.lambda.lesson_03;
@FunctionalInterface
public interface Math {
    int calculate(int a,int b);
}
