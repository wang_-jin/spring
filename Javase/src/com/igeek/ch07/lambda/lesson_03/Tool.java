package com.igeek.ch07.lambda.lesson_03;

public class Tool {
    /*
    我们需要求和,求差,求积三个功能,要定义三个方法去实现,
    这3个方法参数完全一样,只有方法体不同,能不能将三个方法
    改写成一个方法,只让方法体代码灵活变动?我们可以使用
    回调函数来实现.
    */
    //求和方法
    public static int sum(int a,int b){
        return a+b;
    }
    //求差方法
    public static int sub(int a,int b){
        return a-b;
    }
    //求积方法
    public static int mul(int a,int b){
        return a*b;
    }
    public static void main(String[] args) {
        System.out.println(sum(1, 2));
        System.out.println(sub(1, 2));
        System.out.println(mul(1, 2));

    }
}
