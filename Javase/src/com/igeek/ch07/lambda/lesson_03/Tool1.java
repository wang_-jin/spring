package com.igeek.ch07.lambda.lesson_03;

public class Tool1 {
    /*
    定义一个计算结果方法,使用函数接口作为参数,
    这样做的好处是可以更加灵活的处理方法逻辑!
    注意:
      当一个方法的形参是一个接口时,实参需要传递一个实现了该接口的对象!
    */
    public static void result(int a,int b,Math math){
        System.out.println(math.calculate(a,b));
    }
    public static void main(String[] args) {
        //求和
        result(1,2,(int a,int b)->{
          return a+b;
        });
        //求差
        result(1,2,(int a,int b)->{
            return a-b;
        });
        //求积
        result(1,2,(int a,int b)->{
            return a*b;
        });
    }
}
