## 常用Stream方法
非终结方法:
  返回值是Stream对象,可以在方法后面使用点继续链式调用其他Stream方法
终结方法:
  返回值不是Stream对象,方法后面
  不能使用点继续链式调用其他Stream方法
1.filter过滤,非终结方法
2.limit(整型)保留前几个元素,非终结方法
3.skip(整型)跳过前几个元素,非终结方法
4.map将集合中的元素按某种规则修改后,形成一个新的集合,非终结方法
5.Stream.concat(Stream对象1,Stream对象2)将两个Stream对象合并,非终结方法
注意:
  concat是一个静态方法

6.count计算集合中元素个数,终结方法
7.forEach循环,终结方法
8.sorted排序,非终结方法
9.findAny发现任意一个符合条件的元素,返回Optional对象,
可以使用orElse方法获取这个元素,终结方法
10.anymatch判断流中是否存在任意一个元素满足指定的条件,该方法返回一个boolean类型的值,
终结方法
11.distinct去除流中重复元素
12.max和min求最大最小值