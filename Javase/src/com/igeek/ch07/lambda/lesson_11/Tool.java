package com.igeek.ch07.lambda.lesson_11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Tool {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(new Integer[]{1,2,3,4,5,6,7});
        //1.filter过滤
        list.stream().filter(i->i<3).forEach(System.out::println);
        System.out.println("-------filter---------");
        //2.limit保留前几个
        list.stream().limit(3).forEach(System.out::println);
        System.out.println("-------limit---------");
        //3.skip跳过前几个
        list.stream().skip(2).forEach(System.out::println);
        System.out.println("-------skip---------");
        //4.map修改集合中元素
        list.stream().map(s->s*s).forEach(System.out::println);
        System.out.println("-------map---------");
        Stream<String> a = Stream.of("a", "b", "c");
        Stream<Integer> b = Stream.of(1, 2, 3);
        Stream.concat(a,b).forEach(System.out::println);
        System.out.println("-------concat---------");
        //5.count计算集合中元素个数
        Long c = list.stream().count();
        System.out.println(c);
        System.out.println("------count---------");
        //6.sorted排序
        Stream.of(1, 23, 3, 4, 55, 6, 8).sorted().forEach(System.out::println);
        System.out.println("------sorted---------");
        //7.findAny发现任何一个符合条件的元素
        Integer v = list.stream().filter(s->s>3).findAny().orElse(-1);
        System.out.println(v);
        System.out.println("------findAny---------");
        //8.anyMatch判断流中是否存在任意一个元素满足指定的条件
        Boolean g = list.stream().anyMatch(s->s>6);
        System.out.println(g);
        System.out.println("------anyMatch---------");
        //9.distinct去重
        Stream.of(1,1,2,2,3,3).distinct().forEach(System.out::println);
        System.out.println("------distinct---------");
        //10.max求最大值
        System.out.println(list.stream().max((a1,a2)->a1-a2).orElse(0));
        System.out.println(list.stream().max((a1,a2)->Integer.compare(a1,a2)).orElse(0));
        System.out.println(list.stream().max(Integer::compare).orElse(0));
        /*
        Comparator.comparingInt用来创建一个比较器,comparingInt参数是
        ToIntFunction<T> 是一个函数接口,它定义了一个抽象方法 applyAsInt(),
        接受一个 T 类型的参数,并返回一个整数值,comparingInt() 中,
        这个函数接口的作用是将对象转换为整数值,从而进行比较
        */
        System.out.println(list.stream().max(Comparator.comparingInt(Integer::intValue)).orElse(0));

    }
}
