package com.igeek.ch07.lambda_test.test5;

public interface Math {
    int calculate(int a, int b);
}
