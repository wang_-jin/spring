package com.igeek.ch07.lambda_test.test5;

/**
 * @author wangjin
 * 2023/8/29 20:31
 * @description TODO
 */
public class Test {
    /*当定义一个计算结果方法时,用函数接口来当参数
    可以更加灵活的处理方法逻辑*/
    /*当方法的形参是接口时,实参需要传入实现接口的对象*/
    public static void result(int a, int b, Math math){
        System.out.println(math.calculate(a,b));
    }

    public static void main(String[] args) {
        result(1,3,(a,b)->{
            return a+b;
        });
        result(4,2,(a,b)->{
            return a*b;
        });
        result(4,2,(a,b)->{
            return a/b;
        });
    }
}
