package com.igeek.ch07.lambda_test.test2;

import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/8/29 16:31
 * @description TODO
 */
public class Test {
    /*格式化打印信息String[] array = { "张三,女", "李四,女", "王二麻,男" };
如上字符串数组当中存有多条信息,请按照格式姓名:XX,性别:XX 的格式将信息打印出来,
要求将打印姓名的动作作为第一个Consumer接口的Lambda实例,
将打印性别的动作作为第二个Consumer接口的Lambda实例*/
    public static void foo(Consumer<String>one,Consumer<String>two,String[] arr){
        for (String str : arr) {
            one.accept(str);
            two.accept(str);
        }
    }
    public static void main(String[] args) {
        String[] array = {"张三,女", "李四,女", "王二麻,男"};
            foo(s1 -> {
                System.out.print("姓名:" + s1.split(",")[0]+",");
            }, s1 -> {
                System.out.println("性别:" + s1.split(",")[1]);
            }, array);
    }
}
