package com.igeek.ch07.lambda_test.test4;

/**
 * @author wangjin
 * 2023/8/29 20:24
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Animal animal = (() -> {
            System.out.println("吃饭");
        });
        animal.eat();
        /*lambda表达式是匿名内部类的简写形式
        自定义函数接口,抽象方法只有一个*/
    }
}
