package com.igeek.ch07.lambda_test.test6;

import java.util.function.Supplier;

/**
 * @author wangjin
 * 2023/8/29 20:48
 * @description TODO
 */
public class Test {
    public static void foo(Supplier<String>supplier){
        System.out.println(supplier.get());
    }
    public static void main(String[] args) {

        foo(()->{
            return "hello".toUpperCase();
        });
    }
}
