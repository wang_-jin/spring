package com.igeek.ch07.lambda_test.funtion;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author wangjin
 * 2023/8/30 19:58
 * @description TODO
 */
public class Test {
    /*function
     抽象方法:R apply(T t)  */
    public static List foo(Function<String,Integer>one,Function<Integer,String>two,String[] arr){
        List<String>list = new ArrayList<>();
        for(String s : arr){
            list.add(one.andThen(two).apply(s));
        }
        return list;
    }
    public static void main(String[] args) {
        String[] arr = {"1","23","12","14"};

/*        String s = "123";
        Function<String,Integer>function = a->{
            Integer i = Integer.parseInt(a)+100;
            return i;
        };
        System.out.println(function.apply(s));*/
        List list = foo(a->{
            return Integer.parseInt(a)+10;
        },b->{
            return b+"";
        },arr);
        System.out.println(list);
    }
}
