package com.igeek.ch07.lambda_test.funtion;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author wangjin
 * 2023/8/31 9:07
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
/*        Function<String,Integer>function=((a)->{
            Integer i = a.length();
            return i;
        });
        System.out.println(function.apply("hello"));*/
        Predicate<String>predicate = a->{
            return a.length()>3;
        };
        boolean flag = predicate.test("hello");
        System.out.println(flag);
    }
}
