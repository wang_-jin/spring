package com.igeek.ch07.lambda_test.test10;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author wangjin
 * 2023/8/30 11:27
 * @description TODO
 */
public class Test {
    /*集合信息筛选String[] array = { "张三:男","李四:女","王二麻子:女","李小小红:女"};
数组当中有多条“姓名+性别”的信息如下,请通过Predicate接口的拼装将符合
要求的字符串筛选到集合ArrayList中,需要同时满足两个条件:and
1.必须为女生
2.姓名为4个字*/
    public static List<String> and(Predicate<String>one, Predicate<String>two, String[] array){
        List<String>list = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            String s = array[i];
           if (one.and(two).test(s)){
               list.add(s);
           }
        }
        list.sort((o1, o2) -> {
            return o1.compareToIgnoreCase(o2);
        });
        return list;
    }
    public static void main(String[] args) {
        String[] array = { "张三:男","李四:女","王二麻子:女","李小小红:女"};
        List list = and(a->{
            String s = a.split(":")[0];
            return s.length()==4;
        },b->{
            String s = b.split(":")[1];
            return s.equals("女");
        },array);
        System.out.println(list);
    }
}
