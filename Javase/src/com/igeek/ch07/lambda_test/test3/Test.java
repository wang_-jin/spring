package com.igeek.ch07.lambda_test.test3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author wangjin
 * 2023/8/29 20:21
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        List<Integer>list = Arrays.asList(1,3,9,7);
        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });
/*        list.forEach(i -> {
            System.out.println(i);
        });*/
    }
}
