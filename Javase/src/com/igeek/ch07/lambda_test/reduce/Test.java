package com.igeek.ch07.lambda_test.reduce;

import java.util.Arrays;
import java.util.List;

/**
 * @author wangjin
 * 2023/8/31 19:59
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        List<Integer>list = Arrays.asList(1,5,3,2,6);
        Integer sum = list.stream().reduce(0, (result, item) -> {
            return result + item;
        });
        System.out.println(sum);
        /*方法引用使用条件:
  1.lambda表达式函数体中只有一句代码,这句代码是一个函数的执行
  2.引用方法的参数和返回值必须和lambda表达式相同,否则会报错*/
        System.out.println("-------方法引用-----");
        Integer sum1 = list.stream().reduce(0, (a, b) -> {
            return Integer.sum(a, b);
        });
        System.out.println("sum1:"+sum1);
        System.out.println("-------方法引用-----");
        Integer sum2 = list.stream().reduce(0,Integer::sum);
        System.out.println("sum2:"+sum2);
        System.out.println("-----------------");
        Integer max = list.stream().reduce(0, ((integer, integer2) -> {
            return integer < integer2 ? integer2 : integer;
        }));
        System.out.println("max:"+max);
        System.out.println("-----------------");
        Integer max2 = list.stream().reduce(0,(a,b)->{
            return Integer.max(a,b);
        });
        System.out.println("max2:"+max2);
        System.out.println("-------方法引用-----");
        Integer max1 = list.stream().reduce(0,Integer::max);
        System.out.println("max1:"+max1);
    }
}
