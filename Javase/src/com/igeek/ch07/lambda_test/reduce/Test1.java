package com.igeek.ch07.lambda_test.reduce;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/8/31 20:18
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        List<Person>list = new ArrayList<>();
        list.add(new Person("张三",22));
        list.add(new Person("李四",28));
        list.add(new Person("王五",20));
        list.add(new Person("赵云",26));
        //
        Integer sum = list.stream().map(Person::getAge).reduce(0, Integer::sum);
        System.out.println("平均年龄:"+sum/ list.size());
    }
}
