package com.igeek.ch07.lambda_test.test9;

import java.util.function.Function;

/**
 * @author wangjin
 * 2023/8/30 10:27
 * @description TODO
 */
public class Test {
    /*请使用Function进行函数模型的拼接,按照顺序需要执行的多个函数操作为:
String str = "张三,20";
将字符串截取数字部分,得到字符串;
将上一步的字符串转换成为int类型的数字;
将上一步的int数字加上100,得到结果int数字*/
    public static Integer foo(Function<String,Integer>one,String str){return one.apply(str)+100;}
    public static void main(String[] args) {
        String str = "张三,20";
        Integer num = foo(s -> {
            Integer i = Integer.parseInt(str.split(",")[1]);
            return i;
        },str);
        System.out.println(num);
    }
}
