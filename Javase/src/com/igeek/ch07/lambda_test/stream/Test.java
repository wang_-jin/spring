package com.igeek.ch07.lambda_test.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangjin
 * 2023/8/30 15:43
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        List<String>list = Arrays.asList("ac","b","cd","d");
        list.stream().forEach(System.out::println);
        //List<String> list1 = list.stream().filter(s -> s.contains("c")).collect(Collectors.toList());
        //System.out.println(list1);
    }

}
