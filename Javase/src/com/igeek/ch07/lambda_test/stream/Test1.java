package com.igeek.ch07.lambda_test.stream;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/8/31 9:23
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        Integer[] arr = {1,2,3,3,4,5,5,6,7,8};
        //Arrays.stream(arr).forEach(System.out::println);
        //Arrays.stream(arr).map(a->a+3).forEach(System.out::println);
        //Arrays.stream(arr).limit(2).forEach(System.out::println);
        //System.out.println(Arrays.stream(arr).skip(4).max(Integer::compare).orElse(0));
        Stream<Integer> limit1 = Arrays.stream(arr).limit(3);
        Stream<Integer> limit2 = Arrays.stream(arr).skip(5);
        //Stream.concat(limit1,limit2).sorted((o1,o2)->o2-o1).forEach(System.out::println);
        //System.out.println(Arrays.stream(arr).anyMatch(a -> a > 5));
        //System.out.println(Arrays.stream(arr).filter(a -> a > 6).findAny().orElse(0));
        //System.out.println(Arrays.stream(arr).count());
        //Arrays.stream(arr).distinct().forEach(System.out::println);
        Arrays.stream(arr).filter(a->a>3&&a<7).forEach(System.out::println);
    }
}
