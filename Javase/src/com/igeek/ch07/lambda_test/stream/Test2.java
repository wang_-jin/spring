package com.igeek.ch07.lambda_test.stream;

import java.util.Arrays;
import java.util.List;

/**
 * @author wangjin
 * 2023/8/31 9:54
 * @description TODO
 */
public class Test2 {
    /*1.筛选出Integer集合中大于7的元素,并打印,例如6,7,3,8,1,2,9
2.获取String集合中最长的元素,并打印,例如"a","att","da","tyua"
3.获取Integer集合中最大值,并打印,例如6,7,3,8,1,2,9
4.计算Integer集合中大于6的元素的个数,例如12,3,66,7,89,4*/
    public static void main(String[] args) {
        //筛选出Integer集合中大于7的元素,并打印,例如6,7,3,8,1,2,9
        Integer[] arr = {6,7,3,8,1,2,9};
        //Arrays.stream(arr).filter(a->a>7).forEach(System.out::println);
        //获取String集合中最长的元素,并打印,例如"a","att","da","tyua"
        List<String>list = Arrays.asList("a","att","da","tyua");
        //list.stream().sorted((o1, o2) -> o1.length()-o2.length()).skip(list.size()-1).forEach(System.out::println);
        //获取Integer集合中最大值,并打印,例如6,7,3,8,1,2,9
        //System.out.println(Arrays.stream(arr).max(Integer::compare).orElse(0));
        //计算Integer集合中大于6的元素的个数,例如12,3,66,7,89,4
        Integer[] arr1 = {12,3,66,7,89,4};
        System.out.println(Arrays.stream(arr1).filter(a -> a > 6).count());


    }
}
