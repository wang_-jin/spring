package com.igeek.ch07.lambda_test.stream;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangjin
 * 2023/8/31 11:15
 * @description TODO
 */
public class Test3 {
    /*5.定义一个员工Employee类,有姓名name,工资salary两个属性,
创建一些员工实例对象存入到集合中,完成下面题目
5.1.筛选员工中工资高于8000的人,并形成新的集合
5.2获取员工工资最高的人
5.3.将员工按照工资由低到高排序
5.4.将员工按照工资由高到低排序
5.5求员工的平均薪资*/
    public static void main(String[] args) {
        List<Employee>list = new ArrayList<>();
        list.add(new Employee("刘备",9000));
        list.add(new Employee("关羽",5000));
        list.add(new Employee("张飞",4500));
        list.add(new Employee("刘禅",5500));
        list.add(new Employee("赵云",4700));
        List list1 = list.stream().filter(s-> s.getSalary()>8000).collect(Collectors.toList());
        System.out.println(list1);
        String s = list.stream().reduce(new Employee(),(result,item)->{
            if (result.getSalary() < item.getSalary()){
                result = item;
            }
            return result;
        }).getName();
        System.out.println(s);
        List list2 = list.stream().sorted((o1, o2) -> {
            return o1.getSalary()-o2.getSalary();
        }).collect(Collectors.toList());
        System.out.println(list2);
        List list3 = list.stream().sorted((o1, o2) -> {
            return o2.getSalary()-o1.getSalary();
        }).collect(Collectors.toList());
        System.out.println(list3);
        Integer sum = list.stream().map(Employee::getSalary).reduce(0,Integer::sum);
        System.out.println("平均薪资"+sum/list.size());
    }
}
