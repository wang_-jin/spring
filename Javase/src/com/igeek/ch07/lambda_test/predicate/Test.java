package com.igeek.ch07.lambda_test.predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author wangjin
 * 2023/8/30 20:33
 * @description TODO
 */
/*将数组中数大于5的数保留组成新数组*/
public class Test {
    public static Integer[] foo(Predicate<Integer>predicate,Integer[] arr){
        Integer[] arr1 = new Integer[arr.length];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (predicate.test(arr[i])){
                arr1[j]=arr[i];
                j++;
            }
        }
        return arr1;
    }
    public static void main(String[] args) {
        Integer[] arr = {1,6,4,7,9};
        Integer[] arr1 = foo(a->{
         return a>5;
        },arr);
        System.out.println(Arrays.toString(arr1));
    }
}
