package com.igeek.ch07.lambda_test.predicate;

import java.util.function.Predicate;

/**
 * @author wangjin
 * 2023/8/30 20:42
 * @description TODO
 */
public class Test1 {
    public static Boolean and(Predicate<String>one,Predicate<String>two,String str){
        return one.and(two).test(str);
    }
    public static Boolean or(Predicate<String>one,Predicate<String>two,String str){
        return one.or(two).test(str);
    }
    public static Boolean negate(Predicate<String>one,String str){
        return one.negate().test(str);
    }

    public static void main(String[] args) {
        String s = "sjfqksidq";
/*        Boolean flag1 = and(a->{
            return a.contains("k");
        },b->{
            return b.length()==9;
        },s);
        System.out.println(flag1);
    }*/
/*    Boolean flag1 = or(a->{
        return a.contains("k");
    },b->{
        return b.length()==4;
    },s);
        System.out.println(flag1);*/
        Boolean flag1 = negate(a->{
            return a.contains("k");
        },s);
        System.out.println(flag1);
    }

}
