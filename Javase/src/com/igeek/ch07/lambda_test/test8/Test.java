package com.igeek.ch07.lambda_test.test8;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author wangjin
 * 2023/8/30 9:25
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
/*        Consumer<Integer>consumer = new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println("hello");
            }
        };
        consumer.accept(1);*/
        Consumer<String>consumer1 = t->{
            System.out.println("t字符串长度:"+t.length());
        };
        consumer1.accept("hello");


        Supplier<String>supplier = ()->{
            return "hello";
        };
        String s = supplier.get();
        System.out.println("s:"+s);
    }
}
