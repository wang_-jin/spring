package com.igeek.ch07.lambda_test.test7;

import java.util.function.Consumer;

/**
 * @author wangjin
 * 2023/8/29 21:07
 * @description TODO
 */
public class Test {
    /*
使用andThen实现先将数字加1,再将数字乘以2
注意:
  第一次操作和第二次操作是独立的,也就是说第二次操作
  不会在第一次数据的基础上修改,第一次和第二次操作
  都是单独的操作数据,只不过有个先后顺序罢了
*/
    public static void calc(Consumer<Integer>one,Consumer<Integer>two,Integer i){
        one.accept(i);
        two.accept(i);
    }

    public static void main(String[] args) {
        calc(a->{
            System.out.println(a+1);
        },b->{
            System.out.println(b*2);
        },10);
    }
}
