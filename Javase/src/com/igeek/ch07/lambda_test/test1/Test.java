package com.igeek.ch07.lambda_test.test1;

import java.util.function.Supplier;

/**
 * @author wangjin
 * 2023/8/29 15:30
 * @description TODO
 */
public class Test {
    /*求数组元素最大值,使用Supplier接口作为方法参数类型,
通过Lambda表达式求出int数组中的最大值*/
    public static int getMax(Supplier<Integer> supplier){
        return supplier.get();
    }

    public static void main(String[] args) {


    }

}
