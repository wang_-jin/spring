package com.igeek.ATMwork1.dao;

import com.igeek.ATMwork1.entity.Account;

import java.io.File;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/11 21:19
 * @description TODO
 */
public class FileDao implements IDao<Account, List<Account>> {

    private File idFile ;//保存 id值的文件对象  id.txt
    private File accFile;//保存 账户信息的目录对象  account
    // account/862150212023090001.txt 每个账户都有一个文件，以账号作为文件名

    public FileDao() {
        //创建idFile 文件对象
        //创建accFile 文件对象
    }

    @Override
    public boolean add(Account acc) {
        //将acc对象写道account/862150212023090001.txt文件  ==> 序列化
        return false;
    }

    @Override
    public Account selectOne(Long id, String password) {
        //反序列化
        return null;
    }

    @Override
    public Account selectOne(Long id) {
        return null;
    }

    @Override
    public List<Account> selectAll() {
        return null;
    }

    @Override
    public void setId(Long id) {
        //将ID值写到文件中 PrintWriter
    }

    @Override
    public Long getId() {
        //读取文件中的ID值  BufferedReader
        return null;
    }

    @Override
    public boolean update(Account acc) {
        //存款 取款 转账等业务 更新数据  ==> 序列化
        return false;
    }
}
