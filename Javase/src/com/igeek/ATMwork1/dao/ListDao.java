package com.igeek.ATMwork1.dao;


import com.igeek.ATMwork1.entity.Account;

import java.util.List;

/**
 * @author wangjin
 * 2023/9/11 21:19
 * @description 数据访问--文件实现
 */
public class ListDao implements IDao<Account, List<Account>> {
    @Override
    public boolean add(Account acc) {
        return false;
    }

    @Override
    public Account selectOne(Long id, String password) {
        return null;
    }

    @Override
    public Account selectOne(Long id) {
        return null;
    }

    @Override
    public List<Account> selectAll() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public boolean update(Account acc) {
        return false;
    }
}

