package com.igeek.ATMwork1.dao;

import com.igeek.ATMwork1.entity.Account;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author wangjin
 * 2023/9/11 21:18
 * @description 数据访问--数组实现
 * 1.当前所有的账户对象的信息,存放在数组中: Account[].
 * 2.当前账户数量index.
 */
public class ArrayDao implements IDao<Account,Account[]>{
    private Account[] accounts = new Account[10];
    private int index = 0;
    //保存最后一个开户用户的ID --> 内存  --> 文件中
    private Long id = 100000000000000000L;


    //保存在内存中 ---》写进文件中
    @Override
    public void setId(Long id) {
        this.id  = id;
    }
    //获取内存中的数据 ==》 读取文件
    @Override
    public Long getId() {
        return id;
    }
    @Override
    public boolean add(Account acc) {
        if(index == accounts.length){
            //容量不够用  则开始扩容  ：每次扩容为上一次的1.5倍
            accounts = Arrays.copyOf(accounts,accounts.length*3/2);
        }
        accounts[index++] = acc;
        return true;
    }
    /**
     * 根据账号和密码 查询账户信息
     * @param id
     * @param password
     * @return    返回值为null表示未查询到相关账户
     */
    @Override
    public Account selectOne(Long id, String password) {
        for (int i = 0; i < index; i++) {
            Account acc = accounts[i];
            if(Objects.equals(id,acc.getId()) && Objects.equals(password,acc.getPassword())){
                return acc;
            }
        }
        return null;
    }
    /**
     * 根据账号 查询账户信息
     * @param id
     * @return  返回值为null表示未查询到相关账户
     */

    @Override
    public Account selectOne(Long id) {
        for (int i = 0; i < index; i++) {
            Account acc = accounts[i];
            if(Objects.equals(id,acc.getId())){
                return acc;
            }
        }
        return null;
    }

    @Override
    public Account[] selectAll() {
        return Arrays.copyOf(accounts,index);
    }




    @Override
    public boolean update(Account acc) {
        return false;
    }


}
