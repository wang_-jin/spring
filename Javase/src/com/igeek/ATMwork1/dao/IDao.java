package com.igeek.ATMwork1.dao;


import com.igeek.ATMwork1.entity.Account;

import java.time.LocalDate;

public interface IDao<T,K> {
    public boolean add(T acc);
    public T selectOne(Long id, String password);
    public T selectOne(Long id);
    public K selectAll();
    public void setId(Long id);
    public Long getId();
    //更新账户信息：内存中的数据 和 文件中数据保持一致
    public boolean update(Account acc);

    //自动生成ID号
    public default Long createId(){
        //截取最后一个开户用户的ID    862150212013020001
        String idStr = getId().toString();
        //年份
        int year = Integer.parseInt(idStr.substring(8,12));
        //月份
        int month = Integer.parseInt(idStr.substring(12,14));
        //序号
        String countStr = idStr.substring(14);

        //本月是否已满
        if("9999".equals(countStr)){
            return  -1L;
        }

        //比对  当前开户账户 是否为 本月的第一人
        int yearNow = LocalDate.now().getYear();
        int monthNow = LocalDate.now().getMonthValue();

        Long id = null;
        if(year!=yearNow || month!=monthNow){
            //本月的第一人
            idStr = "86215021"+yearNow+(monthNow<10?"0"+monthNow:monthNow)+"0001";
            id = Long.valueOf(idStr);
        }else{
            //序号 递增
            id = Long.valueOf(idStr)+1;
        }
        //保存ID值
        setId(id);
        return id;
    }
}
