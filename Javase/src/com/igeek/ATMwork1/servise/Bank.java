package com.igeek.ATMwork1.servise;

import com.igeek.ATMwork1.dao.ArrayDao;
import com.igeek.ATMwork1.dao.IDao;
import com.igeek.ATMwork1.entity.*;
import com.igeek.ATMwork1.entity.vo.RankVO;
import com.igeek.ATMwork1.exception.ATMException;
import com.igeek.ATMwork1.exception.RegisterException;



import java.util.Objects;
import java.util.TreeSet;


/**
 * @author wangjin
 * 2023/9/11 21:14
 * @description 核心业务
 * 方法:
 * 1.用户开户(register)
 *      参数列表: Long 账号, String密码, String确认密码,String 姓名,String身份证号码,String邮箱,int 账户类型；
 * (Long id, String password, String repassword, String name, String personID, String email, int type)
 *      返回类型：Account
 *      项目需求规定账户类型：0 – 储蓄账户  1 – 信用账户 2 – 可贷款储蓄账户 3– 可贷款信用账户
 * 2.用户登录(login)
 *      参数列表: Long 账号, String密码；
 * (Long id, String password)
 *      返回类型：Account
 * 3.用户存款(deposit)
 *      参数列表: Long 账号, double存款金额；
 * (Long id, double money)
 *      返回类型：Account
 * 4.用户取款(withdraw)
 *      参数列表: Long 账号,String 密码，double取款金额；
 * (Long id, String password, double money)
 *      返回类型：Account
 * 5.设置透支额度(updateCeiling)
 *      参数列表: Long 账号, String 密码，double透支额度金额；
 * (Long id, String password, double money)
 *      返回类型：Account
 * 提示：这个方法需要验证账户是否是信用账户
 * 6.转账功能(transfer)
 *     参数：from转出账户，passwordFrom 转出账号的密码，to转入账户，money转账的金额
 *           (Long from, String passwordFrom, Long to, double money)
 *      返回值：boolean
 *
 * 要求3：另外,请为Bank类添加几个统计方法
 * 1.统计银行所有账户余额总数
 * 2.统计所有信用账户透支额度总数
 *
 * 要求3：为Bank类添加三个新方法
 * a)	贷  款(requestLoan)
 * 参数：id 账户，money贷款金额
 *       (Long id , double money)
 * 返回类型：Account
 * b)	还贷款(payLoan)
 * 参数：id 账户，money还贷款金额，String password 账户密码
 *       (Long id，String password , double money)
 * 返回类型：Account
 * c)	统计所有账户贷款的总额(totoal)
 * 参数：无
 * 返回类型：double
 *
 * 要求2：
 * 为Bank类添加一个方法，能够打印所有用户的总资产排名（提高部分
 *
 *
 * 懒汉式 单例模式：
 * 1.构造方法私有
 * 2.提供静态变量  保存 该类的唯一实例
 * 3.对外提供静态方法 获取 唯一实例
 *
 */
public class Bank {
    private Bank() {}
    private static Bank bank;
    public static Bank getInstance(){
        if (bank == null){
            bank = new Bank();
        }
        return bank;
    }
    private IDao<Account,Account[]> dao = new ArrayDao();

    /**
     * 开户
     * @param password  密码
     * @param repassword  确认密码
     * @param name  姓名
     * @param personID  身份证号码
     * @param email  邮箱
     * @param type  账户类型  0 – 储蓄账户  1 – 信用账户 2 – 可贷款储蓄账户 3– 可贷款信用账户
     * @return
     */

    public Account register(String password, String repassword, String name, String personID, String email, int type) throws RegisterException {
        Account acc = null;
        if(password!=null && !"".equals(password) && Objects.equals(password,repassword)){
            //获取自动生成的ID
            Long id = dao.createId();
            if(id== -1L){
                //System.out.println("本月开户人数已达上限，请下月再来！");
                throw new RegisterException("本月开户人数已达上限，请下月再来！");
            }
            switch (type){
                case 0:
                    acc = new SavingAccount(id,password,name,personID,email,0.0);
                    break;
                case 1:
                    acc = new CreditAccount(id,password,name,personID,email,0.0,2000.0);
                    break;
                case 2:
                    acc = new LoanSavingAccount(id,password,name,personID,email,0.0,1000L);
                    break;
                case 3:
                    acc = new LoanCreditAccount(id,password,name,personID,email,0.0,2000.0,1000L);
                    break;
                default:
                    throw new RegisterException("不存在此账户类型！");
            }
            //添加账户到容器中
            boolean flag = dao.add(acc);
            if(flag){
                System.out.println("开户成功,id="+id);
            }else{
                throw new RegisterException("开户失败");
            }
        }else{
            // System.out.println("两次输入的密码不一致！");
            throw new RegisterException("两次输入的密码不一致！");
        }
        return acc;
    }
    /**
     * 登录
     * @param id 账号
     * @param password  密码
     * @return  登录成功后的账户
     */

    public Account login(Long id, String password){
        if (id!=null && password!=null && !"".equals(password)){
            Account account = dao.selectOne(id,password);
            if (account!=null){
                System.out.println("登陆成功");
                return account;
            }else {
                System.out.println("输入的账号或密码不正确！");
            }
        }else{
            System.out.println("输入的账号或密码不能为空！");
        }
        return null;
    }
    /**
     * 存款
     * @param id 账号
     * @param money  存款金额
     * @return  存款成功后的账户
     */



    public Account deposit(Long id, double money){
        if(id!=null && money!=0){
            Account account = dao.selectOne(id);
            if (account!=null){
                //存款
                account.deposit(money);
                return account;
            }
        }else{
            System.out.println("输入的账号或金额不能为空！");
        }
        return null;
    }

    /**
     * 取款
     * @param id  账号
     * @param password  密码
     * @param money  取款金额
     * @return
     */
    public Account withdraw(Long id, String password, double money) throws ATMException {
        if(id!=null && password!=null && !"".equals(password) && money!=0){
            Account account = dao.selectOne(id, password);
            if(account!=null){
                account.withdraw(money);
                return account;
            }
        }
        return null;
    }
//     * 5.设置透支额度(updateCeiling)
// *      参数列表: Long 账号, String 密码，double透支额度金额；
    public Account updateCeiling(Long id, String password, double money){
        if(id!=null && password!=null && !"".equals(password) && money!=0){
            Account account = dao.selectOne(id, password);
            if (account != null) {
                if (account instanceof CreditAccount) {
                    ((CreditAccount) account).updateCeiling(money);
                    return account;
                } else {
                    System.out.println("非透支用户!");
                }
            }else {
                System.out.println("账号密码不正确!");
            }
        }
        return null;
    }
    /**
     * 转账
     * @param from  转出账户
     * @param passwordFrom  转出账号的密码
     * @param to   转入账户
     * @param money  转账金额
     * @return
     */
    public boolean transfer(Long from, String passwordFrom, Long to, double money) throws ATMException {
        if(from!=to){
            //转出账户对象
            Account fromAcc = dao.selectOne(from, passwordFrom);
            if(fromAcc!=null){
                //判断转入账户是否存在
                Account toAcc = dao.selectOne(to);
                if(toAcc == null){
                    System.out.println("转入账户不存在！");
                    return false;
                }
                //转账
                if(fromAcc instanceof SavingAccount){
                    if(fromAcc.getBalance()<money){
                        System.out.println("转出账户余额不足！");
                        return false;
                    }
                }
                if(fromAcc instanceof CreditAccount){
                    if(fromAcc.getBalance()+((CreditAccount) fromAcc).getCeiling()<money){
                        System.out.println("转出账户余额不足！");
                        return false;
                    }
                }
                //转出账户 取款
                fromAcc.withdraw(money);
                //转入账户 存款
                toAcc.deposit(money);
                return true;
            }
        }else{
            System.out.println("转入账户和转出账户不能为同一个！");
        }
        return false;
    }
    //统计银行所有账户余额总数
    public double totalBalance(){
        double sum = 0.0;
        Account[] accs = dao.selectAll();
        for (Account acc : accs) {
            sum += acc.getBalance();
        }
        return sum;
    }

    //统计所有信用账户透支额度总数  --需要验证账户类型 信用账户才有透支额度
    public double totalCeiling(){
        double sum = 0;
        Account[] accs = dao.selectAll();
        for (Account acc : accs) {
            if (acc instanceof CreditAccount){
                sum += ((CreditAccount) acc).getCeiling();
            }
        }
        return sum;
    }


    /**  贷款
     *
     * @param id
     * @param money
     * @return
     */

    public Account requestLoan(Long id , double money){
        if(id!=null && money!=0){
            Account account = dao.selectOne(id);
            if (account != null) {
                ((Loanable) account).requestLoan(money);
                return account;
            }else {
                System.out.println("账号不正确!");
            }
        }else {
            System.out.println("输入的账号或金额不能为空！");
        }
        return null;
    }


    /**  还贷
     *
     * @param id
     * @param password
     * @param money
     * @return
     */
    public Account payLoan(Long id ,String password, double money) throws ATMException {
        if(id!=null && password!=null && !"".equals(password) && money!=0){
            Account account = dao.selectOne(id,password);
            if (account != null) {
                if (account instanceof Loanable) {
                    ((Loanable) account).payLoan(money);
                    return account;
                }else {
                    System.out.println("非贷款用户!");
                }
            }else {
                System.out.println("账号密码不正确!");
            }
        }
        return null;
    }
    //所有用户的总资产排名
    public TreeSet<RankVO> rank(){
        return null;
    }


}
