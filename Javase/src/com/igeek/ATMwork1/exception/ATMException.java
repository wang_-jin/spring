package com.igeek.ATMwork1.exception;

/**
 * @author wangjin
 * 2023/9/11 21:08
 * @description 自定义异常的规范：
 * 1.继承Exception,提供构造方法           编译期异常
 *      使用场景：除了main函数或者视图层要trycatch处理，其他地方直接throws向上抛取
 * 2.继承RuntimeException,提供构造方法    运行期异常
 *      使用场景:无需进行trycatch或者throws处理，当出现此类异常时，直接中断程序运行
 */
public class ATMException extends Exception{
    public ATMException() {
    }

    public ATMException(String message) {
        super(message);
    }
}