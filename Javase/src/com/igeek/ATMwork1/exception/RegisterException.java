package com.igeek.ATMwork1.exception;



/**
 * @author wangjin
 * 2023/9/11 21:09
 * @description 用于开户异常的情况,例如密码两次输入不一致等情况，本月开户已达上限....

 */
public class RegisterException extends ATMException {

    public RegisterException() {
    }

    public RegisterException(String message) {
        super(message);
    }
}
