package com.igeek.ATMwork1.test.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author wangjin
 * 2023/9/11 22:04
 * @description TODO
 */
public class MyFrame extends JFrame {

    public MyFrame() throws HeadlessException {
        //设置标题
        this.setTitle("第一个窗体");
        //设置大小
        this.setSize(300,500);
        //设置位置 绝对居中
        this.setLocationRelativeTo(null);
        //设置默认关闭操作：退出程序
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        MyFrame myFrame = new MyFrame();
        //设置其可见性
        myFrame.setVisible(true);
    }
}

