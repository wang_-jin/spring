package com.igeek.ATMwork1.test.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author wangjin
 * 2023/9/11 22:04
 * @description TODO
 */
public class CardLayoutDemo extends JFrame {

    public CardLayoutDemo() throws HeadlessException {
        //设置窗体属性
        this.setTitle("这是一个卡片式布局案例");
        this.setSize(300,600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        //获取容器(内容面板)
        Container container = this.getContentPane();
        //设置边界式布局
        container.setLayout(new BorderLayout());

        //创建两个JPanel 并放置在container里面
        JPanel pan1 = new JPanel();
        JPanel pan2 = new JPanel();

        //pan1 卡片式布局
        CardLayout cardLayout = new CardLayout();
        pan1.setLayout(cardLayout);
        //pan1 放置5个按钮
        for (int i = 1; i <=5; i++) {
            pan1.add(new JButton("按钮"+i));
        }

        //pan2 流式布局
        pan2.setLayout(new FlowLayout());
        //pan 放置 上一个 下一个按钮
        JButton preBtn = new JButton("上一个");
        JButton nextBtn = new JButton("下一个");
        //添加点击事件
        preBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.previous(pan1);
            }
        });
        nextBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.next(pan1);
            }
        });
        pan2.add(preBtn);
        pan2.add(nextBtn);

        //把pan1 pan2 放置在container
        container.add(pan1,BorderLayout.CENTER);
        container.add(pan2,BorderLayout.SOUTH);

    }

    public static void main(String[] args) {
        CardLayoutDemo demo = new CardLayoutDemo();
        demo.setVisible(true);
    }
}