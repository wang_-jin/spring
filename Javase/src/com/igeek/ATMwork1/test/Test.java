package com.igeek.ATMwork1.test;


import com.igeek.ATMwork1.entity.Account;
import com.igeek.ATMwork1.exception.ATMException;
import com.igeek.ATMwork1.exception.RegisterException;
import com.igeek.ATMwork1.servise.Bank;

/**
 * @author wangjin
 * 2023/9/11 22:00
 * @description TODO
 */
public class Test {
    public static void main(String[] args) throws ATMException {
        Bank bank = Bank.getInstance();
        //开户
        Account acc1 =
                bank.register( "1", "1", "a", "111", "111@163", 0);
        System.out.println("acc1 = " + acc1);

        Account acc4 =
                bank.register( "1", "1", "a", "111", "111@163", 1);
        System.out.println("acc4 = " + acc4);

        //登录
        Bank bank2 = Bank.getInstance();
        Account acc2 = bank2.login(862150212023090001L, "1");
        System.out.println("acc2 = " + acc2);

        //存款
        Account acc3 = bank.deposit(862150212023090001L, 2000);
        System.out.println("acc3 = " + acc3);

        //取款
        Account acc5 = bank.withdraw(862150212023090002L, "1", 1000);
        System.out.println("acc5 = " + acc5);


        //设置透支
        System.out.println("================设置透支===============");
        Account acc6 = bank.updateCeiling(862150212023090002L,"1",5000);
        System.out.println("acc6账户："+bank.login(862150212023090002L, "1"));


        //转账
        System.out.println("================转账===============");
        boolean transfer = bank.transfer(862150212023090001L, "1", 862150212023090002L, 500);
        System.out.println("转出账户："+bank.login(862150212023090001L, "1"));
        System.out.println("转入账户："+bank.login(862150212023090002L, "1"));

        //统计银行所有账户余额总数
        System.out.println("所有账户的总余额 = " + bank.totalBalance());

        //统计所有信用账户透支额度总数
        System.out.println("所有信用账户透支额度总数 = " + bank.totalCeiling());
    }
}

