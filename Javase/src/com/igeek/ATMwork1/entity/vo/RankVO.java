package com.igeek.ATMwork1.entity.vo;

/**
 * @author wangjin
 * 2023/9/11 21:12
 * @description 专门用来 封装 总资产排名结果的 实体类
 */
public class RankVO {
    private String personId;//身份证
    private String name;//姓名
    private Double balance;//总资产

    public RankVO() {
    }

    public RankVO(String personId, String name, Double balance) {
        this.personId = personId;
        this.name = name;
        this.balance = balance;
    }

    /**
     * 获取
     * @return personId
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * 设置
     * @param personId
     */
    public void setPersonId(String personId) {
        this.personId = personId;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return balance
     */
    public Double getBalance() {
        return balance;
    }

    /**
     * 设置
     * @param balance
     */
    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String toString() {
        return "RankVO{personId = " + personId + ", name = " + name + ", balance = " + balance + "}";
    }
}
