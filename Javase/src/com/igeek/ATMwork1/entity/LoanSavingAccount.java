package com.igeek.ATMwork1.entity;


import com.igeek.ATMwork1.exception.ATMException;

/**
 * @author wangjin
 * 2023/9/11 20:59
 * @description TODO
 */
public class LoanSavingAccount extends SavingAccount implements Loanable {
    //贷款金额
    private Long loanAmount;

    public LoanSavingAccount() {
    }

    public LoanSavingAccount(long id, String password, String name, String personId, String email, double balance, Long loanAmount) {
        super(id, password, name, personId, email, balance);
        this.loanAmount = loanAmount;
    }

    public LoanSavingAccount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }


    @Override
    public Account requestLoan(double money) {
        //贷款金额增加
        this.setLoanAmount(this.getLoanAmount()+(long) money);
        //余额增加
        this.setBalance(this.getBalance()+money);
        return this;
    }

    @Override
    public Account payLoan(double money) throws ATMException {
        //判断余额是否充足 不足以还款
        if (this.getBalance()>=money){
            //贷款金额减少
            this.setLoanAmount(this.getLoanAmount()-(long) money);
            //余额减少
            this.setBalance(this.getBalance()-money);
        }else {
            throw new ATMException("银行余额不足");
        }

        return this;
    }

    /**
     * 获取
     * @return loanAmount
     */
    public long getLoanAmount() {
        return loanAmount;
    }

    /**
     * 设置
     * @param loanAmount
     */
    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String toString() {
        return super.toString()+"LoanSavingAccount{loanAmount = " + loanAmount + "}";
    }
}
