package com.igeek.ATMwork1.entity;

import com.igeek.ATMwork1.exception.ATMException;

/**
 * @author wangjin
 * 2023/9/11 20:33
 * @description TODO
 */
public abstract class Account {
    private Long id;//账户号码
    private String password;//账户密码
    private String name;//真实姓名
    private String personId;//身份证号码
    private String email;//客户的电子邮箱
    private Double balance;//账户余额

    public Account() {
    }

    public Account(long id, String password, String name, String personId, String email, double balance) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.personId = personId;
        this.email = email;
        this.balance = balance;
    }
    //右击  选择 ptg to javaBean 一键 生成构造方法 set/get方法 toString()


    public final Account deposit(double money){
        this.balance += money;
        return this;
    }
    public abstract Account withdraw(double money) throws ATMException;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", personId='" + personId + '\'' +
                ", email='" + email + '\'' +
                ", balance=" + balance +
                '}';
    }
}
