package com.igeek.ATMwork1.entity;


import com.igeek.ATMwork1.exception.ATMException;

/**
 * 贷款接口
 */
public interface Loanable {

    //贷款
    public Account requestLoan(double  money);

    //还贷
    public Account payLoan(double money) throws ATMException;
}
