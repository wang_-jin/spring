package com.igeek.ATMwork1.entity;

import com.igeek.ATMwork1.exception.ATMException;

/**
 * @author wangjin
 * 2023/9/11 20:59
 * @description TODO
 */
public class LoanCreditAccount extends CreditAccount implements Loanable{
    //贷款金额
    private Long loanAmount;

    public LoanCreditAccount() {
    }

    public LoanCreditAccount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public LoanCreditAccount(long id, String password, String name, String personId, String email, double balance, double ceiling, Long loanAmount) {
        super(id, password, name, personId, email, balance, ceiling);
        this.loanAmount = loanAmount;
    }

    @Override
    public Account requestLoan(double money) {
        //余额增加
        this.setBalance(this.getBalance()+money);
        //贷款金额增加
        this.setLoanAmount(this.getLoanAmount()+(long)money);
        return this;
    }

    @Override
    public Account payLoan(double money) throws ATMException {
        if ((this.getBalance()+this.getCeiling()+this.getLoanAmount())>=money){
            //方案一：余额  贷款金额减少 余额减少
            if (this.getBalance()>=money){
                this.setLoanAmount(this.getLoanAmount()-(long)money);
                this.setBalance(this.getBalance()-money);
            }else {
                //方案二：余额+透支额度  贷款金额减少 扣用透支额度 余额为0
                this.setLoanAmount(this.getLoanAmount()-(long)money);
                this.setBalance(0.0);
                this.setCeiling(this.getCeiling()-money);
            }
        }else {
            throw new ATMException("银行余额不足");
        }
        return this;
    }

    /**
     * 获取
     * @return loanAmount
     */
    public long getLoanAmount() {
        return loanAmount;
    }

    /**
     * 设置
     * @param loanAmount
     */
    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String toString() {
        return super.toString()+"LoanCreditAccount{loanAmount = " + loanAmount + "}";
    }
}
