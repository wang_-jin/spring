package com.igeek.ATMwork1.entity;

import com.igeek.ATMwork1.exception.ATMException;

/**
 * @author wangjin
 * 2023/9/11 20:40
 * @description TODO
 */
public class CreditAccount extends Account{
    private double ceiling;//透支数额

    public CreditAccount() {
    }

    public CreditAccount(double ceiling) {
        this.ceiling = ceiling;
    }

    public CreditAccount(long id, String password, String name, String personId, String email, double balance, double ceiling) {
        super(id, password, name, personId, email, balance);
        this.ceiling = ceiling;
    }

    @Override
    public Account withdraw(double money) throws ATMException {
        if((this.getBalance()+ceiling)>=money){
            if(this.getBalance()>=money){
                //余额充足
                this.setBalance(this.getBalance()-money);
            }else {
                //余额1000+透支额度2000充足  1500
                this.ceiling = this.ceiling-(money-this.getBalance());
                this.setBalance(0.0);
            }
        }else{
            throw new ATMException("银行余额不足");
        }
        return this;
    }

    /**
     * 透支额度
     * @param money
     * @return
     */
    public Account updateCeiling(double money){
        this.setCeiling(this.getCeiling()+money);
        return this;
    }

    /**
     * 获取
     * @return ceiling
     */
    public double getCeiling() {
        return ceiling;
    }

    /**
     * 设置
     * @param ceiling
     */
    public void setCeiling(double ceiling) {
        this.ceiling = ceiling;
    }

    public String toString() {
        return super.toString()+"CreditAccount{ceiling = " + ceiling + "}";
    }
}
