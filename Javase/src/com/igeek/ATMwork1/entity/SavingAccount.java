package com.igeek.ATMwork1.entity;

import com.igeek.ATMwork1.exception.ATMException;

/**
 * @author wangjin
 * 2023/9/11 20:55
 * @description TODO
 */
public class SavingAccount extends Account{
    public SavingAccount() {
    }

    public SavingAccount(long id, String password, String name, String personId, String email, double balance) {
        super(id, password, name, personId, email, balance);
    }

    @Override
    public Account withdraw(double money) throws ATMException {
        if(this.getBalance()<money){
            throw new ATMException("余额不足");//抛出自定义异常
        }
        this.setBalance(this.getBalance()-money);
        return this;
    }

    @Override
    public String toString() {
        return "SavingAccount{} " + super.toString();
    }
}
