package com.igeek.ATMwork1.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author wangjin
 * 2023/9/11 21:06
 * @description TODO
 */
public class RegisterPanel extends JPanel {


    public RegisterPanel() {
        initComponents();
    }

    //提交注册表单
    private void submitButtonActionPerformed(ActionEvent e) {
        //收集注册表单中的数据
        String name = nameTextField.getText();
        String password = new String(passwordField1.getPassword());
        String repassword = new String(passwordField2.getPassword());
        int type = comboBox.getSelectedIndex();
        String personId = personIdTextField.getText();
        String address = addressTextField.getText();
        String email = emailTextField.getText();

        //跳转至BusinessPanel页面
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"businessPanel");

        //清空注册界面数据
        clearRegister();


    }

    //返回主界面
    private void backButtonActionPerformed(ActionEvent e) {
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"mainPanel");

        //清空注册界面
        clearRegister();
    }

    //清空注册表单
    public void clearRegister(){
        nameTextField.setText("");
        passwordField1.setText("");
        passwordField2.setText("");
        comboBox.setSelectedIndex(0);
        personIdTextField.setText("");
        addressTextField.setText("");
        emailTextField.setText("");
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        title = new JLabel();
        typeLabel = new JLabel();
        nameLabel = new JLabel();
        pwdLabel = new JLabel();
        repwdLabel = new JLabel();
        personIdLabel = new JLabel();
        addressLabel = new JLabel();
        emailLabel = new JLabel();
        nameTextField = new JTextField();
        passwordField1 = new JPasswordField();
        passwordField2 = new JPasswordField();
        comboBox = new JComboBox<>();
        personIdTextField = new JTextField();
        addressTextField = new JTextField();
        emailTextField = new JTextField();
        submitButton = new JButton();
        backButton = new JButton();

        //======== this ========
        setLayout(null);

        //---- title ----
        title.setText("\u6CE8 \u518C \u754C \u9762");
        title.setFont(new Font("Microsoft YaHei", Font.PLAIN, 24));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        add(title);
        title.setBounds(115, 65, 245, 35);

        //---- typeLabel ----
        typeLabel.setText("\u8D26\u6237\u7C7B\u578B");
        typeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(typeLabel);
        typeLabel.setBounds(55, 135, 105, 32);

        //---- nameLabel ----
        nameLabel.setText("\u7528 \u6237 \u540D");
        nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(nameLabel);
        nameLabel.setBounds(55, 190, 105, 32);

        //---- pwdLabel ----
        pwdLabel.setText("\u5BC6    \u7801");
        pwdLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(pwdLabel);
        pwdLabel.setBounds(55, 245, 105, 32);

        //---- repwdLabel ----
        repwdLabel.setText("\u91CD \u590D \u5BC6 \u7801");
        repwdLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(repwdLabel);
        repwdLabel.setBounds(55, 300, 105, 32);

        //---- personIdLabel ----
        personIdLabel.setText("\u8EAB \u4EFD \u8BC1 \u53F7");
        personIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(personIdLabel);
        personIdLabel.setBounds(55, 355, 105, 32);

        //---- addressLabel ----
        addressLabel.setText("\u8BE6 \u7EC6 \u5730 \u5740");
        addressLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(addressLabel);
        addressLabel.setBounds(55, 415, 105, 32);

        //---- emailLabel ----
        emailLabel.setText("\u90AE    \u7BB1");
        emailLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(emailLabel);
        emailLabel.setBounds(55, 470, 105, 32);
        add(nameTextField);
        nameTextField.setBounds(185, 195, 190, 30);
        add(passwordField1);
        passwordField1.setBounds(185, 250, 190, 30);
        add(passwordField2);
        passwordField2.setBounds(185, 305, 190, 30);

        //---- comboBox ----
        comboBox.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u50a8\u84c4\u8d26\u6237",
                "\u4fe1\u7528\u8d26\u6237",
                "\u53ef\u8d37\u6b3e\u7684\u50a8\u84c4\u8d26\u6237",
                "\u53ef\u8d37\u6b3e\u7684\u4fe1\u7528\u8d26\u6237"
        }));
        add(comboBox);
        comboBox.setBounds(185, 135, 190, 30);
        add(personIdTextField);
        personIdTextField.setBounds(185, 360, 190, 30);
        add(addressTextField);
        addressTextField.setBounds(185, 415, 190, 30);
        add(emailTextField);
        emailTextField.setBounds(185, 470, 190, 30);

        //---- submitButton ----
        submitButton.setText("\u786E\u8BA4");
        submitButton.addActionListener(e -> submitButtonActionPerformed(e));
        add(submitButton);
        submitButton.setBounds(115, 550, 92, 30);

        //---- backButton ----
        backButton.setText("\u8FD4\u56DE");
        backButton.addActionListener(e -> backButtonActionPerformed(e));
        add(backButton);
        backButton.setBounds(280, 550, 92, 30);

        setPreferredSize(new Dimension(500, 650));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel title;
    private JLabel typeLabel;
    private JLabel nameLabel;
    private JLabel pwdLabel;
    private JLabel repwdLabel;
    private JLabel personIdLabel;
    private JLabel addressLabel;
    private JLabel emailLabel;
    private JTextField nameTextField;
    private JPasswordField passwordField1;
    private JPasswordField passwordField2;
    private JComboBox<String> comboBox;
    private JTextField personIdTextField;
    private JTextField addressTextField;
    private JTextField emailTextField;
    private JButton submitButton;
    private JButton backButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

