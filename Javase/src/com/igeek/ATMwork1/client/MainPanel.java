package com.igeek.ATMwork1.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author wangjin
 * 2023/9/11 21:06
 * @description TODO
 */
public class MainPanel extends JPanel {



    public MainPanel() {
        initComponents();
    }

    //点击登陆按钮，跳转至登陆界面
    private void loginButtonActionPerformed(ActionEvent e) {
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"loginPanel");
    }

    //点击注册按钮，跳转至注册界面
    private void registerButtonActionPerformed(ActionEvent e) {
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"registerPanel");
    }



    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        title = new JLabel();
        loginButton = new JButton();
        registerButton = new JButton();

        //======== this ========
        setLayout(null);

        //---- title ----
        title.setFont(new Font("Microsoft YaHei", Font.BOLD, 24));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setText("\u6A21\u62DF ICBC ATM \u7EC8\u7AEF");
        add(title);
        title.setBounds(75, 65, 320, 45);

        //---- loginButton ----
        loginButton.setText("\u767B\u9646");
        loginButton.addActionListener(e -> loginButtonActionPerformed(e));
        add(loginButton);
        loginButton.setBounds(75, 180, 125, 30);

        //---- registerButton ----
        registerButton.setText("\u6CE8\u518C");
        registerButton.addActionListener(e -> registerButtonActionPerformed(e));
        add(registerButton);
        registerButton.setBounds(275, 180, 125, 30);

        setPreferredSize(new Dimension(500, 360));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel title;
    private JButton loginButton;
    private JButton registerButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

