package com.igeek.ATMwork1.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author wangjin
 * 2023/9/11 21:06
 * @description TODO
 */
public class LoginPanel extends JPanel {

    public LoginPanel() {

        initComponents();
    }

    //确认按钮
    private void okButtonActionPerformed(ActionEvent e) {
        //直接跳转至BusinessPanel界面
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"businessPanel");

        //清空登录界面数据
        clearLogin();
    }

    //点击取消按钮，回到主界面
    private void cancelButtonActionPerformed(ActionEvent e) {
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"mainPanel");

        //清空登录界面
        clearLogin();
    }

    //清空登陆界面
    public void clearLogin(){
        textField.setText("");
        passwordField.setText("");
    }



    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        title = new JLabel();
        idLabel = new JLabel();
        pwdLabel = new JLabel();
        textField = new JTextField();
        passwordField = new JPasswordField();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setLayout(null);

        //---- title ----
        title.setText("\u767B \u9646 \u754C \u9762");
        title.setFont(new Font("Microsoft YaHei", Font.PLAIN, 24));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        add(title);
        title.setBounds(115, 70, 255, 40);

        //---- idLabel ----
        idLabel.setText("\u7528\u6237\u7F16\u53F7");
        idLabel.setFont(new Font("Microsoft YaHei", Font.PLAIN, 20));
        idLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(idLabel);
        idLabel.setBounds(55, 140, 125, 32);

        //---- pwdLabel ----
        pwdLabel.setText("\u7528\u6237\u5BC6\u7801");
        pwdLabel.setFont(new Font("Microsoft YaHei", Font.PLAIN, 20));
        pwdLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(pwdLabel);
        pwdLabel.setBounds(55, 195, 125, 32);
        add(textField);
        textField.setBounds(190, 145, 200, 30);
        add(passwordField);
        passwordField.setBounds(190, 200, 200, 30);

        //---- okButton ----
        okButton.setText("\u786E\u8BA4");
        okButton.addActionListener(e -> okButtonActionPerformed(e));
        add(okButton);
        okButton.setBounds(115, 285, 92, 30);

        //---- cancelButton ----
        cancelButton.setText("\u53D6\u6D88");
        cancelButton.addActionListener(e -> cancelButtonActionPerformed(e));
        add(cancelButton);
        cancelButton.setBounds(270, 285, 92, 30);

        setPreferredSize(new Dimension(500, 360));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel title;
    private JLabel idLabel;
    private JLabel pwdLabel;
    private JTextField textField;
    private JPasswordField passwordField;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

