package com.igeek.ATMwork1.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author wangjin
 * 2023/9/11 21:06
 * @description TODO
 */
public class BusinessPanel extends JPanel {



    public BusinessPanel() {

        initComponents();
    }

    //点击返回按钮，跳转至登陆界面
    private void backButtonActionPerformed(ActionEvent e) {
        CardLayout cardLayout = (CardLayout) this.getParent().getLayout();
        cardLayout.show(this.getParent(),"loginPanel");

        //清空业务界面数据
        clearBusiness();
    }

    //点击提交按钮，执行业务操作，刷新当前页面
    private void submitButtonActionPerformed(ActionEvent e) {
        initBusiness();
    }

    //清空业务界面数据
    public void clearBusiness() {
        idLabelText.setText("");
        nameLabelText.setText("");
        balanceLabelText.setText("");
        moneyTextField.setText(0.0+"");
        ceilingLabelText.setText("");
        loanLabelText.setText("");
    }

    //初始化业务界面
    public void initBusiness(){
        idLabelText.setText("");
        nameLabelText.setText("");
        balanceLabelText.setText("");
        moneyTextField.setText("");


    }



    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        title = new JLabel();
        idLabel = new JLabel();
        idLabelText = new JLabel();
        nameLabel = new JLabel();
        nameLabelText = new JLabel();
        balanceLabel = new JLabel();
        balanceLabelText = new JLabel();
        ceilingLabel = new JLabel();
        ceilingLabelText = new JLabel();
        loanLabel = new JLabel();
        loanLabelText = new JLabel();
        comboBox = new JComboBox<>();
        moneyTextField = new JTextField();
        submitButton = new JButton();
        backButton = new JButton();

        //======== this ========
        setLayout(null);

        //---- title ----
        title.setText("\u4E1A \u52A1 \u754C \u9762");
        title.setFont(new Font("Microsoft YaHei", Font.PLAIN, 24));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        add(title);
        title.setBounds(115, 55, 240, 50);

        //---- idLabel ----
        idLabel.setText("\u8D26      \u6237");
        idLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(idLabel);
        idLabel.setBounds(90, 130, 90, 30);
        add(idLabelText);
        idLabelText.setBounds(185, 135, 190, 30);

        //---- nameLabel ----
        nameLabel.setText("\u59D3      \u540D");
        nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(nameLabel);
        nameLabel.setBounds(90, 190, 90, 30);
        add(nameLabelText);
        nameLabelText.setBounds(185, 195, 190, 30);

        //---- balanceLabel ----
        balanceLabel.setText("\u4F59      \u989D");
        balanceLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(balanceLabel);
        balanceLabel.setBounds(90, 250, 90, 30);
        add(balanceLabelText);
        balanceLabelText.setBounds(185, 255, 190, 30);

        //---- ceilingLabel ----
        ceilingLabel.setText("\u4FE1 \u7528 \u989D \u5EA6");
        ceilingLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(ceilingLabel);
        ceilingLabel.setBounds(90, 305, 90, 30);
        add(ceilingLabelText);
        ceilingLabelText.setBounds(185, 310, 190, 30);

        //---- loanLabel ----
        loanLabel.setText("\u8D37 \u6B3E \u989D");
        loanLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(loanLabel);
        loanLabel.setBounds(90, 360, 90, 30);
        add(loanLabelText);
        loanLabelText.setBounds(185, 360, 190, 30);

        //---- comboBox ----
        comboBox.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u5b58\u6b3e",
                "\u53d6\u6b3e",
                "\u8f6c\u8d26",
                "\u4fee\u6539\u900f\u652f\u989d\u5ea6",
                "\u8d37\u6b3e",
                "\u8fd8\u8d37"
        }));
        comboBox.setPrototypeDisplayValue("\u5B58\u6B3E,\u53D6\u6B3E,\u8F6C\u8D26,\u4FEE\u6539\u900F\u652F\u989D\u5EA6,\u8D37\u6B3E,\u8FD8\u8D37");
        add(comboBox);
        comboBox.setBounds(75, 410, 155, 40);
        add(moneyTextField);
        moneyTextField.setBounds(235, 410, 150, 40);

        //---- submitButton ----
        submitButton.setText("\u63D0\u4EA4");
        submitButton.addActionListener(e -> submitButtonActionPerformed(e));
        add(submitButton);
        submitButton.setBounds(125, 485, 92, 30);

        //---- backButton ----
        backButton.setText("\u8FD4\u56DE");
        backButton.addActionListener(e -> backButtonActionPerformed(e));
        add(backButton);
        backButton.setBounds(265, 485, 92, 30);

        setPreferredSize(new Dimension(500, 650));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel title;
    private JLabel idLabel;
    private JLabel idLabelText;
    private JLabel nameLabel;
    private JLabel nameLabelText;
    private JLabel balanceLabel;
    private JLabel balanceLabelText;
    private JLabel ceilingLabel;
    private JLabel ceilingLabelText;
    private JLabel loanLabel;
    private JLabel loanLabelText;
    private JComboBox<String> comboBox;
    private JTextField moneyTextField;
    private JButton submitButton;
    private JButton backButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
