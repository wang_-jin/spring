package com.igeek.ATMwork1.client;

import javax.swing.*;
import java.awt.*;

/**
 * @author wangjin
 * 2023/9/11 21:06
 * @description TODO
 */
public class ATMClient extends JFrame {

    //JPanel
    MainPanel mainPanel;
    LoginPanel loginPanel;
    RegisterPanel registerPanel;
    BusinessPanel businessPanel;


    public ATMClient(){
        //设置窗体基本的信息
        this.setTitle("ATM取款机");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500,650);
        //居中显示
        this.setLocationRelativeTo(getOwner());



        //实例化JPanel界面
        mainPanel = new MainPanel();
        loginPanel = new LoginPanel();
        registerPanel = new RegisterPanel();
        businessPanel = new BusinessPanel();

        //将JPanel添加至JFrame的容器中
        Container container = this.getContentPane();
        //将容器设置为卡片式布局
        container.setLayout(new CardLayout());
        //默认MainPanel显示在第一个
        container.add(mainPanel,"mainPanel");
        container.add(loginPanel,"loginPanel");
        container.add(registerPanel,"registerPanel");
        container.add(businessPanel,"businessPanel");
    }

    public static void main(String[] args) {
        new ATMClient().setVisible(true);
    }
}
