package com.igeek.ch06.generics.lesson_04.test.test1;

public class Apple<T> {
    T weight;
    public Apple(T weight){
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "weight=" + weight +
                '}';
    }
}
