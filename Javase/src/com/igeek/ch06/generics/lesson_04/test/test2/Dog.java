package com.igeek.ch06.generics.lesson_04.test.test2;

public class Dog implements Animal<String>{
  @Override
  public String eat(String food) {
    System.out.println("小狗在吃"+food);
    return food;
  }
}
