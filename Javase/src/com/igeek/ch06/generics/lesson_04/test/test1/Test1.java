package com.igeek.ch06.generics.lesson_04.test.test1;
/*
1.定义一个泛型类Apple,Apple有一个重量属性weight,weight的类型不确定,
创建两个Apple的实例对象weight分别传入"500克",500,打印重量.
*/
public class Test1 {
    public static void main(String[] args) {
        Apple<String> a = new Apple("500克");
        Apple<Integer> b = new Apple(500);
        System.out.println(a);
        System.out.println(b);
    }
}
