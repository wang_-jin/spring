package com.igeek.ch06.generics.lesson_04.test.test3;
/*
3.定义一个泛型方法foo,泛型参数是T,方法的形参是一个泛型数组,数组中元素类型是T,
方法的返回值类型也是T,当数组是一个数字数组,对数组求和将和返回;当数组是一个字符串
数组,将数组中最后一个字符串返回即可!
*/

public class Tool {
  public static <T> T foo(T[] arr){
    if(arr instanceof Integer[]){
      Integer sum = 0;
      for (T item : arr) {
        int i = (Integer)item;
        sum+=i;
      }
      return (T)sum;
    } else if (arr instanceof String[]) {
      return arr[arr.length-1];
    }else{
      return null;
    }
  }

  public static void main(String[] args) {
    Integer[] arr = {1,2,3,4,5};
    int a = foo(arr);
    System.out.println("a:"+a);
    String[] arr1 = {"a","b","c","d","e"};
    String b = foo(arr1);
    System.out.println("b:"+b);
  }
}
