package com.igeek.ch06.generics.lesson_04.test.test2;

public interface Animal<T> {
  T eat(T food);
}
