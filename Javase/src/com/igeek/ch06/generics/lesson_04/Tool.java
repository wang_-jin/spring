package com.igeek.ch06.generics.lesson_04;

import java.util.ArrayList;
import java.util.Collection;

public class Tool {
    /* 通配符做方法的参数类型 */
    //集合col中元素类型是Number或Number的子类
    public static void foo1(Collection<? extends Number>col){
      //因为col是通配符类型,所以不允许添加元素.但是可以读取元素
      //col.add(1);
      for(Number item:col){
          System.out.println("item:"+item);
      }

    }
    //集合col中元素类型是Number或Number父类
    public static void foo2(Collection<? super Number>col){

    }
    public static void main(String[] args) {
        /*
        注意:
          Number是包装器类型的父类
        */
        //父类(集合中元素类型Object)
        Collection<Object>c1 = new ArrayList<>();
        //当前类(集合中元素类型Number)
        Collection<Number>c2 = new ArrayList<>();
        c2.add(11);
        c2.add(22);
        c2.add(33);
        //子类(集合中元素类型Integer)
        Collection<Integer>c3 = new ArrayList<>();

        //foo1(c1);报错
        foo1(c2);
        foo1(c3);

        foo2(c1);
        foo2(c2);
        //foo2(c3);报错


    }
}
