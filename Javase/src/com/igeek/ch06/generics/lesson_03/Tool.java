package com.igeek.ch06.generics.lesson_03;

public class Tool {
  /*泛型参数作为返回值*/
  public static <T> T foo1(String a) {
    return (T)a;
  }
  /*泛型参数作为方法的形参类型*/
  public static <T> void foo2(T b){
    if(b instanceof String){
      System.out.println("字符串长度为:"+((String) b).length());
    } else if (b instanceof Integer) {
      Integer x =(Integer)b;
      System.out.println("数字的平方是:"+(x*x));
    }
  }
  /*泛型数组作为方法的参数*/
  public static <T> void foo3(T[] arr){
    for (T item:arr){
      System.out.println("item:"+item);
    }
  }

  public static void main(String[] args) {
    String b = foo1("hello");
    foo2(2);
    foo2("hello");
    Integer[] arr = {1,2,3};
    foo3(arr);
  }

}
