package com.igeek.ch06.generics.lesson_02.geninterface;

public class Tool {
  public static void main(String[] args) {
    Person p = new Person();
    //foo的参数和返回值都是泛型T的类型,即String类型
    String a = p.foo("hello");
    System.out.println(a);
  }
}
