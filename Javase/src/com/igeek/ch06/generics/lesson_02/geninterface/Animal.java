package com.igeek.ch06.generics.lesson_02.geninterface;
/*
是昆虫了一个接口Animal,有一个泛型参数T,
foo方法的参数和返回值都是T类型
*/
public interface Animal<T>{
  T foo(T a);
}
