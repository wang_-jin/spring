package com.igeek.ch06.generics.lesson_02.geninterface;
/*
Person类实现了Animal接口并将泛型T实参设置为String类型,
这意味着Animal接口中的foo方法的返回值和参数都是String类型!
*/
public class Person implements Animal<String>{

  @Override
  public String foo(String a) {
    return a;
  }
}
