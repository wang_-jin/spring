package com.igeek.ch06.generics.lesson_02.genclass;
/*
定义泛型类语法:
  class 类名<T,E,...>{
  }
*/
public class Person<T>{
    T id;
    int age;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", age=" + age +
                '}';
    }

    public Person(){};
    public Person(T id,int age){
        this.id = id;
        this.age = age;
    }

}
