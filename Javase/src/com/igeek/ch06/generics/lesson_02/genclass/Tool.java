package com.igeek.ch06.generics.lesson_02.genclass;

public class Tool {
    public static void main(String[] args) {
        /*
        使用泛型类语法:
          类名<数据类型> 变量名= new 类名<数据类型>();
        */

        //下面代码的意思是将String作为泛型实参传入到Person中,
        //Person类中的泛型形参T实际的类型就是String了
        Person<String> p1 = new Person<String>("9527",18);
        //简写形式
        Person<Integer> p2 = new Person("3567",20);
        System.out.println(p2);
    }
}
