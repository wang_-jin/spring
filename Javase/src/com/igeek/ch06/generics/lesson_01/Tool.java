package com.igeek.ch06.generics.lesson_01;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Tool {
    public static void main(String[] args) {
        /*ArrayList list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add("a");
        for (Object o : list) {
            *//*
            list中不仅仅有Integer类型的元素,还有String类型的元素,
            将String强转为Integer会报错,有没有什么办法保证集合中
            元素类型统一为Integer呢?答案是YES,使用泛型!
            *//*
            Integer i = (Integer) o;
            System.out.println(i);
        }*/

        /*
        ArrayList<Integer>的意思:
          ArrayList集合中元素都是Integer类型,
          尖括号<>是表示泛型的符号,尖括号里面
          可以填写某种 数据类型!

        注意:
          泛型只能是引用类型,不能是基本类型
        */
        ArrayList<Integer> list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        for (Integer i : list) {
            System.out.println(i);
        }

        //泛型Map,键和值都是String类型
        Map<String,String> map = new HashMap();
        map.put("name","刘备");
        map.put("age","18");
    }
}
