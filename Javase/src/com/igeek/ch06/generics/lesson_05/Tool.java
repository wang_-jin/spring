package com.igeek.ch06.generics.lesson_05;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Tool {
    public static void main(String[] args) {
        /*将数组转为List*/
        List list = Arrays.asList(7,1,11,9,5,3);
        //反转
        Collections.reverse(list);
        System.out.println("反转后:"+list);
        //乱序
        Collections.shuffle(list);
        System.out.println("乱序后:"+list);
        //升序
        Collections.sort(list);
        System.out.println("升序后:"+list);
        //交换
        Collections.swap(list,0,5);
        System.out.println("交换后:"+list);
        //最大值
        System.out.println("最大值:"+Collections.max(list));
        //最小值
        System.out.println("最小值:"+Collections.min(list));
    }
}
