## Collections工具类
addAll(Collection,Object... elements)添加元素到集合中(将数组转集合)
reverse(List)反转List
shuffle(List)打乱List中元素顺序
sort(List)升序排序
sort(List,Comparator)自定义比较器排序
swap(List list,int i,int j)将i和j位置元素交换
max(Collection)根据自然排序返回最大值
min(Collection)根据自然排序返回最小值
fill(List list, Object obj)用指定的元素替换集合中所有元素
boolean replaceAll(List list, Object oldVal, Object newVal)替换元素