package com.igeek.ch06.generics_test.test4;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author wangjin
 * 2023/8/29 10:21
 * @description TODO
 */
public class Test {
    static String getList(List list){
        Map<String,Integer> map = new HashMap<>();
        int count = 0;
        for(int i=0; i< list.size();i++){
            String str = (String) list.get(i);
            if (list.indexOf(str)!=i){
                count++;
                map.put(str,count);
            }
        }
        System.out.println(map);
        List<String> list1 = new ArrayList<>();
        Integer maxValues = Collections.max(map.values());
        map.forEach((key,values)->{
            if (values.equals(maxValues)){
                list1.add(key);
            }
        });
        String s = list1.get(0);
        return s;
    }
    public static void main(String[] args) {
        String[] str = {"a","b","c","c","d","d","c"};
        List<String> list = new ArrayList<>();
        Collections.addAll(list,str);
        System.out.println(list);
        System.out.println("重复出现次数最多的字符:"+getList(list));
    }
}
