package com.igeek.ch06.generics_test.test3;

/**
 * @author wangjin
 * 2023/8/28 16:24
 * @description TODO
 */
public class Test {
    /*
3.定义一个泛型方法foo,泛型参数是T,方法的形参是一个泛型数组,数组中元素类型是T,
方法的返回值类型也是T,当数组是一个数字数组,对数组求和将和返回;当数组是一个字符串
数组,将数组中最后一个字符串返回即可!*/
    public static <T> T foo(T[] arr) {
        Integer sum = 0;
/*        for (Object o : arr) {
            if (o instanceof Integer) {
                Integer i = (Integer) o;
                sum += i;
            } else if (o instanceof String) {
                return arr[arr.length - 1];
            }
        }
        return (T) sum;*/
        if (arr instanceof Integer[]){
            for (Object o : arr){
                Integer i = (Integer) o;
                sum += i;
            }
            return (T) sum;
        }else if(arr instanceof String[]){
                return arr[arr.length-1];
        }else {
            return null;
        }
    }

    public static void main(String[] args) {
        Integer[] arr = {1,3,5};
        String[] arr1 = {"a","b","sgs","cs"};
        Integer sum = foo(arr);
        String s = foo(arr1);
        System.out.println("sum:"+sum);
        System.out.println("s:"+s);

    }
}
