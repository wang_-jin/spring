package com.igeek.ch06.generics_test.test5;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author wangjin
 * 2023/8/29 19:37
 * @description TODO
 */
public class Test {
    //col是Number类型或其子类类型
    public static void foo1(Collection<? extends Number>col){
        //col是通配符类型,不允许添加元素,可读取元素
        for(Object i : col){
            System.out.println(i);
        }
    }
    //col是Number类型或其父类类型
    public static void foo2(Collection<? super Number>col){
        //col是通配符类型,不允许添加元素,可读取元素
            System.out.println("Object类");
    }

    public static void main(String[] args) {
        Collection<Number>col1 = new ArrayList<>();
        col1.add(1);
        col1.add(2);
        col1.add(3);
        /*Number是包装器类型的父类*/
        Collection<Integer>col2 = new ArrayList<>();
        Collection<Object>col3 = new ArrayList<>();
        foo1(col1);//col1是foo1传入形参类型的类型或其子类类型
        foo1(col2);//col2是foo1传入形参类型的类型或其子类类型
        //foo1(col3);//col3是foo1传入形参类型的父类类型

        foo2(col1);
        foo2(col3);
        //foo2(col2);//col2是Object的子类类型
    }
}
