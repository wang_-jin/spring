package com.igeek.ch06.generics_test.test1;

/**
 * @author wangjin
 * 2023/8/28 16:25
 * @description TODO
 */
public class Apple<T> {
    T weight;

    public Apple() {
    }

    public Apple(T weight) {
        this.weight = weight;
    }
}
