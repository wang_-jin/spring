package com.igeek.ch06.generics_test.test1;

/**
 * @author wangjin
 * 2023/8/28 16:23
 * @description TODO
 */
public class Test {
    /*1.定义一个泛型类Apple,Apple有一个重量属性weight,weight的类型不确定,
创建两个Apple的实例对象weight分别传入"500克",500,打印重量.
*/
    public static void main(String[] args) {
        Apple<String> apple1 = new Apple<>("500克");
        Apple<Integer> apple2 = new Apple<>(500);
        System.out.println(apple1.weight);
        System.out.println(apple2.weight);
    }
}
