package com.igeek.ch06.generics_test.test2;

/**
 * @author wangjin
 * 2023/8/28 16:34
 * @description TODO
 */
public class Dog implements Animal<String>{

    @Override
    public String eat(String a) {
        System.out.println("小狗在吃"+a);
        return a;
    }
}
