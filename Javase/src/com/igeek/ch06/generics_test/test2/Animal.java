package com.igeek.ch06.generics_test.test2;

public interface Animal<T> {
     T eat(T a);
}
