package com.igeek.ch06.generics_test.test2;

/**
 * @author wangjin
 * 2023/8/28 16:24
 * @description TODO
 */
public class Test {
    /*2.定义一个泛型接口Animal,Animal有一个抽象方法eat,eat的返回值类型和
参数类型都是T,创建一个Animal接口的实现类Dog,Animal接口泛型实参传入String类型,
创建Dog的实例对象,调用eat方法并传入字符串参数"骨头",打印"小狗在吃骨头",
并返回传入的实参"骨头"*/
    public static void main(String[] args) {
        Dog dog = new Dog();
        System.out.println(dog.eat("骨头"));
    }
}
