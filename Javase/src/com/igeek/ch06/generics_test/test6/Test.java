package com.igeek.ch06.generics_test.test6;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author wangjin
 * 2023/8/29 19:56
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        /*将数组转换为集合*/
        List<Integer> list = Arrays.asList(1,6,2,14,5);
        /*反转*/
        Collections.reverse(list);
        System.out.println(list);
        /*打乱*/
        Collections.shuffle(list);
        System.out.println(list);
        /*排序:默认升序*/
        Collections.sort(list);
        System.out.println(list);
        /*交换元素*/
        Collections.swap(list,1,4);
        System.out.println(list);
        /*取最大值*/
        System.out.println(Collections.max(list));
        /*取最小值*/
        System.out.println(Collections.min(list));

    }
}
