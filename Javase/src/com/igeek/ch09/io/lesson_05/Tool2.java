package com.igeek.ch09.io.lesson_05;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/*
写出properties文件
*/
public class Tool2 {
    public static void main(String[] args) {
        try(
                FileWriter fw = new FileWriter("javase/src/user.properties",true);
                ){
            //创建properties对象
            Properties props = new Properties();
            //写出数据到properties文件中
            props.setProperty("gender","女");
            props.setProperty("like","游泳");
            //存储属性列表,第二个参数是注释
            props.store(fw,"");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
