package com.igeek.ch09.io.lesson_05;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

/*
读入properties文件  属性集文件
*/
public class Tool1 {
    public static void main(String[] args) {
        try(
                FileReader fr = new FileReader("javase/src/user.properties");
                ){
            //创建properties对象
            Properties props = new Properties();
            //加载properties文件
            props.load(fr);
            //根据key键获取value值
            System.out.println("name="+props.getProperty("name"));
            System.out.println("age="+props.getProperty("age"));
            System.out.println("---------获取所有的键-----------");
            Set<String> keys = props.stringPropertyNames();
            keys.forEach(System.out::println);



        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
