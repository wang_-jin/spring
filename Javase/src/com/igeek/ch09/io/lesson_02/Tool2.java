package com.igeek.ch09.io.lesson_02;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Tool2 {
    /*
    BufferedOutputStream:写出字节流
    */
    public static void main(String[] args) {
        try(
                FileOutputStream outputStream = new FileOutputStream("b.txt",true);
                BufferedOutputStream bos = new BufferedOutputStream(outputStream);
                ){
            bos.write(new byte[]{'1','2','3'});
            bos.flush();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
