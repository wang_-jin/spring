package com.igeek.ch09.io.lesson_02;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Tool4 {
    /*
    BufferedWriter:带缓存的写出字符流
    常用方法:
      newLine:换行
    */
    public static void main(String[] args) {
        try(
                BufferedWriter bw = new BufferedWriter(new FileWriter("d.txt",true));
                ){
            bw.newLine();
            bw.write("你好");
            bw.flush();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
