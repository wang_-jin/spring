package com.igeek.ch09.io.lesson_02;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Tool1 {
  /*
  BufferedInputStream:带缓存的读入字节流
  BufferedOutputStream:带缓存的写出字节流
  BufferedReader:带缓存的读入字符流
  BufferedWriter:带缓存的写出字符流
  */
  public static void main(String[] args) {
    try (
      FileInputStream fileInputStream = new FileInputStream("a.txt");
      BufferedInputStream bis = new BufferedInputStream(fileInputStream);
    ) {
      byte[] bytes = new byte[1024];
      int len = 0;
      while ((len = bis.read(bytes)) != -1) {
        System.out.println(new String(bytes, 0, len));
      }

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
