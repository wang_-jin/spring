package com.igeek.ch09.io.lesson_02;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Tool3 {
    /*
    BufferedReader:带缓存的读入字符流
    常用方法:
      readLine
    */
    public static void main(String[] args) {
        try(
                BufferedReader br = new BufferedReader(new FileReader("c.txt"))
                ){
            String s = "";
            while ((s=br.readLine())!=null){
                System.out.println(s);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
