package com.igeek.ch09.io.lesson_04;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

/*
PrintWriter:打印流,是字节写出流(使用的比较多)
PrintWriter构造函数参数可以是路径,还可以是OutputStream实现类,
因此使用范围比较广,使用的比较多!
*/
public class Tool2 {
    public static void main(String[] args) {
        try(
                PrintWriter pw = new PrintWriter("1.txt");
                ){
            pw.println(1);
            pw.println(2);
            pw.println(3);
            pw.flush();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
