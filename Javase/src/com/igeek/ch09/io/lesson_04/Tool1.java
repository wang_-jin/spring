package com.igeek.ch09.io.lesson_04;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/*
PrintStream:打印流,是字节写出流
*/
public class Tool1 {
    public static void main(String[] args) {
        try(
                PrintStream ps = new PrintStream("1.txt");
                ){
            /*
            将字符串写出到1.txt中
            注意:
              PrintStream不能在文件后面追加内容,新内容会替换掉旧内容!
              如果想在文件后面追加内容,可以使用FileOutputStream.
            */
            System.setOut(ps);
            ps.println("你好");
            ps.println("我好");
            ps.println("大家好");
            ps.flush();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
