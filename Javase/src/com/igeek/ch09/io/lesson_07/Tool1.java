package com.igeek.ch09.io.lesson_07;

import sun.misc.IOUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/*copy:文件拷贝*/
public class Tool1 {
    public static void main(String[] args) {
        try(
            FileInputStream fis = new FileInputStream("1.txt");
            FileOutputStream fos = new FileOutputStream("2.txt");
           ) {
            //文件复制
            //IOUtils.copy(fis,fos);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
