package com.igeek.ch09.io.lesson_03;

import java.io.*;

public class Tool2 {
    public static void main(String[] args) {
        try(
                //获取文件写出字节流
                OutputStream outputStream =new FileOutputStream("1.txt",true);
                //将字节流转换为字符流
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);
                //包装成更高效的缓存流
                BufferedWriter bw = new BufferedWriter(osw);
        ){
            bw.newLine();
            bw.write("你好在下洪七公");
            bw.flush();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
