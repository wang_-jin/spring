package com.igeek.ch09.io.lesson_03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Tool1 {
    public static void main(String[] args) {
        try(
                //获取同级目录下字节流文件
                InputStream inputStream = Tool1.class.getResourceAsStream("a.txt");
                //将字节流转换为字符流
                InputStreamReader isr =new InputStreamReader(inputStream);
                //包装成高效的缓冲流
                BufferedReader br = new BufferedReader(isr);
                ){
            String str = null;
            while ((str=br.readLine())!=null){
                System.out.println(str);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
