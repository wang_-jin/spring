package com.igeek.ch09.io.lesson_06;

import java.io.Serializable;

public class Person1 implements Serializable {
    /*
    设置serialVersionUID可以解决对象修改时,反序列化失败的问题
    */
    private static final long serialVersionUID = 1L;
    private String name;
    private int age;
    private String like;
    /*
    给对象添加一个属性再反序列化会报错:
    java.io.InvalidClassException: ch09.io.lesson_06.Person1;
    local class incompatible:
      stream classdesc serialVersionUID = 8210909208429632797,
      local class serialVersionUID = -427846836965983095
	at ch09.io.lesson_06.Tool4.main(Tool4.java:32)
	因为每个对象都有一个唯一的标记serialVersionUID,序列化
	和反序列化都是根据这个serialVersionUID来判断是否是同一个对象,
	当对象发生变化serialVersionUID就会变化,序列化前和序列化后就不是
	同一个对象了,因此会出错!
	如何解决:
	  在类中添加唯一的serialVersionUID属性即可
    */

    public Person1() {
    }

    public Person1(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person1{name = " + name + ", age = " + age + "}";
    }
}
