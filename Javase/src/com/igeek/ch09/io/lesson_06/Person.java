package com.igeek.ch09.io.lesson_06;

import java.io.Serializable;

public class Person implements Serializable {
    private String name;
    private int age;
    /*
    transient瞬态:
      使用transient修饰的属性不会被序列化到本地,序列化的是该
      属性的默认值
    */
    private transient String password;

    public Person() {
    }
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String password) {
        this.name = name;
        this.age = age;
        this.password = password;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + ", password = " + password + "}";
    }
}
