package com.igeek.ch09.io.lesson_06;

import java.io.*;

public class Tool4 {
  public static void main(String[] args) {
        /*try(
                ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream("person2.txt"))
        ) {
            oos.writeObject(new Person1(
                    "刘备",18));
            oos.flush();
            System.out.println("写入成功");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/

    try (
      ObjectInputStream ois = new ObjectInputStream(
        new FileInputStream("person2.txt")
      )
    ) {
      Person1 p = (Person1) ois.readObject();
      System.out.println(p);

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
