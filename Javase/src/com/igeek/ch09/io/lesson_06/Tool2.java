package com.igeek.ch09.io.lesson_06;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/*对象反序列化*/
public class Tool2 {
    public static void main(String[] args) {
        try(
                ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream("person.txt")
                )
                ){
            Person p =(Person)ois.readObject();
            System.out.println(p);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
}
