package com.igeek.ch09.io.lesson_06;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/*序列化*/
public class Tool1 {
    public static void main(String[] args) {
        try(
                ObjectOutputStream oos = new ObjectOutputStream(new
                FileOutputStream("person.txt"));
            ){
            oos.writeObject(new Person("刘备",18));
            oos.flush();
            System.out.println("序列化成功");

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
