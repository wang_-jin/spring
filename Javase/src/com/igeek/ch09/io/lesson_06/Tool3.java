package com.igeek.ch09.io.lesson_06;

import java.io.*;

/*transient*/
public class Tool3 {
  public static void main(String[] args) {
    try (
      ObjectOutputStream oos = new ObjectOutputStream(
        new FileOutputStream("person1.txt"))
    ) {
      oos.writeObject(new Person(
        "刘备", 18, "123456"));
      oos.flush();
      System.out.println("写入成功");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    try (
      ObjectInputStream ois = new ObjectInputStream(
        new FileInputStream("person1.txt")
      )
    ) {
      Person p = (Person) ois.readObject();
      System.out.println(p);

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
