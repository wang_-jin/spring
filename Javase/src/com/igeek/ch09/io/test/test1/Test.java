package com.igeek.ch09.io.test.test1;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
  public static void main(String[] args) {

    try (
      //读入文件对象
      BufferedReader br = IOUtils.buffer(new FileReader("1.txt"));
      //写出文件对象
      BufferedWriter bw = IOUtils.buffer(new FileWriter("2.txt"));
    ) {
      //获取一行文本
      String s = br.readLine();
      //获取字符串中字母
      Pattern ptn = Pattern.compile("[a-zA-Z]");
      Matcher matcher = ptn.matcher(s);
      Map<String, Integer> map = new HashMap<>();
      while (matcher.find()) {
        String key = matcher.group();
        //将字母作为key,字母出现的次数作为value存储到map中
        Integer value = map.getOrDefault(key, 0) + 1;
        map.put(key, value);
      }

      //for循环将map键值使用=号连接,存储到文件2中
      map.forEach((key, value) -> {
        try {
          bw.write(key + "=" + value);
          bw.newLine();
          bw.flush();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

  }
}
