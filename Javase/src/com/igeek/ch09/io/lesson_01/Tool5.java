package com.igeek.ch09.io.lesson_01;

import java.io.FileWriter;
import java.io.IOException;

public class Tool5 {
    /*
    JDK1.7之后资源操作新的写法:
      try(创建流对象语句,如果有多个流对象,使用分号隔开即可){

      }catch{

      }
     该写法的特点:
       1.不用close关闭,流会自动关闭
       2.try圆括号里面只能创建实现了Closeable类的流对象
    */
    public static void main(String[] args) {
        try(FileWriter writer=new FileWriter("a.txt",true)){
          writer.write("hello");
          writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
