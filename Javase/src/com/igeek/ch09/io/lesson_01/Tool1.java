package com.igeek.ch09.io.lesson_01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Tool1 {
    public static void main(String[] args) {
        /*
        FileInputStream流:读入字节
          int read(byte b[]):按照指定的字节数组读入,
          返回一个整数值,该整数值表示读取到的字节数据,
          如果已经读取到文件末尾,
          read方法会返回-1,表示没有更多的数据可以读取了

          void close():关闭操作

          使用场景:
            字节流可以处理各种类型的二进制文件,例如图片,音频,视频等
        */
        FileInputStream inputStream = null;
        //注意:try catch是一个代码块作用域,在里面定义的变量外面使用不了
        //变量只在try catch代码块内部生效
        try {
            //1.创建输入流对象,参数是当前项目下的文件名
            inputStream = new FileInputStream("1.txt");
            //定义字节数组接受读出的数据
            byte[] bytes = new byte[1024];
            int len = 0;
            //2.读入文件内容
            while ((len=inputStream.read(bytes))!=-1){
                System.out.println(new String(bytes,0,len));
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            //3.关闭资源
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }
}
