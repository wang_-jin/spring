package com.igeek.ch09.io.lesson_01;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Tool2 {
    /*
    FileOutputStream流:写出字节
      int write(byte b[]):按照指定的字节数组写入
      flush:刷新流
      close:关闭流
    */
    public static void main(String[] args) {
        FileOutputStream outputStream = null;
        try {
            //1.创建输出流对象
            //第二个参数是个bool值,如果为true,写入时在原文件后面追加
            //为false,写入时替换原文件内容
            outputStream = new FileOutputStream("b.txt",true);
            //2.写入数据
            outputStream.write(new byte[]{'a','b','c'});
            //3.刷新流,不刷新的话可能修改会失败
            outputStream.flush();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            try {
                //4.关闭流
                outputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
