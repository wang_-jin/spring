package com.igeek.ch09.io.lesson_01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Tool3 {
    /*
    FileReader:读入字符
      public FileReader(String fileName)
      public int read(char cbuff[])
      close:关闭流
      使用场景:
        字符流更适合读入文本文件,效率比字节流FileInputStream高,
        字符流在处理文本数据时,可以直接操作字符,而字节流需要进行
        字符编码和解码的过程!
    */
    public static void main(String[] args) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader("c.txt");
            char[] chars = new char[1024];
            int len = 0;
            while ((len=fileReader.read(chars))!=-1){
                System.out.println(new String(chars,0,len));
            }


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
