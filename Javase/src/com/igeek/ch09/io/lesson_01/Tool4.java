package com.igeek.ch09.io.lesson_01;

import java.io.FileWriter;
import java.io.IOException;

public class Tool4 {
    /*
    FileWriter:写出字符
      write(String str)
      flush:刷新流
      close:关闭流
      FileReader更适合读写出文本文件,效率比
      FileOutputStream高
    */
    public static void main(String[] args) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("d.txt",true);
            fileWriter.write("hello");
            fileWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
