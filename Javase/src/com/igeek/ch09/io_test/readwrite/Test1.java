package com.igeek.ch09.io_test.readwrite;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/8/31 21:29
 * @description TODO
 */
public class Test1 {
    /*FileOutputStream:写出字节*/
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("1.txt",true);
            fos.write(new byte[]{'2','1','3'});
            fos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
