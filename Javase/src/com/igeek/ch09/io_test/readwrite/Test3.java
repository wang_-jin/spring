package com.igeek.ch09.io_test.readwrite;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/8/31 21:37
 * @description TODO
 */
public class Test3 {
    /*FileWriter:写出字符*/
    public static void main(String[] args) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("1.txt",true);
            fileWriter.write("cccc");
            fileWriter.flush();
            System.out.println("写入成功");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
