package com.igeek.ch09.io_test.readwrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/8/31 21:16
 * @description TODO
 */
public class Test {
    /*FileInputStream:读入字节*/
    public static void main(String[] args) {
        FileInputStream fis = null;
        byte[] bytes = new byte[1024];
        int length = 0;
        try {
            fis = new FileInputStream("2.txt");
            while ((length=fis.read(bytes))!=-1){
                System.out.println(new String(bytes,0,length));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //关闭资源
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
