package com.igeek.ch09.io_test.readwrite;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/8/31 21:36
 * @description TODO
 */
public class Test2 {
    /*FileReader:读入字符*/
    public static void main(String[] args) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader("2.txt");
            char[] cs = new char[1024];
            int len = 0;
            while((len=fileReader.read(cs))!=-1){
                System.out.println(new String(cs,0,len));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
