package com.igeek.ch09.io_test.commonsio_test;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/9/4 0:20
 * @description TODO
 */
public class Test2 {
    public static void main(String[] args) {
   /*3.已知1.txt文件中有这样的一个字符串:hcexfgijkamdnoqrzstuvwybpl
    请编写程序读取数据内容,把数据排序后写入2.txt中*/
        try(BufferedReader br = IOUtils.buffer(new FileReader("3.txt"));
            BufferedWriter bw = IOUtils.buffer(new FileWriter("5.txt",true))){
            Set<String>set = new TreeSet<>();
            String str = br.readLine();
            Pattern ptn = Pattern.compile("[a-z]");
            Matcher matcher = ptn.matcher(str);
            while(matcher.find()){
                String s = matcher.group();
                set.add(s);
            }
            set.forEach(a->{
                try {
                    bw.write(a);
                    bw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
