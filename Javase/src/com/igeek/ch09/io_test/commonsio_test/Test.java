package com.igeek.ch09.io_test.commonsio_test;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wangjin
 * 2023/9/1 18:03
 * @description TODO
 */
public class Test {
    /*1.编写一个Java程序,读取一个文本文件,并统计其中每个字母出现的次数，
    然后将结果输出到另一个文件中,假设文本内容为:Hello, world! This is a sample text.
    输出格式如下:
    h=1
    e=1
    ......*/
    public static void main(String[] args) {
        try(
                BufferedReader br = new BufferedReader(new FileReader("1.txt"));
                BufferedWriter bw = new BufferedWriter(new FileWriter("4.txt",true))
                ){
            String s = br.readLine();
            Pattern ptn = Pattern.compile("[a-zA-Z]");
            Matcher matcher = ptn.matcher(s);
            Map<String,Integer> map = new HashMap<>();
            while(matcher.find()){
                String s1 = matcher.group();
                int i = map.getOrDefault(s1, 0) + 1;
                map.put(s1,i);
            }
            map.forEach((a,b)->{
                try {
                    bw.write(a+"="+b);
                    bw.newLine();
                    bw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            System.out.println(map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/**/
