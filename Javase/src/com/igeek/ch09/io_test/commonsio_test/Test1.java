package com.igeek.ch09.io_test.commonsio_test;

import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * @author wangjin
 * 2023/9/1 16:33
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        try( BufferedInputStream bir = IOUtils.buffer(new FileInputStream("D:\\imgs1\\1.jpg"));
             BufferedOutputStream bos = IOUtils.buffer(new FileOutputStream("D:\\imgs2\\2.jpg"))) {

            IOUtils.copy(bir,bos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
