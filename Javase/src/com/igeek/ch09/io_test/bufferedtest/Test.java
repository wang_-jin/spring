package com.igeek.ch09.io_test.bufferedtest;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/9/1 11:42
 * @description TODO
 */
public class Test {
    /*BufferedInputStream: 带缓存的读入字节流 byte
      BufferedOutputStream: 带缓存的写进字节流 byte
      BufferedReader: 带缓存的读入字符流
      BufferedWriter: 带缓存的写进字符流*/
    public static void main(String[] args) {
        try(BufferedInputStream bis = new BufferedInputStream(new FileInputStream("1.txt"));) {
            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len=bis.read(bytes))!=-1){
                System.out.println(new String(bytes,0,len));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
