package com.igeek.ch09.io_test.bufferedtest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/9/1 19:40
 * @description TODO
 */
public class Test2 {
    public static void main(String[] args) {
        try(BufferedReader br = new BufferedReader(new FileReader("1.txt"));){
            String str = "";
            while((str = br.readLine())!=null){
                System.out.println(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
