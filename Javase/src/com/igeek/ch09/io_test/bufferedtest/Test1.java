package com.igeek.ch09.io_test.bufferedtest;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/9/1 19:36
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("2.txt",true));){
            bos.write(new byte[]{'2','1','1'});
            bos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
