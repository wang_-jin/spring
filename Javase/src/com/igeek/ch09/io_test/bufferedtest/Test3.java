package com.igeek.ch09.io_test.bufferedtest;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author wangjin
 * 2023/9/1 19:47
 * @description TODO
 */
public class Test3 {
    public static void main(String[] args) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("2.txt",true))){
            bw.newLine();
            bw.write("在下令狐冲");
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
