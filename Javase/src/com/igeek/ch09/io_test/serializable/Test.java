package com.igeek.ch09.io_test.serializable;

import java.io.*;

/**
 * @author wangjin
 * 2023/9/3 22:46
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //对象序列化
/*        try(
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person1.txt"))
        ){
                oos.writeObject(new Person("刘备",18));
                oos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        //对象反序列化
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("person1.txt"))){
            Person p = (Person)  ois.readObject();
            System.out.println(p.age);
            System.out.println(p.name);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
