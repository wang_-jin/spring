package com.igeek.ch09.io_test.serializable;

import java.io.*;

/**
 * @author wangjin
 * 2023/9/3 23:22
 * @description TODO
 */
public class Test2 {
    static void write(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person3.txt"))){
            oos.writeObject(new Person1("张飞",20));
            System.out.println("创建成功!");
            oos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void read(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("person3.txt"))){
            Person1 p = (Person1) ois.readObject();
            System.out.println(p);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //write();
        read();
    }
}
