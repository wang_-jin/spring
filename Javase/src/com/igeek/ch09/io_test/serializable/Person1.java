package com.igeek.ch09.io_test.serializable;

import java.io.Serializable;

/**
 * @author wangjin
 * 2023/9/3 22:54
 * @description TODO
 */
public class Person1 implements Serializable {
    /**/
    private static final long serialVersionUID = 1L;
    String name;
    int age;
    String like;

    public Person1() {
    }


    public Person1(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
}
