package com.igeek.ch09.io_test.serializable;

import java.io.Serializable;

/**
 * @author wangjin
 * 2023/9/3 22:31
 * @description TODO
 */
public class Person implements Serializable {
    /*对象序列化是要实现Serializable接口,否则序列化时会报错*/
    String name;
    int age;
    /*transient 修饰的属性序列化时不会被存到本地,序列化的是属性的默认值*/
    transient int password;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, int password) {
        this.name = name;
        this.age = age;
        this.password = password;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
}
