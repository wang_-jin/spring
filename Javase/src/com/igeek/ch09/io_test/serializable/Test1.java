package com.igeek.ch09.io_test.serializable;

import java.io.*;

/**
 * @author wangjin
 * 2023/9/3 22:54
 * @description TODO
 */
public class Test1 {
    static void write(){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person2.txt"))){
            //transient 修饰的属性序列化时不会被存到本地,序列化的是属性的默认值
            oos.writeObject(new Person("关羽",18,123456));
            System.out.println("创建成功!");
            oos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void read(){
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("person2.txt"))){
            Person p = (Person) ois.readObject();
            System.out.println("姓名:"+p.name);
            System.out.println("密码:"+p.password);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        //write();
        read();
    }
}
