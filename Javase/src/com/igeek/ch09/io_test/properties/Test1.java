package com.igeek.ch09.io_test.properties;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * @author wangjin
 * 2023/9/1 20:59
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        try(
                FileWriter fr = new FileWriter("user.properties",true);
        ){
            Properties pr = new Properties();
            //写出数据
            pr.setProperty("name","刘备");
            pr.setProperty("age","18");
            //存储属性列表,第二个参数是注释
            pr.store(fr,"");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
