package com.igeek.ch09.io_test.properties;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author wangjin
 * 2023/9/1 20:33
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        try(
                FileReader fr = new FileReader("user.properties");
                ){
            Properties pr = new Properties();
            pr.load(fr);
            //获取属性名对应的属性值
            System.out.println("name="+pr.getProperty("name"));
            System.out.println("age="+pr.getProperty("age"));
            //获取所有属性名
            Set<String> set = new TreeSet<>(pr.stringPropertyNames());
            set.forEach(System.out::println);

            //获取所有的属性值
            Collection<Object> values = pr.values();
            System.out.println(values);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
