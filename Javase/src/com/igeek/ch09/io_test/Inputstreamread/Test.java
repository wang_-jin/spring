package com.igeek.ch09.io_test.Inputstreamread;

import java.io.*;

/**
 * @author wangjin
 * 2023/9/1 19:54
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        try(    //读入字节流
                FileInputStream fis = new FileInputStream("1.txt");
                //转换字符流
                InputStreamReader isr = new InputStreamReader(fis);
                //带入缓存的字符流
                BufferedReader br = new BufferedReader(isr);
                ){
                String str = "";
                while ((str= br.readLine())!=null){
                    System.out.println(str);
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
