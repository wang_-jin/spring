package com.igeek.ch09.io_test.Inputstreamread;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/1 20:02
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        try(
                //写出字节流
                FileOutputStream fos = new FileOutputStream("2.txt",true);
                //转换字符流
                OutputStreamWriter osw = new OutputStreamWriter(fos);
                //带缓存写出字符流
                BufferedWriter bw = new BufferedWriter(osw);
                ){

            List<String>list = new ArrayList<>();
            list.add("窗前明月光");
            list.add("疑是地上霜");
            list.add("举头望明月");
            list.add("低头思故乡");
            list.forEach(s->
            {
                try {
                    bw.newLine();
                    bw.write(s);
                    bw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
