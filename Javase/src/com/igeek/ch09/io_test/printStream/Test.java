package com.igeek.ch09.io_test.printStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author wangjin
 * 2023/9/1 20:10
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        /*PrintStream打印字节流会替换原有的内容,不能拼接*/
        try(
                PrintStream ps = new PrintStream(new FileOutputStream("3.txt"));
                ){
/*                ps.println("hello");
                ps.println("world");
                ps.flush();*/
            System.setOut(ps);
            System.out.println("nihao");
            System.out.println("wohao");
            ps.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
