package com.igeek.ch09.io_test.printStream;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author wangjin
 * 2023/9/1 20:18
 * @description TODO
 */
public class Test1 {
    public static void main(String[] args) {
        //PrintWriter
        try(
                PrintWriter pw = new PrintWriter("3.txt")
                ){
            pw.println("你好");
            pw.println("世界");
            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
