package com.igeek.day04;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 15:35
 * @description TODO
 */
public class Array {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入第一个数:");
        int a = sc.nextInt();
        System.out.println("请输入第二个数:");
        int b = sc.nextInt();
        System.out.println("请输入第三个数:");
        int c = sc.nextInt();
        System.out.println("请输入第四个数:");
        int d = sc.nextInt();
        System.out.println("请输入第五个数:");
        int e = sc.nextInt();
        int[] arr = new int[]{a,b,c,d,e};
        int temp = 0;
        //冒泡排序从左至右由小到大排序
        for(int i=0;i< arr.length;i++){
           for (int j=0;j< arr.length-1-i;j++){
               if (arr[j]>arr[j+1]){
                   temp = arr[j];
                   arr[j]=arr[j+1];
                   arr[j+1]=temp;
               }
           }
        }
        System.out.println("最大值为:"+arr[arr.length-1]);//最后一个值最大
    }
}
