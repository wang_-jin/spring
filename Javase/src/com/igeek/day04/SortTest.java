package com.igeek.day04;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/14 13:38
 * @description TODO
 */
public class SortTest {
    public static void main(String[] args) {
        //冒泡排序
        int[] arr = {1,6,3,2,4};
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j]>arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));

        System.out.println("-------------------");
        //折半查找
        int a = 5;
        int leftIndex = 0;
        int rightIndex = arr.length-1;
        boolean isExist = false;
        while(leftIndex<rightIndex){
            int midIndex = (leftIndex+rightIndex)/2;
            if (a<arr[midIndex]){
                rightIndex = rightIndex-1;
            }else if (a>arr[midIndex]){
                leftIndex = leftIndex+1;
            }else if (a==arr[midIndex]){
                isExist = true;
                break;
            }
        }
        if (isExist){
            System.out.println("找到了！");
        }else {
            System.out.println("没找到！");
        }
    }
}
