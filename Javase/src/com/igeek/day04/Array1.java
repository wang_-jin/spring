package com.igeek.day04;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 16:09
 * @description TODO
 */
public class Array1 {
    public static void main(String[] args) {
        int[] arr = new int[]{8,4,2,1,23,344,12};
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入:");
        int a = sc.nextInt();
        for (int item : arr) {
            if (a==item){
                System.out.println("数组包含此数！");
                System.exit(0);
            }
        }
        System.out.println("数组没有此数！");
    }
}
