package com.igeek.day04;

/**
 * @author wangjin
 * 2023/8/14 10:01
 * @description TODO
 * 练习2:二维数组3X3,求其对角线元素之和
 */
public class Array4 {
    public static void main(String[] args) {
        int[][] arr = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i==j){
                    sum+=arr[i][j];
                }
                if (i+j==2){
                    sum+=arr[i][j];
                }
            }
        }
        System.out.println("sum:"+sum);
    }
}
