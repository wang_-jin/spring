package com.igeek.day04;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/11 16:27
 * @description TODO
 */
public class Array2 {
    public static void main(String[] args) {
        int[] arr = {5,11,15,24,36,47,59,66};
        for(int i=0;i< (arr.length)/2;i++){
            int temp = arr[i];
            arr[i]=arr[arr.length-1-i];
            arr[arr.length-1-i]=temp;

        }
        System.out.println(Arrays.toString(arr));
    }
}
