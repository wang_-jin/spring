package com.igeek.day04;

/**
 * @author wangjin
 * 2023/8/11 18:02
 * @description TODO
 */
public class Test05 {

    /*public static void main(String[] args) {
        //字面量赋值且temp1和temp2改变arr1也会变
        int[] arr1 = {1,2,3,4,5};
        int[] temp1 = arr1;
        int[] temp2 = arr1;
        System.out.println("通过temp1取出数组值: ");

        for(int i = 0;i<temp1.length;i++) {
            System.out.print(temp1[i]+" ");//1 2 3 4 5
        }
        System.out.println();
        System.out.println("通过temp2取出数组值: ");
        for(int i = 0;i<temp2.length;i++) {
            System.out.print(temp2[i]+" ");//1 2 3 4 5
        }
        System.out.println();
        temp1[2] = 9;//1 2 9 4 5

        System.out.println("通过arr1取出数组值: ");
        for(int i = 0;i<arr1.length;i++) {
            System.out.print(arr1[i]+" ");//1 2 9 4 5
        }
        System.out.println();
        System.out.println("通过temp1取出数组值: ");
        for(int i = 0;i<temp1.length;i++) {
            System.out.print(temp1[i]+" ");//1 2 9 4 5
        }


        System.out.println();
        System.out.println("通过temp2取出数组值: ");
        for(int i = 0;i<temp2.length;i++) {
            System.out.print(temp2[i]+" ");//1 2 9 4 5
        }
        System.out.println();

    }*/



 public static void main(String[] args) {
			int[] arr1 = {1,2,3,4,5};
			int[] arr2 = {5,6,7};
			int[] temp = arr1;

			System.out.println("通过temp取出arr1中的元素: ");

			for(int i = 0;i<temp.length;i++) {
				System.out.print(temp[i]+" ");//1 2 3 4 5
			}

			temp = arr2;
            System.out.println();
			System.out.println("通过temp取出arr2中的元素: ");

			for(int i = 0;i<temp.length;i++) {
				System.out.print(temp[i]+" ");//5 6 7
			}

		}

}
