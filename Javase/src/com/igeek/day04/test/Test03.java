package com.igeek.day04;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/11 17:59
 * @description TODO
 * 已知数组int[] nums = {5,10,15},,要求创建一个新数组
 * 1.新数组的长度和已知数组相同
 * 2.新数组每个元素的值 是已知数组对应位置元素的2倍
 * 3.在控制台中打印新数组的所有元素
 * 1.定义题目要求的已知数组
 * 2.定义题目要求的新数组
 * 3.根据条件为新数组中每个元素赋值
 * 4.遍历新数组打印到控制台中
 */
public class Test03 {
    public static void main(String[] args) {
        int[] arr1 = new int[]{5,10,15};
        System.out.println(Arrays.toString(arr1));
        int[] arr2 = new int[arr1.length];
        for (int i=0;i<arr1.length;i++) {
            arr2[i] = arr1[i]*2;
        }
        System.out.println(Arrays.toString(arr2));
    }
}
