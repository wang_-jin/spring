package com.igeek.day04;

import java.util.Arrays;
import java.util.Random;

/**
 * @author wangjin
 * 2023/8/11 18:02
 * @description TODO
 * 将数字1-10保存到一个长度为10的一维数组中
 * 定义一个新数组,长度为3,取出原来数组中随机三个元素(不考虑是否重复)
 * 给新数组的元素赋值
 * 求新数组所有元素的和
 */
public class Test06 {
    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5,6,7,8,9,10};
        int[] arr2 = new int[3];
        Random random = new Random();
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            int randomIndex = random.nextInt(arr1.length);//随机获取arr1的索引
            int randomNumber = arr1[randomIndex];//获取arr1索引对应的值
            arr2[i] = randomNumber;//将值赋给arr2
        }
        System.out.println(Arrays.toString(arr2));
        for (int j : arr2) {
            sum += j;//arr2所有数组值之和
        }
        System.out.println("sum="+sum);

    }
}
