package com.igeek.day04;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 17:59
 * @description TODO
 * 定义一个含有五个元素的数组,并为每个元素赋值,求数组中所有元素的最小值
 * 1.定义5个元素数组
 * 2.可以使用初始化数组的两种方式之一为数组元素赋值
 * 3.遍历数组求数组中的最小值
 */
public class Test01 {
    public static void main(String[] args) {
        int[] arr = new int[5];
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {
            System.out.println("第"+(i+1)+"个值为:");
            arr[i] = sc.nextInt();
        }
        int min = arr[0];
        for (int j = 0; j < arr.length-1; j++) {
            if (min>arr[j]){
                min = arr[j];
            }
        }
        System.out.println("min="+min);
    }
}
