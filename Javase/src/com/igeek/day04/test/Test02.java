package com.igeek.day04;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/8/11 17:59
 * @description TODO
 * 定义一个长度为3的一维数组,给每个元素赋值. (要求数组中每个元素的值是0-9的随机数)
 * 遍历数组打印每个元素的值
 * 1.定义长度为3的数组
 * 2.创建Random引用数据类型的变量
 * 3.生成3个0-9的随机数,为每一个元素赋值(可以用循环,也可以不使用循环)
 * 4.遍历数组，并打印每一个元素
 */
public class Test02 {
    public static void main(String[] args) {
        int[] arr = new int[3];
        for (int i = 0; i < 3; i++) {
            arr[i] = (int)(Math.random()*10);
        }
        System.out.println(Arrays.toString(arr));
    }
}
