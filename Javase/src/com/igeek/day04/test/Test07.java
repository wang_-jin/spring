package com.igeek.day04;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 18:02
 * @description TODO
 * 分析以下需求，并用代码实现
 * 	1.键盘录入班级人数
 * 	2.根据录入的班级人数创建数组
 * 	3.利用随机数产生0-100的成绩(包含0和100)
 * 	4.要求:
 * 		(1)打印该班级的不及格人数
 * 		(2)打印该班级的平均分
 * 		(3)演示格式如下:
 * 			请输入班级人数:
 * 			键盘录入:100
 * 			控制台输出:
 * 				不及格人数:19
 * 				班级平均分:87
 */
public class Test07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("班级人数：");
        int num = scanner.nextInt();
        int[] arr = new int[num];
        int notSum = 0;
        int allSum = 0;
        for (int i = 0; i < num; i++) {
            Random random = new Random();
            arr[i] = random.nextInt(101);
            if(arr[i]<60){
                notSum++;
            }
            allSum+= arr[i];
        }
        int aver = allSum/num;
        System.out.println(Arrays.toString(arr));
        System.out.println("不及格人数:"+notSum);
        System.out.println("班级平均分:"+aver);

    }
}
