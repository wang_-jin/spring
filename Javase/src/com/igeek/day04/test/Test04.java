package com.igeek.day04;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 18:00
 * @description TODO
 * 键盘录入一个整数，创建一个该长度的数组，
 * 为每一个元素赋值为1-10的随机整数，最后打印数组中所有值大于5且为偶数的元素.
 * 键盘录入一个整数
 * 2定义长度为该整数的数组
 * 3创建Random引用数据类型的变量
 * 4生成5个0-9的随机数,为每一个元素赋值(建议用循环)
 * 5 遍历数组,输出满足条件的元素
 */
public class Test04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个整数:");
        int a = sc.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            arr[i] = (int)(Math.random()*10);
        }
        System.out.println(Arrays.toString(arr));
        for (int i:arr) {
            if(i>5 && i%2==0){
                System.out.print(i+" ");
            }
        }
    }
}
