package com.igeek.day04;

import java.util.Random;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/11 18:03
 * @description TODO
 * 分析以下需求，并用代码实现
 * 	1.基础班考试要进行分组,键盘录入该班级总共多少组?以及每组的学生数量?
 * 	2.根据录入的组数及每组学员的数量创建二维数组
 * 	3.利用随机数产生0-100的成绩(包含0和100)
 * 	4.要求:
 * 		(1)打印该班级中每组的不及格人数
 * 		(2)打印该班级中每组的平均分
 * 		(3)打印组的最高平均分
 * 		(4)打印班级中的不及格人数
 * 		(5)打印班级平均分
 * 		(6)演示格式如下:
 * 			请输入班级总组数:3
 * 			请输入班级中每组的人数:10
 * 			控制台输出:
 * 				第1组不及格人数为: 6 人
 * 				第1组平均分为: 52
 * 				第2组不及格人数为: 7 人
 * 				第2组平均分为: 46
 * 				第3组不及格人数为: 3 人
 * 				第3组平均分为: 69
 * 				班级中单组最高平均分为:69
 * 				班级中不及格人数为: 16 人
 * 				班级总平均分为: 56
 */
public class Test08 {
    public static void main(String[] args) {
        //键盘录入该班级总共多少组?以及每组的学生数量?
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入班级总组数:");
        int groupNum = sc.nextInt();
        System.out.println("请输入班级中每组的人数:");
        int stuNum = sc.nextInt();
        int[][] arr = new int[groupNum][stuNum];
        int maxAver = 0;
        int sum = 0;
        int sumAver = 0;
        for (int i = 0; i < groupNum; i++) {
            int notSum = 0;
            int sumScore = 0;
            for (int j = 0; j < stuNum; j++) {
                Random random = new Random();
                arr[i][j] = random.nextInt(101);//随机获取每个学生的分数
                if(arr[i][j]<60){
                    notSum++;//统计每组不及格人数
                }
                sumScore+=arr[i][j];//统计每组总分
                System.out.print(arr[i][j]+" ");//查看每组学生分数

            }
            sum+=notSum;
            int aver = sumScore/stuNum;//计算每组平均分
            sumAver+=aver;
            if(maxAver<aver){
                maxAver = aver;
            }
            System.out.println();
            System.out.println("第"+(i+1)+"组"+"不及格人数为:"+notSum+"人");
            System.out.println("第"+(i+1)+"组"+"平均分为:"+aver);
        }
        System.out.println("班级中单组最高平均分为:"+maxAver);
        System.out.println("班级中不及格人数为:"+sum+"人");
        System.out.println("班级总平均分为:"+sumAver/groupNum);

    }
}
