package com.igeek.day04.array;

public class ArrayTest5 {
    public static void main(String[] args) {
        /*
        Arrays.copyOf:
          拷贝一个数组,copyOf(要拷贝的数组,新数组长度),
          拷贝的新数组内容和原数组一模一样,但是拷贝会新开辟
          一个内存空间去存储新的数组,因此原数组和新数组虽然
          内容相同,但是地址不同!

        ctrl+d可以复制当前行代码到下一行,d是duplicate赋值的简写
        */
        /*
        int[] arr = {1, 3, 5, 7, 9};
        //1.拷贝
        int[] arr1 = Arrays.copyOf(arr, arr.length);
        System.out.println("arr=" + Arrays.toString(arr));
        System.out.println("arr1=" + Arrays.toString(arr1));
        System.out.println("arr地址信息:" + Arrays.toString(arr));
        System.out.println("arr1地址信息:" + Arrays.toString(arr1));
        *//*
        Arrays.equals比较的是两个数组中所有元素的内容是否相同,
        如果所有元素都相同返回true,否则返回false.
        *//*
        System.out.println("equals:" + Arrays.equals(arr, arr1));
        *//*
        ==比较引用类型数据时,比较的是两个数据的地址是否相同,如果相同
        返回true,否则返回false.
        *//*
        System.out.println("==:" + (arr == arr1));
        System.out.println("--------------------------");
        //2.赋值
        int[] arr2 = arr;//赋值传的是地址
        System.out.println("arr=" + Arrays.toString(arr));
        System.out.println("arr2=" + Arrays.toString(arr2));
        System.out.println("arr地址信息:" + arr);
        System.out.println("arr2地址信息:" + arr2);
        System.out.println("equals:" + Arrays.equals(arr, arr2));
        System.out.println("==:" + (arr == arr2));
        */


        /*
        数组扩容:
          数组扩容是指在数组中添加更多元素时,需要增加数组的容量.
          当数组已经无法容纳新的元素时,需要对数组进行扩容操作.
          数组扩容主要使用的是Array.copyOf方法.
          注意:
            数组扩容一般是将数组长度增加原来的一半加1!

        为什么扩容使用"扩大一半加1的策略"?
        在 Java 中,当需要对数组进行扩容时,通常会采用 "扩大一半加一" 的策略.
        这个策略的目的是在动态调整数组大小时,平衡内存分配和性能.
        这种策略的原因有两个主要方面:
        1.减少频繁的扩容操作:
          当使用一个固定大小的数组时,如果每次都只增加一个固定的长度,比如加1,
          那么当数组需要频繁扩容时,会导致多次的内存分配和数据复制操作,
          这会对性能产生负面影响.而将数组大小扩大一半加一,可以减少频繁的扩容操作,
          从而提高性能;
        2.控制内存浪费:如果每次数组扩容时都按固定长度增加,可能会导致过多的内存浪费.
          因为在某些情况下,数组实际存储的元素数量可能远远小于数组的容量.
          通过按一定比例扩容,可以在一定程度上控制内存的使用,避免过多地浪费内存空间.
        综上所述,通过"扩大一半加一"的策略,可以在动态调整数组大小时,
        在性能和内存利用之间取得一个平衡.这种策略在Java中的集合类如ArrayList
        中被广泛采用,从而提供高效的动态数组实现.
        */
        /*
        int[] arr = {1,3,5,7,9};
        int[] arr1 = Arrays.copyOf(arr,arr.length*3/2+1);
        System.out.println(Arrays.toString(arr1));
        */


    }
}
