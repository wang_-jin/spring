package com.igeek.day04.array;

public class SortTest3 {
    public static void main(String[] args) {
        /*
        冒泡排序:
        冒泡排序是一种简单的排序算法,它的思想就像水中的气泡一样,
        较小的元素会慢慢“冒泡”到数组的前面,较大的元素则会被“压缩”到数组的后面.
        具体地说,冒泡排序会重复地遍历要排序的数组,比较相邻的两个元素,
        如果它们的顺序错误就交换它们的位置,直到遍历完整个数组.
        这个过程会重复多次,每次遍历都会将一个最大的元素放到数组的末尾,
        因此称为"冒泡".

        假设数组为arr;
        数组长度为arr.length=5;
        5 2 1 9 8        第i轮       每轮arr.length-i-1次
        2 1 5 8 9:         0                4
        1 2 5 8:9:         1                3
        1 2 5:8:9:         2                2
        1 2:5:8:9:         3                1
                       共arr.length-1轮
        */

        /*
        int[] arr = {5,2,1,9,8};
        //第i轮
        for (int i = 0; i < arr.length-1; i++) {
            //每轮arr.length-i-1次
            for (int j = 0; j<arr.length-i-1; j++) {
              if(arr[j]>arr[j+1]){
                  //交换变量
                  *//*int temp = arr[j];
                  arr[j] = arr[j+1];
                  arr[j+1] = temp;*//*
              }
            }
        }
        System.out.println(Arrays.toString(arr));
        */

        /*
        折半查找法:
          折半查找法(Binary Search)也被称为二分查找法,
          是一种在有序数组中查找特定元素的高效算法.
          它的基本思想是,先将数组按照升序或降序排列,然后用数组
          中间的元素与目标值进行比较,如果相等则返回,如果中间元素大于目标值,
          则在左侧部分继续进行折半查找,否则在右侧部分继续进行折半查找,
          直到找到目标值为止.
        */

        /*
        例子:键盘上输入一个数字,从数组{1,2,4,8,12,23,344}中查找
        该数字看是否存在!
        */
        /*
        Scanner sc = new Scanner(System.in);
        int[] arr = {1,2,4,8,12,23,344};
        System.out.println("请输入一个数字x:");
        int x = sc.nextInt();
        //左边索引
        int left_index = 0;
        //右边索引
        int right_index = arr.length-1;
        //数组中是否存在输入的数字
        boolean isExist = false;
        //左边索引小于等于右边索引循环继续,否则循环结束
        while (left_index<=right_index){
          //中间索引
          int mid_index = (left_index+right_index)/2;
          if(x<arr[mid_index]){
              right_index = mid_index-1;
          }else if(x>arr[mid_index]){
              left_index = mid_index+1;
          }else if(x==arr[mid_index]){
              isExist = true;
              break;
          }
        }
        if(isExist){
            System.out.println("数组中存在数字x");
        }else{
            System.out.println("数组中不存在数字x");
        }
        */
    }
}
