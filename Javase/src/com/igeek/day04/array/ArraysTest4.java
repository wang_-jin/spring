package com.igeek.day04.array;

public class ArraysTest4 {
    public static void main(String[] args) {
        /*
        Arrays.binarySearch:
          二分查找法寻找某个元素,返回值是一个int类型的数字,如果没找到返回值
          是负值,如果找到了返回值是元素在数组中的索引
          注意:
            使用前必须先对数组进行排序,否则返回结果是不确定的
        */
        /*
        int[] arr = {12,32,9,45,33,5};
        //排序:默认是升序排序
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        *//*
        使用binarySearch查找arr中是否存在12,如果存在
        i就是12在arr中的索引值,否则i是个负数
        *//*
        int i = Arrays.binarySearch(arr,12);
        System.out.println(i);
        *//*
        注意:
          选中方法名,然后按ctrl+p可以查看该方法可以传递的参数
          @NotNull int[] a,@Range int fromIndex,@Range int toIndex,long key
          上面参数的意思:
            首先能看到有4个参数:
              第一个参数是一个非null的int数组;
              第二个参数fromIndex表示开始的索引,在java中fromIndex
              一般是可以取到这个索引值的;
              第三个参数toIndex表示结束的索引值,在java中toIndex
              一般都是取不到这个索引值的;
              第四个参数是要搜索的长整型数字
        *//*
        int j = Arrays.binarySearch(arr,2,5,5);
        System.out.println(j);
        */

        /*
        Arrays.fill:
          数组填充
        */
        /*
        int[] arr = new int[5];
        //将8填充到数组
        Arrays.fill(arr,8);
        //[8,8,8,8,8]
        System.out.println(Arrays.toString(arr));
        *//*
        从索引1开始填充到索引2,注意这里toIndex仍然取不到索引3
        *//*
        Arrays.fill(arr,1,3,6);
        System.out.println(Arrays.toString(arr));
        */

        /*
        Arrays.toString:将数组转为字符串
        Arrays.deepToString:将多维数组转为字符串
        */
        /*
        int[][] arr = {{1,2,3},{4,5,6}};
        String s = Arrays.deepToString(arr);
        System.out.println(s);
        */


    }
}
