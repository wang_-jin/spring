package com.igeek.day04.array;

public class TwoArrayTest2 {
    public static void main(String[] args) {
      /*
      二维数组的概念:
        二维数组也是一个数组,它的元素是一维数组.

      二维数组的定义:
        数据类型[][] 数组名 = new 数据类型[m][n];
        如: int[][] arr = new int[2][],表示定义了
        一个二维数组arr,arr里面有2个一维数组元素,这两个
        一维数组默认是空值null;

        也可以使用字面量形式定义二维数组(不常用):
          int[][] arr = {{1,2},{3,4,5},{6,7,8,9}}
      */
        /*int[][] arr = new int[2][];
        //第一个维度赋值:给一维数组赋值
        arr[0] = new int[2];
        arr[1] = new int[2];
        //第二个维度赋值:给一维数组中元素赋值
        arr[0][0] = 1;
        arr[0][1] = 2;
        arr[1][0] = 3;
        arr[1][1] = 4;
        //循环获取二维数组的所有元素值
        for (int i = 0; i < arr.length; i++) {
            //获取所有的一维数组
            int[] arr1 = arr[i];
            for (int j = 0; j < arr1.length; j++) {
                //获取一维数组中元素的值
                int item = arr1[j];
                System.out.print(item+"\t");
            }
            System.out.println();
        }*/

        //例1:打印下面二维数组中的所有元素
        /*int[][] arr = {{1,2},{3,4,5},{6,7,8,9}};
        //写法1
        for (int i = 0; i < arr.length; i++) {
            //获取一维数组
            int[] arr1 = arr[i];
            for (int j = 0; j < arr1.length; j++) {
                //获取一维数组中的元素
                int item = arr1[j];
                System.out.print(item+"\t");
            }
            System.out.println();
        }
        //写法2
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                //数组中的元素
                int item = arr[i][j];
                System.out.print(item+"\t");
            }
            System.out.println();
        }*/


        /*
        练习1:用二维字符数组保存一首唐诗(五言或七言),
        每一行代表一句,要求将这首唐诗按照古文的方式
        输出出来(从右至左,竖行排列)
        如果从左往右竖行排列又该如何写?
        低  举  疑  床
        30  20  10 00
        头  头  是  前
        31  21  11 01
        思  望  地  明
        32  22  12 02
        故  明  上  月
        33  23  13 03
        乡  月  霜  光
        34  24  14 04
        */
        /*char[][] cs = {
                {'床','前','明','月','光'},
                {'疑','是','地','上','霜'},
                {'举','头','望','明','月'},
                {'低','头','思','故','乡'},
        };
        for (int i = 0; i < cs.length; i++) {
            for (int j = 0; j < cs[i].length; j++) {
                //数组中元素
                char item = cs[i][j];
                System.out.print(item+"\t");
            }
            System.out.println();
        }
        System.out.println("--------------------");
        for (int i = 0; i <5; i++) {
            for (int j = cs.length-1; j >=0; j--) {
              char item = cs[j][i];
              System.out.print(item+"\t");
            }
            System.out.println();
        }*/

        /*
         //练习2:二维数组3X3,求其对角线元素之和
         int[][] arr = new int[3][];
        */
        /*int[][] arr = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length; j++) {
                //主对角线之和
                if(i==j){
                    sum1+=arr[i][j];
                }
                //副对角线之和
                if(i+j==2){
                    sum2+=arr[i][j];
                }
            }
        }
        System.out.println("对角线之和:"+(sum1+sum2));*/
    }
}
