package com.igeek.day04.array;

public class OneArrayTest1 {
    public static void main(String[] args) {
        /*
         * 一维数组的概念:
         *   数组是一种用于存储相同类型数据的容器,
         *   并且数组的长度是固定的!
         *
         *   数组是引用数据类型,数组中可以存储基本类型
         *   和引用类型数据.
         *
         *
         * 数组的定义:
         *   方式1:使用new
         *     数组中元素类型[] 数组名 = new [数组长度](推荐使用这种方式)
         *     数组中元素类型 数组名[] = new [数组长度]
         *     如:int[] arr = new [3];
         *   方式2:字面量形式
         *     数组中元素类型[] 数组名 = {元素1,元素2,......};
         *     如: int arr = {1,2,3,4,5};
         *   方式3:new+字面量
         *     数组中元素类型[] 数组名 = new int[]{元素1,元素2,......};
         *
         * 获取数组中的元素:
         *   数组名[元素索引],如arr[0]就是获取数组中第一个元素
         * 获取数组长度:
         *   数组名.length,如arr.length
         * 注意:
         *   数组索引的最大值:是数组长度-1,获取数组元素值时
         *   索引不能超过最大值,否则会报数组越界的错误
         * */

        /*
        方式1定义数组:
          左边：
		  		int:说明数组中的元素的数据类型是int类型
		  		[]:说明这是一个数组
		  		arr:是数组的名称
		  右边：
		  		new:为数组分配内存空间
		  		int:说明数组中的元素的数据类型是int类型
		  		[]:说明这是一个数组
		  		3:数组的长度,其实就是数组中的元素个数
        */
        /*
        int[] arr = new int[3];
        int a0 = arr[0];
        int a1 = arr[1];
        int a2 = arr[2];
        //数组越界
        //int a3 = arr[3];
        */

        /*
        方式2定义数组:
          使用字面量形式定义数组
          字面量:
            字面量是一个不可变的常量值,它的特点是
            所见即所得,在编辑器中显示的值就是程序运行后
            使用的值!如:123,"a",true,3.4...这些都是字面量
        */
        /*
        int[] arr = {1,2,3,4,5,6};
        */
        /*
        注意:
          打印数组变量会发现数组没有被打印出来,而是打印出了
          这样一串东西:[I@4eec7777,这是咋回事呢?
          这是因为System.out.print打印的原理是将数据转为
          字符串打印出来.
          对于基本数据类型,Java会调用相应的toString()方法将其转换为字符串;
          对于引用数据类型,Java会调用其toString()方法将其转换为字符串,如果
          该类没有重写toString方法,则会调用Object.toString方法将对象转为
          字符串输出,Object.toString函数体源码是这样的:
            getClass().getName()+"@"+Integer.toHexString(hashCode());
            getClass():获取对象在运行时所属的类
            getName():获取类名
            hashCode():获取哈希码,哈希码主要是用来确定对象在哈希表中的位置
            Integer.toHexString():将整数转字符串
          [I@4eec7777的含义:
            [表示数组类型;
            I是int的简写表示数组中元素是int类型;
            4eec7777表示对象位置的哈希码;

          那么如何正确打印出数组类型的变量呢?
            使用Arrays.toString(数组变量);
        */
        /*
        int[] arr = {1,2,3,4,5,6};
        System.out.println(arr);
        System.out.println(Arrays.toString(arr));
        */


        //数组内存分配图:
        /*
        int[] a = new int[3],内存图如下:
             +--------------+
        a -->|      0       |   // a[0]=0
             +--------------+
             |      0       |   // a[1]=0
             +--------------+
             |      0       |   // a[2]=0
             +--------------+
        */

        /*
        获取数组中所有元素的方法:
          1.for循环
          2.增强for循环
        */

        //1.for循环获取数组元素
        /*
        int[] arr = {1,3,5,7,9};
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        */

        //2.增强for循环获取数组元素
        /*
        //这里i表示数组中的每个元素
        int[] arr = {1,3,5,7,9};
        for (int i : arr) {
            System.out.println(i);
        }
        */

        /*
        练习1：需求：定义整型数组，从键盘输入5个元素，并求其最大值

        练习2:有一个数组:8,4,2,1,23,344,12
        此时从键盘中任意输入一个数据,判断数组中是否包含此数

        练习3：把一个数组元素逆序交换，
        int[] arr ={5,11,15,24,36,47,59,66};
        交换元素后int[] arr = {66,59,47,36,24,15,11,5}
        */




        //练习1答案
        /*Scanner sc = new Scanner(System.in);
        int[] arr = new int[5];
        System.out.println("请输入第一个数");
        int a1 = sc.nextInt();
        arr[0] = a1;
        System.out.println("请输入第二个数");
        int a2 = sc.nextInt();
        arr[1] = a2;
        System.out.println("请输入第三个数");
        int a3 = sc.nextInt();
        arr[2] = a3;
        System.out.println("请输入第四个数");
        int a4 = sc.nextInt();
        arr[3] = a4;
        System.out.println("请输入第五个数");
        int a5 = sc.nextInt();
        arr[4] = a5;
        //for循环求最大值
        int temp = 0;
        for (int item : arr) {
            //如果item大于temp,将item赋值给temp
            if(item>temp){
                temp = item;
            }
        }
        System.out.println("数组中最大数字:"+temp);*/

        //练习2答案
        /*int[] arr = {8,4,2,1,23,344,12};
        Scanner sc = new Scanner(System.in);
        System.out.println("数组内容:{8,4,2,1,23,344,12}");
        System.out.println("请输入一个数字");
        int x = sc.nextInt();
        boolean flag = false;
        for (int i : arr) {
            if(i == x){
                flag = true;
                break;
            }
        }
        if(flag){
            System.out.println("数组中存在此数字");
        }else{
            System.out.println("数组中不存在此数字");
        }*/

        //练习3答案
        /*
        int[] arr ={5,11,15,24,36,47,59};
        //交换的次数为arr.length/2
        for (int i = 0; i < arr.length/2; i++) {
            int temp = arr[i];
            //因为要交换的两个元素的索引和正好是arr.length-1,
            //可以用这个规律确定两个要交换元素的索引
            arr[i] = arr[arr.length-1-i];
            arr[arr.length-1-i] = temp;
        }
        System.out.println(Arrays.toString(arr));
        */

    }
}
