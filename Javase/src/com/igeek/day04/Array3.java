package com.igeek.day04;

/**
 * @author wangjin
 * 2023/8/14 9:45
 * @description TODO
 */
public class Array3 {
    public static void main(String[] args) {
        char[][] cs = {
                {'床','前','明','月','光'},
                {'疑','是','地','上','霜'},
                {'举','头','望','明','月'},
                {'低','头','思','故','乡'},
        };
        for (int i = 0; i < cs.length; i++) {
            for (int j = 0; j < cs[i].length; j++) {
                System.out.print(cs[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println("--------------------");
        for (int i = 0; i <5; i++) {
            for (int j = cs.length-1; j >=0; j--) {
                char item = cs[j][i];
                System.out.print(item+"\t");
            }
            System.out.println();
        }
    }
}
