package com.igeek.day03;

/**
 * @author wangjin
 * 2023/8/10 19:07
 * @description TODO
 * 分析以下需求，并用代码实现：
 * 	1.珠穆朗玛峰高度为8848米，有一张足够大的纸，厚度为0.0001米。
 * 	2.请问，我折叠多少次，可以折成珠穆朗玛峰的高度。
 */
public class Test09 {
    public static void main(String[] args) {
        double height = 0.0001;
        int count = 0;
        while(height<8848.00){
            height = height * 2 ;
            count++;
        }
        System.out.println("折叠了"+count+"次,"+"高度为:"+height);

    }
}
