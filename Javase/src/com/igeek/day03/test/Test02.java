package com.igeek.day03;

/**
 * @author wangjin
 * 2023/8/10 18:15
 * @description TODO
 * 使用for循环,求出1-100之间的奇数之和.
 * 1.定义一个变量,用来记录奇数的累加和;
 * 2.for循环得到1到100之间的每个数字;
 * 3.在for循环里面,判断这个数字是否为奇数;
 * 4.如果为奇数,就与变量累加;
 * 5.for循环结束后,打印累加和;
 */
public class Test02 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            if (i%2!=0){
                sum+=i;
            }
        }
        System.out.println("sum="+sum);
    }
}
