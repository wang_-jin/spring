package com.igeek.day03;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 18:15
 * @description TODO
 * 键盘录入学生考试成绩，判断学生等级:
 * 90-100	优秀
 * 80-90	好
 * 70-80	良
 * 60-70	及格
 * 			60以下	不及格
 * 		1.从键盘上录入一个学生的考试成绩:
 * 		2.使用if语句的第三种格式判断考试成绩属于哪个范围,然后确定成绩的等级并打印:
 */
public class Test01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入分数：");
        int score = sc.nextInt();
        if (score<60){
            System.out.println("不及格");
        }
        else if (score<70){
            System.out.println("及格");
        }
        else if (score<80){
            System.out.println("良");
        }
        else if (score<90){
            System.out.println("好");
        }
        else if (score<=100){
            System.out.println("优秀");
        }
    }
}
