package com.igeek.day03;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 18:15
 * @description TODO
 * 请按如下要求编写程序,打印菜单:
 * 		1.从键盘上录入一个1到5的数字;
 * 		2.当数字为1时打印菜单"新建";
 * 		当数字为2时打印菜单"打开文件";
 * 		当数字为3时打印菜单"保存";
 * 		当数字为4时打印菜单"刷新";
 * 		当数字为5时打印菜单"退出",并退出程序;
 * 		此行代码可以终止java程序运行: System.exit(0);
 * 	    1.从键盘上录入一个三位数;
 * 		2.使用switch语句判断录入的数字为哪个菜单编号,并打印对应的菜单名称;
 * 		3.使用while循环,重复执行1,2两个步骤;
 */
public class Test05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("请输入：");
            int m  = sc.nextInt();
            switch(m){
                case 1:
                    System.out.println("新建");
                    break;
                case 2:
                    System.out.println("打开文件");
                    break;
                case 3:
                    System.out.println("保存");
                    break;
                case 4:
                    System.out.println("刷新");
                    break;
                case 5:
                    System.out.println("退出");
                    System.exit(0);
                default:
                    System.out.println("输入错误，请重新输入！");
            }
        }
    }
}

