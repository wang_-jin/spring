package com.igeek.day03;

/**
 * @author wangjin
 * 2023/8/10 18:15
 * @description TODO
 */
public class Test06 {
    public static void main(String[] args) {
        for(int x=1; x<=10; x++) {
            if(x%3==0) {
                //()在此处填写代码
            }
            System.out.println("Java基础班");
        }
    }
}

/*问题:
		1.想在控制台输出2次:"Java基础班"   括号()里面应该填写什么?  break;
		2.想在控制台输出7次:"Java基础班"   括号()里面应该填写什么?  continue;
		3.想在控制台输出13次:"Java基础班"   括号()里面应该填写什么?
System.out.println("Java基础班");
*/

/*public class Task01 {
		public static void main(String[] args) {
			int x = 1,y = 1;
			if(x++==2 & ++y==2)	{
				x =7;
			}
			System.out.println("x="+x+",y="+y);  //2  2

			int a = 1,b = 1;
			if(a++==2 && ++b==2) {
				a =7;
			}
			System.out.println("a="+a+",b="+b); //2  1
		}
	}
*/


/*public class Task02{
		public static void main(String[] args) {
			int x = 1,y = 1;
			if(x++==1 | ++y==1)	{  //true
				x =7;
			}
			System.out.println("x="+x+",y="+y);  //7 2

			int a = 1,b = 1;
			if(a++==1 || ++b==1) {
				a =7;
			}
			System.out.println("a="+a+",b="+b); //7 1

			boolean c = true;
			if(c==false)
				System.out.println("a");
			else if(c)
				System.out.println("b");//执行
			else if(!c)
				System.out.println("c");
			else
				System.out.println("d");
			}
	}
*/

/*public class Task03 {
		public static void main(String[] args) {
			int x = 2,y=3;
			switch(x)
			{
				default:
					y++;  //4
				case 3:
					y++;  //5
					break;
				case 4:
					y++;
			}
			System.out.println("y="+y);  //5
}
	}
*/
