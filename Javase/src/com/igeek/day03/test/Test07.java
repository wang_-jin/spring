package com.igeek.day03;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 19:07
 * @description TODO
 * 分析以下需求，并用代码实现：
 * 	1.根据工龄(整数)给员工涨工资(整数),工龄和基本工资通过键盘录入
 * 	2.涨工资的条件如下：
 * 		[10-15)     +5000
 * 		[5-10)      +2500
 * 		[3~5)       +1000
 * 		[1~3)       +500
 * 		[0~1)       +200
 * 	3.如果用户输入的工龄为10，基本工资为3000，程序运行后打印格式
 * 	"您目前工作了10年，基本工资为 3000元, 应涨工资 5000元,涨后工资 8000元"
 */
public class Test07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("工龄为：");
        int age = sc.nextInt();
        System.out.println("基本工资为：");
        int salary = sc.nextInt();
        //[0~1)       +200
        if(age>=0 && age<1){
            int add = 200;
            System.out.println("您目前工作了"+age+"年,"+"基本工资为 "+salary+"元,"
                    +"应涨工资 "+add+"元,"+"涨后工资 "+(salary+add)+"元");
            //[1~3)       +500
        }else if(age>=1 && age<3){
            int add = 500;
            System.out.println("您目前工作了"+age+"年,"+"基本工资为 "+salary+"元,"
                    +"应涨工资 "+add+"元,"+"涨后工资 "+(salary+add)+"元");
            //[3~5)       +1000
        }else if(age>=3 && age<5) {
            int add = 1000;
            System.out.println("您目前工作了" + age + "年," + "基本工资为 " + salary + "元,"
                    + "应涨工资 " + add + "元," + "涨后工资 " + (salary + add) + "元");
            //[5-10)      +2500
        }else if(age>=5 && age<10) {
            int add = 2500;
            System.out.println("您目前工作了" + age + "年," + "基本工资为 " + salary + "元,"
                    + "应涨工资 " + add + "元," + "涨后工资 " + (salary + add) + "元");
            //[10-15)     +5000
        }else if(age>=10 && age<=15) {
            int add = 5000;
            System.out.println("您目前工作了" + age + "年," + "基本工资为 " + salary + "元,"
                    + "应涨工资 " + add + "元," + "涨后工资 " + (salary + add) + "元");
        }
    }
}
