package com.igeek.day03;

import java.util.Scanner;

/**
 * @author wangjin
 * 2023/8/10 18:15
 * @description TODO
 * 从键盘上录入一个大于100的三位数,求出100到该数字之间满足如下要求的数字之和:
 * 		1.数字的个位数不为7;
 * 		2.数字的十位数不为5;
 * 		3.数字的百位数不为3;
 * 		1.从键盘上录入一个三位数;
 * 		2.定义一个变量,用来记录满足条件的数字累加之和;
 * 		3.使用for循环获取100到该数字之间的所有数字;
 * 		4.得到当前拿到的这个数的个位数,十位数,百位数;
 * 		5.判断个位数不为7,十位数不为5,百位数不为3;
 * 		6.如果满足条件就累加;
 * 		7.循环结束后打印累加之和;
 */
public class Test04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个大于100的数：");
        int m  = sc.nextInt();
        int sum = 0;
        if (m>100) {
            for (int i = 100; i < m; i++) {
                int a = i%10;//个位数
                int b = i/100;//百位数
                int c = (i/10)%10;//十位数
                //判断个位数不为7,十位数不为5,百位数不为3;
                if(a!=7 && c!=5 && b!=3){
                    System.out.println("i="+i);
                    sum+=i;
                }
            }
            System.out.println("sum="+sum);
        }else{
            System.out.println("输入错误！");
        }
    }
}
