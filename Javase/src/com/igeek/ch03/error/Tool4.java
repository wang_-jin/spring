package com.igeek.ch03.error;

public class Tool4 {
    //自定义异常
    public static void foo(String str) throws CustomException {
        if (str == null){
            throw new CustomException();
        }else {
            System.out.println("传入的字符串为:"+str);
        }
    }
    public static void main(String[] args) {
        try{
            foo(null);
        }catch(Exception err){
            System.out.println("传入的字符串不能为空");
        }

    }
}
