package com.igeek.ch03.error;
/*
  自定义异常:
    继承自Exception,需要重写无参构造方法和参数是String的构造方法
*/
public class CustomException extends Exception{
    public CustomException() {
        super();
    }

    public CustomException(String message) {
        super(message);
    }
}
