package com.igeek.ch03.error;

public class Tool1 {
  public static void main(String[] args) {
    String[] arr = {"a", "b", "c"};
    /*
    ArrayIndexOutOfBoundsException:数组越界异常
    如何找到出错的代码:
      一般来说异常说明信息(Exception in thread "main"...)的下面显示
      (类名.java:数字)就是告诉你错误行在哪的代码,如(Tool1.java:13)表明
      错误代码在类Tool1中的第13行!
    */
    System.out.println(arr[4]);
    System.out.println("出现错误后,这行代码不会执行");
  }
}
