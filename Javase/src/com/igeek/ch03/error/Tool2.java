package com.igeek.ch03.error;

import java.text.SimpleDateFormat;

public class Tool2 {
    public static void main(String[] args){
       /*
        try...catch...finally捕获异常并处理异常,
        使用try catch的好处是即使发生了错误,也不会终止
        程序的运行,程序依然可以往下执行!
        try{
          //可能会出现异常的代码块,一旦某处代码出现异常,下面的
          //代码就不会执行了
        }catch(异常类型 对象名){
          //处理异常的位置
        }finally{
          //无论是否出现异常都会执行
        }
        */
        try{
            //可能会出现异常的代码
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            fmt.parse("2023/03/12");
        }catch(Exception err){
            //出现异常之后的解决
            System.out.println("日期格式不正确!");
        }finally{
            System.out.println("finally始终会执行");
        }
    }

}
