package com.igeek.ch03.error;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Tool3 {
    public static void main(String[] args) {
      /*
      如果方法中代码遇到了错误你希望马上解决就可以使用try catch,
      如果方法中代码遇到了错误你不希望马上解决,而是想让这个方法的
      调用者解决,就可以使用throw和throws关键字
      */
        try {
            foo();
        } catch (ParseException e) {
            System.out.println("日期格式不正确!");
        }

        try{
            divide(1,0);
        }catch(Exception e){
            System.out.println("除数不能为0");
        }

    }
    /*
    throws关键字用在方法名后面,他的作用是不在当前方法里面解决错误,
    将错误向上抛出,让这个方法的调用者去解决错误
    */
    public static void foo() throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        fmt.parse("2023/03/12");
    }

    //定义一个除法方法
    public static int divide(int a,int b){
      if(b==0){
          /*
          throw关键字用于主动抛出错误,主要用在方法里面
          */
          throw new ArithmeticException("除数不能为0");
      }
      return a/b;
    }
}
