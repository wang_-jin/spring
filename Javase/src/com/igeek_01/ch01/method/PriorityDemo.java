package com.igeek_01.ch01.method;

/**
 * @author wangjin
 * 2023/9/6 20:21
 * @description Thread类的常用方法
 * 常用方法：
 * public final void setPriority(int newPriority) 设置此线程的优先级
 * public final int getPriority() 获取此线程的优先级
 *
 * 常量：
 * public final static int MIN_PRIORITY = 1; 线程可以拥有的最小优先级
 * public final static int NORM_PRIORITY = 5;线程可以拥有的默认优先级
 * public final static int MAX_PRIORITY = 10; 线程可以拥有的最大优先级
 *
 * 如果给线程设置了优先级，优先级高的线程 实际 不一定会优先获取CPU执行权，所以线程调度还是具有随机性
 */
public class PriorityDemo {
    public static void main(String[] args) {
        //主线程优先级  5
        System.out.println(Thread.currentThread().getName()+"的优先级"+Thread.currentThread().getPriority());

        //最小优先级
        Thread th1 = new Thread("线程1"){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"的优先级"+Thread.currentThread().getPriority());
            }
        };
        th1.setPriority(Thread.MIN_PRIORITY); //1
        th1.start();
        //最大优先级
        Thread th2 = new Thread("线程2"){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"的优先级"+Thread.currentThread().getPriority());
            }
        };
        th2.setPriority(Thread.MAX_PRIORITY); //1
        th2.start();
    }
}
