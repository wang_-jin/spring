package com.igeek_01.ch01.method;

import com.igeek_01.ch01.thread.th03.MyCallable;

/**
 * @author wangjin
 * 2023/9/6 20:39
 * @description join()方法
 * join()方法
 *  void join()  throws InterruptedException  等待该线程终止。   无限等待
 *  void join(long millis) throws InterruptedException 等待时间结束了 或者 等待该线程终止。计时等待
 *
 *  sleep()，yield()，join()的区别：
 *  1.sleep：一旦当前线程调用sleep方法后，则当前线程会进入休眠状态，直到沉睡时间结束，再次争抢时间片，才能获取执行权。
 *  2.yield：进入 假意退让状态， 将当前的cpu执行权交给其他线程，立即再次进行争抢时间片，一旦获取时间片后，将会继续执行任务
 *  3.join: 调用join的线程对象，等待该调用执行完毕....真正决定线程执行顺序的，通过join（）方法是最靠谱的。
 *
 * 需求：th1>th2>main
 */
class MyThread extends Thread{
    public MyThread(String name) {
        super(name);
    }
    @Override
    public void run() {
        System.out.println("th1开始执行!");
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
        System.out.println("th1执行结束!");
    }
}
class MyThread2 extends Thread{
    private MyThread th1;
    public MyThread2(String name, MyThread th1) {
        super(name);
        this.th1 = th1;
    }

    @Override
    public void run() {
        System.out.println("th2开始执行!");
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
        try {
            th1.join();//等到th1执行完了,再执行后续任务
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("th2执行结束!");
    }
}
public class JoinDemo {
    public static void main(String[] args) {
        System.out.println("主线程开始了!");
        MyThread th1 = new MyThread("线程1");
        MyThread2 th2 = new MyThread2("线程2",th1);
        th1.start();
        th2.start();

        try {
            th2.join(10);//等10ms,th2执行完了再执行后续任务
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程结束了!");
    }
}
