package com.igeek_01.ch01.method;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/6 20:21
 * @description sleep和yield的区别
 * sleep和yield的区别：
 * sleep()
 * 1.static void sleep(long millis)   在指定的毫秒数内让当前正在执行的线程休眠（暂停执行）
 * 2.一旦当前线程调用sleep方法后，则当前线程会进入休眠状态，直到沉睡时间结束，再次争抢时间片，才能获取执行权。
 * 3.sleep()线程沉睡过程中不会释放对象锁
 * 4.InterruptedException 有可能会产生中断异常
 *
 * yield()
 * 1.static void yield()      暂停当前正在执行的线程对象，并执行其他线程。
 * 2.假意退让：将当前的cpu执行权交给其他线程，立即再次进行争抢时间片，一旦获取时间片后，将会继续执行任务
 * 需求：线程2在线程1之前执行
 */
public class SleepAndYieldDemo {
    public static void main(String[] args) {
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                Thread.yield();//当前线程暂停执行
/*                try {
                    //Thread.sleep(1000);
                    TimeUnit.SECONDS.sleep(1);//线程休眠1秒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                System.out.println(Thread.currentThread().getName()+",i="+i);


            }
        },"线程1").start();


        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+",i="+i);
            }
        },"线程2").start();
    }
}
