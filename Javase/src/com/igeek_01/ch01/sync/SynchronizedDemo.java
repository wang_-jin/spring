package com.igeek_01.ch01.sync;

/**
 * @author wangjin
 * 2023/9/6 20:59
 * @description 解读字节码的文件 --- 解析synchronized
 * 字节码文件的反编译命令：
 * javap -c ***.class  文件反编译
 * javap -v ***.class  文件反编译
 * -c 对代码进行反汇编
 * -v -verbose 输出附加信息（包括行号、本地变量表，反汇编等详细信息）
 *
 * synchronized同步代码块:
 *   1.synchronized实现使用的是monitorenter和monitorexit指令。
 *   2.monitorenter和monitorexit配比是1:2，
 *      因为需要确保无论是正常执行程序还是出现异常，都可以释放对象锁。
 *   3.特殊情况：monitorenter和monitorexit配比是1:1，
 *     因为块中抛出异常，因为此时要告知系统是非正常中断的释放锁。
 *
 * 管程(监视器)monitor：
 *  在HotSpot虚拟机中，monitor采用ObjectMonitor（c++实现的）实现。
 *  每个对象天生都带着一个对象监视器。
 *
 *
 * synchronized同步方法:
 *      1.成员方法 通过是否添加标记 ACC_SYNCHRONIZED
 *      2.静态方法 通过是否添加标记 ACC_STATIC, ACC_SYNCHRONIZED
 *      3.调用monitorenter指令将会检查方法的ACC_SYNCHRONIZED访问标志是否被设置。
 *          如果设置了，执行线程会将先持有monitorenter然后再执行方法，
 *          最后在方法完成(无论是正常完成还是非正常完成)时释放 monitorexit。
 */
public class SynchronizedDemo {
    //代码块方法
    //反编译命令 javap -c SynchronizedDemo.class
    public void test1(){
        synchronized (this){
            System.out.println("hello test2");
            throw new RuntimeException("运行期异常");
        }
    }
    //同步方法
    //反编译命令 javap -v SynchronizedDemo.class
/*    public static synchronized void test2(){
        System.out.println("hello test2");
    }*/

    public static void main(String[] args) {

    }
}
