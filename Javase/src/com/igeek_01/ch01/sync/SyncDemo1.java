package com.igeek_01.ch01.sync;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/6 20:58
 * @description 线程安全和线程同步
 * 线程安全：
 * 多个线程，并发访问共享资源时，会出现 争抢现象。
 * 例如：买票时，会可能重复票,负数等问题
 *
 * 线程同步：
 * 线程同步是为了解决线程安全问题。
 * 实现的目的：和单线程是一样的执行结果。要求多线程并发时，能够有序等待的现象，使用同步机制synchronized 或者 lock锁
 *
 * 实现线程同步的方式：
 * 1.同步代码块
 *      语法：synchronized(对象锁){ 代码块}
 *      对象锁可以有哪些表现形式：
 *      1.this       who调用当前方法，who就是对象锁   搭配成员方法进行使用
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 争抢 现象
 *
 *      2.类.class   类锁 ，无论创建多少个对象，类锁都是同一把  搭配静态方法进行使用
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 有序等待 现象
 *
 *      3.字符串常量 例如“A”  "B"  在字符串常量池中分配内存  String a = "A" ; String b = "A"  a == b 结果是true
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 有序等待 现象
 *
 *      4.byte[] lock = new byte[0]  开销最小的锁  一般当作属性使用 作用和this是一致的
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 争抢 现象
 *
 *
 * 2.同步方法
 *      语法：public synchronized 返回值类型 方法名(){}
 *      1.成员方法
 *          类似于this this作为对象锁   who在调用方法，who就是对象锁
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 争抢 现象
 *
 *      2.静态方法
 *          类似于类锁  类作为对象锁 无论创建多少个对象，类锁都是同一把
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *          通过不同的s1 s2来调用的(不同的锁)，出现 有序等待 现象
 *
 * 3.lock锁
 *      class ReentrantLock implements Lock 可重入锁
 *      方法：lock()获取锁  unlock()释放锁
 *
 *      1.作为成员属性  和对象有关
 *          private final Lock lock = new ReentrantLock();
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *  *       通过不同的s1 s2来调用的(不同的锁)，出现 争抢 现象
 *
 *      2.作为静态属性 和类有关
 *          private final static Lock lock = new ReentrantLock();
 *          通过同一个s1来调用的（同一把锁），出现 有序等待 现象
 *  *       通过不同的s1 s2来调用的(不同的锁)，出现 有序等待 现象
 *
 *      注意：必须在finally中释放锁，确保无论是否有异常产生都可以释放锁，
 *         从而避免出现异常没有释放锁导致其他线程始终获取不到锁 --死锁现象
 */
public class SyncDemo1 {
    private byte[] lock = new byte[0];
    public void test(){
        //同步代码块-->给一段代码加锁
            //synchronized (lock){
            //synchronized ("A"){
            //synchronized (this){
            synchronized (SyncDemo1.class){
                try {
                    TimeUnit.SECONDS.sleep(1);//线程沉睡不会释放对象锁(辅助作用,线程1和线程2直接过渡有个停顿作用)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+",i="+i);
                }
            }
    }
    public static void main(String[] args) {
        //两把锁
        SyncDemo1 s1 = new SyncDemo1();
        SyncDemo1 s2 = new SyncDemo1();
        //调用任务,开启线程
        new Thread(()->s1.test(),"线程1").start();
        new Thread(()->s2.test(),"线程2").start();
    }
}
