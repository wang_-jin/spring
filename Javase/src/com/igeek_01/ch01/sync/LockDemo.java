package com.igeek_01.ch01.sync;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/6 20:57
 * @description 线程同步 -- Lock锁
 *
 * Lock 锁 ---Lock接口
 * 位于  java.util.concurrent包下
 *
 * ReentrantLock -- 可重入锁
 * class ReentrantLock implements Lock
 * 构造方法：ReentrantLock()
 * 常用方法：
 *      void lock(); 获取锁
 *      void unlock();释放锁
 */
public class LockDemo {
    //private final Lock lock = new ReentrantLock();
    private static final Lock lock = new ReentrantLock();
    public void test1(){
            try {
                lock.lock();
                System.out.println(Thread.currentThread().getName() +"获取到了锁");
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ",i=" + i);
                }
            }finally {
                //无论是否有异常产生 都要释放锁
                lock.unlock();//unlock要在finally里用
                System.out.println(Thread.currentThread().getName() +"释放了锁");
            }
        }
    public void test2(){
        try {
            lock.lock();
            System.out.println(Thread.currentThread().getName() +"获取到了锁");
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + ",i=" + i);
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
            System.out.println(Thread.currentThread().getName() +"释放了锁");
        }
    }

    public static void main(String[] args) {
        LockDemo lock1 = new LockDemo();
        LockDemo lock2 = new LockDemo();
        new Thread(()->lock1.test1(),"线程1").start();
        new Thread(()->lock2.test1(),"线程2").start();

    }
}
