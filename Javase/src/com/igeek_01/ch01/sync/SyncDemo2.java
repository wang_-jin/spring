package com.igeek_01.ch01.sync;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/6 20:58
 * @description 同步方法
 */
public class SyncDemo2 {
    //成员方法 两个对象会出现争抢
    public synchronized void test1(){
/*        try {
            TimeUnit.SECONDS.sleep(1);//线程沉睡不会释放对象锁
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }
    //静态方法 两个对象会有序等待
    public static synchronized void test2(){
        try {
            TimeUnit.SECONDS.sleep(1);//线程沉睡不会释放对象锁
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }

    public static void main(String[] args) {
        //同一个锁
        SyncDemo2 s1 = new SyncDemo2();
        SyncDemo2 s2 = new SyncDemo2();
        //开启多线程
        new Thread(()-> s1.test1(),"线程1").start();
        new Thread(()-> s1.test1(),"线程2").start();
    }
}
