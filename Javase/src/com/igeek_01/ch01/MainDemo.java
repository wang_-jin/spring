package com.igeek_01.ch01;


/**
 * @author wangjin
 * 2023/9/6 18:04
 * @description 线程的概念
 *
 */
public class MainDemo {
    public static void main(String[] args) {
        //currentThread 获取当前正在执行的线程对象  Thread[线程名,优先级,线程组]
        System.out.println(Thread.currentThread());
        String str = null;
        System.out.println("abcd");
        System.out.println(str.length());//NullPointerException 空指针异常
    }
}
