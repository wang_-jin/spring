package com.igeek_01.ch01.thread_test.test1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author wangjin
 * 2023/9/6 22:23
 * @description TODO
 */
public class Test {
    /*2.请按如下要求编写多线程程序：
		1.得到一个随机的整数n，创建n个子线程对象；
		2.要求在子线程中把当前线程的名称作为元素添加一个集合中；
		3.当n个线程的名称都添加到集合中，遍历集合打印每个线程的名称；*/
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Random random = new Random();
        int n = random.nextInt(10);
        System.out.println(n);
        for (int i = 1; i <= n; i++) {
            new Thread(()->{
                list.add(Thread.currentThread().getName());
            },"线程"+i).start();
        }
        list.forEach(System.out::println);
    }
}
