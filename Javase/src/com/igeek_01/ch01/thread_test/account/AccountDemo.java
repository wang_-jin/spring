package com.igeek_01.ch01.thread_test.account;

import com.igeek_01.ch01.ticket.Ticket;

import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/6 22:44
 * @description TODO
 */
public class AccountDemo {
    public static void main(String[] args) {
        //共享资源  对象锁
        Account account = new Account();

        //开启多线程
        Stream.of("张三","李四").forEach(name->new Thread(account,name).start());
    }
}
