package com.igeek_01.ch01.thread_test.account;

/**
 * @author wangjin
 * 2023/9/6 22:44
 * @description TODO
 */
public class Account implements Runnable{
    //共同资源
    private int balance = 10000;

    //售卖行为
    public  void getBalance(){
        synchronized (this) {//对象锁 和 ticket对象有关
            if(balance>=1) {
                int money = (int)(Math.random()*1000);
                System.out.println(Thread.currentThread().getName() + "您当前取款金额" + money);
                balance=balance-money;
                System.out.println(Thread.currentThread().getName() + "账户余额" + balance);
            }else{
                System.out.println(Thread.currentThread().getName() + "无钱可取");
            }
        }
    }

    @Override
    public void run() {
        while(true){
            if(balance<=0){
                break;//循环的结束条件：无钱可取
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            getBalance();
        }
    }
}
