package com.igeek_01.ch01.thread.th02;

/**
 * @author wangjin
 * 2023/9/6 11:01
 * @description 创建多线程方式-实现Runnable接口
 */
public class MyRunnable implements Runnable{
    public MyRunnable() {
    }


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //创建一个任务对象
        MyRunnable myRunnable = new MyRunnable();
        //创建两个线程对象,Runnable target 不能为null
        Thread thread = new Thread(myRunnable,"线程1");
        Thread thread1 = new Thread(myRunnable,"线程2");
        //创建的两条多线程执行同一个任务
        thread.start();
        thread1.start();
        //主线程的任务
        for (int i = 0; i < 10; i++) {
            Thread.sleep(100);
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }
}
