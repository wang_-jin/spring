package com.igeek_01.ch01.thread.lambda.th03;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author wangjin
 * 2023/9/6 18:03
 * @description TODO
 */
public class LambdaDemo3 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        new Thread(new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int sum = 0;
                for (int i = 0; i < 5; i++) {
                    System.out.println(Thread.currentThread().getName()+",i="+i);
                }
                return sum;
            }
        }),"线程1").start();

        new Thread(new FutureTask<Integer>(()->{
            int sum = 0;
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName()+",i="+i);
            }
            return sum;
        }),"线程2").start();

        FutureTask<Integer>futureTask = new FutureTask<Integer>(()->{
            int sum = 0;
            for (int i = 0; i < 5; i++) {
                sum+=i;
                System.out.println(Thread.currentThread().getName()+",i="+i);
            }
            return sum;
        });
        new Thread(futureTask,"线程3").start();
        System.out.println("线程3执行结果:"+futureTask.get());
    }
}
