package com.igeek_01.ch01.thread.lambda.th01;

/**
 * @author wangjin
 * 2023/9/6 18:03
 * @description 简写多线程
 */
public class LambdaDemo1 {

    public static void main(String[] args) {
        new Thread("线程1"){
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+",i="+i);
                }
            }
        }.start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+",i="+i);
            }
        },"线程2").start();
    }
}
