package com.igeek_01.ch01.thread.lambda.th02;


/**
 * @author wangjin
 * 2023/9/6 18:03
 * @description TODO
 */
public class LambdaDemo2 {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName()+",i="+i);
                }
            }
        },"线程1").start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+",i="+i);
            }
        },"线程2").start();
    }
}
