package com.igeek_01.ch01.thread.th03;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author wangjin
 * 2023/9/6 18:02
 * @description 创建多个线程-implements Callable 接口 带有返回值的线程任务
 */
public class MyCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum+=i;
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
        return sum;
    }

    public static void main(String[] args) {
        //创建带有返回值的任务对象
        MyCallable myCallable = new MyCallable();
        //创建FutureTask对象 (实现Runnable接口)
        FutureTask<Integer> futureTask = new FutureTask<Integer>(myCallable);
        FutureTask<Integer> futureTask1 = new FutureTask<Integer>(myCallable);
        //创建线程对象
        Thread thread = new Thread(futureTask,"线程1");
        Thread thread1 = new Thread(futureTask1,"线程2");
        thread.start();
        thread1.start();
        //阻塞在此处，直到线程1执行完毕才解除阻塞 --  获取带有返回值的任务的执行结果
        try {
            System.out.println("线程1任务返回值结果为:"+futureTask.get());
            System.out.println("线程2任务返回值结果为:"+futureTask1.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
