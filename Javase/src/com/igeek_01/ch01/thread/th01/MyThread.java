package com.igeek_01.ch01.thread.th01;

/**
 * @author wangjin
 * 2023/9/6 10:47
 * @description 创建多线程方式---继承Thread
 */
public class MyThread extends Thread{
    public MyThread(String name) {
        super(name);
    }

    @Override
    //定义一个线程任务
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //创建线程对象,并指定线程名字
        MyThread myThread = new MyThread("线程1");
        //开启多线程
        myThread.start();
        //myThread.run();//直接调用run()方法,是由主线程来做任务,没有真正开启多线程
        //myThread.start();//IllegalThreadStateException 同一个线程对象不能开启多次

        for (int i = 0; i < 10; i++) {
            Thread.sleep(100);//让线程沉睡100ms,目的是让出cpu执行权
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }
}
