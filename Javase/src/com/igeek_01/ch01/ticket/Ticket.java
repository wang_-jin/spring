package com.igeek_01.ch01.ticket;

/**
 * @Description 售卖任务
 * @Author fqq
 * @Date 2023/9/6 16:30
 */
public class Ticket implements Runnable{
    //共同资源
    private int ticketNum = 100;

    //售卖行为
    public  void sale(){
        synchronized (this) {//对象锁 和 ticket对象有关
            if(ticketNum>=1) {
                System.out.println(Thread.currentThread().getName() + "您当前购买的票号是" + ticketNum);
                ticketNum--;
            }else{
                System.out.println(Thread.currentThread().getName() + "无法购买了");
            }
        }
    }

    @Override
    public void run() {
        while(true){
            if(ticketNum<=0){ //窗口2 窗口1 窗口3
                break;//循环的结束条件：无票可卖了
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //售卖  窗口2 窗口1 窗口3
            sale();
        }
    }
}
