package com.igeek_01.ch01.ticket;

import java.util.stream.Stream;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/9/6 16:33
 */
public class TicketDemo {
    public static void main(String[] args) {
        //共享资源  对象锁
        Ticket ticket = new Ticket();

        //开启多线程
        Stream.of("窗口1","窗口2","窗口3").forEach(name->new Thread(ticket,name).start());
    }
}
