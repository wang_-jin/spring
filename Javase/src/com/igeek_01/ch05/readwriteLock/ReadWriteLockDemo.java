package com.igeek_01.ch05.readwriteLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author wangjin
 * 2023/9/13 14:02
 * @description 读写锁
 * 读写锁：对资源读取和写入的时候拆分为2部分处理，读的时候可以多线程一起读，写的时候必须同步地写
 *
 * Lock接口           -- ReentrantLock 可重入锁
 * ReadWriteLock接口  -- ReentrantReadWriteLock 可重入的读写锁
 *                      静态内部类：读锁static class ReadLock implements Lock
 *                      静态内部类：写锁static class WriteLock implements Lock
 *                      常用方法：lock()获取锁  unlock()释放锁
 *
 * 总结：
 * 1.ReentrantLock，synchronized 读读互斥，读写互斥，写写互斥，不管什么操作每次只能进来一个
 * 2.ReentrantReadWriteLock     读读不互斥，读写和写写互斥
 *  读写锁的缺点： --》StampedLock 邮戳锁(并发写的时候，采用CAS乐观锁)
 *  1.锁饥饿现象：写线程始终获取不到锁
 *  2.读的过程中，如果没有释放锁，写线程不可能获得锁，必须读完后，才能开始写，带有锁降级的问题，适用于读多写少的场景
 */
class Operation{
    //可重入锁
    //针对 读操作较多的场景时，ReentrantLock就不是很合适，会造成读读互斥，从而使得效率较低
    //private final Lock lock = new ReentrantLock();

    //可重入的读写锁 ReentrantReadWriteLock实现读读不互斥，读写和写写互斥
    //适用于 读操作较多的场景
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    //读
    public void read(){
        try {
            //获取读锁后，再上锁
            lock.readLock().lock();

            System.out.println(Thread.currentThread().getName()+"开始读数据....");
            //sleep 模拟读数据的过程
            TimeUnit.SECONDS.sleep(2);
            System.out.println(Thread.currentThread().getName()+"结束读数据....");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            //获取读锁后，再释放锁
            lock.readLock().unlock();
        }

    }

    public void write(){
        try {
            //获取写锁后，再上锁
            lock.writeLock().lock();
            System.out.println(Thread.currentThread().getName()+"开始写数据....");
            //sleep 模拟写数据的过程
            TimeUnit.SECONDS.sleep(2);
            System.out.println(Thread.currentThread().getName()+"结束写数据....");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            //获取写锁后，再释放锁
            lock.writeLock().unlock();
        }
    }
}
public class ReadWriteLockDemo {
    public static void main(String[] args) {
        Operation operation = new Operation();

        //目的：读读不互斥 读写互斥 写写互斥
        //并发读
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->operation.read(),"读线程"+i).start();
        }

        //并发写
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->operation.write(),"写线程"+i).start();
        }
    }
}
