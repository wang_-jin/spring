package com.igeek_01.ch05.lock;
//8锁：关于锁的8个问题

//问题1：如下情况，两个线程启动，通过同一个对象调用其两个synchronized方法，此时执行顺序？
//   答：    同步方法 同一把锁调用会出现有序等待
//    学习
//    睡觉
public class Test1 {
    /*
    同步方法 同一把锁调用会出现有序等待
    学习
    睡觉
    */
    public static void main(String[] args) {
        Student1 stu = new Student1();
        //对象锁stu 同一把锁
        new Thread(()-> stu.study(),"A").start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()-> stu.sleep(),"B").start();
    }
}

class Student1{
   
    public synchronized void sleep(){
        System.out.println("睡觉");
    }

    
    public synchronized void study(){
        System.out.println("学习");
    }
}

