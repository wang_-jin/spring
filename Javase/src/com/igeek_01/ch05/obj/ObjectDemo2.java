package com.igeek_01.ch05.obj;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author wangjin
 * 2023/9/13 11:37
 * @description Synchronized的性能变化
JDK1.6已经优化了synchronized
 *          提出了锁升级的机制，即无锁->偏向锁->轻量级锁->重量级锁
 *
 *      锁类型(按锁的状态分类)
 *      non-biasable无锁且不可偏向
 *      biasable无锁可偏向
 *      biased偏向锁
 *      thin lock轻量级锁
 *      fat lock重量级锁
 *
 */
class User{
    private String name;
    private int age;
    private boolean gender;

    public User(String name, int age, boolean gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public User() {
    }
}
public class ObjectDemo2 {

    public static void main(String[] args) {
        User user = new User("张三", 20, true);

        /*无锁 锁标志位：01  non-biasable
        OFF  SZ               TYPE DESCRIPTION               VALUE
          0   8                (object header: mark)     0x0000000000000001 (non-biasable; age: 0)
          8   4                (object header: class)    0x2000c143
         */
        //System.out.println(ClassLayout.parseInstance(user).toPrintable());


        /*轻量级锁 (锁竞争不激烈) 锁标志位：00  thin lock
        OFF  SZ               TYPE DESCRIPTION               VALUE
        0   8                 (object header: mark)     0x000000000287f6d8 (thin lock: 0x000000000287f6d8)

        synchronized (user){
            System.out.println(ClassLayout.parseInstance(user).toPrintable());
        }*/

        /*
        重量级锁（锁竞争比较激烈）  锁标记位：10  fat lock
        OFF  SZ               TYPE DESCRIPTION               VALUE
        0   8                    (object header: mark)     0x000000000334a2ba (fat lock: 0x000000000334a2ba)
         */
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                synchronized (user){
                    System.out.println(ClassLayout.parseInstance(user).toPrintable());
                }
            }).start();
        }
    }
}
