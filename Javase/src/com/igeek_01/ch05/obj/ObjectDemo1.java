package com.igeek_01.ch05.obj;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * @author wangjin
 * 2023/9/13 11:04
 * @description Java对象内存布局和对象头
 * 导入第三方 jol-core-0.16.jar  :
 *      JOL  Java  Object  Layout 分析对象在JVM的大小和分布
 */
class MyObject{
    private int age = 20;//4个字节
    private char gender='男';//2个字节
    private long height;//8个字节
}
public class ObjectDemo1 {
    public static void main(String[] args) {

        //查看虚拟机的详细信息  VM.current().details()
        //Running 64-bit HotSpot VM. 运行在64位HotSpot虚拟机上
        //Objects are 8 bytes aligned. 对象的对齐方式：按照8字节来读取的
        System.out.println(VM.current().details());

        /*
        对象在堆内存中的存储布局：
        基于64位虚拟机来看：
        对象头
            -对象标记markword    8个字节
            -类型指针            8个字节(类型指针压缩4个字节)
            -合计 共16个字节/12个字节（默认有存在类型指针压缩）
        实例数据
        对齐填充

        总结：MarkWord 8个字节 类型指针压缩4个字节  对齐填充4个字节 = 8+4+4=16个字节
        ch05.obj_02.MyObject object internals:  锁标记位01  无锁non-biasable
        OFF  SZ   TYPE DESCRIPTION               VALUE
          0   8        (object header: mark)     0x0000000000000001 (non-biasable; age: 0)
          8   4        (object header: class)    0x20012287
         12   4        (object alignment gap)
        Instance size: 16 bytes
        Space losses: 0 bytes internal + 4 bytes external = 4 bytes total

        添加一个属性：private int age = 20;//4个字节
        总结：MarkWord 8个字节 类型指针压缩4个字节 age属性4个字节 没有对齐填充 = 8+4+4=16个字节
        ch05.obj_02.MyObject object internals:
        OFF  SZ   TYPE DESCRIPTION               VALUE
          0   8        (object header: mark)     0x0000000000000001 (non-biasable; age: 0)
          8   4        (object header: class)    0x20012287
         12   4    int MyObject.age              20
        Instance size: 16 bytes
        Space losses: 0 bytes internal + 0 bytes external = 0 bytes total


        添加2个属性：private int age = 20;//4个字节 private char gender='男';//2个字节
        总结：MarkWord 8个字节 类型指针压缩4个字节 age属性4个字节 gender属性2个字节 对齐填充6个字节 = 24个字节
        ch05.obj_02.MyObject object internals:
        OFF  SZ   TYPE DESCRIPTION               VALUE
          0   8        (object header: mark)     0x0000000000000005 (biasable; age: 0)
          8   4        (object header: class)    0x20012287
         12   4    int MyObject.age              20
         16   2   char MyObject.gender           男
         18   6        (object alignment gap)
        Instance size: 24 bytes
        Space losses: 0 bytes internal + 6 bytes external = 6 bytes total


        添加三个属性：
        总结：MarkWord 8个字节 类型指针压缩4个字节 age属性4个字节 gender属性2个字节 height属性8个字节 对齐填充6个字节 = 32个字节
        ch05.obj_02.MyObject object internals:
        OFF  SZ   TYPE DESCRIPTION               VALUE
          0   8        (object header: mark)     0x0000000000000001 (non-biasable; age: 0)
          8   4        (object header: class)    0x20013287
         12   4    int MyObject.age              20
         16   8   long MyObject.height           0
         24   2   char MyObject.gender           男
         26   6        (object alignment gap)
        Instance size: 32 bytes
        Space losses: 0 bytes internal + 6 bytes external = 6 bytes total
         */
        MyObject myObject = new MyObject();
        System.out.println("----------------------------------");
        //对象的内存大小
        System.out.println(ClassLayout.parseInstance(myObject).toPrintable());
    }
}
