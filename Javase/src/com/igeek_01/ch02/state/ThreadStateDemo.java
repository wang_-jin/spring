package com.igeek_01.ch02.state;

/**
 * @Description 线程状态
 * @Author fqq
 * @Date 2023/9/7 14:07
 *
 * Thread的内部枚举类State 线程状态的取值：
 * NEW      新建
 * RUNNABLE 可运行
 * BLOCKED  阻塞
 * WAITING  无限等待
 * TIMED_WAITING 计时等待
 * TERMINATED 终止
 *
 */
class MyRunnable implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+",i="+i);
        }
    }
}
public class ThreadStateDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new MyRunnable(), "线程1");
        System.out.println("新建状态 = " + thread.getState()); //NEW
        thread.start();
        System.out.println("可运行状态 = " + thread.getState());//RUNNABLE

        //主线程稍微睡一会儿 确保线程1先执行
        Thread.sleep(50);
        System.out.println("计时等待状态 = "+thread.getState());//TIMED_WAITING

        //主线程在 线程1执行完毕后 再继续执行
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("终止状态 = " + thread.getState());//TERMINATED
    }
}
