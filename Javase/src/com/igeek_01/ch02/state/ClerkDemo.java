package com.igeek_01.ch02.state;


import java.util.concurrent.TimeUnit;

/**
 * @Description TODO
 * @Author wangjin
 * @Date 2023/9/7 14:19
 */

//生产者
class Product implements Runnable{
    private Clerk clerk;

    public Product(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 15; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.add();
        }
    }
}
 //消费者
class Customer implements Runnable{
     private Clerk clerk;

     public Customer(Clerk clerk) {
         this.clerk = clerk;
     }

     @Override
     public void run() {
         for (int i = 0; i < 15; i++) {
             clerk.sale();
         }
     }

 }
public class ClerkDemo {
    public static void main(String[] args) {
        Clerk clerk = new Clerk();

        //开启线程
        Thread threadA = new Thread(new Product(clerk), "生产者A");
        threadA.start();
        Thread threadB = new Thread(new Product(clerk), "生产者B");
        threadB.start();
        Thread threadC = new Thread(new Customer(clerk), "消费者C");
        threadC.start();
        Thread threadD = new Thread(new Customer(clerk), "消费者D");
        threadD.start();

        //测试线程状态
        while(true){
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("threadA的状态 = " + threadA.getState());
            System.out.println("threadB的状态 = " + threadB.getState());
            System.out.println("threadC的状态 = " + threadC.getState());//WAITING 无限等待
            System.out.println("threadD的状态 = " + threadD.getState());//WAITING 无限等待
        }

    }
}
