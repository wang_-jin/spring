package com.igeek_01.ch02.state;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/9/7 14:18
 */
public class Clerk {
    //库存 最多只能有10件
    private int num = 0;

    //补货  对象锁就是this
    public synchronized void add(){
        //测试一个临界值：库存只有一件
        while(num>=1){
            System.out.println("仓库已满....");
            //一旦仓库满10件商品 就不允许生产者进行生产 只能进入等待状态
            try {
                this.wait();//必须通过对象锁来调用的
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName()+"生产了"+(++num));
        //一旦生产了 将消费者唤醒 进行消费
        this.notifyAll();
    }
    //售卖 对象锁就是this
    public synchronized void sale(){
        while(num<=0){
            System.out.println("仓库已空....");
            //一旦仓库空了，就不允许消费者进行消费 只能进入等待状态
            try {
                this.wait();//消费者B D 一开始都处于等待状态
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName()+"消费了"+(num--));
        //一旦消费了 将生产者唤醒 进行生产
        this.notifyAll();
    }
}
