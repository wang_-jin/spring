package com.igeek_01.ch02.tongxun.sync;

/**
 * @author wangjin
 * 2023/9/7 11:34
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //共同资源
        Clerk clerk = new Clerk();
        new Thread(new Product(clerk),"生产者1").start();
        new Thread(new Product(clerk),"生产者2").start();
        new Thread(new Customer(clerk),"消费者1").start();
        new Thread(new Customer(clerk),"消费者2").start();
    }
}
