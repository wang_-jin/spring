package com.igeek_01.ch02.tongxun.sync;

/**
 * @author wangjin
 * 2023/9/7 11:32
 * @description 售货机
生产者和消费者模型：
 * 1.线程通讯是一种等待唤醒机制
 * 2.线程通讯一定是以多条线程并发访问共同资源为前提
 * 3.确保线程安全的下，进行线程通讯，否则没有任何意义
 * 4.锁对象调用wait()和notifyAll()，必须搭配synchronized一起使用，若不搭配则会出现IllegalMonitorStateException异常
 * 5.可能导致虚假唤醒的问题，必须在循环中使用
 * 6.线程通讯必须确保对象锁是唯一的
 *
 * 解决 虚假唤醒的问题：
 * 判断条件 必须放在while循环里，万一进入等待状态后，被唤醒时，可以再次进行条件判断是否满足，只有条件满足才能继续往下执行
 */
public class Clerk {
    private int num = 0;
    //当num为1时仓库满
    public synchronized void add(){
        while (num>=10){
            try {
                System.out.println("仓库已满!");
                //仓库满了生产者不进行生产,进入等待状态
                this.wait();//必须通过对象锁来调用
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName()+"生产了"+(++num));
        //一旦生产者开始生产,消费者就要进入唤醒状态
        this.notifyAll();
    }
    public synchronized void sale(){
        while(num<=0){
            System.out.println("仓库已空!");
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName()+"消费了"+(num--));
        this.notifyAll();
    }
}
