package com.igeek_01.ch02.tongxun.sync;

/**
 * @author wangjin
 * 2023/9/7 11:34
 * @description TODO
 */
public class Customer implements Runnable{
    private Clerk clerk;

    public Customer(Clerk clerk) {
        this.clerk=clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            clerk.sale();
        }
    }
}
