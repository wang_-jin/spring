package com.igeek_01.ch02.tongxun.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/7 11:32
 * @description 线程通讯 -- 使用Lock
生产者和消费者模型：
 * 1.线程通讯是一种等待唤醒机制
 * 2.线程通讯一定是以多条线程并发访问共同资源为前提
 * 3.确保线程安全的下，进行线程通讯，否则没有任何意义
 * 4.Condition对象调用await()和singalAll()，必须搭配lock锁一起使用，若不搭配则会出现IllegalMonitorStateException异常
 * 5.可能导致虚假唤醒的问题，必须在循环中使用
 * 6.线程通讯必须确保Lock锁是唯一的
 *
 *
 */
public class Clerk {
    //创建锁
    private static Lock lock = new ReentrantLock();
    //创建Condition实例
    private static Condition condition = lock.newCondition();
    private int num = 0;
    public void add(){
        lock.lock();
        try {
            while (num >= 10) {
                System.out.println("仓库已满!");
                condition.await();
            }
            System.out.println(Thread.currentThread().getName() + "生产了" + (++num));
            condition.signalAll();
            }catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

    public void sale(){
        lock.lock();
        try{
            while(num<=0){
                System.out.println("仓库空了");
                condition.await();
            }
            System.out.println(Thread.currentThread().getName() + "消费了" + (num--));
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
