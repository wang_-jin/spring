package com.igeek_01.ch02.tongxun.lock;

/**
 * @author wangjin
 * 2023/9/7 11:33
 * @description TODO
 */
public class Product implements Runnable{
    private Clerk clerk;
    public Product (Clerk clerk){
        this.clerk = clerk;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            clerk.add();
        }
    }
}
