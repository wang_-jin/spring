package com.igeek_01.ch02.syncAndlock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/8 18:46
 * @description 公平锁 和 非公平锁
 * 公平锁： 按等待获取锁的线程的等待时间进行获取，等待时间长的具有优先获取锁权利
 * synchronized 非公平锁
 * lock         通过new ReentrantLock() 非公平锁
 *              通过new ReentrantLock(true) 公平锁
 *
 * 非公平锁：
 *      NonfairSync
 *      先占先得非公平锁，只要获得同步状态就可以了
 *      最大的缺点：锁饥饿现象
 *      特点：为了实现高吞吐量，选择非公平锁，节省线程的切换的开销
 * 公平锁：
 *      FairSync
 *      按序排队公平锁，就是判断同步队列是否还有先驱节点的存在，如果没有先驱节点才能获取锁；
 *      特点：雨露均沾
锁饥饿现象（Lock starvation）是指在并发编程中，由于锁的竞争和使用不当，导致某些线程长时间无法获取到所需的锁资源，从而无法继续执行的现象。这会导致这些线程处于等待状态，无法完成任务，造成系统性能下降。

锁饥饿现象通常发生在以下情况下：

锁的使用顺序不当：如果线程获取锁的顺序不一致，可能会导致死锁或锁饥饿现象。
锁的粒度过大：如果一个锁被多个线程频繁竞争，那么其他线程可能会长时间等待，无法获取到锁资源。
锁的等待超时设置不合理：如果等待获取锁的时间设置过长，会导致其他线程长时间等待。
解决锁饥饿现象的方法主要有：

合理设计锁的粒度：将大锁拆分成小锁，减少锁的竞争。
公平锁机制：采用公平锁可以保证线程按照请求的顺序获取锁资源，避免某些线程长时间等待。
优化锁的等待超时设置：根据实际情况，合理设置等待获取锁的时间，避免过长等待。
以上是关于锁饥饿现象的简要介绍和解决方法。如果有更具体的问题或者需要更详细的讨论，请提供更多信息。
 */
class Ticket{
    private static Lock lock = new ReentrantLock();//非公平锁
    private int num = 30;
    public void sale(){
        lock.lock();

            try {
                if(num>=1) {
                    System.out.println(Thread.currentThread().getName() + "买到了票" + num);
                    num--;
                }
/*                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();*/
            }finally {
                lock.unlock();
            }


    }
}
public class FairDemo {
    public static void main(String[] args) {

        Ticket ticket = new Ticket();
        new Thread(()->{
            for (int i = 0; i < 20; i++) {
                ticket.sale();
            }
        },"线程1").start();

        new Thread(()->{
            for (int i = 0; i < 20; i++) {
                ticket.sale();
            }
        },"线程2").start();

        new Thread(()->{
            for (int i = 0; i < 20; i++) {
                ticket.sale();
            }
        },"线程3").start();

    }
}
