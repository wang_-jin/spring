package com.igeek_01.ch02.syncAndlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/7 11:36
 * @description synchronized 和 Lock 区别
synchronized 和 Lock 区别
 * 1.存在层次上，
 *      synchronized是Java的关键字，在JVM层面上；
 *      Lock是一个接口。
 * 2.锁的释放上，
 *      synchronized
 *            - 获取锁的线程执行完同步代码，则直接释放锁
 *            - 线程执行同步代码时发生异常，则JVM会让线程释放锁
 *      Lock锁
 *            - 在finally中调用unlock()进行释放锁，确保加锁次数和释放次数一样，不然很容易造成线程死锁
 * 3.锁的获取上，
 *      synchronized 获取锁，线程A持有对象锁，线程B在等待对象锁， 此时线程A执行任务出现sleep阻塞不释放对象锁，线程B则会一直进入等待状态；
 *      Lock锁 ， 可以通过tryLock()尝试获取锁，获取不到则不会一直等待。
 * 4.锁的状态上，
 *      synchronized 不可判断锁的状态；
 *      Lock锁     可判断锁的状态。
 * 5.锁的类型上，
 *      synchronized 可重入锁、不可中断锁、非公平锁；
 *      Lock锁   可重入锁、可中断锁、可公平锁也可非公平锁（构造方法传入true/false）
 * 6.锁的性能上，
 *      synchronized  同步代码块，支持同步少量的代码；
 *      Lock锁  支持同步大量代码
 * 7.JDK1.6已经优化了synchronized
 *      - 提出了锁升级的机制，即无锁->偏向锁->轻量级锁->重量级锁
 *      锁类型(按锁的状态分类) non-biasable无锁且不可偏向 、biasable无锁可偏向、biased偏向锁、thin lock轻量级锁、fat lock重量级锁
 *      - synchronized是JVM亲生的
 *
 * 名词介绍：
 * 可重入锁：在执行对象中所有同步方法不用再次获得锁
 * 可中断锁：在等待获取锁过程中可中断
 * 公平锁： 按等待获取锁的线程的等待时间进行获取，等待时间长的具有优先获取锁权利
 * 读写锁：对资源读取和写入的时候拆分为2部分处理，读的时候可以多线程一起读，写的时候必须同步地写
 */
public class SyncAndLockDemo {
    private final Lock lock = new ReentrantLock();
    public void test1(){
        if (lock.tryLock()){
                System.out.println(Thread.currentThread().getName()+"执行了");

        }else {
            System.out.println(Thread.currentThread().getName()+"未执行");
        }
    }

    public static void main(String[] args) {
        SyncAndLockDemo sa = new SyncAndLockDemo();
        new Thread(()->{
            sa.test1();
        },"线程1").start();
        new Thread(()->{
            sa.test1();
        },"线程2").start();
    }
}
