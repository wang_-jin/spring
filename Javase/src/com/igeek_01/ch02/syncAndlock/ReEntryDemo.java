package com.igeek_01.ch02.syncAndlock;

import com.igeek.ch09.io_test.serializable.Person;
import sun.awt.windows.ThemeReader;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/7 13:27
 * @description 可重入锁
 * 可重入锁：在执行对象中所有同步方法不用再次获得锁
 * 1.是指在同一个线程在外层方法获取锁的时候，再进入该线程的内层方法会自动获取锁(前提，锁对象得是同一个对象)
 * 2.Java中ReentrantLock和synchronized都是可重入锁，可重入锁的一个优点是可一定程度避免死锁。
 *
 * 进入什么？
 * 进入同步域（即同步代码块/方法或显式锁锁定的代码）
 * 一个线程中的多个流程可以获取同一把锁，持有这把同步锁可以再次进入。
 * 自己可以获取自己的内部锁。
 */
public class ReEntryDemo {
    //隐式锁 同步方法
    public synchronized void test1(){
        System.out.println("执行test1方法");
        test2();
        /*在同一个类中，可以直接调用该类的其他方法，而不需要使用类名来引用。这是因为在同一个类的方法之间进行调用时，
        默认会使用当前实例对象来引用方法。所以，在test1()方法中直接写test2()就可以调用test2()方法，
        而不需要使用类名来引用。*/
    }
    public synchronized void test2(){
        System.out.println("执行test2方法");
    }

    final Object objectA = new Object();
    //隐式锁 同步代码块  对象锁objectA
    public void method1(){
        synchronized (objectA){
            System.out.println("---外层调用----");
            synchronized (objectA){
                System.out.println("---中层调用----");
                synchronized (objectA){
                    System.out.println("---内层调用---");
                }
            }
        }
    }

    //显式锁
    private final Lock lock = new ReentrantLock();
    public void method2(){
        try {
            lock.lock();
            System.out.println("---method2---1----");
            try {
                lock.lock();
                System.out.println("---method2---2----");
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
                //lock一定在finally中关闭
                //加锁和释放锁的次数一定要相同，否则可能会导致第二个线程始终获取不到锁，一直处于等待状态==>死锁
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void method3(){
        try {
            lock.lock();
            System.out.println("method3执行");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
//        new ReEntryDemo().test1();

//        new ReEntryDemo().method1();

        ReEntryDemo demo = new ReEntryDemo();
        new Thread(()->demo.method2(),"线程a").start();
        new Thread(()->demo.method3(),"线程b").start();
    }
}
