package com.igeek_01.ch02.pool.mypool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description 自定义线程池-拒绝策略
 * @Author fqq
 * @Date 2023/9/8 10:41
 *
 * 执行拒绝策略的前提条件：一旦任务数> maxPoolSize+queueSize
 *
 * 拒绝策略：
 * 1.AbortPolicy    默认策略，直接拒绝，抛出异常
 * 2.DiscardPolicy  直接拒绝，不抛出异常
 * 3.DiscardOldestPolicy  直接丢弃等待最久的任务，将最新的任务加入到阻塞队列中
 * 4.CallerRunsPolicy  由当前线程执行任务时，所在的线程来执行此任务
 */
public class MyThreadPool2 {

    public static void main(String[] args) {
        ThreadPoolExecutor pool = new ThreadPoolExecutor(
                2,
                3,
                30L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(2),
                new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 1; i <= 6 ; i++) {
            pool.execute(new MyRunnable(i+""));
        }

        pool.shutdown();

        /**
         * AbortPolicy
         * 任务1   pool-1-thread-1
         * 任务2   pool-1-thread-2
         * 任务3   进入queue
         * 任务4   进入queue
         * 任务5   创建1条线程（maxPoolsize-corePoolsize）
         * 任务6   直接拒绝，抛出异常
         *
         *
         * DiscardPolicy
         * 任务1   pool-1-thread-1
         * 任务2   pool-1-thread-2
         * 任务3   进入queue
         * 任务4   进入queue
         * 任务5   创建1条线程（maxPoolsize-corePoolsize）
         * 任务6   直接拒绝，不抛出异常
         *
         *DiscardOldestPolicy
         * 任务1   pool-1-thread-1
         * 任务2   pool-1-thread-2
         * 任务3   进入queue
         * 任务4   进入queue
         * 任务5   创建1条线程（maxPoolsize-corePoolsize）
         * 任务6   丢弃等待最久的任务3，将任务6加入到queue
         *
         * CallerRunsPolicy
         * 任务1   pool-1-thread-1
         * 任务2   pool-1-thread-2
         * 任务3   进入queue
         * 任务4   进入queue
         * 任务5   创建1条线程（maxPoolsize-corePoolsize）
         * 任务6   main主线程执行
         */
    }
}
