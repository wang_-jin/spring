package com.igeek_01.ch02.pool.mypool;

import java.util.concurrent.TimeUnit;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/9/8 10:22
 */
public class MyRunnable implements Runnable {
    private String name;

    public MyRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"正在执行任务"+name+".....");
    }
}
