package com.igeek_01.ch02.pool.mypool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description 自定义的线程池  形参的配置
 * @Author fqq
 * @Date 2023/9/8 9:44
 *
 * ThreadPoolExecutor 类  一般用于配置自定义线程池
 *
 *
 * 自定义线程池
 * ThreadPoolExecutor
 * 1.构造方法
 * public ThreadPoolExecutor(int corePoolSize,
 *                           int maximumPoolSize,
 *                           long keepAliveTime,
 *                           TimeUnit unit,
 *                           BlockingQueue<Runnable> workQueue,
 *                           ThreadFactory threadFactory,
 *                           RejectedExecutionHandler handler)
 * 参数一：
 * corePoolSize
 * 线程池中的核心线程数，当提交一个任务时，线程池创建一个新线程执行任务，直到当前线程数等于corePoolSize;
 * 如果当前线程数为corePoolSize，继续提交的任务被保存到阻塞队列中，等待被执行;
 * 如果执行了线程池的prestartAllCoreThreads()方法，线程池会提前创建并启动所有核心线程。
 *
 * 参数二：
 * maximumPoolSize
 * 线程池中允许的最大线程数。
 * 如果当前阻塞队列满了，且继续提交任务，则创建新的线程执行任务，前提是当前线程数小于maximumPoolSize;
 *
 * 参数三：
 * keepAliveTime
 * 线程池维护线程所允许的空闲时间。
 * 当线程池中的线程数量大于corePoolSize的时候，如果这时没有新的任务提交，核心线程外的线程不会立即销毁，而是会等待，直到等待的时间超过了keepAliveTime;
 *
 * 参数四：
 * unit
 * keepAliveTime的单位;
 *
 * 参数五：
 * workQueue
 * 用来保存等待被执行的任务的阻塞队列，且任务必须实现Runnable接口，
 * 在JDK中提供了如下阻塞队列:ArrayBlockingQueue、LinkedBlockingQueue、SynchronousQueue、PriorityBlockingQueue
 *
 * 参数六：
 * threadFactory
 * 它是ThreadFactory类型的变量。用来创建新线程。
 * 默认使用Executors.defaultThreadFactory()来创建线程。使用默认的ThreadFactory来创建线程时，
 * 会使断创建的线程具有柑同的NORM PRIORITY优先级并巨是非守护线程。同时也设普了线程的名称。
 *
 * 参数七：
 * handler
 * 线程池的饱和策略，当阻塞队列满了，且没有空闲的工作线程，如果继续提交任务，必须采取一种策略处理该任务，
 * 线程池提供了4种策略:AbortPolicy、CallerRunsPolicy、DiscardOldestPolicy、DiscardPolicy
 * 上面的4种策略都是ThreadPoolExecutor的内部类。
 * 当然也可以根据应用场景实现RejectedExecutionHandler接口，自定义饱和策略，如记录日志或持久化存储不能处理的任务。

 * 面试题 - 常见阻塞队列：用来保存等待执行任务的队列
 * 1.ArrayBlockingQueue     基于数组结构的有界阻塞队列,按FIFO排序任务
 * 2.LinkedBlockingQueue    基于链表结构的阻塞队列，按FIFO排序任务。吞吐量通常要高于ArrayBlockingQueue
 * 3.SynchronousQueue       一个不存储元素的阻塞队列，每个插入操作必须等到另一个线程调用移除操作，否则插入操作一直处于阻塞状态，吞吐量通常要高于LinkedBlockingQueue
 * 4.PriorityBlockingQueue  具有优先级的无界阻塞队列
 */
public class MyThreadPool {
    //线程池
    private ThreadPoolExecutor pool;

    //自定义线程池的配置

    /**
     * int corePoolSize,                    核心线程数量，在线程池中允许保留的线程数量   1
     * int maximumPoolSize,                 在线程池中允许存在的最大线程数量(当任务数量>corePoolSize+queueSize才会创建剩余的线程)  5
     * long keepAliveTime,                  最大非活动时间，当池子中线程数量大于corePoolSize 这是多余的空闲线程等待新任务终止前的最大时间，超过该时间还没有新任务则销毁线程 最多销毁5-1=4条
     * TimeUnit unit,                       keepAliveTime的时间单位
     * BlockingQueue<Runnable> workQueue,   阻塞队列，将未执行的任务放置在队列中。queueSize阻塞队列能存放的任务数量   50
     * RejectedExecutionHandler handler     拒绝策略，一旦任务数量> maxPoolSize+queueSize 就会执行拒绝策略
     */
    public MyThreadPool(int corePoolSize,int maxPoolSize,int queueSize) {
        this.pool = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(queueSize),
                new ThreadPoolExecutor.AbortPolicy()); //默认拒绝策略：一旦任务数量> maxPoolSize+queueSize 就会直接拒绝任务并且抛出异常
    }

    //定义一个执行任务的方法
    public void execute(Runnable task){
        pool.execute(task);
    }

    public static void main(String[] args) {
        //创建自定义线程池
        MyThreadPool pool = new MyThreadPool(2, 5, 4);

        //执行任务
        pool.execute(new MyRunnable("a"));//pool-1-thread-1  corePoolsize执行任务
        pool.execute(new MyRunnable("b"));//pool-1-thread-2  corePoolsize执行任务
        pool.execute(new MyRunnable("c"));//进入queue
        pool.execute(new MyRunnable("d"));//进入queue
        pool.execute(new MyRunnable("e"));//进入queue
        pool.execute(new MyRunnable("f"));//进入queue
        pool.execute(new MyRunnable("g"));//queue满了  则创建一条新线程
/*        pool.execute(new MyRunnable("h"));//queue满了  则创建一条新线程
        pool.execute(new MyRunnable("k"));//queue满了  则创建一条新线程
        pool.execute(new MyRunnable("m"));//直接拒绝，抛出异常RejectedExecutionException*/

    }

    public static void main1(String[] args) {
        MyThreadPool pool = new MyThreadPool(2, 3, 3);

        /*
        任务1 corePoolSize 执行
        任务2 corePoolSize 执行
        任务3 进入阻塞队列
        任务4 进入阻塞队列
        任务5 进入阻塞队列
        任务6 创建新线程
        任务7 直接拒绝，抛出异常
         */
        for (int i = 1; i <=7 ; i++) {
            pool.execute(new MyRunnable(""+i));
        }
    }
}
