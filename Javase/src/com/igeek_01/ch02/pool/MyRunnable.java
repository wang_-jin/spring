package com.igeek_01.ch02.pool;

/**
 * @author wangjin
 * 2023/9/9 13:32
 * @description TODO
 */
public class MyRunnable implements Runnable{

    @Override
    public void run() {
        //获取当前线程名字
        System.out.println(Thread.currentThread().getName());
    }
}
