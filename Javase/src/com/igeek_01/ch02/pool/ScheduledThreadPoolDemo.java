package com.igeek_01.ch02.pool;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/9 14:08
 * @description 创建可执行周期任务的线程池
静态工厂方法：
 * public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize)
 * 创建一个线程池，可以调度命令在一个给定的延迟后运行，或周期性地执行。
 *
 * 创建一个大小无限的线程池。此线程池支持定时以及周期性执行任务的需求。
 */
public class ScheduledThreadPoolDemo {
    public static void main(String[] args) {
        //创建线程池
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);

        //周期性执行任务
        //每隔一秒打印当前时间
        /*
        scheduleAtFixedRate ：
            参数 Runnable command, 周期性做的任务
            参数 long initialDelay, 执行时延迟时间
            参数 long period,       执行定时任务的周期时间
            参数 TimeUnit unit      时间单位，秒，分钟...
         */
       /* pool.scheduleAtFixedRate(()->{
            //获取当前时间       JDK8线程安全的时间类：LocalDate LocalDateTime LocalTime
//            Date date = new Date();
//            System.out.println("date = " + date);

            LocalDateTime date = LocalDateTime.now();
            String str = date.format(DateTimeFormatter.ofPattern("yyyy年yy月dd日"));
            System.out.println("date = " + date);
            System.out.println("str = " + str);

            System.out.println("==================================");
        },1,1, TimeUnit.SECONDS);*/

        //方式二：执行周期性任务
        /*
            Timer类：void schedule(TimerTask task, long delay, long period)
            计划重复固定延迟执行指定的任务，在指定的延迟后开始。

            参数 TimerTask task  周期性执行的任务    abstract class TimerTask implements Runnable
            参数 long delay      执行时延迟时间 以毫秒为单位
            参数 long period     执行定时任务的周期时间  以毫秒为单位
         */
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                LocalDateTime now = LocalDateTime.now();
                System.out.println(now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                System.out.println("----------------------------");
            }
        },1000,1000);
    }
}
