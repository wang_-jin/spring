package com.igeek_01.ch02.pool;

/**
 * @author wangjin
 * 2023/9/9 13:26
 * @description 线程池
使用线程池的好处：
 * 1. 降低资源消耗。减少了创建和销毁线程的次数，每个工作线程都可以被重复利用，可执行多个任务。
 * 2. 提高响应速度。当任务到达时，任务可以不需要的等到线程创建就能立即执行。
 * 3. 提高线程的可管理性。可以根据系统的承受能力，调整线程池中工作线线程的数目，
防止因为消耗过多的内存，而把服务器累趴下(每个线程需要大约1MB内存，线程开的越多，消耗的内存也就越大，最后死机)。

Executors  线程池的工具类
常用的线程池：
public static ExecutorService newSingleThreadExecutor() 创建单线程的线程池
public static ExecutorService newFixedThreadPool(int nThreads) 创建固定数量线程的线程池
public static ExecutorService newCachedThreadPool() 创建可缓存的线程池
public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize) 创建可执行周期任务的线程池
自定义线程池：
ThreadPoolExecutor 是ExecutorService的实现类，一般用于自定义线程池


ExecutorService  线程池的接口
常用方法：
Future<?> submit(Runnable task) 执行线程任务
Future<T> submit(Callable<T> task)
void shutdown()   关闭线程池

Executor    线程池的父接口
void execute(Runnable command) 执行线程任务

ScheduledExecutorService 是ExecutorService的子接口。执行定时任务的线程池的接口
 */
public class PoolDemo {
}
