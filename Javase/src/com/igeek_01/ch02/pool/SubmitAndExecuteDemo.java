package com.igeek_01.ch02.pool;

import java.util.concurrent.*;

/**
 * @author wangjin
 * 2023/9/9 15:09
 * @description submit() 和 execute()的区别
 * submit() 和 execute()
 * 1.两者都是执行线程任务
 *
 * 2.区别：
 * submit() 是ExecutorService  线程池的接口
 * 1.Future<?> submit(Runnable task)  能传入Runnable任务，返回值为null
 * 2.Future<T> submit(Callable<T> task) 还能传入Callable任务，返回值是Future，通过Future的get()方法获取线程执行结果
 * 3.submit()对于出现异常的处理更加友善，实现Callable接口的call()方法时可以向外抛出异常，
 *   如果一旦线程池中执行任务时，若遇到异常则通过Future的get()方法抛出异常就可以在主函数捕获到异常，从而阻止执行任务时后续代码的执行。
 *
 * execute() 是Executor    线程池的父接口
 * 1.void execute(Runnable command) 只能传入Runnable任务，没有返回值
 */
class MyRun implements Runnable{
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }
}
class MyCall implements Callable<String> {

    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return Thread.currentThread().getName()+"====aaa";
    }
}
public class SubmitAndExecuteDemo {
    public static void main(String[] args) {
        //创建固定大小线程数量的线程池
        ExecutorService pool = Executors.newFixedThreadPool(1);
        //execute只能传入Runnable任务，没有返回值
//        pool.execute(new MyRun());//pool-1-thread-1
//        pool.execute(new MyRun());//pool-1-thread-1
//        pool.execute(new MyRun());//pool-1-thread-1

        try {
            //submit能传入Runnable任务，返回值为null
//            Future<?> f1 = pool.submit(new MyRun());
//            System.out.println("f1.get() = " + f1.get());
//
//            Future<?> f2 = pool.submit(new MyRun());
//            System.out.println("f2.get() = " + f2.get());

            //submit还能传入Callable任务，返回值是Future
            Future<String> f1 = pool.submit(new MyCall());
            System.out.println("f1.get() = " + f1.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        pool.shutdown();
    }
}

