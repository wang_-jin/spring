package com.igeek_01.ch02.pool;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wangjin
 * 2023/9/9 13:27
 * @description newSingleThreadExecutor 创建单线程的线程池
 * newSingleThreadExecutor
 * 创建一个单线程的线程池。这个线程池只有一个线程在工作，也就是相当于单线程串行执行所有任务。
 * 如果这个唯一的线程因为异常结束，那么会有一个新的线程来替代它。
 * 此线程池保证所有任务的执行顺序按照任务的提交顺序执行。
 *
 * 静态工厂方法：
 * static ExecutorService newSingleThreadExecutor()
 * 使用一个单一的工作线程操作关闭一个无限的队列。
 */
public class SingleThreadExecutorDemo {
    public static void main(String[] args) {
        //创建单线程的线程池
        ExecutorService pool = Executors.newSingleThreadExecutor();

        //让线程池中的线程执行任务
        pool.execute(new MyRunnable()); //pool-1-thread-1  corePoolSize 执行
        pool.execute(new MyRunnable()); //pool-1-thread-1  阻塞队列中
        pool.execute(new MyRunnable()); //pool-1-thread-1  阻塞队列中
        pool.execute(new MyRunnable()); //pool-1-thread-1  阻塞队列中
        //关闭线程池
        pool.shutdown();
    }
}
