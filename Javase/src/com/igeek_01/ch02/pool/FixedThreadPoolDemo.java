package com.igeek_01.ch02.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wangjin
 * 2023/9/9 13:52
 * @description TODO
 */
public class FixedThreadPoolDemo {
    public static void main(String[] args) {
        //创建线程池
        ExecutorService pool = Executors.newFixedThreadPool(3);
        //执行任务
        pool.submit(new MyRunnable());
        pool.submit(new MyRunnable());
        pool.submit(new MyRunnable());

        pool.execute(new MyRunnable());
        pool.execute(new MyRunnable());
        //关闭
        pool.shutdown();
    }
}
