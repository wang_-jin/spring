package com.igeek_01.ch02.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description 创建可缓存的线程池
 * @Author fqq
 * @Date 2023/9/7 15:50
 *
 *
 * 静态工厂方法：
 * public static ExecutorService newCachedThreadPool()
 * 创建一个线程池，根据需要创建新的线程，但在可用时将重用先前构建的线程。
 *
 * 创建一个可缓存的线程池。
 * 如果线程池的大小超过了处理任务所需要的线程,
 * 那么会回收部分空闲（60秒不执行任务）的线程，当任务数增加时,
 * 此线程池又可以智能的添加新线程来处理任务。此线程池不会对线程池大小做限制，
 * 线程池大小完全依赖于操作系统（或者说JVM）能够创建的最大线程大小。
 */
public class CachedThreadPoolDemo {
    public static void main(String[] args) {
        //创建线程池
        ExecutorService pool = Executors.newCachedThreadPool();
        //执行任务
        pool.execute(new MyRunnable());//pool-1-thread-1
        pool.execute(new MyRunnable());//pool-1-thread-2
        pool.execute(new MyRunnable());//pool-1-thread-3
        pool.execute(new MyRunnable());//pool-1-thread-4
        pool.execute(new MyRunnable());//pool-1-thread-5
        //关闭
        pool.shutdown();
    }
}
