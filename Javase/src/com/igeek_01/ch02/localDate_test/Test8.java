package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;

/**
 * @author wangjin
 * 2023/9/10 23:51
 * @description TODO
 */
public class Test8 {
    public static void main(String[] args) {
        /*8.请问今天是今年的第多少天？*/
        LocalDate day = LocalDate.now();
        int dayOfYear = day.getDayOfYear();
        System.out.println(dayOfYear);
    }
}
