package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;

/**
 * @author wangjin
 * 2023/9/10 23:51
 * @description TODO
 */
public class Test9 {
    /*9.输入任意一个年份，得到这年的2月份有几天？*/
    public static void main(String[] args) {
        LocalDate day = LocalDate.parse("2023-03-01");
        LocalDate dayNum = day.withMonth(2);
        int i = dayNum.withMonth(dayNum.getMonthValue()).lengthOfMonth();
        System.out.println("这年的2月份有:"+i);
    }
}
