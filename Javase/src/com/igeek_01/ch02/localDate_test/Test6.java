package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/10 23:51
 * @description TODO
 */
class News{
    String title;
    String content;
    LocalDate time;

    public News(String title, String content, LocalDate time) {
        this.title = title;
        this.content = content;
        this.time = time;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", time=" + time +
                '}';
    }
}
public class Test6 {
    /*6.新闻类：标题，内容，时间   三个属性。 有10条新闻，按照时间的降序排列。*/
    public static void main(String[] args) {
        List<News>list = new ArrayList<>();

        list.add(new News("新闻1","内容1",LocalDate.of(2006,2,1)));
        list.add(new News("新闻2","内容2",LocalDate.of(2001,2,1)));
        list.add(new News("新闻3","内容3",LocalDate.of(2602,2,1)));
        list.add(new News("新闻4","内容4",LocalDate.of(2005,2,1)));
        list.add(new News("新闻5","内容5",LocalDate.of(2004,2,1)));
        list.add(new News("新闻6","内容6",LocalDate.of(2040,2,1)));
        list.add(new News("新闻7","内容7",LocalDate.of(2050,2,1)));
        list.add(new News("新闻8","内容8",LocalDate.of(2000,2,1)));
        list.add(new News("新闻9","内容9",LocalDate.of(2010,2,1)));
        list.add(new News("新闻10","内容10",LocalDate.of(2020,2,1)));
        Collections.sort(list,(o1, o2) -> o2.time.compareTo(o1.time));
        for (News news : list) {
            System.out.println(news);
        }

    }
}
