package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

/**
 * @author wangjin
 * 2023/9/10 23:50
 * @description TODO
 */
public class Test5 {
    public static void main(String[] args) {
        /*5.找出下个月的倒数第3天是哪天*/
        LocalDate nextDate = LocalDate.now().plusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
        LocalDate day = nextDate.minusDays(3);
        System.out.println(day);
    }
}
