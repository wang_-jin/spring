package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;

/**
 * @author wangjin
 * 2023/9/10 23:50
 * @description TODO
 */
public class Test4 {
    /*4.求上个月的第三天是星期几？*/
    public static void main(String[] args) {
        LocalDate lastMonth = LocalDate.now().minusMonths(1);
        LocalDate day = lastMonth.withDayOfMonth(3);
        System.out.println(day.getDayOfWeek());
    }
}
