package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author wangjin
 * 2023/9/10 23:50
 * @description TODO
 */
public class Test1 {
    /*1.计算出生日期至今的天数*/
    public static void main(String[] args) {
        LocalDate bir = LocalDate.parse("2000-02-18");
        LocalDate now = LocalDate.now();
        long between = ChronoUnit.DAYS.between(bir,now);
        System.out.println("出生日期至今的天数为:"+between);
    }
}
