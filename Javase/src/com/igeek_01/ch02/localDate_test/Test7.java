package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/10 23:51
 * @description TODO
 */
class Stu{
    String name;
    int id;
    LocalDate bir;

    public Stu(String name, int id, LocalDate bir) {
        this.name = name;
        this.id = id;
        this.bir = bir;
    }

    @Override
    public String toString() {
        return "Stu{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", bir=" + bir +
                '}';
    }
}
public class Test7 {
    /*7.学生类：姓名，学号，出生日期  三个属性。找出比“张三”大的所有同学信息。*/
    public static void main(String[] args) {
        List<Stu>list = new ArrayList<>();
        List<Stu>list1 = new ArrayList<>();
        list.add(new Stu("张三",123,LocalDate.of(2001,3,4)));
        list.add(new Stu("李四",121,LocalDate.of(2000,3,6)));
        list.add(new Stu("王五",133,LocalDate.of(2001,1,4)));
        LocalDate targetBir = null;
        for (Stu stu : list) {
            if (stu.name.equals("张三")){
                targetBir = stu.bir;
            }
        }
        for (Stu stu : list) {
            if (stu.bir.isBefore(targetBir)){
                list1.add(stu);
            }
        }
        System.out.println(list1);
    }
}
