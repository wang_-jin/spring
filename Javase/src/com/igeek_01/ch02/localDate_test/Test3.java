package com.igeek_01.ch02.localDate_test;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.time.LocalDate;

/**
 * @author wangjin
 * 2023/9/10 23:50
 * @description TODO
 */
public class Test3 {
    public static void main(String[] args) {
        /*3.一批食品的生产日期是：2022-5-10，保质期：180天，求过期时间*/
        LocalDate product = LocalDate.parse("2022-05-10");
        LocalDate overDay = product.plusDays(180);
        System.out.println(overDay);
    }


}
