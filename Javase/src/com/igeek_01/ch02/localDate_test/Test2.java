package com.igeek_01.ch02.localDate_test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author wangjin
 * 2023/9/10 23:50
 * @description TODO
 */
public class Test2 {
    /*2.当前日期三天后的日期是 (格式： 2022年10月29日 10:10:10 )*/
    public static void main(String[] args) {
        LocalDateTime toDay = LocalDateTime.now();
        LocalDateTime time = toDay.plusDays(3);
        DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
        System.out.println("三天后的日期:"+time.format(dft));
    }
}
