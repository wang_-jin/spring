package com.igeek_01.ch02.AccountTest.sync;


/**
 * @author wangjin
 * 2023/9/7 20:02
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Account account = new Account();
        new Thread(new WithDrawThread(account),"姐姐").start();
        new Thread(new WithDrawThread(account),"弟弟").start();

        new Thread(new DepositThread(account),"妈妈").start();
        new Thread(new DepositThread(account),"爸爸").start();
    }

}
