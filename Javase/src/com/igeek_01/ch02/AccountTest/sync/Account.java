package com.igeek_01.ch02.AccountTest.sync;

/**
 * @author wangjin
 * 2023/9/7 20:01
 * @description TODO
 */
public class Account {
    private double balance = 10000.0;
    public synchronized void add(){
        try {
        while (balance>0) {
            System.out.println("有余额,不能存!");
            this.wait();
        }
            balance  = balance + 1000;
            System.out.println(Thread.currentThread().getName()+"存了"+(1000));
            this.notifyAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    public synchronized void sale(){
        try {
        while(balance<=0){
            System.out.println("余额为空!");
                this.wait();
        }
            balance = balance - 1000;
            System.out.println(Thread.currentThread().getName()+"消费了"+(1000));
            this.notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
