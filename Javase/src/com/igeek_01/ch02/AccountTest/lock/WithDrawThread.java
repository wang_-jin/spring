package com.igeek_01.ch02.AccountTest.lock;

/**
 * @author wangjin
 * 2023/9/7 20:02
 * @description TODO
 */
public class WithDrawThread implements Runnable{

    private Account account;
    public WithDrawThread (Account account){
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            account.sale();
        }
    }
}
