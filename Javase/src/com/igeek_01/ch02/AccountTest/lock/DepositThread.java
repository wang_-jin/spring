package com.igeek_01.ch02.AccountTest.lock;


import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/7 20:02
 * @description TODO
 */
public class DepositThread implements Runnable {
    private Account account;
    public DepositThread (Account account){
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            account.add();
        }
    }
}
