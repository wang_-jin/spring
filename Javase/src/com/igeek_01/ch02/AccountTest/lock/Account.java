package com.igeek_01.ch02.AccountTest.lock;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/7 20:01
 * @description TODO
 */
public class Account {
    //创建锁
    private static Lock lock = new ReentrantLock();
    //创建Condition实例
    private static Condition condition = lock.newCondition();
    private double balance = 10.0;
    public void add(){

        try {
            lock.lock();
            while (balance > 0) {
                System.out.println("有余额,不能存!");
                condition.await();
            }
            //Random random = new Random();
            //int money = (int)((Math.random()*10));
            balance = balance + 1000;
            System.out.println(Thread.currentThread().getName() + "存了" + (1000));
            condition.signalAll();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }

    public void sale(){

        try{
            lock.lock();
            while(balance<=0){
                System.out.println("余额为空,不能取");
                condition.await();
            }
            //Random random = new Random();
            //int money = (int)((Math.random()*10));
            balance = balance - 1000;
            System.out.println(Thread.currentThread().getName() + "消费了" + (1000));
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
