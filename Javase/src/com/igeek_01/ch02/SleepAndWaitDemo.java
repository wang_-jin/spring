package com.igeek_01.ch02;

import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/7 13:26
 * @description Sleep和wait的区别
 * sleep():
 *1.Thread类的静态方法：static void sleep(long mills)
 * 2.计时等待，等待时间一结束，获取到时间片后，则继续执行
 * 3.不会释放对象锁
 *
 * wait():
 * 1.Object类的成员方法：void wait()/void wait(long mills)
 * 2.wait() 无限等待，直到被其他线程的notifyAll唤醒后，获得对象锁，则继续执行
 *   wait(long mills) 计时等待 要么等待时间到了 要么被唤醒后 ，获得对象锁，则继续执行
 * 3.会释放对象锁
 * 4.wait()必须在线程安全的情况下使用，必须通过锁对象调用wait方法，必须搭配synchronized一起使用。
 *          否则会有IllegalMonitorStateException异常
 */
public class SleepAndWaitDemo {
    public static void main(String[] args) {
        //对象锁
        Object lock = new Object();
        Stream.of("线程1","线程2").forEach(name->new Thread(()->{
            synchronized (lock) {
                System.out.println(Thread.currentThread().getName() + "开始执行了");
                try {
                    //线程沉睡--不会释放对象锁
                    //Thread.sleep(1000);
                    // 计时等待--会释放对象锁
                    lock.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "执行结束了");
            }
        },name).start());

    }

}

//
/*
sleep
线程1开始执行了
线程1执行结束了
线程2开始执行了
线程2执行结束了
*/

/*
wait
线程1开始执行了
线程2开始执行了
线程1执行结束了
线程2执行结束了*/
