package com.igeek_01.ch02.dead;

/**
 * @author wangjin
 * 2023/9/9 16:26
 * @description 死锁
 * 死锁的出现的原因：
 * 1.多线程之间相互持有对方的资源
 * 2.线程同步代码块出现了嵌套
 *
 * 如何排查死锁：
 * 方式一：纯命令 jps -l     查看正在运行的java程序的进程号  2516
 *        jstack 进程编号    jstack 2516
 *
 * 方式二：打开cmd终端  输入  jconsole
 *
 * 如何解决：
 * 1.尽量不要让多线程 相互持有对方的资源
 * 2，线程同步代码块 尽量不要出现了嵌套
 */
class PersonA extends Thread{
    private Object smoke = new Object();
    private Object lighter = new Object();
    public PersonA (String name, Object smoke, Object lighter){
        super(name);
        this.lighter = lighter;
        this.smoke = smoke;
    }

    @Override
    public void run() {
        synchronized (smoke){
            System.out.println(Thread.currentThread().getName()+"抢到了香烟");
            synchronized (lighter){
                System.out.println(Thread.currentThread().getName()+"可以嗨起来了");
            }
        }
    }
}
class PersonB extends Thread{

    private Object smoke = new Object();
    private Object lighter = new Object();
    public PersonB (String name, Object smoke, Object lighter){
        super(name);
        this.lighter = lighter;
        this.smoke = smoke;
    }

    @Override
    public void run() {
        synchronized (lighter){
            System.out.println(Thread.currentThread().getName()+"抢到了打火机");
            synchronized (smoke){
                System.out.println(Thread.currentThread().getName()+"可以嗨起来了");
            }
        }
    }
}
public class DeadDemo {
    public static void main(String[] args) {
        Object smoke = new Object();
        Object lighter = new Object();
        new PersonA("A",smoke,lighter).start();
        new PersonB("B",smoke,lighter).start();
    }
}
