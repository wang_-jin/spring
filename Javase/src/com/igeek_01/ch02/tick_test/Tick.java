package com.igeek_01.ch02.tick_test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wangjin
 * 2023/9/7 21:37
 * @description TODO
 */
public class Tick {
    private int fiveNum = 3;
    private int tenNum;
    private int twentyNum;
    private int paperValues;
    /*编写多线程同步程序，模拟3个人排除买票，张某、李某和赵某买电影票，售票员只有3张五元的钱，
    电影票5元一张。张某拿20元一张的RMB排在李某的前面，李某排在赵某的前面拿一张10元的RMB买票，
    赵某拿一张5元的RMB买票。*/

    /*    private static final Lock lock = new ReentrantLock();
        private static final Condition condition = lock.newCondition();*/
    public synchronized void buy(int paperValues) {
        try {

                if (paperValues == 20 ) {

                    while (fiveNum<3){
                        System.out.println(Thread.currentThread().getName() + "买票失败");
                        this.wait();
                    }
                    fiveNum = fiveNum - 3;
                    twentyNum++;
                    System.out.println(Thread.currentThread().getName() + "买票成功");

                }
                else if (paperValues == 10 ) {
                    while (fiveNum == 0 ){
                        System.out.println(Thread.currentThread().getName() + "买票失败");
                        this.wait();
                    }
                    fiveNum--;
                    tenNum++;
                    System.out.println(Thread.currentThread().getName() + "买票成功");

                }
                else if (paperValues==5){
                    fiveNum++;
                    System.out.println(Thread.currentThread().getName()+"买票成功");
                }
                this.notifyAll();



        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getPaperValues() {
        return paperValues;
    }

    public void setPaperValues(int paperValues) {
        this.paperValues = paperValues;
    }
}

