package com.igeek_01.ch02.tick_test;

import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/7 23:19
 * @description TODO
 */
public class TickRun implements Runnable {
    private Tick tick;
    private int paperValues;

    public TickRun(Tick tick, int paperValues) {
        this.tick = tick;
        tick.setPaperValues(paperValues);


    }

    @Override
    public void run() {
            tick.buy(tick.getPaperValues());
    }
}
