package com.igeek_01.ch02.tick_test;

/**
 * @author wangjin
 * 2023/9/7 23:18
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        Tick tick = new Tick();
        new Thread(new TickRun(tick,20),"张三").start();
        new Thread(new TickRun(tick,10),"李白").start();
        new Thread(new TickRun(tick,5),"赵钱").start();
    }
}
