package com.igeek_01.ch02.tick_test1;

/**
 * @author wangjin
 * 2023/9/11 19:11
 * @description TODO
 */
public class TicketDemo {
    public static void main(String[] args) {
        // 2.1 在main方法中创建三个购票者子线程对象
        TicketBuyer t1 = new TicketBuyer(20);
        TicketBuyer t2 = new TicketBuyer(10);
        TicketBuyer t3 = new TicketBuyer(5);

        // 2.2 开启三个子线程，开始购票
        t1.start();
        t2.start();
        t3.start();
    }
}

