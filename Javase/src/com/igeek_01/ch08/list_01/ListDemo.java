package com.igeek_01.ch08.list_01;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @Description 反射可以破坏封装
 * @Author fqq
 * @Date 2023/9/20 9:29
 *
 * 1.反射可以破坏封装
 * 泛型本质上是编译期间的一种约束，反射是运行时的一种技术
 */
public class ListDemo {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //创建的集合，泛型起作用
        //约束编译期间添加的元素必须是Integer类型
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(100);
        list.add(200);
        list.add(300);

        //利用反射调用add()
        //获取ArrayList的类类型
        Class<ArrayList> klass = ArrayList.class;
        //获取Method对象 add()
        Method method = klass.getDeclaredMethod("add", Object.class);
        //通过Method对象 执行方法
        method.invoke(list,"abc");

        System.out.println("list = " + list);
    }
}
