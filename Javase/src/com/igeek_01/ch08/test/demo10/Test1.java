package com.igeek_01.ch08.test.demo10;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangjin
 * 2023/9/20 20:03
 * @description TODO
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
@interface MyAnno1{}
@MyAnno1()
public class Test1 {
}
