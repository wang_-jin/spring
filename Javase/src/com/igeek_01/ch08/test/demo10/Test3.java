package com.igeek_01.ch08.test.demo10;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangjin
 * 2023/9/20 20:20
 * @description TODO
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno3{
    String type();
    int[] intArr();

}

public class Test3 {
    @MyAnno3(type = "java",intArr = {1,2})
    public void show(){}
}
