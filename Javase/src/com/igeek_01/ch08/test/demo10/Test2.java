package com.igeek_01.ch08.test.demo10;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangjin
 * 2023/9/20 20:11
 * @description TODO
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
@interface MyAnno2{
    String type() default "java";
}

public class Test2 {
    @MyAnno2()
    int num;
}
