package com.igeek_01.ch08.test.demo13;

/**
 * @author wangjin
 * 2023/9/20 23:17
 * @description TODO
 */
public class ArithmeticCalculatorLogImpl implements ArithmeticCalculator{
    @Override
    public int add(int i, int j) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("日志: the method add begin with ["+i+" , "+j+"]");
        int result = i+j;
        System.out.println("日志: the method add begin with "+ result);
        return result;
    }

    @Override
    public int sub(int i, int j) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("日志: the method add begin with ["+i+" , "+j+"]");
        int result = i-j;
        System.out.println("日志: the method add begin with "+ result);
        return result;
    }

    @Override
    public int mul(int i, int j) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("日志: the method add begin with ["+i+" , "+j+"]");
        int result = i*j;
        System.out.println("日志: the method add begin with "+ result);
        return result;
    }

    @Override
    public int div(int i, int j) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("日志: the method add begin with ["+i+" , "+j+"]");
        int result = i/j;
        System.out.println("日志: the method add begin with "+ result);
        return result;
    }
}
