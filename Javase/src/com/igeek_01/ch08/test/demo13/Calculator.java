package com.igeek_01.ch08.test.demo13;

import com.igeek_01.ch08.proxy.jdks.BankService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author wangjin
 * 2023/9/20 23:13
 * @description TODO
 */
public class Calculator {
    //目标对象
    private ArithmeticCalculator calculator;

    public Calculator(ArithmeticCalculator calculator) {
        this.calculator = calculator;
    }


    public ArithmeticCalculator getProxy(){

        ClassLoader classLoader = calculator.getClass().getClassLoader();

        Class<?>[] interfaces = calculator.getClass().getInterfaces();

        InvocationHandler handler = new InvocationHandler() {

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                System.out.println("开始日志追踪：method="+method.getName());
                long start = System.currentTimeMillis();

                Object result = method.invoke(calculator, args);

                long end = System.currentTimeMillis();
                System.out.println("结束日志追踪：method="+method.getName()+"，耗时="+(end-start)+"ms");
                return result;
            }
        };
        Object proxy = Proxy.newProxyInstance(classLoader,interfaces,handler);
        return (ArithmeticCalculator)proxy;
    }
}
