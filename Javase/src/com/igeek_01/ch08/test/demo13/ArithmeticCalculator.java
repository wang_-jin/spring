package com.igeek_01.ch08.test.demo13;

public interface ArithmeticCalculator {
    public int add(int i, int j);
    public int sub(int i, int j);
    public int mul(int i, int j);
    public int div(int i, int j);
}
