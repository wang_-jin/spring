package com.igeek_01.ch08.test.demo13;



/**
 * @author wangjin
 * 2023/9/20 23:29
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //创建目标对象
        ArithmeticCalculatorLogImpl target = new ArithmeticCalculatorLogImpl();
        //创建代理对象
        ArithmeticCalculator proxy = new Calculator(target).getProxy();

        System.out.println("proxy.getClass() = " + proxy.getClass());

        //测试加
        int add = proxy.add(1, 3);
        System.out.println("加 = " + add);
        //测试减
        int sub = proxy.sub(4, 2);
        System.out.println("减 = " + sub);
        //测试乘
        int mul = proxy.mul(4, 2);
        System.out.println("乘 = " + mul);
        //测试除
        int div = proxy.div(4, 2);
        System.out.println("除 = " + div);
    }
}
