package com.igeek_01.ch08.test.demo12;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/20 20:32
 * @description TODO
 */
public class TestAnnotation {
    //解析获得该成员方法上使用注解的属性值
    @Test
    public void testAnnotationMethod() throws NoSuchMethodException {
        Class<BookStore> klass = BookStore.class;
        Method method = klass.getDeclaredMethod("sale");
        if (method.isAnnotationPresent(Book.class)){
            Book annotation = method.getDeclaredAnnotation(Book.class);
            System.out.println("书名 = " + annotation.value());
            System.out.println("价格 = " + annotation.price());
            System.out.println("多位作者 = " + Arrays.toString(annotation.authors()));
        }
    }
    //解析获得该成员属性上使用注解的属性值
    @Test
    public void testAnnotationField() throws NoSuchFieldException {
        Class<BookStore> klass = BookStore.class;
        Field field = klass.getDeclaredField("name");
        field.setAccessible(true);
        if (field.isAnnotationPresent(Book.class)){
            Book annotation = field.getDeclaredAnnotation(Book.class);
            System.out.println("书名 = " + annotation.value());
            System.out.println("多位作者 = " + Arrays.toString(annotation.authors()));
        }
    }
}
