package com.igeek_01.ch08.test.demo12;


import org.junit.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangjin
 * 2023/9/20 20:25
 * @description TODO
 */
@Target({ElementType.TYPE,ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface Book{
    String value();
    double price() default 100;
    String[] authors();
}
@Book(value = "java从入门到放弃",authors = {"张鑫龙","莫凡"})
public class BookStore {
    @Book(value = "java从入门到放弃", authors = {"张鑫龙", "莫凡"})
    private String name;

    //购买
    @Book(value = "mysql从建表到删库", price = 88.8, authors = "陈召")
    public void sale() {
    }

}


