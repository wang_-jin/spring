package com.igeek_01.ch08.test.demo9;

import java.io.*;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/20 18:28
 * @description TODO
//有一个用于记录程序运行次数的属性文件，运行次数保存在一个count属性中，当到达指定次数3次时，则提示:"程序使   用次数已满，请续费"
1. 开发思路：
1).判断属性文件是否存在，如果不存在则创建一个。
2).使用load()方法加载文件中所有的属性到Properties集合中。
3).取得count属性，如果count属性为null，则设置count属性为0。
4).将取得的字符串转成整型，并判断是否大于等于3次，大于3次则到期，退出。
5).小于3则输出运行次数，并加1。
6).将整数转成字符串后存到Properties集合中。
7).创建输出流，并用store方法保存到文件中。
 */
public class Test {
    public static void main(String[] args) throws IOException {
        File file = new File("demo.properties");
        Properties prop = new Properties();
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileInputStream fis = new FileInputStream(file);
        prop.load(fis);
        fis.close();
        String str = prop.getProperty("count","0");
        int count = Integer.parseInt(str);
        if (count > 3) {
            System.out.println("程序使用次数已满，请续费");
        } else {
            count++;
            System.out.println("程序运行了：" + count + "次");
            prop.setProperty("count", count+"");
            FileOutputStream fos = new FileOutputStream(file);
            prop.store(fos, "software run times");
            fos.close();
        }
    }
}

