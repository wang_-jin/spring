package com.igeek_01.ch08.test.demo11;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/20 20:49
 * @description TODO
 */
public class Test {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Class<TestDemo> klass = TestDemo.class;
        TestDemo testDemo = klass.newInstance();
        Method[] methods = klass.getDeclaredMethods();
        Stream.of(methods).forEach(method->{
            if (method.isAnnotationPresent(MyTest.class)){
                try {
                    //执行
                    method.invoke(testDemo);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
