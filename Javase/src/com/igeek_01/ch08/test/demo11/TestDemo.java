package com.igeek_01.ch08.test.demo11;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangjin
 * 2023/9/20 20:45
 * @description TODO
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyTest{}
public class TestDemo {
    @MyTest
    public void sleep(){
        System.out.println("睡觉");
    }

    @MyTest
    public void eat(){
        System.out.println("吃饭");
    }

    public void study(){
        System.out.println("学习");
    }

}
