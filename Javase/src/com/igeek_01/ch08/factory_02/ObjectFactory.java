package com.igeek_01.ch08.factory_02;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Description 工厂模式
 * @Author fqq
 * @Date 2023/9/20 9:38
 *
 * Spring的思想：IOC控制反转 (工厂模式+单例模式)
 *
 * 工厂模式：将对象的管理权交给了另一个类，这个类称为工厂类
 *
 * ObjectFactory:负责读取objects.properties的文件，将每一个键值对 实例成对象，并且存储在内存中
 *
 * 创建的对象都是 饿汉式单例
 */
public class ObjectFactory {
    //key 就是配置文件中等号前的内容  value 就是 根据配置文件中等号后的内容创建的实例
    private static Map<String,Object> map = new HashMap<>();

    //类加载的时期：负责读取objects.properties的文件，将每一个键值对 实例成对象，并且存储在内存中
    private static Properties properties;
    static{
        try {
            properties = new Properties();
            //获取 与类同级目录下 的 objects.properties文件的输入流
            InputStream is = ObjectFactory.class.getResourceAsStream("objects.properties");
            properties.load(is);
            //遍历properties的集合，根据key 来创建 每一个实例
            properties.forEach((key,value)->{
                try {
                    Class klass = Class.forName((String)value);
                    Object instance = klass.newInstance();
                    map.put((String)key,instance);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //对外提供公开的静态方法：根据key获取对应的单例对象
    public static Object getObject(String key){
        return map.get(key);
    }

    public static void main(String[] args) {
        Object obj1 = ObjectFactory.getObject("demoClass");
        System.out.println("obj1 = " + obj1);

        Object obj2 = ObjectFactory.getObject("a");
        System.out.println("obj2 = " + obj2);
    }
}
