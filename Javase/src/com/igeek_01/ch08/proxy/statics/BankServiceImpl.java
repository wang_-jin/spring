package com.igeek_01.ch08.proxy.statics;

/**
 * @author wangjin
 * 2023/9/20 15:25
 * @description 业务逻辑的实现类--目标对象
 */
public class BankServiceImpl implements BankService{
    @Override
    public boolean login(Long id, String pwd) {
        //模拟业务执行耗时1秒
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(id.equals(1L) && "123".equals(pwd)){
            System.out.println("登陆成功");
            return true;
        }
        System.out.println("登陆失败");
        return false;
    }

    @Override
    public void register(Long id, String pwd, String name) {
        //模拟业务执行耗时1秒
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("插入数据成功：id="+id+",pwd="+pwd+",name="+name);
    }
}
