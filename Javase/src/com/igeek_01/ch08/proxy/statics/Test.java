package com.igeek_01.ch08.proxy.statics;

/**
 * @author wangjin
 * 2023/9/20 16:13
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //创建目标对象
        BankService bankService = new BankServiceImpl();
        //创建代理对象
        BankLogService proxy = new BankLogService(bankService);

        //测试登陆
        proxy.login(1L,"123");
        System.out.println("-------------------------------");
        //测试开户
        proxy.register(1L,"123","aaa");
    }
}
