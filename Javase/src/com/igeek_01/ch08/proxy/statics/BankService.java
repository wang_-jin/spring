package com.igeek_01.ch08.proxy.statics;

/**
 * @author wangjin
 * 2023/9/20 15:25
 * @description Bank业务逻辑接口
 */
public interface BankService {
    //登陆
    public boolean login(Long id,String pwd);
    //开户
    public void register(Long id,String pwd,String name);
}
