package com.igeek_01.ch08.proxy.statics;

/**
 * @author wangjin
 * 2023/9/20 15:28
 * @description 代理对象：业务逻辑+ 日志记录+性能检测
 * 静态代理模式：
 * 使用前提条件：无论是目标对象 还是 代理对象 都必须一起实现相同的接口
 *
 * 优点：
 * 在没有修改原本的目标类的基础上，完成对原有类的功能的增强
 *
 * 缺点：
 * 1.目标对象和代理对象 必须是实现相同的接口
 * 2.如果目标类数量很多，会导致代理类的数量急剧膨胀
 * 3.一旦需要改变需求，扩展性较差
 */
public class BankLogService implements BankService {
    //目标对象
    private BankService bankService;

    public BankLogService(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public boolean login(Long id, String pwd) {
        //非核心业务
        System.out.println("开始日志追踪：method=login");
        long start = System.currentTimeMillis();

        //核心业务
        boolean result = bankService.login(id, pwd);

        //非核心业务
        long end = System.currentTimeMillis();
        System.out.println("结束日志追踪：method=login，耗时="+(end-start));
        return result;
    }

    @Override
    public void register(Long id, String pwd, String name) {
        //非核心业务
        System.out.println("开始日志追踪：method=register");
        long start = System.currentTimeMillis();

        //核心业务
        bankService.register(id,pwd,name);

        //非核心业务
        long end = System.currentTimeMillis();
        System.out.println("结束日志追踪：method=register，耗时="+(end-start));
    }
}
