package com.igeek_01.ch08.proxy.cglibs;

/**
 * @author wangjin
 * 2023/9/20 18:17
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //创建目标对象
        BankServiceImpl target = new BankServiceImpl();
        //创建代理对象
        BankServiceImpl proxy = new CglibProxyBankLogService(target).getProxy();
        // class ch08.proxy_05.cglibs.BankServiceImpl$$EnhancerByCGLIB$$70e13b0c
        System.out.println("proxy.getClass() = " + proxy.getClass());

        //测试登陆
        proxy.login(1L,"123");
        System.out.println("---------------------------");
        //测试开户
        proxy.register(1L,"123","1111");
    }
}
