package com.igeek_01.ch08.proxy.cglibs;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author wangjin
 * 2023/9/20 18:16
 * @description cglib动态代理
 * cglib代理模式实现步骤：
 * 1)	需要引入cglib的jar文件
 * 2）  当前代理类 需要实现MethodInterceptor方法拦截器接口，重写拦截方法
 * 3)   提供一个方法，生成目标类的子类对象作为代理对象
 *
 *
 * Cglib代理模式使用的注意事项：
 * 1)	在内存中动态构建子类，注意代理的类不能为final，否则报错java.lang.IllegalArgumentException
 *      IllegalArgumentException: Cannot subclass final class class ch08.proxy_05.cglibs.BankServiceImpl
 *
 * 2)	目标对象的方法如果为final/static,那么就不会被拦截,即不会执行目标对象额外的业务方法.
 *
 * 3）  在内存中动态构建子类，注意代理的类需要提供一个公开的无参的构造方法，否则将无法创建代理对象
 *      IllegalArgumentException: Superclass has no null constructors but no arguments were given
 *
 * 优点：
 *  * 1.在对目标类的方法的增强时，可以不改变目标类原有方法的业务逻辑
 *  * 2.不需要通过java类来定义代理类，代理对象利用反射技术在内存中构建的，所以 扩展性更高
 *  * 3.目标类不需要实现接口，也能产生代理对象
 */
public class CglibProxyBankLogService implements MethodInterceptor {
    //目标对象
    private BankServiceImpl target;

    public CglibProxyBankLogService(BankServiceImpl target) {
        this.target = target;
    }

    //提供生成代理对象的方法
    public BankServiceImpl getProxy(){
        //在内存中构建一个子类对象
        Enhancer enhancer = new Enhancer();
        //1.设置其父类类型
        enhancer.setSuperclass(BankServiceImpl.class);
        //2.设置回调函数 ==> 在构建代理对象时，通过执行回调函数来完成对方法的功能增强
        //提供CallBack接口的对象  ==> class 当前类 interface MethodInterceptor extends Callback
        enhancer.setCallback(this);
        Object proxy = enhancer.create();
        return (BankServiceImpl) proxy;
    }

    /**
     * 对目标类的所有方法进行功能增强：非核心业务
     * @param o 代理对象，此处不使用
     * @param method  目标类的目标方法
     * @param objects  目标类的目标方法的参数列表数据
     * @param methodProxy   此处不使用
     * @return  目标类的目标方法的执行结果
     * @throws Throwable
     */
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        //非核心业务
        System.out.println("开始日志追踪：method="+method.getName());
        long start = System.currentTimeMillis();

        //核心业务
        Object result = method.invoke(target, objects);

        //非核心业务
        long end = System.currentTimeMillis();
        System.out.println("结束日志追踪：method="+method.getName()+"，耗时="+(end-start)+"ms");
        return result;
    }
}
