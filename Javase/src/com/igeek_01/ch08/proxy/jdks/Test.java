package com.igeek_01.ch08.proxy.jdks;

/**
 * @author wangjin
 * 2023/9/20 18:12
 * @description TODO
 */
public class Test {
    public static void main(String[] args) {
        //创建目标对象
        BankServiceImpl target = new BankServiceImpl();
        //创建代理对象
        BankService proxy = new JDKProxyBankLogService(target).getProxy();
        // System.out.println("proxy = " + proxy);//System.out.println(proxy)底层调用的是proxy的toString()方法

        //class com.sun.proxy.$Proxy0 利用JDK的api 在内存中构建代理对象
        System.out.println("proxy.getClass() = " + proxy.getClass());

        //测试登陆
        proxy.login(1L,"123");

        System.out.println("---------------------");
        //测试开户
        proxy.register(1L,"123","aaa");
    }
}
