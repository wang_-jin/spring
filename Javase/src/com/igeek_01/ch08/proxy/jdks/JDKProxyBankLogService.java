package com.igeek_01.ch08.proxy.jdks;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author wangjin
 * 2023/9/20 18:12
 * @description jdk动态代理模式
 * 动态代理简单来说是：拦截对真实对象方法的直接访问，增强真实对象方法的功能
 * Spring-AOP 面向切面编程
 * 实现原理本质就是jdk动态代理。若未发现有实现接口，转而使用cglib动态代理
 * 注意：
 * 1.代理对象,不需要实现接口，但是目标对象要实现接口，否则不能用动态代理
 * 2.代理对象的生成，是利用JDK的API，动态的在内存中构建代理对象
 * 3.动态代理也叫做：JDK代理、接口代理
 *
 * 优点：
 * 1.在对目标类的方法的增强时，可以不改变目标类原有方法的业务逻辑
 * 2.不需要通过java类来定义代理类，代理对象利用反射技术在内存中构建的，所以 扩展性更高
 * 缺点：只能针对接口的实现类做代理对象，普通类是不能做代理对象的。
 */
public class JDKProxyBankLogService {
    //目标对象
    private BankService bankService;

    public JDKProxyBankLogService(BankService bankService) {
        this.bankService = bankService;
    }

    //获取代理对象==> 根据 目标对象的接口 来生成实现类对象
    public BankService getProxy(){
        //利用JDK的API，动态的在内存中构建代理对象
        /**
         * ClassLoader loader,  目标类的类加载器
         * Class<?>[] interfaces, 目标类的实现的接口
         * InvocationHandler h    执行器：拦截对目标类的所有方法的访问，在执行目标方法的前后加入非核心业务
         */
        //类加载器
        ClassLoader classLoader = bankService.getClass().getClassLoader();
        //目标类的实现的接口
        Class<?>[] interfaces = bankService.getClass().getInterfaces();
        //执行器
        InvocationHandler handler = new InvocationHandler() {
            /**
             *
             * @param proxy 代理对象
             * @param method  目标类的中所有目标方法
             * @param args  目标类的中所有目标方法的参数列表
             * @return  目标类的中所有目标方法的执行结果
             * @throws Throwable
             */
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //非核心业务
                System.out.println("1111开始日志追踪：method="+method.getName());
                long start = System.currentTimeMillis();

                //核心业务
                Object result = method.invoke(bankService, args);

                //非核心业务
                long end = System.currentTimeMillis();
                System.out.println("2222结束日志追踪：method="+method.getName()+"，耗时="+(end-start)+"ms");
                return result;
            }
        };
        Object proxy = Proxy.newProxyInstance(classLoader,interfaces,handler);
        return (BankService)proxy;
    }
}
