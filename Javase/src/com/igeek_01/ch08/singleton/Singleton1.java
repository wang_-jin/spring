package com.igeek_01.ch08.singleton;

/**
 * @author wangjin
 * 2023/9/20 11:28
 * @description 线程安全的懒汉式单例 并发三特性：可见性 原子性  指令重排
 */
public class Singleton1 {
    private volatile static Singleton1 instance;
    private Singleton1(){}
    public static Singleton1 getInstance(){
        if(instance==null){
            synchronized (Singleton1.class) {
                if(instance==null) {
                    /**
                     * 1.创建实例变量
                     * 2.分配内存创建实例对象
                     * 3.实例变量 指向 分配的内存空间
                     * 期望执行顺序：123  ==> 禁止指令重排
                     */
                    instance = new Singleton1();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        //多线程获取单例对象
        for (int i = 1; i <=50 ; i++) {
            new Thread(()->{
                Singleton1 instance = Singleton1.getInstance();
                System.out.println(Thread.currentThread().getName()+"获取的实例为"+instance);
            },"线程"+i).start();
        }
    }
}
