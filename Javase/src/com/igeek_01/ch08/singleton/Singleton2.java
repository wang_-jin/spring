package com.igeek_01.ch08.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @author wangjin
 * 2023/9/20 11:28
 * @description 线程安全的懒汉式单例 -- 反射可以破坏单例
 */
public class Singleton2 {
    private volatile static Singleton2 instance;
    //标志 是否有用过私有构造方法
    private static boolean dsdfdfdfdfererebvbv = false;
    private Singleton2(){
        synchronized (Singleton2.class){
            if(!dsdfdfdfdfererebvbv) {
                dsdfdfdfdfererebvbv = true;
            }else{
                throw new IllegalArgumentException("非法操作单实例，禁止执行！！！");
            }
        }
    }
    public static Singleton2 getInstance(){
        if(instance==null){
            synchronized (Singleton2.class) {
                if(instance==null) {
                    /**
                     * 1.创建实例变量
                     * 2.分配内存创建实例对象
                     * 3.实例变量 指向 分配的内存空间
                     * 期望执行顺序：123  ==> 禁止指令重排
                     */
                    instance = new Singleton2();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        //方式1：通过静态方法获取单例对象
//        Singleton2 s1 = Singleton2.getInstance();
//        Singleton2 s2 = Singleton2.getInstance();
//        System.out.println("s1 = " + s1);
//        System.out.println("s2 = " + s2);
//        System.out.println(s1 == s2);//true

        System.out.println("-------------------------------");
        //方式2：通过反射技术 获取对象
        //1.获取Singleton2的类类型 Class对象
        Class<Singleton2> klass = Singleton2.class;
        //2.获取private 构造方法
        Constructor<Singleton2> con = klass.getDeclaredConstructor();
        //3.取消访问权限检查
        con.setAccessible(true);
        //4.创建对象
        Singleton2 s3 = con.newInstance();
        System.out.println("s3 = " + s3);

        //通过反射 获取dsdfdfdfdfererebvbv这个属性，将其值恢复为flase 再去执行构造方法
        Field field = klass.getDeclaredField("dsdfdfdfdfererebvbv");
        field.setAccessible(true);
        field.set(null,false);
        Singleton2 s4 = con.newInstance();
        System.out.println("s4 = " + s4);
        System.out.println(s4 == s3);//false
    }
}