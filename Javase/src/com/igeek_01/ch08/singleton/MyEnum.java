package com.igeek_01.ch08.singleton;

/**
 * 一组固定值（多实例）
 *
 * 反射获取私有的构造方法，进行创建实例，与静态方法getInstance()获取的实例 不是同一个
 *
 * 通过反射获取实例，如何保证获取的是单实例？只能通过枚举，利用反射无法破坏枚举
 */
public enum MyEnum {
    //常量  public static final com.igeek_01.ch08.singleton.MyEnum.INSTANCE;
    INSTANCE;

    public static MyEnum getInstance(){
        return INSTANCE;
    }
}
