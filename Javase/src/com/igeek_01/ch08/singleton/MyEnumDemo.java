package com.igeek_01.ch08.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author wangjin
 * 2023/9/20 11:27
 * @description TODO
 */
public class MyEnumDemo {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //1.通过静态方法获取对象
        MyEnum s1 = MyEnum.getInstance();
        MyEnum s2 = MyEnum.getInstance();
        System.out.println(s1 == s2); //true

        //2.通过反射获取对象
        //2.1 获取MyEnum的类类型
        Class<MyEnum> klass = MyEnum.class;
        //2.2 获取枚举的构造方法 ？？？？ 查看字节码文件
        //方式1：打开终端terminal javap -p MyEnum.class 反编译字节码文件 ==> private ch08.singleton_03.MyEnum();
        //Constructor<MyEnum> con1 = klass.getDeclaredConstructor();

        //方式2：选中java文件 菜单view--show bytecode ==> private <init>(Ljava/lang/String;I)V
        Constructor<MyEnum> con1 = klass.getDeclaredConstructor(String.class, int.class);
        con1.setAccessible(true);
        // 如果类类型的修饰符是Enum，调用newInstance该方法：会引发异常IllegalArgumentException: Cannot reflectively create enum objects
        MyEnum s3 = con1.newInstance();
        System.out.println(s3);
        //2.3 创建对象
    }
}
