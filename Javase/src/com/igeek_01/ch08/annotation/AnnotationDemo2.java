package com.igeek_01.ch08.annotation;

/**
 * @author wangjin
 * 2023/9/20 13:36
 * @description 自定义注解+属性   作用：给类额外携带一些信息
 * 自定义注解+属性   作用：给类额外携带一些信息
 * 1.如何声明
 *      @interface 注解名｛
 *          数据类型 自定义属性名();
 *          数据类型 自定义属性名() default 默认值;
 *          数据类型[] 自定义属性名() ;
 *          数据类型[] value() ;  //默认属性
 *      ｝
 *      数据类型:可以基本数据类型 引用类型 数组 枚举等
 *
 * 2.如何使用
 *      @注解名(属性名1=属性值1,属性名2=属性值2,属性名3=属性值3...)
 *      若有属性有default默认值的则可以不赋值，其他属性都必须赋值
 *
 * 3.特殊点：
 *      @interface MyTeacher{
 *          String value();
 *      }
 *      若只有默认属性，可以省略属性名value不写 则 @MyTeacher("xxx")
 *
 *      @interface MyTeacher{
 *          String value();
 *          int age();
 *      }
 *      使用时，不能省略默认属性名value  则@MyTeacher(value = "李四",age=30)
 *
 *      @interface MyTeacher{
 *          String value();
 *          int age() default 19;
 *      }
 *      使用时，因为age带有默认值，则可以省略属性名value 则 @MyTeacher("王五")
 *
 */
@interface  Mystu{
    String value();//默认属性
    String name();
    int age() default 18;
    double height();
    String[] courses();
}
@interface  MyTeacher{
    String value();
    int age() default 19;
    double height() default 188;
}
@Mystu(value = "001",name = "张三", height = 180.0, courses = {"java","mysql"})
public class AnnotationDemo2 {
    @Mystu(value="001",height = 180.0,courses = {"java","mysql"},name="张三",age=22)
    private String a;

    //@MyTeacher("小明")
//    @MyTeacher(value = "李四",age=30)
    //@MyTeacher("王五")
//    @MyTeacher(value = "aaa",height = 188)
    @MyTeacher("bbb")
    public void test(){}
}
