package com.igeek_01.ch08.annotation.book;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/20 14:05
 * @description 测试类:获取Book注解上的数据
 * 注解解析--利用反射技术读取注解的信息
 *
 *  什么是注解解析
 *         * 使用Java技术获得注解上数据的过程则称为注解解析。
 *     与注解解析相关的接口
 *         * Annotation: 注解类，该类是所有注解的父类。
 *         * AnnotatedElement:该接口定义了与注解解析相关的方法
 *              T getAnnotation(Class<T> annotationClass) 根据注解类型获得对应注解对象
 *              Annotation[]	getAnnotations()
 *                 * 获得当前对象上使用的所有注解，返回注解数组，包含父类继承的
 *              Annotation[]	getDeclaredAnnotations()
 *                 * 获得当前对象上使用的所有注解，返回注解数组,只包含本类的
 *              boolean	isAnnotationPresent(Class<Annotation> annotationClass)
 *                 * 判断当前对象是否使用了指定的注解，如果使用了则返回true，否则false
 *
 *         获取注解数据的原理
 *             * 注解作用在哪个成员上就会得该成员对应的对象来获得注解
 *                 * 比如注解作用成员方法，则要获得该成员方法对应的Method对象
 *                 * 比如注解作用在类上，则要该类的Class对象
 *                 * 比如注解作用在成员变量上，则要获得该成员变量对应的Field对象。
 *             * Field,Method,Constructor，Class等类都是实现了AnnotatedElement接口
 */
public class TestAnnotation {

    //测试方法 获取类上的注解以及属性信息
    @Test
    public void testAnnotationClass() throws NoSuchMethodException {
        Class<BookStore> klass = BookStore.class;
        if (klass.isAnnotationPresent(Book.class)){
            Book annotation = klass.getDeclaredAnnotation(Book.class);
            System.out.println("annotation.authors() = " + Arrays.toString(annotation.authors()));
            System.out.println("annotation.value() = " + annotation.value());
        }else{
            System.out.println(klass.getName()+"类上面没有标记@Book注解！");
        }
    }
    //测试方法 获取方法上的注解以及属性信息
    @Test
    public void testAnnotationMethod(){
        //1.获取BookStore的类类型
        Class<BookStore> klass = BookStore.class;
        //2.获取BookStore类中的方法对象Method
        Method[] methods = klass.getDeclaredMethods();
        //3.判断当前方法上是否有标记注解@Book
        Stream.of(methods).forEach(method -> {
            if(method.isAnnotationPresent(Book.class)){
                //3.1 获取当前方法上的注解并且解析
                Book annotation = method.getDeclaredAnnotation(Book.class);
                //获取注解中的属性信息
                System.out.println("书名 = " + annotation.value());
                System.out.println("价格 = " + annotation.price());
                System.out.println("作者="+Arrays.toString(annotation.authors()));
            }else{
                System.out.println(method.getName()+"方法上面没有标记@Book注解！");
            }
                });
    }
    //测试方法 获取属性上的注解以及属性信息
    @Test
    public void testAnnotationField(){
        Class<BookStore> klass = BookStore.class;
        Field[] fields = klass.getDeclaredFields();
        Stream.of(fields).forEach(field -> {
            if (field.isAnnotationPresent(Book.class)){
                Book annotation = field.getDeclaredAnnotation(Book.class);
                System.out.println("annotation.value() = " + annotation.value());
                System.out.println("annotation.price() = " + annotation.price());
                System.out.println("Arrays.toString(annotation.authors()) = " + Arrays.toString(annotation.authors()));
            }else{
                System.out.println(field.getName()+"属性上面没有标记@Book注解！");
            }
        });
    }
}
