package com.igeek_01.ch08.annotation.book;

/**
 * @author wangjin
 * 2023/9/20 14:05
 * @description 定义BookStore类，在类和成员方法,属性 上使用Book注解
 */
@Book(value = "web前端设计", authors = {"张三","李四"})
public class BookStore {
    //属性
    @Book(value = "mysql从删库到跑路",price = 88.0,authors = {"张三","李四"})
    private String storeName;
    //购买方法
    @Book(value = "mysql从删库到跑路",price = 88.0,authors = {"张三","李四"})
    public void purchase(){}

    public void add(){}
}
