package com.igeek_01.ch08.annotation.book;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定义注解Book，要求如下：
 * - 包含属性：String value()   书名
 * - 包含属性：double price()  价格，默认值为 100
 * - 包含属性：String[] authors() 多位作者
 * - 限制注解使用的位置：类,成员方法，属性上
 * - 指定注解的有效范围：RUNTIME
 */

@Target({ElementType.TYPE,ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Book {
    String value();
    double price() default 100;
    String[] authors();
}
