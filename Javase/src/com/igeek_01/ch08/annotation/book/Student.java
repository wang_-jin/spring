package com.igeek_01.ch08.annotation.book;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/20 14:06
 * @description 模拟spring的依赖注入原理：解析@MyValue注解 完成对标记该注解的属性赋值
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface MyValue{
    String value();
}
public class Student {
    @MyValue("张三")
    private String name;
    @MyValue("23")
    private int age;
    @MyValue("男")
    private String gender;
    @Override

    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}
class Test{
    //利用反射：完成对任意对象的属性初始化
    public static void init(Object obj){
        //1.获取对象obj的类类型
        Class<?> klass = obj.getClass();
        //2.获取类型上的所有属性
        Field[] fields = klass.getDeclaredFields();
        //3.遍历所有属性：判断是否有标记@MyValue注解
        Stream.of(fields).forEach(field -> {
            if(field.isAnnotationPresent(MyValue.class)){
                //获取@MyValue注解 获取到value属性值
                String value = field.getDeclaredAnnotation(MyValue.class).value();
                //对该属性field 赋值
                field.setAccessible(true);
                Class fieldType = field.getType();
                try {
                    if(fieldType == int.class){
                        field.set(obj,Integer.parseInt(value));
                    }else {
                        field.set(obj, value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public static void main(String[] args) {
        Student student = new Student();
        init(student);

        System.out.println("student = " + student);

    }
}

