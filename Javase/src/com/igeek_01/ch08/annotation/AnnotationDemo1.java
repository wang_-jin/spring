package com.igeek_01.ch08.annotation;

/**
 * @author wangjin
 * 2023/9/20 13:36
 * @description 自定义注解
 * 自定义注解：
 * 1.如何声明
 *     @interface 注解名｛｝
 * 2.如何使用
 *    @注解名
 * 3.注解使用的位置
 *    类，方法，成员变量，方法的形参，局部变量。同时可以出现在多个位置
 *
 * 内置注解 jdk提供的：
 * @Override  判断该方法是否是重写
 * @FunctionalInterface 判断该接口是否是函数式接口
 */
//声明注解
@interface MyAnno1{}
@interface MyAnno2{}

@MyAnno1
public class AnnotationDemo1 {
    @MyAnno1
    private int age;
    @MyAnno1
    private String name;

    @MyAnno2
    public void a(@MyAnno1 int x,@MyAnno1 String y){}
}
