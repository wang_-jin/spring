package com.igeek_01.ch07.test.demo2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/19 20:18
 * @description TODO
 */
public class Test {
    /*1. 现有集合：ArrayList list = new ArrayList();
2. 利用反射机制在这个泛型为Integer的ArrayList中存放一个String类型的对象。 -- 测试反射能否破环泛型？
3. Class对象 获取对应的方法：add()
*/
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Class klass = list.getClass();
        Method addMethod = klass.getMethod("add", Object.class);
        Object result = addMethod.invoke(list, "ABC");
        System.out.println(list);
    }

}
