package com.igeek_01.ch07.test.demo7;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author wangjin
 * 2023/9/19 22:07
 * @description TODO
 */
class Person{
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Person{name = " + name + ", age = " + age + "}";
    }
}
public class Test {
    /*1. 定义一个Person类，包含私有属性name、age。
2. 使用反射的方式创建一个实例、调用构造函数初始化name、age。使用反射方式调用setName方法对姓名进行设置，
不使用setAge方法直接使用反射方式对age赋值。
*/
    public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        Class<Person> klass = Person.class;
        Constructor<Person> constructor = klass.getDeclaredConstructor(String.class, int.class);
        Person person = klass.newInstance();
        Method method = klass.getMethod("setName", String.class);
        Object obj = method.invoke(person, "张三");
        Field ageField = klass.getDeclaredField("age");
        ageField.setAccessible(true);
        ageField.set(person, 22);
        System.out.println(person);
    }
}
