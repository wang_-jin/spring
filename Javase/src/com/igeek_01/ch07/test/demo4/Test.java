package com.igeek_01.ch07.test.demo4;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author wangjin
 * 2023/9/19 21:10
 * @description TODO
 */
class Person{
    /*1. 定义一个类，在类中定义一个成员方法show，方法功能是：打印一个字符串。
2. 使用反射机制创建该类的对象，并调用该对象的show方法。
*/
    public static void show(String str){
        System.out.println(str);
    }
}
public class Test {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<Person> klass = Person.class;
        Method method = klass.getMethod("show", String.class);
        Object result = method.invoke(null, "我爱学习!");
        System.out.println("result = " + result);
    }
}
