package com.igeek_01.ch07.test.demo6;

import java.lang.reflect.Field;

/**
 * @author wangjin
 * 2023/9/19 21:31
 * @description TODO
 */
public class Test {
    //按要求完成下面两个方法的方法体
    public static void main(String[] args) {

        try {
            User user = new User("张三");
            Test test = new Test();
            test.getProperty(user,user.getPropertyName());
            test.setProperty(user,user.getPropertyName(),"李四");
            Object obj = test.getProperty(user, user.getPropertyName());
            System.out.println(obj);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    //写一个方法，此方法可将obj对象中名为propertyName的属性的值设置为value.
    public void setProperty(Object obj, String propertyName, Object value) throws NoSuchFieldException, IllegalAccessException {
        Class klass = obj.getClass();
        Field field = klass.getDeclaredField("propertyName");
        field.setAccessible(true);
        field.set(obj, value);
    }

    //写一个方法，此方法可以获取obj对象中名为propertyName的属性的值
    public Object getProperty(Object obj, String propertyName) throws NoSuchFieldException, IllegalAccessException {
        Class klass = obj.getClass();
        Field field = klass.getDeclaredField("propertyName");
        field.setAccessible(true);
        return field.get(obj);
    }
}
class User{
    private String propertyName;

    public User(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
