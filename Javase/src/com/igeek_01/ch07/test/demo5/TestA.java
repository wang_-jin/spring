package com.igeek_01.ch07.test.demo5;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/19 21:16
 * @description TODO
 */
public class TestA {
    /*1. 编写一个类A，定义一个实例方法showString	，用于打印一个字符串。
2. 在编写一个类TestA，用键盘输入一个字符串，该字符串就是类A的全名，使用反射机制创建该类的对象，并  调用该对象中的方法showString
*/
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符串:");
        String str = scanner.next();
        Class klass = Class.forName(str);
        //创建对象
        Object o = klass.newInstance();
        //获取对应方法
        Method method = klass.getMethod("showString");
        //执行方法
        Object obj = method.invoke(o);
    }
}
class A{
    public void showString(){
        System.out.println("你好!");
    }
}
