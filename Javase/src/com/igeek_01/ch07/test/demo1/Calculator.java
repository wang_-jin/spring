package com.igeek_01.ch07.test.demo1;

import org.junit.Test;

/**
 * @author wangjin
 * 2023/9/19 20:09
 * @description TODO
 */
/*1. 设置一个类Calculator，包含4个方法：加、减、乘、除，使用JUnit对4个方法进行单元测试。
2. 在每个方法运行之前创建Calculator对象，在测试方法运行完毕之后将对象设置为null 。
*/
public class Calculator {
    public int sum(int a,int b){
        return a+b;
    }
    public int subtract(int a,int b){
        return a-b;
    }
    public int multiply(int a,int b){
        return a*b;
    }
    public int divide(int a,int b){
        return a/b;
    }
}
