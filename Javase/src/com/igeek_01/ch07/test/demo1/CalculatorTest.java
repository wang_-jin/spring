package com.igeek_01.ch07.test.demo1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    private Calculator calculator;

    @Before
    public void setUp() throws Exception {
        if (calculator == null){
            calculator = new Calculator();
        }
    }

    @After
    public void tearDown() throws Exception {
        calculator = null;
    }

    @Test
    public void sum() {
        int sum = calculator.sum(3, 4);
        System.out.println("sum = " + sum);
    }

    @Test
    public void subtract() {
        int subtract = calculator.subtract(5, 1);
        System.out.println("subtract = " + subtract);
    }

    @Test
    public void multiply() {
        int multiply = calculator.multiply(2, 2);
        System.out.println("multiply = " + multiply);
    }

    @Test
    public void divide() {
        int divide = calculator.divide(9, 3);
        System.out.println("divide = " + divide);
    }
}