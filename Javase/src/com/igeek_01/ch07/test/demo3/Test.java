package com.igeek_01.ch07.test.demo3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author wangjin
 * 2023/9/19 20:57
 * @description TODO
 */
public class Test {
    /*定义一个Student类，用反射去创建一个Student对象，使用两种方式
1. 通过Class对象的方法创建。
2. 通过Constructor对象的方法创建。
*/
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Class<Student> klass = Student.class;
        //方式一:通过Class对象的方法创建
        Student student = klass.newInstance();
        System.out.println("student = " + student);
        //方式二:通过Constructor对象的方法创建
        Constructor<Student> studentConstructor = klass.getConstructor(String.class, int.class);
        Student student1 = studentConstructor.newInstance("张三", 20);
        System.out.println("student1 = " + student1);
    }
}
class Student{
    private String name;
    private int age;


    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Student{name = " + name + ", age = " + age + "}";
    }
}
