package com.igeek_01.ch07.test.demo8;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @author wangjin
 * 2023/9/19 22:58
 * @description TODO
 */
public class Test {
    /*(1)写一个Properties格式的配置文件，配置类的完整名称。 --Spring配置xml文件中配置实例bean的思想
demoClass=com.igeek.DemoClass
(2)写一个程序，读取这个Properties配置文件，获得类的完整名称并加载这个类。
(3)用反射的方式运行run方法。*/
    public static void main(String[] args) throws InvocationTargetException {
        try(
                FileReader fis = new FileReader("config.properties");
        )
        {
            Properties properties = new Properties();
            properties.load(fis);
            String str = properties.getProperty("demoClass");
            Class klass = Class.forName(str);
            Method method = klass.getMethod("run");
            Object obj = klass.newInstance();
            method.invoke(obj);
        } catch (IOException | ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
