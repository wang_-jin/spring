package com.igeek_01.ch07.reflection.constructor;

/**
 * @author wangjin
 * 2023/9/19 14:38
 * @description TODO
 */
public class Student {
    private String name;
    private int age;
    //私有的无参构造方法
    private Student() {
    }

    //公开的 一个参数构造方法
    public Student(String name) {
        this.name = name;
    }

    //公开的 两个个参数构造方法
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
