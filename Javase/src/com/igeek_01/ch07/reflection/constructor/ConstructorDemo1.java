package com.igeek_01.ch07.reflection.constructor;

import java.lang.reflect.Constructor;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/19 14:39
 * @description 通过Class对象获取构造方法
 * 反射之操作构造方法的目的
 *     * 获得Constructor对象来创建类的对象。
 *
 * Constructor类概述
 *     * 类中的每一个构造方法都是一个Constructor类的对象
 *
 * 通过Class对象获取构造方法：
 * 1. Constructor getConstructor(Class... parameterTypes)
 *         * 根据参数类型获得对应的Constructor对象。
 *         * 只能获得public修饰的构造方法
 *  2. Constructor getDeclaredConstructor(Class... parameterTypes)
 *          * 根据参数类型获得对应的Constructor对象，包括private
 *  3. Constructor[] getConstructors()
 *         获得类中的所有构造方法对象，只能获得public的
 *  4. Constructor[] getDeclaredConstructors()
 *         获得类中的所有构造方法对象，包括private修饰的
 */
public class ConstructorDemo1 {
    public static void main(String[] args) throws NoSuchMethodException {
        //1.获取Student的类类型
        Class<Student> klass = Student.class;
        //2.通过Class对象获取 私有的无参构造方法 private Student()
        //通过  getConstructor()来获取 会引发异常NoSuchMethodException 因为该方法只能获取公开的构造方法
        //Constructor<Student> con1 = klass.getConstructor();
        //System.out.println("con1 = " + con1);

        //只能使用getDeclaredConstructor（）来获取 私有的构造方法
        Constructor<Student> con1 = klass.getDeclaredConstructor();
        System.out.println("con1 = " + con1);

        //3.通过Class对象获取 公开的一个参数构造方法  public Student(String name)
        Constructor<Student> con2 = klass.getConstructor(String.class);
        System.out.println("con2 = " + con2);

        //4.通过Class对象获取 公开的2个参数构造方法 public Student(String name,int age)
        Constructor<Student> con3 = klass.getDeclaredConstructor(String.class, int.class);
        System.out.println("con3 = " + con3);

        System.out.println("==================================");
        //getConstructors() 获得类中的所有构造方法对象，只能获得public的
        //getDeclaredConstructors()获得类中的所有构造方法对象，包括private修饰的
        Constructor<?>[] cons1 = klass.getConstructors();
        Stream.of(cons1).forEach(System.out::println);

        System.out.println("--------------------------------------");
        Constructor<?>[] cons2 = klass.getDeclaredConstructors();
        Stream.of(cons2).forEach(System.out::println);
    }
}
