package com.igeek_01.ch07.reflection.constructor;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author wangjin
 * 2023/9/19 14:39
 * @description 利用反射如何创建对象
 * Constructor对象常用方法：
 * 1. T newInstance(Object... initargs)
 *  	根据指定的参数创建对象
 * 2. void setAccessible(true)
 *    设置是否取消权限检查，true取消权限检查，false表示不取消(暴力反射)
 *
 * 利用反射如何创建对象的步骤：
 * 1.获取操作的类的类类型 就是Class对象
 * 2.通过Class对象 获取构造方法Constructor。建议使用getDeclaredConstructor(Class... parameterType)
 * 3.通过Constructor对象 调用newInstance()创建实例。若遇到的私有构造方法则需要先暴力反射setAccessible(true)再去创建实例
 */
public class ConstructorDemo2 {
    //1.使用公开的构造方法创建对象  public Student(String name, int age)
    @Test
    public void testCon1() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //1.获取Student的类类型
        Class<Student> klass = Student.class;
        //2.获取对应的构造方法
        Constructor<Student> constructor = klass.getConstructor(String.class, int.class);
        //3.创建对象
        Student obj = constructor.newInstance("张三", 22);
        System.out.println("obj = " + obj);
    }

    //2.使用私有的构造方法创建对象private Student()
    @Test
    public void testCon2() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        //1.获取Student的类类型
        Class<Student> klass = Student.class;
        //2.获取对应的构造方法
        Constructor<Student> constructor = klass.getDeclaredConstructor();
        //跳过访问权限符的检查(暴力反射)
        constructor.setAccessible(true);
        //3.创建对象
        Student obj = constructor.newInstance();
        System.out.println("obj = " + obj);
    }
}
