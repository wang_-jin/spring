package com.igeek_01.ch07.reflection.field;

import java.lang.reflect.Field;

/**
 * @author wangjin
 * 2023/9/19 16:11
 * @description 利用反射如何给属性赋值
 * Field对象常用方法:
 * 给属性赋值的方法 ：
 * 第一个参数Object obj：要赋值的对象
 * 第二个参数Object value：要赋值的数据
 *
 * 要给引用类型的属性赋值
 * void set(Object obj, Object value)
 *
 * 要给基本数据类型的属性赋值
 * void setInt(Object obj, int i)
 * void setLong(Object obj, long l)
 * void setBoolean(Object obj, boolean z)
 * void setDouble(Object obj, double d)
 *
 * 获取属性值的方法：
 * 第一个参数Object obj：要获取值的对象
 * 返回值：获取到的属性的值
 *
 * 获取引用类型属性的值：
 * Object get(Object obj)
 *
 * 获取基本数据类型的属性的值：
 * int	getInt(Object obj)
 * long getLong(Object obj)
 * boolean getBoolean(Object ob)
 * double getDouble(Object obj)
 *
 * void setAccessible(true);暴力反射，设置为可以直接访问私有类型的属性。
 * Class getType(); 获取属性的类型，返回Class对象。
 *
 * 利用反射如何给属性赋值的步骤：
 * 1.获取类类型 Class对象
 * 2.通过Class对象的getDeclaredField（String name） 获取Field属性对象
 * 3.通过Field对象的set() get()方法 给属性赋值或者 获取值
 *      若遇到操作的是私有属性，先去暴力反射setAccessible(true)
 */
public class FieldDemo2 {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        //获取Person类的类型
        Class<Person> klass = Person.class;
        //创建Person类的对象  所有属性都是默认值
        Person person = klass.newInstance();

        //给私有属性赋值 private String name;
        Field nameField = klass.getDeclaredField("name");
        //先暴力反射 再去给属性赋值
        nameField.setAccessible(true);
        nameField.set(person, "小尼");

        //给公开属性赋值 public int age;
        Field ageField = klass.getField("age");
        ageField.set(person, 22);

        //给静态属性赋值 private static int count;
        Field countField = klass.getDeclaredField("count");
        countField.setAccessible(true);
        countField.setInt(null, 66);

        System.out.println("person = " + person);
        //获取所有属性值
        System.out.println("私有属性的值 = " + nameField.get(person));
        System.out.println("公开属性的值 = " + ageField.getInt(person));
        System.out.println("私有静态属性的值 = " + countField.getInt(null));
    }
}
