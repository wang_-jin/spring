package com.igeek_01.ch07.reflection.field;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/19 16:10
 * @description Class类中与Field相关的方法
 * 反射之操作成员变量的目的
 *     * 通过Field对象给对应的成员/静态变量赋值和取值
 *
 * Field类概述
 *     * 每一个成员/静态变量都是一个Field类的对象。
 *
 * Class类中与Field相关的方法:
 * * Field getField(String name);
 *     *  根据成员变量名获得对应Field对象，只能获得public修饰
 * * Field getDeclaredField(String name);
 *     *  根据成员变量名获得对应Field对象，包含private修饰的
 * * Field[] getFields();
 *     * 获得所有的成员变量对应的Field对象，只能获得public的
 * * Field[] getDeclaredFields();
 *     * 获得所有的成员变量对应的Field对象，包含private的
 */
public class FieldDemo1 {
    //测试Field getField(String name)
    @Test
    public void testField1() throws NoSuchFieldException {
        //获取Person类的类型
        Class<Person> klass = Person.class;
        //获取对应的公开属性 public int age
        Field field = klass.getField("age");
        System.out.println("属性的名称 = " + field.getName()+",属性的类型="+field.getType());
    }

    //测试Field getDeclaredField(String name)
    @Test
    public void testField2() throws NoSuchFieldException {
        //获取Person类的类型
        Class<Person> klass = Person.class;
        //获取对应的私有属性 private String name;
        Field field = klass.getDeclaredField("name");
        System.out.println("属性的名称 = " + field.getName()+",属性的类型="+field.getType());
    }

    //测试Field[] getFields()  只能获得public 包含继承过来的
    @Test
    public void testField3() {
        //获取Person类的类型
        Class<Person> klass = Person.class;
        //获取所有的公开属性
        Field[] fields = klass.getFields();
        Stream.of(fields).forEach(field -> System.out.println("属性的名称 = " + field.getName()+",属性的类型="+field.getType()));
    }

    //测试Field[] getDeclaredFields() 包含private的 不包含继承过来的
    @Test
    public void testField4() {
        //获取Person类的类型
        Class<Person> klass = Person.class;
        //获取所有的属性
        Field[] fields = klass.getDeclaredFields();
        Stream.of(fields).forEach(
                field -> System.out.println("属性的名称 = " + field.getName()+",属性的类型="+field.getType()));
    }
}
