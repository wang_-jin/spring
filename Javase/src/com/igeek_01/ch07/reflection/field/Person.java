package com.igeek_01.ch07.reflection.field;

/**
 * @author wangjin
 * 2023/9/19 16:10
 * @description TODO
 */
class Father{
    public String level;
    private double sum;
}
public class Person extends Father {
    //私有属性
    private String name;
    //公开属性
    public int age;
    //私有静态属性
    private static int count;
    //公开静态属性
    public static final int LEVEL = 100;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}