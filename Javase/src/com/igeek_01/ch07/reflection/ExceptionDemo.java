package com.igeek_01.ch07.reflection;

/**
 * @author wangjin
 * 2023/9/19 16:22
 * @description 反射中的异常总结
 * 反射中的异常总结:
 * ClassNotFoundException   调用Class.forName()时会可能会引发
 * InstantiationException   目标类无法被实例化
 * IllegalAccessException   无法直接操作私有成员
 * NoSuchMethodException    不存在指定的方法
 * InvocationTargetException 通过反射的方法内部有异常抛出但未捕获
 * IllegalArgumentException  非法参数
 * NoSuchFieldException     不存在指定的属性
 */
public class ExceptionDemo {
}
