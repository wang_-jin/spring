package com.igeek_01.ch07.reflection;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

/**
 * @author wangjin
 * 2023/9/19 11:19
 * @description 反射
 * 类加载：第一次使用某个类的时候，将class文件加载到jvm内存中
 * 哪些情况会导致类加载：
 * 1. new A() 类A被加载
 * 2. 执行main方法所在的类 会被加载
 * 3. 使用B.静态方法/静态属性 类B会被加载
 * 4. Class.forName("全类名字符串")
 *
 * 反射是一种机制，利用该机制可以在程序运行过程中对类进行解剖并操作类中的所有成员(成员变量，成员方法，构造方法)
 *
 * 总结：
 * 1.反射是工作在运行时的技术，因为只有运行之后才会有class类对象。
 * 2.反射的核心思想和关键就是得到，编译以后的class对象。
 * 3.反射是在运行时获取类的字节码文件对象，然后可以解析类中的全部成分。
 *
 * 使用反射的前提条件是 获取Class对象（就是某个类的字节码文件对象）
 *
 * 获取Class对象的方式：
 * 1.通过类名.class获得
 * 2.通过对象名.getClass()方法获得
 * 3.通过Class类的静态方法获得： static Class forName("类全名")
 * 注意： 每一个类的Class对象都只有一个，实现单例
 */
class Person extends JFrame implements Serializable,Cloneable {
    public String name;
    public int age;
    public double height;

    public Person() throws HeadlessException {
    }

    public Person(String name, int age, double height) throws HeadlessException {
        this.name = name;
        this.age = age;
        this.height = height;
    }
    public void eat(){
        System.out.println("Person.eat");
    }
    public void eat(String name){
        System.out.println("Person.eat(String name)");
    }
    //内部类
    public static class InnerClass{
        int level;
        public void sleep(){
            System.out.println(level+"...sleep....");
        }
    }
}
public class ReflectDemo1 {
    public static void main(String[] args) {
        try {
            //1.通过Class类的静态方法获得： static Class forName("类全名")--框架中最多用
            Class klass1 = Class.forName("com.igeek_01.ch07.reflection.Person");
            System.out.println("klass1 = " + klass1);
            //利用反射创建实例
            //newInstance() 默认调用的公开的无参构造方法来创建实例
            Object obj = klass1.newInstance();
            System.out.println("obj = " + obj);

            //2.通过类名.class获得 使用场景: 已知类名
            Class<Person> klass2 = Person.class;
            Person person = klass2.newInstance();
            System.out.println("person = " + person);

            //3.通过对象名.getClass()方法获得 使用场景: 通过定义方法,在方法形参中提供对象，在方法内部对象.getClass()方式获取Class对象
            testObj(new String("aaaa"));
            Class klass3 = new Person().getClass();

            //测试Class对象 是单例
            System.out.println(klass1 == klass2);//true
            System.out.println(klass1 == klass3);//true
            System.out.println(klass2 == klass3);//true
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("提供的全类名不正确！");
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    //定义方法 在方法形参中提供对象
    public static void testObj(Object obj) throws InstantiationException, IllegalAccessException {
        //获取obj对象的类类型
        Class klass3 = obj.getClass();
        Object result = klass3.newInstance();
        System.out.println("result = " + result);
    }
}

