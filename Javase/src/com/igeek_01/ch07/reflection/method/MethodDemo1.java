package com.igeek_01.ch07.reflection.method;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/19 15:27
 * @description 通过Class对象获取方法
 * 反射之操作成员方法的目的
 *     * 操作Method对象来调用成员方法
 * Method类概述
 *     * 每一个成员/静态方法都是一个Method类的对象。
 *
 * Class类中与Method相关的方法：
 * * Method getMethod(String name,Class...args);
 *     * 根据方法名和参数类型获得对应的方法对象，只能获得public的
 *
 * * Method getDeclaredMethod(String name,Class...args);
 *     * 根据方法名和参数类型获得对应的方法对象，包括private的
 *
 * * Method[] getMethods();
 *     * 获得类中的所有成员方法对象，返回数组，只能获得public修饰的且包含父类的
 *
 * * Method[] getDeclaredMethods();
 *     * 获得类中的所有成员方法对象，返回数组,只获得本类的，包含private修饰的
 */
public class MethodDemo1 {
    //测试getMethod
    @Test
    public void testMethod1() throws NoSuchMethodException {
        //获取User类的类型
        Class<User> klass = User.class;
        //获取对应的方法public String show(String name,int age)
        Method method = klass.getMethod("show", String.class, int.class);
        System.out.println("method = " + method+",方法名="+method.getName());
    }

    //测试getDeclaredMethod
    @Test
    public void testMethod2() throws NoSuchMethodException {
        //获取User类的类型
        Class<User> klass = User.class;
        //获取方法private void sleep()
        Method method = klass.getDeclaredMethod("sleep");
        System.out.println("method = " + method+",方法的形参个数="+method.getParameterCount());
    }

    //测试getMethods  只能获得public修饰的且包含父类的
    @Test
    public void testMethod3(){
        //获取User类的类型
        Class<User> klass = User.class;
        //获取所有公开的方法 包含继承过来的
        Method[] methods = klass.getMethods();
        Stream.of(methods).forEach(method ->
                System.out.println("方法名="+method.getName()
                        +"，形参情况="+ Arrays.toString(method.getParameterTypes())));
    }

    //测试getDeclaredMethods  只获得本类的，包含private修饰的
    @Test
    public void testMethod4(){
        //获取User类的类型
        Class<User> klass = User.class;
        //获取本类的所有方法 包含私有 不包含继承的
        Method[] methods = klass.getDeclaredMethods();
        Stream.of(methods).forEach(method ->
                System.out.println("方法名="+method.getName()
                        +"，形参情况="+ Arrays.toString(method.getParameterTypes())));
    }
}
