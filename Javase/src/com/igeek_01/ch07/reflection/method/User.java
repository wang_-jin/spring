package com.igeek_01.ch07.reflection.method;

/**
 * @author wangjin
 * 2023/9/19 15:27
 * @description TODO
 */
public class User {
    //私有方法
    private void sleep(){
        System.out.println("sleep...");
    }

    //公开的带形参 带返回值方法
    public String show(String name,int age){
        return "大家好，我叫"+name+",今年"+age;
    }

    //公开的静态方法
    public static void play(String name){
        System.out.println(name+"爱玩王者...");
    }
}
