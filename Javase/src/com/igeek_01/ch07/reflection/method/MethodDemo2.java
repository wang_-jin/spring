package com.igeek_01.ch07.reflection.method;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author wangjin
 * 2023/9/19 15:27
 * @description 利用反射如何执行方法
 * Method对象常用方法：
 * *  Object invoke(Object obj, Object... args)
 *     * 调用指定对象obj的该方法
 *     * args：调用方法时传递的参数
 *     * 返回值是 该方法的执行结果   如果该方法是无返回值类型的则返回null
 * *  void setAccessible(true)
 *     设置是否取消权限检查，true取消权限检查，false表示不取消(暴力反射)
 *
 * 利用反射如何执行方法的步骤：
 * 1.获取类的类型 Class对象
 * 2.通过Class对象 获取对应的Method方法对象
 * 3.利用Method中的invoke(Object obj,Object... args)执行方法，获取方法的执行结果
 *      若遇到执行的是私有方法，先暴力反射 setAccessible(true)
 */
public class MethodDemo2 {
    //1.执行公开成员方法
    @Test
    public void test1() throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        //1.获取User类的类型
        Class<User> klass = User.class;
        //2.获取对应的方法  public String show(String name,int age)
        Method method = klass.getMethod("show", String.class, int.class);
        //3.执行方法
        //创建对象
        User user = klass.newInstance();
        Object result = method.invoke(user, "李四", 18);
        System.out.println("result = " + result);
    }

    //2.执行私有成员方法
    @Test
    public void test2() throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        //1.获取User类的类型
        Class<User> klass = User.class;
        //2.获取对应的方法   private void sleep()
        Method method = klass.getDeclaredMethod("sleep");
        //3.执行方法
        //在执法私有方法之前 需要暴力反射-->取消访问权限检查
        method.setAccessible(true);
        //创建对象
        User user = klass.newInstance();
        Object result = method.invoke(user);
        System.out.println("result = " + result);
    }
    //3.执行静态方法
    @Test
    public void test3() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //1.获取User类类型
        Class klass = Class.forName("com.igeek_01.ch07.reflection.method.User");
        //2.获取对应的方法 public static void play(String name)
        Method method = klass.getDeclaredMethod("play", String.class);
        //3.执行静态方法 第一个参数可以写null  和对象无关
        Object result = method.invoke(null, "张三");
        System.out.println("result = " + result);
    }
}
