package com.igeek_01.ch07.reflection;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * @author wangjin
 * 2023/9/19 14:18
 * @description Class类的常用方法
 * Class类的常用方法：
 * String getSimpleName();           获得类名字符串：类名
 * String getName();                 获得类全名：包名+类名
 * T newInstance() ;                 创建Class对象关联类的对象
 * Class<? super T> getSuperclass()  获取父类类型
 * Package getPackage()              获取此类的包
 * Class<?>[] getDeclaredClasses()   获取所有的内部类
 * Class<?>[] getInterfaces()        获取所有实现的接口
 *
 * InputStream getResourceAsStream(String name) 查找具有给定名称的资源,返回是输入流。
 * URL getResource(String name)                查找带有给定名称的资源。
 * ClassLoader getClassLoader()                返回该类的类加载器。
 */
public class ReflectDemo2 {
    public static void main(String[] args) throws IOException {
        //获取Person类的类类型
        Class<Person> klass = Person.class;

        //获得类名字符串：类名  Person
        System.out.println("简单类名 = " + klass.getSimpleName());
        //获得类全名：包名+类名 ch07.reflection.Person
        System.out.println("全类名 = " + klass.getName());

        //获取父类类型
        Class superclass = klass.getSuperclass();
        System.out.println("父类类型 = " + superclass);

        //获取此类的包
        System.out.println("klass.getPackage() = " + klass.getPackage());

        //获取所有的内部类
        Class<?>[] klasses = klass.getDeclaredClasses();
        System.out.println(Arrays.toString(klasses));

        //获取所有实现的接口
        Class<?>[] interfaces = klass.getInterfaces();
        System.out.println(Arrays.toString(interfaces));

        System.out.println("======================查找带有给定名称的资源==========================");
        //读取与类同级目录下 a.txt
        InputStream input = klass.getResourceAsStream("a.txt");
        System.out.println("input.read() = " + input.read());

        //读取out目录下 与类同级目录下的绝对路径
        String url = klass.getResource("b.txt").getPath();
        System.out.println("url = " + url);

        //读取out目录下  当前项目的绝对路径
        String url2 = klass.getClassLoader().getResource("").getPath();
        System.out.println("url2 = " + url2);

    }
}
