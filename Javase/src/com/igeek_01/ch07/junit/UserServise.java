package com.igeek_01.ch07.junit;

/**
 * @author wangjin
 * 2023/9/19 10:35
 * @description 用户模块的业务逻辑层
 */
public class UserServise {

    /**
     * 用户登录
     * @param name 姓名
     * @param password  密码
     * @return  true代表登录成功 false代表登录失败
     */
    public boolean login(String name,String password){
        if("张三".equals(name) && "123".equals(password)){
            return true;
        }
        return false;
    }

    public void selectOne(String name){
        if("lisi".equals(name)){
            System.out.println("查询成功");
        }else{
            System.out.println("查询失败");
        }
    }

}
