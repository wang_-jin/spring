package com.igeek_01.ch07.hibernate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.stream.Stream;

/**
 * @author wangjin
 * 2023/9/19 16:43
 * @description TODO
 *
 *
 * 模拟hibernate框架的原理：
 * 获取对象的所有属性和值，并存入文件中
 *
 *  object.txt:
 *  name=张三,age=22
 *  major=软件工程,workAge=10
 */


class Student{
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
class Teacher{
    private String major;
    private int workAge;

    public Teacher(String major, int workAge) {
        this.major = major;
        this.workAge = workAge;
    }
}
public class HibernateTest {
    public static void main(String[] args) throws FileNotFoundException {
        Student stu = new Student("张三", 22);
        Teacher teacher = new Teacher("软件工程", 10);

        transfer(stu);
        transfer(teacher);
    }

    public static void transfer(Object obj) throws FileNotFoundException {
        //创建打印流对象
        PrintStream ps = new PrintStream(new FileOutputStream("object.txt",true));
        //改变输出位置 输出到文件中
        System.setOut(ps);

        //1.获取obj对象的类类型
        Class klass = obj.getClass();
        System.out.println("---------"+klass.getSimpleName()+"-----------");
        //2.获取此类型的所有属性 包括private
        Field[] fields = klass.getDeclaredFields();
        //3.获取所有属性的名称以及属性值
        Stream.of(fields).forEach(field -> {
            try{
                //取消访问权限的检查
                field.setAccessible(true);
                //输出 属性名=属性值
                System.out.println(field.getName()+"="+field.get(obj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        System.out.println();
        ps.flush();
        ps.close();
    }
}