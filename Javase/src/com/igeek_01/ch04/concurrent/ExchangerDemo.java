package com.igeek_01.ch04.concurrent;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author wangjin
 * 2023/9/12 16:20
 * @description Exchanger
 * Exchanger（交换者）是一个用于线程间协作的工具类。Exchanger用于进行线程间的数据交换。
 *
 * 构造方法：public Exchanger() 创建一个新的交换机。
 * 常用方法：
 *      V exchange(V x)
 *          等待另一个线程到达这个交换点（除非当前线程 interrupted），然后将给定对象，回报的对象。
 *      V exchange(V x, long timeout, TimeUnit unit)
 *          等待另一个线程到达这个交换点（除非当前线程 interrupted或指定的等待时间的流逝），然后将给定对象，回报的对象。
 *
 *  案例：情侣之间交换礼物
 */
class BoyThread extends Thread{
    private Exchanger<String> exchanger;
    public BoyThread(String name, Exchanger<String> exchanger){
        super(name);
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            //沉睡7秒 模拟让女生线程等待的时间
            Thread.sleep(3000);
            //交换 等待对方线程达到交换点后执行exchanger（）方法 交出去的是礼物A
            //形参：当前线程交给对方的数据  返回值：当前线程收到的数据
            String str = exchanger.exchange("礼物A");
            System.out.println(Thread.currentThread().getName()+"收到了"+str);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
class GirlThread extends Thread{
    private Exchanger<String> exchanger;
    public GirlThread(String name, Exchanger<String> exchanger){
        super(name);
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            //交换 等待对方线程达到交换点后执行exchanger（）方法 交出去的是礼物B
            //形参：当前线程交给对方的数据  返回值：当前线程收到的数据
            //设置交换数据的时间，若超过时间未接受到对方的数据，则会抛出异常TimeoutException
            String str = exchanger.exchange("礼物B",2, TimeUnit.SECONDS);
            System.out.println(Thread.currentThread().getName()+"收到了"+str);
        } catch (InterruptedException | TimeoutException e) {
            e.printStackTrace();
            System.out.println("你不给我，我也不给你！");
        }
    }
}
public class ExchangerDemo {
    public static void main(String[] args) {
        //创建交换机对象
        Exchanger<String> exchanger = new Exchanger<>();
        //启动多线程
        new BoyThread("张三",exchanger).start();
        new GirlThread("如花",exchanger).start();
    }
}
