package com.igeek_01.ch04.concurrent;

import java.util.concurrent.Semaphore;

/**
 * @author wangjin
 * 2023/9/12 19:15
 * @description TODO
 */
public class SemaphoreDemo1 {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        for (int i = 1; i <= 15; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"进入闸机");
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName()+"离开闸机");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();
                }
            },"线程"+i).start();
        }
    }
}
