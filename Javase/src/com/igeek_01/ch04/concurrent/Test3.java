package com.igeek_01.ch04.concurrent;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author wangjin
 * 2023/9/12 18:23
 * @description TODO
 * 4.使用Exchanger+Callable实现：
 * 线程A读取a.txt文件的一行数据，并交给线程B，线程A接受线程B的回报数据，并作为线程A的任务的返回值
 * 线程B读取b.txt文件的一行数据，并交给线程A，线程B接受线程A的回报数据，并作为线程B的任务的返回值
 *
 * 主线程中获取线程A和线程B的执行结果，并计算两个数据之和
 *
 * a.txt:20
 * b.txt:40
 *
 * 获取InputStream的方式：
 * 1. new FileInputStream()
 * 2. System.in
 * 3.当前类.class.getResourceAsStream() 获取 与当前类同级目录下 文件的输入流
 */
class CallableA implements Callable<String>{
    private Exchanger<String> exchanger;

    public CallableA(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public String call()  {
        try(
                //读取与类同级目录下 文件
                InputStream is = CallableA.class.getResourceAsStream("a.txt");
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
        ){
            String str = br.readLine();
            String result = exchanger.exchange(str);
            return result;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
class CallableB implements Callable<String>{
    private Exchanger<String> exchanger;

    public CallableB(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public String call()  {
        try(
                //读取与类同级目录下 文件
                InputStream is = CallableA.class.getResourceAsStream("b.txt");
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
        ){
            String str = br.readLine();
            String result = exchanger.exchange(str);
            return result;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}


/*-----------------------------------------------------------------------*/
class FileA implements Callable<Integer> {
    private Exchanger<Integer> exchanger;

    public FileA(Exchanger<Integer> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public Integer call() throws Exception {
        BufferedReader br = IOUtils.buffer(new FileReader("a.txt"));
        String s = br.readLine();
        Integer numA = Integer.parseInt(s);
        Integer exchange = exchanger.exchange(numA);
        return exchange;
    }
}
class FileB implements Callable<Integer> {
    private Exchanger<Integer> exchanger;

    public FileB(Exchanger<Integer> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public Integer call() throws Exception {
        BufferedReader br = IOUtils.buffer(new FileReader("b.txt"));
        String s = br.readLine();
        Integer numB = Integer.parseInt(s);
        Integer exchange = exchanger.exchange(numB);
        return exchange;
    }
}
public class Test3 {
    /*4.使用Exchanger+Callable实现：
线程A读取a.txt文件的一行数据，并交给线程B，线程A接受线程B的回报数据，并作为线程A的任务的返回值
线程B读取b.txt文件的一行数据，并交给线程A，线程B接受线程A的回报数据，并作为线程B的任务的返回值*/
    public static void main(String[] args) {
/*        File file = new File("a.txt");
        File file1 = new File("b.txt");
        if (!file.exists()&&!file1.exists()){
            try {
                file.createNewFile();
                file1.createNewFile();
                System.out.println("创建成功!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(BufferedWriter bw1 = IOUtils.buffer(new FileWriter("a.txt",true));
            BufferedWriter bw2 = IOUtils.buffer(new FileWriter("b.txt",true));
            )
        {
                bw1.write("hello");
                bw1.flush();
                bw2.write("world");
                bw2.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        Exchanger<Integer> exchanger = new Exchanger<>();
        FileA fileA = new FileA(exchanger);
        FileB fileB = new FileB(exchanger);
        FutureTask<Integer>futureTaskA = new FutureTask<Integer>(fileA);
        FutureTask<Integer>futureTaskB = new FutureTask<Integer>(fileB);
        new Thread(futureTaskA,"线程A").start();
        new Thread(futureTaskB,"线程B").start();
        try {
            System.out.println("futureTaskA = " + futureTaskA.get());
            System.out.println("futureTaskB = " + futureTaskB.get());
            System.out.println("两数之和:"+(futureTaskA.get()+futureTaskB.get()));

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
/*    public static void main(String[] args) {
        //创建交换机
        Exchanger<String> exchanger = new Exchanger<>();
        //开启多线程
        FutureTask<String> a = new FutureTask<>(new CallableA(exchanger));
        new Thread(a).start();

        FutureTask<String> b = new FutureTask<>(new CallableB(exchanger));
        new Thread(b).start();

        //获取线程a b执行的任务的返回值
        try {
            String s1 = a.get();
            String s2 = b.get();
            System.out.println("result="+(Integer.parseInt(s1)+Integer.parseInt(s2)));
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }*/
}

