package com.igeek_01.ch04.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author wangjin
 * 2023/9/12 15:28
 * @description TODO
 * 练习：使用CountDownLatch来实现：
 * 线程1要执行打印：A和C，
 * 线程2要执行打印：B，
 * 但线程1在打印A后，要线程2打印B之后才能打印C，
 *
 * 所以：线程1在打印A后，必须等待线程2打印完B之后才能继续执行。
 * 面试题 - 常见阻塞队列：用来保存等待执行任务的队列
 * 1.ArrayBlockingQueue     基于数组结构的有界阻塞队列,按FIFO排序任务
 * 2.LinkedBlockingQueue    基于链表结构的阻塞队列，按FIFO排序任务。吞吐量通常要高于ArrayBlockingQueue
 * 3.SynchronousQueue       一个不存储元素的阻塞队列，每个插入操作必须等到另一个线程调用移除操作，否则插入操作一直处于阻塞状态，吞吐量通常要高于LinkedBlockingQueue
 * 4.PriorityBlockingQueue  具有优先级的无界阻塞队列
 *
 * 需求：C-->B-->A
 */
public class Test1 {
/*    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(2);

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"打印A");
            countDownLatch.countDown();
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"打印C");
        },"线程A").start();

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"打印B");
                countDownLatch.countDown();
        },"线程B").start();
    }*/

    //静态内部类 生产者 给阻塞队列添加数据  ==> 队尾添加
    public static class Product{
        private LinkedBlockingQueue<Runnable> blockingQueue;

        public Product(LinkedBlockingQueue<Runnable> blockingQueue) {
            this.blockingQueue = blockingQueue;
        }

        public void product(Runnable task){
            try {
                blockingQueue.put(task);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //静态内部类 消费者 在阻塞队列中获取数据  ==>队头元素
    public static class Customer{
        private LinkedBlockingQueue<Runnable> blockingQueue;

        public Customer(LinkedBlockingQueue<Runnable> blockingQueue) {
            this.blockingQueue = blockingQueue;
        }

        public void sale(){
            while(true){
                try {
                    blockingQueue.take().run();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        //创建阻塞队列 FIFO
        LinkedBlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<>();
        //消费者线程
        new Thread(()->{new Customer(blockingQueue).sale();},"消费者").start();
        //生产者线程
        new Thread(()->{
            new Product(blockingQueue).product(()-> System.out.println("C"));
            new Product(blockingQueue).product(()-> System.out.println("B"));
            new Product(blockingQueue).product(()-> System.out.println("A"));
        },"生产者").start();

    }
}
