package com.igeek_01.ch04.concurrent;

import java.util.concurrent.CountDownLatch;

/**
 * @author wangjin
 * 2023/9/12 15:06
 * @description 计数器 -1
 * 构造方法：
 *      CountDownLatch(int count)  需要给定 计数器的初始值count
 * 常用方法：
 *      void await() 使当前线程等待直到 计数为0时 解除阻塞状态
 *      void countDown()  减少锁的数量 每次计数减一
 *
 * 原理：类似join
 * CountDownLatch是通过一个计数器来实现的，每当一个线程完成了自己的任务后，可以调用countDown()让计数减一，
 * 当计数为0时，调用await()方法的所在的线程会解除阻塞状态，继续执行后续的任务。
 *
 * 案例：电影院 5个人走完后 清场
 *
 * 练习：使用CountDownLatch来实现：
 * 线程1要执行打印：A和C，
 * 线程2要执行打印：B，
 * 但线程1在打印A后，要线程2打印B之后才能打印C，
 *
 * 所以：线程1在打印A后，必须等待线程2打印完B之后才能继续执行。
 */
public class CountDownLatchDemo {
    public static void main(String[] args) {
        //创建一个计数器
        CountDownLatch countDownLatch = new CountDownLatch(5);

        //开启5条子线程
        for (int i = 1; i <= 5; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"正在离场....");
                countDownLatch.countDown();//计数 -1
            },"线程"+i).start();
        }

        //等待 直到计数为0 主线程解除阻塞状态
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("大爷大妈打扫卫生");
    }
}
