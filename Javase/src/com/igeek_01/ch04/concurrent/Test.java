package com.igeek_01.ch04.concurrent;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * @author wangjin
 * 2023/9/12 18:06
 * @description TODO
 */
public class Test {
    /*1.请按如下要求编写多线程程序：(作业卷)
		1.得到一个随机的整数n，创建n个子线程对象；
		2.要求在子线程中把当前线程的名称作为元素添加一个集合中；--》CopyOnWriteArrayList
		3.当n个线程的名称都添加到集合中，遍历集合打印每个线程的名称；--》CountDownLatch
*/
    public static void main(String[] args) {

        int a = (int)(Math.random()*5+5);
        CopyOnWriteArrayList<String> arrayList = new CopyOnWriteArrayList<>();
        CountDownLatch countDownLatch = new CountDownLatch(a);
        for (int i = 1; i <= a; i++) {
            new Thread(()->{
                arrayList.add(Thread.currentThread().getName());
            },"线程"+i).start();
        }
//        arrayList.stream().forEach(System.out::println);
        for (int i = 0; i < a; i++) {
            final int temp = i;
            new Thread(()->{
                System.out.println("正在打印"+arrayList.get(temp));
                countDownLatch.countDown();
            }).start();
        }
        try {
            countDownLatch.await();
            System.out.println("打印结束!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
