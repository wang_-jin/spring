package com.igeek_01.ch04.concurrent;

import java.util.concurrent.Semaphore;

/**
 * @author wangjin
 * 2023/9/12 16:11
 * @description Semaphore
 * Semaphore（发信号）的主要作用是控制线程的并发数量。
 *
 * 构造方法：
 *      public Semaphore(int permits)				permits 表示许可线程的数量
 *      public Semaphore(int permits, boolean fair)	    fair 表示公平性，如果这个设为 true 的话，下次执行的线程会是等待最久的线程
 * 常用方法：
 *      public void acquire() throws InterruptedException	    表示获取许可
 *      public void release()								    无论是否有异常产生，都需要释放许可
 *
 * Semaphore和synchronized的区别：
 * 1.Semaphore 根据创建对象时指定的并发线程数量，并发执行任务
 * 2.synchronized 互斥排他锁，每次至多只能有一个线程进行操作
 *
 *      案例：一次允许5个人过闸机
 */
public class SemaphoreDemo {
    public static void main(String[] args) {
        //创建Semaphore 对象
        Semaphore semaphore = new Semaphore(5);//一次最多五个

        //多线程并发执行
        for (int i = 1; i <= 15; i++) {
            new Thread(()->{
                //获取许可
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+"进入闸机,进入检查...");
                    Thread.sleep(2000);
                    System.out.println(Thread.currentThread().getName()+"离开闸机,结束检查...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally{
                    //释放许可 ==> 释放锁
                    semaphore.release();
                }
            },"线程"+i).start();
        }
    }
}
