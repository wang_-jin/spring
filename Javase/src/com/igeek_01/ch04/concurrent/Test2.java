package com.igeek_01.ch04.concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author wangjin
 * 2023/9/12 15:37
 * @description TODO
 *3.使用CyclicBarrier实现：集齐七颗龙珠，召唤神龙
 */
public class Test2 {
    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier(7,()-> System.out.println("召唤神龙!!!"));
        for (int i = 1; i <= 7 ; i++) {
            new Thread(()->{
                System.out.println("收集"+Thread.currentThread().getName());
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },"龙珠"+i).start();
        }
    }
}
