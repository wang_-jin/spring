package com.igeek_01.ch04.concurrent;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author wangjin
 * 2023/9/12 15:15
 * @description 计数器 +1
 * 构造方法：
 *  public CyclicBarrier(int parties, Runnable barrierAction)
 *      第一个参数int parties 必须达到屏障点的线程数量
 *      第二个参数Runnable barrierAction ：  回调函数 达到屏障之后要执行的任务
 * 常用方法：
 *      public int await()// 每个线程调用await方法告诉CyclicBarrier我已经到达了屏障，然后当前线程被阻塞
 *
 *
 * CountDownLatch和CyclicBarrier的区别：
 *  1.CountDownLatch 计数器 -1
 *    await()会让调用方法所在的调用进入到阻塞状态，直到计数为0时才会解除阻塞。
 *
 *  2.CyclicBarrier 计数器 +1
 *    await()会将计数+1,阻塞的是子线程，达到总计数时会执行回调任务，并且可以循环执行。
 *
 * 案例：开会
 */
public class CyclicBarrierDemo {
    public static void main(String[] args) {
        //创建 CyclicBarrier对象
        CyclicBarrier barrier = new CyclicBarrier(5,()-> System.out.println("可以开会了!"));

        for (int i = 1; i <= 15; i++) {//超过五个是因为并发了
            final int temp = i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"第"+ temp +"个到达会议室了!");
                //计数器 +1 直到达到parties数量时，会执行回调函数，而且可以循环执行任务
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                //执行完回调任务之后 解除阻塞状态
/*                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                System.out.println(Thread.currentThread().getName()+"正在开会中....");
            },"员工"+i).start();
        }
    }
}
