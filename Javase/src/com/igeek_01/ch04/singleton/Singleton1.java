package com.igeek_01.ch04.singleton;

/**
 * @author wangjin
 * 2023/9/12 14:24
 * @description 线程安全的单例模式
 * 饿汉式单例：线程安全
 * 懒汉式单例：线程不安全
 *
 * 线程安全的单例模式
 * 1.DCL(double check lock 双重检查锁)懒汉式单例
 * 2.静态内部类的单例模式
 *
 * 并发的三特性：可见性 原子性 指令重排性
 */
public class Singleton1 {
    //构造方法私有
    private Singleton1(){}
    //静态变量 保存本类唯一的实例 ==> 可见性
    private static Singleton1 instance;
    //公开的静态方法 获取本类的唯一实例
    public static Singleton1 getInstance(){
        if (instance==null) {
            synchronized (Singleton1.class) {
                if (instance==null) {
                    try {
                        Thread.sleep(1000);//对象锁不会释放
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //synchronized 保证原子性:1.先new对象 2.声明变量instance 3.将对象赋值给引用变量instance
                    //禁止指令重排: 期望执行顺序123 ==> 通过volatile禁止重排序
                    instance = new Singleton1();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        //单线程的运行：
//        Singleton1 s1 = Singleton1.getInstance();
//        System.out.println("s1 = " + s1);//1b6d3586
//        Singleton1 s2 = Singleton1.getInstance();
//        System.out.println("s2 = " + s2);//1b6d3586


        //多线程并发获取单例对象
        for (int i = 0; i < 650; i++) {
            new Thread(()->{
                Singleton1 instance = Singleton1.getInstance();
                System.out.println(Thread.currentThread().getName()+"获得单例为"+instance);
            },"线程"+i).start();
        }
    }
}
