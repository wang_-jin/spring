package com.igeek_01.ch04.singleton;

import com.igeek_01.ch06.tcp.tcp07.ThreadPool;

/**
 * @author wangjin
 * 2023/9/13 9:48
 * @description 饿汉式单例
 * 特点：线程安全，在类第一次加载的时候，创建单例对象 ==> jvm保证了线程安全
 * 缺点：容易产生垃圾对象
 */
public class Singleton3 {
    private Singleton3(){}

    private static Singleton3 instance = new Singleton3();

    public static Singleton3 getInstance(){
        return instance;
    }

    public static void m1(){}
    public static void m2(){}
    public static void m3(){}

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            new Thread(()->{
                Singleton3 instance = Singleton3.getInstance();
                System.out.println(Thread.currentThread().getName()+"获得单例为 " + instance);
            },"线程"+i).start();
        }
    }
}

