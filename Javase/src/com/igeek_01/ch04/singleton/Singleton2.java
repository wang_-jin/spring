package com.igeek_01.ch04.singleton;

/**
 * @author wangjin
 * 2023/9/12 14:35
 * @description 静态内部类的单例模式
 * 特点：线程安全，利用静态内部类有作用于延迟加载，效率较高
 */
//如果只使用外部类时，只会装载外部类，不会装载内部类
    /*同一个加载器下，一个类型只会初始化一次*/
public class Singleton2 {
    //构造方法私有
    private Singleton2(){}
    //静态内部类: 只有装载内部类时，才会创建单例对象
    private static class InnerClass{
        private static Singleton2 instance = new Singleton2();
    }
    //公开的静态方法获取本类实例
    public static Singleton2 getInstance(){
        return InnerClass.instance;
    }

    public static void main(String[] args) {
        for (int i = 1; i <=50 ; i++) {
            new Thread(()->{
                Singleton2 instance = Singleton2.getInstance();
                System.out.println(Thread.currentThread().getName()+"获得单例为"+instance);
            },"线程"+i).start();
        }
    }

}
