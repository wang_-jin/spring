package com.igeek_01.ch04.collections;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangjin
 * 2023/9/12 11:25
 * @description ConcurrentHashMap
 * 数据结构：
 * HashMap  jdk1.8之前    底层实现 数组+链表
 * HashMap  jdk1.8之后    底层实现 数组+链表+红黑树
 *
 *
 * HashMap          多线程并发操作集合时，造成线程不安全
 * Hashtable        线程安全，所有的方法都添加synchronized同步方法，锁的是整张哈希表，相对来说效率较低
 * ConcurrentHashMap 线程安全,add,remove方法中添加synchronized同步代码块，get方法没有锁，相对来说效率较高
 *
 *操作线程安全的map:
 * 方式1：Collections.synchronizedMap（）
 * 方式2：Hashtable 锁的是整张哈希表
 * 方式3：ConcurrentHashMap 局部锁定(分段锁):不会锁定整张哈希表，锁的某个哈希桶
 *

 * 面试题 - JDK1.7与JDK1.8中ConcurrentHashMap的区别
 * 1.数据结构:
 *      取消了Segment分段锁的数据结构，取而代之的是数组+链表+红黑树的结构。
 * 2.保证线程安全机制:
 *      JDK1.7采用Segment的分段锁机制实现线程安全，其中 Segment继承自ReentrantLock ：
 *
 *      JDK1.8采用CAS+synchronized保证线程安全：
 *      JDK1.8中只需要一次定位，采用CAS+synchronized，计算key的hash找到哈希表的位置，如果对应下标处没有结点，说明没有发生哈希冲突，此时直接通过CAS进行插入。如果对应下标处有节点存在哈希冲突，则使用synchronized加锁。锁定的是哈希桶。
 *
 * 3.锁的粒度:
 *      JDK1.7是对需要进行数据操作的Segment加锁。
 *      JDK1.8调整为对每个数组元素加锁(HashEntry)，锁的粒度从段锁缩小为节点锁。
 * 4.查询时间复杂度:
 *      从JDK1.7的遍历链表O(n)，JDK1.8变成遍历红黑树O(logN)。
 *
 *
 * 面试题 - 总结一下put流程
 * 1.校验Key,value是否为空。如果有一个为null，那么直接报NullPointerException异常。所以可以得出concurrentHashMap中Key，value不能为空。
 * 2.循环尝试插入。进入循环。
 * 3.case1:如果没有初始化就先调用initTable()方法来进行初始化过程
 * 4.case2:根据Hash值计算插入位置(n - 1)&hash=i。如果没有hash冲突,也就是说插入位置上面没有数据，就直接casTabAt()方法将数据插入。
 * 5.case3:插入位置上有数据。数据的头节点的哈希地址MOVED为-1(即链表的头节点为ForwardingNode节点)，则表示其他线程正在对table进行扩容(transfer)，就先等着,等其他线程扩容完了咱们再尝试插入。
 * 6.case4:上面情况都没有。就对首节点加synchronized锁来保证线程安全，两种情况，一种是链表形式就直接遍历到尾端插入，一种是红黑树就按照红黑树结构插入，结束加锁。
 * 7.如果Hash冲突时会形成Node链表，在链表长度超过8，Node数组超过64时会将链表结构转换为红黑树的结构。
 * 8.break退出循环。
 * 9.调用addCount()方法统计Map已存储的键值对数量size++，检查是否需要扩容，需要扩容就扩容。
 *
 * 面试题 - 并发情况下，各线程中的数据可能不是最新的，那为什么get方法不需要加锁?
 * 答:transient volatile Node<K,V>[] table;
 * final K key;  volatile V val;  volatile Node<K,V> next;
 * get操作全程不需要加锁是因为Node的成员val是用volatile修饰的，在多线程环境下线程A修改结点的val或者新增节点的时候是对线程B可见的。

面试题 - Hashtable和 ConcurrentHashMap的区别：
1.Hashtable java.util包下 线程安全的，所有的方法都是同步方法synchronized，锁定的是整张哈希表,相对效率较低
2.ConcurrentHashMap juc并发包下 线程安全，put()方法中采用CAS+synchronized同步代码块锁定某个哈希桶(局部锁定),相对效率较高
 */
public class ConcurrentHashMapDemo1 {
    public static void main(String[] args) {
        //使用HashMap 并发操作集合 会引发 并发修改异常ConcurrentModificationException
        HashMap<String,String> map = new HashMap<>();

        //方式1：Collections.synchronizedMap（）将线程不安全的集合转换为线程安全的集合
        //实现线程安全的原理：所有的方法都添加synchronized同步代码块，锁的是全局map，相对来说效率较低
        Map<String,String> map1 = Collections.synchronizedMap(map);

        //方式2：Hashtable
        Hashtable<String,String> map2 = new Hashtable<>();

        //方式3: ConcurrentHashMap
        ConcurrentHashMap<String,String> map3 = new ConcurrentHashMap<>();

        //多线程并发操作集合
        for (int i = 0; i < 50; i++) {
            final int temp = i;
            //int temp = i;////jdk1.8之后默认使用 final来修饰temp变量
            new Thread(()->{
                //局部内部对象中使用 外部的局部变量，将局部变量一定要使用final修饰，从而可以使得延长final局部变量的生命周期
                //原理：局部内部对象中出现 final变量，将final局部变量拷贝一份副本作为对象的成员属性来使用的。
                //key线程名 value:hello1
                map3.put(Thread.currentThread().getName(),"hello"+temp);
                System.out.println(map3);
            },"线程"+i).start();
        }
    }
}
