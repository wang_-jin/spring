package com.igeek_01.ch04.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author wangjin
 * 2023/9/12 9:44
 * @description 线程安全的集合类

 * ArrayList  线程不安全
 *
 * 线程安全的集合类
 * Vector ：线程安全的，所有的方法都添加synchronized,效率低。 位于java.util包
 * CopyOnWriteArrayList:线程安全的，针对add,remove,set等写操作使用Reentrant锁，读操作的方法是无锁的，效率较高 位于java.util.concurrent包

当使用集合对象.add() remove() 操作集合时，为何造成ConcurrentModificationException 并发修改异常？
1.add添加 remove删除 都会修改modCount++ 记录集合对象修改的次数
2.执行Iterator的next方法时，会进行检测 expectedModCount != modCount;throw new ConcurrentModificationException
3.使用增强foreach循环，forEach方法，实现遍历的本质就是Iterator的next方法，是一样的。

如何解决ConcurrentModificationException 并发修改异常？
1.单线程的情况，ArrayList集合，get(int index) fori循环通过索引来遍历
2.单线程的情况，遍历ArrayList集合中，使用Iterator中的remove()方法来进行删除元素
3.多线程的并发情况，直接使用JUC并发包下CopyOnWriteArrayList 线程安全的集合
 */
public class CopyOnWriteArrayListDemo {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();

        list.add("a");//modCount=1
        list.add("b");//modCount=2
        list.add("c");//modCount=3

        /*Iterator<String> it = list.iterator();//expectedModCount = modCount = 3
        while(it.hasNext()){
            System.out.println(it.next());//expectedModCount != modCount;throw new ConcurrentModificationException
            list.add("hhhh");//modCount=4 使用迭代器的遍历过程中，不能修改集合，会引发异常ConcurrentModificationException。只能使用Iterator中的remove()
        }*/

       /* for (String s : list) {
            System.out.println("s="+s);
            list.add("hhhh");
        }*/

        list.forEach(s->{
            System.out.println("s="+s);
            list.add("999");
        });

        //多线程操作集合  多线程添加元素时，引发并发修改异常ConcurrentModificationException
//        for (int i = 1; i <=50 ; i++) {
//            new Thread(()->{
//                //将线程的名称添加到list集合中
//                list.add(Thread.currentThread().getName());
//                System.out.println("list = " + list);
//            },"线程"+i).start();
//        }
    }
}
