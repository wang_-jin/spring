package com.igeek_01.ch04.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wangjin
 * 2023/9/12 10:26
 * @description TODO
 * 线程安全的集合操作：
 * 方式1：通过Collections集合工具类的synchronizedList,提供将线程不安全的集合 转换为 线程安全的集合
 * 实现线程安全的原理：将所有的方法都添加了synchronized同步代码块，相对来说效率较低
 *
 * 方式2：Vector集合
 * 实现线程安全的原理：所有的方法都添加synchronized同步方法,效率低
 *
 * 方式3：CopyOnWriteArrayList集合
 * 实现线程安全的原理：add,remove方法等写操作，都使用ReentrantLock可重入锁，
 *  其次操作的共享变量Object[] array是使用volatile修饰的。
 *  其中读操作相关的方法都是无锁的，相对来说效率较高
 *
 * CopyOnWrite的机制：
 * 核心思想：读写分离，空间换时间，避免为保证并发安全导致的激烈的锁竞争。
 *
 * 1、CopyOnWrite适用于读多写少的情况，最大程度的提高读的效率；
 *  public E get(int index) {
 * 	    return get(getArray(), index); //无锁
 *  }
 *
 * 2、CopyOnWrite是最终一致性，在写的过程中，原有的读的数据是不会发生更新的，只有新的读才能读到最新数据；
 *
 * 3、如何使其他线程能够及时读到新的数据，需要使用volatile变量；
 * private transient volatile Object[] array;
 *
 * 4、写的时候不能并发写，需要对写操作进行加锁；
 * add():lock.lock();操作集合;lock.unlock()
 *
 * CopyOnWriteArraySet 底层实现和CopyOnWriteArrayList是一样的，因为内置属性CopyOnWriteArrayList al;
 */
public class CopyOnWriteArrayListDemo2 {
    public static void main(String[] args) {
        ArrayList<String> list =  new ArrayList<>();
        //方式一:通过Collections集合类的synchronizedList,提供将线程不安全的集合 转换为 线程安全的集合
        //实现线程安全的原理:将所有的方法都添加了synchronized同步代码块,相对来说效率较低
        List<String> list1 = Collections.synchronizedList(list);
        //方式二: Vector集合
        //实现线程安全的原理:所有的方法都添加synchronized同步方法,效率低
        Vector<String> list2 = new Vector<>();
        //方式三:CopyOnWriteArrayList集合
        //实现线程安全的原理:add,remove方法等写操作,都使用ReentrantLock可重入锁,
        //其次操作的共享变量Object[] array是使用volatile修饰的.
        //其中读操作相关的方法都是无锁的,相对来说效率较高
        CopyOnWriteArrayList<String> list3 = new CopyOnWriteArrayList<>();
        //多线程操作集合  多线程添加元素时，引发并发修改异常ConcurrentModificationException
        for (int i = 1; i <=50 ; i++) {
            new Thread(()->{
                //将线程的名称添加到list集合中
                list1.add(Thread.currentThread().getName());
                System.out.println("list = " + list1);
            },"线程"+i).start();
        }
    }
}
