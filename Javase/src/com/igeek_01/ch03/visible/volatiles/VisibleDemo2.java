package com.igeek_01.ch03.visible.volatiles;


/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/9/11 10:10
 */
public class VisibleDemo2 {
    public static void main(String[] args) {
        MyRunnable2 myRunnable1 = new MyRunnable2();
        //创建线程1:flag写动作
        new Thread(myRunnable1,"线程1").start();

        //主线程:读取flag值
        while(true){
                if (myRunnable1.isFlag()) {
                    System.out.println("main主线程感知到了flag改变~~~~");
                }
        }
    }
}
