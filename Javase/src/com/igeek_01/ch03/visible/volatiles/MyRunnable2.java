package com.igeek_01.ch03.visible.volatiles;

/**
 * @Description TODO
 * @Author fqq
 * @Date 2023/9/11 10:10
 */
public class MyRunnable2 implements Runnable {
    //共享变量：  类变量，实例变量 都存储在主内存中的
    //volatile保证共享变量的可见性
    private volatile boolean flag = false;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //int i = 10;局部变量。是某个线程的私有的
        flag = true;//写操作：1.读取主内存中的变量，拷贝副本到当前线程的工作内存中 2.重新赋值  3.更新后的值刷回到主内存中
        System.out.println(Thread.currentThread().getName()+"改变了flag值："+flag);
    }
}
