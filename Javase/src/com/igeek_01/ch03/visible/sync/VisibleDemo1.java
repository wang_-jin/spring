package com.igeek_01.ch03.visible.sync;

/**
 * @author wangjin
 * 2023/9/11 9:42
 * @description 可见性
 * 引发：
 *      多个线程访问共享变量，会出现一个线程修改变量的值后，其他线程看不到变量最新值的情况。
 * 总结：
 *      并发编程下，多线程修改变量，会出现线程间变量的不可见性。
 *不可见性原因：
 *      每个线程都有自己的工作内存，线程都是从主内存拷贝共享变量的副本值。
 *      每个线程都是在自己的工作内存中操作共享变量的。
 * 解决方法：
 *      方式一：加锁synchronized：在线程获取到锁时，会清空自己的工作内存，再从主内存中读取最新的值
 *      方式二：对共享的变量进行volatile关键字修饰
 *              一旦有线程改变了volatile修饰共享变量的值，
 *              其他线程会立即会失效，其他线程如果要读取共享变量，只能从主内存中重新读取最新的值
 *
 *volatile与synchronized的区别
 * 1).volatile只能修饰实例变量和类变量，而synchronized可以修饰方法，以及代码块。
 * 2). volatile保证数据的可见性，但是不保证原子性(即：多线程进行写操作，不保证线程安全)；
 *    而synchronized是一种排他（互斥）的机制，实现线程安全。
 * 3).从性能上说，volatile更好点，仅仅是对实现线程间变量的可见性上。
 */
public class VisibleDemo1 {
    public static void main(String[] args) {
        MyRunnable1 myRunnable1 = new MyRunnable1();
        //创建线程1:flag写动作
        new Thread(myRunnable1,"线程1").start();

        //主线程:读取flag值
        while (true) {
            //方式一: 加锁synchronozed 在线程获取到锁时,会清空自己的工作内存,再从主内存中读取最新的值
            synchronized ("A") {
                if (myRunnable1.isFlag()) {
                    System.out.println("主线程感受到flag的改变");
                }
            }
        }
    }
}
