package com.igeek_01.ch03.atomic;

/**
 * @author wangjin
 * 2023/9/11 10:37
 * @description 并发三特性--原子性
 *
 * 出现非原子性的原因:
 *      多线程并发操作共享变量时,不能保证以下动作的原子性操作:
 *      count++:1. 从主内存中读取共享变量 2.运行count++ 3.从工作内存中写回到主内存中
 * 解决方法：
 * 方法1：加锁synchronized  悲观锁
 * 方法2：原子类             CAS比较并交换机制  乐观锁
 */
class MyRunnable1 implements Runnable {
    //共享变量 volatile不能保证共享变量具有原子性,适合读多写少的并发场景
    private  int count = 0;

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            //synchronized 是一种互斥排他锁可以保证多线程写操作时,实现线程安全
            synchronized (this) {
                //1. 从主内存中读取共享变量 2.运行count++ 3.从工作内存中写回到主内存中
                count++;
                System.out.println("count = " + count);
            }
        }
    }
}
public class AtomicDemo1 {
    public static void main(String[] args) {
        MyRunnable1 myRunable1 = new MyRunnable1();
        for (int i = 0; i < 100; i++) {
            new Thread(myRunable1, "线程" + i).start();
        }
    }
}
