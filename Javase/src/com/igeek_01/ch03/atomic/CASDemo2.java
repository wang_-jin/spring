package com.igeek_01.ch03.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/9/11 14:06
 * @description CAS 会出现ABA问题
 */
public class CASDemo2 {
    public static void main(String[] args) {
        //原子类的实例
        AtomicInteger atomicInteger = new AtomicInteger(10);

        new Thread(()->{
            boolean flag1 = atomicInteger.compareAndSet(10,11);
            System.out.println("flag1 = " + flag1);
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());

            boolean flag2 = atomicInteger.compareAndSet(11,10);
            System.out.println("flag2 = " + flag2);
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());
        }).start();

        new Thread(()->{
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag3 = atomicInteger.compareAndSet(10,66);
            System.out.println("flag3 = " + flag3);
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());
        }).start();
    }
}
