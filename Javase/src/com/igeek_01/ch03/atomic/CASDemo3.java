package com.igeek_01.ch03.atomic;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author wangjin
 * 2023/9/11 14:11
 * @description 会出现ABA问题（此时建议引入原子引用，使用带版本号的原子操作，例如：AtomicStampedReference中的getStamp()方法）
 * 解决方法：使用AtomicStampedReference类，比较并交换的时候 带有版本号的校验
 *
 * 构造方法：
 * public AtomicStampedReference(V initialRef, int initialStamp)
 * V initialRef；初始值
 * int initialStamp：初始版本号
 *
 * 常用方法：
 * boolean compareAndSet(V   expectedReference,V   newReference,int expectedStamp,int newStamp)
 * V   expectedReference    期望值
 * V   newReference         更新值
 * int expectedStamp        期望版本号
 * int newStamp             更新后的版本号
 */
public class CASDemo3 {
    public static void main(String[] args) {
        AtomicStampedReference<Integer> asr = new AtomicStampedReference<>(10, 1);
        //线程1
        new Thread(()->{
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("第一次更新前的版本号------"+asr.getStamp());
            boolean flag = asr.compareAndSet(10, 11,
                    asr.getStamp(), asr.getStamp() + 1);
            System.out.println("flag = " + flag);
            System.out.println("主内存中最新值 = "+asr.getReference());
            System.out.println("第一次更新后的版本号------"+asr.getStamp());


            System.out.println("第二次更新前的版本号------"+asr.getStamp());
            boolean flag1 = asr.compareAndSet(11, 10,
                    asr.getStamp(), asr.getStamp() + 1);
            System.out.println("flag1 = " + flag1);
            System.out.println("主内存中最新值 = "+asr.getReference());
            System.out.println("第一次更新后的版本号------"+asr.getStamp());
        }).start();
        //线程2
        new Thread(()->{
            int stamp = asr.getStamp();//初始版本号
            System.out.println("stamp = " + stamp);

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag2 = asr.compareAndSet(10, 66,
                    stamp, stamp + 1);
            System.out.println("flag2 = " + flag2);
            System.out.println("主内存中最新值 = "+asr.getReference());
            System.out.println("第一次更新后的版本号------"+asr.getStamp());
        }).start();
    }
}
