package com.igeek_01.ch03.atomic;

import com.igeek_01.ch03.visible.sync.MyRunnable1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/9/11 10:55
 * @description 原子类
 * 原子类 AtomicInteger
 *       class AtomicInteger extends Number
 *
 * 构造方法：
 * public AtomicInteger()：	   				初始化一个默认值为0的原子型Integer
 * public AtomicInteger(int initialValue)： 初始化一个指定值的原子型Integer
 * 常用方法：
 * int getAndIncrement():      			    以原子方式将当前值加1，注意，这里返回的是自增前的值。
 * int incrementAndGet():    				 以原子方式将当前值加1，注意，这里返回的是自增后的值。
 */
class MyRunnable2 implements Runnable{
    //创建原子类的实例
    private AtomicInteger atomicInteger = new AtomicInteger();
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            //保证原子性: 1.从主存中读取共享变量 2.运行count++ 3.从工作内存中写回主内存中
            int count = atomicInteger.incrementAndGet();//++count
            System.out.println("count = "+count);
        }
    }

}
public class AtomicDemo2 {
    public static void main(String[] args) {
        MyRunnable2 myRunnable2 = new MyRunnable2();
        for (int i = 0; i < 100; i++) {
            new Thread(myRunnable2).start();
        }
    }
}
