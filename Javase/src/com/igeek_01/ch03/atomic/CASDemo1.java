package com.igeek_01.ch03.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangjin
 * 2023/9/11 11:33
 * @description CAS 比较并交换原理
 * 可以使用AtomicInteger类中的public  final  boolean  **compareAndSet**(int expect , int update)方法进行校验。
 * 参数1 int expect：期望值
 * 参数2 int update：更新值
 *
 * CAS比较并交换的原理：
 * 工作线程中的写回主内存的值，必须先和主内存的实际值进行比较
 * 若一致 则写回成功
 * 若不一致，自旋获取主内存中的实际值，再进行后续计算和比较
 */


public class CASDemo1 {
    public static void main(String[] args) {
        //主内存的初始值是10
        AtomicInteger atomicInteger = new AtomicInteger(10);

        new Thread(()->{
            boolean flag1 = atomicInteger.compareAndSet(10,11);
            System.out.println("flag1 = " + flag1);//true 一致 则写回成功
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());//获取主内存中的最新值11

            boolean flag2 = atomicInteger.compareAndSet(10,12);
            System.out.println("flag2 = " + flag2);//true 不一致 更新失败
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());//获取主内存中的最新值11
            boolean flag3 = atomicInteger.compareAndSet(11,13);
            System.out.println("flag3 = " + flag3);//
            System.out.println("atomicInteger.intValue() = " + atomicInteger.intValue());//获取主内存中的最新值13

        }).start();
    }
}
