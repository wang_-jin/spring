package com.igeek_01.ch03.barrier;

/**
 * @author wangjin
 * 2023/9/11 14:46
 * @description 指令重排
 * 如何禁止 指令重排？
 *
 *
 * 以下代码可能出现的执行结果
 * x=0,y=1  线程A先执行
 * x=2,y=0  线程B先执行
 * x=0,y=0  指令重排序
 *
 * 下面是基于保守策略的JMM内存屏障插入策略：
 * 在每个volatile写操作的前面插入一个StoreStore屏障。
 * 在每个volatile写操作的后面插入一个StoreLoad屏障。
 * 在每个volatile读操作的后面插入一个LoadLoad屏障。
 * 在每个volatile读操作的前面插入一个LoadStore屏障。
 *
 * 面试题：volatile 和 synchronized的区别：
 * 1.volatile 支持可见性 指令重排 ，不能保证原子性
 * 2.synchronized 支持可见性，原子性
 */
public class BarrierDemo {
    //共享变量
    //一旦volatile修饰共享变量,可以在变量发生读写时,添加内存屏障,以防止发生指令重排序
    private static /*volatile*/ int x = 0;
    private static /*volatile*/ int y = 0;
    private static /*volatile*/ int a = 0;
    private static /*volatile*/ int b = 0;

    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        while(true){
            i++;

            x=0;y=0;a=0;b=0;
            //开两条线程 模拟处理A 处理器B
            Thread thA = new Thread(()->{
                //可能会发生指令重排序: 期望的顺序: 1 2 可能会出现的顺序: 2 1
                //==> volatile写 必须按照顺序 12来执行
                a = 1;//1.volatile写
                x = b;//2.volatile读
            });
            Thread thB = new Thread(()->{
                //可能会发生指令重排序: 期望的顺序: 1 2 可能会出现的顺序: 2 1
                b = 2;//1.volatile写
                y = a;//2.volatile读
            });
            //等待A ,B执行完毕后在输入显示
            thA.start();
            thB.start();
            thA.join();
            thB.join();

            if (x == 0 && y ==0){
                System.out.println("第"+i+"次的执行x="+x+",y="+y);
            }else {
                System.out.println("第"+i+"次的执行x="+x+",y="+y);
            }

        }
    }
}
