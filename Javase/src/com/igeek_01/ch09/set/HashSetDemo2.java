package com.igeek_01.ch09.set;

import java.util.HashMap;

/**
 * @author wangjin
 * 2023/9/21 18:01
 * @description 通过构造方法创建对象时，HashMap<E,Object>map = new HashMap<>() 为何添加Object类型的PRESENT作为value？？？
 * 通过构造方法创建对象时，HashMap<E,Object>map = new HashMap<>() 为何添加Object类型的PRESENT作为value？？？
 * 为了更准确得到remove(E e)移除后的结果，希望返回true表示移除成功，返回false代表移除失败。
 * 如果将null作为value值，很难准确表示移除成功与否的信号。
 */
//自定义HashSet集合
class MyHashSet<T>{
    private HashMap<T,Object> map;

    public MyHashSet() {
        this.map = new HashMap<>();
    }
    //定义常量 作为hashMap中添加的value值
    private static final Object PRESENT = new Object();

    //添加元素 ==》此处添加元素不用null
    public boolean add(T e){
        //map.put(key,value)如果添加成功则返回value值  如果添加失败则返回null
        Object value = map.put(e, PRESENT);
        return value!=null;
    }
    //删除元素
    public boolean remove(T e){
        //map.remove(key) 如果删除成功则返回的是被删除的key相关联值  如果删除失败则返回null
        Object value = map.remove(e);
        return value!=null;
    }

    //集合的元素数量
    public int size(){
        return map.size();
    }
}
class A{}
public class HashSetDemo2 {
    public static void main(String[] args) {
        A a = new A();
        A b = new A();
        //添加元素
        MyHashSet<A> hashSet = new MyHashSet<>();
        hashSet.add(a);//map.put(a,null)
        hashSet.add(b);//map.put(b,null)
        System.out.println("hashSet.size() = " + hashSet.size());
        //删除元素
        System.out.println(hashSet.remove(a));//true
        System.out.println(hashSet.remove(b));//true
        System.out.println("hashSet.size() = " + hashSet.size());
    }
}