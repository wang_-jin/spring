package com.igeek_01.ch09.set;

import java.util.HashSet;

/**
 * @author wangjin
 * 2023/9/21 14:04
 * @description HashSet
 * HashSet
 * 1. jdk8 之前 数据结构是  数组+链表
 *    jdk8 之后 数据结构是  数组+链表+红黑树
 * 2.不允许存放重复的元素  依据hashCode()和equals()
 * 3.线程不安全
 * 4.存取无序
 * 5.HashMap的key存储原理 和 HashSet的值存储原理 是相同的
 *
 *
 * 添加元素的核心代码：
 *
 * 哈希表： transient Node<K,V>[] table
 * 数组默认初始容量：static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
 * 默认负载因子：  static final float DEFAULT_LOAD_FACTOR = 0.75f;  ==> 作为扩容的标准
 * ==》实际第一次存储容量16*0.75=12，一旦放置的元素超过12，则就会扩容
 *
 *  链表转换为红黑树的阈值 ：static final int TREEIFY_THRESHOLD = 8;
 *  红黑树退化为链表的标准： static final int UNTREEIFY_THRESHOLD = 6;
 *
 * 根据添加元素的hashcode()计算哈希值：
 *  static final int hash(Object key) {
 *         int h;
 *         return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
 *     }
 */
public class HashSetDemo1 {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("a");//index=2
        set.add("b");
        set.add("中");//index=2
        set.add("yyy");
        set.add("a");
        System.out.println("set = " + set);
    }
}