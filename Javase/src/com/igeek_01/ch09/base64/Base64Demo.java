package com.igeek_01.ch09.base64;

import java.util.Base64;

/**
 * @author wangjin
 * 2023/9/21 9:21
 * @description base64编码
 * 在Java 8中，Base64编码已经成为Java类库的标准。
 * Java 8 内置了 Base64 编码的编码器和解码器。
 *
 * static Base64.Decoder getDecoder()返回一个 Base64.Decoder ，解码使用基本型 base64 编码方案。
 * static Base64.Encoder getEncoder()返回一个 Base64.Encoder ，编码使用基本型 base64 编码方案。
 */
public class Base64Demo {
    public static void main(String[] args) {
        String str = "今天周四了，明天放假了！";
        //编码  5LuK5aSp5ZGo5Zub5LqG77yM5piO5aSp5pS+5YGH5LqG77yB
        String encodeToString = Base64.getEncoder().encodeToString(str.getBytes());
        System.out.println("encodeToString = " + encodeToString);

        //解码
        byte[] bytes = Base64.getDecoder().decode(encodeToString);
        System.out.println("解码内容："+new String(bytes));
    }
}
