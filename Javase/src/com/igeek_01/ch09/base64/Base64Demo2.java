package com.igeek_01.ch09.base64;

import java.util.Base64;

/**
 * @author wangjin
 * 2023/9/21 9:30
 * @description url编码解码
 * url: http://127.0.0.1:8080/bams/login?id=1&password=123
 *
 * static Base64.Decoder getUrlDecoder()返回一个 Base64.Decoder ，解码使用 URL 和文件名安全型 base64 编码方案。
 * static Base64.Encoder getUrlEncoder()返回一个 Base64.Encoder ，编码使用 URL 和文件名安全型 base64 编码方案。
 */
public class Base64Demo2 {
    public static void main(String[] args) {
        //编码
        String url = "http://127.0.0.1:8080/bams/login?id=1&password=123";

        String encodeToString = Base64.getUrlEncoder().encodeToString(url.getBytes());
        //aHR0cDovLzEyNy4wLjAuMTo4MDgwL2JhbXMvbG9naW4_aWQ9MSZwYXNzd29yZD0xMjM=
        System.out.println("编码后的url:"+encodeToString);

        byte[] bytes = Base64.getUrlDecoder().decode(encodeToString);
        System.out.println("解码后的url:"+new String(bytes));
    }
}
