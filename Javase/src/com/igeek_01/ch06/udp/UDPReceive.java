package com.igeek_01.ch06.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @author wangjin
 * 2023/9/18 9:41
 * @description TODO
 */
public class UDPReceive {
    public static void main(String[] args) throws IOException {
        //接收到的数据包 DatagramPacket(byte[] buf, int length)
        //DatagramSocket void receive(DatagramPacket p)


        //创建字节数组 存储接收道的内容
        byte[] buf = new byte[1024];
        //创建数据包 来接收数据
        DatagramPacket dp = new DatagramPacket(buf, buf.length);

        //创建接收端的socket对象
        DatagramSocket ds = new DatagramSocket(6677);
        //接收数据
        ds.receive(dp);

        //获取实际接收到的内容长度
        int len = dp.getLength();
        //获取接收到的内容
        System.out.println("接收到的内容："+new String(buf,0,len));

        //释放资源
        ds.close();
    }
}
