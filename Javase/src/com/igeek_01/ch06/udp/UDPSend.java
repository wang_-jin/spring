package com.igeek_01.ch06.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author wangjin
 * 2023/9/18 9:41
 * @description TODO
 */
public class UDPSend {
    public static void main(String[] args) throws IOException {
        //打包数据 DatagramPacket(byte[] buf, int length, InetAddress address, int port)
        //发送数据 DatagramSocket  void send(DatagramPacket dp)


        //定义一个字符串内容
        String message = "你好吗";
        //字符串转换为字节数组
        byte[] buf = message.getBytes();
        //创建数据包
        DatagramPacket dp =
                new DatagramPacket(buf, buf.length, InetAddress.getLocalHost(), 6677);
        //创建发送端的socket对象 并且指定发送端的端口号是8888
        DatagramSocket ds = new DatagramSocket(8888);
        //发送数据
        ds.send(dp);
        //释放资源
        ds.close();
    }
}
