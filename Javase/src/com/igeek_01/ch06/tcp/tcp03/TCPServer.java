package com.igeek_01.ch06.tcp.tcp03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 14:28
 * @description 服务端
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        try(
                ServerSocket serverSocket = new ServerSocket(8888);
                Socket socket = serverSocket.accept();
                //写信息
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                //读信息
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //创建扫描器
                Scanner scanner = new Scanner(System.in);
        ){
            System.out.println("客户端接入成功");
            while(true){
                //1.读信息
                String line = br.readLine();
                System.out.println(socket.getRemoteSocketAddress()+"读了:"+line);
                //2.写信息
                System.out.println("请说:");
                String str = scanner.next();
                pw.println(str);
                pw.flush();
                if ("886".equals(str)){
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("客户端异常退出!");
            e.printStackTrace();
        }
    }
}
