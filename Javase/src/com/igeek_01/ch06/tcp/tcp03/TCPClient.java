package com.igeek_01.ch06.tcp.tcp03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 14:28
 * @description 客户端
 * 需求3：服务端接收多条消息，服务端也可以发送多条消息；
 * 客户端可以发送多条消息，也可以接收多条消息；（一对一，阻塞的现象）
 *
 * 一对一，阻塞的现象：
 * 客户端先写再读
 * 服务端先读再写
 */
public class TCPClient {
    public static void main(String[] args) {
        try(
                Socket socket = new Socket("127.0.0.1",8888);
                //写信息
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                //读信息
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //创建扫描器
                Scanner scanner = new Scanner(System.in)
        )
        {
            while(true){
                //1.写信息
                System.out.println("客户端请说:");
                String str = scanner.next();
                pw.println(str);
                pw.flush();
                if ("886".equals(str)){
                    break;
                }
                //2.读信息
                String line = br.readLine();
                System.out.println(socket.getRemoteSocketAddress()+"说了"+line);
            }
        } catch (IOException e) {
            System.out.println("服务器崩溃了");
            e.printStackTrace();
        }
    }
}
