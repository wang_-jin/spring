package com.igeek_01.ch06.tcp.tcp01;

import java.io.IOException;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author wangjin
 * 2023/9/14 11:14
 * @description TODO
 * 需求1：客户端与服务端可以建立连接 ； 服务端接收一条消息，客户端发送一条消息
 */
public class TCPClient {
    public static void main(String[] args) {
        System.out.println("-------------客户端---------------");
        try(
                //创建客户端,根据host和端口号接入到指定的服务端
                Socket socket = new Socket("192.168.21.9",7788);
                //获取输出流 PrintWriter
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os)
        ){
                pw.println("hello!服务器!");//写出后自动换行
                pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
