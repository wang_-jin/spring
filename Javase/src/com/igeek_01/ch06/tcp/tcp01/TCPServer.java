package com.igeek_01.ch06.tcp.tcp01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author wangjin
 * 2023/9/14 11:13
 * @description 服务端
 * TCP ==> Transfer Control Protocol ==> 传输控制协议
 * TCP协议的特点
 *     * 面向连接的协议
 *     * 只能由客户端主动发送数据给服务器端，服务器端接收到数据之后，可以给客户端响应数据。
 *     * 通过三次握手建立连接，连接成功形成数据传输通道。
 *     * 通过四次挥手断开连接
 *     * 基于IO流进行数据传输
 *     * 传输数据大小没有限制
 *     * 因为面向连接的协议，速度慢，但是是可靠的协议。
 *
 * TCP协议的使用场景
 *     * 文件上传和下载
 *     * 邮件发送和接收
 *     * 远程登录
 *
 * TCP协议相关的类
 *     * Socket
 *         * 一个该类的对象就代表一个客户端程序。
 *     * ServerSocket
 *         * 一个该类的对象就代表一个服务器端程序。
 *
 * Socket类构造方法
 *     * Socket(String host, int port)
 *         * 根据ip地址字符串和端口号创建客户端Socket对象
 *         * 注意事项：只要执行该方法，就会立即连接指定的服务器程序，如果连接不成功，则会抛出异常。
 *             如果连接成功，则表示三次握手通过。
 *
 * Socket类常用方法
 *     * OutputStream getOutputStream(); 获得字节输出流对象
 *     * InputStream getInputStream();获得字节输入流对象
 *
 *  try(资源对象){}
 *  资源对象:implements java.io.Closeable，
 *  可以放置在try（)括号里面，无论有没有异常产生，最后都会自动关闭资源对象
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        try(
                //开启一个服务端
                ServerSocket ss = new ServerSocket(7788);
                //等待接入客户端 ==>处于阻塞状态，接入客户端后才会解除阻塞
                //返回值socket：接入成功的客户端的套接字
                Socket socket = ss.accept();
                //获取输入流
                InputStream is = socket.getInputStream();
                InputStreamReader reader = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(reader);

        ){
            System.out.println("客户端已成功接入...");
            //服务端读取消息
            String s = br.readLine();
            //socket.getRemoteSocketAddress() 获取发送消息者的ip
            System.out.println(socket.getRemoteSocketAddress()+"说了"+s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
