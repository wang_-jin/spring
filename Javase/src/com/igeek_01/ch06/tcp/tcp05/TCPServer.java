package com.igeek_01.ch06.tcp.tcp05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author wangjin
 * 2023/9/14 19:00
 * @description TODO
 * 需求5：当服务器只是读取消息，客户端写出消息，此时一个服务器可以接收多个客户端
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("--------------服务端-----------------");
        try(
                ServerSocket serverSocket = new ServerSocket(6677);
        ){
            //循环接入客户端
            while(true){
                Socket socket = serverSocket.accept();//当前接入的客户端
                System.out.println(socket.getRemoteSocketAddress()+"已上线！");
                //每接入一个客户端，都开辟读线程：维系客户端和服务器之间的通信
                new Thread(new ReadThread(socket)).start();
            }
        }catch (SocketException e){
            System.out.println("over!");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
