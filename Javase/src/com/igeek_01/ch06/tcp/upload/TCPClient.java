package com.igeek_01.ch06.tcp.upload;

import java.io.*;
import java.net.Socket;

/**
 * @author wangjin
 * 2023/9/15 14:03
 * @description TODO
文件上传--从客户端的文件 上传至 图片服务器
 */
public class TCPClient {
    public static void main(String[] args) {
        Socket socket = null;
        System.out.println("----------------TCPClient----------------");
        try {
            socket = new Socket("192.168.24.30",8866);
            //1.创建高效的输入流 文件-->内存
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File("D:\\abc\\1.png")));
            //2.创建高效的输出流 客户端-->服务端
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            //6.接受服务器的反馈结果
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //边读边写
                byte[] bytes = new byte[1024];
                int len = 0;
                while ((len = bis.read(bytes))!=-1){
                    //写入服务端
                    bos.write(bytes,0,len);
                    bos.flush();
                }
                //手动只关闭输出流对象,通知服务器, 写数据完毕
                socket.shutdownOutput();
                //6.接受到服务器的反馈结果
                String line = null;
                line = br.readLine();
                System.out.println("服务器反馈结果为:"+line);

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
