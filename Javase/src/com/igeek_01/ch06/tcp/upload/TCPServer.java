package com.igeek_01.ch06.tcp.upload;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;

/**
 * @author wangjin
 * 2023/9/15 14:16
 * @description TODO
 * 服务端：
 * 1.读取 客户端的数据
 * 2.写数据 至图片服务器E:/upload目录下
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("----------------TCPServer----------------");
        try {
            ServerSocket ss = new ServerSocket(8866);
            Socket socket = ss.accept();
            //3.创建高效的输入流 客户端==>服务端
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            //4.创建高效的输出流 内存-->文件中
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File("D:\\upload", UUID.randomUUID().toString()+".png")));
            //5.发消息给客户端
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
                //边读边写
                byte[] bytes = new byte[1024];
                int len = 0;
                while ((len = bis.read(bytes))!=-1){
                    bos.write(bytes,0,len);
                    bos.flush();
                }
                //5.上传成功的结果反馈给客户端
                pw.println("文件上传成功!");
                pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
