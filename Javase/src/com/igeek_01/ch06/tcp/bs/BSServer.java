package com.igeek_01.ch06.tcp.bs;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author wangjin
 * 2023/9/18 9:17
 * @description TODO
 */
public class BSServer {
    //创建线程池
     private static ThreadPool pool = new ThreadPool(2,2,1);
    public static void main(String[] args) {
        try(
                //开启服务器
                ServerSocket serverSocket = new ServerSocket(8888);
                )
        {
                while(true) {
                    //循环等待接入客户端
                    Socket socket = serverSocket.accept();
                    System.out.println("客户端接入成功!");
                    //让线程池中的线程执行任务:响应一个页面 设置response headers
                    pool.execute(()->{
                        try{
                            PrintWriter pw = new PrintWriter(socket.getOutputStream());
                            //响应成功的状态码 HTTP/1.1 200 OK
                            pw.println("HTTP/1.1 200 OK");
                            //响应文档类型   Content-Type: text/html;charset=utf-8
                            pw.println("Content-Type: text/html;charset=utf-8");
                            pw.println();//写一个空行  响应头和页面内容 做一下分割
                            //响应给页面内容
                            pw.println("<h1 style='color:red;'>今天是周一</h1>");
                            pw.println("<h1 style='color:green;'>欢迎,"+socket.getRemoteSocketAddress()+"</h1>");
                            //刷新
                            pw.flush();

                            Thread.sleep(10000);
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
