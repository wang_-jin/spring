package com.igeek_01.ch06.tcp.tcp06;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wangjin
 * 2023/9/14 19:28
 * @description TODO
 */
public class TCPServer {
    private static ThreadPool pool = new ThreadPool(2,5,3);
    public static void main(String[] args) {

        System.out.println("-------------服务端----------------");
        try(
                ServerSocket serverSocket = new ServerSocket(6677);
        ){
            //循环接入客户端
            while(true){
                Socket socket = serverSocket.accept();//当前接入的客户端
                System.out.println(socket.getRemoteSocketAddress()+"已上线！");
                //每接入一个客户端，都开辟读线程：维系客户端和服务器之间的通信
                pool.execute(new ReadThread(socket));
            }
        }catch (SocketException e){
            System.out.println("over!");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
