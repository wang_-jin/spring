package com.igeek_01.ch06.tcp.tcp06;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 19:28
 * @description TODO
 */
public class TCPClient {
    public static void main(String[] args) {
        System.out.println("-------------客户端----------------");

        try(
                Socket socket = new Socket("192.168.21.9",6677);
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in)
                //负责发信息
        ){
            while (true){
                System.out.println("请说:");
                String str = scanner.next();
                pw.println(str);
                pw.flush();
                if ("886".equals(str)){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}