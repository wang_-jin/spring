package com.igeek_01.ch06.tcp.tcp06;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author wangjin
 * 2023/9/14 19:28
 * @description TODO
 */
public class ReadThread implements Runnable{
    private Socket socket;

    public ReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try(
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        ){
            String line = null;
            while ((line = br.readLine())!=null){
                if (!"886".equals(line)){
                    System.out.println(socket.getRemoteSocketAddress()+"读了:"+line);
                }else {
                    System.out.println("通信结束");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
