package com.igeek_01.ch06.tcp.tcp04;

import javax.security.sasl.SaslException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 15:18
 * @description 读线程
 */
public class ReadThread implements Runnable{
    //维系客户端和服务端之间的通信
    private Socket socket;
    public ReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        //读信息
        try(
                //关闭socket有关的输入流输出流，都会自动关闭socket
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ){
            String line = null;
            while ((line = br.readLine())!=null){//一端下线，另一端读线程读到的是null值，读线程结束
                if (!line.equals("886")){
                    System.out.println(socket.getRemoteSocketAddress()+"读了:"+line);
                }else {
                    System.out.println("客户端已下线");
                }
            }
        }catch (SocketException e){//Socket closed:自己 某一条写线程中主动关闭socket ，而另一条读线程仍在通过socket进行读操作
            e.printStackTrace();
            System.out.println("我下线了 拜拜您了~~");
            System.exit(0);
        }catch (IOException e) {
            e.printStackTrace();
        }finally {//????
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
