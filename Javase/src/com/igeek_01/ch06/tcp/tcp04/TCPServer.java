package com.igeek_01.ch06.tcp.tcp04;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author wangjin
 * 2023/9/14 15:18
 * @description 服务端
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        ServerSocket ss = null;//要在读写线程结束后再关闭主线程的socket,
        try{
            ss = new ServerSocket(8866);
            Socket accept = ss.accept();
            System.out.println("客户端接入成功.......");
            //读写操作
            new Thread(new WriteThread(accept)).start();
            new Thread(new ReadThread(accept)).start();
        }catch(SocketException e) {
            System.out.println("服务端下线");
        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (ss!=null){
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
