package com.igeek_01.ch06.tcp.tcp04;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 15:18
 * @description 客户端
 * 需求4：服务端接收多条消息，服务端也可以发送多条消息；
 * 客户端可以发送多条消息，也可以接收多条消息；（一对一，非阻塞的现象）  打电话  畅聊
 *
 * 客户端 服务端 都有 读写线程 ==> 非阻塞的现象
 */
public class TCPClient {
    public static void main(String[] args) {
        System.out.println("-------------客户端---------------");

        try{
            Socket socket = new Socket("192.168.21.9", 8866);
            //读写操作
            new Thread(new WriteThread(socket)).start();
            new Thread(new ReadThread(socket)).start();
        } catch (SocketException e){
            System.out.println("客户端下线");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
