package com.igeek_01.ch06.tcp.tcp04;

import javax.security.sasl.SaslException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 15:19
 * @description 写线程
 */
public class WriteThread implements Runnable{
    //通信资源 维系客户端和服务端之间的通信
    private Socket socket;
    public WriteThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        //发消息
        try(
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in)
        ){
                while(true){
                    System.out.println("请说:");
                    String str = scanner.next();
                    pw.println(str);
                    pw.flush();
                    if ("886".equals(str)){
                        break;
                    }
                }
        }catch (SocketException e){
            System.out.println("下线!");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {//????
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
