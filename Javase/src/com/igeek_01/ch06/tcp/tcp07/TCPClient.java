package com.igeek_01.ch06.tcp.tcp07;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author wangjin
 * 2023/9/15 11:22
 * @description TODO
 * 需求7：实现一对多群聊，客户端写出消息，服务器端可以转发消息给其它客户端，其它客户端也可以读取消息
 */
public class TCPClient {
    private static ThreadPool pool = new ThreadPool(2,3,5);
    public static void main(String[] args) {
        System.out.println("-------------客户端----------------");
        try {
            Socket socket = new Socket("127.0.0.1",8877);
            //获取自己的ip
            System.out.println(socket.getInetAddress()+"已上线");
            //线程池中的线程 执行任务：读 写
            pool.execute(new WriteThread(socket));
            pool.execute(new ReadThread(socket));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
