package com.igeek_01.ch06.tcp.tcp07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author wangjin
 * 2023/9/15 11:23
 * @description 客户端--读线程
 */
public class ReadThread implements Runnable{
    private Socket socket;

    public ReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try(
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                )
        {
                String line;
                while ((line = br.readLine())!=null){
                    if ("886".equals(line)) {
                        System.out.println(socket.getRemoteSocketAddress() +"已下线");
                    }else {
                        System.out.println("来自服务器的转发信息" + line);
                    }
                }
        }catch (SocketException e){//socket close 自己某一条写线程主动关闭socket,而另一条读线程人在通过socket进行读操作
            System.out.println("客户端下线");/*当输入886,程序进到finally*/
            System.exit(0);
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
