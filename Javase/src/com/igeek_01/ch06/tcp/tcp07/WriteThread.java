package com.igeek_01.ch06.tcp.tcp07;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/15 11:23
 * @description 客户端--写线程
 */
public class WriteThread implements Runnable{
    private Socket socket;

    public WriteThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try(
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
                )
        {
                //写信息给服务器
                while (true){
                    System.out.println("请说:");
                    String str = scanner.next();
                    pw.println(str);
                    pw.flush();
                    if ("886".equals(str)){
                        break;
                    }
                }
        }catch (SocketException e){
            System.out.println(socket.getRemoteSocketAddress()+"下线");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
