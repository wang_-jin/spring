package com.igeek_01.ch06.tcp.tcp07;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/9/15 11:23
 * @description 自定义线程池
 */
public class ThreadPool {
    private ExecutorService pool;

    public ThreadPool(int corePoolSize, int maxPoolSize, int queueSize) {
        this.pool = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(queueSize),
                new ThreadPoolExecutor.AbortPolicy());
    }

    public void execute(Runnable task){
        pool.execute(task);
    }
}
