package com.igeek_01.ch06.tcp.tcp07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wangjin
 * 2023/9/15 11:23
 * @description 服务端的任务---话务员
 */
public class Tasker implements Runnable{
    //维系 要读取信息的客户端 的通讯管道
    private Socket socket;
    //转发信息:写信息给其他客户端(获取到其他客户端的socket)
    private CopyOnWriteArrayList<Tasker> taskers;
    //写信息给其他客户端的输出流
    private PrintWriter pw;
    //读取 当前客户端的信息的输入流
    private BufferedReader br;

    public Tasker(Socket socket, CopyOnWriteArrayList<Tasker> taskers) {
        this.socket = socket;
        this.taskers = taskers;
    }

    @Override
    public void run() {
        try{
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = null;
            while ((line = br.readLine())!=null){
                //读取服务器信息
                if ("886".equals(line)){
                    //若读到的是 退出 信息
                    //2.转发信息给其他客户端
                    for (Tasker tasker : taskers) {
                        if (tasker != this){//当话务员不是自己
                            //给除了自己以外的其它所有客户端 发送 我已下线
                            pw = new PrintWriter(tasker.socket.getOutputStream());
                            pw.println(this.socket.getRemoteSocketAddress()+"已下线");
                            pw.flush();
                        }
                    }
                    //既然我已下线,从容器中移除自己
                    taskers.remove(this);
                    System.out.println(this.socket.getRemoteSocketAddress()+"已下线...");//给服务端看的

                }else {
                    //若未读到 退出 信息
                    //2.转发信息给其他客户端
                    for (Tasker tasker : taskers) {
                        if (tasker != this){//当话务员不是自己
                            //给除了自己以外的其它所有客户端发送信息,我已下线
                            pw = new PrintWriter(tasker.socket.getOutputStream());
                            pw.println(this.socket.getRemoteSocketAddress()+"说了"+line);
                            pw.flush();
                        }
                    }
                }
            }
        }catch (SocketException e) {
            System.out.println("客户端异常退出了");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
