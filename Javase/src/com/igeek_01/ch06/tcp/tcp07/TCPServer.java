package com.igeek_01.ch06.tcp.tcp07;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wangjin
 * 2023/9/15 11:22
 * @description TODO
 */
public class TCPServer {
    //线程池
    private static ThreadPool pool = new ThreadPool(5,8,50);
    //保存所有话务员的容器
    private static CopyOnWriteArrayList<Tasker> taskers = new CopyOnWriteArrayList<>();
    public static void main(String[] args) {
        System.out.println("--------------服务端-----------------");
        try{
           ServerSocket ss = new ServerSocket(8877);
           while (true){
               Socket socket = ss.accept();
               System.out.println("server:"+socket.getRemoteSocketAddress()+"已上线");
               //执行Tasker任务:每一个接入的客户端,都为其提供一个tasker话务员,读取当前客户端的信息,并转发给其它客户端
               Tasker tasker = new Tasker(socket,taskers);
               taskers.add(tasker);
               pool.execute(tasker);
           }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
