package com.igeek_01.ch06.tcp.tcp02;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 14:01
 * @description 客户端
 * 需求2：服务端接收多条消息，客户端发送多条消息== 发的是886表示结束
 */
public class TCPClient {
    public static void main(String[] args) throws IOException {
        System.out.println("-------------客户端---------------");
        PrintWriter pw = null;
        Socket socket = null;
        try {
            //创建客户端,根据host和端口号接入到指定的服务端
            socket = new Socket("192.168.21.9",7788);
            //获取输出流 PrintWriter
            OutputStream os = socket.getOutputStream();
            pw = new PrintWriter(os);
            Scanner input = new Scanner(System.in);
            while (true){
                System.out.println("请说:");
                String str = input.next();
                pw.println(str);
                pw.flush();
                if ("886".equals(str)){
                    break;
                }
            }
        }catch (SocketException e){
            e.printStackTrace();
            System.out.println("我下线了");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (pw!=null){
                pw.close();
            }
            if (socket!=null){
                socket.close();
            }
        }
    }
}
