package com.igeek_01.ch06.tcp.tcp02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author wangjin
 * 2023/9/14 14:01
 * @description 服务端
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端----------------");
        ServerSocket ss = null;
        Socket socket = null;
        BufferedReader br = null;
        try
        {                       //开启一个服务端
            ss = new ServerSocket(7788);
            //等待接入客户端
            socket = ss.accept();
            //获取输入流
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = null;
            while ((line = br.readLine())!=null){
                if ("886".equals(line)){
                    System.out.println(socket.getRemoteSocketAddress()+"客户端已下线!!!");
                }else {
                    System.out.println(socket.getRemoteSocketAddress()+"客户端说:"+line);
                }
            }
        }catch (SocketException e){
            System.out.println("客户端异常退出了！");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (br!=null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }if (ss!=null){//实际开发中 服务器一般不会关闭
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
