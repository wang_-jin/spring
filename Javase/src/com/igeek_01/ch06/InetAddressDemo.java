package com.igeek_01.ch06;

/**
 * @author wangjin
 * 2023/9/14 11:10
 * @description InetAddress
 *  InetAddress 一个该类的对象就代表一个IP地址对象。
 *  位于java.net包下
 *
 *  构造方法：
 *      获得本地主机IP地址对象
 *      static InetAddress getLocalHost()
 *
 *      根据IP地址字符串或主机名获得对应的IP地址对象
 *      static InetAddress getByName(String host)
 *
 *  常用方法：
 *      //获得主机名
 *      String getHostName()
 *
 *      //获得IP地址字符串
 *      String getHostAddress()
 */
public class InetAddressDemo {
}
