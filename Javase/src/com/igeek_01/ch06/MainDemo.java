package com.igeek_01.ch06;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author wangjin
 * 2023/9/14 10:14
 * @description 网络编程
 * 1.C/S 和 B/S软件结构的区别：
 *   C/S软件结构，client/server 客户端/服务器  例子:idea qq微信等
 *   B/S软件结构，browser/server 浏览器/服务器 例子：淘宝 京东网页
 *   C/S一遇到推广期，更新客户端，造成推广比较麻烦。 B/S比较易于推广。
 *
 * 2.网络编程，就是在一定的协议下，实现两台计算机的通信的程序。
 *
 * 3.网络通信协议
 *   TCP/IP协议：
 *   传输控制协议/因特网互联协议( Transmission Control Protocol/Internet Protocol)，是Internet最基本、最广泛的协议。
 *   它采用了四层分层模型：
 *   3.1 应用层
 *      HTTP： 超文本传输协议，网页传输
 *      FTP：文件传输
 *      SMTP：基本的邮件传输
 *   3.2 传输层
 *      TCP：传输控制协议 (Transmission Control Protocol)  面向连接的安全的数据传输协议
UDP：用户数据报协议(User Datagram Protocol) 面向无连接的不安全的数据传输协议
 *   3.3 网络层
 *      获取本机IP，和对方机器的IP及端口号
 *   3.4 数据链路层
 *      硬件层面上
 *
 * 4.TCP和UDP的区别
 * 1.TCP：传输控制协议 (Transmission Control Protocol)  面向连接的安全的数据传输协议
 *   UDP：用户数据报协议(User Datagram Protocol) 面向无连接的不安全的数据传输协议
 * 2.TCP协议可以保证传输数据的安全，传输速度相对较低
 *   UDP协议它是不可靠协议，因为无连接，所以传输速度快，但是容易丢失数据。
 * 3.TCP使用场景：文件上传、下载、消息传递等等
 *   UDP使用场景：语音聊天、视频聊天等
 *
 * 5.网络编程三要素
 *   5.1 协议：计算机网络通信必须遵守的规则
 *   5.2 IP地址：指互联网协议地址（Internet Protocol Address
 *   5.3 端口号：用两个字节表示的整数，它的取值范围是0~65535
 */
public class MainDemo {
    public static void main(String[] args) throws IOException {
        //创建本机地址IP对象
        InetAddress ip1 = InetAddress.getLocalHost();
        System.out.println("ip1 = " + ip1);
        //获取IP地址字符串
        System.out.println("ip1.getHostAddress() = " + ip1.getHostAddress());
        //获得主机名
        System.out.println("ip1.getHostName() = " + ip1.getHostName());

        System.out.println("---------------------------------");
        //根据主机名创建 特定IP对象
        InetAddress ip2 = InetAddress.getByName("www.baidu.com");
        System.out.println("ip2 = " + ip2);
        //获取IP地址字符串
        System.out.println("ip2.getHostAddress() = " + ip2.getHostAddress());
        //获得主机名
        System.out.println("ip2.getHostName() = " + ip2.getHostName());
        System.out.println("==============================");
        //指定时间内 是否能够ping通ip2
        //相当于: ping www.baidu.com
        boolean flag = ip2.isReachable(3000);
        System.out.println("flag = " + flag);
    }
}
