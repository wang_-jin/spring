package com.igeek_01.ch06.test.tcp3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 22:40
 * @description TODO
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        try(
                ServerSocket ss = new ServerSocket(8866);
                Socket accept = ss.accept();
                BufferedReader br = new BufferedReader(new InputStreamReader(accept.getInputStream()));
                PrintWriter pw = new PrintWriter(accept.getOutputStream());
                Scanner sc = new Scanner(System.in);

        ){
            System.out.println("客户端接入");
            String line = null;
            while (true) {
                //读
                if ((line = br.readLine())!=null) {
                    System.out.println(accept.getLocalSocketAddress() + "说了" + line);
                }else {
                    System.out.println("结束");
                    break;
                }
                //写
                System.out.println("服务端写:");
                String str = sc.next();
                if (!str.equals("886")) {
                    pw.println(str);
                    pw.flush();
                }else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
