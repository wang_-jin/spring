package com.igeek_01.ch06.test.tcp2;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 22:40
 * @description TODO
需求2：服务端接收多条消息，客户端发送多条消息== 发的是886表示结束
 */
public class TCPClient {
    public static void main(String[] args) {
        System.out.println("-------------客户端---------------");
        try(
                Socket socket = new Socket("192.168.21.9",8866);
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
        ){

            while (true){
                System.out.println("请说:");
                String str = scanner.next();
                if (!str.equals("886")){
                    pw.println(str);
                    pw.flush();
                }else {
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
