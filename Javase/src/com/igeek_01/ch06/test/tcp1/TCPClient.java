package com.igeek_01.ch06.test.tcp1;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 22:40
 * @description TODO
 * 需求1：客户端与服务端可以建立连接 ； 服务端接收一条消息，客户端发送一条消息
 */
public class TCPClient {
    public static void main(String[] args) {
        System.out.println("-------------客户端---------------");
        try(
                Socket socket = new Socket("192.168.21.9",8866);
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner scanner = new Scanner(System.in);
        ){
            System.out.println("请说:");
            String str = scanner.next();
            pw.println(str);
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
