package com.igeek_01.ch06.test.tcp1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author wangjin
 * 2023/9/14 22:40
 * @description TODO
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        try(
                ServerSocket ss = new ServerSocket(8866);
                Socket accept = ss.accept();
                BufferedReader br = new BufferedReader(new InputStreamReader(accept.getInputStream()));
        ){
            System.out.println("客户端接入");
            String line = br.readLine();
            System.out.println(accept.getLocalSocketAddress()+"说了"+line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
