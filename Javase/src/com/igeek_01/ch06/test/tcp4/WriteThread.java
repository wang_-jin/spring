package com.igeek_01.ch06.test.tcp4;

import com.igeek.ch01.obj.lesson_06.test.test2.Score;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wangjin
 * 2023/9/14 23:33
 * @description TODO
 */
public class WriteThread implements Runnable{
    private Socket socket;

    public WriteThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try(
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
                Scanner sc = new Scanner(System.in);
                )
        {
            while (true){
                System.out.println("写了:");
                String str = sc.next();
                pw.println(str);
                pw.flush();
                if (str.equals("886")){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
