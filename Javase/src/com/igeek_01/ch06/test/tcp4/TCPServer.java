package com.igeek_01.ch06.test.tcp4;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.ServerException;

/**
 * @author wangjin
 * 2023/9/14 23:33
 * @description TODO
 */
public class TCPServer {
    public static void main(String[] args) {
        System.out.println("-------------服务端---------------");
        ServerSocket ss = null;
        try{
            ss = new ServerSocket(8888);
            Socket accept = ss.accept();
            new Thread(new ReadThread(accept)).start();
            new Thread(new WriteThread(accept)).start();
        }catch (SocketException e){
            System.out.println("服务器异常");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (ss!=null){
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
