package com.igeek_01.ch06.test.tcp4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author wangjin
 * 2023/9/14 23:33
 * @description TODO
 */
public class ReadThread implements Runnable{
    private Socket socket;

    public ReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try(
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));)
        {
            String line = null;
            while ((line = br.readLine())!=null){
                System.out.println(socket.getRemoteSocketAddress()+"读了:"+line);
                if ("886".equals(line)){
                    break;
                }
            }
        }catch (SocketException e){
            System.out.println("崩溃");
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
