local pid = KEYS[1];
local uid = KEYS[2];
local stockKey = "kill_"..pid.."_sk";
local userKey = "kill_"..pid.."_uk";
print(stockKey)
print(userKey)

if redis.call('EXISTS' , stockKey)==0 then
    return 1;
end;
if tonumber(redis.call('get' , stockKey))<=0 then
    return 2;
end;
if redis.call('SISMEMBER' , userKey , uid)==1 then
    return 3;
end;
redis.call('decr' , stockKey);
redis.call('sadd' , userKey , uid);
return 4;

