package com.igeek.boot.controller;


import com.igeek.boot.common.Result;
import com.igeek.boot.common.SmsConstant;
import com.igeek.boot.utils.RandomUtil;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/19
 */
@RestController
@RequestMapping("/sms")
@Slf4j
@Api(tags = "短信接口")
public class SendSMSController {

    @Autowired(required = false)
    private StringRedisTemplate stringRedisTemplate;

    //登陆时发送的验证码
    @PostMapping("/sendLogin")
    public Result sendLogin(String phone){
        //1.调用工具类，产生6位的验证码
        String code = RandomUtil.getSixBitRandom();

        //TODO 2.调用阿里云SMS短信服务  SMSUtils.sendMsg(phone,code)

        //3.将短信验证码存储至Redis中，并设置5分钟有效期
        stringRedisTemplate.opsForValue().set(phone+"_"+ SmsConstant.SEND_SMS_LOGIN , code , 5*60 , TimeUnit.SECONDS);

        return new Result(true , "验证码发送成功" , code);
    }

    //找回密码时发送的验证码

    //预约时发送的验证码

}
