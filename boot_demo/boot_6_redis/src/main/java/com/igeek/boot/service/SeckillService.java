package com.igeek.boot.service;

import com.igeek.boot.common.Result;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/19
 */
public interface SeckillService {

    public Result kill(Integer pid , String uid);
    public Result killByTrans(Integer pid , String uid);
    public Result killByLua(String pid , String uid);

}
