package com.igeek.boot.controller;

import com.igeek.boot.common.Result;
import com.igeek.boot.entity.Orders;
import com.igeek.boot.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * http://localhost:8080/swagger-ui.html
 */
@RestController
@RequestMapping("/order")
@Slf4j
@Api(tags = "订单接口")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("创建订单")
    @PostMapping("create")
    public Result create(){
        Orders orders = orderService.createOrder();
        return new Result(true , "创建成功" , orders);
    }

    @ApiOperation("查询订单")
    @GetMapping("/{oid}")
    public Result create(@PathVariable("oid")Integer oid){
        Orders orders = orderService.findByOid(oid);
        return new Result(true , "查询成功" , orders);
    }
}
