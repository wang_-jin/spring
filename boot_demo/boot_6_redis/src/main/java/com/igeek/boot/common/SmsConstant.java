package com.igeek.boot.common;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/19
 */
public interface SmsConstant {

    public static final String SEND_SMS_LOGIN = "001";

    public static final String SEND_SMS_PWD = "002";

    public static final String SEND_SMS_ORDER = "003";

}
