package com.igeek.boot.controller;

import com.igeek.boot.common.Result;
import com.igeek.boot.common.SmsConstant;
import com.igeek.boot.entity.LoginVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户的控制层
 *
 * @author chemin
 * @since 2023/12/19
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Api(tags = "用户接口")
public class UserController {

    @Autowired(required = false)
    private StringRedisTemplate stringRedisTemplate;

    //登陆
    @PostMapping("/login")
    public Result login(@RequestBody LoginVO loginVO){
        //1.获取页面上输入的手机号和验证码
        if(ObjectUtils.isEmpty(loginVO)){
            return new Result(false , "登陆信息不能为空，登陆失败");
        }
        String phone = loginVO.getPhone();
        String code = loginVO.getCode();

        //2.将Redis中存储的验证码 与 输入的验证码  比对
        String codeInRedis = stringRedisTemplate.opsForValue().get(phone + "_" + SmsConstant.SEND_SMS_LOGIN);
        if(StringUtils.hasLength(code) && StringUtils.hasLength(codeInRedis)){
            if(code.equals(codeInRedis)){
                return new Result(true , "登陆成功");
            }
        }
        return new Result(false , "登陆失败");
    }

    //注册

    //...

}
