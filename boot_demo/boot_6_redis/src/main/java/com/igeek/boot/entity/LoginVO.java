package com.igeek.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginVO {

    private String phone;
    private String code;

}
