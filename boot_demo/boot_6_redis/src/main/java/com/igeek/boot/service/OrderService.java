package com.igeek.boot.service;

import com.igeek.boot.entity.Orders;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 基于Redis完成添加数据、查询数据
 *
 * @author wangjin
 * @since 2023/12/16
 *
 * redisTemplate.opsForValue()  String字符串类型
 * redisTemplate.opsForHash()   Hash
 * redisTemplate.opsForList()   List链表、队列、栈
 * redisTemplate.opsForSet()    Set无序不重复
 * redisTemplate.opsForZSet()   ZSet排序
 */
@Service
@Slf4j
public class OrderService {

    //定制Redis中存储数据的key规则
    private static final String REDIS_OID = "oid::";

    @Autowired(required = false)
    private RedisTemplate redisTemplate;

    //创建订单
    public Orders createOrder() {
        //随机订单号
        int oid = ThreadLocalRandom.current().nextInt(1000);
        //随机序列号
        String serial = UUID.randomUUID().toString();
        //创建订单
        Orders orders = new Orders(oid , "订单流水号："+serial);
        //Redis中存入订单数据
        redisTemplate.opsForValue().set(REDIS_OID+oid , orders);
        log.info("创建订单 orders:{}" , orders);
        return orders;
    }

    //查询订单
    public Orders findByOid(Integer oid) {
        Orders o = (Orders)redisTemplate.opsForValue().get(REDIS_OID+oid);
        log.info("查询订单 orders:{}" , o);
        return o;
    }
}
