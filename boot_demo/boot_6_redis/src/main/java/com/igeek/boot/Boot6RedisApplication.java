package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot6RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot6RedisApplication.class, args);
    }

}
