package com.igeek.boot.config;

import com.igeek.boot.entity.Address;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangjin
 * 2023/12/5 15:38
 * @description TODO
 *
 * 如何实现配置绑定？ Java程序可以读取到xxx.properties配置文件中的数据
 * 第一种方式： @PropertySource("classpath:db.properties") + @Value("${key}")
 * 使用场景：SpringMVC项目中
 *
 * 第二种方式：@Component + @ConfigurationProperties(prefix = "")
 * 使用场景：在实体类中
 *
 * 第三种方式：@EnableConfigurationProperties(Address.class) + @ConfigurationProperties(prefix = "")
 * 使用场景：底层源代码很常见
 * 配置类上  @EnableConfigurationProperties(Address.class) 开启配置绑定
 * XxxProperties文件上 @ConfigurationProperties(prefix = "") 指定操作前缀
 */
@Configuration
@EnableConfigurationProperties(Address.class)
public class MyConfig4 {
}
