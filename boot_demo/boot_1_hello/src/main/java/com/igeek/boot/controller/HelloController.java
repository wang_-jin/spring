package com.igeek.boot.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjin
 * 2023/12/5 11:09
 * @description TODO
 *
 * @RestController 复合注解
 * 1.@Controller   控制层，被IOC托管
 * 2.@ResponseBody 返回值都是json数据格式
 */

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "Hello SpringBoot2";
    }
}
