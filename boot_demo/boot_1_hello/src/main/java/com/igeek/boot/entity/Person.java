package com.igeek.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/5 14:39
 * @description TODO
 *
 * 安装lombok插件，引入依赖
 * @Data
 * @NoArgsConstructor
 * @AllArgsConstructor
 * @ToString
 * @EqualsAndHashCode
 * @Slf4j
 * @Builder
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String name;
    private int age;

    private Car car;
}
