package com.igeek.boot.config;

import com.igeek.boot.entity.Person;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangjin
 * 2023/12/5 15:23
 * @description TODO
 *
 * @Conditional条件装配
 * @ConditionalOn...  如果有什么条件，则
 * @ConditionalOnMissing...  如果没有什么条件，则
 *
 * 例如：
 * 1.@ConditionalOnWebApplication(type = Type.SERVLET)
 * 2.@ConditionalOnClass({ Servlet.class, DispatcherServlet.class, WebMvcConfigurer.class })
 * 3.@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
 * ...
 *
 * 使用场景：遵循 约定 > 配置 前提 ， 可以替换底层组件的操作 ， 从而达到定制化开发
 */
@Configuration
public class MyConfig3 {
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    @Bean(value = "pp")
    public Person person(){
        return new Person();
    }
}
