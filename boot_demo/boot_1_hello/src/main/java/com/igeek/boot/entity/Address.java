package com.igeek.boot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wangjin
 * 2023/12/5 16:08
 * @description TODO
 */
@Data
@ConfigurationProperties(prefix = "addr")
public class Address {
    private String street;
    private String city;
}
