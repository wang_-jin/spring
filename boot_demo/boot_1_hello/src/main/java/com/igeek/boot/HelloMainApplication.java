package com.igeek.boot;

import com.igeek.boot.config.MyConfig1;
import com.igeek.boot.config.MyConfig3;
import com.igeek.boot.entity.Address;
import com.igeek.boot.entity.Car;
import com.igeek.boot.entity.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

/**
 * @author wangjin
 * 2023/12/5 11:07
 * @description TODO
 */
@SpringBootApplication
public class HelloMainApplication {

    public static void main(String[] args) {
        //"just run"
        //创建IOC容器
        ConfigurableApplicationContext ac = SpringApplication.run(HelloMainApplication.class, args);
        /*String[] beanDefinitionNames = ac.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);*/

        System.out.println("----------MyConfig1----------");
        MyConfig1 myConfig1 = ac.getBean(MyConfig1.class);
        //com.igeek.boot.config.MyConfig1$$EnhancerBySpringCGLIB$$ed940fe4@3605c4d3
        System.out.println("myConfig1 = "+myConfig1);
        Person p1 = ac.getBean("p1", Person.class);
        //Person(name=张三, age=18, car=com.igeek.boot.entity.Car@585c13de)
        System.out.println("p1 = "+p1);
        Person p2 = ac.getBean("p2", Person.class);
        //Person(name=李四, age=22, car=com.igeek.boot.entity.Car@585c13de)
        System.out.println("p2 = "+p2);
        Car car = ac.getBean("car", Car.class);
        //com.igeek.boot.entity.Car@585c13de
        System.out.println("car = "+car);

        System.out.println("----------MyConfig2----------");
        String[] personBeanNamesForType = ac.getBeanNamesForType(Person.class);
        //[p1 , p2 , com.igeek.boot.entity.Person]
        Arrays.stream(personBeanNamesForType).forEach(System.out::println);
        String[] carBeanNamesForType = ac.getBeanNamesForType(Car.class);
        //[car , car1 , car2]
        Arrays.stream(carBeanNamesForType).forEach(System.out::println);

        System.out.println("----------MyConfig3----------");
        MyConfig3 myConfig3 = ac.getBean(MyConfig3.class);
        System.out.println("myConfig3 = "+myConfig3);
        Person pp = ac.getBean("pp", Person.class);
        System.out.println("pp = "+pp);

        System.out.println("----------MyConfig4----------");
        Address address = ac.getBean(Address.class);
        System.out.println("address = "+address);
    }
}
