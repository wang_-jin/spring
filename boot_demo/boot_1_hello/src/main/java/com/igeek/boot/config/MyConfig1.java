package com.igeek.boot.config;

import com.igeek.boot.entity.Car;
import com.igeek.boot.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangjin
 * 2023/12/5 14:30
 * @description TODO
 *
 * IOC容器本质上ConcurrentHashMap，以beanName作为key,以BeanDefinition作为value存储的
 *
 * @Configuration 标注当前是一个配置类
 * 1.本质上是@Component，受IOC容器管理
 *
 * 2.属性 proxyBeanMethods 默认是true
 * 2.1 boolean proxyBeanMethods() default true;
 *  > FULL模式  全模式
 *  > 用来监测当前IOC容器中是否有此实例，若有则不会再创建，直接返回此单实例
 *  > com.igeek.boot.config.MyConfig1$$EnhancerBySpringCGLIB$$ed940fe4@3605c4d3
 *
 * 2.2 boolean proxyBeanMethods() false;
 *  > LITE模式  轻模式
 *  > 每一次调用都会创建新的实例
 *  > com.igeek.boot.config.MyConfig1@272a179c
 *
 * 使用场景总结：
 * Lite模式，指定proxyBeanMethods值为false，配置类组件之间无依赖关系用Lite模式加速容器启动过程，减少判断配置
 * Full模式，默认指定proxyBeanMethods值为true，类组件之间有依赖关系，方法会被调用得到之前单实例组件
 *
 * 3.配置类里面使用@Bean标注在方法上给容器注册组件，默认也是单实例的
 */
@Configuration(proxyBeanMethods = false)
public class MyConfig1 {
    //通过@Bean注解，将Person注册至IOC容器中，以方法名p1作为beanName
    //@Bean(value = "pp" , initMethod = "" , destroyMethod = "") 以指定的value，例如：pp作为beanName
    @Bean
    public Person p1(){
        return new Person("张三", 18, car());
    }
    @Bean
    public Person p2(){
        return new Person("李四", 22, car());
    }
    @Bean
    public Car car(){
        return new Car("奥迪" , 300000.0);
    }
}
