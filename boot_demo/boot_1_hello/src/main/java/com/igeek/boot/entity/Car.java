package com.igeek.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wangjin
 * 2023/12/5 14:39
 * @description TODO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car {
    private String label;
    private double price;
}
