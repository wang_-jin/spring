package com.igeek.boot.config;

import com.igeek.boot.entity.Person;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * @author wangjin
 * 2023/12/5 15:10
 * @description TODO
 *
 * 如何将组件注册至IOC容器中？
 * 方式一：@Configuration + @Bean
 * 在配置类上，标注@Configuration注解，代表当前是配置类；
 * 在方法上，标注@Bean注解，代表将当前方法的返回值注册至IOC容器中，以方法名作为beanName
 * 使用场景：配置类中，替换底层的组件
 *
 * 方式二：@Import导入组件
 * @Import(Person.class)
 * 将value注册至IOC容器中，以全类名（包名.类名）作为beanName
 * 使用场景：底层的注解
 *
 * 方式三：@ImportResource导入Spring配置文件
 * 将Spring的配置文件中的bean实例，注解注册至IOC容器中
 * 使用场景：项目的翻新
 */
@Configuration
@Import(Person.class)
@ImportResource(locations = "classpath:beans.xml")
public class MyConfig2 {
}
