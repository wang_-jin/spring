package com.igeek.boot.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/26 11:28
 *
 * 方式四：异常处理
 * 1.@Component  将自定义的全局异常处理器，注册至IOC容器中
 * 2.implements HandlerExceptionResolver  接口，重写resolveException()
 * 3.@Order(Ordered.HIGHEST_PRECEDENCE) 优先级
 *
 * 使用场景：返回ModelAndView，更适用于前后端不分离
 *
 * 底层：
 * 1.只要执行response.sendError() ，error请求就会转给controller；
 * 2.出现的异常没有任何人能处理，tomcat底层也是 response.sendError()，error请求就会转给controller；
 * 3.BasicErrorController 要跳转的页面地址是由ErrorViewResolver视图解析器解析的
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomHandlerExceptionResolver implements HandlerExceptionResolver {

    /**
     * @param request  请求
     * @param response 响应
     * @param handler  真正执行的handler，即 全类名#方法名(参数列表)
     * @param ex       异常信息
     * @return  ModelAndView  数据与逻辑视图
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        try {
            response.sendError(411 , ex.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ModelAndView();
    }
}
