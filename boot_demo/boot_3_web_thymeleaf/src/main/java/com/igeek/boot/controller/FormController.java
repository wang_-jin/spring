package com.igeek.boot.controller;

import com.igeek.boot.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/24 16:27
 */
@Controller
@Slf4j
public class FormController {

    //跳转显示form_layouts.html
    @GetMapping("/form_layouts.html")
    public String gotoBasicTable(){
        return "form_layouts";
    }

    //文件上传
    @PostMapping("/upload")
    public String upload(User user , @RequestPart("photo") MultipartFile photo , @RequestPart("photos") MultipartFile[] photos) throws IOException {
        log.info("user {}" , user);

        //处理单个图片
        if(photo!=null){
            String oldName = photo.getOriginalFilename();
            if(StringUtils.hasLength(oldName)){
                String newName = UUID.randomUUID()+oldName.substring(oldName.lastIndexOf("."));
                log.info("newName {}" , newName);
                //传输图片资源
                user.setPic(newName);
                photo.transferTo(new File("F:\\117\\1.SpringBoot\\other files\\temp\\"+newName));
            }
        }

        //处理多个图片
        if(photos!=null && photos.length>0){
            for (MultipartFile file : photos) {
                if(file!=null){
                    String oldName = file.getOriginalFilename();
                    if(StringUtils.hasLength(oldName)){
                        String newName = UUID.randomUUID()+oldName.substring(oldName.lastIndexOf("."));
                        log.info("newName {}" , newName);
                        //传输图片资源
                        file.transferTo(new File("F:\\117\\1.SpringBoot\\other files\\temp\\"+newName));
                    }
                }
            }
        }

        //响应重定向到主页面   地址栏显示目标地址，避免重复提交表单
        return "redirect:/main.html";
    }
}
