package com.igeek.boot.config;

import com.igeek.boot.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/25 15:10
 */
@Configuration(proxyBeanMethods = false)
public class WebConfig {

    //通过@Bean标注在方法上，替换WebMvcConfigurer组件
    @Bean
    public WebMvcConfigurer webMvcConfigurer(){
        return new WebMvcConfigurer() {
            //资源路径映射
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                //访问路径下的资源时，以 http://localhost:8899/pic/car2.png
                registry.addResourceHandler("/pic/**").addResourceLocations("file:\\F:\\117\\1.SpringBoot\\other files\\temp\\");
            }

            //添加拦截器
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //addPathPatterns添加拦截路径   excludePathPatterns排除拦截路径
                registry.addInterceptor(new LoginInterceptor())
                        .addPathPatterns("/main.html" , "/basic_table.html" , "/form_layouts.html")  //以后写"/user/**"
                        .excludePathPatterns("/css/**" , "/fonts/**" , "/images/**" , "/js/**" , "/login.html" , "/login" , "/logout");
            }
        };
    }


    //通过@Bean，注册CommonsMultipartResolver的bean实例  替换底层多部件解析器StandardServletMultipartResolver
    /*@Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setSupportedMethods("POST","PUT");  //Spring版本 5.3.9
        multipartResolver.setMaxUploadSize(104857600);
        multipartResolver.setMaxInMemorySize(4096);
        multipartResolver.setDefaultEncoding("UTF-8");
        return multipartResolver;
    }*/
}
