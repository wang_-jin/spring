package com.igeek.boot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/26 11:18
 *
 * 方式三：异常处理
 * 注解@ResponseStatus
 * 1.code属性：自定义异常，状态码
 * 2.reason属性：异常信息
 *      2.1 若reason不写，抛出自定义异常时，调用有参构造方法
 *      2.2 若reason写，抛出自定义异常，调用无参构造方法
 *
 * 使用场景：自定义异常
 *
 * 底层
 * 1.ResponseStatusExceptionResolver支持的
 * 2.把ResponseStatus注解的信息底层调用 response.sendError(statusCode, resolvedReason)；
 * 3.tomcat发送的/error请求
 * 4.默认会被BasicErrorController处理/error请求 --> HTML 精准匹配 error/404.html  系列匹配 error/4xx.html
 */
@ResponseStatus(code = HttpStatus.UNAUTHORIZED , reason = "客户端错误")
public class CustomException extends Exception {

    public CustomException() {
        super();
    }

    public CustomException(String message) {
        super(message);
    }
}
