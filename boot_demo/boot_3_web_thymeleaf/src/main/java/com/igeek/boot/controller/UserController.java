package com.igeek.boot.controller;

import com.igeek.boot.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/24 15:17
 */
@Controller
public class UserController {

    //跳转至login.html页面
    @GetMapping({"/","/login.html"})
    public String gotoLogin(){
        //路径 classpath:/templates/login.html
        return "login";
    }

    //处理登录请求  Method Not Allowed, status=405
    @PostMapping("/login")
    public String login(User user , Model model , RedirectAttributes redirectAttributes, HttpSession session){
        if(ObjectUtils.isEmpty(user)){
            redirectAttributes.addAttribute("msg" , "用户信息为空");
            return "redirect:/login.html";
        }
        if(StringUtils.hasLength(user.getUsername()) && StringUtils.hasLength(user.getPassword())){
            session.setAttribute("user" , user);
            //登录成功，页面跳转至主页面main.html
            //请求转发：forward:/main.html  地址栏显示请求地址 之后如果重复刷新页面的话，会发起Get方式访问
            //响应重定向：redirect:/main.html  地址栏显示目标地址  避免表单重复发送请求
            return "redirect:/main.html";
        }
        redirectAttributes.addAttribute("msg","用户姓名或密码为空");
        return "redirect:/login.html";
    }

    //处理登出请求
    @GetMapping("/logout")
    public String logout(HttpSession session , Model model){
        session.invalidate();
        return "redirect:/login.html";
    }

    //跳转至main.html页面
    @GetMapping({"/main.html"})
    public String gotoMain(){
        return "main";
    }
}
