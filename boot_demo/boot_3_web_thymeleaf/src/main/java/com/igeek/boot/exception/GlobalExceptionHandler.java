package com.igeek.boot.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;



/**
 * @Description 全局异常处理器
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/26 10:43
 *
 * 方式二：异常处理
 * 1.@ControllerAdvice  增强控制器
 *      1.1 @ControllerAdvice
 *      1.2 @RestControllerAdvice   等价于@ControllerAdvice+@ResponseBody
 * 2.@ExceptionHandler  全局异常处理器
 *
 * 使用场景：
 * 1.前后端分离 @RestControllerAdvice
 * 2.适用于自定义异常
 *
 * 底层：ExceptionHandlerExceptionResolver 支持的
 */
//@RestControllerAdvice
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    //设置返回页面
    @ExceptionHandler(ArithmeticException.class)
    public String error(Exception e){
        log.info("异常信息：{}" , e.getMessage());
        return "redirect:/login.html";
    }
    //设置返回页面
    @ExceptionHandler(ArithmeticException.class)
    public String error1(Exception e){
        log.info("异常信息: {}",e.getMessage());
        return "redirect:/login.html";
    }


    //设置返回json格式数据  必须搭配@RestControllerAdvice
    /*@ExceptionHandler(Exception.class)
    public Result error(){
        return new Result(302 , "发生异常");
    }*/

    //设置返回json格式数据  必须搭配@RestControllerAdvice
/*    @RestControllerAdvice
    public Result error(){
        return new Result(302,"发生异常");
    }*/

    /*@ExceptionHandler(CustomException.class)
    public Result error(){
        return new Result(302 , "发生异常");
    }*/
}
