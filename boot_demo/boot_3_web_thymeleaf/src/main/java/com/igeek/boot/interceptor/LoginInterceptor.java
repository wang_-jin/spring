package com.igeek.boot.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/25 16:12
 *
 * 登录拦截器
 * 1.implements HandlerInterceptor 拦截器
 * 2.在配置文件中 @Bean + WebMvcConfigurer + 重写addInterceptors()
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 在执行handler之前
     * @param request  请求
     * @param response 响应
     * @param handler  真正执行的handler，全类名#方法名(形参)
     * @return
     * @throws Exception
     * 使用场景：登录拦截、权限拦截、校验拦截等
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("LoginInterceptor  preHandle~ handler = {}" , handler);

        HttpSession session = request.getSession();
        Object user = session.getAttribute("user");
        if(user==null){
            response.sendRedirect("/login.html");
            //返回false代表不放行
            return false;
        }

        //返回true代表放行
        return true;
    }

    /**
     * 在执行handler之后，ModelAndView对象渲染之前
     * @param request  请求
     * @param response 响应
     * @param handler  真正执行的handler，即全类名#方法名(形参)
     * @param modelAndView 数据及视图对象
     * @throws Exception
     * 使用场景：干涉ModelAndView对象的操作
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("LoginInterceptor  postHandle~ handler = {} ， modelAndView = {}" , handler , modelAndView);
    }

    /**
     * 在渲染ModelAndView数据之后
     * @param request  请求
     * @param response 响应
     * @param handler  真正执行的handler，即全类名#方法名(形参)
     * @param ex       发生的异常信息
     * @throws Exception
     * 使用场景：统一日志记录、统一的异常的处理等
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("LoginInterceptor  afterCompletion~ handler = {} ， ex = {}" , handler , ex);
    }
}
