package com.igeek.boot.controller;

import com.igeek.boot.entity.User;
import com.igeek.boot.exception.CustomException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/24 16:27
 */
@Controller
public class TableController {

    //跳转显示basic_table.html
    //测试位置，400 错误状态码,id是必填的
    @GetMapping("/basic_table.html")
    public String gotoBasicTable(Model model , @RequestParam Integer id) throws CustomException {
        //准备数据
        List<User> list = new ArrayList<>();
        list.add(new User(10 , "zhangsan" , "123" , "/pic/52758593-2f88-47a5-8e7d-5fbf6c4dbeca.jpg"));
        list.add(new User(20 , "lisi" , "123" , "/pic/e52c4a3e-6766-48f0-acd5-edabbbcd8e03.jpg"));
        list.add(new User(30 , "zhanglong" , "222" , ""));
        list.add(new User(4 , "zhaohu" , "123" , ""));
        list.add(new User(50 , "wangchao" , "333" , "/pic/eb866556-0606-46f2-a398-4110371afb65.jpg"));
        list.add(new User(6 , "mahan" , "123" , ""));
        list.add(new User(70 , "gongsun" , "444" , ""));

        //测试位置：数学异常  500内部错误
//        int i = 10/0;

        //测试位置：抛出自定义异常
        if(list.size()>3){
            //throw new CustomException("值太多了~");
            throw new CustomException();
        }

        //添加至请求域
        model.addAttribute("users",list);
        return "basic_table";
    }
}
