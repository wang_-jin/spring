package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot3WebThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot3WebThymeleafApplication.class, args);
    }

}
