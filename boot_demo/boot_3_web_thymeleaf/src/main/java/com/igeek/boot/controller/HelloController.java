package com.igeek.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description TODO
 * @Author chenmin
 * @Version 1.0
 * @Date 2023/4/24 14:07
 *
 * SpringBoot默认不能直接访问 /templates/xxx.html资源
 */
@Controller
public class HelloController {

    //跳转success.html的页面
    @GetMapping("/success.html")
    public String gotoSuccess(Model model){
        model.addAttribute("msg","你好 Thymeleaf！");
        model.addAttribute("msgHTML","<p style='color:red'>你好 Thymeleaf！</p>");
        model.addAttribute("link","http://www.baidu.com");
        //真实路径  classpath:/templates/success.html
        return "success";
    }
}
