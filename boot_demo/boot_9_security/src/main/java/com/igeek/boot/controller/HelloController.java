package com.igeek.boot.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 */
@RestController
public class HelloController {

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('订单管理员')")
    @PostMapping("hello")
    public String hello(){
        return "Hello Spring Security!";
    }
}
