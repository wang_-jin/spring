package com.igeek.boot.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 权限类
 * @author chenmin
 * @since 2024-01-03
 */
@Data
@TableName("permission")
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    /**
     * 权限名称
     */
    @TableField("permissionName")
    private String permissionName;

    /**
     * 权限描述
     */
    @TableField("permissionDesc")
    private String permissionDesc;

    /**
     * 权限地址
     */
    @TableField("permissionUrl")
    private String permissionUrl;


}
