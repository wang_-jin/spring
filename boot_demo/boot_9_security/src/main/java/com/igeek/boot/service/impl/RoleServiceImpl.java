package com.igeek.boot.service.impl;

import com.igeek.boot.entity.Role;
import com.igeek.boot.mapper.RoleMapper;
import com.igeek.boot.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
