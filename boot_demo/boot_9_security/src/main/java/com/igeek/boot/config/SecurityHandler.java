package com.igeek.boot.config;

import cn.hutool.json.JSONUtil;
import com.igeek.boot.common.Result;
import com.igeek.boot.util.WebUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author chenmin
 * @Description TODO
 * @Date 2023/12/22
 */
@Component
public class SecurityHandler {

    //授权失败处理器  403
    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new AccessDeniedHandler() {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
                Result result = new Result(false, "您的权限不足!" , 403);
                String json = JSONUtil.toJsonStr(result);
                // 将字符串渲染到客户端
                WebUtils.renderString(response, json);
            }
        };
    }


    //认证失败处理器
    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint(){
        return new AuthenticationEntryPoint() {
            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
                Result result = new Result(false, "认证失败，请重新登录!" , 401);
                String json = JSONUtil.toJsonStr(result);
                // 将字符串渲染到客户端
                WebUtils.renderString(response, json);
            }
        };
    }

}
