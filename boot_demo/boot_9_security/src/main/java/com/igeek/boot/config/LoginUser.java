package com.igeek.boot.config;

import cn.hutool.core.collection.CollectionUtil;
import com.igeek.boot.entity.Admin;
import com.igeek.boot.entity.Permission;
import com.igeek.boot.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 *
 * UserDetails  是SpringSecurity提供的接口
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUser implements UserDetails {

    private Admin admin;

    public LoginUser(Admin admin){
        this.admin = admin;
    }

    //管理员Admin拥有的角色和权限信息
    private List<SimpleGrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 权限为空的时候才遍历，不为空直接返回
        if(CollectionUtil.isNotEmpty(authorities)){
            return authorities;
        }

        //添加角色和权限
        authorities = new ArrayList<>();
        Set<Role> roles = admin.getRoles();
        roles.forEach(role -> {
            //RBAC基于角色控制 添加角色
            authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getRoleName()));
            Set<Permission> permissions = role.getPermissions();
            permissions.forEach(permission -> {
                //RBAC基于资源控制  添加权限
                authorities.add(new SimpleGrantedAuthority(permission.getPermissionName()));
            });
        });
        return authorities;
    }

    @Override
    public String getPassword() {
        return admin.getPassword();
    }

    @Override
    public String getUsername() {
        return admin.getUsername();
    }

    //认证是否过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //账户是否锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //授权是否过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //当前管理员账号是否启用   0未开启 1开启
    @Override
    public boolean isEnabled() {
        return admin.getStatus()==1;
    }
}
