package com.igeek.boot.mapper;

import com.igeek.boot.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select * from role r , admin_role ar where r.id = ar.role_id and ar.admin_id = #{adminId}")
    @Results({
            @Result(id = true , column = "id" , property = "id"),
            @Result(column = "id" , property = "permissions" , javaType = Set.class ,
                    many = @Many(select = "com.igeek.boot.mapper.PermissionMapper.selectPermissionByRoleId"))
    })
    public Set<Role> selectRoleByAdminId(Integer adminId);

}
