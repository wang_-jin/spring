package com.igeek.boot.util;

import io.jsonwebtoken.*;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @Author chenmin
 * @Description TODO
 */
public class JwtHelper {

    private static long tokenExpiration = 24*60*60*1000;
    private static String tokenSignKey = "com.igeek.health.aGVhbHRoMTIz";

    //生成Token
    public static String createToken(String userId, String userName) {
        String token = Jwts.builder()
                .setSubject("HEALTH-USER")
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .claim("userId", userId)
                .claim("userName", userName)
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)
                .compressWith(CompressionCodecs.GZIP)
                .compact();
        return token;
    }

    //通过token获取用户ID
    public static Integer getUserId(String token) {
        if(StringUtils.hasLength(token)){
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
            Claims claims = claimsJws.getBody();
            Integer userId = (Integer)claims.get("userId");
            return userId;
        }
        return null;
    }

    //通过token获取用户姓名
    public static String getUserName(String token) {
        if(StringUtils.hasLength(token)){
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
            Claims claims = claimsJws.getBody();
            return (String)claims.get("userName");
        }
        return null;
    }

    public static void main(String[] args) {
        String token = JwtHelper.createToken("1", "admin");
        System.out.println(token);
        System.out.println(JwtHelper.getUserId(token));
        System.out.println(JwtHelper.getUserName(token));
    }

}
