package com.igeek.boot.service;

import com.igeek.boot.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
public interface IPermissionService extends IService<Permission> {

}
