package com.igeek.boot.mapper;

import com.igeek.boot.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

    @Select("select * from admin where username = #{username}")
    @Results({
            @Result(id = true , column = "id" , property = "id"),
            @Result(column = "id" , property = "roles" , javaType = Set.class ,
            many = @Many(select = "com.igeek.boot.mapper.RoleMapper.selectRoleByAdminId"))
    })
    Admin selectAdminByUsername(String username);
}
