package com.igeek.boot.mapper;

import com.igeek.boot.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {


    @Select("select * from permission p , role_permission rp where p.id = rp.permission_id and rp.role_id = #{roleId}")
    public Set<Permission> selectPermissionByRoleId(Integer roleId);

}
