package com.igeek.boot.config;

import com.igeek.boot.entity.Admin;
import com.igeek.boot.mapper.AdminMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 *
 * 自定义认证逻辑  implements实现  UserDetailsService
 */
@Service
@Slf4j
public class SpringSecurityDetailsService implements UserDetailsService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //Admin管理员信息，包含角色信息、权限信息
        Admin admin = adminMapper.selectAdminByUsername(username);
        if(ObjectUtils.isEmpty(admin)){
            throw new RuntimeException("认证失败");
        }

        //封装LoginUser
        LoginUser loginUser = new LoginUser(admin);

        // 将Authentication对象（用户信息、已认证状态、权限信息）存入 SecurityContextHolder
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser , null , loginUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        return loginUser;
    }
}
