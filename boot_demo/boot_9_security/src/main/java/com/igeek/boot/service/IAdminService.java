package com.igeek.boot.service;

import com.igeek.boot.config.LoginUser;
import com.igeek.boot.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
public interface IAdminService extends IService<Admin> {

    LoginUser login(Admin admin);
}
