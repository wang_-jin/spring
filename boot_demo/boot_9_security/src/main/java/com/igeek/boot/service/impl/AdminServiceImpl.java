package com.igeek.boot.service.impl;

import com.igeek.boot.config.LoginUser;
import com.igeek.boot.entity.Admin;
import com.igeek.boot.mapper.AdminMapper;
import com.igeek.boot.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public LoginUser login(Admin admin) {
        //通过管理员的账户和密码封装认证token
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(admin.getUsername() , admin.getPassword());
        //认证管理器中匹配token
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        // 认证没通过
        if (Objects.isNull(authenticate)) {
            throw new RuntimeException("查询时用户名或密码错误!");
        }

        //认证通过，获取认证通过的信息
        LoginUser loginUser = (LoginUser)authenticate.getPrincipal();
        return loginUser;
    }
}
