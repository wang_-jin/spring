package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot9SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot9SecurityApplication.class, args);
    }

}
