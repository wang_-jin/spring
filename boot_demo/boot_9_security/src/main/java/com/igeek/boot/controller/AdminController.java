package com.igeek.boot.controller;

import com.igeek.boot.common.Result;
import com.igeek.boot.config.LoginUser;
import com.igeek.boot.entity.Admin;
import com.igeek.boot.service.IAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 */
@Controller
@RequestMapping("/admin")
@Slf4j
public class AdminController {

    @Autowired
    private IAdminService adminService;
    @GetMapping("findAll")
    public Result findAll(){
        List<Admin> list = adminService.list();
        return new Result(true , "查询成功" , list);
    }
    @PostMapping("login")
    public String login(@RequestBody Admin admin){
        LoginUser loginUser = adminService.login(admin);
        log.info("loginUser :{}" , loginUser);
        return "/hello";
    }
}
