package com.igeek.boot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.session.InvalidSessionStrategy;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 *
 * 开启Web认证授权 @EnableWebSecurity
 * 方式一：@Configuration + @Bean 替换底层SecurityFilterChain组件
 * @Bean
 * public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
 *     http.authorizeRequests().anyRequest().authenticated();
 *     http.formLogin();
 *     http.httpBasic();
 *     return http.build();
 * }
 *
 * 方式二：extends WebSecurityConfigurerAdapter
 * 重写configure(HttpSecurity http)
 */
@Configuration
@EnableWebSecurity
//开启支持表达式的注解
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityInDBConfig extends WebSecurityConfigurerAdapter {

    //认证过滤器，处理token
    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    //权限失败处理器
    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    //认证失败处理器
    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;


    //加密方式
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //基于数据库完成认证操作
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //认证逻辑
        http
                .csrf().disable() //关闭CSRF（跨服务器的请求访问）,默认是开启的，设置为禁用；如果使用自定义登录页面需要关闭此项，否则登录操作会被禁用403
                .cors() // Spring Security 允许跨域
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // 禁用session (前后端分离项目，不通过Session获取SecurityContext)
                .and()
                //请求认证配置
                .authorizeRequests()
                .mvcMatchers("/index.html" , "/admin/login").permitAll()  //登录或未登录都可以访问，直接放行的资源
                .anyRequest().authenticated();  //任意用户，认证通过才可以访问

        //添加认证过滤器
        http.addFilterBefore(jwtAuthenticationTokenFilter , UsernamePasswordAuthenticationFilter.class);

        //配置异常处理器
        http.exceptionHandling()
                //授权失败  403
                .accessDeniedHandler(accessDeniedHandler)
                //认证失败  401
                .authenticationEntryPoint(authenticationEntryPoint);
    }

    //注入AuthenticationManager 进行用户认证
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
