package com.igeek.boot.config;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 *
 * OncePerRequestFilter   认证过滤器
 * 作用：解析请求头中的token，并验证合法性。继承 OncePerRequestFilter 保证请求经过过滤器一次
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter{

    //过滤逻辑
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //登陆直接放行

        //获取请求头中的token  若没有token也直接放行

        //JWT将token中的数据解析userId

        //从Redis中获取用户信息，若不存在直接抛出异常

        //若存在，则将Authentication对象（用户信息、已认证状态、角色权限信息）存入 SecurityContextHolder

        //放行
        filterChain.doFilter(request , response);
    }
}
