package com.igeek.boot.service.impl;

import com.igeek.boot.entity.Permission;
import com.igeek.boot.mapper.PermissionMapper;
import com.igeek.boot.service.IPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chenmin
 * @since 2024-01-03
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
