package com.igeek.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/3
 *
 * 开启Web认证授权 @EnableWebSecurity
 *
 * 方式一：@Configuration + @Bean 替换底层SecurityFilterChain组件
 * @Bean
 * public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
 *     http.authorizeRequests().anyRequest().authenticated();
 *     http.formLogin();
 *     http.httpBasic();
 *     return http.build();
 * }
 *
 * 方式二：extends WebSecurityConfigurerAdapter
 * 重写configure(AuthenticationManagerBuilder auth)
 */
//@Configuration
//@EnableWebSecurity
public class SecurityInMemoryConfig extends WebSecurityConfigurerAdapter {

    //加密方式
    //@Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //基于内存完成认证操作
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")  //用户名
                .password(passwordEncoder().encode("admin"))  //密码
                .roles("ADMIN")  //拥有的角色
                .authorities("删除","增加") //拥有的权限
                .and()
                .withUser("user")
                .password(passwordEncoder().encode("123"))
                .roles("USER")
                .and()
                .withUser("guest")
                .password(passwordEncoder().encode("123456"))
                .roles("GUEST");
    }
}
