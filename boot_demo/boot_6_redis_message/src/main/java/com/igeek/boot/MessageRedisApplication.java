package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjin
 * 2023/12/18 16:23
 * @description TODO
 */
@SpringBootApplication
public class MessageRedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessageRedisApplication.class, args);
    }
}
