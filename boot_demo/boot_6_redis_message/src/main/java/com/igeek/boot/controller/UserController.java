package com.igeek.boot.controller;

import com.igeek.boot.common.Result;
import com.igeek.boot.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjin
 * 2023/12/18 16:43
 * @description TODO
 *
 * http://localhost:8080/swagger-ui.html
 *
 * http://localhost:8080/doc.html#/home
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Api(tags = "登录接口")
public class UserController {
    @Autowired(required = false)
    private MessageService messageService;

    //生成验证码
    @PostMapping("getCode")
    @ApiOperation("根据手机号获取验证码")
    public Result getCode(String phone){
        String code = messageService.generateVerifyCode(phone);
        return new Result(true,"验证码获取成功",code);
    }

    //登录 获取验证码
    @GetMapping("login")
    @ApiOperation("手机验证码登录")
    public Result login(String phone,String verifyCode){
        String code = messageService.getVerifyCode(phone);
        if (code.equals(verifyCode)){
            return new Result(true,"登录成功",code);
        }
        return new Result(false,"登录失败");
    }
}
