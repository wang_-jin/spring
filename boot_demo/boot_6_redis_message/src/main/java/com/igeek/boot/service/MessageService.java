package com.igeek.boot.service;

import com.igeek.boot.utils.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author wangjin
 * 2023/12/18 16:38
 * @description TODO
 */
@Service
@Slf4j
public class MessageService {

    //定制Redis中存储数据的key规则
    private static final String REDIS_PHONE = "phone::";

    @Autowired(required = false)
    private RedisTemplate redisTemplate;

    //生成验证码
    public String generateVerifyCode(String phone) {

        // 生成验证码
        String verifyCode = RandomUtil.getSixBitRandom();
        // 存储验证码到Redis，使用String数据类型存储,20秒有效时间
        redisTemplate.opsForValue().set(REDIS_PHONE + phone, verifyCode,30, TimeUnit.SECONDS);
        log.info("verifyCode:{}",verifyCode);
        return verifyCode;
    }


    //获取验证码
    public String getVerifyCode(String phone) {
        // 从Redis获取该手机号的验证码
        String code = redisTemplate.opsForValue().get(REDIS_PHONE + phone)+"";
        log.info("code:{}",code);
        return code;
    }
}
