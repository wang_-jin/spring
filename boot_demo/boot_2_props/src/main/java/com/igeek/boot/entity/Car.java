package com.igeek.boot.entity;

import lombok.Data;

/**
 * @author wangjin
 * 2023/12/6 14:11
 * @description TODO
 */
@Data
public class Car {
    private String label;
    private double price;
}
