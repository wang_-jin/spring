package com.igeek.boot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DataSizeUnit;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * @author wangjin
 * 2023/12/6 11:32
 * @description TODO
 */
@Data
@ConfigurationProperties(prefix = "my")
public class MyServer {
    private String ipaddr;
    private int port;
    @DurationUnit(ChronoUnit.MINUTES)
    private Duration timeout;
    @DataSizeUnit(DataUnit.GIGABYTES)
    private DataSize data;
}
