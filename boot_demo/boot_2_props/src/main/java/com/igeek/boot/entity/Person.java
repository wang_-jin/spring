package com.igeek.boot.entity;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/6 14:11
 * @description TODO
 */
@Data
@ConfigurationProperties(prefix = "person")
public class Person {
    private String name;
    private int age;
    private Date birthday;

    private String[] hobby;

    private Car car;
    private List<Car> cars;

    private Map<String,String> map;
    private Map<String,List<Car>> carMap;

}
