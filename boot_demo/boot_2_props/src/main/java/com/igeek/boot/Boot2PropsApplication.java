package com.igeek.boot;

import com.igeek.boot.entity.MyServer;
import com.igeek.boot.entity.People;
import com.igeek.boot.entity.Person;
import com.igeek.boot.entity.Person1;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@EnableConfigurationProperties({MyServer.class, Person.class, People.class, Person1.class})
@Slf4j
@SpringBootApplication
public class Boot2PropsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ac = SpringApplication.run(Boot2PropsApplication.class, args);

        MyServer server = ac.getBean(MyServer.class);
        log.info("server:{}",server);

        Person person = ac.getBean(Person.class);
        log.info("person:{}",person);

        Person1 person1 = ac.getBean(Person1.class);
        log.info("person1:{}",person1);
    }



}
