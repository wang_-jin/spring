package com.igeek.boot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjin
 * 2023/12/6 14:46
 * @description TODO
 */
@Data
@ConfigurationProperties(prefix = "p")
public class People {
    private String name;
    private boolean sex;
    private Date birth;

    private String[] hobby;
    private Pet pet;
    private Set<Pet> pets;

    private List<Map<String,Pet>> mapList;
    private Map<String, List<Pet>> map;
}
