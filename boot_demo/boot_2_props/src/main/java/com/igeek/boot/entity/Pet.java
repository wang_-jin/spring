package com.igeek.boot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wangjin
 * 2023/12/6 14:47
 * @description TODO
 */
@Data
public class Pet {

    private String petName;
    private int petAge;
}
