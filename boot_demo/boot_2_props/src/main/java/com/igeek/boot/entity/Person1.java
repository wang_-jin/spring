package com.igeek.boot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjin
 * 2023/12/6 20:51
 * @description TODO
 */
@Data
@ConfigurationProperties(prefix = "p1")
public class Person1 {
    //字面量：  字符串  整型   时间   布尔
    private String name;
    private Integer age;
    private Date birthday;
    private Boolean flag;

    //对象、Map ： 键值对
    private Car1 car;
    private Map<String,Car1> maps;

    //数组（List、Set） ： 一组值
    private Set<String> set;
    private List<Pet> listObj;

    //复合结构
    private List<Car1> listCars;
    private List<Map<String,Car1>> listMap;
    private Map<String,List<Car1>> mapList;
}
