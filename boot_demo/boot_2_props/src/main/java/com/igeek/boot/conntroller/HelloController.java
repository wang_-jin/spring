package com.igeek.boot.conntroller;

import com.igeek.boot.entity.MyServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author wangjin
 * 2023/12/6 11:10
 * @description TODO
 */

@RestController
@Slf4j
public class HelloController {
    @GetMapping("hello")
    public String hello(){
        try{
            log.info("结果正常: "+ LocalDateTime.now());
        }catch(Exception e){
            e.printStackTrace();
            log.info("结果异常: "+e.getMessage());
        }
        return "HelloController";
    }
}
