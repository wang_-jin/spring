package com.igeek.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.igeek.boot.entity.Items;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjin
 * 2023/12/20 20:07
 * @description TODO
 */
public interface ItemsService extends IService<Items> {

    Items findOne(Integer id);
    Items update(Items items);
    void delete(Integer id);

    Items findByName(String name);
}
