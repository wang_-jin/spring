package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjin
 * 2023/12/20 20:11
 * @description TODO
 */
@SpringBootApplication
//开启缓存
//@EnableCaching
public class CacheMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CacheMainApplication.class, args);
    }
}
