package com.igeek.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author wangjin
 * 2023/12/20 20:02
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("商品类")
public class Items implements Serializable {
    @ApiModelProperty("商品编号")
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;
    private double price;
    private String detail;
    private String pic;

    /*@JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "Asia/Shanghai")*/
    private LocalDateTime createtime;
}
