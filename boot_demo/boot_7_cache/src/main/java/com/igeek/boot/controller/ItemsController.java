package com.igeek.boot.controller;

import com.igeek.boot.entity.Items;
import com.igeek.boot.service.ItemsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangjin
 * 2023/12/20 20:06
 * @description TODO
 */
@RestController
@RequestMapping("/items")
@Api(tags = "商品接口")
public class ItemsController {
    @Autowired(required = false)
    private ItemsService itemsService;

    @ApiOperation("根据商品编号查询商品信息")
    @GetMapping("/findOne/{id}")
    public Items findOne(@PathVariable("id")Integer id){
        return itemsService.findOne(id);
    }

    @ApiOperation("更新商品明细")
    @PutMapping("/update")
    public Items update(Items items){
        return itemsService.update(items);
    }

    @ApiOperation("删除商品明细")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id")Integer id){
        itemsService.delete(id);
    }

    @ApiOperation("根据商品名称查询商品信息")
    @GetMapping("findByName")
    public Items findByName(String name){
        Items items = itemsService.findByName(name);
        return items;
    }
}
