package com.igeek.boot.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/20
 */
//@Configuration
public class MyCacheConfig {

    //@Bean
    public KeyGenerator keyGenerator(){
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {
                // findOne[1]
                return method.getName()+ Arrays.asList(params);
            }
        };
    }

}
