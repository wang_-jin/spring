package com.igeek.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.igeek.boot.entity.Items;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangjin
 * 2023/12/20 20:06
 * @description TODO
 */
@Mapper
public interface ItemsMapper extends BaseMapper<Items> {
}
