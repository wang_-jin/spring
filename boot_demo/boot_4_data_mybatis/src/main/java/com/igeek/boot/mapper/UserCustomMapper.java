package com.igeek.boot.mapper;

import com.igeek.boot.pojo.User;
import org.apache.ibatis.annotations.*;

//UserCustomMapper 自己定义的UserMapper可进行多表查询,不通过xml进行CRUD
public interface UserCustomMapper {

    //<script> 判断
    @Select("select u.name from orders o join user u on o.user_id=u.id where o.id=#{oid}")
    public String selectOrdersAndUser(@Param("oid") int oid);

    //根据编号进行查询用户信息
    @Select("select * from  user where  id=#{id}")
    public User selectOneById(int id);

    //插入用户信息 增删改操作：返回值可以是boolean或者int
    //如果要获取自动生成的键,会自动封装到user对象的id属性中
    @Insert("insert into user values(null ,#{name},#{age},#{gender},#{birthday},null,null,null )")
    public boolean insertUser(User user);
    //根据id更新用户信息
    @Update("update user set name=#{name},age=#{age},gender=#{gender} where id=#{id}")
    public boolean updateUserById(User user);

    //根据id删除用户信息
    @Delete("delete from user where id=#{id}")
    public boolean deleteUserById(int id);
}
