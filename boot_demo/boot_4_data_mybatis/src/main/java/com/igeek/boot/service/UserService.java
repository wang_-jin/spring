package com.igeek.boot.service;

import com.igeek.boot.pojo.User;

import java.util.List;

public interface UserService {

    public User findOne(Integer id);

    public List<User> findAll(String name);

    public boolean insert(User user);

    public boolean update(User user);

    public boolean delete(int id);

    public String findNameByOid(int oid);
}
