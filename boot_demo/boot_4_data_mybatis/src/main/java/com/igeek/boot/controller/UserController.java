package com.igeek.boot.controller;

import com.igeek.boot.mapper.UserCustomMapper;
import com.igeek.boot.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author wangjin
 * 2023/12/8 20:38
 * @description TODO
 */
@RestController
@Slf4j
public class UserController {

    @Autowired(required = false)
    private JdbcTemplate jdbcTemplate;

    @Autowired(required = false)
    private UserCustomMapper userCustomMapper;

/*    @GetMapping("count")
    public Long selectCount(){
        Long count = jdbcTemplate.queryForObject("select count(*) from user", Long.class);
        log.info("count:{}" , count);
        return count;
    }*/
    @GetMapping("/{id}")
    public User selectOneById(@PathVariable("id")int id) {
        User user = userCustomMapper.selectOneById(id);
        log.info("user:{}",user);
        return user;
    }
    @PostMapping("add")
    public String insertUser(){
        User user = new User();
        user.setName("小花");
        user.setAge(18);
        user.setGender("女");
        user.setBirthday(new Date());
        boolean b = userCustomMapper.insertUser(user);
        return b?"插入成功":"插入失败";
    }
    @PutMapping("update")
    public String updateUserById() {
        User user = new User();
        user = userCustomMapper.selectOneById(22);
        user.setName("小明");
        user.setAge(18);
        user.setGender("男");
        boolean b = userCustomMapper.updateUserById(user);
        return b?"更新成功":"更新失败";
    }
    @DeleteMapping("/{id}")
    public String deleteUserById(@PathVariable("id")int id) {
        boolean b = userCustomMapper.deleteUserById(id);
        return b?"删除成功":"删除失败";
    }

    @GetMapping("/findNameByOid/{oid}")
    public String findNameByOid(@PathVariable("oid")int oid){
        return userCustomMapper.selectOrdersAndUser(oid);
    }
}
