package com.igeek.boot.service.impl;

import com.igeek.boot.pojo.User;
import com.igeek.boot.pojo.UserExample;
import com.igeek.boot.mapper.UserCustomMapper;
import com.igeek.boot.mapper.UserMapper;
import com.igeek.boot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Autowired(required = false)
    private UserCustomMapper userCustomMapper;

    @Override
    public User findOne(Integer id) {
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }

    @Override
    public List<User> findAll(String name) {
        //构建条件
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andNameLike("%"+name+"%");
        //发起查询
        List<User> users = userMapper.selectByExample(userExample);
        return users;
    }

    @Override
    public boolean insert(User user) {
        int i = userMapper.insertSelective(user);
        return i>0?true:false;
    }

    @Override
    public boolean update(User user) {
        int i = userMapper.updateByPrimaryKeySelective(user);
        return i>0?true:false;
    }

    @Override
    public boolean delete(int id) {
        int i = userMapper.deleteByPrimaryKey(id);
        return i>0?true:false;
    }

    @Override
    public String findNameByOid(int oid) {
        return userCustomMapper.selectOrdersAndUser(oid);
    }
}
