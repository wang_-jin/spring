package com.igeek.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//方式二：指定扫描包，为其生成代理对象
@MapperScan("com.igeek.boot.mapper")
@SpringBootApplication
public class Boot4DataMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot4DataMybatisApplication.class, args);
    }

}
