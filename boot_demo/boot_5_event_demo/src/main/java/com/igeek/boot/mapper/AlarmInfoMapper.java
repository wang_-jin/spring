package com.igeek.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.igeek.boot.entity.AlarmInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author wangjin
 * 2023/12/13 13:36
 * @description TODO
 */
@Mapper
public interface AlarmInfoMapper extends BaseMapper<AlarmInfo> {
    @Select("<script>" +
                "select a.id , a.wan_ip, a.event_time , a.event_reasons , d.dept_name , m.display_value\n" +
                "from zhrj_alarm_info a join zhrj_dept d join zhrj_menu m " +
                "on a.dept_id=d.dept_id and a.event_type=m.actual_value " +
                "<where>" +
                    "and m.menu_name='event_type' " +
                    "<if test='wanIp!=null and wanIp!=\"\"'> " +
                        "and a.wan_ip=#{wanIp}" +
                    "</if>" +
                "</where>" +
            "</script>")
    public List<AlarmInfo> findAllAlarmInfo(@Param("wanIp") String wanIp);
}

