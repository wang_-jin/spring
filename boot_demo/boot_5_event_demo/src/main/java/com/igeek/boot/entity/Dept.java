package com.igeek.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/13 11:22
 * @description TODO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("组织机构表")
@TableName("zhrj_dept")
public class Dept {

    @TableId(value = "dept_id" , type = IdType.AUTO)
    @ApiModelProperty("部门ID")
    private Long deptId;

    @ApiModelProperty("父部门id")
    private Long parentId;

    @ApiModelProperty("祖级列表")
    private String ancestors;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("部门编号")
    private String deptno;

    @ApiModelProperty("显示顺序")
    private Integer orderNum;
}

