package com.igeek.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.igeek.boot.dto.AlarmInfoEventDateDTO;
import com.igeek.boot.entity.AlarmInfo;

import java.util.List;
import java.util.Map;

public interface AlarmInfoService extends IService<AlarmInfo> {
    Map<String,Object> findEventStatistic();
    List<AlarmInfoEventDateDTO> findEventMonthDateCount(String month);
    List<AlarmInfo> findAllAlarmInfo(String wanIp);
    //根据事件种类查询分页
    List<AlarmInfo> findPageAlarmInfoByType(int eventType);
}
