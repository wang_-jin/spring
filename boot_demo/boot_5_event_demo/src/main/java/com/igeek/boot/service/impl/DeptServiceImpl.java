package com.igeek.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.igeek.boot.entity.Dept;
import com.igeek.boot.mapper.DeptMapper;
import com.igeek.boot.service.DeptService;
import org.springframework.stereotype.Service;

/**
 * @author wangjin
 * 2023/12/13 13:38
 * @description TODO
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper,Dept> implements DeptService {
}
