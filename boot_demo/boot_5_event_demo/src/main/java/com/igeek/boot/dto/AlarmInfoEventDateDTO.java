package com.igeek.boot.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
/**
 * @author wangjin
 * 2023/12/13 18:18
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("日期数量表")
public class AlarmInfoEventDateDTO {
    @ApiModelProperty("事件日期")
    private LocalDate eventDate;
    @ApiModelProperty("数量")
    private Integer count;
}
