package com.igeek.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.igeek.boot.dto.AlarmInfoDeptIdDTO;
import com.igeek.boot.dto.AlarmInfoEventDateDTO;
import com.igeek.boot.dto.AlarmInfoEventTypeDTO;
import com.igeek.boot.entity.AlarmInfo;
import com.igeek.boot.mapper.AlarmInfoMapper;
import com.igeek.boot.service.AlarmInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wangjin
 * 2023/12/13 13:38
 * @description TODO
 */
@Service
public class AlarmInfoServiceImpl extends ServiceImpl<AlarmInfoMapper, AlarmInfo> implements AlarmInfoService {

    @Autowired(required = false)
    private AlarmInfoMapper alarmInfoMapper;
    //事件按类型统计查询
    @Override
    public Map<String, Object> findEventStatistic() {
        //查询结果包括不同类型事件数量，每种类型及数量在一个对象中，例如：{eventType:1,counts:10}
        //select event_type as eventType ,count(*) as count from zhrj_alarm_info GROUP BY event_type
        QueryWrapper<AlarmInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("event_type" , "count(*) as count").groupBy("event_type");
        List<Map<String, Object>> mapList1 = baseMapper.selectMaps(queryWrapper);
        List<AlarmInfoEventTypeDTO> alarmInfoEventTypeDTOS = mapList1.stream().map(map ->{
            Integer eventType = Integer.parseInt(map.get("event_type") + "");
            Integer count = Integer.parseInt(map.get("count") + "");
            return new AlarmInfoEventTypeDTO(eventType,count);
        }).collect(Collectors.toList());

        //查询结果包括不同（维护）部门的事件数量，每个部门及数量在同一个对象中，例如：{deptId:103,count:10}
        //sselect dept_id,count(*) as count from zhrj_alarm_info GROUP BY dept_id
        List<Map<String,Object>> mapList2 = baseMapper.selectMaps(new QueryWrapper<AlarmInfo>()
                .select("dept_id","count(*) as count")
                .groupBy("dept_id"));
        List<AlarmInfoDeptIdDTO> alarmInfoDeptIdDTOs = mapList2.stream().map(map ->{
            Integer deptId = Integer.parseInt(map.get("dept_id")+"");
            Integer count = Integer.parseInt(map.get("count")+"");
            return new AlarmInfoDeptIdDTO(deptId,count);
        }).collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("alarmInfoEventTypeDTOS",alarmInfoEventTypeDTOS);
        map.put("alarmInfoDeptIdDTOs",alarmInfoDeptIdDTOs);
        return map;
    }
    //月度事件数量趋势接口
    //select DATE(event_time),count(*) as count from zhrj_alarm_info where MONTH(event_time)='03' GROUP BY DATE(event_time);
    @Override
    public List<AlarmInfoEventDateDTO> findEventMonthDateCount(String month) {
        //SELECT DATE(event_time) as eventDate , COUNT(1) as count from zhrj_alarm_info where MONTH(event_time)='03' GROUP BY DATE(event_time);
        List<Map<String, Object>> maps = baseMapper.selectMaps(new QueryWrapper<AlarmInfo>()
                .select("DATE(event_time) as eventDate" , "COUNT(1) as count")
                .eq("MONTH(event_time)", month)
                .groupBy("DATE(event_time)"));
        //List<Map<String, Object>>  -->  List<AlarmInfoEventDateDTO>
        List<AlarmInfoEventDateDTO> eventDateDTOList = maps.stream().map(entry -> {
            LocalDate eventDate = LocalDate.parse(entry.get("eventDate") + "");
            Integer count = Integer.parseInt(entry.get("count") + "");
            return new AlarmInfoEventDateDTO(eventDate, count);
        }).collect(Collectors.toList());
        return eventDateDTOList;
    }

    //通过终端IP查询事件信息详情（包含部门名称、展示菜单名字）
    @Override
    public List<AlarmInfo> findAllAlarmInfo(String wanIp) {
        List<AlarmInfo> allAlarmInfo = alarmInfoMapper.findAllAlarmInfo(wanIp);
        return allAlarmInfo;
    }

    @Override
    public List<AlarmInfo> findPageAlarmInfoByType(int eventType) {
        List<AlarmInfo> alarmInfos = baseMapper.selectList(new LambdaQueryWrapper<AlarmInfo>()
                .select(AlarmInfo::getEventType, AlarmInfo::getEventTime, AlarmInfo::getEventReasons)
                .eq(AlarmInfo::getEventType, eventType));
        return alarmInfos;
    }
}