package com.igeek.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.igeek.boot.entity.Dept;

public interface DeptService extends IService<Dept> {
}
