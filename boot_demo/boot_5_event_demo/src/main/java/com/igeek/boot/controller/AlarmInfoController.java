package com.igeek.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeek.boot.common.Result;
import com.igeek.boot.dto.AlarmInfoEventDateDTO;
import com.igeek.boot.dto.AlarmPageVO;
import com.igeek.boot.entity.AlarmInfo;
import com.igeek.boot.mapper.AlarmInfoMapper;
import com.igeek.boot.service.AlarmInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/13 13:33
 * @description TODO
 *
 * Swagger2 测试地址
 * http://localhost:8080/swagger-ui.html
 *
 * Knife4j 测试地址
 * http://localhost:8080/doc.html#/home
 */
@RestController
@RequestMapping("/zhrj/events")
@Slf4j
@Api(tags = "事件信息的控制层")
public class AlarmInfoController {

    @Autowired
    private AlarmInfoService infoService;


    @GetMapping("/statistic")
    @ApiOperation("事件按类型统计查询")
    public Result eventStatistic(){
        try{
            Map<String, Object> map = infoService.findEventStatistic();
            return new Result(200,"查询成功",map);
        }catch(Exception e){
            return new Result(311,"查询失败");
        }
    }

    @GetMapping("/month_date_count")
    @ApiOperation("月度事件数量趋势接口")
    public Result eventMonthDateCount(String month){
        //List
        try{
            month = month.substring(5);
            List<AlarmInfoEventDateDTO> list = infoService.findEventMonthDateCount(month);
            return new Result(200,"查询成功",list);
        }catch(Exception e){
            return new Result(311,"查询失败");
        }
    }

    @GetMapping("/findAll/{wanIp}")
    @ApiOperation("通过终端IP查询事件信息详情")
    public Result findAll(@ApiParam("终端IP") @PathVariable("wanIp")String wanIp){
        //List
        try{
            List<AlarmInfo> list = infoService.findAllAlarmInfo(wanIp);
            return new Result(200,"查询成功",list);
        }catch(Exception e){
            return new Result(311,"查询失败");
        }
    }

    @PostMapping("/findPage/{current}/{size}")
    @ApiOperation("通过事件类型分页查询事件信息详情")
    public Result findPage(
            @ApiParam("当前页")@PathVariable(value = "current")Integer current,
            @ApiParam("每页展示最大条数")@PathVariable(value = "size")Integer size,
            @ApiParam("查询条件")@RequestBody AlarmPageVO alarmPageVO
            ){

        try{
            Page<AlarmInfo> page = new Page<>(current,size);
            LambdaQueryWrapper<AlarmInfo> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper
                    .select(AlarmInfo::getId,AlarmInfo::getEventTime,AlarmInfo::getEventType, AlarmInfo::getEventTime, AlarmInfo::getEventReasons)
                    .eq(AlarmInfo::getEventType, alarmPageVO.getEventType());
            Page<AlarmInfo> infoPage = infoService.page(page, queryWrapper);
            return new Result(200,"查询成功",infoPage);
        }catch(Exception e){
            return new Result(311,"查询失败");
        }
    }
}

