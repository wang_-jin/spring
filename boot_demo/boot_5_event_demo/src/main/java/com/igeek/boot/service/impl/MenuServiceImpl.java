package com.igeek.boot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.igeek.boot.entity.Menu;
import com.igeek.boot.mapper.MenuMapper;
import com.igeek.boot.service.MenuService;
import org.springframework.stereotype.Service;

/**
 * @author wangjin
 * 2023/12/13 13:39
 * @description TODO
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
}
