package com.igeek.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/13 11:22
 * @description TODO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("zhrj_menu")
@ApiModel("事件菜单")
public class Menu {

    //@TableId(value = "menu_name" , type = IdType.INPUT)
    @ApiModelProperty("菜单名称")
    private String menuName;

    @TableId(value = "actual_value" , type = IdType.INPUT)
    @ApiModelProperty("实际值")
    private int actualValue;

    @ApiModelProperty("显示值")
    private String displayValue;
    @ApiModelProperty("菜单描述")
    private String menuDescription;

}
