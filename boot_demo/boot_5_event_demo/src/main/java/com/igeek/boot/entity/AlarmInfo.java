package com.igeek.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author wangjin
 * 2023/12/13 11:30
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("事件信息表")
@TableName("zhrj_alarm_info")
public class AlarmInfo {

    @TableId(type = IdType.INPUT)
    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("部门ID")
    private Long deptId;

    @ApiModelProperty("终端IP")
    private String wanIp;

    @ApiModelProperty("采集时间")
    private LocalDateTime eventTime;

    @ApiModelProperty("事件类型")
    private Integer eventType;

    @ApiModelProperty("事件状态")
    private Integer eventStatus;

    @ApiModelProperty("事件归集原因")
    private String eventReasons;
    @TableField(exist = false)
    private String deptName;
    @TableField(exist = false)
    private String displayValue;
}

