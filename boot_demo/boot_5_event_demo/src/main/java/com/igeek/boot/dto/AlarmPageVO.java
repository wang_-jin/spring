package com.igeek.boot.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/13 20:04
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("分页查询事件条件封装")
public class AlarmPageVO {
    @ApiModelProperty("事件种类")
    private Integer eventType;

}
