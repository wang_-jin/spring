package com.igeek.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.igeek.boot.entity.Menu;

public interface MenuService extends IService<Menu> {
}
