package com.igeek.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.igeek.boot.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangjin
 * 2023/12/13 13:37
 * @description TODO
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
}