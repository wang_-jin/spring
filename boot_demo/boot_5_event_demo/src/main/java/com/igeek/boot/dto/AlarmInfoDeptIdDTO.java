package com.igeek.boot.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author wangjin
 * 2023/12/13 18:18
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("部门数量表")
public class AlarmInfoDeptIdDTO {
    @ApiModelProperty("部门ID")
    private Integer deptId;
    @ApiModelProperty("数量")
    private Integer count;
}
