package com.igeek.boot.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/13 13:32
 * @description TODO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("结果集")
public class Result {

    @ApiModelProperty("响应结果状态码")
    private Integer code;
    @ApiModelProperty("响应结果信息")
    private String message;
    @ApiModelProperty("响应结果数据")
    private Object data;

    public Result(Integer code , String message){
        this.code = code;
        this.message = message;
    }

}