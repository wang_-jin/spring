package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjin
 * 2023/12/13 11:13
 * @description TODO
 */
@SpringBootApplication
public class EventMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(EventMainApplication.class,args);
    }
}
