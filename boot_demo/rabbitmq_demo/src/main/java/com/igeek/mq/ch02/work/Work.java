package com.igeek.mq.ch02.work;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

/**
 * @author wangjin
 * 2024/1/23 19:40
 * @description TODO
 */
public class Work {
    public static void main(String[] args) throws IOException {
        //获取信道
        Channel channel = MQUtils.getChannel();
        System.out.println("=======Work02=======");

        DeliverCallback deliverCallback = (s, delivery) -> {
            String message = new String(delivery.getBody() , "UTF-8");
            System.out.println("Work02 正常接收到消息："+message);
        };

        /**
         * 消费者接收消息
         * 第一个参数：队列的名称。
         * 第二个参数：消费成功之后是否要自动应答，true代表自动应答，false手动应答
         * 第三个参数：消息传递时的回调
         * 第四个参数：消息被取消时的回调
         */
        channel.basicConsume(Producer.QUEUE_WORK, true, deliverCallback, s -> System.out.println("消息中断接收~"));
    }
}
