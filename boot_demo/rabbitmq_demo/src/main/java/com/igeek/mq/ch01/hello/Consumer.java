package com.igeek.mq.ch01.hello;

import com.rabbitmq.client.*;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author wangjin
 * 2024/1/23 19:18
 * @description 消费者
 */
public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        //1.建立连接
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("116.62.111.201");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("123456");
        Connection connection = factory.newConnection();

        //2.获取信道
        Channel channel = connection.createChannel();
        //消息正常传递时的回调
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String s, Delivery delivery) throws IOException {
                String msg = new String(delivery.getBody(), "UTF-8");
                System.out.println("正常消费接收到消息：" + msg);
            }
        };

        //消息被取消时的回调
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String s) throws IOException {
                System.out.println("消息中断消费~");
            }
        };

        //3.消费者消费消息
        /**
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
         * 3.消息传递时的回调
         * 4.消息被取消时的回调
         */
        channel.basicConsume(Producer.QUEUE_HELLO , true , deliverCallback , cancelCallback);
    }
}
