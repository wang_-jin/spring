package com.igeek.mq.ch02.ack;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 消费者工作很快
 *
 * @author chemin
 * @since 2024/1/24
 */
public class Work01 {

    public static void main(String[] args) throws IOException {
        Channel channel = MQUtils.getChannel();
        System.out.println("--------Work01工作效率高--------");

        //正常接收到消息回调
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery delivery) throws IOException {
                String msg = new String(delivery.getBody() , "UTF-8");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Work01 正常接收到消息："+msg);

                //第二步：消息应答 deliveryTag消息的Tag , multiple是否批量应答，true批量应答，false一个个应答
                channel.basicAck(delivery.getEnvelope().getDeliveryTag() , false);
            }
        };

        //轮询分发  默认prefetchCount值为0
        //int prefetchCount = 0;

        //设置不公平分发
        //int prefetchCount = 1;

        //设置预取值分发
        int prefetchCount = 2;
        channel.basicQos(prefetchCount);

        /**
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
         * 3.消息传递时的回调
         * 4.消息被取消时的回调
         */
        //第一步：设置手动应答消息
        boolean autoAck = false;
        channel.basicConsume(Producer.QUEUE_ACK, autoAck , deliverCallback , cancelCallback-> System.out.println("消息中断接收~"));
    }

}
