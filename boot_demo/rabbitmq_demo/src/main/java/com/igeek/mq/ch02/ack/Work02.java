package com.igeek.mq.ch02.ack;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 消费者工作很慢
 *
 * @author chemin
 * @since 2024/1/24
 */
public class Work02 {

    public static void main(String[] args) throws IOException {
        Channel channel = MQUtils.getChannel();
        System.out.println("--------Work02工作效率低--------");

        //正常接收到消息回调
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery delivery) throws IOException {
                String msg = new String(delivery.getBody() , "UTF-8");

                try {
                    TimeUnit.SECONDS.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Work02 正常接收到消息："+msg);

                //消息应答
                channel.basicAck(delivery.getEnvelope().getDeliveryTag() , false);

            }
        };

        //设置不公平分发
        //int prefetchCount = 1;

        //设置预取值分发
        int prefetchCount = 4;
        channel.basicQos(prefetchCount);

        /**
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
         * 3.消息传递时的回调
         * 4.消息被取消时的回调
         */
        boolean autoAck = false;
        channel.basicConsume(Producer.QUEUE_ACK, autoAck , deliverCallback , cancelCallback-> System.out.println("消息中断接收~"));
    }

}
