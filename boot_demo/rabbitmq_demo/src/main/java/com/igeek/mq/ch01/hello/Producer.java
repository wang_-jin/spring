package com.igeek.mq.ch01.hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 *
 * @author chemin
 * @since 2024/1/23
 *
 * 需求：生产者和消费者连接RabbitMQ服务器，由生产者发送一条消息，消费者接收消息。
 * “默认交换机”
 */
public class Producer {

    //声明队列
    public static final String QUEUE_HELLO= "hello";

    public static void main(String[] args) throws IOException, TimeoutException {

        //1.建立连接
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("116.62.111.201");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("123456");
        Connection connection = factory.newConnection();
        //2.获取信道
        Channel channel = connection.createChannel();

        //3.声明队列
        /**
         * queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
         * 第一个参数：队列名称。
         * 第二个参数：队列的持久化（磁盘）即服务器重启后队列是否还存在，默认false代表存储在内存中，true代表持久化。
         * 第三个参数：该队列是否只供一个消费者进行消费，是否进行消息共享，true代表可以多个消费者消费，false代表只能一个消费者消费。
         * 第四个参数：是否自动删除，最后一个消费者断开连接以后，该队列是否自动删除， true自动删除，false不自动删除。
         * 第五个参数：其它参数
         */
        channel.queueDeclare(QUEUE_HELLO, false, false, false, null);
        //4.生产者发送消息
        /**
         * 第一个参数：发送到哪个交换机，此处使用""默认交换机
         * 第二个参数：路由的Key值是哪个，即本次队列的名称
         * 第三个参数：其它参数信息
         * 第四个参数：发送消息的消息体的字节数组
         */
        String msg = "你好 Hello RabbitMQ";
        channel.basicPublish("", QUEUE_HELLO, null, msg.getBytes("UTF-8"));
        System.out.println("消息发送完毕！");
    }
}
