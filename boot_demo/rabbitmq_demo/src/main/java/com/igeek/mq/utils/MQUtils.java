package com.igeek.mq.utils;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/23
 */
public class MQUtils {

    public static Channel getChannel(){

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("116.62.111.201");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("123456");
        Connection connection = null;

        try {
            //1.建立连接
            connection = factory.newConnection();
            //2.获取信道
            Channel channel = connection.createChannel();
            return channel;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

}
