package com.igeek.mq.ch02.ack;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;

/**
 * 需求：生产者无限发送消息，由2个工作线程消费消息，符合轮询分发消息的规则。
 * 其中一个工作线程工作很快，另一个工作线程工作很慢。
 *
 * @author chemin
 * @since 2024/1/23
 */
public class Producer {

    //声明队列的名字
    public static final String QUEUE_ACK = "ack";

    public static void main(String[] args) throws IOException {
        //1.获取信道
        Channel channel = MQUtils.getChannel();

        //2.声明队列
        /**
         * queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
         * 第一个参数：队列名称。
         * 第二个参数：队列的持久化（磁盘）即服务器重启后队列是否还存在，默认false代表存储在内存中，true代表持久化。
         * 第三个参数：该队列是否只供一个消费者进行消费，是否进行消息共享，true代表可以多个消费者消费，false代表只能一个消费者消费。
         * 第四个参数：是否自动删除，最后一个消费者断开连接以后，该队列是否自动删除， true自动删除，false不自动删除。
         * 第五个参数：其它参数
         */
        //设置队列的持久化
        boolean durable = true;
        channel.queueDeclare(QUEUE_ACK , durable , false , false , null);

        //3.发送消息
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String msg = sc.nextLine();
            /**
             * 第一个参数：发送到哪个交换机，此处使用""默认交换机
             * 第二个参数：路由的Key值是哪个，即本次队列的名称
             * 第三个参数：其它参数信息，例如：设置消息的持久化等MessageProperties.PERSISTENT_TEXT_PLAIN
             * 第四个参数：发送消息的消息体的字节数组
             */
            //设置消息的持久化
            channel.basicPublish("" , QUEUE_ACK , MessageProperties.PERSISTENT_TEXT_PLAIN, msg.getBytes("UTF-8"));
            System.out.println("消息发送完毕 message = "+msg);
        }
    }

}
