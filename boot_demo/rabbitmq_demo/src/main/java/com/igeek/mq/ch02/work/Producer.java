package com.igeek.mq.ch02.work;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author wangjin
 * 2024/1/23 19:40
 * @description TODO
 */
public class Producer {

    //声明队列的名字
    public static final String QUEUE_WORK = "work";
    public static void main(String[] args) throws IOException {
        //1.获取信道
        Channel channel = MQUtils.getChannel();

        //2.声明队列
        channel.queueDeclare(QUEUE_WORK , false , false , false , null);

        //3.发送消息
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String msg = sc.nextLine();
            channel.basicPublish("" , QUEUE_WORK , null , msg.getBytes("UTF-8"));
            System.out.println("消息发送完毕 message = "+msg);
        }
    }
}
