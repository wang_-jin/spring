package com.igeek.mq.ch04.fanout;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
/**
 * @author wangjin
 * 2024/1/24 19:57
 * @description TODO
 */
public class ReceiveLogs02 {
    public static void main(String[] args) throws IOException {
        //1.获取信道
        Channel channel = MQUtils.getChannel();
        //2.创建交换机  第一个参数：交换机名字  第二个参数：交换机类型
        channel.exchangeDeclare(SendLogs.EXCHANGE_FANOUT_LOGS , BuiltinExchangeType.FANOUT);
        System.out.println("========消费者ReceiveLogs02等待接收消息========");

        //3.创建临时队列
        String queue = channel.queueDeclare().getQueue();
        //4.将交换机和队列绑定
        channel.queueBind(queue , SendLogs.EXCHANGE_FANOUT_LOGS , "");

        //5.接收消息
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery delivery) throws IOException {
                String message = new String(delivery.getBody() , "UTF-8");
                //消费者ReceiveLogs02消费队列Q中的消息，将消息输出至磁盘上。
                FileUtils.writeStringToFile(new File("E:\\127\\6.RabbitMQ\\test.txt") , message , "UTF-8" , true);
                System.out.println("将消息输出至磁盘完成~");
            }
        };
        channel.basicConsume(queue , false , deliverCallback , cancelCallback-> System.out.println("消息中断接收~"));
    }
}
