package com.igeek.mq.ch04.fanout;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * 需求：日志生产者，error、debug、info、warning等等，将会按照“广播”模式，将所有日志消息投递到所有队列中；
 * 消费者ReceiveLogs01消费队列A中的消息，将消息输出至控制台上，
 * 消费者ReceiveLogs02消费队列Q中的消息，将消息输出至磁盘上。
 * @author wangjin
 * 2024/1/24 19:57
 * @description TODO
 */
public class SendLogs {
    //声明交换机
    public static final String EXCHANGE_FANOUT_LOGS = "exchange_fanout_logs";

    public static void main(String[] args) throws IOException {
        //1.获取信道
        Channel channel = MQUtils.getChannel();
        //2.创建交换机  第一个参数：交换机名字  第二个参数：交换机类型
        channel.exchangeDeclare(EXCHANGE_FANOUT_LOGS , BuiltinExchangeType.FANOUT);

        //3.组装消息
        Map<String,String> map = new HashMap<>();
        map.put("info" , "这是一条info日志");
        map.put("debug" , "这是一条debug日志");
        map.put("warning" , "这是一条warning日志");
        map.put("error" , "这是一条error日志");

        //4.发送消息
        Set<String> keys = map.keySet();
        keys.forEach(key ->{
            String message = map.get(key);
            try {
                channel.basicPublish(EXCHANGE_FANOUT_LOGS , "" , MessageProperties.PERSISTENT_TEXT_PLAIN , message.getBytes("UTF-8"));
                System.out.println("发送消息完毕"+message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
