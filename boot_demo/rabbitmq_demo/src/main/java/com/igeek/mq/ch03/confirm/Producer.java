package com.igeek.mq.ch03.confirm;

import com.igeek.mq.utils.MQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author wangjin
 * 2024/1/24 19:56
 * @description TODO
 */
public class Producer {
    //声明队列
    public static final String QUEUE_CONFIRM = "confirm";
    //发布消息数量
    public static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) throws IOException, InterruptedException {
        //单个确认发布耗时情况：20769ms
        //singleConfirmPublish();

        //批量确认发布耗时情况：441ms
        //batchConfirmPublish();

        //异步确认发布耗时情况：41ms
        asyncConfirmPublish();
    }
    //1.单个确认发布  同步确认发布  发布速度特别的慢
    //特点：最多提供每秒不超过数百条发布消息的吞吐量
    public static void singleConfirmPublish() throws IOException, InterruptedException {
        Channel channel = MQUtils.getChannel();
        //队列持久化
        channel.queueDeclare(QUEUE_CONFIRM , true , false , false , null);

        //第一步：开启发布确认
        channel.confirmSelect();

        //单个确认发布
        long begin = System.currentTimeMillis();
        for (int i = 0; i <= MESSAGE_COUNT ; i++) {
            String msg = "msg"+i;
            //消息持久化
            channel.basicPublish("" , QUEUE_CONFIRM , MessageProperties.PERSISTENT_TEXT_PLAIN , msg.getBytes("UTF-8"));

            //第二步：等待消息确认
            boolean flag = channel.waitForConfirms();
            if (flag){
                System.out.println("确认消息发送成功："+msg);
            }else{
                System.out.println("确认消息发送未成功："+msg);
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("单个确认发布耗时情况："+(end - begin)+"ms");
    }

    //2.批量确认发布  同步确认发布  吞吐量会提高
    //特点：当发生故障导致发布出现问题时，不知道是哪个消息出现问题了，我们必须将整个批处理保存在内存中，以记录重要的信息而后重新发布消息。
    public static void batchConfirmPublish() throws IOException, InterruptedException {
        Channel channel = MQUtils.getChannel();
        //队列持久化
        channel.queueDeclare(QUEUE_CONFIRM , true , false , false , null);

        //第一步：开启发布确认
        channel.confirmSelect();

        //设置批量
        int batchSize = 100;

        //单个确认发布
        long begin = System.currentTimeMillis();
        for (int i = 1; i <=MESSAGE_COUNT; i++) {
            String msg = "msg"+i;
            //消息持久化
            channel.basicPublish("" , QUEUE_CONFIRM , MessageProperties.PERSISTENT_TEXT_PLAIN , msg.getBytes("UTF-8"));

            //第二步：等待消息确认
            if(i%batchSize==0){
                channel.waitForConfirms();
                System.out.println("确认消息发送成功");
            }

        }
        long end = System.currentTimeMillis();
        System.out.println("批量确认发布耗时情况："+(end - begin)+"ms");
    }

    //3.异步确认发布
    public static void asyncConfirmPublish() throws IOException, InterruptedException {
        Channel channel = MQUtils.getChannel();
        //队列持久化
        channel.queueDeclare(QUEUE_CONFIRM , true , false , false , null);

        //第一步：开启发布确认
        channel.confirmSelect();

        /**
         * ConcurrentSkipListMap  存储所有的发布消息  键Key是消息tag  值Value消息msg
         * 线程安全有序的一个哈希表 适用于高并发的情况下
         * 1.将序号与消息Map关联 序号作为key，消息作为value
         * 2.通过序号，可以批量删除消息
         * 3.支持高并发（多线程）
         */
        ConcurrentSkipListMap<Long,String> skipListMap = new ConcurrentSkipListMap();

        //确认回调
        ConfirmCallback ackConfirmCallback = new ConfirmCallback() {
            //deliveryTag消息标记tag  multiple是否批量确认，true批量确认，false一个个确认
            @Override
            public void handle(long deliveryTag, boolean multiple) throws IOException {
                if(multiple){
                    //true批量确认
                    skipListMap.headMap(deliveryTag).clear();
                }else{
                    //false一个个确认
                    skipListMap.remove(deliveryTag);
                }
                System.out.println("确认的消息的标记："+deliveryTag);
            }
        };

        //未确认回调
        //Channel newChannel = MQUtils.getChannel();
        ConfirmCallback nackConfirmCallback = new ConfirmCallback() {
            @Override
            public void handle(long deliveryTag, boolean multiple) throws IOException {
                String msg = skipListMap.get(deliveryTag);
                System.out.println("未确认消息 tag："+ deliveryTag +" , 未确认消息的内容 msg："+msg);
                //newChannel.basicPublish("" , Producer.QUEUE_CONFIRM  ,MessageProperties.PERSISTENT_TEXT_PLAIN , msg.getBytes("UTF-8"));
            }
        };

        //第二步：添加确认监听
        channel.addConfirmListener(ackConfirmCallback , nackConfirmCallback);

        //异步确认发布
        long begin = System.currentTimeMillis();
        for (int i = 1; i <=MESSAGE_COUNT; i++) {
            String msg = "msg"+i;
            //消息持久化
            channel.basicPublish("" , QUEUE_CONFIRM , MessageProperties.PERSISTENT_TEXT_PLAIN , msg.getBytes("UTF-8"));

            //第三步：存储发布的消息 = 确认消息 + 未确认消息
            //以获取信道下一次发布消息的序号作为key，以消息内容作为value
            skipListMap.put(channel.getNextPublishSeqNo() , msg);
        }
        long end = System.currentTimeMillis();
        System.out.println("异步确认发布耗时情况："+(end - begin)+"ms");
    }
}
