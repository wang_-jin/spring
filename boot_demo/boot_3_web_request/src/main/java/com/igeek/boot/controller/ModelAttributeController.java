package com.igeek.boot.controller;

import com.igeek.boot.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wangjin
 * 2023/12/7 20:33
 * @description TODO
 */
@Controller
public class ModelAttributeController {

    //方式一：@ModelAttribute  直接用在方法上
    /**
     * @ModelAttribute 用在没有返回值的方法上
     * 则必须搭配Model一起使用 ， 将数据放置在请求作用域中
     */
    @ModelAttribute
    public void test1(Model model){
        User user = new User("李思思" , "123123");
        model.addAttribute("user" , user);
    }

    /**
     * @ModelAttribute 用在有返回值的方法上
     * 则默认将返回值放置在请求作用域中
     * 默认策略：
     *      - 默认使用类名首字母小写作为key，即user
     *      - @ModelAttribute("user1") ,则会使用指定的key，即user1
     */
    @ModelAttribute("user1")
    public User test2(){
        User user = new User("张三三" , "555666");
        return user;
    }

    //方式二：@ModelAttribute  直接用在方法的形参上,获取请求域中的数据
    @GetMapping("/getUser1")
    @ResponseBody
    public User getUser1(@ModelAttribute User user){
        return user;
    }

    @GetMapping("/getUser2")
    @ResponseBody
    public User getUser2(@ModelAttribute("user1") User user){
        return user;
    }
}
