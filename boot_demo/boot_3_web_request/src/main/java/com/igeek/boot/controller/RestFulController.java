package com.igeek.boot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangjin
 * 2023/12/6 16:16
 * @description TODO
 *
 * 接口测试常用工具
 * 1.Postman
 * 2.ApiFox
 *
 * SpringBoot集成测试文档
 * 1.Swagger
 * 2.knife4j  离线API文档
 *
 * 作业：Vue 发起axios请求（增、删、改、查）
 */
@RestController
@RequestMapping(value = "/rest", produces = "application/json; charset=utf-8")
@Slf4j
public class RestFulController {

    /**
     * 1.增  @PostMapping  等价于  @RequestMapping(value = "add" , method = RequestMethod.POST)
     * 常见异常：405异常 Method Not Allowed
     * Postman测试，测试地址：http://localhost:8080/rest/add  POST方式
     * @return  JSON数据格式
     */

    @PostMapping("add")
    public String add(){
        String msg = "add 增加";
        return msg;
    }
    /**
     * 2.删  @DeleteMapping
     * Postman测试，测试地址：http://localhost:8080/rest/del  DELETE方式
     * @return  JSON数据格式
     */
    @DeleteMapping("del")
    public String del(){
        String msg = "delete 删除";
        return msg;
    }

    /**
     * 3.改 @PutMapping
     * Postman测试, 测试地址:http://localhost:8080/rest/update PUT方式
     * @return JSON数据格式
     */
    @PutMapping("update")
    public String update(){
        String msg = "update 更新";
        return msg;
    }

    /**
     * 4.查  @GetMapping
     * Postman测试，测试地址：http://localhost:8080/rest/find  GET方式
     * @return JSON数据格式
     */
    @GetMapping("find")
    public String find(){
        String msg = "update 修改";
        return msg;
    }
}
