package com.igeek.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author wangjin
 * 2023/12/7 15:19
 * @description TODO
 *
 * @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
 * 相当于自定义参数绑定器，解析时间格式为指定的内容 2000-12-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private String name;
    private int age;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    //一对一
    private Pet pet;

}