package com.igeek.boot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/7 20:33
 * @description TODO
 *
 * /url?k1=v1&k2=v2
 * /url;k1=v1;k2=v2
 *
 * 矩阵变量 @MatrixVariable     /userList.jsp;jessionId=xxxxx
 */
@RestController
public class MatrixVariableController {

    /**
     * 矩阵变量 @MatrixVariable
     *
     * 测试地址1：http://localhost:8080/matrix/1;name=zs;gender=false;age=18
     * 测试地址2：http://localhost:8080/matrix/1;name=zs;gender=false;age=18;age=20
     * 测试地址3：http://localhost:8080/matrix/1;name=zs;gender=false;age=18,20
     */
    @GetMapping("/matrix/{user}")
    public Map<String,Object> testMatrix1(
            @MatrixVariable(value = "name",pathVar = "user",required = false,defaultValue = "")String name,
            @MatrixVariable(value = "gender",pathVar = "user",required = false,defaultValue = "false")boolean gender,
            @MatrixVariable(value = "age",pathVar = "user",required = false,defaultValue = "18")Integer[] age
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("name" , name);
        map.put("gender" , gender);
        map.put("age" , Arrays.toString(age));
        return map;
    }


    /**
     * 矩阵变量 @MatrixVariable
     *
     * 测试地址4：http://localhost:8080/matrix/1;name=zs;gender=false;age=18/2;name=ls;gender=true;age=22
     */
    @GetMapping("/matrix/{user}/{emp}")
    public Map<String,Object> testMatrix2(
            @MatrixVariable(value = "name",pathVar = "user",required = false,defaultValue = "")String userName,
            @MatrixVariable(value = "name",pathVar = "emp",required = false,defaultValue = "")String empName,
            @MatrixVariable(value = "gender",pathVar = "user",required = false,defaultValue = "false")boolean userGender,
            @MatrixVariable(value = "gender",pathVar = "emp",required = false,defaultValue = "false")boolean empGender,
            @MatrixVariable(value = "age",pathVar = "user",required = false,defaultValue = "18")Integer userAge,
            @MatrixVariable(value = "age",pathVar = "emp",required = false,defaultValue = "18")Integer empAge
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("userName" , userName);
        map.put("userGender" , userGender);
        map.put("userAge" , userAge);
        map.put("empName" , empName);
        map.put("empGender" , empGender);
        map.put("empAge" , empAge);
        return map;
    }
}
