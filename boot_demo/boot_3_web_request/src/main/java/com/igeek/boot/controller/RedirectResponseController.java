package com.igeek.boot.controller;

import com.igeek.boot.entity.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/7 20:33
 * @description TODO
 */
@Controller
public class RedirectResponseController {

    @GetMapping("/gotoRedirect")
    public String gotoRedirect(
            HttpServletRequest request,
            Model model,
            RedirectAttributes redirectAttributes
    ){
        //请求作用域中添加数据
        request.setAttribute("requestKey" , "111111");
        model.addAttribute("modelKey" , "222222");

        //地址栏追加数据  类似于 url?key=value&key=value
        redirectAttributes.addAttribute("KEY" , "VALUE");

        //响应重定向 无法携带请求域中数据，地址栏会显示目标地址
        return "redirect:mainRedirect";
    }

    @GetMapping("/mainRedirect")
    @ResponseBody
    public Map<String,Object> gotoMain(
            @RequestAttribute(value = "requestKey" , required = false)String requestKey,
            @RequestAttribute(value = "modelKey" , required = false)String modelKey,
            @RequestParam(value = "KEY",required = false , defaultValue = "")String KEY
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("requestKey" , requestKey);
        map.put("modelKey" , modelKey);
        map.put("KEY" , KEY);
        return map;
    }
}

