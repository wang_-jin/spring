package com.igeek.boot.controller;

import com.igeek.boot.entity.Person;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjin
 * 2023/12/7 20:33
 * @description TODO
 */
@RestController
public class CustomBindingController {
    @PostMapping("savePerson")
    public Person savePerson(Person person){
        return person;
    }
}
