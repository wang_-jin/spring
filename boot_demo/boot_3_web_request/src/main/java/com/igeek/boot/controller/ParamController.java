package com.igeek.boot.controller;

import com.igeek.boot.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/6 16:15
 * @description 参数注解
 */
@Controller
public class ParamController {

    /**
     * 1.@PathVariable 路径变量
     * 接收在请求的url中的数据，{key}占位符 与 @PathVariable注解中的name="key" 一致
     *
     * Postman测试，测试地址：http://localhost:8080/find/100/李四
     *
     * 使用场景：通过ID查询数据，指定ID删除数据
     */
    @GetMapping("find/{id}/{name}")
    @ResponseBody
    public Map<String,Object> find(
            @PathVariable(name = "id" , required = false)Integer id,
            @PathVariable(name = "name" , required = false)String userName,
            @PathVariable Map<String,Object> pv
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("id" , id);
        map.put("userName" , userName);
        map.put("pv" , pv);
        return map;
    }


    /**
     * 2.@RequestParam 获取请求参数   前端 --->  数据  ----> 后端
     * 方式一：表单提交  <input type="text" name="username" value="张三" />
     * Postman测试  测试地址： http://localhost:8080/add  POST方式
     * Body选项卡 -> form-data -> 填写信息（text、file）
     *
     * 方式二：携带在请求的url中   url?name=zs&age=10&hobby=music&hobby=code
     * Postman测试  测试地址： http://localhost:8080/add?name=zs&age=10&hobby=music&hobby=code  POST方式
     * Params选项卡 -> Query Params -> 填写信息
     */
    @PostMapping("add")
    @ResponseBody
    public Map<String,Object> add(
            @RequestParam(value = "name" , required = false , defaultValue = "")String name,
            @RequestParam(value = "age" , required = false , defaultValue = "18")Integer age,
            @RequestParam(value = "hobby" , required = false , defaultValue = "")String[] hobby,
            @RequestPart(value = "pic") MultipartFile pic,
            @RequestParam Map<String,Object> pv
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("name" , name);
        map.put("age" , age);
        map.put("hobby" , hobby);
        map.put("pic" , pic.getOriginalFilename());
        map.put("pv" , pv);
        return map;
    }


    /**
     * 3.
     * @ResponseBody 处理响应结果，将Java对象转成JSON数据格式
     * @RequestBody  处理请求数据，将JSON数据格式，解析成Java对象
     *
     * 问题：GET ？ POST ？ 请求，能够接收到@RequestBody数据
     * 分析：请求，包含：请求行、请求头、请求体
     * GET方式，请求行  GET  /body?key=value HTTP/1.1 、请求头  User-Agent：Mozilla/5.0
     * POST方式，请求行 POST /body HTTP/1.1、请求头 User-Agent：Mozilla/5.0、请求体 数据value
     *
     * Postman测试，测试地址：http://localhost:8080/body
     * 此时报错：错误代码400，Required request body is missing
     * 此时报错：错误代码415，Unsupported Media Type
     * 此时报错：错误代码405，Method Not Allowed
     *
     */
    @PostMapping("body")
    @ResponseBody
    public User testRequestBody(@RequestBody User user){
        user.setPassword("888888");
        return user;
    }

    /**
     * 4.@RequestHeader 获取请求头
     * GET方式，请求行  GET  /body?key=value HTTP/1.1 、请求头  User-Agent：Mozilla/5.0
     * POST方式，请求行 POST /body HTTP/1.1、请求头 User-Agent：Mozilla/5.0、请求体 数据value
     *
     * Postman测试
     * Headers选项卡 -> 携带token数据
     */
    @GetMapping("header")
    @ResponseBody
    public Map<String,Object> testRequestHeader(
            @RequestHeader(value = "User-Agent" , required = true) String userAgent,
            @RequestHeader(value = "Host" , required = true) String host,
            @RequestHeader(value = "token" , required = false , defaultValue = "") String token
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("User-Agent" , userAgent);
        map.put("Host" , host);
        map.put("token" , token);
        return map;
    }


    /**
     * 5.@CookieValue 获取Cookie值
     *
     * Postman测试
     * 选择Cookies -> 设置domain信息 localhost -> Add Cookie Cookie_2=value; Path=/; Expires=Fri, 06 Dec 2024 01:54:05 GMT;
     */
    @GetMapping("cookie")
    @ResponseBody
    public Map<String,Object> testCookieValue(
            @CookieValue(value = "username" , required = false , defaultValue = "")String username
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("username" , username);
        return map;
    }

    /**
     * 6.@RequestAttribute 获取request域属性
     * 请求作用域、会话作用域、上下文作用域、当前页作用域
     *
     * 使用场景：前后端不分离
     *
     * 6.1 发送请求数据
     */
    @GetMapping("setAttr")
    public String setAttr(
            HttpServletRequest request,
            Model model,
            ModelMap modelMap
    ){
        request.setAttribute("requestKey" , "aaa");
        model.addAttribute("modelKey" , "bbb");
        modelMap.addAttribute("modelMapKey" , "ccc");
        //请求转发，将数据携带至下一个请求
        return "forward:getAttr";
    }

    /**
     * 6.@RequestAttribute 获取request域属性
     * 6.2 接收请求数据
     */
    @GetMapping("getAttr")
    @ResponseBody
    public Map<String,Object> getAttr(
            @RequestAttribute("requestKey") String requestKey,
            @RequestAttribute("modelKey") String modelKey,
            @RequestAttribute("modelMapKey") String modelMapKey
    ){
        Map<String,Object> map = new HashMap<>();
        map.put("requestKey" , requestKey);
        map.put("modelKey" , modelKey);
        map.put("modelMapKey" , modelMapKey);
        return map;
    }

}

