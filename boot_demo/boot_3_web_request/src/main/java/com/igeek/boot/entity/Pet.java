package com.igeek.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangjin
 * 2023/12/7 15:16
 * @description TODO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pet {

    private String petName;
    private Integer petAge;

}