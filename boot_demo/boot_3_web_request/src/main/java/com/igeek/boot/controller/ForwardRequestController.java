package com.igeek.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangjin
 * 2023/12/7 20:33
 * @description TODO
 *
 * 面试题：请求转发 和 响应重定向 区别？
 * 1.语法上，
 * 请求转发   request.getRequestDispatcher("").forward(request,response);
 * 响应重定向 response.sendRedirect("");
 * 2.是否生成新的请求，
 * 请求转发，不会生成新的请求；
 * 响应重定向，会生成新的请求；
 * 3.是否携带数据，
 * 请求转发，可以携带数据；
 * 响应重定向，不可以携带数据；
 * 4.地址栏上URL，
 * 请求转发，地址栏上显示的请求地址；更适用于查询操作
 * 响应重定向，地址栏上显示的目标地址；更适用于增删改操作，避免重复提交表单
 * 5.跳转的地址，
 * 请求转发，转发至本项目中的地址；
 * 响应重定向，可以重定向到任意地址；
 */
@Controller
public class ForwardRequestController {
/*    public void test(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //请求转发
        request.getRequestDispatcher("").forward(request,response);
        //响应重定向
        response.sendRedirect("");
    }*/
    //SpringMVC版本
    @GetMapping("/gotoWard")
    public String gotoWard(
            HttpServletRequest request,
            Model model
    ){
        //设置请求属性
        request.setAttribute("requstKey","AAA");
        model.addAttribute("modelKey","BBB");
        //请求转发，可以将请求域数据携带至下一个请求
        return "forward:mainWard";
    }

    @GetMapping("/mainWard")
    @ResponseBody
    public Map<String,Object> gotoMain(
            HttpServletRequest request ,
            @RequestAttribute("requestKey") String requestKey
    ){
        Map<String,Object> map = new HashMap<>();
        //获取请求域属性
        Object requestObj = request.getAttribute("requestKey");
        Object modelObj = request.getAttribute("modelKey");
        map.put("requestObj" , requestObj);
        map.put("modelObj" , modelObj);
        map.put("requestKey" , requestKey);
        return map;
    }
}
