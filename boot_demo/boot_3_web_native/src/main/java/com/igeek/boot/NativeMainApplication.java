package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//扫描原生三大组件
//@ServletComponentScan(basePackages = "com.igeek.boot.servlet")
@SpringBootApplication
public class NativeMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(NativeMainApplication.class , args);
    }

}
