package com.igeek.boot.servlet;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

//过滤器
//@WebFilter(filterName = "MyFilter" , urlPatterns = "/css/*")
public class MyFilter implements Filter {
    public void destroy() {
        System.out.printf("MyFilter destroy销毁~");
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        System.out.println("MyFilter doFilter过滤~");

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        System.out.printf("MyFilter init初始化~");
    }

}
