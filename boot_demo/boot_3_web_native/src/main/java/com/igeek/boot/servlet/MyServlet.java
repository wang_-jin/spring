package com.igeek.boot.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * SpringMVC 、 SpringBoot
 *      - DispatcherServlet 前端控制器
 * Servlet3.0
 *      - 自定义的Servlet    类似于MyServlet
 *
 * 思考：发起 /my 的请求 ， 请问是DispatcherServlet处理呢？ MyServlet处理呢？
 * 答案：若可以被精准匹配处理，则直接是自定义的Servlet，即MyServlet进行处理
 */
//@WebServlet(name = "MyServlet" , urlPatterns = {"/my","/first"})
public class MyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.printf("MyServlet doPost~");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.printf("MyServlet doGet~");
    }
}
