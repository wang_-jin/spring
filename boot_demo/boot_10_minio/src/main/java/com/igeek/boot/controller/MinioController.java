package com.igeek.boot.controller;

import com.igeek.boot.common.Result;
import com.igeek.boot.service.impl.MinIOFileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;

/**
 * http://localhost:8080/doc.html
 *
 * @author chemin
 * @since 2024/1/17
 */
@RestController
@RequestMapping("/minio")
@Api(tags = "MinIO文件上传")
public class MinioController {

    @Autowired
    private MinIOFileStorageService minIOFileStorageService;

    @ApiOperation("文件上传操作")
    @PostMapping("upload")
    public Result upload(@ApiParam("图片信息") MultipartFile file){
        if(Objects.isNull(file)){
            return Result.error("上传错误");
        }

        //igeek.jpg
        String oldName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String fileName = uuid+oldName;
        try {
            //获取输入流
            InputStream inputStream = file.getInputStream();
            String imgPath = minIOFileStorageService.uploadImgFile("img", fileName, inputStream);
            return Result.success(imgPath , "上传成功");
        } catch (IOException e) {
            e.printStackTrace();
            return Result.error("上传错误");
        }
    }

}
