package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO
 *
 * @author chemin
 * @since 2024/1/17
 */
@SpringBootApplication
public class MinioMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinioMainApplication.class , args);
    }

}
