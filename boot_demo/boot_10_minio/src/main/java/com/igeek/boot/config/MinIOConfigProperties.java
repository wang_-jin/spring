package com.igeek.boot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 与配置文件进行配置绑定
 *
 * @author chemin
 * @since 2024/1/17
 */
@Data
@ConfigurationProperties(prefix = "minio")
public class MinIOConfigProperties {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String endpoint;
    private String readPath;

}
