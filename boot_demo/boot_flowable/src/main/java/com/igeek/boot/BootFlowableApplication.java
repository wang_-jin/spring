package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wangjin
 * 2023/12/18 22:13
 * @description TODO
 */
@SpringBootApplication
public class BootFlowableApplication {
    public static void main(String[] args) {
        SpringApplication.run(BootFlowableApplication.class, args);
    }
}
