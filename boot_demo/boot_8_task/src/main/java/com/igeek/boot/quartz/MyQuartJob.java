package com.igeek.boot.quartz;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/21
 *
 * SpringBoot整合QuartZ
 * 1.引入spring-boot-starter-quartz场景启动器
 * 2.制定任务，extends QuartzJobBean
 * 3.@Bean + @Configuration 注入JobDetail任务明细 、 Trigger触发器
 */
@Slf4j
public class MyQuartJob extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("MyQuartJob printJob 运行了....");
    }
}
