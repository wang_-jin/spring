package com.igeek.boot.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Spring Task 底层是基于 JDK 的 ScheduledThreadPoolExecutor 线程池来实现的。
 *
 * 一、使用：
 * 1.开启定时任务 @EnableScheduling
 * 2.指定任务执行的时间 @Scheduled(cron = "0/5 * * * * ?")
 *
 * 二、当出现多任务执行时
 * 1.默认单线程同步执行
 * 2.多任务之间按顺序执行，一个任务执行完成之后才会执行另一个任务
 * 出现现象：
 * 一个任务执行完上一次之后，才会执行下一次调度 spring task的调度任务是串行的，
 * 意思就是如果配了多个任务的话，前面一个任务没有结束，后面的任务即使是时间到点了也不会跑。
 *
 * 三、解决问题：
 * 方式一：调整线程池的大小 spring.com.igeek.boot.task.scheduling.pool.size=3
 * 方式二：开启异步任务@EnableAsync + 执行任务@Async  等同于 CompletableFuture异步
 *
 * @author chemin
 * @since 2023/12/21
 */
@Component
@Slf4j
//开启定时任务
@EnableScheduling
//开启异步任务
@EnableAsync
public class MyTask {

    //cron 指定任务执行的时间
    @Scheduled(cron = "0/5 * * * * ?")
    //让当前方法能在异步线程中执行
    @Async
    public void printBy5() throws InterruptedException {
        log.info("MyTask printBy5 正在打印~" + Thread.currentThread().getName());
        //沉睡30s
        TimeUnit.SECONDS.sleep(20);
    }

    //cron 指定任务执行的时间
    @Scheduled(cron = "0/10 * * * * ?")
    @Async
    public void printBy10() throws InterruptedException {
        log.info("MyTask printBy10 正在打印~" + Thread.currentThread().getName());
    }

}
