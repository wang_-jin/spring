package com.igeek.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO
 *
 * @author wangjin
 * @since 2023/12/21
 */
@SpringBootApplication
public class TaskMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskMainApplication.class , args);
    }

}
