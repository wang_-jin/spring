package com.igeek.boot.config;


import com.igeek.boot.quartz.MyEmailQuartJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.igeek.boot.quartz.MyQuartJob;


/**
 * TODO
 *
 * @author wangjin
 * @since 2023/12/21
 */
@Configuration
public class QuartConfig {

    //任务明细
    @Bean
    public JobDetail printJobDetail(){
        //构建Job工作，通过newJob()指定工作明细，storeDurably()将任务持久化
        return JobBuilder.newJob(MyEmailQuartJob.class).storeDurably().build();
    }

    //触发器
    @Bean
    public Trigger printTrigger(){
        //构建调度器  cron表达式，规定任务执行时间
        ScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0 38 * * * ?  ");
        //触发器 用来绑定 "任务明细"和"调度器"
        return TriggerBuilder.newTrigger().forJob(printJobDetail()).withSchedule(scheduleBuilder).build();
    }


}
