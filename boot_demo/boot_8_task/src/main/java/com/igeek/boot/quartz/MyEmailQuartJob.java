package com.igeek.boot.quartz;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.ResourceUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author wangjin
 * 2023/12/21 19:44
 * @description TODO
 */
@Slf4j
public class MyEmailQuartJob extends QuartzJobBean {
    @Autowired(required = false)
    private JavaMailSender javaMailSender;
    //邮件发送者
    private String from = "dangeradmin@163.com";

    //邮件接受者
    private String to = "3168850469@qq.com";

    //邮件主题
    private String subject = "邮件测试";
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        //邮件内容
        String content = "携带附件发送";
        //创建MimeMessage对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //创建MimeMessageHelper对象
        MimeMessageHelper helper = null;  //true 支持携带附件
        try {
            helper = new MimeMessageHelper(mimeMessage , true);
            helper.setFrom(from+"(汪进)");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content);
            //携带附件
            File file = ResourceUtils.getFile("classpath:Git分支合并操作.pdf");
            helper.addAttachment(file.getName() , file);
            //发送邮件
            javaMailSender.send(mimeMessage);
            //System.out.println("邮件发送成功");
            log.info("邮件发送成功");
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
