package com.igeek.boot;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.ResourceUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * TODO
 *
 * @author chemin
 * @since 2023/12/21
 */
@SpringBootTest
@Slf4j
public class EmailTest {

    @Autowired(required = false)
    private JavaMailSender javaMailSender;

    //邮件发送者
    private String from = "dangeradmin@163.com";

    //邮件接受者
    private String to = "1343347786@qq.com";

    //邮件主题
    private String subject = "邮件测试";

    //测试1：发送简单邮件内容
    @Test
    public void sendSimpleEmail(){
        //邮件内容
        String content = "测试邮件是否可以正常使用？yes";
        //创建SimpleMailMessage对象
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from+"(张三)");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        //发送邮件
        javaMailSender.send(message);
        System.out.println("邮件发送成功");
    }

    //测试2：发送邮件内容中携带HTML代码
    @Test
    public void sendHTMLEmail() throws MessagingException {
        //邮件内容
        String content = "测试携带网页内容 <h1><a href='http://www.baidu.com'>点我点我</a></h1>";
        //创建MimeMessage对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //创建MimeMessageHelper对象
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setFrom(from+"(李四)");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content,true);  //此处设置正文支持html解析
        //发送邮件
        javaMailSender.send(mimeMessage);
        System.out.println("邮件发送成功");
    }

    //测试3：发送邮件内容中携带附件
    @Test
    public void sendAttachEmail() throws MessagingException, FileNotFoundException {
        //邮件内容
        String content = "测试携带附件内容";
        //创建MimeMessage对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //创建MimeMessageHelper对象
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage , true);  //true 支持携带附件
        helper.setFrom(from+"(王二麻)");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content);

        //携带附件
        File file1 = ResourceUtils.getFile("classpath:5.jpg");
        File file2 = ResourceUtils.getFile("classpath:极客营项目.xlsx");
        helper.addAttachment("示例图片.jpg" , file1);
        helper.addAttachment(file2.getName() , file2);

        //发送邮件
        javaMailSender.send(mimeMessage);
        System.out.println("邮件发送成功");
    }
}
